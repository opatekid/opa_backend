<?php
$token = 'BOT TOKEN';
$botname = 'BOT USERNAME';

require 'Telebot.php';

$tg = new Telebot($token, $botname);
$tg->cmd('hello', 'Hello world!');

// Simple command with parameter : /echo telebot => telebot
$tg->cmd('echo', function($text){
  if (isset($text)) {
    return $text;
  } else {
    return '/echo <text>';
  }
 });


/*

Send Photo
$tg->cmd('upload', array(
  'type' => 'photo',
  'send' => 'path/to/photo.jpg'
);
// OR
$tg->cmd('upload2', function($text) {
  return array(
    'type' => 'photo',
    'send' => 'path/to/photo.jpg'
  )
});

Send Location
$tg->cmd('myloc', function($text) {
  return array(
    'type' => 'location',
    'send' => array(-7.61, 109.51) // Gombong, Kebumen, Indonesia, you can integrate with google maps api
  )
});

*/
 
$tg->run();