<?php

class Model_laporan extends CI_Model{
    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    public function get_kategori() {
        $query = $this->db->query("SELECT * FROM m_jenis_kategori_usaha where status='1' order by id_jenis desc");
        return $query;
    }
//    /////Penjualan_Kategori////
//    public function caritgl($tanggal,$jenis,$tglawal){
//        $query = $this->db->query("select a.*, b.tanggal_order from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik and date_format(b.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal' and a.id_jenis='$jenis'");
//        return $query;
//    }
//    public function caribln($jenis,$bulan,$tahun) {
//        $query = $this->db->query("select a.*, b.tanggal_order from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik and month(b.tanggal_order)='$bulan' and year(b.tanggal_order)='$tahun' and a.id_jenis='$jenis'");
//        return $query;
//    }
//    public function carithn($jenis,$tahun) {
//        $query = $this->db->query("select a.*, b.tanggal_order from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik and year(b.tanggal_order)='$tahun' and a.id_jenis='$jenis'");
//        return $query;
//    }
//    ///Penjualan_Pemilik///
//    public function caritgl_pemilik($tanggal,$tglawal){
//        $query = $this->db->query("select a.*, b.tanggal_order from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik and date_format(b.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal'");
//        return $query;
//    }
//    public function caribln_pemilik($bulan,$tahun) {
//        $query = $this->db->query("select a.*, b.tanggal_order from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik and month(b.tanggal_order)='$bulan' and year(b.tanggal_order)='$tahun'");
//        return $query;
//    }
//    public function carithn_pemilik($tahun) {
//        $query = $this->db->query("select a.*, b.tanggal_order from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik and year(b.tanggal_order)='$tahun'");
//        return $query;
//    }
//    //////Penjualan_Provinsi////
    public function get_provinsi() {
        $query = $this->db->query("select * from m_provinsi where status='1' order by nama asc");
        return $query;
    }
//    public function caritgl_prov($tanggal,$jenis,$tglawal){
//        $query = $this->db->query("select a.*, b.tanggal_order, c.nama as 'nama_provinsi' from m_pemilik_usaha a, konsumen_order b, m_provinsi c where a.id_pemilik=b.id_pemilik and a.id_provinsi=c.id and date_format(b.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal' and a.id_provinsi='$jenis'");
//        return $query;
//    }
//    public function caribln_prov($jenis,$bulan,$tahun) {
//        $query = $this->db->query("select a.*, b.tanggal_order, c.nama as 'nama_provinsi' from m_pemilik_usaha a, konsumen_order b, m_provinsi c where a.id_pemilik=b.id_pemilik and a.id_provinsi=c.id and month(b.tanggal_order)='$bulan' and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis'");
//        return $query;
//    }
//    public function carithn_prov($jenis,$tahun) {
//        $query = $this->db->query("select a.*, b.tanggal_order, c.nama as 'nama_provinsi' from m_pemilik_usaha a, konsumen_order b, m_provinsi c where a.id_pemilik=b.id_pemilik and a.id_provinsi=c.id and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis'");
//        return $query;
//    }
//    ////Penjualan_Kota///
    public function get_kotkab($id) {
        $query = $this->db->query("select * from m_kota where status='1' and id_provinsi ='$id' order by nama asc");
        return $query;   
    }
//    public function caritgl_kota($tanggal,$jenis,$tglawal,$kota){
//        $query = $this->db->query("select a.*, b.tanggal_order, c.nama as 'nama_provinsi', d.nama as 'nama_kota' from m_pemilik_usaha a, konsumen_order b, m_provinsi c, m_kota d where a.id_pemilik=b.id_pemilik and a.id_provinsi=c.id and a.id_kota=d.id and date_format(b.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal' and a.id_provinsi='$jenis' and a.id_kota='$kota'");
//        return $query;
//    }
//    public function caribln_kota($jenis,$bulan,$tahun,$kota) {
//        $query = $this->db->query("select a.*, b.tanggal_order, c.nama as 'nama_provinsi', d.nama as 'nama_kota' from m_pemilik_usaha a, konsumen_order b, m_provinsi c, m_kota d where a.id_pemilik=b.id_pemilik and a.id_provinsi=c.id and a.id_kota=d.id and month(b.tanggal_order)='$bulan' and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis' and a.id_kota='$kota'");
//        return $query;
//    }
//    public function carithn_kota($jenis,$tahun,$kota) {
//        $query = $this->db->query("select a.*, b.tanggal_order, c.nama as 'nama_provinsi', d.nama as 'nama_kota' from m_pemilik_usaha a, konsumen_order b, m_provinsi c, m_kota d where a.id_pemilik=b.id_pemilik and a.id_provinsi=c.id and a.id_kota=d.id and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis' and a.id_kota='$kota'");
//        return $query;
//    }
    
//    //////Belanja User////
//    public function caritgl_belanja_prov($tanggal,$jenis,$tglawal) {
//        $query = $this->db->query("select a.*, b.tanggal_order, c.nama as 'nama_provinsi' from m_konsumen a, konsumen_order b, m_provinsi c where a.id_konsumen=b.id_konsumen and a.id_provinsi=c.id and date_format(b.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal' and a.id_provinsi='$jenis'");
//        return $query;
//    }
//    public function caribln_belanja_prov($jenis,$bulan,$tahun) {
//        $query = $this->db->query("select a.*, b.tanggal_order, c.nama as 'nama_provinsi' from m_konsumen a, konsumen_order b, m_provinsi c where a.id_konsumen=b.id_konsumen and a.id_provinsi=c.id and month(b.tanggal_order)='$bulan' and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis'");
//        return $query;
//    }
//    public function carithn_belanja_prov($jenis,$tahun) {
//        $query = $this->db->query("select a.*, b.tanggal_order, c.nama as 'nama_provinsi' from m_konsumen a, konsumen_order b, m_provinsi c where a.id_konsumen=b.id_konsumen and a.id_provinsi=c.id and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis'");
//        return $query;
//    }
    
//    public function caritgl_belanja_kota($tanggal,$jenis,$tglawal,$kota) {
//        $query = $this->db->query("select a.*, b.tanggal_order, c.nama as 'nama_provinsi', d.nama as 'nama_kota' from m_konsumen a, konsumen_order b, m_provinsi c, m_kota d where a.id_konsumen=b.id_konsumen and a.id_provinsi=c.id and a.id_kota=d.id and date_format(b.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal' and a.id_provinsi='$jenis' and a.id_kota='$kota'");
//        return $query;
//    }
//    public function caribln_belanja_kota($jenis,$bulan,$tahun,$kota) {
//        $query = $this->db->query("select a.*, b.tanggal_order, c.nama as 'nama_provinsi', d.nama as 'nama_kota' from m_konsumen a, konsumen_order b, m_provinsi c, m_kota d where a.id_konsumen=b.id_konsumen and a.id_provinsi=c.id and a.id_kota=d.id and month(b.tanggal_order)='$bulan' and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis' and a.id_kota='$kota'");
//        return $query;
//    }
//    public function carithn_belanja_kota($jenis,$tahun,$kota) {
//        $query = $this->db->query("select a.*, b.tanggal_order, c.nama as 'nama_provinsi', d.nama as 'nama_kota' from m_konsumen a, konsumen_order b, m_provinsi c, m_kota d where a.id_konsumen=b.id_konsumen and a.id_provinsi=c.id and a.id_kota=d.id and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis' and a.id_kota='$kota'");
//        return $query;
//    }
    
//    //////Pendapatan Aplikasi///
//    public function caritgl_pendapatan_waktu($tanggal,$tglawal) {
//        $query = $this->db->query("select a.*, b.tanggal_order from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik and date_format(b.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal' and b.status='2'");
//        return $query;
//    }
//    public function caribln_pendapatan_waktu($bulan,$tahun) {
//        $query = $this->db->query("select a.*, b.tanggal_order from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik and month(b.tanggal_order)='$bulan' and year(b.tanggal_order)='$tahun' and b.status='2'");
//        return $query;
//    }
//    public function carithn_pendapatan_waktu($tahun) {
//        $query = $this->db->query("select a.*, b.tanggal_order from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik and year(b.tanggal_order)='$tahun' and b.status='2'");
//        return $query;
//    }
//    //
//    public function caritgl_pendapatan_kategori($tanggal,$jenis,$tglawal) {
//        $query = $this->db->query("select a.*, b.tanggal_order from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik and date_format(b.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal' and a.id_jenis='$jenis' and b.status='2' order by b.tanggal_order desc");
//        return $query;
//    }
//    public function caribln_pendapatan_kategori($jenis,$bulan,$tahun) {
//        $query = $this->db->query("select a.*, b.tanggal_order from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik and month(b.tanggal_order)='$bulan' and year(b.tanggal_order)='$tahun' and a.id_jenis='$jenis' and b.status='2' order by b.tanggal_order desc");
//        return $query;
//    }
//    public function carithn_pendapatan_kategori($jenis,$tahun) {
//        $query = $this->db->query("select a.*, b.tanggal_order from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik and year(b.tanggal_order)='$tahun' and a.id_jenis='$jenis' and b.status='2' order by b.tanggal_order desc");
//        return $query;
//    }
//    //
//    public function caritgl_pendapatan_prov($tanggal,$jenis,$tglawal) {
//        $query = $this->db->query("select a.*, b.tanggal_order, c.nama as 'nama_provinsi' from m_pemilik_usaha a, konsumen_order b, m_provinsi c where a.id_pemilik=b.id_pemilik and a.id_provinsi=c.id and date_format(b.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal' and a.id_provinsi='$jenis' and b.status='2' order by b.tanggal_order desc");
//        return $query;
//    }
//    public function caribln_pendapatan_prov($jenis,$bulan,$tahun) {
//        $query = $this->db->query("select a.*, b.tanggal_order, c.nama as 'nama_provinsi' from m_pemilik_usaha a, konsumen_order b, m_provinsi c where a.id_pemilik=b.id_pemilik and a.id_provinsi=c.id and month(b.tanggal_order)='$bulan' and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis' and b.status='2' order by b.tanggal_order desc");
//        return $query;
//    }
//    public function carithn_pendapatan_prov($jenis,$tahun) {
//        $query = $this->db->query("select a.*, b.tanggal_order, c.nama as 'nama_provinsi' from m_pemilik_usaha a, konsumen_order b, m_provinsi c where a.id_pemilik=b.id_pemilik and a.id_provinsi=c.id and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis' and b.status='2' order by b.tanggal_order desc");
//        return $query;
//    }
//    //
//    public function caritgl_pendapatan_kota($tanggal,$jenis,$tglawal,$kota) {
//        $query = $this->db->query("select a.*, b.tanggal_order, c.nama as 'nama_provinsi', d.nama as 'nama_kota' from m_pemilik_usaha a, konsumen_order b, m_provinsi c, m_kota d where a.id_pemilik=b.id_pemilik and a.id_provinsi=c.id and a.id_kota=d.id and date_format(b.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal' and a.id_provinsi='$jenis' and a.id_kota='$kota' and b.status='2' order by b.tanggal_order desc");
//        return $query;
//    }
//    public function caribln_pendapatan_kota($jenis,$bulan,$tahun,$kota) {
//        $query = $this->db->query("select a.*, b.tanggal_order, c.nama as 'nama_provinsi', d.nama as 'nama_kota' from m_pemilik_usaha a, konsumen_order b, m_provinsi c, m_kota d where a.id_pemilik=b.id_pemilik and a.id_provinsi=c.id and a.id_kota=d.id and a.id_provinsi=c.id and month(b.tanggal_order)='$bulan' and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis' and a.id_kota='$kota' and b.status='2' order by b.tanggal_order desc");
//        return $query;
//    }
//    public function carithn_pendapatan_kota($jenis,$tahun,$kota) {
//        $query = $this->db->query("select a.*, b.tanggal_order, c.nama as 'nama_provinsi', d.nama as 'nama_kota' from m_pemilik_usaha a, konsumen_order b, m_provinsi c, m_kota d where a.id_pemilik=b.id_pemilik and a.id_provinsi=c.id and a.id_kota=d.id and a.id_provinsi=c.id and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis' and a.id_kota='$kota' and b.status='2' order by b.tanggal_order desc");
//        return $query;
//    }
//    //
//    public function caritgl_pendapatan_pemilik($tanggal,$tglawal) {
//        $query = $this->db->query("select a.*, b.tanggal_order from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik and date_format(b.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal' and b.status='2' order by b.tanggal_order desc");
//        return $query;
//    }
//    public function caribln_pendapatan_pemilik($bulan,$tahun) {
//        $query = $this->db->query("select a.*, b.tanggal_order from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik and month(b.tanggal_order)='$bulan' and year(b.tanggal_order)='$tahun' and b.status='2' order by b.tanggal_order desc");
//        return $query;
//    }
//    public function carithn_pendapatan_pemilik($tahun) {
//        $query = $this->db->query("select a.*, b.tanggal_order from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik and year(b.tanggal_order)='$tahun' and b.status='2' order by b.tanggal_order desc");
//        return $query;
//    }
    
    ///Data User//
    public function caritgl_data_pemiliku($tanggal,$tglawal) {
        $query = $this->db->query("select * from m_pemilik_usaha where date_format(tanggal_insert,'%Y-%m-%d') between '$tglawal' and '$tanggal'");
        return $query;
    }
    public function caribln_data_pemiliku($bulan,$tahun) {
        $query = $this->db->query("select * from m_pemilik_usaha where month(tanggal_insert)='$bulan' and year(tanggal_insert)='$tahun'");
        return $query;
    }
    public function carithn_data_pemiliku($tahun) {
        $query = $this->db->query("select * from m_pemilik_usaha where year(tanggal_insert)='$tahun'");
        return $query;
    }
    //
    public function caritgl_data_konsumen($tanggal,$tglawal) {
        $query = $this->db->query("select * from m_konsumen where date_format(tanggal_insert,'%Y-%m-%d') between '$tglawal' and '$tanggal'");
        return $query;
    }
    public function caribln_data_konsumen($bulan,$tahun) {
        $query = $this->db->query("select * from m_konsumen where month(tanggal_insert)='$bulan' and year(tanggal_insert)='$tahun'");
        return $query;
    }
    public function carithn_data_konsumen($tahun) {
        $query = $this->db->query("select * from m_konsumen where year(tanggal_insert)='$tahun'");
        return $query;
    }
    
    ////serverside//////
    
    public function get_data() {
        $sql = "select jumlah from jumlah_data where id='2' ";
        
        $query =  $this->db->query($sql);
        return $query->first_row()->jumlah;
    }
    
    //////////////PENJUALAN//////////////////
    //per kategori//
    
    public function filter_pen_kat_get_data($tglawal,$tanggal,$bulan,$tahun,$jenis,$tipe) {
        $sql = "";
        if($tipe == "1"){
            $sql = "select count(*) as jumlah from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik 
                and date_format(b.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal' and a.id_jenis='$jenis'";
        }else if($tipe == "2"){
            $sql = "select count(*) as jumlah from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik 
                and month(b.tanggal_order)='$bulan' and year(b.tanggal_order)='$tahun' and a.id_jenis='$jenis'";
        }else if($tipe == "3"){
            $sql = "select count(*) as jumlah from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik 
                and year(b.tanggal_order)='$tahun' and a.id_jenis='$jenis'";
        }
        
        $query =  $this->db->query($sql);
        return $query->first_row()->jumlah;
    }
    
    public function filter_pen_kat($requestData,$tglawal,$tanggal,$bulan,$tahun,$jenis,$tipe) {
        
        //tipe -> 1= cari per tanggal, 2= cari per bulan, 3=cari pertahun
        $sql = "";
        
        if($tipe == "1"){
            $sql = "select a.*, b.tanggal_order,b.total from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik 
                and date_format(b.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal' and a.id_jenis='$jenis'";
        }else if($tipe == "2"){
            $sql = "select a.*, b.tanggal_order,b.total from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik 
                and month(b.tanggal_order)='$bulan' and year(b.tanggal_order)='$tahun' and a.id_jenis='$jenis'";
        }else if($tipe == "3"){
            $sql = "select a.*, b.tanggal_order,b.total from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik 
                and year(b.tanggal_order)='$tahun' and a.id_jenis='$jenis'";
        }
        
        $sql .= " AND ( b.tanggal_order LIKE '%".$requestData['search']['value']."%' 
                OR a.nama LIKE '%".$requestData['search']['value']."%'
                OR b.tanggal_order LIKE '%".$requestData['search']['value']."%'
            OR b.total LIKE '%".$requestData['search']['value']."%') ";
        
        $query = $this->db->query($sql);
        return $query;
    }
    
    public function order_pen_kat($requestData,$tglawal,$tanggal,$bulan,$tahun,$jenis,$tipe) {
        $sql = "";
        if($tipe == "1"){
            $sql = "select a.*, b.tanggal_order,b.total from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik 
                and date_format(b.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal' and a.id_jenis='$jenis'";
        }else if($tipe == "2"){
            $sql = "select a.*, b.tanggal_order,b.total from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik 
                and month(b.tanggal_order)='$bulan' and year(b.tanggal_order)='$tahun' and a.id_jenis='$jenis'";
        }else if($tipe == "3"){
            $sql = "select a.*, b.tanggal_order,b.total from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik and year(b.tanggal_order)='$tahun'  and a.id_jenis='$jenis'";
        }
        
        if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
            $sql .= " AND ( b.tanggal_order LIKE '%".$requestData['search']['value']."%' 
                OR a.nama LIKE '%".$requestData['search']['value']."%'
                OR b.tanggal_order LIKE '%".$requestData['search']['value']."%'
            OR b.total LIKE '%".$requestData['search']['value']."%') ";
        }
        
        $sql.=" order by b.tanggal_order LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
        
        $query = $this->db->query($sql);
        return $query;
    }
    
    
    //per pemilik//
    public function filter_pen_pemilik($requestData,$tglawal,$tanggal,$bulan,$tahun,$tipe) {
        $sql = "";
        if($tipe == "1"){
            $sql = "select a.*, b.tanggal_order,b.total from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik"
                    . " and date_format(b.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal'";
        }else if($tipe == "2"){
            $sql = "select a.*, b.tanggal_order,b.total from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik"
                    . " and month(b.tanggal_order)='$bulan' and year(b.tanggal_order)='$tahun'";
        }else if($tipe == "3"){
            $sql = "select a.*, b.tanggal_order,b.total from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik 
                and year(b.tanggal_order)='$tahun'";
        }
        
        $sql .= " AND ( b.tanggal_order LIKE '%".$requestData['search']['value']."%' 
                OR a.nama LIKE '%".$requestData['search']['value']."%'
                OR b.tanggal_order LIKE '%".$requestData['search']['value']."%'
            OR b.total LIKE '%".$requestData['search']['value']."%') ";
        
        $query = $this->db->query($sql);
        return $query;
    }
    
    public function filter_pen_pemilik_get_data($tglawal,$tanggal,$bulan,$tahun,$tipe) {
        $sql = "";
        if($tipe == "1"){
            $sql = "select count(*) as jumlah from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik"
                    . " and date_format(b.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal'";
        }else if($tipe == "2"){
            $sql = "select count(*) as jumlah from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik"
                    . " and month(b.tanggal_order)='$bulan' and year(b.tanggal_order)='$tahun'";
        }else if($tipe == "3"){
            $sql = "select count(*) as jumlah from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik 
                and year(b.tanggal_order)='$tahun'";
        }
        
        $query =  $this->db->query($sql);
        return $query->first_row()->jumlah;
    }
    
    public function order_pen_pemilik($requestData,$tglawal,$tanggal,$bulan,$tahun,$tipe) {
        $sql = "";
        if($tipe == "1"){
            $sql = "select a.*, b.tanggal_order,b.total from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik 
                and date_format(b.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal'";
        }else if($tipe == "2"){
            $sql = "select a.*, b.tanggal_order,b.total from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik 
                and month(b.tanggal_order)='$bulan' and year(b.tanggal_order)='$tahun'";
        }else if($tipe == "3"){
            $sql = "select a.*, b.tanggal_order,b.total from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik 
                and year(b.tanggal_order)='$tahun'";
        }
        
        if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
            $sql .= " AND ( b.tanggal_order LIKE '%".$requestData['search']['value']."%' 
                OR a.nama LIKE '%".$requestData['search']['value']."%'
                OR b.tanggal_order LIKE '%".$requestData['search']['value']."%'
            OR b.total LIKE '%".$requestData['search']['value']."%') ";
        }
        
        $sql.=" order by b.tanggal_order LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
        
        $query = $this->db->query($sql);
        return $query;
    }

    public function filter_pen_pemilik_get_data_with_id_pemilik($tglawal,$tanggal,$bulan,$tahun,$tipe,$id_pemilik) {
        $sql = "";
        if($tipe == "1"){
            $sql = "select count(*) as jumlah, sum(b.total) as sum from m_pemilik_usaha a left join konsumen_order b on a.id_pemilik=b.id_pemilik left join m_konsumen c on c.id_konsumen=b.id_konsumen where b.tanggal_order between '$tanggal 00:00:00' and '$tanggal 23:59:59' and a.id_pemilik='$id_pemilik'";
        }else if($tipe == "2"){
            $sql = "select count(*) as jumlah, sum(b.total) as sum from m_pemilik_usaha a left join konsumen_order b on a.id_pemilik=b.id_pemilik left join m_konsumen c on c.id_konsumen=b.id_konsumen where month(b.tanggal_order)='$bulan' and year(b.tanggal_order)='$tahun' and a.id_pemilik='$id_pemilik'";
        }else if($tipe == "3"){
            $sql = "select count(*) as jumlah, sum(b.total) as sum from m_pemilik_usaha a left join konsumen_order b on a.id_pemilik=b.id_pemilik left join m_konsumen c on c.id_konsumen=b.id_konsumen where year(b.tanggal_order)='$tahun' and a.id_pemilik='$id_pemilik'";
        }
        
        $query =  $this->db->query($sql);
        return $query->first_row();
    }
    
    public function order_pen_pemilik_with_id_pemilik($requestData,$tglawal,$tanggal,$bulan,$tahun,$tipe,$id_pemilik) {
        $sql = "";
        if($tipe == "1"){
            // $sql = "select a.*, b.tanggal_order,b.total, c.nama as nama_pelanggan from m_pemilik_usaha a, konsumen_order b, m_konsumen c where a.id_pemilik=b.id_pemilik 
            //     and date_format(b.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal' and a.id_pemilik='$id_pemilik'";
            $sql = "select a.*, b.tanggal_order,b.total, c.nama as nama_pelanggan from konsumen_order b left join m_pemilik_usaha a on a.id_pemilik=b.id_pemilik left join m_konsumen c on c.id_konsumen=b.id_konsumen where b.tanggal_order between '$tanggal 00:00:00' and '$tanggal 23:59:59' and a.id_pemilik='$id_pemilik'";
        } else if($tipe == "2"){
            // $sql = "select a.*, b.tanggal_order,b.total, c.nama as nama_pelanggan from m_pemilik_usaha a, konsumen_order b, m_konsumen c where a.id_pemilik=b.id_pemilik 
            //     and month(b.tanggal_order)='$bulan' and year(b.tanggal_order)='$tahun' and a.id_pemilik='$id_pemilik'";
            $sql = "select a.*, b.tanggal_order,b.total, c.nama as nama_pelanggan from konsumen_order b left join m_pemilik_usaha a on a.id_pemilik=b.id_pemilik left join m_konsumen c on c.id_konsumen=b.id_konsumen where month(b.tanggal_order)='$bulan' and year(b.tanggal_order)='$tahun' and a.id_pemilik='$id_pemilik'";
        } else if($tipe == "3"){
            // $sql = "select a.*, b.tanggal_order,b.total, c.nama as nama_pelanggan from m_pemilik_usaha a, konsumen_order b, m_konsumen c where a.id_pemilik=b.id_pemilik 
            //     and year(b.tanggal_order)='$tahun' and a.id_pemilik='$id_pemilik'";
            $sql = "select a.*, b.tanggal_order,b.total, c.nama as nama_pelanggan from konsumen_order b left join m_pemilik_usaha a on a.id_pemilik=b.id_pemilik left join m_konsumen c on c.id_konsumen=b.id_konsumen where year(b.tanggal_order)='$tahun' and a.id_pemilik='$id_pemilik'";
        }
        
        if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
            $sql .= " AND ( b.tanggal_order LIKE '%".$requestData['search']['value']."%' 
                OR a.nama LIKE '%".$requestData['search']['value']."%'
                OR b.tanggal_order LIKE '%".$requestData['search']['value']."%'
            OR b.total LIKE '%".$requestData['search']['value']."%') ";
        }
        
        $sql.=" order by b.tanggal_order LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
        
        // echo $sql; die();
        $query = $this->db->query($sql);
        return $query;
    }
    
    //per provinsi//
    public function filter_pen_prov($requestData,$tglawal,$tanggal,$bulan,$tahun,$tipe,$jenis) {
        $sql = "";
        if($tipe == "1"){
            $sql = "select a.*, b.tanggal_order, c.nama as 'nama_provinsi' from m_pemilik_usaha a, konsumen_order b, m_provinsi c where a.id_pemilik=b.id_pemilik"
                    . " and a.id_provinsi=c.id and date_format(b.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal' and a.id_provinsi='$jenis'";
        }else if($tipe == "2"){
            $sql = "select a.*, b.tanggal_order, c.nama as 'nama_provinsi' from m_pemilik_usaha a, konsumen_order b, m_provinsi c where a.id_pemilik=b.id_pemilik"
                    . " and a.id_provinsi=c.id and month(b.tanggal_order)='$bulan' and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis'";
        }else if($tipe == "3"){
            $sql = "select a.*, b.tanggal_order, c.nama as 'nama_provinsi' from m_pemilik_usaha a, konsumen_order b, m_provinsi c where a.id_pemilik=b.id_pemilik"
                    . " and a.id_provinsi=c.id and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis'";
        }
        
        $sql .= " AND ( b.tanggal_order LIKE '%".$requestData['search']['value']."%' 
                OR c.nama LIKE '%".$requestData['search']['value']."%'
                OR a.nama LIKE '%".$requestData['search']['value']."%'
                OR b.tanggal_order LIKE '%".$requestData['search']['value']."%'
                OR a.kredit LIKE '%".$requestData['search']['value']."%') ";
        
        $query = $this->db->query($sql);
        return $query;
    }
    
    public function filter_pen_prov_get_data($tglawal,$tanggal,$bulan,$tahun,$tipe,$jenis) {
        $sql = "";
        if($tipe == "1"){
            $sql = "select count(*) as jumlah from m_pemilik_usaha a, konsumen_order b, m_provinsi c where a.id_pemilik=b.id_pemilik"
                    . " and a.id_provinsi=c.id and date_format(b.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal' and a.id_provinsi='$jenis'";
        }else if($tipe == "2"){
            $sql = "select count(*) as jumlah from m_pemilik_usaha a, konsumen_order b, m_provinsi c where a.id_pemilik=b.id_pemilik"
                    . " and a.id_provinsi=c.id and month(b.tanggal_order)='$bulan' and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis'";
        }else if($tipe == "3"){
            $sql = "select count(*) as jumlah from m_pemilik_usaha a, konsumen_order b, m_provinsi c where a.id_pemilik=b.id_pemilik"
                    . " and a.id_provinsi=c.id and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis'";
        }
        
        $query =  $this->db->query($sql);
        return $query->first_row()->jumlah;
    }
    
    public function order_pen_prov($requestData,$tglawal,$tanggal,$bulan,$tahun,$tipe,$jenis) {
        $sql = "";
        if($tipe == "1"){
            $sql = "select a.*, b.tanggal_order, c.nama as 'nama_provinsi' from m_pemilik_usaha a, konsumen_order b, m_provinsi c where a.id_pemilik=b.id_pemilik"
                    . " and a.id_provinsi=c.id and date_format(b.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal' and a.id_provinsi='$jenis'";
        }else if($tipe == "2"){
            $sql = "select a.*, b.tanggal_order, c.nama as 'nama_provinsi' from m_pemilik_usaha a, konsumen_order b, m_provinsi c where a.id_pemilik=b.id_pemilik"
                    . " and a.id_provinsi=c.id and month(b.tanggal_order)='$bulan' and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis'";
        }else if($tipe == "3"){
            $sql = "select a.*, b.tanggal_order, c.nama as 'nama_provinsi' from m_pemilik_usaha a, konsumen_order b, m_provinsi c where a.id_pemilik=b.id_pemilik"
                    . " and a.id_provinsi=c.id and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis'";
        }
        
        if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
            $sql .= " AND ( b.tanggal_order LIKE '%".$requestData['search']['value']."%' 
                OR c.nama LIKE '%".$requestData['search']['value']."%'
                OR a.nama LIKE '%".$requestData['search']['value']."%'
                OR b.tanggal_order LIKE '%".$requestData['search']['value']."%'
                OR a.kredit LIKE '%".$requestData['search']['value']."%') ";
        }
        
        $sql.=" order by b.tanggal_order LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
        
        $query = $this->db->query($sql);
        return $query;
    }
    
    //per kota//
    public function filter_pen_kota($requestData,$tglawal,$tanggal,$bulan,$tahun,$tipe,$jenis,$kota) {
        $sql = "";
        if($tipe == "1"){
            $sql = "select a.*, b.tanggal_order, c.nama as 'nama_provinsi', d.nama as 'nama_kota' from m_pemilik_usaha a, konsumen_order b, m_provinsi c, m_kota d where a.id_pemilik=b.id_pemilik"
                    . " and a.id_provinsi=c.id and a.id_kota=d.id and date_format(b.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal' and a.id_provinsi='$jenis' and a.id_kota='$kota'";
        }else if($tipe == "2"){
            $sql = "select a.*, b.tanggal_order, c.nama as 'nama_provinsi', d.nama as 'nama_kota' from m_pemilik_usaha a, konsumen_order b, m_provinsi c, m_kota d where a.id_pemilik=b.id_pemilik"
                    . " and a.id_provinsi=c.id and a.id_kota=d.id and month(b.tanggal_order)='$bulan' and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis' and a.id_kota='$kota'";
        }else if($tipe == "3"){
            $sql = "select a.*, b.tanggal_order, c.nama as 'nama_provinsi', d.nama as 'nama_kota' from m_pemilik_usaha a, konsumen_order b, m_provinsi c, m_kota d where a.id_pemilik=b.id_pemilik"
                    . " and a.id_provinsi=c.id and a.id_kota=d.id and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis' and a.id_kota='$kota'";
        }
        
        $sql .= " AND ( b.tanggal_order LIKE '%".$requestData['search']['value']."%' 
                OR c.nama LIKE '%".$requestData['search']['value']."%'
                OR a.nama LIKE '%".$requestData['search']['value']."%'
                OR d.nama LIKE '%".$requestData['search']['value']."%'
                OR b.tanggal_order LIKE '%".$requestData['search']['value']."%'
                OR a.kredit LIKE '%".$requestData['search']['value']."%') ";
        
        $query = $this->db->query($sql);
        return $query;
    }
    
    public function filter_pen_kota_get_data($tglawal,$tanggal,$bulan,$tahun,$tipe,$jenis,$kota) {
        $sql = "";
        if($tipe == "1"){
            $sql = "select count(*) as jumlah from m_pemilik_usaha a, konsumen_order b, m_provinsi c, m_kota d where a.id_pemilik=b.id_pemilik"
                    . " and a.id_provinsi=c.id and a.id_kota=d.id and date_format(b.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal' and a.id_provinsi='$jenis' and a.id_kota='$kota'";
        }else if($tipe == "2"){
            $sql = "select count(*) as jumlah from m_pemilik_usaha a, konsumen_order b, m_provinsi c, m_kota d where a.id_pemilik=b.id_pemilik"
                    . " and a.id_provinsi=c.id and a.id_kota=d.id and month(b.tanggal_order)='$bulan' and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis' and a.id_kota='$kota'";
        }else if($tipe == "3"){
            $sql = "select count(*) as jumlah from m_pemilik_usaha a, konsumen_order b, m_provinsi c, m_kota d where a.id_pemilik=b.id_pemilik"
                    . " and a.id_provinsi=c.id and a.id_kota=d.id and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis' and a.id_kota='$kota'";
        }
        
        $query =  $this->db->query($sql);
        return $query->first_row()->jumlah;
    }
    
    public function order_pen_kota($requestData,$tglawal,$tanggal,$bulan,$tahun,$tipe,$jenis,$kota) {
        $sql = "";
        if($tipe == "1"){
            $sql = "select a.*, b.tanggal_order, c.nama as 'nama_provinsi', d.nama as 'nama_kota' from m_pemilik_usaha a, konsumen_order b, m_provinsi c, m_kota d where a.id_pemilik=b.id_pemilik"
                    . " and a.id_provinsi=c.id and a.id_kota=d.id and date_format(b.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal' and a.id_provinsi='$jenis' and a.id_kota='$kota'";
        }else if($tipe == "2"){
            $sql = "select a.*, b.tanggal_order, c.nama as 'nama_provinsi', d.nama as 'nama_kota' from m_pemilik_usaha a, konsumen_order b, m_provinsi c, m_kota d where a.id_pemilik=b.id_pemilik"
                    . " and a.id_provinsi=c.id and a.id_kota=d.id and month(b.tanggal_order)='$bulan' and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis' and a.id_kota='$kota'";
        }else if($tipe == "3"){
            $sql = "select a.*, b.tanggal_order, c.nama as 'nama_provinsi', d.nama as 'nama_kota' from m_pemilik_usaha a, konsumen_order b, m_provinsi c, m_kota d where a.id_pemilik=b.id_pemilik"
                    . " and a.id_provinsi=c.id and a.id_kota=d.id and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis' and a.id_kota='$kota'";
        }
        
        if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
            $sql .= " AND ( b.tanggal_order LIKE '%".$requestData['search']['value']."%' 
                OR c.nama LIKE '%".$requestData['search']['value']."%'
                OR a.nama LIKE '%".$requestData['search']['value']."%'
                OR d.nama LIKE '%".$requestData['search']['value']."%'
                OR b.tanggal_order LIKE '%".$requestData['search']['value']."%'
                OR a.kredit LIKE '%".$requestData['search']['value']."%') ";
        }
        
        $sql.=" order by b.tanggal_order LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
        
        $query = $this->db->query($sql);
        return $query;
    }
    
    ////////Belanja User///
    ///per Provinsi///
    public function filter_bel_prov($requestData,$tglawal,$tanggal,$bulan,$tahun,$tipe,$jenis) {
        $sql = "";
        if($tipe == "1"){
            $sql = "select a.*, b.tanggal_order, c.nama as 'nama_provinsi' from m_konsumen a, konsumen_order b, m_provinsi c where a.id_konsumen=b.id_konsumen"
                    . " and a.id_provinsi=c.id and date_format(b.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal' and a.id_provinsi='$jenis'";
        }else if($tipe == "2"){
            $sql = "select a.*, b.tanggal_order, c.nama as 'nama_provinsi' from m_konsumen a, konsumen_order b, m_provinsi c where a.id_konsumen=b.id_konsumen"
                    . " and a.id_provinsi=c.id and month(b.tanggal_order)='$bulan' and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis'";
        }else if($tipe == "3"){
            $sql = "select a.*, b.tanggal_order, c.nama as 'nama_provinsi' from m_konsumen a, konsumen_order b, m_provinsi c where a.id_konsumen=b.id_konsumen"
                    . " and a.id_provinsi=c.id and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis'";
        }
        
        $sql .= " AND ( b.tanggal_order LIKE '%".$requestData['search']['value']."%' 
                OR c.nama LIKE '%".$requestData['search']['value']."%'
                OR a.nama LIKE '%".$requestData['search']['value']."%'
                OR b.tanggal_order LIKE '%".$requestData['search']['value']."%'
                OR a.alamat LIKE '%".$requestData['search']['value']."%') ";
        
        $query = $this->db->query($sql);
        return $query;
    }
    
    public function filter_bel_prov_get_data($tglawal,$tanggal,$bulan,$tahun,$tipe,$jenis) {
        $sql = "";
        if($tipe == "1"){
            $sql = "select count(*) as jumlah from m_konsumen a, konsumen_order b, m_provinsi c where a.id_konsumen=b.id_konsumen"
                    . " and a.id_provinsi=c.id and date_format(b.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal' and a.id_provinsi='$jenis'";
        }else if($tipe == "2"){
            $sql = "select count(*) as jumlah from m_konsumen a, konsumen_order b, m_provinsi c where a.id_konsumen=b.id_konsumen"
                    . " and a.id_provinsi=c.id and month(b.tanggal_order)='$bulan' and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis'";
        }else if($tipe == "3"){
            $sql = "select count(*) as jumlah from m_konsumen a, konsumen_order b, m_provinsi c where a.id_konsumen=b.id_konsumen"
                    . " and a.id_provinsi=c.id and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis'";
        }
        
        $query =  $this->db->query($sql);
        return $query->first_row()->jumlah;
    }
    
    public function order_bel_prov($requestData,$tglawal,$tanggal,$bulan,$tahun,$tipe,$jenis) {
        $sql = "";
        if($tipe == "1"){
            $sql = "select a.*, b.tanggal_order, c.nama as 'nama_provinsi' from m_konsumen a, konsumen_order b, m_provinsi c where a.id_konsumen=b.id_konsumen"
                    . " and a.id_provinsi=c.id and date_format(b.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal' and a.id_provinsi='$jenis'";
        }else if($tipe == "2"){
            $sql = "select a.*, b.tanggal_order, c.nama as 'nama_provinsi' from m_konsumen a, konsumen_order b, m_provinsi c where a.id_konsumen=b.id_konsumen"
                    . " and a.id_provinsi=c.id and month(b.tanggal_order)='$bulan' and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis'";
        }else if($tipe == "3"){
            $sql = "select a.*, b.tanggal_order, c.nama as 'nama_provinsi' from m_konsumen a, konsumen_order b, m_provinsi c where a.id_konsumen=b.id_konsumen"
                    . " and a.id_provinsi=c.id and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis'";
        }
        
        if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
            $sql .= " AND ( b.tanggal_order LIKE '%".$requestData['search']['value']."%' 
                OR c.nama LIKE '%".$requestData['search']['value']."%'
                OR a.nama LIKE '%".$requestData['search']['value']."%'
                OR b.tanggal_order LIKE '%".$requestData['search']['value']."%'
                OR a.alamat LIKE '%".$requestData['search']['value']."%') ";
        }
        
        $sql.=" order by b.tanggal_order LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
        
        $query = $this->db->query($sql);
        return $query;
    }
    
    //per Kota//
    public function filter_bel_kota($requestData,$tglawal,$tanggal,$bulan,$tahun,$tipe,$jenis,$kota) {
        $sql = "";
        if($tipe == "1"){
            $sql = "select a.*, b.tanggal_order, c.nama as 'nama_provinsi', d.nama as 'nama_kota' from m_konsumen a, konsumen_order b, m_provinsi c, m_kota d where a.id_konsumen=b.id_konsumen"
                    . " and a.id_provinsi=c.id and a.id_kota=d.id and date_format(b.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal' and a.id_provinsi='$jenis' and a.id_kota='$kota'";
        }else if($tipe == "2"){
            $sql = "select a.*, b.tanggal_order, c.nama as 'nama_provinsi', d.nama as 'nama_kota' from m_konsumen a, konsumen_order b, m_provinsi c, m_kota d where a.id_konsumen=b.id_konsumen"
                    . " and a.id_provinsi=c.id and a.id_kota=d.id and month(b.tanggal_order)='$bulan' and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis' and a.id_kota='$kota'";
        }else if($tipe == "3"){
            $sql = "select a.*, b.tanggal_order, c.nama as 'nama_provinsi', d.nama as 'nama_kota' from m_konsumen a, konsumen_order b, m_provinsi c, m_kota d where a.id_konsumen=b.id_konsumen"
                    . " and a.id_provinsi=c.id and a.id_kota=d.id and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis' and a.id_kota='$kota'";
        }
        
        $sql .= " AND ( b.tanggal_order LIKE '%".$requestData['search']['value']."%' 
                OR c.nama LIKE '%".$requestData['search']['value']."%'
                OR a.nama LIKE '%".$requestData['search']['value']."%'
                OR d.nama LIKE '%".$requestData['search']['value']."%'
                OR b.tanggal_order LIKE '%".$requestData['search']['value']."%'
                OR a.alamat LIKE '%".$requestData['search']['value']."%') ";
        
        $query = $this->db->query($sql);
        return $query;
    }
    
    public function filter_bel_kota_get_data($tglawal,$tanggal,$bulan,$tahun,$tipe,$jenis,$kota) {
        $sql = "";
        if($tipe == "1"){
            $sql = "select count(*) as jumlah from m_konsumen a, konsumen_order b, m_provinsi c, m_kota d where a.id_konsumen=b.id_konsumen"
                    . " and a.id_provinsi=c.id and a.id_kota=d.id and date_format(b.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal' and a.id_provinsi='$jenis' and a.id_kota='$kota'";
        }else if($tipe == "2"){
            $sql = "select count(*) as jumlah from m_konsumen a, konsumen_order b, m_provinsi c, m_kota d where a.id_konsumen=b.id_konsumen"
                    . " and a.id_provinsi=c.id and a.id_kota=d.id and month(b.tanggal_order)='$bulan' and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis' and a.id_kota='$kota'";
        }else if($tipe == "3"){
            $sql = "select count(*) as jumlah from m_konsumen a, konsumen_order b, m_provinsi c, m_kota d where a.id_konsumen=b.id_konsumen"
                    . " and a.id_provinsi=c.id and a.id_kota=d.id and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis' and a.id_kota='$kota'";
        }
        
        $query =  $this->db->query($sql);
        return $query->first_row()->jumlah;
    }
    
    public function order_bel_kota($requestData,$tglawal,$tanggal,$bulan,$tahun,$tipe,$jenis,$kota) {
        $sql = "";
        if($tipe == "1"){
            $sql = "select a.*, b.tanggal_order, c.nama as 'nama_provinsi', d.nama as 'nama_kota' from m_konsumen a, konsumen_order b, m_provinsi c, m_kota d where a.id_konsumen=b.id_konsumen"
                    . " and a.id_provinsi=c.id and a.id_kota=d.id and date_format(b.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal' and a.id_provinsi='$jenis' and a.id_kota='$kota'";
        }else if($tipe == "2"){
            $sql = "select a.*, b.tanggal_order, c.nama as 'nama_provinsi', d.nama as 'nama_kota' from m_konsumen a, konsumen_order b, m_provinsi c, m_kota d where a.id_konsumen=b.id_konsumen"
                    . " and a.id_provinsi=c.id and a.id_kota=d.id and month(b.tanggal_order)='$bulan' and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis' and a.id_kota='$kota'";
        }else if($tipe == "3"){
            $sql = "select a.*, b.tanggal_order, c.nama as 'nama_provinsi', d.nama as 'nama_kota' from m_konsumen a, konsumen_order b, m_provinsi c, m_kota d where a.id_konsumen=b.id_konsumen"
                    . " and a.id_provinsi=c.id and a.id_kota=d.id and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis' and a.id_kota='$kota'";
        }
        
        if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
            $sql .= " AND ( b.tanggal_order LIKE '%".$requestData['search']['value']."%' 
                OR c.nama LIKE '%".$requestData['search']['value']."%'
                OR a.nama LIKE '%".$requestData['search']['value']."%'
                OR d.nama LIKE '%".$requestData['search']['value']."%'
                OR b.tanggal_order LIKE '%".$requestData['search']['value']."%'
                OR a.alamat LIKE '%".$requestData['search']['value']."%') ";
        }
        
        $sql.=" order by b.tanggal_order LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
        
        $query = $this->db->query($sql);
        return $query;
    }
    
    ////////Pendapatan Aplikasi///////
    
    //per Waktu//
    public function filter_pendapatan_waktu($requestData,$tglawal,$tanggal,$bulan,$tahun,$tipe) {
        $sql = "";
        if($tipe == "1"){
            $sql = "select a.*, b.tanggal_order from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik"
                    . " and date_format(b.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal' and b.status='2'";
        }else if($tipe == "2"){
            $sql = "select a.*, b.tanggal_order from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik"
                    . " and month(b.tanggal_order)='$bulan' and year(b.tanggal_order)='$tahun' and b.status='2'";
        }else if($tipe == "3"){
            $sql = "select a.*, b.tanggal_order from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik"
                    . " and year(b.tanggal_order)='$tahun' and b.status='2'";
        }
        
        $sql .= " AND ( b.tanggal_order LIKE '%".$requestData['search']['value']."%' 
                OR a.nama LIKE '%".$requestData['search']['value']."%'
                OR b.tanggal_order LIKE '%".$requestData['search']['value']."%'
                OR a.kredit LIKE '%".$requestData['search']['value']."%') ";
        
        $query = $this->db->query($sql);
        return $query;
    }
    
    public function filter_pendapatan_waktu_get_data($tglawal,$tanggal,$bulan,$tahun,$tipe) {
        $sql = "";
        if($tipe == "1"){
            $sql = "select count(*) as jumlah from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik"
                    . " and date_format(b.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal' and b.status='2'";
        }else if($tipe == "2"){
            $sql = "select count(*) as jumlah from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik"
                    . " and month(b.tanggal_order)='$bulan' and year(b.tanggal_order)='$tahun' and b.status='2'";
        }else if($tipe == "3"){
            $sql = "select count(*) as jumlah from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik"
                    . " and year(b.tanggal_order)='$tahun' and b.status='2'";
        }
        
        $query =  $this->db->query($sql);
        return $query->first_row()->jumlah;
    }
    
    public function order_pendapatan_waktu($requestData,$tglawal,$tanggal,$bulan,$tahun,$tipe) {
        $sql = "";
        if($tipe == "1"){
            $sql = "select a.*, b.tanggal_order from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik"
                    . " and date_format(b.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal' and b.status='2'";
        }else if($tipe == "2"){
            $sql = "select a.*, b.tanggal_order from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik"
                    . " and month(b.tanggal_order)='$bulan' and year(b.tanggal_order)='$tahun' and b.status='2'";
        }else if($tipe == "3"){
            $sql = "select a.*, b.tanggal_order from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik"
                    . " and year(b.tanggal_order)='$tahun' and b.status='2'";
        }
        
        if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
            $sql .= " AND ( b.tanggal_order LIKE '%".$requestData['search']['value']."%' 
                OR a.nama LIKE '%".$requestData['search']['value']."%'
                OR b.tanggal_order LIKE '%".$requestData['search']['value']."%'
                OR a.kredit LIKE '%".$requestData['search']['value']."%') ";
        }
        
        $sql.=" order by b.tanggal_order LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
        
        $query = $this->db->query($sql);
        return $query;
    }
    
    //per Ketegori//
    public function filter_pendapatan_kategori($requestData,$tglawal,$tanggal,$bulan,$tahun,$tipe,$jenis) {
        $sql = "";
        if($tipe == "1"){
            $sql = "select a.*, b.tanggal_order from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik"
                    . " and date_format(b.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal' and a.id_jenis='$jenis' and b.status='2'";
        }else if($tipe == "2"){
            $sql = "select a.*, b.tanggal_order from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik"
                    . " and month(b.tanggal_order)='$bulan' and year(b.tanggal_order)='$tahun' and a.id_jenis='$jenis' and b.status='2'";
        }else if($tipe == "3"){
            $sql = "select a.*, b.tanggal_order from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik"
                    . " and year(b.tanggal_order)='$tahun' and a.id_jenis='$jenis' and b.status='2'";
        }
        
        $sql .= " AND ( b.tanggal_order LIKE '%".$requestData['search']['value']."%' 
                OR a.nama LIKE '%".$requestData['search']['value']."%'
                OR b.tanggal_order LIKE '%".$requestData['search']['value']."%'
                OR a.kredit LIKE '%".$requestData['search']['value']."%') ";
        
        $query = $this->db->query($sql);
        return $query;
    }
    
    public function filter_pendapatan_kategori_get_data($tglawal,$tanggal,$bulan,$tahun,$tipe,$jenis) {
        $sql = "";
        if($tipe == "1"){
            $sql = "select count(*) as jumlah from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik"
                    . " and date_format(b.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal' and a.id_jenis='$jenis' and b.status='2'";
        }else if($tipe == "2"){
            $sql = "select count(*) as jumlah from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik"
                    . " and month(b.tanggal_order)='$bulan' and year(b.tanggal_order)='$tahun' and a.id_jenis='$jenis' and b.status='2'";
        }else if($tipe == "3"){
            $sql = "select count(*) as jumlah from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik"
                    . " and year(b.tanggal_order)='$tahun' and a.id_jenis='$jenis' and b.status='2'";
        }
        
        $query =  $this->db->query($sql);
        return $query->first_row()->jumlah;
    }
    
    public function order_pendapatan_kategori($requestData,$tglawal,$tanggal,$bulan,$tahun,$tipe,$jenis) {
        $sql = "";
        if($tipe == "1"){
            $sql = "select a.*, b.tanggal_order from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik"
                    . " and date_format(b.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal' and a.id_jenis='$jenis' and b.status='2'";
        }else if($tipe == "2"){
            $sql = "select a.*, b.tanggal_order from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik"
                    . " and month(b.tanggal_order)='$bulan' and year(b.tanggal_order)='$tahun' and a.id_jenis='$jenis' and b.status='2'";
        }else if($tipe == "3"){
            $sql = "select a.*, b.tanggal_order from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik"
                    . " and year(b.tanggal_order)='$tahun' and a.id_jenis='$jenis' and b.status='2'";
        }
        
        if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
            $sql .= " AND ( b.tanggal_order LIKE '%".$requestData['search']['value']."%' 
                OR a.nama LIKE '%".$requestData['search']['value']."%'
                OR b.tanggal_order LIKE '%".$requestData['search']['value']."%'
                OR a.kredit LIKE '%".$requestData['search']['value']."%') ";
        }
        
        $sql.=" order by b.tanggal_order LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
        
        $query = $this->db->query($sql);
        return $query;
    }
    
    //per Provinsi//
    public function filter_pendapatan_prov($requestData,$tglawal,$tanggal,$bulan,$tahun,$tipe,$jenis) {
        $sql = "";
        if($tipe == "1"){
            $sql = "select a.*, b.tanggal_order, c.nama as 'nama_provinsi' from m_pemilik_usaha a, konsumen_order b, m_provinsi c where a.id_pemilik=b.id_pemilik"
                    . " and a.id_provinsi=c.id and date_format(b.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal' and a.id_provinsi='$jenis' and b.status='2'";
        }else if($tipe == "2"){
            $sql = "select a.*, b.tanggal_order, c.nama as 'nama_provinsi' from m_pemilik_usaha a, konsumen_order b, m_provinsi c where a.id_pemilik=b.id_pemilik"
                    . " and a.id_provinsi=c.id and month(b.tanggal_order)='$bulan' and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis' and b.status='2'";
        }else if($tipe == "3"){
            $sql = "select a.*, b.tanggal_order, c.nama as 'nama_provinsi' from m_pemilik_usaha a, konsumen_order b, m_provinsi c where a.id_pemilik=b.id_pemilik"
                    . " and a.id_provinsi=c.id and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis' and b.status='2'";
        }
        
        $sql .= " AND ( b.tanggal_order LIKE '%".$requestData['search']['value']."%' 
                OR c.nama LIKE '%".$requestData['search']['value']."%'
                OR a.nama LIKE '%".$requestData['search']['value']."%'
                OR b.tanggal_order LIKE '%".$requestData['search']['value']."%'
                OR a.kredit LIKE '%".$requestData['search']['value']."%') ";
        
        $query = $this->db->query($sql);
        return $query;
    }
    
    public function filter_pendapatan_prov_get_data($tglawal,$tanggal,$bulan,$tahun,$tipe,$jenis) {
        $sql = "";
        if($tipe == "1"){
            $sql = "select count(*) as jumlah from m_pemilik_usaha a, konsumen_order b, m_provinsi c where a.id_pemilik=b.id_pemilik"
                    . " and a.id_provinsi=c.id and date_format(b.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal' and a.id_provinsi='$jenis' and b.status='2'";
        }else if($tipe == "2"){
            $sql = "select count(*) as jumlah from m_pemilik_usaha a, konsumen_order b, m_provinsi c where a.id_pemilik=b.id_pemilik"
                    . " and a.id_provinsi=c.id and month(b.tanggal_order)='$bulan' and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis' and b.status='2'";
        }else if($tipe == "3"){
            $sql = "select count(*) as jumlah from m_pemilik_usaha a, konsumen_order b, m_provinsi c where a.id_pemilik=b.id_pemilik"
                    . " and a.id_provinsi=c.id and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis' and b.status='2'";
        }
        
        $query =  $this->db->query($sql);
        return $query->first_row()->jumlah;
    }
    
    public function order_pendapatan_prov($requestData,$tglawal,$tanggal,$bulan,$tahun,$tipe,$jenis) {
        $sql = "";
        if($tipe == "1"){
            $sql = "select a.*, b.tanggal_order, c.nama as 'nama_provinsi' from m_pemilik_usaha a, konsumen_order b, m_provinsi c where a.id_pemilik=b.id_pemilik"
                    . " and a.id_provinsi=c.id and date_format(b.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal' and a.id_provinsi='$jenis' and b.status='2'";
        }else if($tipe == "2"){
            $sql = "select a.*, b.tanggal_order, c.nama as 'nama_provinsi' from m_pemilik_usaha a, konsumen_order b, m_provinsi c where a.id_pemilik=b.id_pemilik"
                    . " and a.id_provinsi=c.id and month(b.tanggal_order)='$bulan' and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis' and b.status='2'";
        }else if($tipe == "3"){
            $sql = "select a.*, b.tanggal_order, c.nama as 'nama_provinsi' from m_pemilik_usaha a, konsumen_order b, m_provinsi c where a.id_pemilik=b.id_pemilik"
                    . " and a.id_provinsi=c.id and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis' and b.status='2'";
        }
        
        if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
            $sql .= " AND ( b.tanggal_order LIKE '%".$requestData['search']['value']."%' 
                OR c.nama LIKE '%".$requestData['search']['value']."%'
                OR a.nama LIKE '%".$requestData['search']['value']."%'
                OR b.tanggal_order LIKE '%".$requestData['search']['value']."%'
                OR a.kredit LIKE '%".$requestData['search']['value']."%') ";
        }
        
        $sql.=" order by b.tanggal_order LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
        
        $query = $this->db->query($sql);
        return $query;
    }
    
    //per Kota//
    public function filter_pendapat_kota($requestData,$tglawal,$tanggal,$bulan,$tahun,$tipe,$jenis,$kota) {
        $sql = "";
        if($tipe == "1"){
            $sql = "select a.*, b.tanggal_order, c.nama as 'nama_provinsi', d.nama as 'nama_kota' from m_pemilik_usaha a, konsumen_order b, m_provinsi c, m_kota d where a.id_pemilik=b.id_pemilik"
                    . " and a.id_provinsi=c.id and a.id_kota=d.id and date_format(b.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal' and a.id_provinsi='$jenis' and a.id_kota='$kota' and b.status='2'";
        }else if($tipe == "2"){
            $sql = "select a.*, b.tanggal_order, c.nama as 'nama_provinsi', d.nama as 'nama_kota' from m_pemilik_usaha a, konsumen_order b, m_provinsi c, m_kota d where a.id_pemilik=b.id_pemilik"
                    . " and a.id_provinsi=c.id and a.id_kota=d.id and a.id_provinsi=c.id and month(b.tanggal_order)='$bulan' and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis' and a.id_kota='$kota' and b.status='2'";
        }else if($tipe == "3"){
            $sql = "select a.*, b.tanggal_order, c.nama as 'nama_provinsi', d.nama as 'nama_kota' from m_pemilik_usaha a, konsumen_order b, m_provinsi c, m_kota d where a.id_pemilik=b.id_pemilik"
                    . " and a.id_provinsi=c.id and a.id_kota=d.id and a.id_provinsi=c.id and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis' and a.id_kota='$kota' and b.status='2'";
        }
        
        $sql .= " AND ( b.tanggal_order LIKE '%".$requestData['search']['value']."%' 
                OR c.nama LIKE '%".$requestData['search']['value']."%'
                OR a.nama LIKE '%".$requestData['search']['value']."%'
                OR d.nama LIKE '%".$requestData['search']['value']."%'
                OR b.tanggal_order LIKE '%".$requestData['search']['value']."%'
                OR a.kredit LIKE '%".$requestData['search']['value']."%') ";
        
        $query = $this->db->query($sql);
        return $query;
    }
    
    public function filter_pendapat_kota_get_data($tglawal,$tanggal,$bulan,$tahun,$tipe,$jenis,$kota) {
        $sql = "";
        if($tipe == "1"){
            $sql = "select count(*) as jumlah from m_pemilik_usaha a, konsumen_order b, m_provinsi c, m_kota d where a.id_pemilik=b.id_pemilik"
                    . " and a.id_provinsi=c.id and a.id_kota=d.id and date_format(b.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal' and a.id_provinsi='$jenis' and a.id_kota='$kota' and b.status='2'";
        }else if($tipe == "2"){
            $sql = "select count(*) as jumlah from m_pemilik_usaha a, konsumen_order b, m_provinsi c, m_kota d where a.id_pemilik=b.id_pemilik"
                    . " and a.id_provinsi=c.id and a.id_kota=d.id and a.id_provinsi=c.id and month(b.tanggal_order)='$bulan' and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis' and a.id_kota='$kota' and b.status='2'";
        }else if($tipe == "3"){
            $sql = "select count(*) as jumlah from m_pemilik_usaha a, konsumen_order b, m_provinsi c, m_kota d where a.id_pemilik=b.id_pemilik"
                    . " and a.id_provinsi=c.id and a.id_kota=d.id and a.id_provinsi=c.id and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis' and a.id_kota='$kota' and b.status='2'";
        }
        
        $query =  $this->db->query($sql);
        return $query->first_row()->jumlah;
    }
    
    public function order_pendapat_kota($requestData,$tglawal,$tanggal,$bulan,$tahun,$tipe,$jenis,$kota) {
        $sql = "";
        if($tipe == "1"){
            $sql = "select a.*, b.tanggal_order, c.nama as 'nama_provinsi', d.nama as 'nama_kota' from m_pemilik_usaha a, konsumen_order b, m_provinsi c, m_kota d where a.id_pemilik=b.id_pemilik"
                    . " and a.id_provinsi=c.id and a.id_kota=d.id and date_format(b.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal' and a.id_provinsi='$jenis' and a.id_kota='$kota' and b.status='2'";
        }else if($tipe == "2"){
            $sql = "select a.*, b.tanggal_order, c.nama as 'nama_provinsi', d.nama as 'nama_kota' from m_pemilik_usaha a, konsumen_order b, m_provinsi c, m_kota d where a.id_pemilik=b.id_pemilik"
                    . " and a.id_provinsi=c.id and a.id_kota=d.id and a.id_provinsi=c.id and month(b.tanggal_order)='$bulan' and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis' and a.id_kota='$kota' and b.status='2'";
        }else if($tipe == "3"){
            $sql = "select a.*, b.tanggal_order, c.nama as 'nama_provinsi', d.nama as 'nama_kota' from m_pemilik_usaha a, konsumen_order b, m_provinsi c, m_kota d where a.id_pemilik=b.id_pemilik"
                    . " and a.id_provinsi=c.id and a.id_kota=d.id and a.id_provinsi=c.id and year(b.tanggal_order)='$tahun' and a.id_provinsi='$jenis' and a.id_kota='$kota' and b.status='2'";
        }
        
        if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
            $sql .= " AND ( b.tanggal_order LIKE '%".$requestData['search']['value']."%' 
                OR c.nama LIKE '%".$requestData['search']['value']."%'
                OR a.nama LIKE '%".$requestData['search']['value']."%'
                OR d.nama LIKE '%".$requestData['search']['value']."%'
                OR b.tanggal_order LIKE '%".$requestData['search']['value']."%'
                OR a.kredit LIKE '%".$requestData['search']['value']."%') ";
        }
        
        $sql.=" order by b.tanggal_order LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
        
        $query = $this->db->query($sql);
        return $query;
    }
    
    //per Pemilik//
    public function filter_pendapatan_pemilik($requestData,$tglawal,$tanggal,$bulan,$tahun,$tipe) {
        $sql = "";
        if($tipe == "1"){
            $sql = "select a.*, b.tanggal_order from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik"
                    . " and date_format(b.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal' and b.status='2'";
        }else if($tipe == "2"){
            $sql = "select a.*, b.tanggal_order from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik"
                    . " and month(b.tanggal_order)='$bulan' and year(b.tanggal_order)='$tahun' and b.status='2'";
        }else if($tipe == "3"){
            $sql = "select a.*, b.tanggal_order from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik"
                    . " and year(b.tanggal_order)='$tahun' and b.status='2'";
        }
        
        $sql .= " AND ( b.tanggal_order LIKE '%".$requestData['search']['value']."%' 
                OR a.nama LIKE '%".$requestData['search']['value']."%'
                OR a.alamat LIKE '%".$requestData['search']['value']."%'
                OR b.tanggal_order LIKE '%".$requestData['search']['value']."%'
                OR a.kredit LIKE '%".$requestData['search']['value']."%') ";
        
        $query = $this->db->query($sql);
        return $query;
    }
    
    public function filter_pendapatan_pemilik_get_data($tglawal,$tanggal,$bulan,$tahun,$tipe) {
        $sql = "";
        if($tipe == "1"){
            $sql = "select count(*) as jumlah from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik"
                    . " and date_format(b.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal' and b.status='2'";
        }else if($tipe == "2"){
            $sql = "select count(*) as jumlah from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik"
                    . " and month(b.tanggal_order)='$bulan' and year(b.tanggal_order)='$tahun' and b.status='2'";
        }else if($tipe == "3"){
            $sql = "select count(*) as jumlah from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik"
                    . " and year(b.tanggal_order)='$tahun' and b.status='2'";
        }
        
        $query =  $this->db->query($sql);
        return $query->first_row()->jumlah;
    }
    
    public function order_pendapatan_pemilik($requestData,$tglawal,$tanggal,$bulan,$tahun,$tipe) {
        $sql = "";
        if($tipe == "1"){
            $sql = "select a.*, b.tanggal_order from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik"
                    . " and date_format(b.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal' and b.status='2'";
        }else if($tipe == "2"){
            $sql = "select a.*, b.tanggal_order from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik"
                    . " and month(b.tanggal_order)='$bulan' and year(b.tanggal_order)='$tahun' and b.status='2'";
        }else if($tipe == "3"){
            $sql = "select a.*, b.tanggal_order from m_pemilik_usaha a, konsumen_order b where a.id_pemilik=b.id_pemilik"
                    . " and year(b.tanggal_order)='$tahun' and b.status='2'";
        }
        
        if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
            $sql .= " AND ( b.tanggal_order LIKE '%".$requestData['search']['value']."%' 
                OR a.nama LIKE '%".$requestData['search']['value']."%'
                OR a.alamat LIKE '%".$requestData['search']['value']."%'
                OR b.tanggal_order LIKE '%".$requestData['search']['value']."%'
                OR a.kredit LIKE '%".$requestData['search']['value']."%') ";
        }
        
        $sql.=" order by b.tanggal_order LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
        
        $query = $this->db->query($sql);
        return $query;
    }
    
    ///////////Data User///////
    //pemilik usaha//
    public function filter_data_pemiliku($requestData,$tglawal,$tanggal,$bulan,$tahun,$tipe) {
        $sql = "";
        if($tipe == "1"){
            $sql = "select * from m_pemilik_usaha where date_format(tanggal_insert,'%Y-%m-%d') between '$tglawal' and '$tanggal'";
        }else if($tipe == "2"){
            $sql = "select * from m_pemilik_usaha where month(tanggal_insert)='$bulan' and year(tanggal_insert)='$tahun'";
        }else if($tipe == "3"){
            $sql = "select * from m_pemilik_usaha where year(tanggal_insert)='$tahun'";
        }
        
        $sql .= " AND ( tanggal_insert LIKE '%".$requestData['search']['value']."%' 
                OR nama LIKE '%".$requestData['search']['value']."%'
                OR alamat LIKE '%".$requestData['search']['value']."%'
                OR tanggal_insert LIKE '%".$requestData['search']['value']."%'
                OR kontak LIKE '%".$requestData['search']['value']."%') ";
        
        $query = $this->db->query($sql);
        return $query;
    }
    
    public function filter_data_pemiliku_get_data($tglawal,$tanggal,$bulan,$tahun,$tipe) {
        $sql = "";
        if($tipe == "1"){
            $sql = "select count(*) as jumlah from m_pemilik_usaha where date_format(tanggal_insert,'%Y-%m-%d') between '$tglawal' and '$tanggal'";
        }else if($tipe == "2"){
            $sql = "select count(*) as jumlah from m_pemilik_usaha where month(tanggal_insert)='$bulan' and year(tanggal_insert)='$tahun'";
        }else if($tipe == "3"){
            $sql = "select count(*) as jumlah from m_pemilik_usaha where year(tanggal_insert)='$tahun'";
        }
        
        $query =  $this->db->query($sql);
        return $query->first_row()->jumlah;
    }
    
    public function order_data_pemiliku($requestData,$tglawal,$tanggal,$bulan,$tahun,$tipe) {
        $sql = "";
        if($tipe == "1"){
            $sql = "select * from m_pemilik_usaha where date_format(tanggal_insert,'%Y-%m-%d') between '$tglawal' and '$tanggal'";
        }else if($tipe == "2"){
            $sql = "select * from m_pemilik_usaha where month(tanggal_insert)='$bulan' and year(tanggal_insert)='$tahun'";
        }else if($tipe == "3"){
            $sql = "select * from m_pemilik_usaha where year(tanggal_insert)='$tahun'";
        }
        
        if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
            $sql .= " AND ( tanggal_insert LIKE '%".$requestData['search']['value']."%' 
                OR nama LIKE '%".$requestData['search']['value']."%'
                OR alamat LIKE '%".$requestData['search']['value']."%'
                OR tanggal_insert LIKE '%".$requestData['search']['value']."%'
                OR kontak LIKE '%".$requestData['search']['value']."%') ";
        }
        
        $sql.=" order by tanggal_insert DESC LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
        
        $query = $this->db->query($sql);
        return $query;
    }
    
    //konsumen//
    public function filter_data_konsumen($requestData,$tglawal,$tanggal,$bulan,$tahun,$tipe) {
        $sql = "";
        if($tipe == "1"){
            $sql = "select * from m_konsumen where date_format(tanggal_insert,'%Y-%m-%d') between '$tglawal' and '$tanggal'";
        }else if($tipe == "2"){
            $sql = "select * from m_konsumen where month(tanggal_insert)='$bulan' and year(tanggal_insert)='$tahun'";
        }else if($tipe == "3"){
            $sql = "select * from m_konsumen where year(tanggal_insert)='$tahun'";
        }
        
        $sql .= " AND ( tanggal_insert LIKE '%".$requestData['search']['value']."%' 
                OR nama LIKE '%".$requestData['search']['value']."%'
                OR alamat LIKE '%".$requestData['search']['value']."%'
                OR tanggal_insert LIKE '%".$requestData['search']['value']."%'
                OR kontak LIKE '%".$requestData['search']['value']."%') ";
        
        $query = $this->db->query($sql);
        return $query;
    }
    
    public function filter_data_konsumen_get_data($tglawal,$tanggal,$bulan,$tahun,$tipe) {
        $sql = "";
        if($tipe == "1"){
            $sql = "select count(*) as jumlah from m_konsumen where date_format(tanggal_insert,'%Y-%m-%d') between '$tglawal' and '$tanggal'";
        }else if($tipe == "2"){
            $sql = "select count(*) as jumlah from m_konsumen where month(tanggal_insert)='$bulan' and year(tanggal_insert)='$tahun'";
        }else if($tipe == "3"){
            $sql = "select count(*) as jumlah from m_konsumen where year(tanggal_insert)='$tahun'";
        }
        
        $query =  $this->db->query($sql);
        return $query->first_row()->jumlah;
    }
    
    public function order_data_konsumen($requestData,$tglawal,$tanggal,$bulan,$tahun,$tipe) {
        $sql = "";
        if($tipe == "1"){
            $sql = "select * from m_konsumen where date_format(tanggal_insert,'%Y-%m-%d') between '$tglawal' and '$tanggal'";
        }else if($tipe == "2"){
            $sql = "select * from m_konsumen where month(tanggal_insert)='$bulan' and year(tanggal_insert)='$tahun'";
        }else if($tipe == "3"){
            $sql = "select * from m_konsumen where year(tanggal_insert)='$tahun'";
        }
        
        if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
            $sql .= " AND ( tanggal_insert LIKE '%".$requestData['search']['value']."%' 
                OR nama LIKE '%".$requestData['search']['value']."%'
                OR alamat LIKE '%".$requestData['search']['value']."%'
                OR tanggal_insert LIKE '%".$requestData['search']['value']."%'
                OR kontak LIKE '%".$requestData['search']['value']."%') ";
        }
        
        $sql.=" order by tanggal_insert LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
        
        $query = $this->db->query($sql);
        return $query;
    }
}