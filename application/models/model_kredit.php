<?php

class Model_kredit extends CI_Model{
    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    public function get_kredit() {
        $query = $this->db->query("select * from m_pemilik_usaha where status='1' order by id_pemilik desc");
        return $query;
    }
    
    public function get_kredit_detail($id) {
        $query = $this->db->query("select tanggal as 'tanggal','Isi Kredit' as ket, nominal as 'nominal' from isi_kredit where id_pemilik='$id'
                                union
                                select tanggal_order as 'tanggal',concat('Fee transaksi konsumen ',b.nama) as ket, fee as 'nominal' from konsumen_order a,m_konsumen b where a.id_konsumen=b.id_konsumen and a.id_pemilik='$id' and a.status='2' order by tanggal desc");
        return $query;
    }
}