<?php

class Model_transaksi extends CI_Model{
    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    /////UPGRADE PEMILIK USAHA////
    public function get_upgrade() {
        $query = $this->db->query("SELECT a.*, b.nama as 'nama_level', c.nama as 'nama_jenis' FROM m_pemilik_usaha a, m_level b, m_jenis_kategori_usaha c where a.level=b.id and a.id_jenis=c.id_jenis order by a.id_pemilik desc");
        return $query;
    }
    public function get_level() {
        $query = $this->db->query("SELECT * FROM m_level where status='1' order by id desc");
        return $query;
    }
    public function get_kategori() {
        $query = $this->db->query("SELECT * FROM m_jenis_kategori_usaha where status='1' order by id_jenis desc");
        return $query;
    }
    
    public function upgrade($idp,$nama,$jenis,$level,$id,$idpemilik_up){
        $namae = $this->db->escape_str($nama);
        $this->db->query("update m_pemilik_usaha set level='$level',tipe=2 where id_pemilik='$id'");
        $this->db->query("insert into pemilik_upgrade(id, id_pemilik, id_admin, id_level) values('$idpemilik_up','$id','$idp','$level')");
    }
    
    ///ISI KREDIT///
    public function isi_kredit($idp,$nama,$tipe,$nominal,$id,$idpemilik_up){
        $namae = $this->db->escape_str($nama);
        $this->db->query("update m_pemilik_usaha set kredit=kredit+'$nominal' where id_pemilik='$id'");
        $this->db->query("insert into isi_kredit(id, id_pemilik, id_admin, nominal) values('$idpemilik_up','$id','$idp','$nominal')");
    }
    
    
    ///////////////serveride///////////////
    
    public function get_data() {
        $sql = "select jumlah from jumlah_data where id='1' ";
        
        $query =  $this->db->query($sql);
        return $query->first_row()->jumlah;

    }
      ////UpgradePusaha///
    public function get_data_filter($requestData) {
        $sql = "";
        if($requestData['search']['value'] == "free"){
            $sql = "SELECT a.*, b.nama as 'nama_level', c.nama as 'nama_jenis' FROM m_pemilik_usaha a, m_level b, 
                m_jenis_kategori_usaha c where a.level=b.id and a.id_jenis=c.id_jenis   
                AND a.tipe='1' ";
        }else if($requestData['search']['value'] == "premium"){
            $sql = "SELECT a.*, b.nama as 'nama_level', c.nama as 'nama_jenis' FROM m_pemilik_usaha a, m_level b, 
                m_jenis_kategori_usaha c where a.level=b.id and a.id_jenis=c.id_jenis   
                AND a.tipe='2' ";
        }else{
            $sql = "SELECT a.*, b.nama as 'nama_level', c.nama as 'nama_jenis' FROM m_pemilik_usaha a, m_level b, 
                m_jenis_kategori_usaha c where a.level=b.id and a.id_jenis=c.id_jenis   
            AND ( a.nama LIKE '%".$requestData['search']['value']."%' 
                    OR b.nama LIKE '%".$requestData['search']['value']."%'
                OR c.nama LIKE '%".$requestData['search']['value']."%') ";
        }
        
        $sql.=" order by a.nama";
        $query = $this->db->query($sql);
        return $query;
    }
    
    public function get_data_order($requestData) {
        
        $sql = "SELECT a.*, b.nama as 'nama_level', c.nama as 'nama_jenis' FROM m_pemilik_usaha a, m_level b, 
            m_jenis_kategori_usaha c where a.level=b.id and a.id_jenis=c.id_jenis ";
        if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
            if($requestData['search']['value'] == "free"){
                $sql .= "AND a.tipe='1' ";
            }else if($requestData['search']['value'] == "premium"){
                $sql .= "AND a.tipe='2' ";
            }else{
                $sql .= "AND ( a.nama LIKE '%".$requestData['search']['value']."%' 
                    OR b.nama LIKE '%".$requestData['search']['value']."%'
                OR c.nama LIKE '%".$requestData['search']['value']."%'  ) ";
            }
            
        }
        
        $sql.=" order by a.nama LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
        
        $query = $this->db->query($sql);
        return $query;
    }
    
    ///IsiKredit///
    public function get_data_filterisi($requestData) {
        $sql = "";
        if($requestData['search']['value'] == "free"){
            $sql = "SELECT a.*, b.nama as 'nama_level', c.nama as 'nama_jenis' FROM m_pemilik_usaha a, m_level b, 
                m_jenis_kategori_usaha c where a.level=b.id and a.id_jenis=c.id_jenis   
                AND a.tipe='1' ";
        }else if($requestData['search']['value'] == "premium"){
            $sql = "SELECT a.*, b.nama as 'nama_level', c.nama as 'nama_jenis' FROM m_pemilik_usaha a, m_level b, 
                m_jenis_kategori_usaha c where a.level=b.id and a.id_jenis=c.id_jenis   
                AND a.tipe='2' ";
        }else{
            $sql = "SELECT a.*, b.nama as 'nama_level', c.nama as 'nama_jenis' FROM m_pemilik_usaha a, m_level b, 
                m_jenis_kategori_usaha c where a.level=b.id and a.id_jenis=c.id_jenis   
            AND ( a.nama LIKE '%".$requestData['search']['value']."%' 
                    OR b.nama LIKE '%".$requestData['search']['value']."%'
                OR a.kredit LIKE '%".$requestData['search']['value']."%') ";
        }
        
        $sql.=" order by a.nama";
        $query = $this->db->query($sql);
        return $query;
    }
    
    public function get_data_orderisi($requestData) {
        
        $sql = "SELECT a.*, b.nama as 'nama_level', c.nama as 'nama_jenis' FROM m_pemilik_usaha a, m_level b, 
            m_jenis_kategori_usaha c where a.level=b.id and a.id_jenis=c.id_jenis ";
        if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
            if($requestData['search']['value'] == "free"){
                $sql .= "AND a.tipe='1' ";
            }else if($requestData['search']['value'] == "premium"){
                $sql .= "AND a.tipe='2' ";
            }else{
                $sql .= "AND ( a.nama LIKE '%".$requestData['search']['value']."%' 
                    OR b.nama LIKE '%".$requestData['search']['value']."%'
                OR a.kredit LIKE '%".$requestData['search']['value']."%'  ) ";
            }
            
        }
        
        $sql.=" order by a.nama LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
        
        $query = $this->db->query($sql);
        return $query;
    }
}