<?php  
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_partner extends CI_Model 
{

    function __construct()
    {
            parent::__construct();
            $this->load->database();
    }

    public function get_last_banner() {
        $query = $this->db->query("select p.* from merchant_banner p order by p.id desc LIMIT 1");
        return $query->row();
    }


    public function get_banners($id_partner) {
        $query = $this->db->query("SELECT a.*, CONCAT(CONCAT('" . DOMAIN_NAME . "', 'berkas/image/partner/'), a.image) as image FROM `merchant_banner` a left join m_partner mp on a.id_partner=mp.id_partner where a.id_partner='$id_partner'");
        return $query;
    } 

    // public function get_banners($id_partner) {
    //     $query = $this->db->query("SELECT a.*, REPLACE(CONCAT('" . ROOT_URL_IMAGE_PARTNER . "', mp.username), 'https://opaindonesia.com/./berkas/image/partner/', 'https://opaindonesia.com/./berkas/image/partner/') as image FROM `merchant_banner` a left join m_partner mp on a.id_partner=mp.id_partner where a.id_partner='$id_partner'");
    //     return $query;
    // } 

    public function add_banner($id_partner) {
        $query = $this->db->query("INSERT INTO merchant_banner (id_partner, image) VALUE ('$id_partner', '')");
        return $this->db->insert_id();
    } 

    public function set_banner($id, $image) {
        $query = $this->db->query("UPDATE merchant_banner SET image='$image' WHERE id=".$id);
        return $query;
    } 



    public function set_produk_filename($id, $produk_filename) {
        $query = $this->db->query("UPDATE m_partner SET produk_filename='$produk_filename' WHERE id_partner='".$id."'");
        return $query;
    } 


    public function set_foto_produk_filename($id, $zip_filename) {
        $stmt = "UPDATE m_partner SET zip_filename='$zip_filename' WHERE id_partner=".$id;
        // echo $stmt; die();
        $query = $this->db->query($stmt);
        return $query;
    } 


    public function delete_banner($id) {
        $query = $this->db->query("DELETE FROM merchant_banner WHERE id=".$id);
        return $query;
    }  


    function get_by_username($username){
        $query = $this->db->query("select * from m_partner where username = '$username'");
        return $query;
    }

    function get($id_partner){
        $query = $this->db->query("select * from m_partner where id_partner = '$id_partner' LIMIT 1");
        return $query;
    }


    function kirim_kode($id_kode,$kode){
       $this->db->query("INSERT INTO `kode_sms`(`id`, `kode`) VALUES('$id_kode','$kode')"); 
    }

    function cek_no_telp($no_telp){
        $query = $this->db->query("select count(*) as total from m_partner where kontak = '$no_telp'");
        return $query->first_row()->total;
    }

    function aktifasi($id_pemilik){
        $query = $this->db->query("update m_partner set status=1 where id_pemilik='$id_pemilik'");
        return $query;
    }

    function deaktifasi($id_pemilik){
        $query = $this->db->query("update m_partner set status=0 where id_pemilik='$id_pemilik'");
        return $query;
    }

    function cek_usr_email($email, $id) {
        $query = $this->db->query("select count(*) as total from m_partner where id_partner <> '$id' and email = '$email'");
        return $query->first_row()->total;
    }

    function cek_usr_username($username, $id) {
        $query = $this->db->query("select count(*) as total from m_partner where id_partner <> '$id' and username = '$username'");
        return $query->first_row()->total;
    }

    public function sunting_prof($id,$nama,$username,$email) {
        $namae = $this->db->escape_str($nama);
        $usernamee = $this->db->escape_str($username);
        $emaile = $this->db->escape_str($email);
        $this->db->query("update m_partner set nama='$namae', username='$usernamee', email='$emaile' where id_partner='$id'");
    }

    function cek_kode($id_kode,$kode){
        $id_kode    = $this->db->escape_str($id_kode);
        $kode    = $this->db->escape_str($kode);
        $query = $this->db->query("select count(*) as total from kode_sms where id = '$id_kode' and kode = '$kode'");
        return $query->first_row()->total;
    }

    function login($no_telp,$password){

        $no_telp   = $this->db->escape_str($no_telp);
        $password   = hash("sha512",$password);

        // $no_telp   = "081265582400";
        // $password   = "a79a122977d9a519f17dea1335ef4dda415f0d3b5005c3cafa1bc7081770240a292f123e4cd14fa986720b99eca38abfb12ab81a765d06cbe00d5b3486b52b93";
       
        $query = $this->db->query("select * from m_partner where kontak='$no_telp' and password='$password' and status=1");
       
        return $query;
    } 

    function u_pwd($new_password, $id) {

        $query = $this->db->query("update m_partner set password='$new_password' where id_partner='$id'");
        return $query;
    }

    function insert_lokasi_usaha($id_pemilik,$id_lokasi,$jenis_lokasi){

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $jenis_lokasi    = $this->db->escape_str($jenis_lokasi);
        $id_lokasi    = $this->db->escape_str($id_lokasi);
        
        $this->db->query("insert into p_detail_lokasi (id_lokasi,id_pemilik,nama,lat,lng,jenis_lokasi)
            values ('$id_lokasi','$id_pemilik','Nama Lokasi','0','0','$jenis_lokasi')");
    }

    function update_nama_lokasi_usaha($id_lokasi,$nama_lokasi){

        $nama_lokasi    = $this->db->escape_str($nama_lokasi);
        $id_lokasi    = $this->db->escape_str($id_lokasi);
        
        $this->db->query("update p_detail_lokasi set nama = '$nama_lokasi' where id_lokasi = '$id_lokasi'");
    }

    function update_lokasi_usaha($id_lokasi,$nama_lokasi,$lat,$lng){

        $lat    = $this->db->escape_str($lat);
        $lng    = $this->db->escape_str($lng);
        $id_lokasi    = $this->db->escape_str($id_lokasi);
        
        $this->db->query("update p_detail_lokasi set lat = '$lat', lng = '$lng', nama = '$nama_lokasi' where id_lokasi = '$id_lokasi'");
    }

    function get_pemilik_all() {
        return $this->db->query("select * from m_partner")->result();
    }

    //LUPA PASS//
    public function get_email($email) {
        $emaile = $this->db->escape_str($email);
        $query = $this->db->query ("select * from m_partner where email = '$emaile' and status='1'");
        return $query;
    }

    public function update_lupa_pas($n_pass,$email){
        $this->db->query("update m_partner set password='$n_pass' where email='$email'");
    }

    function get_pemilik_of_partner($id_partner) {
        return $this->db->query("select * from m_pemilik_usaha where id_partner='$id_partner'")->result();
    }

    function update_id($id_pemilik, $new_id) {
        return $this->db->query("update m_partner set id_pemilik='$new_id' where id_pemilik='$id_pemilik'");
    }


    function get_lokasi_usaha($id_pemilik){
        
        $id_pemilik    = $this->db->escape_str($id_pemilik);
       
        $query = $this->db->query("select * from p_detail_lokasi where id_pemilik = '$id_pemilik' ");
       
        return $query;
    } 

    function get_radius(){
        $query = $this->db->query("select * from m_radius order by radius");
        return $query->result();
    }
        
    function get_random_num() {
        $result = $this->db->query("SELECT FLOOR(RAND() * 99999999) AS random_num
            FROM m_partner WHERE 'random_num' NOT IN (SELECT id_pemilik FROM m_partner)
            LIMIT 1")->row();
        return $result->random_num;
    }

    function get_kategori_produk(){

        $query = $this->db->query("select * from m_kategori_produk where status=1");
        
        return $query->result();
    }

    function get_kategori_produk_pemilik($id_pemilik){

        $query = $this->db->query("select * from m_kategori_produk where id_pemilik='$id_pemilik'");
        
        return $query->result();
    }

    function get_kategori_produk_by_nama($kategori,$id_pemilik){

        $kategori = $this->db->escape_str($kategori);
        $id_pemilik= $this->db->escape_str($id_pemilik);

        /*$query = $this->db->query("select * from m_kategori_produk where status=1 and nama_kategori='$nama'");*/
        $query = $this->db->query("select * from m_kategori_produk where nama_kategori='$kategori' and id_pemilik='$id_pemilik'");  
        
        return $query->first_row()->id_kategori;
    }

    function insert_kategori_produk($kategori,$id_pemilik){
	
        $query_kategori = "insert into m_kategori_produk (id_pemilik,nama_kategori)
                            SELECT '$id_pemilik',tmp.* FROM (SELECT '$kategori') AS tmp
                            WHERE NOT EXISTS (
                                SELECT nama_kategori FROM m_kategori_produk WHERE nama_kategori = '$kategori' and id_pemilik = '$id_pemilik' 
                            ) LIMIT 1";
                            //echo $query_kategori;
                            
        //$query_kategori = "insert into m_kategori_produk (nama_kategori,id_pemilik) values ('$kategori','$id_pemilik')";                  
        $this->db->query($query_kategori);
        return $this->db->insert_id();
    }

    function insert_produk($id_produk,$id_pemilik,$nama,$harga,$harga_promo,$stok,$keterangan,$id_kategori, $min_antar){        

        $id_produk    = $this->db->escape_str($id_produk);
        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $nama    = $this->db->escape_str($nama);
        $harga    = $this->db->escape_str($harga);
        $harga_promo    = $this->db->escape_str($harga_promo);
        $stok    = $this->db->escape_str($stok);
        $keterangan    = $this->db->escape_str($keterangan);
        $id_kategori    = $this->db->escape_str($id_kategori);

        $query_produk   = "INSERT INTO `p_detail_produk`(`id_produk`, `id_pemilik`, `id_kategori`, `nama_produk`, `stok`, `keterangan`, `harga`, `harga_promo`, `status`, `min_antar`) 
        VALUES('$id_produk','$id_pemilik','$id_kategori','$nama','$stok','$keterangan','$harga', '$harga_promo', '1', '$min_antar') ";

        // echo $query_produk; die();
        
        $this->db->query($query_produk);
    }

    function update_produk($id_produk,$id_kategori,$nama,$harga,$harga_promo,$stok,$keterangan, $min_antar){        

        $id_produk    = $this->db->escape_str($id_produk);
        $nama    = $this->db->escape_str($nama);
        $harga    = $this->db->escape_str($harga);
        $harga_promo    = $this->db->escape_str($harga_promo);
        $stok    = $this->db->escape_str($stok);
        $keterangan    = $this->db->escape_str($keterangan);

        $query_produk   = "update p_detail_produk set nama_produk = '$nama', id_kategori = '$id_kategori', harga = '$harga', harga_promo = '$harga_promo', stok = '$stok', keterangan = '$keterangan', min_antar = '$min_antar'
                            where id_produk = '$id_produk'";
        
        $this->db->query($query_produk);
    }

    function update_stok($id_produk,$stok){

        $id_produk    = $this->db->escape_str($id_produk);
        $stok    = $this->db->escape_str($stok);
        
        $this->db->query("update p_detail_produk set stok = '$stok' where id_produk = '$id_produk'");
    }


    function update_stocks($stocks){
        $when = ""; 
        $in = "where id_produk in (";
        $stmt = "update p_detail_produk set stok = case";
        $i = 0;
        foreach ($stocks as $stock) {
            $when = $when . " when id_produk = '" . $stock->id_produk . "' then " .  $stock->stok;
            if ($i == (count($stocks)-1)) {
                $in = $in . "'" . $stock->id_produk . "'";
            } else {    
                $in = $in . "'" . $stock->id_produk . "',";
            }
            $i++;
        }
        $when = $when . " end ";
        $in = $in . ")";

        $stmt = $stmt . $when . $in;
        $this->db->query($stmt);

        // echo $stmt; die();

        // $this->db->update_batch("p_detail_produk", $stocks, "id_produk'");
    }

    // function get_produk($id_pemilik){

    //     $id_pemilik    = $this->db->escape_str($id_pemilik);

    //     return $this->db->query("select a.*,b.nama_kategori
    //                         from p_detail_produk a, m_kategori_produk b
    //                         where
    //                         a.id_kategori = b.id_kategori
    //                         and
    //                         a.id_pemilik = '$id_pemilik' and a.status = 1
    //                         order by b.nama_kategori")->result();
    
    // }

    function delete_produk($id_produk){

        $id_produk    = $this->db->escape_str($id_produk);
        
        $this->db->query("delete from p_detail_produk where id_produk = '$id_produk'");
    }

    function get_order($id_pemilik){
        
        $id_pemilik    = $this->db->escape_str($id_pemilik);
        
        $query = $this->db->query("select a.*,b.nama,b.alamat,b.kontak,b.lat,b.lng,b.foto,b.versi, c.nama as nama_kota from konsumen_order a, m_konsumen b , m_kota c 
                    where  
                    a.id_konsumen=b.id_konsumen and a.id_pemilik='$id_pemilik' and c.id = b.id_kota group by a.id_order
                    order by a.status asc 
                    ");
        
        return $query->result();
    }


    function get_undone_order($id_pemilik){
        
        $id_pemilik    = $this->db->escape_str($id_pemilik);
        
        $query = $this->db->query("select a.*,b.nama,b.alamat,b.kontak,b.lat,b.lng,b.foto,b.versi, c.nama as nama_kota, e.nama_produk as nama_produk, e.harga, d.kuantitas as kuantitas, d.id_produk 
                    from konsumen_order a, m_konsumen b , m_kota c, detail_order d, p_detail_produk e 
                    where  
                    a.id_konsumen=b.id_konsumen and a.id_pemilik='$id_pemilik' and c.id = b.id_kota and d.id_order=a.id_order and e.id_produk=d.id_produk and a.status!=3
                    order by a.id_order asc 
                    ");
        
        return $query->result();
    }

    function buka_toko($id_pemilik,$status){

        $status    = $this->db->escape_str($status);
        $id_pemilik    = $this->db->escape_str($id_pemilik);
        
        $this->db->query("update m_partner set status_toko = '$status' where id_pemilik = '$id_pemilik'");
    }

    function update_nama_toko($id_pemilik,$nama){

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $nama    = $this->db->escape_str($nama);
        
        $this->db->query("update m_partner set nama = '$nama' where id_pemilik = '$id_pemilik'");
    }

    function update_jargon_toko($id_pemilik,$jargon){

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $jargon    = $this->db->escape_str($jargon);
        
        $this->db->query("update m_partner set jargon = '$jargon' where id_pemilik = '$id_pemilik'");
    }

    function update_radius_toko($id_pemilik,$id_radius){

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $id_radius    = $this->db->escape_str($id_radius);
        
        $this->db->query("update m_partner set id_radius = $id_radius where id_pemilik = '$id_pemilik'");
    }

    function update_jenis($id_pemilik,$id_jenis){

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $id_jenis    = $this->db->escape_str($id_jenis);
        
        $this->db->query("update m_partner set id_jenis = $id_jenis where id_pemilik = '$id_pemilik'");
    }


    function update_min_antar($id_pemilik,$min_antar){

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $min_antar    = $this->db->escape_str($min_antar);
        
        $this->db->query("update m_partner set min_antar = $min_antar where id_pemilik = '$id_pemilik'");
    }

    function update_kategori_usaha($id_pemilik,$json){

        $id_pemilik    = $this->db->escape_str($id_pemilik);

        $this->db->query("delete from p_detail_kategori where id_pemilik='$id_pemilik'");
        // echo $json; die();
        
        $arr = json_decode($json);

        for($i=0;$i<count($arr);$i++){

            $id_kategori_usaha  = $arr[$i];

            $this->db->query("insert into p_detail_kategori (id_pemilik,id_kategori) values ('$id_pemilik','$id_kategori_usaha')");
        }
    }

    function update_kategori_usaha_awal($id_pemilik,$json){

        $id_pemilik    = $this->db->escape_str($id_pemilik);

        $this->db->query("delete from p_detail_kategori where id_pemilik='$id_pemilik'");
        
        $arr = json_decode($json);

        for($i=0;$i<count($arr);$i++){

            $id_kategori_usaha  = $arr[$i]->id_kategori;
            $id_jenis  = $arr[$i]->id_jenis;

            $this->db->query("insert into p_detail_kategori (id_pemilik,id_kategori) values ('$id_pemilik','$id_kategori_usaha')");
            $this->db->query("update m_partner set id_jenis = '$id_jenis' where id_pemilik = '$id_pemilik'");
        }
    }

    function get_jenis_kategori_usaha(){
        
        $query = $this->db->query("select * from m_jenis_kategori_usaha");
        
        return $query->result();
    }

    function get_detail_order($id_order){

        $id_order    = $this->db->escape_str($id_order);

        $query = $this->db->query("select a.*,b.nama_produk, b.foto, b.keterangan as keterangan_produk, d.alamat_antar, d.ket_perkiraan_antar, d.lat_antar, d.lng_antar, d.status as order_status, nama_kategori, e.kontak
            from detail_order a,p_detail_produk b, m_kategori_produk c, konsumen_order d, m_konsumen e 
            where b.id_produk = a.id_produk and a.id_order='$id_order' and b.id_kategori=c.id_kategori and d.id_order=a.id_order and e.id_konsumen=d.id_konsumen");
        
        return $query->result();
    }

    function create_perkiraan_antar($pesan_perkiraan_antar){
        $this->db->query("insert into m_perkiraan_antar (nama, ket_konsumen, ket_pemilik, status) value ('$pesan_perkiraan_antar','$pesan_perkiraan_antar','$pesan_perkiraan_antar', 1)");
        return $this->db->insert_id();
    }

    function get_perkiraan_antar($id_perkiraan_antar){

        $id_perkiraan_antar    = $this->db->escape_str($id_perkiraan_antar);

        $query = $this->db->query("select * from m_perkiraan_antar where id='$id_perkiraan_antar' and status = 1");
        
        return $query->result();
    }

    function get_all_perkiraan_antar(){

        $query = $this->db->query("select * from m_perkiraan_antar where status = 1");
        
        return $query->result();
    }

    function get_kategori_usaha($id_pemilik){

        $query = $this->db->query("select a.*,b.nama as induk_kategori from m_kategori_usaha a,m_jenis_kategori_usaha b 
            where a.id_jenis=b.id_jenis and a.status=1 and b.status=1 and a.id_jenis = (select id_jenis from m_partner where id_pemilik = '$id_pemilik') order by a.nama_kategori");
        
        return $query->result();
    }

    function get_kategori_usaha_awal(){

        $query = $this->db->query("select a.*,b.nama as induk_kategori from m_kategori_usaha a,m_jenis_kategori_usaha b 
            where a.id_jenis=b.id_jenis and a.status=1 and b.status=1 order by a.nama_kategori");
        
        return $query->result();
    }

    function get_pemilik_kategori($id_pemilik){

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        
        $query = $this->db->query("select a.*,b.nama_kategori from p_detail_kategori a,m_kategori_usaha b where a.id_pemilik='$id_pemilik' and a.id_kategori = b.id_kategori");
        
        return $query->result();
    }
    
    function get_pemilik_lokasi($id_pemilik){

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        
        $query = $this->db->query("select * from p_detail_lokasi where id_pemilik='$id_pemilik'");
        
        return $query->result();
    }
    
    function get_pemilik_produk($id_pemilik){

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        
        $query = $this->db->query("select a.*,b.nama_kategori from p_detail_produk a left join m_kategori_produk b on a.id_kategori=b.id_kategori where a.id_pemilik='$id_pemilik' order by b.nama_kategori");
        
        // $sql = "SELECT a . * , b.nama_kategori,'1' as acuan
        // FROM p_detail_produk a, m_kategori_produk b
        // WHERE a.id_kategori = b.id_kategori
        // AND a.id_pemilik = '$id_pemilik'
        // AND b.id_pemilik = '$id_pemilik' AND a.status = '1' 
        // UNION
        // select '' as id_produk,'' as id_pemilik,a.id_kategori,'' as nama_produk,0 as stok,'' as keterangan, 
        // 0 as harga, '1' as status, '' as foto, '' as versi, a.nama_kategori, '2' as acuan 
        // from m_kategori_produk a where a.id_pemilik='$id_pemilik' and a.status = '1' 
        // and 
        // a.id_kategori not in 
        // (select id_kategori from p_detail_produk where id_pemilik='$id_pemilik' and status = '1') group by a.nama_kategori ORDER BY acuan, nama_kategori";
        // $query = $this->db->query($sql);
        return $query->result();
    }

    function get_pemilik($id_pemilik){

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        
        // $query = $this->db->query("select a.*,b.tipe as tipe_usaha,c.nama_kategori 
        //     from m_partner a,m_jenis_kategori_usaha b,m_kategori_usaha c 
        //     where a.id_jenis=b.id_jenis and c.id_kategori=a.id_kategori and
        //     a.id_pemilik='$id_pemilik'");
       
        $query = $this->db->query("select a.*,b.tipe as tipe_usaha, c.radius as radius  
            from m_partner a,m_jenis_kategori_usaha b,m_radius c
            where a.id_jenis=b.id_jenis and a.id_radius=c.id_radius and 
            a.id_pemilik='$id_pemilik'");

        return $query->result();
    }

    function get_preactive_pemilik($id_pemilik){

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        
        // $query = $this->db->query("select a.*,b.tipe as tipe_usaha,c.nama_kategori 
        //     from m_partner a,m_jenis_kategori_usaha b,m_kategori_usaha c 
        //     where a.id_jenis=b.id_jenis and c.id_kategori=a.id_kategori and
        //     a.id_pemilik='$id_pemilik'");
       
        $query = $this->db->query("select a.*,b.tipe as tipe_usaha, c.radius as radius  
            from m_partner a,m_jenis_kategori_usaha b,m_radius c
            where a.id_jenis=b.id_jenis and a.id_radius=c.id_radius and 
            a.id_pemilik='$id_pemilik'");

        return $query->row();
    }

    function get_preactive_produk($id_pemilik){

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        
        // $query = $this->db->query("select a.*,b.tipe as tipe_usaha,c.nama_kategori 
        //     from m_partner a,m_jenis_kategori_usaha b,m_kategori_usaha c 
        //     where a.id_jenis=b.id_jenis and c.id_kategori=a.id_kategori and
        //     a.id_pemilik='$id_pemilik'");
       
        $query = $this->db->query("select * 
            from p_detail_produk 
            where  id_pemilik='$id_pemilik'");

        return $query->row();
    }

    function proses_order($id_order,$id_perkiraan_antar, $pesan_perkiraan_antar){
        
        $id_order   = $this->db->escape_str($id_order);
        $id_perkiraan_antar   = $this->db->escape_str($id_perkiraan_antar);
        
        $this->db->query("update konsumen_order set status=2,tanggal_proses=now(),id_perkiraan_antar='$id_perkiraan_antar', ket_perkiraan_antar='$pesan_perkiraan_antar' 
            where id_order='$id_order'");    
    }

    function selesai_order($id_order){
        
        $id_order   = $this->db->escape_str($id_order);
        
        $this->db->query("update konsumen_order set status=4
            where id_order='$id_order'");
        $fee = "";
        $id_pemilik = "";
        
        $query = $this->db->query("select * from konsumen_order where id_order = '$id_order'")->first_row();
        
        $fee = $query->fee;
        $id_pemilik = $query->id_pemilik;
        
       

        $this->db->query("update m_partner set kredit = kredit - $fee where id_pemilik = '$id_pemilik'");        
    }

     function get_provinsi(){
        $query = $this->db->query("select * from m_provinsi where status=1 order by nama");
        return $query->result();
    }

    function get_kota($id_provinsi){
        $id_provinsi    = $this->db->escape_str($id_provinsi);
        
        $query = $this->db->query("select * from m_kota where id_provinsi = '$id_provinsi' and status=1 order by nama");
        return $query->result();
    }

    function daftar($id_pemilik,$kontak,$password,$username,$kode,$id_provinsi,$id_kota, $id_radius){

        $id_pemilik = $this->db->escape_str($id_pemilik);
        $kontak     = $this->db->escape_str($kontak);
        $password       = hash("sha512",$password);
        $username       = $this->db->escape_str($username);
        $kode           = $this->db->escape_str($kode);
        $id_radius      = $this->db->escape_str($id_radius);
        $id_provinsi    = $this->db->escape_str($id_provinsi);
        $id_kota        = $this->db->escape_str($id_kota);
        $lat = "-6.2293866";
        $lng = "106.6890876";
        
        $sql_stmt = "INSERT INTO `m_partner`(`id_pemilik`, `username`, `email`, `kontak`, `password`, `nama`, `jargon`, `alamat`, 
            `lat`, `lng`, `id_radius`, `foto`, `kode`, `tanggal_update`, `token`, `id_kategori`,id_provinsi,id_kota,level,id_jenis,tipe,kredit) 
            select '$id_pemilik','$username','','$kontak','$password','','','','$lat','$lng','$id_radius','','$kode',now(),'','0','$id_provinsi','$id_kota',1,0,1,default_kredit 
            from setting_umum where id = '1'";
		$this->db->query($sql_stmt);
		$affected_rows = $this->db->affected_rows();

        $this->db->query("delete from kode_sms where kode = '$kode'");
        
        return $affected_rows;
    }


    function daftar_pre($id_pemilik,$kontak,$password,$username,$kode,$id_provinsi,$id_kota, $id_radius){

        $id_pemilik = $this->db->escape_str($id_pemilik);
        $kontak     = $this->db->escape_str($kontak);
        $password       = hash("sha512",$password);
        $username       = $this->db->escape_str($username);
        $kode           = $this->db->escape_str($kode);
        $id_radius      = $this->db->escape_str($id_radius);
        $id_provinsi    = $this->db->escape_str($id_provinsi);
        $id_kota        = $this->db->escape_str($id_kota);
        $lat = "-6.2293866";
        $lng = "106.6890876";
        
        $sql_stmt = "INSERT INTO `m_partner`(`id_pemilik`, `username`, `email`, `kontak`, `password`, `nama`, `jargon`, `alamat`, 
            `lat`, `lng`, `id_radius`, `foto`, `kode`, `tanggal_update`, `token`, `id_kategori`, `status`,id_provinsi,id_kota,level,id_jenis,tipe,kredit) 
            select '$id_pemilik','$username','','$kontak','$password','','','','$lat','$lng','$id_radius','','$kode',now(),'','0', '0', '$id_provinsi','$id_kota',1,0,1,default_kredit 
            from setting_umum where id = '1'";
        $this->db->query($sql_stmt);
        $affected_rows = $this->db->affected_rows();

        $this->db->query("delete from kode_sms where kode = '$kode'");
        
        return $affected_rows;
    }

    function insert_pemilik($id_pemilik,$id_jenis, $kontak,$password,$username,$nama, $jargon, $alamat, $kode, $id_provinsi, $id_kota, $id_kategori, $id_radius, $lat, $lng) {

        $id_pemilik = $this->db->escape_str($id_pemilik);
        $kontak     = $this->db->escape_str($kontak);
        $password       = hash("sha512",$password);
        $username       = $this->db->escape_str($username);
        $jargon       = $this->db->escape_str($jargon);
        $kode           = $this->db->escape_str($kode);
        $id_radius      = $this->db->escape_str($id_radius);
        $id_provinsi    = $this->db->escape_str($id_provinsi);
        $id_kota        = $this->db->escape_str($id_kota);
        $alamat       = $this->db->escape_str($alamat);
        
        $sql_stmt = "INSERT INTO `m_partner`(`id_pemilik`, `username`, `email`, `kontak`, `password`, `nama`, `jargon`, `alamat`, 
            `lat`, `lng`, `id_radius`, `foto`, `kode`, `tanggal_update`, `token`, `id_kategori`, `status`,id_provinsi,id_kota,level,`id_jenis`,tipe,kredit) 
            select '$id_pemilik','$username','','$kontak','$password','$nama','$jargon','$alamat','$lat','$lng','$id_radius','','$kode',now(),'', '0', '0','$id_provinsi','$id_kota',1,$id_jenis,1,default_kredit 
            from setting_umum where id = '1'";

        $this->db->query($sql_stmt);
        
        return $this->db->affected_rows() == 1;
    }

    function isi_profil_awal($id_pemilik,$nama,$alamat,$jargon, $id_radius){

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $nama    = $this->db->escape_str($nama);
        $alamat    = $this->db->escape_str($alamat);
        $jargon    = $this->db->escape_str($jargon);
        
        $this->db->query("update m_partner set nama = '$nama', alamat = '$alamat',jargon = '$jargon', id_radius='$id_radius' where id_pemilik = '$id_pemilik'");
    }

    function isi_lokasi_awal($id_pemilik,$lat,$lng,$nama_lokasi){

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $lat    = $this->db->escape_str($lat);
        $lng    = $this->db->escape_str($lng);
        $nama_lokasi    = $this->db->escape_str($nama_lokasi);
        
        $this->db->query("update m_partner set lat = '$lat',lng = '$lng' where id_pemilik = '$id_pemilik'");

        $lokasi = $this->db->query("select * from p_detail_lokasi where id_pemilik='$id_pemilik' LIMIT 1")->row();

        if (empty($lokasi)) {
            $id_lokasi = date("YmdHis").$id_pemilik;
            $this->db->query("insert into p_detail_lokasi (id_lokasi,id_pemilik,nama,lat,lng,jenis_lokasi) values ('$id_lokasi','$id_pemilik','$nama_lokasi','$lat','$lng','1')");
        } else {
            $this->db->query("update p_detail_lokasi set id_pemilik='$id_pemilik', nama='$nama_lokasi', lat='$lat',lng='$lng', jenis_lokasi='1' where id_pemilik='$id_pemilik'");
        }
    }

    function get_id_pemilik_by_no_telp($no_telp){
        
        $no_telp   = $this->db->escape_str($no_telp);

        $query = $this->db->query("select * from m_partner where kontak = '$no_telp'");
        return $query->first_row();
    }

    function new_password($id_pemilik,$password){

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $password   = hash("sha512",$password);
       
        $this->db->query("update m_partner set password = '$password' where id_pemilik = '$id_pemilik'");
    }

    function check_password($id_pemilik,$password){
        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $password   = hash("sha512",$password);

        return $this->db->query("select * from m_partner where id_pemilik = '$id_pemilik' and password = '$password'")->row();
    }

    function hapus_akun($id_pemilik){
        
        $id_pemilik   = $this->db->escape_str($id_pemilik);

        $this->db->query("DELETE FROM `m_partner` WHERE id_pemilik = '$id_pemilik'");
    }

    function get_penjualan_harian($id_pemilik,$tanggal){
        $query = $this->db->query("select a.id_order,a.tanggal_order,a.id_pemilik,b.nama_kategori,c.id_produk,c.nama_produk,c.keterangan,
                    sum(d.kuantitas) as total_kuantitas,sum(d.subtotal) as subtotal
                    from
                    konsumen_order a,m_kategori_produk b,p_detail_produk c,detail_order d
                    where
                    a.id_order = d.id_order
                    and
                    b.id_kategori = c.id_kategori
                    and
                    c.id_produk = d.id_produk
                    and
                    DATE(a.tanggal_order) = '$tanggal'
                    and
                    a.id_pemilik = '$id_pemilik'
                    and
                    a.status = 2
                    group by c.id_produk
                    order by b.nama_kategori");
        return $query->result();
    }
    
    function get_penjualan_bulanan($id_pemilik,$bulan,$tahun){
        $query = $this->db->query("select a.id_order,a.tanggal_order,a.id_pemilik,b.nama_kategori,c.id_produk,c.nama_produk,c.keterangan,
                    sum(d.kuantitas) as total_kuantitas,sum(d.subtotal) as subtotal
                    from
                    konsumen_order a,m_kategori_produk b,p_detail_produk c,detail_order d
                    where
                    a.id_order = d.id_order
                    and
                    b.id_kategori = c.id_kategori
                    and
                    c.id_produk = d.id_produk
                    and
                    MONTH(a.tanggal_order) = '$bulan' and YEAR(a.tanggal_order) = '$tahun'
                    and
                    a.id_pemilik = '$id_pemilik'
                    and
                    a.status = 2
                    group by c.id_produk
                    order by b.nama_kategori");
        return $query->result();
    }
    
    function get_penjualan_tahunan($id_pemilik,$tahun){
        $query = $this->db->query("select a.id_order,a.tanggal_order,a.id_pemilik,b.nama_kategori,c.id_produk,c.nama_produk,c.keterangan,
                    sum(d.kuantitas) as total_kuantitas,sum(d.subtotal) as subtotal
                    from
                    konsumen_order a,m_kategori_produk b,p_detail_produk c,detail_order d
                    where
                    a.id_order = d.id_order
                    and
                    b.id_kategori = c.id_kategori
                    and
                    c.id_produk = d.id_produk
                    and
                    YEAR(a.tanggal_order) = '$tahun'
                    and
                    a.id_pemilik = '$id_pemilik'
                    and
                    a.status = 2
                    group by c.id_produk
                    order by b.nama_kategori");

        
        
        return $query->result();
    }

    function set_foto($id_pemilik,$foto){
        
        $id_pemilik   = $this->db->escape_str($id_pemilik);
        $foto   = $this->db->escape_str($foto);

        $this->db->query("update m_partner set foto = '$foto', versi = versi+1 where id_partner = '$id_pemilik'");
    }

    function set_foto_produk($id_produk,$foto){
        
        $id_produk   = $this->db->escape_str($id_produk);
        $foto   = $this->db->escape_str($foto);

        $this->db->query("update p_detail_produk set foto = '$foto', versi = versi+1 where id_produk = '$id_produk'");
    }

    function set_foto_avatar($id_pemilik,$foto){
        
        $id_pemilik   = $this->db->escape_str($id_pemilik);
        $foto   = $this->db->escape_str($foto);

        $this->db->query("update m_partner set avatar = '$foto', versi_avatar = versi_avatar+1 where id_pemilik = '$id_pemilik'");
    }

    function get_foto($id_pemilik){
        
        $id_pemilik   = $this->db->escape_str($id_pemilik);

        $query = $this->db->query("select foto,versi from m_partner where id_pemilik = '$id_pemilik'");

        return $query->first_row();
    }

    function get_foto_avatar($id_pemilik){
        
        $id_pemilik   = $this->db->escape_str($id_pemilik);

        $query = $this->db->query("select avatar,versi_avatar from m_partner where id_pemilik = '$id_pemilik'");

        return $query->first_row();
    }

    function get_foto_produk($id_produk){
        
        $id_pemilik   = $this->db->escape_str($id_produk);

        $query = $this->db->query("select foto,versi from p_detail_produk where id_produk = '$id_produk'");

        return $query->first_row();
    }

    function notif_get_pemilik($id_pemilik){
        
        $query = $this->db->query("select * from m_partner where id_pemilik = '$id_pemilik'");
        return $query->result();
    }

    function notif_get_konsumen($id_order){
        
        $query = $this->db->query("select a.* 
            from m_konsumen a, konsumen_order b
            where a.id_konsumen = b.id_konsumen and b.id_order = '$id_order'");
        return $query->result();
    }

    function notif_get_order($id_order){
        $query = $this->db->query("select ko.id_order, ko.total, ko.status, ko.id_perkiraan_antar 
            from konsumen_order ko
            where ko.id_order = '$id_order'");
        return $query->result();
    }

    function update_token($id_pemilik,$token){
        $id_pemilik   = $this->db->escape_str($id_pemilik);
        $token   = $this->db->escape_str($token);
        
        $query = $this->db->query("update m_partner set token='$token' where id_pemilik='$id_pemilik'");
    }

    function get_order_dari_notif($id_order){
        
        $id_order    = $this->db->escape_str($id_order);
        
        $query = $this->db->query("select a.*,b.nama,b.alamat,b.kontak,b.lat,b.lng,b.foto,b.versi, c.nama as nama_kota 
                    from konsumen_order a, m_konsumen b , m_kota c 
                    where  
                    a.id_konsumen=b.id_konsumen and a.id_order='$id_order' and c.id = b.id_kota 
                    order by a.status asc 
                    ");
        
        return $query->result();
    }

    function get_konsumen_order($id_order){
        
        $id_order   = $this->db->escape_str($id_order);

        $query = $this->db->query("select * from konsumen_order where id_order='$id_order' LIMIT 1")->row();

        // echo $this->db->last_query(); die();

        return $query;
    }


    function update_provinsi_kota($id_konsumen,$id_provinsi,$id_kota){

        $id_konsumen    = $this->db->escape_str($id_konsumen);
        $id_provinsi    = $this->db->escape_str($id_provinsi);
        $id_kota    = $this->db->escape_str($id_kota);
       
        $this->db->query("update m_partner set id_provinsi = '$id_provinsi' AND id_kota = '$id_kota' where id_pemilik = '$id_konsumen'");
    }

    
    function update_kategori_produk($id_kategori,$nama_kategori){        

        $id_kategori    = $this->db->escape_str($id_kategori);
        $nama_kategori    = $this->db->escape_str($nama_kategori);

        $query   = "update m_kategori_produk set nama_kategori = '$nama_kategori' 
                            where id_kategori = '$id_kategori'";
        
        $this->db->query($query);
    }

    function delete_kategori_produk($id_kategori){

        $id_kategori    = $this->db->escape_str($id_kategori);
        
        $this->db->query("update m_kategori_produk set status = '2' where id_kategori = '$id_kategori'");        
        $this->db->query("update p_detail_produk set status = '2' where id_kategori = '$id_kategori'");
    }

}