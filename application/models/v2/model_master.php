<?php

class Model_master extends CI_Model{
    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function get_level() {
        $query = $this->db->query("SELECT * FROM m_level where status='1' order by id desc");
        return $query;
    }

    public function get_pemilik_usaha($num) {
        $query = $this->db->query("select * from m_pemilik_usaha where status='1' order by id_pemilik desc LIMIT $num");
        return $query;
    }

    public function get_all_pemilik_usaha() {
        $query = $this->db->query("select * from m_pemilik_usaha where status='1' order by id_pemilik desc");
        return $query;
    }

    //  public function get_data() {
    //     $sql = "select jumlah from jumlah_data where id='1' ";
        
    //     $query =  $this->db->query($sql);
    //     return $query->first_row()->jumlah;

    // }

    public function get_data($id_partner) {
        $sql = "select * from m_pemilik_usaha where id_partner='$id_partner' ";
        
        $query =  $this->db->query($sql);
        return $query;

    }

    public function get_preactive_pemilik() {
        $query = $this->db->query("select mpu.*, mku.nama_kategori as nama_kategori, mr.radius as radius from m_pemilik_usaha mpu join m_radius mr ON mr.id_radius=mpu.id_radius join p_detail_kategori pdk ON pdk.id_pemilik=mpu.id_pemilik join m_kategori_usaha mku ON mku.id_kategori=pdk.id_kategori where mpu.status='0'order by mpu.id_pemilik desc");
        return $query;
    }

    public function get_data_order($requestData, $id_partner) {
        
        $sql = "SELECT a.* FROM m_pemilik_usaha a where a.id_partner='$id_partner'";
        
        $sql.=" order by a.nama LIMIT ".$requestData['start']." ,".$requestData['length']."   ";

        // echo $sql; die();
        
        $query = $this->db->query($sql);
        return $query;
    }

    public function get_data_filter($requestData, $id_partner) {
        $sql = "";

        $keyword = " like '%" . $requestData['search']['value'] . "%'";
            $sql = "SELECT a.* FROM m_pemilik_usaha a where a.id_partner='$id_partner' AND (a.id_pemilik ". $keyword . " OR a.nama ". $keyword . " OR a.username " . $keyword . " OR a.kontak " . $keyword . " OR a.kontak_sms " . $keyword .  " OR a.kontak_sms2 " . $keyword .")";
        
        $sql.=" order by a.nama";
        $query = $this->db->query($sql);
        return $query;
    }
    
    //////RADIUS/////
    public function get_radius() {
        $query = $this->db->query("SELECT * FROM m_level where status='1' order by nama asc");
        return $query;
    }
    public function insert_radius($nama,$radius,$harga,$id) {
        $namae = $this->db->escape_str($nama);
        $radiuse = $this->db->escape_str($radius);
        $hargae = $this->db->escape_str($harga);
        if ($id == ""){
            $this->db->query("insert into m_level(nama, radius, harga) values('$namae','$radiuse','$hargae')");
        }  else {
            $this->db->query("update m_level set nama='$namae', radius='$radiuse', harga='$hargae' where id='$id'");
        }
    }
    public function delete_radius($id){
        $this->db->query("update m_level set status='0' where id='$id'");
    }
    
    //////Jenis KATEGORI////
    public function get_kategori() {
        $query = $this->db->query("SELECT a.*, b.nama as 'nama_level',b.radius as 'nama_radius' FROM m_jenis_kategori_usaha a, m_level b where a.id_level=b.id and a.status='1' and b.status='1' ORDER BY a.nama ASC");
        return $query;
    }
    public function get_max_id() {
        $query = $this->db->query("select max(id_jenis) as 'jumlah' from m_jenis_kategori_usaha");
        $row= $query->first_row();
        return $row->jumlah;
    }
    public function get_foto_biru($id) {
        $query = $this->db->query("select icon_biru from m_jenis_kategori_usaha where id_jenis='$id'");
        return $query->row();
    }
    public function get_foto_hitam($id) {
        $query = $this->db->query("select icon_hitam from m_jenis_kategori_usaha where id_jenis='$id'");
        return $query->row();
    }
    public function insert_kategori($nama,$tipe,$level,$id,$ibiru,$ihitam){
        $namae = $this->db->escape_str($nama);
        $tipee = $this->db->escape_str($tipe);
        $levele = $this->db->escape_str($level);
        if ($id == ""){
            $this->db->query("insert into m_jenis_kategori_usaha(nama, tipe, id_level, icon_biru, icon_hitam) values('$namae','$tipee','$levele','$ibiru','$ihitam')");
            $max_id  = $this->get_max_id();
            $ibiru2 = explode(".", $ibiru);
            $ibiru3 = "icon_biru".$max_id.'.'.$ibiru2[1];
            
            $ihitam2 = explode(".", $ihitam);
            $ihitam3 = "icon_hitam".$max_id.'.'.$ihitam2[1];
            $this->db->query("update m_jenis_kategori_usaha set icon_biru='$ibiru3', icon_hitam='$ihitam3' where id_jenis='$max_id' ");
        }  else {
            $this->db->query("update m_jenis_kategori_usaha set nama='$namae', tipe='$tipee', id_level='$levele', icon_biru='$ibiru', versi_icon_biru=versi_icon_biru+1, icon_hitam='$ihitam', versi_icon_hitam=versi_icon_hitam+1 where id_jenis='$id'");
        }
    }
    public function delete_kategori($id){
        $this->db->query("update m_jenis_kategori_usaha set status='0' where id_jenis='$id'");
    }
    
    ///Kategori Ush///
    /*public function m_kategori() {
        $query = $this->db->query("SELECT a.*,b.nama as 'jenis_kategori' FROM m_kategori_usaha a, m_jenis_kategori_usaha b WHERE a.id_jenis=b.id_jenis and a.status='1' order by a.nama_kategori asc");
        return $query;
    }*/
    
    public function m_kategori() {
        $query = $this->db->query("SELECT a.*,b.nama as 'jenis_kategori',c.radius FROM m_kategori_usaha a, m_jenis_kategori_usaha b,m_level c WHERE a.id_jenis=b.id_jenis and b.id_level = c.id and a.status='1' order by a.nama_kategori asc");
        return $query;
    }
    
    public function get_jenisk() {
        $query = $this->db->query("SELECT * FROM m_jenis_kategori_usaha order by id_jenis desc");
        return $query;
    }
    public function insert_kategoriushm($nama,$jenisk,$id){
        $namae = $this->db->escape_str($nama);
        $jenis = $this->db->escape_str($jenisk);
        if ($id == ""){
            $this->db->query("insert into m_kategori_usaha(nama_kategori, id_jenis) values('$namae','$jenis')");
        }  else {
            $this->db->query("update m_kategori_usaha set nama_kategori='$namae', id_jenis='$jenis' where id_kategori='$id'");
        }
    }
    public function delete_kategoriushm($id){
        $this->db->query("update m_kategori_usaha set status='0' where id_kategori='$id'");
    }
    
    //////FEE////
    public function get_fee() {
        $query = $this->db->query("SELECT * FROM m_fee where status='1' order by batas_bawah asc");
        return $query;
    }
    public function insert_fee($batasb,$batasa,$nominal,$id){
        $batasbe = $this->db->escape_str($batasb);
        $batasae = $this->db->escape_str($batasa);
        if ($id == ""){
            $this->db->query("insert into m_fee(batas_bawah, batas_atas, nominal) values('$batasbe','$batasae','$nominal')");
        }  else {
            $this->db->query("update m_fee set batas_bawah='$batasbe', batas_atas='$batasae', nominal='$nominal' where id='$id'");
        }
    }
    public function delete_fee($id){
        $this->db->query("update m_fee set status='0' where id='$id'");
    }
    
    //////PROVINSI////
     public function m_provinsi() {
        $query = $this->db->query("SELECT * FROM m_provinsi WHERE status='1' order by nama asc");
        return $query;
    }
    public function insert_provinsi($nama,$id){
        $namae = $this->db->escape_str($nama);
        if ($id == ""){
            $this->db->query("insert into m_provinsi(nama) values('$namae')");
        }  else {
            $this->db->query("update m_provinsi set nama='$namae' where id='$id'");
        }
    }
    public function delete_provinsi($id){
        $this->db->query("update m_provinsi set status='0' where id='$id'");
    }
    
    ///KOTA///
    public function m_kota() {
        $query = $this->db->query("SELECT a.*, b.nama as 'nama_provinsi' FROM m_kota a, m_provinsi b WHERE a.id_provinsi=b.id and a.status='1' order by a.nama asc");
        return $query;
    }
    public function insert_kota($nama,$provinsi,$id){
        $namae = $this->db->escape_str($nama);
        $provinsie = $this->db->escape_str($provinsi);
        if ($id == ""){
            $this->db->query("insert into m_kota(id_provinsi, nama) values('$provinsie','$namae')");
        }  else {
            $this->db->query("update m_kota set id_provinsi='$provinsie', nama='$namae' where id='$id'");
        }
    }
    public function delete_kota($id){
        $this->db->query("update m_kota set status='0' where id='$id'");
    }
    
}