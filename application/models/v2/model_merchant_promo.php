<?php

class Model_merchant_promo extends CI_Model{
    function __construct() {
        parent::__construct();
        $this->load->database();
    }


    public function list_active_promo($id_partner) {
        $query = $this->db->query("select p.* from merchant_promo p where id_partner='$id_partner' order by p.id desc");
        return $query;
    } 

    public function get($id) {
        $query = $this->db->query("select p.* from merchant_promo p where id='$id' order by p.id desc LIMIT 1");
        return $query->row();
    }

    public function update($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('merchant_promo', $data); 
    }

    public function get_last_banner() {
        $query = $this->db->query("select p.* from merchant_promo_banner p order by p.id desc LIMIT 1");
        return $query->row();
    }

    public function get_promo($id_partner) {


        $stmt = "SELECT * FROM `merchant_promo` where CURDATE() BETWEEN start_date AND end_date ORDER BY id DESC";
        $query = $this->db->query($stmt);
        // echo $stmt; die();
        return $query;
    } 


    public function get_promo_by_pemilik($id_pemilik) {
        $stmt = "SELECT * FROM `merchant_promo` where id_pemilik='$id_pemilik' AND CURDATE() BETWEEN start_date AND end_date ORDER BY start_date";
        // echo $stmt; die();
        $query = $this->db->query($stmt);
        return $query;
    } 

    public function get_inactive_promo_by_pemilik($id_pemilik) {
        $query = $this->db->query("SELECT * FROM `merchant_promo` where id_pemilik='$id_pemilik' AND CURDATE() < start_date OR CURDATE() > end_date");
        return $query;
    } 

    public function get_active_banners($id_partner) {
        $query = $this->db->query("SELECT * FROM `merchant_promo_banner` mpb LEFT JOIN `merchant_promo` mp ON mpb.id_merchant_promo=mp.id where CURDATE() BETWEEN mp.start_date AND mp.end_date and mp.id_partner='$id_partner' ORDER BY mp.start_date");
        return $query;
    } 

    public function get_promo_diskon($id_partner) {
        $query = $this->db->query("SELECT * FROM `merchant_promo` where CURDATE() BETWEEN start_date AND end_date and id_partner='$id_partner' ORDER BY start_date LIMIT 1");
        return $query;
    }  

    public function get_free_item($id_merchant_promo, $id_pemilik, $page=0) {
        $pagingParam = "";
            
        if (intval($page >=1 )) { // kalau paging dimulai dari 1, enable pagint. Jika tidak, tampilkan seluruhnya
        $offset = ($page-1) * 20; 
            $pagingParam =  "LIMIT 20 OFFSET " . $offset;
        }

        $stmt = "SELECT mpp.id, CONCAT(" . $id_pemilik . ", '_', mpp.id_barcode) as id_produk, pdp.nama_produk as nama_produk, pdp.foto as foto, mpp.qty, pdp.stok as stok, pdp.harga as harga, pdp.harga_promo as harga_promo, pdp.status, pdp.id_kategori as id_kategori_produk, CONCAT(" . $id_pemilik . ", '_', mpp.id_barcode2) as id_produk2, pdp2.nama_produk as nama_produk2, pdp2.foto as foto2, mpp.qty2, pdp2.stok as stok2, pdp2.harga as harga2, pdp2.harga_promo as harga_promo2, pdp2.status as status2, pdp2.id_kategori as id_kategori_produk2, mpp.kelipatan FROM `m_promo_produk` mpp LEFT JOIN p_detail_produk pdp ON CONCAT('" . $id_pemilik . "', '_', mpp.id_barcode)=pdp.id_produk LEFT JOIN p_detail_produk pdp2 ON CONCAT('" . $id_pemilik . "', '_', mpp.id_barcode2)=pdp2.id_produk where mpp.id_merchant_promo='$id_merchant_promo' AND pdp.nama_produk <> '' AND pdp.harga > 0 AND pdp2.nama_produk <> '' AND pdp2.harga > 0 order by id_produk " . $pagingParam;
        // echo $stmt; die();
        $query = $this->db->query($stmt);
        return $query;
    }  


    public function get_banners($id_merchant_promo) {
        $query = $this->db->query("SELECT * FROM `merchant_promo_banner` where id_merchant_promo='$id_merchant_promo'");
        return $query;
    } 


    public function add_banner($id_merchant_promo) {
        $query = $this->db->query("INSERT INTO merchant_promo_banner (id_merchant_promo, image) VALUE ('$id_merchant_promo', '')");
        return $this->db->insert_id();
    } 

    public function set_banner($id, $image) {
        $stmt = "UPDATE merchant_promo_banner SET image='$image' WHERE id=".$id;
        // echo $stmt; die();
        $query = $this->db->query($stmt);
        return $query;
    } 


    public function delete_banner($id) {
        $query = $this->db->query("DELETE FROM merchant_promo_banner WHERE id=".$id);
        return $query;
    }  


    public function delete($id) {
        $query = $this->db->query("DELETE FROM merchant_promo WHERE id=".$id);
        return $query;
    }  

    public function delete_produk_by_promo_id($promo_id) {
        $query = $this->db->query("DELETE FROM m_promo_produk WHERE id_merchant_promo=".$promo_id);
        return $query;
    }  

    public function delete_old_promo() {


        $stmt = "DELETE ppd FROM `produk_promo_diskon` ppd LEFT JOIN  merchant_promo mp on mp.id=ppd.id_merchant_promo where CURDATE() > mp.end_date";
        $query = $this->db->query($stmt);

        $stmt = "DELETE FROM `merchant_promo` where CURDATE() > end_date";
        $query = $this->db->query($stmt);

        // echo $stmt; die();
        return $query;
    } 


    public function get_produk_promo($id_merchant_promo) {
        $query = $this->db->query("select p.* from m_promo_produk p where p.id_merchant_promo='$id_merchant_promo' order by p.id desc");
        return $query;
    } 

    public function get_produk_diskon($id_merchant_promo) {
        $stmt = "select ppd.*, p.harga_promo, '' as ket_promo from produk_promo_diskon ppd left join p_detail_produk p on p.id_produk=ppd.id_produk left join merchant_promo mp on mp.id=ppd.id_merchant_promo where ppd.id_merchant_promo=$id_merchant_promo order by ppd.id_produk desc";
        // echo $stmt; die();
        $query = $this->db->query($stmt);
        return $query;
    }  

    public function get_promo_by_id_promo_and_pemilik($id_promo, $id_pemilik) {
        $query = $this->db->query("select * from merchant_promo WHERE id_promo='$id_promo' AND id_pemilik='$id_pemilik' LIMIT 1");
        return $query->first_row();
    }


    public function create_promo($id_promo, $nama, $id_partner, $id_pemilik, $tipe, $start_date, $end_date) {
        $query = $this->db->query("insert into merchant_promo (id_promo, nama, id_partner, id_pemilik, tipe, start_date, end_date) VALUE ('$id_promo', '$nama', '$id_partner', '$id_pemilik', $tipe, '$start_date', '$end_date')");
        return $this->db->insert_id();
    }

    function insert_produk_promo_batch($id_pemilik, $headers, $rows){    

        $query_produk   = "INSERT INTO `m_promo_produk` (";
        $query_produk = $query_produk . "`id_merchant_promo`, ";
        $id_produk_pos = 0;
        $k = 0;
        foreach ($headers as $header) {
            if ($header == 'id_produk') {
                $id_produk_pos = $k;
            }

            if ($header != '' && $header != null) {
	            $query_produk = $query_produk . "`".$header . "`, ";
	        }
            $k++;
        }
        $query_produk = $query_produk . ") VALUES ";
        $query_produk = str_replace(", ) VALUES ", ") VALUES ", $query_produk);

        // echo $k; die();

        $i = 0;
        $missedI = 0;
        foreach ($rows as $produk) {
            if ($missedI >= 10) break;
            // echo count($produk);
            if (count($produk) ==  $k) {
                $j=0;
                $toContinue = false;

                $paramBuilder = "";
                foreach ($produk as $column) {
                    if ($column == '') {
                        $missedI++;
                        $toContinue =  true;
                        break;
                    } else {
                        $toContinue = false;
                    }

                    // $column = $produk[$j];
                    // if ($j == $id_produk_pos) {
                    //     $column = $id_pemilik . "_" . $column;
                    // }
    	            // if ($column != '' && $column != null) {
    	                $paramBuilder = $paramBuilder . "'".$column . "', ";
    	            // }
                    $j++;
                }
                if ($toContinue) continue;

                $query_produk = $query_produk . "(";
                $query_produk = $query_produk . "'".$id_pemilik . "', ";
                $query_produk = $query_produk . $paramBuilder;

                $query_produk = $query_produk . "), HIHI";
                $query_produk = str_replace(", ), HIHI", ")", $query_produk);
            }

            $i++;
        }
        $query_produk = str_replace(")(", "),(", $query_produk);

        // echo $query_produk; die();
        
        $this->db->query($query_produk);
    }

    function set_harga_promo_batch($id_pemilik, $headers, $rows, $id_merchant_promo) { 
    	$id_produk_pos = 0;
    	$harga_promo_pos = 0;
        $k = 0;
        foreach ($headers as $header) {
            if ($header == 'id_barcode') {
                $id_produk_pos = $k;
            }
            if ($header == 'harga_promo') {
                $harga_promo_pos = $k;
            }
            $k++;
        }
 		$i = 0;

        $insert = "INSERT INTO p_detail_produk (id_produk, id_pemilik, nama_produk, id_kategori, keterangan, harga, harga_promo, active) VALUES ";
        $update = "ON DUPLICATE KEY UPDATE harga_promo=VALUES(harga_promo), active=1";

        foreach ($rows as $row) {
            if ($row[$harga_promo_pos] != '' && $row[$id_produk_pos]) {
                $insert = $insert . "('" . $id_pemilik . "_" . $row[$id_produk_pos] . "', '" . $id_pemilik . "', '', 1, '', 0, " . $row[$harga_promo_pos] . ", 1), ";
            }
            $i++;
        }
        $stmt = $insert . $update;
        $stmt = str_replace("), ON DUPLICATE KEY UPDATE", ") ON DUPLICATE KEY UPDATE", $stmt);

        // echo $stmt; die();
        $this->db->query($stmt);
        
    }

    function insert_produk_promo_diskon_batch($headers, $rows, $id_merchant_promo) { 
        $id_produk_pos = 0;
        $harga_promo_pos = 0;
        $k = 0;
        foreach ($headers as $header) {
            if ($header == 'id_barcode') {
                $id_produk_pos = $k;
            }
            if ($header == 'harga_promo') {
                $harga_promo_pos = $k;
            }
            $k++;
        }
        $i = 0;

        $update2 = "XXX";

        $insert2 = "INSERT INTO produk_promo_diskon (id_produk, id_merchant_promo) VALUES ";
        foreach ($rows as $row) {
            if ($row[$harga_promo_pos] != '' && $row[$id_produk_pos]) {
                $insert2 = $insert2 . "('" . $row[$id_produk_pos] . "', " . $id_merchant_promo . "), ";
            }
            $i++;
        }

        $stmt2 = $insert2 . $update2;
        $stmt2 = str_replace("), XXX", ")", $stmt2);
        // echo $stmt; die();
        $this->db->query($stmt2);
        
    }



}

?>