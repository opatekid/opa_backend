<?php

class Model_promo_old extends CI_Model{
    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function list_active_promo() {
        $query = $this->db->query("select p.*, mp.nama as nama_provinsi, mk.nama as nama_kota from promo p left join m_provinsi mp on p.id_provinsi=mp.id left join m_kota mk on p.id_kota=mk.id where p.status=1 order by p.id desc");
        return $query;
    }  

    public function show_promo($id) {
        $query = $this->db->query("select pc.*, p.nama as nama_promo, p.jenis as jenis, mpu.id_pemilik, mpu.nama as nama_pemilik from promo_code pc left join promo p on pc.promo_id=p.id left join m_pemilik_usaha mpu on pc.id_pemilik=mpu.id_pemilik WHERE p.id=$id AND mpu.nama IS NOT NULL AND mpu.nama != '' AND mpu.id_pemilik IS NOT NULL AND mpu.id_pemilik != ''");
        return $query;
    }   

    public function get_promo_detail($id) {
        $query = $this->db->query("select pc.*, p.nama as nama_promo, p.jenis as jenis from promo_code pc left join promo p on pc.promo_id=p.id WHERE p.id=$id");
        return $query;
    }  

    public function show_promo_with_filter($id, $filter_select) {
        $stmt = "select pc.*, p.nama as nama_promo, p.jenis as jenis, mpu.id_pemilik, mpu.nama as nama_pemilik from promo_code pc left join promo p on pc.promo_id=p.id left join m_pemilik_usaha mpu on pc.id_pemilik=mpu.id_pemilik WHERE p.id=$id AND mpu.nama IS NOT NULL AND mpu.nama != '' AND mpu.id_pemilik IS NOT NULL AND mpu.id_pemilik != ''";
        if ($filter_select == "1") {
            $stmt = $stmt . " AND pc.used=1";
        }
        $query = $this->db->query($stmt);
        return $query;
    }   


    public function show_promo_for_konsumen($id) {
        $stmt = "select pc.*, p.nama as nama_promo, p.jenis as jenis from promo_code pc left join promo p on pc.promo_id=p.id WHERE p.id=$id"; 

        $query = $this->db->query($stmt);
        return $query;
    }       

    // public function get_promo($id_pemilik) {
    //     $query = $this->db->query("select pc.*, p.nama as nama_promo, p.description as description, p.valid_duration as valid_duration, p.max_konsumen as max_konsumen, p.message as message, p.jenis as jenis, mpu.nama as nama_pemilik from promo_code pc left join promo p on pc.promo_id=p.id left join m_pemilik_usaha mpu on pc.id_pemilik=mpu.id_pemilik WHERE pc.id_pemilik=$id_pemilik AND p.status=1 AND pc.used=0 LIMIT 1");
    //     return $query;
    // }  

     public function get_promo($id_pemilik) {
        $stmt = "select pc.*, p.nama as nama_promo, p.description as description, p.valid_duration as valid_duration, p.max_konsumen as max_konsumen, p.message as message, p.jenis as jenis, mpu.nama as nama_pemilik from promo_code pc left join promo p on pc.promo_id=p.id left join m_pemilik_usaha mpu on pc.id_pemilik=mpu.id_pemilik WHERE pc.id_pemilik=$id_pemilik AND p.status=1 AND pc.used=1 LIMIT 1";
        // echo $stmt; die();
        $query = $this->db->query($stmt);
        return $query;
    } 

    public function get_promo_code($id_pemilik) {
        $id_pemilik = $this->db->escape_str($id_pemilik);
        $stmt = "select * from promo_code where id_pemilik='$id_pemilik' AND used=1 ORDER BY id DESC";
        // echo $stmt; die();
        $query = $this->db->query($stmt);
        return $query;
    }  

    public function get_promo_for_konsumen($id_pemilik) {
        $query = $this->db->query("select pc.*, p.nama as nama_promo, p.description as description, p.valid_duration as valid_duration, p.max_konsumen as max_konsumen, p.message as message, p.jenis as jenis, mpu.nama as nama_pemilik from promo_code pc left join promo p on pc.promo_id=p.id left join m_pemilik_usaha mpu on pc.id_pemilik=mpu.id_pemilik WHERE pc.id_pemilik=$id_pemilik AND p.status=1 LIMIT 1");
        return $query;
    }  

    public function check_promo_membership($id_konsumen) {
        $query = $this->db->query("select p.* from promo_code_konsumen pck left join promo_code pc on pck.promo_code_id=pc.id left join promo p on pc.promo_id=p.id WHERE p.status=1 AND pck.konsumen_id='$id_konsumen' ORDER BY p.id DESC LIMIT 1");
        return $query;
    }      

    public function get_latest_promo() {
        $query = $this->db->query("select p.*, pku.id_kategori, pc.code from promo p JOIN promo_kategori_usaha pku ON pku.id_promo=p.id JOIN promo_code pc ON pc.promo_id=p.id WHERE status=1 ORDER BY id DESC LIMIT 1");
        return $query;
    }    

    public function count_promo_code_konsumen($id) {
        $query = $this->db->query("select mk.id_konsumen, mk.username as username_konsumen, mk.nama as nama_konsumen, mk.kontak, pc.*, pck.* from m_konsumen mk left join promo_code_konsumen pck on pck.konsumen_id=mk.id_konsumen left join promo_code pc on pc.id=pck.promo_code_id where pc.id=$id");
        return $query;
    }  

    public function histori_promo_code($id) {
        $query = $this->db->query("select mk.id_konsumen, mk.username as username_konsumen, mk.nama as nama_konsumen, mk.kontak, pc.*, pck.* from m_konsumen mk left join promo_code_konsumen pck on pck.konsumen_id=mk.id_konsumen left join promo_code pc on pc.id=pck.promo_code_id where pc.id=$id");
        return $query;
    }      

    public function list_promo_code_konsumen($id) {
        $query = $this->db->query("select mk.username as username_konsumen, mk.nama as nama_konsumen, mk.kontak, pc.*, pck.* from m_konsumen mk left join promo_code_konsumen pck on pck.konsumen_id=mk.id_konsumen left join promo_code pc on pc.id=pck.promo_code_id where pc.id=$id");
        return $query;
    }      

    public function list_expired_promo() {
        $query = $this->db->query("select * from promo where status=0 order by id desc");
        return $query;
    }

    public function create_promo($nama, $description, $promo_for, $id_provinsi, $id_kota, $jenis_promo, $message, $valid_duration, $max_konsumen, $target, $code_count) {
        $query = $this->db->query("insert into promo(nama, description, promo_for, id_provinsi, id_kota, jenis, message, valid_duration, max_konsumen, target, code_count, created_at) VALUE ('$nama', '$description', $promo_for, $id_provinsi, $id_kota, $jenis_promo, '$message', $valid_duration, $max_konsumen, $target, $code_count, now())");
        return $this->db->insert_id();
    }

    public function create_promo_konsumen($nama, $description, $promo_for, $jenis_promo, $message, $fail_message, $valid_duration, $time_unit, $max_purchase, $max_konsumen, $target, $code_count) {
        $query = $this->db->query("insert into promo(nama, description, promo_for, jenis, message, fail_message, valid_duration, time_unit, max_purchase, max_konsumen, target, code_count, created_at) VALUE ('$nama', '$description', $promo_for, $jenis_promo, '$message', '$fail_message', $valid_duration, $time_unit, $max_purchase, $max_konsumen, $target, $code_count, now())");
        return $this->db->insert_id();
    }

    public function get_orders($id_produk, $id_konsumen, $back_date) {
        $query = $this->db->query("SELECT ko.* FROM konsumen_order ko JOIN detail_order do ON do.id_order=ko.id_order WHERE ko.tanggal_order between '$back_date' and now() AND ko.id_konsumen='$id_konsumen' AND do.id_produk='$id_produk'");
        return $query->num_rows();
    }


    function copy_produk_promo($id_pemilik, $produk){        

        $produk->id_pemilik = $this->db->escape_str($id_pemilik);
        $produk->id_produk = $this->db->escape_str($id_pemilik) . "_" . $produk->id_produk;
        unset($produk->id_kategori_produk);
        unset($produk->nama_kategori);
        $this->db->insert('p_detail_produk', $produk);
    }


    public function generate_promo_codes($promo_id, $p_usahas, $promo_codes) {
        $stmt = "insert into promo_code(promo_id, id_pemilik, code) values ";
        $i = 0;
        foreach ($p_usahas as $p_usaha) {
            $code = $promo_codes[$i];
            if ($i > 0) {
                $stmt = $stmt . ", ";
            }
            $stmt = $stmt . "(".$promo_id.",".$p_usaha->id_pemilik.", '".$code."')";
            $i++;
        }
        $query = $this->db->query($stmt);
        return $query;
    }

    public function generate_promo_code($promo_id, $promo_code) {
        $stmt = "insert into promo_code(promo_id, id_pemilik, code) values ($promo_id, '00000000', '$promo_code')";
        $query = $this->db->query($stmt);
        return $query;
    }

    public function validate_promo_code($id_pemilik, $promo_code) {
        $promo_code    = $this->db->escape_str($promo_code);
        $query = $this->db->query("select pc.* from promo_code pc left join promo p on p.id=pc.promo_id where pc.code='$promo_code' AND pc.id_pemilik='$id_pemilik' AND p.status=1 LIMIT 1");
        return $query->row();
    }    

    public function check_promo_code($promo_code) {
        $query = $this->db->query("select pc.*, mpu.nama as nama_toko from promo_code pc left join promo p on p.id=pc.promo_id left join m_pemilik_usaha mpu on pc.id_pemilik=mpu.id_pemilik where p.status=1 AND pc.code=$promo_code AND pc.konsumen_count < p.max_konsumen LIMIT 1");
        return $query;
    }

    public function check_konsumen_promo($id_konsumen, $promo_code) {
        $query = $this->db->query("select pc.*, pck.activated_at from promo_code pc left join promo_code_konsumen pck on pck.promo_code_id=pc.id where pc.code='$promo_code' AND pck.konsumen_id='$id_konsumen' LIMIT 1");
        return $query;
    }

    public function get_promo_order_konsumen($id_konsumen, $id_promo, $id_pemilik) {
        $stmt = "select ko.* from promo_konsumen pk left join konsumen_order ko on pk.id_order=ko.id_order left join where pk.id_promo='$id_promo' AND ko.id_konsumen='$id_konsumen' AND ko.id_pemilik='$id_pemilik' AND ko.tanggal_order >= pk.first_order AND ko.tanggal_order < pk.last_order AND pk.last_order > CURRENT_DATE()";

        // echo $stmt; die();

        $query = $this->db->query($stmt);
        return $query->result();
    }

    public function insert_promo_konsumen($id_order, $id_promo) {
        $stmt1 = "SELECT * FROM promo_konsumen WHERE id_order='$id_order' AND id_promo='$id_promo'";
        // echo $stmt1; die();
        $promo_konsumen = $this->db->query($stmt1)->result();

        // echo json_encode($promo_konsumen); die();

        $stmt = "INSERT INTO promo_konsumen(`id_order`, `id_promo`, `first_order`, `last_order`) VALUE ('$id_order', 'id_promo', NOW(), DATE_ADD(NOW(), INTERVAL 14 DAY)";

        if (count($promo_konsumen) > 0) {
            $stmt = "update promo_konsumen SET `first_order` = `last_order`, `last_order` = DATE_ADD(`last_order` , INTERVAL 14 DAY) WHERE id_order='$id_order' AND id_promo='$id_promo'";
        } else {
            $stmt = "INSERT INTO promo_konsumen(`id_order`, `id_promo`, `first_order`, `last_order`) VALUE ('$id_order', '$id_promo', NOW(), DATE_ADD(NOW(), INTERVAL 14 DAY))";
        }
        // echo $stmt; die();

        $query = $this->db->query($stmt);
        return $query;
    }

    public function confirm_promo_code($promo_code) {
        $query = $this->db->query("update promo_code set used=1 where code='$promo_code'");
        $query3 = $this->db->query("select * from promo_code where code='$promo_code' LIMIT 1");
        $query2 = $this->db->query("update promo set code_count=code_count+1 where id=".$query3->row()->promo_id);
        return $query;
    }

    public function confirm_promo_code_konsumen($id, $promo_code_id) {
        $query = $this->db->query("update promo_code set konsumen_count=konsumen_count+1 where id=$promo_code_id");
        $query2 = $this->db->query("insert into promo_code_konsumen (konsumen_id, promo_code_id) value ($id, $promo_code_id)");
        return $query;
    }

    public function insert_histori_transaksi_promo($id_konsumen, $promo_code_id, $konsumen_order_id) {
        $this->db->query("insert into histori_transaksi_promo (id_konsumen, id_promo_code, id_order) value ('$id_konsumen', $promo_code_id, '$konsumen_order_id')");
        return $this->db;
    }

    public function set_kategori_usaha_promo($id_promo, $id_kategori) {
        $this->db->query("insert into promo_kategori_usaha (id_promo, id_kategori) value ($id_promo, $id_kategori)");
        return $this->db;
    }

    public function set_promo_produk($id_promo, $id_produk) {
        $this->db->query("insert into promo_produk (id_promo, id_produk) value ('$id_promo', $id_produk)");
        return $this->db;
    }
        
    public function get_produk_promo($id_promo) {
        $stmt = "select pdpp.id_produk, pdpp.nama_produk, pdpp.stok, pdpp.harga, pdpp.harga_promo, pdpp.foto, pdpp.status, pdpp.id_kategori as id_kategori_produk, mkp.nama_kategori from p_detail_produk_promo pdpp JOIN promo_produk pp ON pdpp.id_produk=pp.id_produk JOIN promo p ON p.id=pp.id_promo LEFT JOIN m_kategori_produk mkp ON mkp.id_kategori=pdpp.id_kategori WHERE pp.id_promo=".$id_promo;
        // echo $stmt; die();
        $query = $this->db->query($stmt)->result();

        return $query;
    }

    public function get_produk_promo_for_konsumen($id_promo, $id_konsumen) {
        // $query = $this->db->query("select pdpp.*, mkp.* from p_detail_produk_promo pdpp JOIN promo_produk pp ON pdpp.id_produk=pp.id_produk JOIN promo p ON p.id=pp.id_promo JOIN m_kategori_produk mkp ON mkp.id_kategori=pdpp.id_kategori JOIN promo_kategori_usaha pku ON pku.id_promo=p.id JOIN m_pemilik_usaha mpu ON mpu.id_kategori=pku.id_kategori WHERE pp.id_promo='$id_promo'")->result();
        $die = "select pdpp.id_produk, pdpp.nama_produk, pdpp.stok, pdpp.harga, pdpp.foto, pdpp.status, pdpp.id_kategori as id_kategori_produk, mkp.nama_kategori from p_detail_produk_promo pdpp JOIN promo_produk pp ON pdpp.id_produk=pp.id_produk JOIN promo p ON p.id=pp.id_promo LEFT JOIN m_kategori_produk mkp ON mkp.id_kategori=pdpp.id_kategori WHERE pp.id_promo=$id_promo";
        // echo $stmt; die();
        $query = $this->db->query($stmt)->result();

        return $query;
    }

    public function histori_transaksi($promo_code_id) {
        return $this->db->query("select htp.*, mk.nama as nama_konsumen, ko.tanggal_order as tanggal_order, ko.tanggal_proses, ko.status as status, ko.total as total, mpu.nama as nama_pemilik, do.kuantitas, pdp.nama_produk from histori_transaksi_promo htp left join konsumen_order ko on htp.id_order=ko.id_order left join m_konsumen mk on ko.id_konsumen=mk.id_konsumen left join m_pemilik_usaha mpu on mpu.id_pemilik=ko.id_pemilik left join detail_order do on do.id_order=ko.id_order left join p_detail_produk pdp on pdp.id_produk=do.id_produk WHERE htp.id_promo_code=$promo_code_id");
        
    }

     function insert_produk($id_produk,$nama,$harga,$stok,$keterangan,$id_kategori, $foto){        

        $id_produk    = $this->db->escape_str($id_produk);
        $nama    = $this->db->escape_str($nama);
        $harga    = $this->db->escape_str($harga);
        $stok    = $this->db->escape_str($stok);
        $keterangan    = $this->db->escape_str($keterangan);
        $id_kategori    = $this->db->escape_str($id_kategori);

        $query_produk   = "INSERT INTO `p_detail_produk_promo`(`id_produk`, `id_kategori`, `nama_produk`, `stok`, `keterangan`, `harga`, `status`, `foto`, `versi`) 
        VALUE('$id_produk',$id_kategori,'$nama','$stok','$keterangan','$harga','1', '$foto', 1) ";
        
        $this->db->query($query_produk);
    }

    public function delete($id) {
        $this->db->query("delete from promo where id=$id");
    }
}