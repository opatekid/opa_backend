<?php

class Model_kurir extends CI_Model{
    function __construct() {
        parent::__construct();
        $this->load->database();
    }


    public function insert_kurir($nama, $kontak, $tele_id) {
        $sub = substr($kontak, 0, 3);
        $sub2 = substr($kontak, 3);
        // str_replace(search, replace, subject)
        $sub = str_replace("628", "08", $sub);
        $kontak = $sub . $sub2;

        $id_pemilik = self::get_random_num();
        $stmt = "insert into m_kurir(id_kurir, nama, kontak, tele_id) value ('$id_kurir', '$nama', '$kontak', '$tele_id')";

        $query = $this->db->query($stmt);
        return $query;
    }

    public function get_by_kontak($kontak) {
        $query = $this->db->query("select * from m_kurir where kontak='$kontak' limit 1");
        return $query;
    }

    public function get_by_teleid($tele_id) {
        $query = $this->db->query("select * from m_kurir where tele_id='$tele_id' limit 1");
        return $query;
    }

    public function get_kredit() {
        $query = $this->db->query("select * from m_pemilik_usaha where status='1' order by id_pemilik desc");
        return $query;
    }
    
    public function get_kredit_detail($id) {
        $query = $this->db->query("select tanggal as 'tanggal','Isi Kredit' as ket, nominal as 'nominal' from isi_kredit where id_pemilik='$id' union
                                select tanggal_order as 'tanggal',concat('Fee transaksi konsumen ',b.nama) as ket, fee as 'nominal' from konsumen_order a,m_konsumen b where a.id_konsumen=b.id_konsumen and a.id_pemilik='$id' and a.status='2' order by tanggal desc");
        return $query;
    }

    public function get_random_num() {
        $result = $this->db->query("SELECT FLOOR(RAND() * 99999999) AS random_num
            FROM m_kurir WHERE 'random_num' NOT IN (SELECT id_kurir FROM m_kurir)
            LIMIT 1")->row();
        return $result->random_num;
    }
}