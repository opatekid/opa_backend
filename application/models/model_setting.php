<?php

class Model_setting extends CI_Model{
    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    public function get_pengguna() {
        $query = $this->db->query("SELECT * FROM pengguna where status='1' order by tanggal_insert desc");
        return $query;
    }
    
    function cek_email($email){
        $emaile      = $this->db->escape_str($email);
        $query = $this->db->query("select * from pengguna where email='$emaile'");
        return $query;
    }
    
    function cek_username($username){
        $usernamee   = $this->db->escape_str($username);
        $query = $this->db->query("select * from pengguna where username='$usernamee'");
        return $query;
    }
    
    public function insert_pengguna($nama,$username,$email,$pass) {
        $namae = $this->db->escape_str($nama);
        $usernamee = $this->db->escape_str($username);
        $emaile = $this->db->escape_str($email);
        $this->db->query("insert into pengguna(nama, username, email, password, level) values('$namae','$usernamee','$emaile','$pass','2')");
        $query = $this->db->query("select max(id) as 'jumlah' from pengguna");
        $row= $query->first_row();
        $this->db->query("insert into role_pengguna(id_pengguna,id_role) select '$row->jumlah',id_role from m_role");
    }
    
    public function delete_pengguna($id) {
        $this->db->query("update pengguna set status='0' where id='$id'");
    }
    
    public function insert_role($isi,$id) {
        $this->db->query("delete from role_pengguna where id_pengguna='$id'");
        $arr_isi = explode(",", $isi);
        for($i=0;$i<count($arr_isi);$i++){
            $this->db->query("insert into role_pengguna(id_pengguna, id_role)values('$id','$arr_isi[$i]')");
        }
    }
    
    public function check_hakakses($id) {
        $query = $this->db->query("select * from role_pengguna where id_pengguna='$id'");
        return $query;
    }
    
    //Login//
    public function get_user($username) {
        $query = $this->db->query ("select * from pengguna where username = '$username' and status='1'");
        return $query;
    }
    
    //
    public function nama_usr($id) {
        $query = $this->db->query ("select * from pengguna where id = '$id' and status='1'");
        return $query;
    }
    public function u_pwd($rnew_pass,$id) {
        $rnew_passe = $this->db->escape_str($rnew_pass);
        $this->db->query("update pengguna set password='$rnew_passe' where id='$id'");
    }
    public function sunting_prof($id,$nama,$username,$email) {
        $namae = $this->db->escape_str($nama);
        $usernamee = $this->db->escape_str($username);
        $emaile = $this->db->escape_str($email);
        $this->db->query("update pengguna set nama='$namae', username='$usernamee', email='$emaile' where id='$id'");
    }
    
    public function cek_username_email($id,$username,$email) {
        $ide = $this->db->escape_str($id);
        $usernamee = $this->db->escape_str($username);
	$emaile = $this->db->escape_str($email);
        $query = $this->db->query("select count(*) as total from pengguna where (username = '$usernamee' or email = '$emaile') and id <> '$ide' ");
        $row= $query->first_row();
        return $row->total;
    }
    public function cek_usr_email($email,$id) {
        $emaile = $this->db->escape_str($email);
        $query = $this->db->query("select count(*) as total from pengguna where email = '$emaile' and id <> '$id' ");
        $row= $query->first_row();
        return $row->total;
    }
    public function cek_usr_username($username,$id) {
        $usernamee = $this->db->escape_str($username);
        $query = $this->db->query("select count(*) as total from pengguna where username = '$usernamee' and id <> '$id' ");
        $row= $query->first_row();
        return $row->total;
    }
    
    //ROLE//
    public function get_role($id) {
        $query = $this->db->query("SELECT * FROM role_pengguna WHERE id_pengguna='$id' order by id_role ");
        return $query;
    }
    
    //LUPA PASS//
    public function get_email($email) {
        $emaile = $this->db->escape_str($email);
        $query = $this->db->query ("select * from pengguna where email = '$emaile' and status='1'");
        return $query;
    }
    public function update_lupa_pas($n_pass,$email){
        $this->db->query("update pengguna set password='$n_pass' where email='$email'");
    }
    
    ///BERANDA//
    public function get_konsumen() {
        $query = $this->db->query("select count(*) as 'total' from m_konsumen where status='1'");
        $row= $query->first_row();
        return $row->total;
    }
    public function get_pusaha() {
        $query = $this->db->query("select count(*) as 'total' from m_pemilik_usaha where status='1'");
        $row= $query->first_row();
        return $row->total;
    }
    public function get_order() {
        $query = $this->db->query("select count(*) as 'total' from konsumen_order where status='1'");
        $row= $query->first_row();
        return $row->total;
    }
        
    public function get_total() {
        $now = new DateTime('now');
//        $month = $now->format('m');
        $year  = $now->format('Y');
        $query = $this->db->query("select '1' as bulan, ifnull(sum(fee),0) as 'total' from konsumen_order where month(tanggal_order)='01' and year(tanggal_order)='$year' and status='2' 
                                    union 
                                    select '2' as bulan, ifnull(sum(fee),0) as 'total' from konsumen_order where month(tanggal_order)='02' and year(tanggal_order)='$year' and status='2' 
                                    union 
                                    select '3' as bulan, ifnull(sum(fee),0) as 'total' from konsumen_order where month(tanggal_order)='03' and year(tanggal_order)='$year' and status='2' 
                                    union 
                                    select '4' as bulan, ifnull(sum(fee),0) as 'total' from konsumen_order where month(tanggal_order)='04' and year(tanggal_order)='$year' and status='2' 
                                    union 
                                    select '5' as bulan, ifnull(sum(fee),0) as 'total' from konsumen_order where month(tanggal_order)='05' and year(tanggal_order)='$year' and status='2' 
                                    union 
                                    select '6' as bulan, ifnull(sum(fee),0) as 'total' from konsumen_order where month(tanggal_order)='06' and year(tanggal_order)='$year' and status='2' 
                                    union 
                                    select '7' as bulan, ifnull(sum(fee),0) as 'total' from konsumen_order where month(tanggal_order)='07' and year(tanggal_order)='$year' and status='2' 
                                    union 
                                    select '8' as bulan, ifnull(sum(fee),0) as 'total' from konsumen_order where month(tanggal_order)='08' and year(tanggal_order)='$year' and status='2' 
                                    union 
                                    select '9' as bulan, ifnull(sum(fee),0) as 'total' from konsumen_order where month(tanggal_order)='09' and year(tanggal_order)='$year' and status='2' 
                                    union 
                                    select '10' as bulan, ifnull(sum(fee),0) as 'total' from konsumen_order where month(tanggal_order)='10' and year(tanggal_order)='$year' and status='2' 
                                    union 
                                    select '11' as bulan, ifnull(sum(fee),0) as 'total' from konsumen_order where month(tanggal_order)='11' and year(tanggal_order)='$year' and status='2' 
                                    union 
                                    select '12' as bulan, ifnull(sum(fee),0) as 'total' from konsumen_order where month(tanggal_order)='12' and year(tanggal_order)='$year' and status='2'");
        return $query;
    }
    
    public function home_order() {
        
        $bulan = date("m");
        $tahun = date("Y");
        
        $query = $this->db->query("select count(*) as total from konsumen_order where year(tanggal_order) = $tahun and 
                month(tanggal_order) = $bulan");
        $row= $query->first_row();
        return $row->total;
    }
    
    public function home_konsumen() {
        
        $bulan = date("m");
        $tahun = date("Y");
        
        $query = $this->db->query("select count(*) as total from m_konsumen where year(tanggal_insert) = $tahun and 
                month(tanggal_insert) = $bulan and (status = 0 or status = 1)");
        $row= $query->first_row();
        return $row->total;
    }
    
    public function home_pemilik() {
        
        $bulan = date("m");
        $tahun = date("Y");
        
        $query = $this->db->query("select count(*) as total from m_pemilik_usaha where year(tanggal_insert) = $tahun and 
                month(tanggal_insert) = $bulan and (status = 0 or status = 1)");
        $row= $query->first_row();
        return $row->total;
    }
    
    public function home_fee() {
        
        $bulan = date("m");
        $tahun = date("Y");
        
        $query = $this->db->query("select SUM(fee) as jumlah from konsumen_order where MONTH(tanggal_order) = $bulan AND YEAR(tanggal_order) = $tahun and status='2' ");
        $row= $query->first_row();
        return $row->jumlah;
    }
    
    //SET_DEFAULT KREDIT//
    public function get_dkredit() {
        $query = $this->db->query("SELECT * FROM setting_umum order by id desc");
        return $query;
    }
    public function update_dkredit($id,$dkredit) {
        $dkredite = $this->db->escape_str($dkredit);
        $this->db->query("UPDATE `setting_umum` SET `default_kredit`='$dkredite' WHERE id='$id'");
    }

        //SET_DEFAULT CHARGE//
    public function get_charge() {
        $query = $this->db->query("SELECT * FROM setting_umum order by id desc");
        return $query;
    }
    public function update_charge($id,$charge) {
        $charge = $this->db->escape_str($charge);
        $this->db->query("UPDATE `setting_umum` SET `charge`='$charge' WHERE id='$id'");
    }
}