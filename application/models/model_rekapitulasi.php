<?php

class Model_rekapitulasi extends CI_Model{
    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    ////Penjualan///
    public function caritgl_penjualan_kategori($tanggal,$tglawal) {
        $query = $this->db->query("select a.nama,count(c.id_order) as jumlah,ifnull(sum(c.total),0) as total 
				from m_jenis_kategori_usaha a 
				left join m_pemilik_usaha b on a.id_jenis = b.id_jenis 
				left join konsumen_order c on b.id_pemilik = c.id_pemilik and date_format(c.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal'
				group by a.id_jenis");
        return $query;
        
    }
    public function caribln_penjualan_kategori($bulan,$tahun) {
        $query = $this->db->query("select a.nama,count(c.id_order) as jumlah,ifnull(sum(c.total),0) as total 
				from m_jenis_kategori_usaha a 
				left join m_pemilik_usaha b on a.id_jenis = b.id_jenis 
				left join konsumen_order c on b.id_pemilik = c.id_pemilik and month(c.tanggal_order)='$bulan' and year(c.tanggal_order)='$tahun'
				group by a.id_jenis");
        return $query;
    }
    public function carithn_penjualan_kategori($tahun) {
        $query = $this->db->query("select a.nama,count(c.id_order) as jumlah,ifnull(sum(c.total),0) as total 
				from m_jenis_kategori_usaha a 
				left join m_pemilik_usaha b on a.id_jenis = b.id_jenis 
				left join konsumen_order c on b.id_pemilik = c.id_pemilik and year(c.tanggal_order)='$tahun'
				group by a.id_jenis");
        return $query;
    }
    //
    public function caritgl_penjualan_pemilik($tanggal,$tglawal) {
        $query = $this->db->query("select b.nama,count(c.id_order) as jumlah,ifnull(sum(c.total),0) as total  
				from m_pemilik_usaha b 
				left join konsumen_order c on b.id_pemilik = c.id_pemilik and date_format(c.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal'
				group by b.id_pemilik");
        return $query;
        
    }
    public function caribln_penjualan_pemilik($bulan,$tahun) {
        $query = $this->db->query("select b.nama,count(c.id_order) as jumlah,ifnull(sum(c.total),0) as total  
				from m_pemilik_usaha b 
				left join konsumen_order c on b.id_pemilik = c.id_pemilik and month(c.tanggal_order)='$bulan' and year(c.tanggal_order)='$tahun'
				group by b.id_pemilik");
        return $query;
    }
    public function carithn_penjualan_pemilik($tahun) {
        $query = $this->db->query("select b.nama,count(c.id_order) as jumlah,ifnull(sum(c.total),0) as total  
				from m_pemilik_usaha b 
				left join konsumen_order c on b.id_pemilik = c.id_pemilik and year(c.tanggal_order)='$tahun'
				group by b.id_pemilik");
        return $query;
    }
    //
    public function caritgl_penjualan_provinsi($tanggal,$tglawal) {
        $query = $this->db->query("select a.nama,count(c.id_order) as jumlah,ifnull(sum(c.total),0) as total  
				from m_provinsi a 
				left join m_pemilik_usaha b on a.id = b.id_provinsi 
				left join konsumen_order c on b.id_pemilik = c.id_pemilik and date_format(c.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal'
				group by a.id");
        return $query;
    }
    public function caribln_penjualan_provinsi($bulan,$tahun) {
        $query = $this->db->query("select a.nama,count(c.id_order) as jumlah,ifnull(sum(c.total),0) as total  
				from m_provinsi a 
				left join m_pemilik_usaha b on a.id = b.id_provinsi 
				left join konsumen_order c on b.id_pemilik = c.id_pemilik and month(c.tanggal_order)='$bulan' and year(c.tanggal_order)='$tahun'
				group by a.id");
        return $query;
    }
    public function carithn_penjualan_provinsi($tahun) {
        $query = $this->db->query("select a.nama,count(c.id_order) as jumlah,ifnull(sum(c.total),0) as total  
				from m_provinsi a 
				left join m_pemilik_usaha b on a.id = b.id_provinsi 
				left join konsumen_order c on b.id_pemilik = c.id_pemilik and year(c.tanggal_order)='$tahun'
				group by a.id");
        return $query;
    }
    //
    public function caritgl_penjualan_kota($tanggal,$tglawal) {
        $query = $this->db->query("select a.nama,count(c.id_order) as jumlah,ifnull(sum(c.total),0) as total  
				from m_kota a 
				left join m_pemilik_usaha b on a.id = b.id_kota 
				left join konsumen_order c on b.id_pemilik = c.id_pemilik and date_format(c.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal'
				group by a.id");
        return $query;
    }
    public function caribln_penjualan_kota($bulan,$tahun) {
        $query = $this->db->query("select a.nama,count(c.id_order) as jumlah,ifnull(sum(c.total),0) as total  
				from m_kota a 
				left join m_pemilik_usaha b on a.id = b.id_kota 
				left join konsumen_order c on b.id_pemilik = c.id_pemilik and month(c.tanggal_order)='$bulan' and year(c.tanggal_order)='$tahun'
				group by a.id");
        return $query;
    }
    public function carithn_penjualan_kota($tahun) {
        $query = $this->db->query("select a.nama,count(c.id_order) as jumlah,ifnull(sum(c.total),0) as total  
				from m_kota a 
				left join m_pemilik_usaha b on a.id = b.id_kota 
				left join konsumen_order c on b.id_pemilik = c.id_pemilik and year(c.tanggal_order)='$tahun'
				group by a.id");
        return $query;
    }
    
    ////Belanja User///
    public function caritgl_belanja_provinsi($tanggal,$tglawal) {
        $query = $this->db->query("select a.nama,count(c.id_order) as jumlah,ifnull(sum(c.total),0) as total  
				from m_provinsi a 
				left join m_konsumen b on a.id = b.id_provinsi 
				left join konsumen_order c on b.id_konsumen = c.id_konsumen and date_format(c.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal'
				group by a.id");
        return $query;
    }
    public function caribln_belanja_provinsi($bulan,$tahun) {
        $query = $this->db->query("select a.nama,count(c.id_order) as jumlah,ifnull(sum(c.total),0) as total  
				from m_provinsi a 
				left join m_konsumen b on a.id = b.id_provinsi 
				left join konsumen_order c on b.id_konsumen = c.id_konsumen and month(c.tanggal_order)='$bulan' and year(c.tanggal_order)='$tahun'
				group by a.id");
        return $query;
    }
    public function carithn_belanja_provinsi($tahun) {
        $query = $this->db->query("select a.nama,count(c.id_order) as jumlah,ifnull(sum(c.total),0) as total  
				from m_provinsi a 
				left join m_konsumen b on a.id = b.id_provinsi 
				left join konsumen_order c on b.id_konsumen = c.id_konsumen and year(c.tanggal_order)='$tahun'
				group by a.id");
        return $query;
    }
    //
    public function caritgl_belanja_kota($tanggal,$tglawal) {
        $query = $this->db->query("select a.nama,count(c.id_order) as jumlah,ifnull(sum(c.total),0) as total  
				from m_kota a 
				left join m_konsumen b on a.id = b.id_kota 
				left join konsumen_order c on b.id_konsumen = c.id_konsumen and date_format(c.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal'
				group by a.id");
        return $query;
    }
    public function caribln_belanja_kota($bulan,$tahun) {
        $query = $this->db->query("select a.nama,count(c.id_order) as jumlah,ifnull(sum(c.total),0) as total  
				from m_kota a 
				left join m_konsumen b on a.id = b.id_kota 
				left join konsumen_order c on b.id_konsumen = c.id_konsumen and month(c.tanggal_order)='$bulan' and year(c.tanggal_order)='$tahun'
				group by a.id");
        return $query;
    }
    public function carithn_belanja_kota($tahun) {
        $query = $this->db->query("select a.nama,count(c.id_order) as jumlah,ifnull(sum(c.total),0) as total  
				from m_kota a 
				left join m_konsumen b on a.id = b.id_kota 
				left join konsumen_order c on b.id_konsumen = c.id_konsumen and year(c.tanggal_order)='$tahun'
				group by a.id");
        return $query;
    }
    
    ///Pendapatan Aplikasi//
    public function caritgl_pendapatan_waktu($tanggal,$tglawal) {
        $query = $this->db->query("select sum(fee),ifnull(sum(fee),0) as 'jumlah_pendapatan' from konsumen_order where status='2' and date_format(tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal'");
        return $query;
    }
    public function caribln_pendapatan_waktu($bulan,$tahun) {
        $query = $this->db->query("select sum(fee),ifnull(sum(fee),0) as 'jumlah_pendapatan' from konsumen_order where status='2' and month(tanggal_order)='$bulan' and year(tanggal_order)='$tahun'");
        return $query;
    }
    public function carithn_pendapatan_waktu($tahun) {
        $query = $this->db->query("select sum(fee),ifnull(sum(fee),0) as 'jumlah_pendapatan' from konsumen_order where status='2' and year(tanggal_order)='$tahun'");
        return $query;
    }
    //
    public function caritgl_pendapatan_kategori($tanggal,$tglawal) {
        $query = $this->db->query("select a.nama,count(c.id_order) as jumlah,ifnull(sum(c.fee),0) as total 
				from m_jenis_kategori_usaha a 
				left join m_pemilik_usaha b on a.id_jenis = b.id_jenis 
				left join konsumen_order c on b.id_pemilik = c.id_pemilik and c.status='2' and date_format(c.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal'
				group by a.id_jenis");
        return $query;
    }
    public function caribln_pendapatan_kategori($bulan,$tahun) {
        $query = $this->db->query("select a.nama,count(c.id_order) as jumlah,ifnull(sum(c.fee),0) as total 
				from m_jenis_kategori_usaha a 
				left join m_pemilik_usaha b on a.id_jenis = b.id_jenis 
				left join konsumen_order c on b.id_pemilik = c.id_pemilik and c.status='2' and month(c.tanggal_order)='$bulan' and year(c.tanggal_order)='$tahun'
				group by a.id_jenis");
        return $query;
    }
    public function carithn_pendapatan_kategori($tahun) {
        $query = $this->db->query("select a.nama,count(c.id_order) as jumlah,ifnull(sum(c.fee),0) as total 
				from m_jenis_kategori_usaha a 
				left join m_pemilik_usaha b on a.id_jenis = b.id_jenis 
				left join konsumen_order c on b.id_pemilik = c.id_pemilik and c.status='2' and year(c.tanggal_order)='$tahun'
				group by a.id_jenis");
        return $query;
    }
    //
    public function caritgl_pendapatan_provinsi($tanggal,$tglawal) {
        $query = $this->db->query("select a.nama,count(c.id_order) as jumlah,ifnull(sum(c.fee),0) as total  
				from m_provinsi a 
				left join m_pemilik_usaha b on a.id = b.id_provinsi 
				left join konsumen_order c on b.id_pemilik = c.id_pemilik and c.status='2' and date_format(c.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal'
				group by a.id");
        return $query;
    }
    public function caribln_pendapatan_provinsi($bulan,$tahun) {
        $query = $this->db->query("select a.nama,count(c.id_order) as jumlah,ifnull(sum(c.fee),0) as total  
				from m_provinsi a 
				left join m_pemilik_usaha b on a.id = b.id_provinsi 
				left join konsumen_order c on b.id_pemilik = c.id_pemilik and c.status='2' and month(c.tanggal_order)='$bulan' and year(c.tanggal_order)='$tahun'
				group by a.id");
        return $query;
    }
    public function carithn_pendapatan_provinsi($tahun) {
        $query = $this->db->query("select a.nama,count(c.id_order) as jumlah,ifnull(sum(c.fee),0) as total  
				from m_provinsi a 
				left join m_pemilik_usaha b on a.id = b.id_provinsi 
				left join konsumen_order c on b.id_pemilik = c.id_pemilik and c.status='2' and year(c.tanggal_order)='$tahun'
				group by a.id");
        return $query;
    }
    //
    public function caritgl_pendapatan_kota($tanggal,$tglawal) {
        $query = $this->db->query("select a.nama,count(c.id_order) as jumlah,ifnull(sum(c.fee),0) as total  
				from m_kota a 
				left join m_pemilik_usaha b on a.id = b.id_kota 
				left join konsumen_order c on b.id_pemilik = c.id_pemilik and c.status='2' and date_format(c.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal'
				group by a.id");
        return $query;
    }
    public function caribln_pendapatan_kota($bulan,$tahun) {
        $query = $this->db->query("select a.nama,count(c.id_order) as jumlah,ifnull(sum(c.fee),0) as total  
				from m_kota a 
				left join m_pemilik_usaha b on a.id = b.id_kota 
				left join konsumen_order c on b.id_pemilik = c.id_pemilik and c.status='2' and month(c.tanggal_order)='$bulan' and year(c.tanggal_order)='$tahun'
				group by a.id");
        return $query;
    }
    public function carithn_pendapatan_kota($tahun) {
        $query = $this->db->query("select a.nama,count(c.id_order) as jumlah,ifnull(sum(c.fee),0) as total  
				from m_kota a 
				left join m_pemilik_usaha b on a.id = b.id_kota 
				left join konsumen_order c on b.id_pemilik = c.id_pemilik and c.status='2' and year(c.tanggal_order)='$tahun'
				group by a.id");
        return $query;
    }
    //
    public function caritgl_pendapatan_pemilik($tanggal,$tglawal) {
        $query = $this->db->query("select b.nama,count(c.id_order) as jumlah,ifnull(sum(c.fee),0) as total  
				from m_pemilik_usaha b 
				left join konsumen_order c on b.id_pemilik = c.id_pemilik and date_format(c.tanggal_order,'%Y-%m-%d') between '$tglawal' and '$tanggal'
				group by b.id_pemilik");
        return $query;
    }
    public function caribln_pendapatan_pemilik($bulan,$tahun) {
        $query = $this->db->query("select b.nama,count(c.id_order) as jumlah,ifnull(sum(c.fee),0) as total  
				from m_pemilik_usaha b 
				left join konsumen_order c on b.id_pemilik = c.id_pemilik and month(c.tanggal_order)='$bulan' and year(c.tanggal_order)='$tahun'
				group by b.id_pemilik");
        return $query;
    }
    public function carithn_pendapatan_pemilik($tahun) {
        $query = $this->db->query("select b.nama,count(c.id_order) as jumlah,ifnull(sum(c.fee),0) as total  
				from m_pemilik_usaha b 
				left join konsumen_order c on b.id_pemilik = c.id_pemilik and year(c.tanggal_order)='$tahun'
				group by b.id_pemilik");
        return $query;
    }
    
    ////Data User////
    public function caritgl_data_pemilik_prov($tanggal,$tglawal) {
        $query = $this->db->query("select a.nama,count(b.id_pemilik) as jumlah 
                                from m_provinsi a 
                                left join m_pemilik_usaha b on a.id = b.id_provinsi and date_format(b.tanggal_insert,'%Y-%m-%d') between '$tglawal' and '$tanggal'
                                group by a.id");
        return $query;
    }
    public function caribln_data_pemilik_prov($bulan,$tahun) {
        $query = $this->db->query("select a.nama,count(b.id_pemilik) as jumlah 
                                from m_provinsi a 
                                left join m_pemilik_usaha b on a.id = b.id_provinsi and month(b.tanggal_insert)='$bulan' and year(b.tanggal_insert)='$tahun'
                                group by a.id");
        return $query;
    }
    public function carithn_data_pemilik_prov($tahun) {
        $query = $this->db->query("select a.nama,count(b.id_pemilik) as jumlah 
                                from m_provinsi a 
                                left join m_pemilik_usaha b on a.id = b.id_provinsi and year(b.tanggal_insert)='$tahun'
                                group by a.id");
        return $query;
    }
    //
    public function caritgl_data_pemilik_kota($tanggal,$tglawal) {
        $query = $this->db->query("select a.nama,count(b.id_pemilik) as jumlah 
                                from m_kota a 
                                left join m_pemilik_usaha b on a.id = b.id_kota and date_format(b.tanggal_insert,'%Y-%m-%d') between '$tglawal' and '$tanggal'
                                group by a.id");
        return $query;
    }
    public function caribln_data_pemilik_kota($bulan,$tahun) {
        $query = $this->db->query("select a.nama,count(b.id_pemilik) as jumlah 
                                from m_kota a 
                                left join m_pemilik_usaha b on a.id = b.id_kota and month(b.tanggal_insert)='$bulan' and year(b.tanggal_insert)='$tahun'
                                group by a.id");
        return $query;
    }
    public function carithn_data_pemilik_kota($tahun) {
        $query = $this->db->query("select a.nama,count(b.id_pemilik) as jumlah 
                                from m_kota a 
                                left join m_pemilik_usaha b on a.id = b.id_kota and year(b.tanggal_insert)='$tahun'
                                group by a.id");
        return $query;
    }
    
    // //
    public function caritgl_data_konsumen_prov($tanggal,$tglawal) {
        $query = $this->db->query("select a.nama,count(b.id_konsumen) as jumlah 
                                from m_provinsi a 
                                left join m_konsumen b on a.id = b.id_provinsi and date_format(b.tanggal_insert,'%Y-%m-%d') between '$tglawal' and '$tanggal'
                                group by a.id");
        return $query;
    }
    public function caribln_data_konsumen_prov($bulan,$tahun) {
        $query = $this->db->query("select a.nama,count(b.id_konsumen) as jumlah 
                                from m_provinsi a 
                                left join m_konsumen b on a.id = b.id_provinsi and month(b.tanggal_insert)='$bulan' and year(b.tanggal_insert)='$tahun'
                                group by a.id");
        return $query;
    }
    public function carithn_data_konsumen_prov($tahun) {
        $query = $this->db->query("select a.nama,count(b.id_konsumen) as jumlah 
                                from m_provinsi a 
                                left join m_konsumen b on a.id = b.id_provinsi and year(b.tanggal_insert)='$tahun'
                                group by a.id");
        return $query;
    }
    //
    public function caritgl_data_konsumen_kota($tanggal,$tglawal) {
        $query = $this->db->query("select a.nama,count(b.id_konsumen) as jumlah 
                                from m_provinsi a 
                                left join m_konsumen b on a.id = b.id_kota and date_format(b.tanggal_insert,'%Y-%m-%d') between '$tglawal' and '$tanggal'
                                group by a.id");
        return $query;
    }
    public function caribln_data_konsumen_kota($bulan,$tahun) {
        $query = $this->db->query("select a.nama,count(b.id_konsumen) as jumlah 
                                from m_provinsi a 
                                left join m_konsumen b on a.id = b.id_kota and month(b.tanggal_insert)='$bulan' and year(b.tanggal_insert)='$tahun'
                                group by a.id");
        return $query;
    }
    public function carithn_data_konsumen_kota($tahun) {
        $query = $this->db->query("select a.nama,count(b.id_konsumen) as jumlah 
                                from m_provinsi a 
                                left join m_konsumen b on a.id = b.id_kota and year(b.tanggal_insert)='$tahun'
                                group by a.id");
        return $query;
    }
}