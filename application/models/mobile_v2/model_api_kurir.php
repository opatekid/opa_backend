<?php  
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_api_kurir extends CI_Model 
{

    function __construct()
    {
            parent::__construct();
            $this->load->database();
    }

    function kirim_kode($id_kode,$kode){
       $this->db->query("INSERT INTO `kode_sms`(`id`, `kode`) VALUES('$id_kode','$kode')"); 
    }

    function cek_no_telp($no_telp){
        $sub = substr($no_telp, 0, 3);
        $sub2 = substr($no_telp, 3);
        // str_replace(search, replace, subject)
        $sub = str_replace("628", "08", $sub);
        //$no_telp = $sub . $sub2;
        
        $query = $this->db->query("select count(*) as total from m_kurir where kontak = '$no_telp'");
        return $query->first_row()->total;
    }

    function check_deviceid($idkurir, $deviceid)
    {
        $row = $this->db->query("select deviceid, id_kurir from m_kurir where id_kurir='$idkurir'")->row();
        
        if(isset($row))
        {
            if($row->deviceid == null) return false;
            else if($row->deviceid == "") return false;
            else if($row->deviceid != $deviceid) return false;
            else return true;
        }
        return false;
    }


    function cek_no_telp_withid($no_telp, $id_kurir){
        $sub = substr($no_telp, 0, 3);
        $sub2 = substr($no_telp, 3);
        // str_replace(search, replace, subject)
        $sub = str_replace("628", "08", $sub);
        //$no_telp = $sub . $sub2;
        
        $query = $this->db->query("select count(*) as total from m_kurir where kontak = '$no_telp' and id_kurir='$id_kurir'");
        return $query->first_row()->total;
    }

    function aktifasi($id_kurir){
        $query = $this->db->query("update m_kurir set status=1 where id_kurir='$id_kurir'");
        return $query;
    }

    function deaktifasi($id_kurir){
        $query = $this->db->query("update m_kurir set status=0 where id_kurir='$id_kurir'");
        return $query;
    }


    function cek_kode($id_kode,$kode){
        $id_kode    = $this->db->escape_str($id_kode);
        $kode    = $this->db->escape_str($kode);
        $query = $this->db->query("select count(*) as total from kode_sms where id = '$id_kode' and kode = '$kode'");
        return $query->first_row()->total;
    }

    function login($no_telp,$password){

        $no_telp   = $this->db->escape_str($no_telp);
        $password   = hash("sha512", $this->db->escape_str($password));

        // $no_telp   = "081265582400";
        // $password   = "a79a122977d9a519f17dea1335ef4dda415f0d3b5005c3cafa1bc7081770240a292f123e4cd14fa986720b99eca38abfb12ab81a765d06cbe00d5b3486b52b93";
        $stmt = "select * from m_kurir where kontak='$no_telp' and password='$password' and status=1";
        // echo "$stmt"; die();
       
        $query = $this->db->query($stmt);
       
        return $query;
    } 

    function insert_lokasi_usaha($id_kurir,$id_lokasi,$jenis_lokasi){

        $id_kurir    = $this->db->escape_str($id_kurir);
        $jenis_lokasi    = $this->db->escape_str($jenis_lokasi);
        $id_lokasi    = $this->db->escape_str($id_lokasi);
        
        $this->db->query("insert into p_detail_lokasi (id_lokasi,id_kurir,nama,lat,lng,jenis_lokasi)
            values ('$id_lokasi','$id_kurir','Nama Lokasi','0','0','$jenis_lokasi')");
    }

    function update_nama_lokasi_usaha($id_lokasi,$nama_lokasi){

        $nama_lokasi    = $this->db->escape_str($nama_lokasi);
        $id_lokasi    = $this->db->escape_str($id_lokasi);
        
        $this->db->query("update p_detail_lokasi set nama = '$nama_lokasi' where id_lokasi = '$id_lokasi'");
    }

    function update_lokasi_usaha($id_lokasi,$nama_lokasi,$lat,$lng){

        $lat    = $this->db->escape_str($lat);
        $lng    = $this->db->escape_str($lng);
        $id_lokasi    = $this->db->escape_str($id_lokasi);
        
        $this->db->query("update p_detail_lokasi set lat = '$lat', lng = '$lng', nama = '$nama_lokasi' where id_lokasi = '$id_lokasi'");
    }

    function get_kurir_all() {
        $sql = "select * from m_kurir";
        // echo $sql;  die();
        return $this->db->query($sql)->result();
    }

    function update_id($id_kurir, $new_id) {
        return $this->db->query("update m_kurir set id_kurir='$new_id' where id_kurir='$id_kurir'");
    }

    function get_open_pemilik($id_kurir) {
        return $this->db->query("select a.*, b.tipe as tipe_usaha  
            from m_kurir a left join m_jenis_kategori_usaha b on (a.tipe=2 OR a.id_jenis=b.id_jenis)
            where a.id_kurir='$id_kurir' AND (((CURRENT_TIME() < a.waktu_tutup) AND a.waktu_buka IS NOT NULL AND a.waktu_tutup IS NOT NULL) OR (a.waktu_buka IS NULL AND a.waktu_tutup IS NULL)) LIMIT 1")->result();
    }

    function get_lokasi_usaha($id_kurir){
        
        $id_kurir    = $this->db->escape_str($id_kurir);
       
        $query = $this->db->query("select * from p_detail_lokasi where id_kurir = '$id_kurir' ");
       
        return $query;
    } 

    function get_radius(){
        $query = $this->db->query("select * from m_radius order by radius");
        return $query->result();
    }

    function get_pemilik_fee($id_kurir){
        $id_kurir    = $this->db->escape_str($id_kurir);
        $query = $this->db->query("select fee from m_kurir where id_kurir='$id_kurir'");
        return $query->row()->fee;
    }
        
    function get_random_num() {
        $result = $this->db->query("SELECT FLOOR(RAND() * 99999999) AS random_num
            FROM m_kurir WHERE 'random_num' NOT IN (SELECT id_kurir FROM m_kurir)
            LIMIT 1")->row();
        return $result->random_num;
    }

    function get_random_nums($count) {
        $result = $this->db->query("SELECT FLOOR(RAND() * 99999999) AS random_num
            FROM m_kurir WHERE 'random_num' NOT IN (SELECT id_kurir FROM m_kurir)
            LIMIT " . $count)->result();
        return $result;
    }

    function get_random_phones($count) {
        $result = $this->db->query("SELECT FLOOR(RAND() * 999999999999) AS random_num
            FROM m_kurir WHERE 'random_num' NOT IN (SELECT kontak FROM m_kurir)
            LIMIT " . $count)->result();
        return $result;
    }

    function get_kategori_produk(){

        $query = $this->db->query("select * from m_kategori_produk where status=1");
        
        return $query->result();
    }

    function get_kategori_produk_pemilik($id_kurir){

        $query = $this->db->query("select * from m_kategori_produk where id_kurir='$id_kurir'");
        
        return $query->result();
    }

    function get_kategori_produk_by_nama($kategori,$id_kurir){

        $kategori = $this->db->escape_str($kategori);
        $id_kurir= $this->db->escape_str($id_kurir);

        /*$query = $this->db->query("select * from m_kategori_produk where status=1 and nama_kategori='$nama'");*/
        $query = $this->db->query("select * from m_kategori_produk where nama_kategori='$kategori' and id_kurir='$id_kurir'");  
        
        return $query->first_row()->id_kategori;
    }

    function get_kategori_by_nama_kategori($kategori){

        $id_kategori = 0;
        $kategori = $this->db->escape_str($kategori);

        /*$query = $this->db->query("select * from m_kategori_produk where status=1 and nama_kategori='$nama'");*/
        $query = $this->db->query("select * from m_kategori_produk where nama_kategori='$kategori'");  
        
        $result = $query->result();
        if (count($result) > 0) {
            $id_kategori = $query->first_row()->id_kategori;
        } else {
            $id_kategori = 0;
        }

        return $id_kategori;
    }

    function insert_kategori_produk($kategori,$id_kurir){
	
        $query_kategori = "insert into m_kategori_produk (id_kurir,nama_kategori)
                            SELECT '$id_kurir',tmp.* FROM (SELECT '$kategori') AS tmp
                            WHERE NOT EXISTS (
                                SELECT nama_kategori FROM m_kategori_produk WHERE nama_kategori = '$kategori' and id_kurir = '$id_kurir' 
                            ) LIMIT 1";
                            // echo $query_kategori; die();
                            
        //$query_kategori = "insert into m_kategori_produk (nama_kategori,id_kurir) values ('$kategori','$id_kurir')";                  
        $this->db->query($query_kategori);
        return $this->db->insert_id();
    }

    function insert_produk($id_produk,$id_kurir,$nama,$harga,$harga_promo,$stok,$keterangan,$id_kategori, $min_antar, $ket_promo){        

        $id_produk    = $this->db->escape_str($id_produk);
        $id_kurir    = $this->db->escape_str($id_kurir);
        $nama    = $this->db->escape_str($nama);
        $harga    = $this->db->escape_str($harga);
        $harga_promo    = $this->db->escape_str($harga_promo);
        $stok    = $this->db->escape_str($stok);
        $keterangan    = $this->db->escape_str($keterangan);
        $id_kategori    = $this->db->escape_str($id_kategori);
        $ket_promo    = $this->db->escape_str($ket_promo);

        // echo "harga_promo: " .$harga_promo . "\n";
        if ($harga_promo == "") {
            $harga_promo = "NULL";
        }


        $query_produk   = "INSERT INTO `p_detail_produk`(`id_produk`, `id_kurir`, `id_kategori`, `nama_produk`, `stok`, `keterangan`, `harga`, `harga_promo`, `status`, `min_antar`, `active`, `ket_promo`) 
        VALUES('$id_produk','$id_kurir','$id_kategori','$nama','$stok','$keterangan','$harga', " . $harga_promo . ", '1', '$min_antar', 1, '$ket_promo') ";

        // echo $query_produk; die();
        
        $this->db->query($query_produk);
    }

    function insert_produk_batch($id_kurir, $headers, $rows){    


        $query_produk   = "INSERT INTO `p_detail_produk` (";
        $query_produk = $query_produk . "`id_kurir`, ";
        $id_produk_pos = 0;
        $k = 0;
        $updateStmt = "ON DUPLICATE KEY UPDATE ";
        foreach ($headers as $header) {
            if ($header == 'id_barcode') {
                $id_produk_pos = $k;
                $header = 'id_produk';
            }
            $query_produk = $query_produk . "`".$header . "`, ";
            $updateStmt = $updateStmt . " " . $header . "=VALUES(" . $header . "), ";
            $k++;
        }
        $query_produk = $query_produk . " `active`) VALUES ";
        $query_produk = str_replace(", ) VALUES ", ") VALUES ", $query_produk);

        $updateStmt = $updateStmt . "HOHOHO";
        $updateStmt = str_replace("), HOHOHO", ") ", $updateStmt);

        $i = 0;
        foreach ($rows as $produk) {
            if (count($produk) <  $k) continue;

            $query_produk = $query_produk . "(";
            $query_produk = $query_produk . "'".$id_kurir . "', ";
            $j=0;
            foreach ($produk as $column) {
                // $column = $produk[$j];
                if ($j == $id_produk_pos) {
                    $column = $id_kurir . "_" . $column;
                }
                $query_produk = $query_produk . "'".$column . "', ";
                $j++;
            }

            $query_produk = $query_produk . "0, ), HIHI";
            $query_produk = str_replace(", ), HIHI", "),", $query_produk);

            $i++;
        }

        $query_produk = $query_produk . " HEHE";
        $query_produk = str_replace("), HEHE", ")", $query_produk);

        $query_produk = $query_produk . " " . $updateStmt;

        // echo $query_produk; die();
        
        $this->db->query($query_produk);
    }

    function set_produk_siap_antar_batch($id_kurir, $headers, $rows){    


        $query_produk   = "UPDATE `p_detail_produk` SET min_antar=1 WHERE id_produk IN (";
        $id_produk_pos = 0;
        $k = 0;
        foreach ($headers as $header) {
            if ($header == 'id_barcode') {
                $id_produk_pos = $k;
            }
            $k++;
        }

        $i = 0;
        foreach ($rows as $produk) {
            $query_produk = $query_produk . "'".$id_kurir . "_" . $produk[$id_produk_pos] . "', ";
            $i++;
        }

        $query_produk = $query_produk . ")";
        $query_produk = str_replace(", )", ")", $query_produk);


        // echo $query_produk; die();
        
        $this->db->query($query_produk);
    }

    // function insert_produk_batch($produks){    


    //     $query_produk   = "INSERT INTO `p_detail_produk`(`id_produk`, `id_kurir`, `id_kategori`, `nama_produk`, `stok`, `keterangan`, `harga`, `harga_promo`, `status`, `min_antar`) VALUES ";

    //     $i = 0;
    //     foreach ($produks as $produk) {
    //         $id_produk    = $produk->$id_produk;
    //         $id_kurir    = $produk->$id_kurir;
    //         $nama    = $produk->$nama;
    //         $harga    = $produk->$harga;
    //         $harga_promo    = $produk->$harga_promo;
    //         $stok    = $produk->$stok;
    //         $keterangan    = $produk->$keterangan;
    //         $id_kategori    = $produk->$id_kategori;

    //         if ($i > 0) {
    //             $query_produk = $query_produk . ", ('$id_produk','$id_kurir','$id_kategori','$nama','$stok','$keterangan','$harga', '$harga_promo', '1', '$min_antar')";
    //         } else {
    //             $query_produk = $query_produk . " ('$id_produk','$id_kurir','$id_kategori','$nama','$stok','$keterangan','$harga', '$harga_promo', '1', '$min_antar')";
    //         }

    //         $i++;
    //     }
        
    //     $this->db->query($query_produk);
    // }

    function update_produk($id_produk,$id_kategori,$nama,$harga,$harga_promo,$stok,$keterangan, $min_antar, $ket_promo){        

        $id_produk    = $this->db->escape_str($id_produk);
        $nama    = $this->db->escape_str($nama);
        $harga    = $this->db->escape_str($harga);
        $harga_promo    = $this->db->escape_str($harga_promo);
        $stok    = $this->db->escape_str($stok);
        $keterangan    = $this->db->escape_str($keterangan);
        $ket_promo    = $this->db->escape_str($ket_promo);

        if ($harga_promo == '') {
            $harga_promo == "NULL";
        }

        $query_produk   = "update p_detail_produk set nama_produk = '$nama', id_kategori = '$id_kategori', harga = '$harga', harga_promo = " . $harga_promo . ", stok = '$stok', keterangan = '$keterangan', min_antar = '$min_antar', ket_promo='$ket_promo'
                            where id_produk = '$id_produk'";
        
        $this->db->query($query_produk);
    }


    function update_produk_umkm($id_produk,$id_kategori,$nama,$harga,$harga_promo,$stok,$keterangan, $min_antar, $ket_promo){        

        $id_produk    = $this->db->escape_str($id_produk);
        $nama    = $this->db->escape_str($nama);
        $harga    = $this->db->escape_str($harga);
        $harga_promo    = $this->db->escape_str($harga_promo);
        $stok    = $this->db->escape_str($stok);
        $keterangan    = $this->db->escape_str($keterangan);
        $ket_promo    = $this->db->escape_str($ket_promo);

        $query_produk   = "update p_detail_produk_umkm set nama_produk = '$nama', id_kategori = '$id_kategori', harga = '$harga', harga_promo = '$harga_promo', stok = '$stok', keterangan = '$keterangan', min_antar = '$min_antar', ket_promo='$ket_promo'
                            where id_produk = '$id_produk'";
        
        $this->db->query($query_produk);
    }

    function update_stok($id_produk,$stok){

        $id_produk    = $this->db->escape_str($id_produk);
        $stok    = $this->db->escape_str($stok);
        
        $this->db->query("update p_detail_produk set stok = '$stok' where id_produk = '$id_produk'");
    }


    // function update_stocks($id_kurir, $stocks){
    //     $when = ""; 
    //     $in = "where id_produk in (";
    //     $stmt = "update p_detail_produk set stok = case";
    //     $i = 0;
    //     // echo json_encode($stocks); die();
    //     foreach ($stocks as $stock) {
    //         $when = $when . " when id_produk = '" . $id_kurir . "_". $stock->id_produk . "' then " .  $stock->stok;
    //         if ($i == (count($stocks)-1)) {
    //             $in = $in . "'" . $id_kurir . "_". $stock->id_produk . "'";
    //         } else {    
    //             $in = $in . "'" . $id_kurir . "_". $stock->id_produk . "',";
    //         }
    //         $i++;
    //     }
    //     $when = $when . " end ";
    //     $in = $in . ")";

    //     $stmt = $stmt . $when . $in;
    //     $this->db->query($stmt);

    //     // echo $stmt; die();

    //     // $this->db->update_batch("p_detail_produk", $stocks, "id_produk'");
    // }

    function update_stocks($id_kurir, $stocks){
        $i = 0;

        // $return = $this->db->query("delete from p_detail_produk where id_kurir='" . $id_kurir . "'");
        
        $insert = "INSERT INTO p_detail_produk (id_produk, id_kurir, nama_produk, id_kategori, stok, harga, foto, keterangan, active) VALUES ";
        $update = "ON DUPLICATE KEY UPDATE id_kurir=VALUES(id_kurir), nama_produk=VALUES(nama_produk), id_kategori=VALUES(id_kategori), stok=VALUES(stok), harga=VALUES(harga), foto=CONCAT(REPLACE(VALUES(id_produk), '".$id_kurir."_', ''), '.png'), active=1";

        foreach ($stocks as $stock) {
            $insert = $insert . "('" . $id_kurir . "_" . $stock->id_produk . "', '" . $id_kurir . "', '" . (isset($stock->nama_produk) ? self::clean($stock->nama_produk) : '') . "', " . (isset($stock->id_kategori) ? $stock->id_kategori : '0') . ", " . (isset($stock->stok) ? $stock->stok : '0') . ", " . (isset($stock->harga) ? $stock->harga : '0') . ", '" . (isset($stock->foto) ? $stock->foto : '') . "', '". (isset($stock->keterangan) ? $stock->keterangan : '') . "', 1), ";
            $i++;
            
        }

        $stmt = $insert . $update;
        $stmt = str_replace("), ON DUPLICATE KEY UPDATE", ") ON DUPLICATE KEY UPDATE", $stmt);
        // echo $stmt; die();
        // $stmt = self::clean($stmt);
        $return = $this->db->query($stmt);
        $errMess = "";

        if(!$return)
        {
           $errNo   = $this->db->_error_number();
           $errMess = $this->db->_error_message();
           // Do something with the error message or just show_404();
        }
        return $errMess;
    }


    function update_produks_umkm($id_kurir, $stocks){
        $i = 0;

        // $return = $this->db->query("delete from p_detail_produk where id_kurir='" . $id_kurir . "'");
        
        $insert = "INSERT INTO p_detail_produk_umkm (id_produk, id_kurir, nama_produk, stok, harga, harga_promo, keterangan, active) VALUES ";
        $update = "ON DUPLICATE KEY UPDATE id_kurir=VALUES(id_kurir), nama_produk=VALUES(nama_produk), nama_produk=VALUES(nama_produk), harga=VALUES(harga), harga_promo=VALUES(harga_promo), active=1";

        foreach ($stocks as $stock) {
            $harga_promo = "";
            if ($stock->harga_promo == "") {
                $harga_promo = "NULL";
            } else {
                $harga_promo = $stock->harga_promo;
            }
            $insert = $insert . "('" . $id_kurir . "_" . $stock->id_produk . "', '" . $id_kurir . "', '" . (isset($stock->nama_produk) ? self::clean($stock->nama_produk) : '') . "', " . (isset($stock->stok) ? $stock->stok : '0') . ", " . (isset($stock->harga) ? $stock->harga : '0')  . ", " . $harga_promo . ", '". (isset($stock->keterangan) ? $stock->keterangan : '') . "', 1), ";
            $i++;
            
        }

        $stmt = $insert . $update;
        $stmt = str_replace("), ON DUPLICATE KEY UPDATE", ") ON DUPLICATE KEY UPDATE", $stmt);
        // echo $stmt; die();
        // $stmt = self::clean($stmt);
        $return = $this->db->query($stmt);
        $errMess = "";

        if(!$return)
        {
           $errNo   = $this->db->_error_number();
           $errMess = $this->db->_error_message();
           // Do something with the error message or just show_404();
        }
        return $errMess;
    }

    function get_produks_umkm($id_kurir, $page){

        $id_kurir = $this->db->escape_str($id_kurir);
        $page = $this->db->escape_str($page);

        if (intval($page >=1 )) { // kalau paging dimulai dari 1, enable pagint. Jika tidak, tampilkan seluruhnya
            $offset = ($page-1) * 50; 
            $pagingParam =  "LIMIT 50 OFFSET " . $offset;
        }
        
        $sql = "select a.*,b.nama_kategori, 0 as is_official from p_detail_produk_umkm a left join m_kurir c on c.id_partner=a.id_kurir or c.id_kurir=a.id_kurir left join m_kategori_produk b on a.id_kategori=b.id_kategori where a.id_kurir='$id_kurir' AND a.nama_produk <> '' AND a.active=1 order by b.nama_kategori " . $pagingParam;
        // echo $sql; die();
        $query = $this->db->query($sql);
        
        return $query->result();
    }

    function get_total_produks_umkm($id_kurir){

        $id_kurir = $this->db->escape_str($id_kurir);
        
        $query = $this->db->select('count(*) as total')->from('p_detail_produk_umkm')->where('id_kurir', $id_kurir);
        $query = $this->db->get();
        return $query->row()->total;
    }

    function count_products($id_kurir) {
        $stmt = "SELECT COUNT(*) as total FROM p_detail_produk WHERE id_kurir='".$id_kurir."'";
        $query = $this->db->query($stmt);
        return $query->row_array();
    }

    function copy_products($id_kurir_old, $id_kurir_new) {
        $stmt = "INSERT INTO p_detail_produk (id_produk,id_kurir,id_kategori,nama_produk,stok,harga,harga_promo,active)
        SELECT REPLACE(id_produk, '".$id_kurir_old."_', '".$id_kurir_new."_'), '".$id_kurir_new."', id_kategori, nama_produk, stok, harga, harga_promo, 1
        FROM p_detail_produk WHERE id_kurir = '".$id_kurir_old."' ON DUPLICATE KEY UPDATE id_kurir=VALUES(id_kurir), id_kategori=VALUES(id_kategori), nama_produk=VALUES(nama_produk), stok=VALUES(stok), harga=VALUES(harga), harga_promo=VALUES(harga_promo), active=1";
        // echo $stmt; die();
        $query = $this->db->query($stmt);
        return $query;
    }

    function clean($string) {
       $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
       return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    // function get_produk($id_kurir){

    //     $id_kurir    = $this->db->escape_str($id_kurir);

    //     return $this->db->query("select a.*,b.nama_kategori
    //                         from p_detail_produk a, m_kategori_produk b
    //                         where
    //                         a.id_kategori = b.id_kategori
    //                         and
    //                         a.id_kurir = '$id_kurir' and a.status = 1
    //                         order by b.nama_kategori")->result();
    
    // }

    function get_pemilik_by_partner($id_partner){

        $id_partner    = $this->db->escape_str($id_partner);
        
        $query = $this->db->query("select * from m_kurir where id_partner = '$id_partner'");

        return $query;
    }

    function get_kurir($id_kurir){

        $id_kurir    = $this->db->escape_str($id_kurir);

        $sql = "select * from m_kurir where id_kurir = '$id_kurir'";

        // echo $sql; die();
        
        $query = $this->db->query($sql);

        return $query->result();
    }


    function get_nearest_kurir($lat,$lng){

            $sql = "SELECT b . *, CONCAT('" . URL_IMAGE_AVATAR_KURIR . "', b.avatar) as avatar, CONCAT('" . URL_IMAGE_AVATAR_KURIR . "', b.foto) as foto, ( 6371 * ACOS( COS( RADIANS( '$lat' ) ) * COS( RADIANS( b.lat ) ) * COS( RADIANS( b.lng ) - RADIANS( '$lng' ) ) + SIN( RADIANS( '$lat' ) ) * SIN( RADIANS( b.lat ) ) ) ) as distance 
            FROM m_kurir b 
            WHERE ( 6371 * ACOS( COS( RADIANS( '$lat' ) ) * COS( RADIANS( b.lat ) ) * COS( RADIANS( b.lng ) - RADIANS( '$lng' ) ) + SIN( RADIANS( '$lat' ) ) * SIN( RADIANS( b.lat ) ) ) ) <= (b.radius /1000 )  AND b.status=1
            ORDER BY distance LIMIT 1";

            // echo $sql; die();

            $query = $this->db->query($sql);

            return $query->first_row();
    }


    function insert_kiriman($id_kiriman, $id_kurir, $tipe, $id_konsumen, $jenis_alamat, $keterangan, $lat, $lng, $alamat, $jenis_alamat, $ket_alamat, $fee, $total){
           // echo "tipe " . $tipe_pemilik; die();
        $id_kiriman   = $this->db->escape_str($id_kiriman);
        $id_kurir   = $this->db->escape_str($id_kurir);
        $id_konsumen    = $this->db->escape_str($id_konsumen);
        $total      = $this->db->escape_str($total);
        $fee            = $this->db->escape_str($fee);
        $jenis_alamat   = $this->db->escape_str($jenis_alamat);
        $alamat   = $this->db->escape_str($alamat);
        $lat   = $this->db->escape_str($lat);
        $lng   = $this->db->escape_str($lng);
        $ket_alamat   = $this->db->escape_str($ket_alamat);
        $keterangan   = $this->db->escape_str($keterangan);

        $query = $this->db->query("insert into konsumen_kiriman(id_kiriman, id_kurir, id_konsumen, tipe, jenis_alamat, lat, lng, alamat, ket_alamat, keterangan, fee, total, tanggal_order) value ('$id_kiriman', '$id_kurir', '$id_konsumen', '$tipe', '$jenis_alamat', '$lat', '$lng', '$alamat', '$ket_alamat', '$keterangan', '$fee', '$total', now())");
    }


    function insert_detail_kiriman($id_kiriman, $nama_penerima, $keterangan, $alamat, $lat, $lng, $fee){
           // echo "tipe " . $tipe_pemilik; die();
        $id_kiriman   = $this->db->escape_str($id_kiriman);
        $nama_penerima    = $this->db->escape_str($nama_penerima);
        $keterangan      = $this->db->escape_str($keterangan);
        $alamat            = $this->db->escape_str($alamat);
        $lat   = $this->db->escape_str($lat);
        $lng   = $this->db->escape_str($lng);
        $fee   = $this->db->escape_str($fee);

        $query = $this->db->query("insert into detail_kiriman(id_kiriman, nama_penerima, keterangan, alamat, lat, lng, fee) value ('$id_kiriman', '$nama_penerima', '$keterangan', '$alamat', '$lat', '$lng', '$fee')");
    }



    function insert_detail_kiriman_ekspedisi($id_kiriman, $nama_penerima, $keterangan, $alamat, $id_agen, $id_layanan_ekspedisi, $id_origin, $kota_origin, $id_destination, $kota_destination, $weight, $fee, $est_price, $etd){
           // echo "tipe " . $tipe_pemilik; die();
        $id_kiriman   = $this->db->escape_str($id_kiriman);
        $nama_penerima    = $this->db->escape_str($nama_penerima);
        $keterangan      = $this->db->escape_str($keterangan);
        $alamat            = $this->db->escape_str($alamat);
        $id_agen   = $this->db->escape_str($id_agen);
        $id_layanan_ekspedisi   = $this->db->escape_str($id_layanan_ekspedisi);
        $id_origin   = $this->db->escape_str($id_origin);
        $id_destination   = $this->db->escape_str($id_destination);
        $kota_origin   = $this->db->escape_str($kota_origin);
        $kota_destination   = $this->db->escape_str($kota_destination);
        $weight   = $this->db->escape_str($weight);
        $fee   = $this->db->escape_str($fee);
        $etd   = $this->db->escape_str($etd);
        $est_price   = $this->db->escape_str($est_price);

        $query = $this->db->query("insert into detail_kiriman_ekspedisi(id_kiriman, nama_penerima, keterangan, alamat, id_agen, id_layanan_ekspedisi, id_origin, kota_origin, id_destination, kota_destination, weight, fee, est_price, etd) value ('$id_kiriman', '$nama_penerima', '$keterangan', '$alamat', '$id_agen', '$id_layanan_ekspedisi', '$id_origin', '$kota_origin', '$id_destination', '$kota_destination', '$weight', '$fee', '$est_price', '$etd')");
    }


    function get_first_pemilik_by_partner($id_partner){

        $id_partner    = $this->db->escape_str($id_partner);
        
        $query = $this->db->query("select * from m_kurir where id_partner = '$id_partner' ORDER BY tanggal_insert LIMIT 1");

        return $query->row();
    }

    function get_kiriman_by_id_detail_kiriman($id) {
        $id    = $this->db->escape_str($id);
        
        // $query = $this->db->query("select * from m_ekspedisi a left join m_detail_kiriman");

        return $query->first_row();
    }

    function delete_produk($id_produk){

        $id_produk    = $this->db->escape_str($id_produk);
        
        $this->db->query("delete from p_detail_produk where id_produk = '$id_produk'");
    }

    function copy_produk_to_histori($id_produk) {
        $stmt = "INSERT INTO histori_p_detail_produk SELECT p.* FROM p_detail_produk p WHERE p.id_produk = '".$id_produk."'";
        $this->db->query($stmt);
    }

    function delete_produk_by_pemilik($id_kurir){

        $id_kurir    = $this->db->escape_str($id_kurir);
        
        $this->db->query("delete from p_detail_produk where id_kurir = '$id_kurir'");
    }

    function get_order($id_kurir, $status=1){
        
        $id_kurir    = $this->db->escape_str($id_kurir);
        /*
        status  1 : baru
                2 : sudah di proses
                3 : sudah selesai
                4 : cancel
        */

        $query = $this->db->query("select a.*,b.nama,b.alamat,b.kontak,b.lat,b.lng,b.foto,b.versi, c.nama as nama_kota, e.nama as nama_merchant, e.lat as lat_merchant, e.lng as lng_merchant, e.alamat as alamatmerchant, e.foto as fotomerchant 
from konsumen_order a, m_konsumen b , m_kota c , m_kurir d , m_pemilik_usaha e 
                    where  
                    a.id_konsumen=b.id_konsumen and a.id_pemilik=e.id_pemilik and e.kontak_sms2=d.kontak and d.id_kurir='$id_kurir' and c.id = b.id_kota and a.status=$status group by a.id_order
                    order by a.status asc");     
        
        return $query->result();
    }


    function get_kiriman($id_kurir, $tipe=1){
                    // echo $no_telp_kurir; die();
        
        $id_kurir   = $this->db->escape_str($id_kurir);
        $tipe    = $this->db->escape_str($tipe);

        $tipe_param = " e.tipe=" . $tipe;
        
        /*
        status  1 : baru
                2 : sudah di proses
                3 : sudah selesai
                4 : cancel
        */

        $stmt = "select a.*,  CONCAT('" . URL_IMAGE_KONSUMEN . "', b.foto) as foto, b.nama,b.alamat,b.kontak,b.lat,b.lng,b.versi, e.nama as nama_kurir, e.lat as lat_merchant, e.lng as lng_merchant, e.alamat as alamatmerchant, e.foto as fotomerchant from konsumen_kiriman a left join m_konsumen b on a.id_konsumen=b.id_konsumen left join m_kurir e on a.id_kurir=e.id_kurir
                    where e.kontak_sms2='$no_telp_kurir' and b.nama IS NOT NULL and " . $tipe_param . " group by a.id_kiriman
                    order by a.status asc";


        $query = $this->db->query($stmt);     
        
        return $query->result();
    }

    function get_done_order($no_telp_kurir, $tipe=1){
                    // echo $no_telp_kurir; die();
        
        $no_telp_kurir   = $this->db->escape_str($no_telp_kurir);
        $tipe    = $this->db->escape_str($tipe);

        $tipe_param = "";
        if (intval($tipe) == 1) { // usaha / toko
            $tipe_param = " (e.tipe=1 OR e.tipe=2 OR e.tipe=3)";
        } else if (intval($tipe) == 2) { // catering
            $tipe_param = " e.tipe=5";
        } else if (intval($tipe) == 3) { // pickup post
            $tipe_param = " e.tipe=6";
        }
        

        /*
        status  1 : baru
                2 : sudah di proses
                3 : sudah selesai
                4 : cancel
        */

        $stmt = "select a.*,  CONCAT('" . URL_IMAGE_KONSUMEN . "', b.foto) as foto, b.nama,b.alamat,b.kontak,b.lat,b.lng,b.versi, c.nama as nama_kota, e.nama as nama_merchant, e.lat as lat_merchant, e.lng as lng_merchant, e.alamat as alamatmerchant, e.foto as fotomerchant 
from konsumen_order a left join m_konsumen b on a.id_konsumen=b.id_konsumen left join m_kota c on c.id = b.id_kota left join m_pemilik_usaha e on a.id_pemilik=e.id_pemilik
                    where e.kontak_sms2='$no_telp_kurir' and (a.status=4 OR a.status=3) and b.nama IS NOT NULL and " . $tipe_param . " group by a.id_order
                    order by a.tanggal_order asc";


        $query = $this->db->query($stmt);     
        
        return $query->result();
    }

    function get_new_order_count($no_telp_kurir, $tipe) {
        $no_telp_kurir    = $this->db->escape_str($no_telp_kurir);
        $tipe    = $this->db->escape_str($tipe);

        $tipe_param = "";
        if (intval($tipe) == 1) { // usaha / toko
            $tipe_param = " (e.tipe=1 OR e.tipe=2 OR e.tipe=3)";
        } else if (intval($tipe) == 2) { // catering
            $tipe_param = " e.tipe=5";
        } else if (intval($tipe) == 3) { // pickup post
            $tipe_param = " e.tipe=6";
        }
        
        $stmt = "select a.*, CONCAT('" . URL_IMAGE_KONSUMEN . "', b.foto) as foto,b.nama,b.alamat,b.kontak,b.lat,b.lng,b.versi, c.nama as nama_kota, e.nama_produk as nama_produk, e.harga, d.kuantitas as kuantitas, d.id_produk 
                    from konsumen_order a left join m_konsumen b on a.id_konsumen=b.id_konsumen left join m_kota c on c.id = b.id_kota left join detail_order d on d.id_order=a.id_order left join p_detail_produk e on e.id_produk=d.id_produk left join m_pemilik_usaha f on f.id_pemilik=e.id_pemilik
                    where f.kontak_sms2='$no_telp_kurir' and a.status!=4 and a.status!=3 and b.nama IS NOT NULL
                    and " . $tipe_param . " group by a.id_order order by a.id_order asc 
                    ";
        $query = $this->db->query($stmt);

        $record =  $query->num_rows();
        // echo json_encode($record); die();

        return $record;
    }

    function get_new_kiriman_count($no_telp_kurir) {
        $no_telp_kurir    = $this->db->escape_str($no_telp_kurir);
        $tipe    = $this->db->escape_str($tipe);
        
        $stmt = "select a.*, CONCAT('" . URL_IMAGE_KONSUMEN . "', b.foto) as foto,b.nama,b.alamat,b.kontak,b.lat,b.lng,b.versi from konsumen_kiriman a left join m_konsumen b on a.id_konsumen=b.id_konsumen left join detail_kiriman d on d.id_kiriman=a.id_kiriman left join m_kurir f on f.id_kurir=a.id_kurir
                    where f.kontak='$no_telp_kurir' and a.status!=4 and a.status!=3 and b.nama IS NOT NULL group by a.id_kiriman order by a.id_kiriman asc";

        // echo $stmt; die();

        $query = $this->db->query($stmt);

        $record =  $query->num_rows();
        // echo json_encode($record); die();

        return $record;
    }

    function get_order_detail($id_kurir, $id_order){
        
        $id_kurir    = $this->db->escape_str($id_kurir);
        $id_order    = $this->db->escape_str($id_order);
        /*
        status  1 : baru
                2 : sudah di proses
                3 : sudah selesai
                4 : cancel
        */

        $query = $this->db->query("select a.*,b.nama,b.alamat,b.kontak,b.lat,b.lng,b.foto,b.versi, c.nama as nama_kota, e.nama as nama_merchant, e.lat as lat_merchant, e.lng as lng_merchant, e.alamat as alamatmerchant, e.foto as fotomerchant 
from konsumen_order a, m_konsumen b , m_kota c , m_kurir d , m_pemilik_usaha e 
                    where  
                    a.id_konsumen=b.id_konsumen and a.id_kurir=e.id_kurir and e.kontak_sms2=d.kontak and d.id_kurir='$id_kurir' and c.id = b.id_kota and a.id_order = '$id_order' group by a.id_order
                    order by a.status asc");     
        
        return $query->result();
    }

    function get_undone_order($no_telp_kurir, $tipe){
        
        $no_telp_kurir    = $this->db->escape_str($no_telp_kurir);
        $tipe    = $this->db->escape_str($tipe);

        $tipe_param = "";
        if (intval($tipe) == 1) { // usaha / toko
            $tipe_param = " (f.tipe=1 OR f.tipe=2 OR f.tipe=3)";
        } else if (intval($tipe) == 2) { // catering
            $tipe_param = " f.tipe=5";
        } else if (intval($tipe) == 3) { // pickup post
            $tipe_param = " f.tipe=6";
        }
        
        $sql = "select a.*, CONCAT('" . URL_IMAGE_KONSUMEN . "', b.foto) as foto,b.nama,b.alamat,b.kontak,b.lat,b.lng,b.versi, c.nama as nama_kota, e.nama_produk as nama_produk, e.harga, d.kuantitas as kuantitas, d.id_produk 
                    from konsumen_order a left join m_konsumen b on a.id_konsumen=b.id_konsumen left join m_kota c on c.id = b.id_kota left join detail_order d on d.id_order=a.id_order left join p_detail_produk e on e.id_produk=d.id_produk left join m_pemilik_usaha f on f.id_pemilik=e.id_pemilik
                    where f.kontak_sms2='$no_telp_kurir' and a.status!=4 and a.status!=3 and b.nama IS NOT NULL
                    and " . $tipe_param . " group by a.id_order order by a.tanggal_order asc 
                    ";

        // echo $sql; die();
        $query = $this->db->query($sql);
        
        return $query->result();
    }


    function get_undone_kiriman($id_kurir, $tipe){
        
        $id_kurir    = $this->db->escape_str($id_kurir);
        $tipe    = $this->db->escape_str($tipe);

        $tipe_param = "";
        if (intval($tipe) == 1) { // usaha / toko
            $tipe_param = " (f.tipe=1 OR f.tipe=2 OR f.tipe=3)";
        } else if (intval($tipe) == 2) { // catering
            $tipe_param = " f.tipe=5";
        } else if (intval($tipe) == 3) { // pickup post
            $tipe_param = " f.tipe=6";
        }
        
        $sql = "select a.*, CONCAT('" . URL_IMAGE_KONSUMEN . "', b.foto) as foto,b.nama,b.alamat,b.kontak,b.lat,b.lng,b.versi
                    from konsumen_kiriman a left join m_konsumen b on a.id_konsumen=b.id_konsumen left join detail_kiriman d on d.id_kiriman=a.id_kiriman left join m_kurir f on f.id_kurir=a.id_kurir
                    where a.id_kurir='$id_kurir' and a.status!=4 and a.status!=3 and b.nama IS NOT NULL
                     group by a.id_kiriman order by a.id_kiriman asc 
                    ";

        // echo $sql; die();
        $query = $this->db->query($sql);
        
        return $query->result();
    }

    function get_done_kiriman($id_kurir, $tipe){
        
        $id_kurir    = $this->db->escape_str($id_kurir);
        $tipe    = $this->db->escape_str($tipe);

        $tipe_param = "";
        if (intval($tipe) == 1) { // usaha / toko
            $tipe_param = " (f.tipe=1 OR f.tipe=2 OR f.tipe=3)";
        } else if (intval($tipe) == 2) { // catering
            $tipe_param = " f.tipe=5";
        } else if (intval($tipe) == 3) { // pickup post
            $tipe_param = " f.tipe=6";
        }
        
        $sql = "select a.*, CONCAT('" . URL_IMAGE_KONSUMEN . "', b.foto) as foto,b.nama,b.alamat,b.kontak,b.lat,b.lng,b.versi
                    from konsumen_kiriman a left join m_konsumen b on a.id_konsumen=b.id_konsumen left join detail_kiriman d on d.id_kiriman=a.id_kiriman left join m_kurir f on f.id_kurir=a.id_kurir
                    where a.id_kurir='$id_kurir' and (a.status=4 OR a.status=3) and b.nama IS NOT NULL
                     group by a.id_kiriman order by a.id_kiriman asc 
                    ";

        // echo $sql; die();
        $query = $this->db->query($sql);
        
        return $query->result();
    }

    function set_status($id_kurir,$status){

        $status    = $this->db->escape_str($status);
        $id_kurir    = $this->db->escape_str($id_kurir);
        
        $this->db->query("update m_kurir set status_toko = '$status' where id_kurir = '$id_kurir'");
    }

    function update_nama_toko($id_kurir,$nama){

        $id_kurir    = $this->db->escape_str($id_kurir);
        $nama    = $this->db->escape_str($nama);
        
        $this->db->query("update m_kurir set nama = '$nama' where id_kurir = '$id_kurir'");
    }


    function update_alamat_kurir($id_kurir,$nama, $no_telp, $alamat){

        $id_kurir    = $this->db->escape_str($id_kurir);
        $nama    = $this->db->escape_str($nama);
        $alamat    = $this->db->escape_str($alamat);
        $no_telp    = $this->db->escape_str($no_telp);
        
        $this->db->query("update m_kurir set nama = '$nama', alamat='$alamat', kontak='$no_telp', id_kurir='$no_telp' where id_kurir = '$id_kurir'");
        if($id_kurir != $no_telp)  $this->db->query("update m_pemilik_usaha set kontak_sms='$no_telp' where kontak_sms='$id_kurir'");
        // update 
    }

    function update_username($id_kurir,$username){

        $id_kurir    = $this->db->escape_str($id_kurir);
        $username    = $this->db->escape_str($username);
        
        $this->db->query("update m_kurir set username = '$username' where id_kurir = '$id_kurir'");
    }

    function update_jargon_toko($id_kurir,$jargon){

        $id_kurir    = $this->db->escape_str($id_kurir);
        $jargon    = $this->db->escape_str($jargon);
        
        $this->db->query("update m_kurir set jargon = '$jargon' where id_kurir = '$id_kurir'");
    }


    function update_no_telp_password_alamat_toko($id_kurir,$kontak, $kontak_sms, $kontak_sms2, $password, $alamat){

        $id_kurir    = $this->db->escape_str($id_kurir);
        $kontak    = $this->db->escape_str($kontak);
        $kontak_sms    = $this->db->escape_str($kontak_sms);
        $kontak_sms2    = $this->db->escape_str($kontak_sms2);
        $wherePemilik = "";
        if (strlen($password) > 0) {
            $password    = hash('sha512', $this->db->escape_str($password));
            $wherePemilik = ", password = '$password'";
        }
        $alamt    = $this->db->escape_str($alamat);
        
        $stmt = "update m_kurir set kontak = '$kontak', kontak_sms='$kontak_sms', kontak_sms2='$kontak_sms2'" . $wherePemilik . ", alamat = '$alamt' where id_kurir = '$id_kurir'";
        // echo $stmt;
        $this->db->query($stmt);
    }

    function update_radius($id_kurir,$radius, $lat, $lng) {

        $id_kurir    = $this->db->escape_str($id_kurir);
        $radius    = $this->db->escape_str($radius);
        $lat    = $this->db->escape_str($lat);
        $lng    = $this->db->escape_str($lng);
        
        $this->db->query("update m_kurir set radius = $radius, lat = '$lat', lng = '$lng' where id_kurir = '$id_kurir'");
    }

    function update_min_antar_fee($id_kurir, $min_antar, $fee) {

        $id_kurir    = $this->db->escape_str($id_kurir);
        $min_antar    = $this->db->escape_str($min_antar);
        $fee    = $this->db->escape_str($fee);
        
        $this->db->query("update m_kurir set min_antar=$min_antar, fee=$fee where id_kurir = '$id_kurir'");
    }

    function update_jenis($id_kurir,$id_jenis){

        $id_kurir    = $this->db->escape_str($id_kurir);
        $id_jenis    = $this->db->escape_str($id_jenis);
        
        $this->db->query("update m_kurir set id_jenis = $id_jenis where id_kurir = '$id_kurir'");
    }


    function update_min_antar($id_kurir,$min_antar){

        $id_kurir    = $this->db->escape_str($id_kurir);
        $min_antar    = $this->db->escape_str($min_antar);
        
        $this->db->query("update m_kurir set min_antar = $min_antar where id_kurir = '$id_kurir'");
    }

    function update_kategori_usaha($id_kurir,$json){

        $id_kurir    = $this->db->escape_str($id_kurir);

        $this->db->query("delete from p_detail_kategori where id_kurir='$id_kurir'");
        
        $arr = json_decode($json);

        for($i=0;$i<count($arr);$i++){

            $id_kategori_usaha  = $arr[$i]->id_kategori;

            $this->db->query("insert into p_detail_kategori (id_kurir,id_kategori) values ('$id_kurir','$id_kategori_usaha')");
        }
    }

    function update_kategori_usaha_awal($id_kurir,$json){

        $id_kurir    = $this->db->escape_str($id_kurir);

        $this->db->query("delete from p_detail_kategori where id_kurir='$id_kurir'");
        
        $arr = json_decode($json);

        for($i=0;$i<count($arr);$i++){

            $id_kategori_usaha  = $arr[$i]->id_kategori;
            $id_jenis  = $arr[$i]->id_jenis;

            $this->db->query("insert into p_detail_kategori (id_kurir,id_kategori) values ('$id_kurir','$id_kategori_usaha')");
            $this->db->query("update m_kurir set id_jenis = '$id_jenis' where id_kurir = '$id_kurir'");
        }
    }

    function get_jenis_kategori_usaha(){
        
        $query = $this->db->query("select * from m_jenis_kategori_usaha");
        
        return $query->result();
    }


    function get_detail_kiriman($id_kiriman, $tipe) {
        $id_kiriman    = $this->db->escape_str($id_kiriman);
        // echo $id_kiriman; die();

        $sql = "SELECT a.*, CONCAT('" . URL_IMAGE_KIRIMAN . "', a.foto_resi) as foto_resi from detail_kiriman a left join konsumen_kiriman b on a.id_kiriman=b.id_kiriman where a.id_kiriman='$id_kiriman'";

        if (intval($tipe) == 3) {
            $sql = "SELECT a.*, IF(a.foto_resi = '', a.foto_resi, CONCAT('" . URL_IMAGE_KIRIMAN . "', a.foto_resi)) as foto_resi, c.id_ekspedisi, c.nama as nama_layanan, d.nama as nama_ekspedisi, d.courier_slug, CONCAT('" . URL_IMAGE_EKSPEDISI_WEBADMIN . "', d.foto) as foto, e.id_agen, e.nama, e.lat, e.lng from detail_kiriman_ekspedisi a left join konsumen_kiriman b on a.id_kiriman=b.id_kiriman left join m_layanan_ekspedisi c on c.id_layanan_ekspedisi=a.id_layanan_ekspedisi left join m_ekspedisi d on d.id_ekspedisi=c.id_ekspedisi left join m_agen_ekspedisi e on e.id_agen=a.id_agen where a.id_kiriman='$id_kiriman' ORDER BY c.id_ekspedisi";
        }
        // echo $sql; die();

        $query = $this->db->query($sql);

        return $query->result();

    }


    function get_detail_order($id_order){

        $id_order    = $this->db->escape_str($id_order);
        $sql = "SELECT *, count(id_produk) id_produk_count from 

            (select a.*,b.nama_produk, b.foto, b.keterangan as keterangan_produk, d.alamat_antar, d.ket_perkiraan_antar as ket_konsumen, f.ket_konsumen as keterangan, d.lat_antar, d.lng_antar, d.status as order_status, nama_kategori, e.kontak, d.charge, d.include_fee, d.include_fee_note, 'umkm' as tipe_produk 
            from p_detail_produk_umkm b left join detail_order a on b.id_produk = a.id_produk left join m_kategori_produk c on b.id_kategori=c.id_kategori left join konsumen_order d on d.id_order=a.id_order left join m_konsumen e on e.id_konsumen=d.id_konsumen left join m_perkiraan_antar f on f.id=d.id_perkiraan_antar
            where  a.id_order='$id_order'
            UNION select a.*,b.nama_produk, b.foto, b.keterangan as keterangan_produk, d.alamat_antar, d.ket_perkiraan_antar as ket_konsumen, f.ket_konsumen as keterangan, d.lat_antar, d.lng_antar, d.status as order_status, nama_kategori, e.kontak, d.charge, d.include_fee, d.include_fee_note, 'promo' as tipe_produk 
            from p_detail_produk_promo b left join detail_order a on b.id_produk = a.id_produk left join m_kategori_produk c on b.id_kategori=c.id_kategori left join konsumen_order d on d.id_order=a.id_order left join m_konsumen e on e.id_konsumen=d.id_konsumen left join m_perkiraan_antar f on f.id=d.id_perkiraan_antar
            where  a.id_order='$id_order'
            UNION select a.*,b.nama_produk, b.foto, b.keterangan as keterangan_produk, d.alamat_antar, d.ket_perkiraan_antar as ket_konsumen, f.ket_konsumen as keterangan, d.lat_antar, d.lng_antar, d.status as order_status, nama_kategori, e.kontak, d.charge, d.include_fee, d.include_fee_note, 'regular' as tipe_produk
            from p_detail_produk b left join detail_order a on b.id_produk = a.id_produk left join m_kategori_produk c on b.id_kategori=c.id_kategori left join konsumen_order d on d.id_order=a.id_order left join m_konsumen e on e.id_konsumen=d.id_konsumen left join m_perkiraan_antar f on f.id=d.id_perkiraan_antar
            where  a.id_order='$id_order'
            UNION select a.*,b.nama_produk, b.foto, b.keterangan as keterangan_produk, d.alamat_antar, d.ket_perkiraan_antar as ket_konsumen, f.ket_konsumen as keterangan, d.lat_antar, d.lng_antar, d.status as order_status, nama_kategori, e.kontak, d.charge, d.include_fee, d.include_fee_note, 'histori' as tipe_produk 
            from histori_p_detail_produk b left join detail_order a on b.id_produk = a.id_produk left join m_kategori_produk c on b.id_kategori=c.id_kategori left join konsumen_order d on d.id_order=a.id_order left join m_konsumen e on e.id_konsumen=d.id_konsumen left join m_perkiraan_antar f on f.id=d.id_perkiraan_antar
            where  a.id_order='$id_order' ) T1

            GROUP BY id_produk ORDER BY id_produk
            ";
            // echo $sql; die();
            $query = $this->db->query($sql);
            $results = $query->result();
            if (count($results) > 0) {
                if ($results[0]->ket_konsumen == '') {
                    $results[0]->ket_konsumen = $results[0]->keterangan;
                }
                return $results;
            } 
        
        return $query->result();
    }

    function get_histori_detail_order($id_order){

        $id_order    = $this->db->escape_str($id_order);
        $stmt = "select a.*,b.harga_promo, b.nama_produk, b.foto, b.keterangan as keterangan_produk, d.alamat_antar, d.ket_perkiraan_antar, d.ket_alamat, d.lat_antar, d.lng_antar, d.status as order_status, nama_kategori, e.kontak, d.include_fee, d.include_fee_note, d.charge, d.fee 
            from histori_p_detail_produk b left join detail_order a on b.id_produk = a.id_produk left join m_kategori_produk c on b.id_kategori=c.id_kategori left join konsumen_order d on d.id_order=a.id_order left join m_konsumen e on e.id_konsumen=d.id_konsumen
            where  a.id_order='$id_order'";
            // echo $stmt; die();
        $query = $this->db->query($stmt);
        
        return $query->result();
    }

    function get_detail_order_promo($id_order){

        $id_order    = $this->db->escape_str($id_order);
        $stmt = "select a.*,b.harga_promo, b.nama_produk, b.foto, b.keterangan as keterangan_produk, d.alamat_antar, d.ket_perkiraan_antar, d.ket_alamat, d.lat_antar, d.lng_antar, d.status as order_status, nama_kategori, e.kontak, d.include_fee, d.include_fee_note, d.charge, d.fee 
            from p_detail_produk_promo b left join detail_order a on b.id_produk = a.id_produk left join m_kategori_produk c on b.id_kategori=c.id_kategori left join konsumen_order d on d.id_order=a.id_order left join m_konsumen e on e.id_konsumen=d.id_konsumen
            where  a.id_order='$id_order'";
            // echo $stmt; die();
        $query = $this->db->query($stmt);
        
        return $query->result();
    }

    function create_perkiraan_antar($pesan_perkiraan_antar){
        $this->db->query("insert into m_perkiraan_antar (nama, ket_konsumen, ket_pemilik, status) value ('$pesan_perkiraan_antar','$pesan_perkiraan_antar','$pesan_perkiraan_antar', 1)");
        return $this->db->insert_id();
    }

    function get_perkiraan_antar($id_perkiraan_antar){

        $id_perkiraan_antar    = $this->db->escape_str($id_perkiraan_antar);

        $query = $this->db->query("select * from m_perkiraan_antar where id='$id_perkiraan_antar' and status = 1");
        
        return $query->result();
    }

    function get_all_perkiraan_antar(){

        $query = $this->db->query("select * from m_perkiraan_antar where status = 1");
        
        return $query->result();
    }

    function get_kategori_usaha($id_kurir){

        $query = $this->db->query("select a.*,b.nama as induk_kategori from m_kategori_usaha a left join m_jenis_kategori_usaha b on a.id_jenis=b.id_jenis
            where a.status=1 and b.status=1 and a.id_jenis = (select id_jenis from m_kurir where id_kurir = '$id_kurir') order by a.nama_kategori");
        
        return $query->result();
    }

    function get_kategori_usaha_awal(){

        $query = $this->db->query("select a.*,b.nama as induk_kategori from m_kategori_usaha a,m_jenis_kategori_usaha b 
            where a.id_jenis=b.id_jenis and a.status=1 and b.status=1 order by a.nama_kategori");
        
        return $query->result();
    }

    function get_pemilik_kategori($id_kurir){

        $id_kurir    = $this->db->escape_str($id_kurir);
        
        $query = $this->db->query("select a.*,b.nama_kategori from p_detail_kategori a left join m_kategori_usaha b on  a.id_kategori = b.id_kategori where a.id_kurir='$id_kurir' ");
        
        return $query->result();
    }
    
    function get_pemilik_lokasi($id_kurir){

        $id_kurir    = $this->db->escape_str($id_kurir);
        
        $query = $this->db->query("select * from p_detail_lokasi where id_kurir='$id_kurir'");
        
        return $query->result();
    }
    
    function get_pemilik_produk($id_kurir){

        $id_kurir = $this->db->escape_str($id_kurir);
        
        $query = $this->db->query("select a.*,b.nama_kategori, 0 as is_official from p_detail_produk a left join m_kurir c on c.id_partner=a.id_kurir or c.id_kurir=a.id_kurir left join m_kategori_produk b on a.id_kategori=b.id_kategori where a.id_kurir='$id_kurir' AND a.nama_produk <> '' AND a.harga > 0 AND a.active=1 order by b.nama_kategori");
        
        // $sql = "SELECT a . * , b.nama_kategori,'1' as acuan
        // FROM p_detail_produk a, m_kategori_produk b
        // WHERE a.id_kategori = b.id_kategori
        // AND a.id_kurir = '$id_kurir'
        // AND b.id_kurir = '$id_kurir' AND a.status = '1' 
        // UNION
        // select '' as id_produk,'' as id_kurir,a.id_kategori,'' as nama_produk,0 as stok,'' as keterangan, 
        // 0 as harga, '1' as status, '' as foto, '' as versi, a.nama_kategori, '2' as acuan 
        // from m_kategori_produk a where a.id_kurir='$id_kurir' and a.status = '1' 
        // and 
        // a.id_kategori not in 
        // (select id_kategori from p_detail_produk where id_kurir='$id_kurir' and status = '1') group by a.nama_kategori ORDER BY acuan, nama_kategori";
        // $query = $this->db->query($sql);
        return $query->result();
    }


    function get_pemilik_produk_with_promo($id_kurir, $id_promos, $tipe){

        $id_kurir = $this->db->escape_str($id_kurir);
            $default_value = "null";
            if ($tipe == '2') {
                $default_value = "null";
            } else {
                $default_value = "a.harga_promo";
            }
        
        $sql = "select a.*,  " . $default_value . " as harga_promo, b.nama_kategori as nama_kategori, 0 as is_official from p_detail_produk a left join m_kurir c on c.id_partner=a.id_kurir or c.id_kurir=a.id_kurir left join m_kategori_produk b on a.id_kategori=b.id_kategori where a.id_kurir='$id_kurir' AND a.nama_produk <> '' AND a.harga > 0 AND a.active=1 ";

        foreach ($id_promos as $id_promo) {
            $sql = $sql . " UNION select a.*, a.harga_promo as harga_promo, b.nama_kategori as nama_kategori, 0 as is_official from p_detail_produk a left join m_kurir c on c.id_partner=a.id_kurir or c.id_kurir=a.id_kurir left join m_kategori_produk b on a.id_kategori=b.id_kategori left join produk_promo_diskon ppd on ppd.id_produk=REPLACE(a.id_produk, '".$id_kurir."_', '') where a.id_kurir='$id_kurir' AND a.nama_produk <> '' AND a.harga > 0 AND a.active=1 AND ppd.id_merchant_promo='".$id_promo."' ";
        }

        $sql = $sql . " order by nama_kategori";
        $query = $this->db->query($sql);
        
        return $query->result();
    }

    function get_pemilik_produk_by_pemilik($id_kurir){

        $id_kurir = $this->db->escape_str($id_kurir);
        
        $query = $this->db->query("select a.*, REPLACE(a.id_produk, '".$id_kurir."_', '') AS id_produk, b.nama_kategori from p_detail_produk a left join m_kurir c on c.id_partner=a.id_kurir or c.id_kurir=a.id_kurir left join m_kategori_produk b on a.id_kategori=b.id_kategori where a.id_kurir='$id_kurir' AND c.nama <> '' order by b.nama_kategori");
        
        // $sql = "SELECT a . * , b.nama_kategori,'1' as acuan
        // FROM p_detail_produk a, m_kategori_produk b
        // WHERE a.id_kategori = b.id_kategori
        // AND a.id_kurir = '$id_kurir'
        // AND b.id_kurir = '$id_kurir' AND a.status = '1' 
        // UNION
        // select '' as id_produk,'' as id_kurir,a.id_kategori,'' as nama_produk,0 as stok,'' as keterangan, 
        // 0 as harga, '1' as status, '' as foto, '' as versi, a.nama_kategori, '2' as acuan 
        // from m_kategori_produk a where a.id_kurir='$id_kurir' and a.status = '1' 
        // and 
        // a.id_kategori not in 
        // (select id_kategori from p_detail_produk where id_kurir='$id_kurir' and status = '1') group by a.nama_kategori ORDER BY acuan, nama_kategori";
        // $query = $this->db->query($sql);
        return $query->result();
    }

    function get_pemilik($id_kurir){

        $id_kurir    = $this->db->escape_str($id_kurir);
        
        // $query = $this->db->query("select a.*,b.tipe as tipe_usaha,c.nama_kategori 
        //     from m_kurir a,m_jenis_kategori_usaha b,m_kategori_usaha c 
        //     where a.id_jenis=b.id_jenis and c.id_kategori=a.id_kategori and
        //     a.id_kurir='$id_kurir'");
        $sql = "select a.*,b.tipe as tipe_usaha  
            from m_kurir a left join m_jenis_kategori_usaha b on (a.tipe=2 OR a.id_jenis=b.id_jenis)
            where a.id_kurir='$id_kurir'";
       // echo $sql; die();
       $stmt = $sql;
        $query = $this->db->query($stmt);

        return $query->result();
    }


    function get_toko($id_pemilik){

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        
        // $query = $this->db->query("select a.*,b.tipe as tipe_usaha,c.nama_kategori 
        //     from m_kurir a,m_jenis_kategori_usaha b,m_kategori_usaha c 
        //     where a.id_jenis=b.id_jenis and c.id_kategori=a.id_kategori and
        //     a.id_kurir='$id_kurir'");
        $sql = "select a.*,b.tipe as tipe_usaha  
            from m_pemilik_usaha a left join m_jenis_kategori_usaha b on (a.tipe=2 OR a.id_jenis=b.id_jenis)
            where a.id_pemilik='$id_pemilik'";
       // echo $sql; die();
       $stmt = $sql;
        $query = $this->db->query($stmt);

        return $query->result();
    }

    function get_preactive_pemilik($id_kurir){

        $id_kurir    = $this->db->escape_str($id_kurir);
        
        // $query = $this->db->query("select a.*,b.tipe as tipe_usaha,c.nama_kategori 
        //     from m_kurir a,m_jenis_kategori_usaha b,m_kategori_usaha c 
        //     where a.id_jenis=b.id_jenis and c.id_kategori=a.id_kategori and
        //     a.id_kurir='$id_kurir'");
       
        $query = $this->db->query("select a.*,b.tipe as tipe_usaha
            from m_kurir a,m_jenis_kategori_usaha b,
            where a.id_jenis=b.id_jenis and 
            a.id_kurir='$id_kurir'");

        return $query->row();
    }

    function get_preactive_produk($id_kurir){

        $id_kurir    = $this->db->escape_str($id_kurir);
        
        // $query = $this->db->query("select a.*,b.tipe as tipe_usaha,c.nama_kategori 
        //     from m_kurir a,m_jenis_kategori_usaha b,m_kategori_usaha c 
        //     where a.id_jenis=b.id_jenis and c.id_kategori=a.id_kategori and
        //     a.id_kurir='$id_kurir'");
       
        $query = $this->db->query("select * 
            from p_detail_produk 
            where  id_kurir='$id_kurir'");

        return $query->row();
    }

    function proses_order($id_order,$id_perkiraan_antar, $pesan_perkiraan_antar){
        
        $id_order   = $this->db->escape_str($id_order);
        $id_perkiraan_antar   = $this->db->escape_str($id_perkiraan_antar);
        $stmt = "update konsumen_order set `status`=2, tanggal_proses = '".date('Y-m-d H:i:s')."', `id_perkiraan_antar`='$id_perkiraan_antar', `ket_perkiraan_antar`='$pesan_perkiraan_antar' where `id_order`='".$id_order."'";
        $this->db->query($stmt);    

        return $stmt;
    }


    function proses_kiriman($id_kiriman,$id_perkiraan_antar, $pesan_perkiraan_antar){
        
        $id_kiriman   = $this->db->escape_str($id_kiriman);
        $stmt = "update konsumen_kiriman set `status`=2, tanggal_proses = '".date('Y-m-d H:i:s')."', `id_perkiraan_antar`='$id_perkiraan_antar', `ket_perkiraan_antar`='$pesan_perkiraan_antar' where `id_kiriman`='".$id_kiriman."'";
        $this->db->query($stmt);    

        return $stmt;
    }



    function selesai_kiriman($id_kiriman){
        
        $id_kiriman   = $this->db->escape_str($id_kiriman);
        $stmt = "update konsumen_kiriman set `status`=4, tanggal_proses = '".date('Y-m-d H:i:s')."' where `id_kiriman`='".$id_kiriman."'";
        $this->db->query($stmt);    

        return $stmt;
    }

    function selesai_order($id_order){
        
        // $id_order   = $this->db->escape_str($id_order);
        
        $stmt = "update konsumen_order set `status`=4 where `id_order`='" . $id_order ."'";
        $this->db->query($stmt);
        $fee = "";
        $id_kurir = "";
        
        $query = $this->db->query("select * from `konsumen_order` where `id_order` = '". $id_order . "'")->first_row();
        
        $fee = $query->fee;
        //$id_kurir = $query->id_kurir;

        
        //$this->db->query("update `m_kurir` set `kredit` = `kredit` - " . $fee . " where `id_kurir` = '".$id_kurir."'"); 

        return $fee;       
    }

     function get_provinsi(){
        $query = $this->db->query("select * from m_provinsi where status=1 order by nama");
        return $query->result();
    }

    function get_kota($id_provinsi){
        $id_provinsi    = $this->db->escape_str($id_provinsi);
        
        $query = $this->db->query("select * from m_kota where id_provinsi = '$id_provinsi' and status=1 order by nama");
        return $query->result();
    }

    function daftar($id_kurir,$kontak,$password,$username,$kode,$id_provinsi,$id_kota, $radius){

        $id_kurir = $this->db->escape_str($id_kurir);
        $kontak     = $this->db->escape_str($kontak);
        $password       = hash("sha512",$password);
        $username       = $this->db->escape_str($username);
        $kode           = $this->db->escape_str($kode);
        $radius      = $this->db->escape_str($radius);
        $id_provinsi    = $this->db->escape_str($id_provinsi);
        $id_kota        = $this->db->escape_str($id_kota);
        $lat = "-6.2293866";
        $lng = "106.6890876";
        
        $sql_stmt = "INSERT INTO `m_kurir`(`id_kurir`, `username`, `email`, `kontak`, `password`, `nama`, `jargon`, `alamat`, 
            `lat`, `lng`, `radius`, `foto`, `kode`, `tanggal_update`, `token`, `id_kategori`,id_provinsi,id_kota,level,id_jenis,tipe,kredit, waktu_buka, waktu_tutup) 
            select '$id_kurir','$username','','$kontak','$password','','','','$lat','$lng','$radius','','$kode',now(),'','0','$id_provinsi','$id_kota',1,1,1,default_kredit, '08:00:00', '20:00:00'
            from setting_umum where id = '1'";
		$this->db->query($sql_stmt);
		$affected_rows = $this->db->affected_rows();

        $this->db->query("delete from kode_sms where kode = '$kode'");
        
        return $affected_rows;
    }


    function daftar_pre($id_kurir,$kontak,$password,$username,$kode,$id_provinsi,$id_kota, $radius){

        $id_kurir = $this->db->escape_str($id_kurir);
        $kontak     = $this->db->escape_str($kontak);
        $password       = hash("sha512",$password);
        $username       = $this->db->escape_str($username);
        $kode           = $this->db->escape_str($kode);
        $radius      = $this->db->escape_str($radius);
        $id_provinsi    = $this->db->escape_str($id_provinsi);
        $id_kota        = $this->db->escape_str($id_kota);
        $lat = "-6.2293866";
        $lng = "106.6890876";
        
        $sql_stmt = "INSERT INTO `m_kurir`(`id_kurir`, `username`, `email`, `kontak`, `password`, `nama`, `jargon`, `alamat`, 
            `lat`, `lng`, `radius`, `foto`, `kode`, `tanggal_update`, `token`, `id_kategori`, `status`,id_provinsi,id_kota,level,id_jenis,tipe,kredit) 
            select '$id_kurir','$username','','$kontak','$password','','','','$lat','$lng','$radius','','$kode',now(),'','0', '0', '$id_provinsi','$id_kota',1,0,1,default_kredit 
            from setting_umum where id = '1'";
        $this->db->query($sql_stmt);
        $affected_rows = $this->db->affected_rows();

        $this->db->query("delete from kode_sms where kode = '$kode'");
        
        return $affected_rows;
    }


    function tambah_toko_preaktif($id_kurir,$id_partner,$kontak,$kontak_sms, $kontak_sms2, $password,$username,$kode,$id_provinsi,$id_kota, $radius, $min_antar, $fee, $email){

        $id_kurir = $this->db->escape_str($id_kurir);
        $id_partner = $this->db->escape_str($id_partner);
        $kontak     = $this->db->escape_str($kontak);
        $kontak_sms     = $this->db->escape_str($kontak_sms);
        $kontak_sms2     = $this->db->escape_str($kontak_sms2);
        $password       = hash("sha512",$password);
        $username       = $this->db->escape_str($username);
        $kode           = $this->db->escape_str($kode);
        $radius      = $this->db->escape_str($radius);
        $min_antar      = $this->db->escape_str($min_antar);
        $id_provinsi    = $this->db->escape_str($id_provinsi);
        $id_kota        = $this->db->escape_str($id_kota);
        $email        = $this->db->escape_str($email);
        $lat = "-6.2293866";
        $lng = "106.6890876";
        
        $sql_stmt = "INSERT INTO `m_kurir`(`id_kurir`, `id_partner`, `username`, `email`, `kontak`, `kontak_sms`, `kontak_sms2`, `password`, `nama`, `jargon`, `alamat`, 
            `lat`, `lng`, `radius`, `foto`, `kode`, `tanggal_update`, `token`, `id_kategori`, `status` ,id_provinsi,id_kota,level,id_jenis,tipe,kredit, min_antar, `fee`) 
            select '$id_kurir', '$id_partner', '$username','$email','$kontak','$kontak_sms','$kontak_sms2','$password','','','','$lat','$lng','$radius','','$kode',now(),'','0', 0, '$id_provinsi','$id_kota',1,1,2,default_kredit, $min_antar, $fee
            from setting_umum where id = '1'";
        $this->db->query($sql_stmt);
        $affected_rows = $this->db->affected_rows();

        $this->db->query("delete from kode_sms where kode = '$kode'");
        
        return $affected_rows;
    }


    function tambah_toko($id_kurir,$id_partner,$kontak,$kontak_sms, $kontak_sms2, $password,$username,$kode,$id_provinsi,$id_kota, $radius, $min_antar, $fee, $nama=''){

        $id_kurir = $this->db->escape_str($id_kurir);
        $id_partner = $this->db->escape_str($id_partner);
        $nama = $this->db->escape_str($nama);
        $kontak     = $this->db->escape_str($kontak);
        $kontak_sms     = $this->db->escape_str($kontak_sms);
        $kontak_sms2     = $this->db->escape_str($kontak_sms2);
        $password       = hash("sha512",$password);
        $username       = $this->db->escape_str($username);
        $kode           = $this->db->escape_str($kode);
        $radius      = $this->db->escape_str($radius);
        $min_antar      = $this->db->escape_str($min_antar);
        $id_provinsi    = $this->db->escape_str($id_provinsi);
        $id_kota        = $this->db->escape_str($id_kota);
        $lat = "-6.2293866";
        $lng = "106.6890876";
        
        $sql_stmt = "INSERT INTO `m_kurir`(`id_kurir`, `id_partner`, `username`, `email`, `kontak`, `kontak_sms`, `kontak_sms2`, `password`, `nama`, `jargon`, `alamat`, 
            `lat`, `lng`, `radius`, `foto`, `kode`, `tanggal_update`, `token`, `id_kategori`, `status`,id_provinsi,id_kota,level,id_jenis,tipe,kredit, min_antar, `fee`, waktu_buka, waktu_tutup) 
            select '$id_kurir', '$id_partner', '$username','','$kontak','$kontak_sms','$kontak_sms2','$password','$nama','','','$lat','$lng','$radius','','$kode',now(),'','0', '1', '$id_provinsi','$id_kota',1,1,2,default_kredit, 0, 0, '08:00:00', '20:00:00' 
            from setting_umum where id = '1'";

            // echo $sql_stmt; die();
        $this->db->query($sql_stmt);
        $affected_rows = $this->db->affected_rows();

        $this->db->query("delete from kode_sms where kode = '$kode'");
        
        return $affected_rows;
    }

    function tambah_toko_batch($id_kurir,$id_partner,$kontak,$kontak_sms, $kontak_sms2, $password,$username,$kode,$id_provinsi,$id_kota, $radius, $min_antar, $fee){

        $id_kurir = $this->db->escape_str($id_kurir);
        $id_partner = $this->db->escape_str($id_partner);
        $kontak     = $this->db->escape_str($kontak);
        $kontak_sms     = $this->db->escape_str($kontak_sms);
        $kontak_sms2     = $this->db->escape_str($kontak_sms2);
        $password       = hash("sha512",$password);
        $username       = $this->db->escape_str($username);
        $kode           = $this->db->escape_str($kode);
        $radius      = $this->db->escape_str($radius);
        $min_antar      = $this->db->escape_str($min_antar);
        $id_provinsi    = $this->db->escape_str($id_provinsi);
        $id_kota        = $this->db->escape_str($id_kota);
        $lat = "-6.2293866";
        $lng = "106.6890876";
        
        $sql_stmt = "INSERT INTO `m_kurir`(`id_kurir`, `id_partner`, `username`, `email`, `kontak`, `kontak_sms`, `kontak_sms2`, `password`, `nama`, `jargon`, `alamat`, 
            `lat`, `lng`, `radius`, `foto`, `kode`, `tanggal_update`, `token`, `id_kategori`, `status`,id_provinsi,id_kota,level,id_jenis,tipe,kredit, min_antar, `fee`, waktu_buka, waktu_tutup) 
            select '$id_kurir', '$id_partner', '$username','','$kontak','$kontak_sms','$kontak_sms2','$password','','','','$lat','$lng','$radius','','$kode',now(),'','0', '1', '$id_provinsi','$id_kota',1,1,3,default_kredit, $min_antar, $fee, '08:00:00', '20:00:00' 
            from setting_umum where id = '1'";

            // echo $sql_stmt; die();
        $this->db->query($sql_stmt);
        $affected_rows = $this->db->affected_rows();

        $this->db->query("delete from kode_sms where kode = '$kode'");
        
        return $affected_rows;
    }


    function insert_pemilik($id_kurir,$id_jenis, $kontak,$password,$username,$nama, $jargon, $alamat, $kode, $id_provinsi, $id_kota, $id_kategori, $radius, $lat, $lng) {

        $id_kurir = $this->db->escape_str($id_kurir);
        $kontak     = $this->db->escape_str($kontak);
        $password       = hash("sha512",$password);
        $username       = $this->db->escape_str($username);
        $jargon       = $this->db->escape_str($jargon);
        $kode           = $this->db->escape_str($kode);
        $radius      = $this->db->escape_str($radius);
        $id_provinsi    = $this->db->escape_str($id_provinsi);
        $id_kota        = $this->db->escape_str($id_kota);
        $alamat       = $this->db->escape_str($alamat);
        
        $sql_stmt = "INSERT INTO `m_kurir`(`id_kurir`, `username`, `email`, `kontak`, `password`, `nama`, `jargon`, `alamat`, 
            `lat`, `lng`, `radius`, `foto`, `kode`, `tanggal_update`, `token`, `id_kategori`, `status`,id_provinsi,id_kota,level,`id_jenis`,tipe,kredit) 
            select '$id_kurir','$username','','$kontak','$password','$nama','$jargon','$alamat','$lat','$lng','$radius','','$kode',now(),'', '0', '0','$id_provinsi','$id_kota',1,$id_jenis,1,default_kredit 
            from setting_umum where id = '1'";

        $this->db->query($sql_stmt);
        
        return $this->db->affected_rows() == 1;
    }

    function isi_profil_awal($id_kurir,$nama,$alamat,$jargon, $radius, $kontak_sms, $kontak_sms2, $fee, $include_fee='1', $include_fee_note=''){

        $id_kurir    = $this->db->escape_str($id_kurir);
        $nama    = $this->db->escape_str($nama);
        $alamat    = $this->db->escape_str($alamat);
        $jargon    = $this->db->escape_str($jargon);
        $fee    = $this->db->escape_str($fee);
        $radius    = $this->db->escape_str($radius);
        $include_fee    = $this->db->escape_str($include_fee);
        $include_fee_note    = $this->db->escape_str($include_fee_note);
        
        $this->db->query("update m_kurir set nama = '$nama', alamat = '$alamat', jargon = '$jargon', radius='$radius', kontak_sms='$kontak_sms', kontak_sms2='$kontak_sms2', fee=$fee, include_fee='$include_fee', include_fee_note='$include_fee_note' where id_kurir='$id_kurir'");
    }


    function update_profile($id_kurir, $nama, $alamat, $kontak, $fee, $include_fee='1', $include_fee_note=''){

        $id_kurir    = $this->db->escape_str($id_kurir);
        $nama    = $this->db->escape_str($nama);
        $alamat    = $this->db->escape_str($alamat);
        $kontak    = $this->db->escape_str($kontak);
        $fee    = $this->db->escape_str($fee);
        $include_fee    = $this->db->escape_str($include_fee);
        $include_fee_note    = $this->db->escape_str($include_fee_note);

        $stmt = "update m_kurir set nama = '$nama', alamat = '$alamat', kontak='$kontak', fee=$fee, include_fee='$include_fee', include_fee_note='$include_fee_note' where id_kurir='$id_kurir'";
        // echo $stmt; die();
        
        $this->db->query($stmt);
    }


    function tambah_ongkirs($id_kurir, $ongkirs) {
        $this->db->query("delete from m_ongkir_kurir where id_kurir='" . $id_kurir ."'");
        foreach ($ongkirs as $ongkir) {
            $this->db->query("insert into m_ongkir_kurir (id_kurir, jarak_min, jarak_max, fee) VALUE('" . $id_kurir . "', " 
                . $ongkir->radius_min . ", " . $ongkir->radius_max . ", " . $ongkir->fee . ")");
        }
    }


    function set_alamat_min_antar($id_kurir,$alamat,$min_antar){

        $id_kurir    = $this->db->escape_str($id_kurir);
        $min_antar    = $this->db->escape_str($min_antar);
        $alamat    = $this->db->escape_str($alamat);
        
        $this->db->query("update m_kurir set alamat = '$alamat', min_antar='$min_antar' where id_kurir = '$id_kurir'");
    }

    function set_fee($id_kurir, $fee){

        $id_kurir    = $this->db->escape_str($id_kurir);
        $fee    = $this->db->escape_str($fee);
        
        $this->db->query("update m_kurir set fee=$fee where id_kurir = '$id_kurir'");
    }

    function get_kurir_fee($id_kurir){

        $id_kurir    = $this->db->escape_str($id_kurir);
        
        $result = $this->db->query("select fee from m_kurir where id_kurir = '$id_kurir' limit 1");
        return $result->first_row();
    }

    function set_radius($id_kurir, $radius){

        $id_kurir    = $this->db->escape_str($id_kurir);
        $radius    = $this->db->escape_str($radius);
        
        $this->db->query("update m_kurir set radius='$radius' where id_kurir = '$id_kurir'");
    }

     function set_no_telp($id_kurir, $kontak, $kontak_sms, $kontak_sms2) {

        $id_kurir    = $this->db->escape_str($id_kurir);
        $kontak    = $this->db->escape_str($kontak);
        $kontak_sms    = $this->db->escape_str($kontak_sms);
        $kontak_sms2    = $this->db->escape_str($kontak_sms2);

        $this->db->query("update m_kurir set kontak='$kontak', kontak_sms='$kontak_sms', kontak_sms2='$kontak_sms2'  where id_kurir = '$id_kurir'");
     }

    function isi_lokasi_awal($id_kurir,$lat,$lng,$nama_lokasi){

        $id_kurir    = $this->db->escape_str($id_kurir);
        $lat    = $this->db->escape_str($lat);
        $lng    = $this->db->escape_str($lng);
        $nama_lokasi    = $this->db->escape_str($nama_lokasi);
        
        $this->db->query("update m_kurir set lat = '$lat',lng = '$lng' where id_kurir = '$id_kurir'");

        $lokasi = $this->db->query("select * from p_detail_lokasi where id_kurir='$id_kurir' LIMIT 1")->row();

        if (empty($lokasi)) {
            $id_lokasi = date("YmdHis").$id_kurir;
            $this->db->query("insert into p_detail_lokasi (id_lokasi,id_kurir,nama,lat,lng,jenis_lokasi) values ('$id_lokasi','$id_kurir','$nama_lokasi','$lat','$lng','1')");
        } else {
            $this->db->query("update p_detail_lokasi set id_kurir='$id_kurir', nama='$nama_lokasi', lat='$lat',lng='$lng', jenis_lokasi='1' where id_kurir='$id_kurir'");
        }
    }

    function get_id_kurir_by_no_telp_and_username($no_telp, $username){
        
        $no_telp   = $this->db->escape_str($no_telp);
        $username   = $this->db->escape_str($username);

        $query = $this->db->query("select * from m_kurir where kontak = '$no_telp' or username = '$username'");
        return $query->first_row();
    }

    function get_id_kurir_by_no_telp($no_telp){
        
        $no_telp   = $this->db->escape_str($no_telp);

        $query = $this->db->query("select * from m_kurir where kontak = '$no_telp'");
        return $query->first_row();
    }

    function new_password($id_kurir,$password){

        $id_kurir    = $this->db->escape_str($id_kurir);
        $password   = hash("sha512",$password);
       
        $this->db->query("update m_kurir set password = '$password' where id_kurir = '$id_kurir'");
    }

    function check_password($id_kurir,$password){
        $id_kurir    = $this->db->escape_str($id_kurir);
        $password   = hash("sha512",$password);

        return $this->db->query("select * from m_kurir where id_kurir = '$id_kurir' and password = '$password'")->row();
    }

    function copy_to_histori($id_kurir){
        
        $id_kurir   = $this->db->escape_str($id_kurir);

        $this->db->query("INSERT INTO `histori_m_kurir` SELECT * FROM `m_kurir` WHERE id_kurir = '$id_kurir'");
    }

    function hapus_akun($id_kurir){

        $this->db->query("DELETE FROM `m_kurir` WHERE id_kurir = '$id_kurir'");
    }

    function get_penjualan_harian($id_kurir,$tanggal){
        $query = $this->db->query("select a.id_order,a.tanggal_order,sum(a.fee) as fee, sum(a.charge) as charge,a.id_kurir,b.nama_kategori,
        COALESCE(f.nama_produk, COALESCE(c.nama_produk, e.nama_produk)) as nama_produk, COALESCE(f.id_produk, COALESCE(c.id_produk, e.id_produk)) as id_produk,  COALESCE(f.keterangan, COALESCE(c.keterangan, e.keterangan)) as keterangan, COALESCE(f.foto, COALESCE(c.foto, e.foto)) as foto,
                    sum(d.kuantitas) as total_kuantitas,sum(d.subtotal) as subtotal
                    from
                    konsumen_order a 
                    left join detail_order d on a.id_order = d.id_order 
                    left join p_detail_produk c on c.id_produk = d.id_produk 
                    left join histori_p_detail_produk e on e.id_produk = d.id_produk 
                    left join p_detail_produk_promo f on f.id_produk = d.id_produk 
                    left join m_kategori_produk b on (b.id_kategori = c.id_kategori or b.id_kategori = e.id_kategori)
                    where
                    DATE(a.tanggal_order) = '$tanggal'
                    and
                    a.id_kurir = '$id_kurir'
                    and
                    a.status = 4
                    group by c.id_produk
                    order by b.nama_kategori");
        return $query->result();
    }
    
    function get_penjualan_bulanan($id_kurir,$bulan,$tahun){
        $stmt = "select a.id_order,a.tanggal_order,sum(a.fee) as fee, sum(a.charge) as charge,a.id_kurir,b.nama_kategori, COALESCE(f.nama_produk, COALESCE(c.nama_produk, e.nama_produk)) as nama_produk, COALESCE(f.id_produk, COALESCE(c.id_produk, e.id_produk)) as id_produk,  COALESCE(f.keterangan, COALESCE(c.keterangan, e.keterangan)) as keterangan,  COALESCE(f.foto, COALESCE(c.foto, e.foto)) as foto,
                    sum(d.kuantitas) as total_kuantitas,sum(d.subtotal) as subtotal
                    from
                    konsumen_order a 
                    left join detail_order d on a.id_order = d.id_order 
                    left join p_detail_produk c on c.id_produk = d.id_produk 
                    left join histori_p_detail_produk e on e.id_produk = d.id_produk 
                    left join p_detail_produk_promo f on f.id_produk = d.id_produk 
                    left join m_kategori_produk b on (b.id_kategori = c.id_kategori or b.id_kategori = e.id_kategori)
                    where MONTH(a.tanggal_order) = '$bulan' and YEAR(a.tanggal_order) = '$tahun'
                    and
                    a.id_kurir = '$id_kurir'
                    and
                    a.status = 4
                    group by c.id_produk
                    order by b.nama_kategori";
                    // echo $stmt;die();
        $query = $this->db->query($stmt);
        return $query->result();
    }
    
    function get_penjualan_tahunan($id_kurir,$tahun){
        $stmt = "select a.id_order,a.tanggal_order,sum(a.fee) as fee, sum(a.charge) as charge,a.id_kurir,b.nama_kategori,
        COALESCE(f.nama_produk, COALESCE(c.nama_produk, e.nama_produk)) as nama_produk, COALESCE(f.id_produk, COALESCE(c.id_produk, e.id_produk)) as id_produk,  COALESCE(f.keterangan, COALESCE(c.keterangan, e.keterangan)) as keterangan,  COALESCE(f.foto, COALESCE(c.foto, e.foto)) as foto,
                    sum(d.kuantitas) as total_kuantitas,sum(d.subtotal) as subtotal
                    from
                    konsumen_order a 
                    left join detail_order d on a.id_order = d.id_order 
                    left join p_detail_produk c on c.id_produk = d.id_produk 
                    left join histori_p_detail_produk e on e.id_produk = d.id_produk 
                    left join p_detail_produk_promo f on f.id_produk = d.id_produk 
                    left join m_kategori_produk b on (b.id_kategori = c.id_kategori or b.id_kategori = e.id_kategori)
                    where
                    YEAR(a.tanggal_order) = '$tahun'
                    and
                    a.id_kurir = '$id_kurir'
                    and
                    a.status = 4
                    group by d.id_produk
                    order by b.nama_kategori";
                    // echo $stmt; die();
        $query = $this->db->query($stmt);

        
        
        return $query->result();
    }

    function set_foto($id_kurir,$foto){
        
        $id_kurir   = $this->db->escape_str($id_kurir);
        $foto   = $this->db->escape_str($foto);

        $this->db->query("update m_kurir set foto = '$foto', versi = versi+1 where id_kurir = '$id_kurir'");
    }



    function set_foto_resi($id,$foto,$tipe){
        
        $id   = $this->db->escape_str($id);
        $foto   = $this->db->escape_str($foto);
        $tipe   = $this->db->escape_str($tipe);

        if (intval($tipe) != 3) { // kalo bukan ekspedisi
            $this->db->query("update detail_kiriman set foto_resi = '$foto' where id = '$id'");
        } else {
            $this->db->query("update detail_kiriman_ekspedisi set foto_resi = '$foto' where id = '$id'");
        }
    }

    function set_foto_produk($id_produk,$foto){
        
        $id_produk   = $this->db->escape_str($id_produk);
        $foto   = $this->db->escape_str($foto);

        $this->db->query("update p_detail_produk set foto = '$foto', versi = versi+1 where id_produk = '$id_produk'");
    }

    function set_foto_avatar($id_kurir,$foto){
        
        $id_kurir   = $this->db->escape_str($id_kurir);
        $foto   = $this->db->escape_str($foto);

        $this->db->query("update m_kurir set avatar = '$foto', versi_avatar = versi_avatar+1 where id_kurir = '$id_kurir'");
    }

    function get_foto($id_kurir){
        
        $id_kurir   = $this->db->escape_str($id_kurir);

        $query = $this->db->query("select foto,versi from m_kurir where id_kurir = '$id_kurir'");

        return $query->first_row();
    }


    function get_foto_resi($id, $tipe){
        
        $id   = $this->db->escape_str($id);
        $tipe   = $this->db->escape_str($tipe);

        if (intval($tipe) != 3) {
            $query = $this->db->query("select CONCAT('" . URL_IMAGE_KIRIMAN . "', foto_resi) as foto_resi from detail_kiriman where id = '$id'");
        } else {
            $query = $this->db->query("select CONCAT('" . URL_IMAGE_KIRIMAN . "', foto_resi) as foto_resi from detail_kiriman_ekspedisi where id = '$id'");
        }

        return $query->first_row();
    }

    function get_foto_avatar($id_kurir){
        
        $id_kurir   = $this->db->escape_str($id_kurir);

        $query = $this->db->query("select avatar,versi_avatar from m_kurir where id_kurir = '$id_kurir'");

        return $query->first_row();
    }

    function get_foto_produk($id_produk){
        
        $id_kurir   = $this->db->escape_str($id_produk);

        $query = $this->db->query("select foto,versi from p_detail_produk where id_produk = '$id_produk'");

        return $query->first_row();
    }

    function get_produk_by_id($id_produk){
        
        $id_kurir   = $this->db->escape_str($id_produk);

        $query = $this->db->query("select * from p_detail_produk where id_produk = '$id_produk'");

        return $query->first_row();
    }

    function get_produk_umkm_by_id($id_produk){
        
        $id_kurir   = $this->db->escape_str($id_produk);

        $query = $this->db->query("select * from p_detail_produk_umkm where id_produk = '$id_produk'");

        return $query->first_row();
    }

    function notif_get_pemilik($id_kurir){
        
        $query = $this->db->query("select * from m_kurir where id_kurir = '$id_kurir'");
        return $query->result();
    }


    function notif_get_kiriman($id_kiriman){
        $sql = "select a.*, CONCAT('" . URL_IMAGE_KONSUMEN . "', b.foto) as foto,b.nama,b.kontak,b.lat,b.lng,b.versi
                    from konsumen_kiriman a left join m_konsumen b on a.id_konsumen=b.id_konsumen left join detail_kiriman d on d.id_kiriman=a.id_kiriman left join m_kurir f on f.id_kurir=a.id_kurir
                where a.id_kiriman = '$id_kiriman'";
        // echo $sql; die();
        
        $query = $this->db->query($sql);
        return $query->first_row();
    }


    function notif_get_konsumen($id_order){
        
        $query = $this->db->query("select a.* 
            from m_konsumen a, konsumen_order b
            where a.id_konsumen = b.id_konsumen and b.id_order = '$id_order'");
        return $query->result();
    }

    function notif_get_konsumen_kiriman($id_kiriman){
        
        $query = $this->db->query("select a.* 
            from m_konsumen a left join konsumen_kiriman b on a.id_konsumen = b.id_konsumen
            where b.id_kiriman = '$id_kiriman'");
        return $query->first_row();
    }

    function notif_get_order($id_order){
        $query = $this->db->query("select ko.id_order, ko.total, ko.status, ko.id_perkiraan_antar, ko.tanggal_order
            from konsumen_order ko
            where ko.id_order = '$id_order'");
        return $query->result();
    }

    function update_token($id_kurir,$token){
        $id_kurir   = $this->db->escape_str($id_kurir);
        $token   = $this->db->escape_str($token);
        
        $query = $this->db->query("update m_kurir set token='$token' where id_kurir='$id_kurir'");
    }

    function get_order_dari_notif($id_order){
        
        $id_order    = $this->db->escape_str($id_order);
        
        $query = $this->db->query("select a.*,b.nama,b.alamat,b.kontak,b.lat,b.lng,b.foto,b.versi, c.nama as nama_kota, e.nama as nama_merchant, e.lat as lat_merchant, e.lng as lng_merchant, e.alamat as alamatmerchant, e.foto as fotomerchant 
                    from konsumen_order a left join m_pemilik_usaha e ON a.id_pemilik=e.id_pemilik left join m_konsumen b ON a.id_konsumen=b.id_konsumen left join m_kota c ON c.id = b.id_kota 
                    where a.id_order='$id_order' 
                    order by a.status asc 
                    ");
        
        return $query->result();
    }

    function get_konsumen_order($id_order){
        
        $id_order   = $this->db->escape_str($id_order);

        $query = $this->db->query("select * from konsumen_order where id_order='$id_order' LIMIT 1")->row();

        // echo $this->db->last_query(); die();

        return $query;
    }


    function get_konsumen_kiriman($id_kiriman){
        
        $id_kiriman   = $this->db->escape_str($id_kiriman);

        $query = $this->db->query("select * from konsumen_kiriman where id_kiriman='$id_kiriman' LIMIT 1")->row();

        // echo $this->db->last_query(); die();

        return $query;
    }


    function update_provinsi_kota($id_konsumen,$id_provinsi,$id_kota){

        $id_konsumen    = $this->db->escape_str($id_konsumen);
        $id_provinsi    = $this->db->escape_str($id_provinsi);
        $id_kota    = $this->db->escape_str($id_kota);
       
        $this->db->query("update m_kurir set id_provinsi = '$id_provinsi', id_kota = '$id_kota' where id_kurir = '$id_konsumen'");
    }

    
    function update_kategori_produk($id_kategori,$nama_kategori){        

        $id_kategori    = $this->db->escape_str($id_kategori);
        $nama_kategori    = $this->db->escape_str($nama_kategori);

        $query   = "update m_kategori_produk set nama_kategori = '$nama_kategori' 
                            where id_kategori = '$id_kategori'";
        
        $this->db->query($query);
    }

    function delete_kategori_produk($id_kategori){

        $id_kategori    = $this->db->escape_str($id_kategori);
        
        $this->db->query("update m_kategori_produk set status = '2' where id_kategori = '$id_kategori'");        
        $this->db->query("update p_detail_produk set status = '2' where id_kategori = '$id_kategori'");
    }

    // function tambah_ongkirs($id_kurir, $ongkirs) {
    //     $this->db->query("delete from m_ongkir where id_kurir='" . $id_kurir ."'");
    //     foreach ($ongkirs as $ongkir) {
    //         $this->db->query("insert into m_ongkir (id_kurir, jarak_min, jarak_max, fee) VALUE('" . $id_kurir . "', " 
    //             . $ongkir->radius_min . ", " . $ongkir->radius_max . ", " . $ongkir->fee . ")");
    //     }
    // }


    function get_ongkirs($id_kurir) {
        return $this->db->query("select * from m_ongkir_kurir where id_kurir='" . $id_kurir . "'")->result();
    }

    function insert_produk_promo_batch($id_kurir, $rows){    

        $query_produk   = "INSERT INTO `m_promo_produk` (`id_merchant_promo`, `id_barcode`, `qty`, `id_barcode2`, `qty2`) VALUES ";

        // echo $k; die();

        $i = 0;
        foreach ($rows as $produk) {
                $j=0;

                $paramBuilder = "";

                $paramBuilder = $paramBuilder . "'".$produk->barcode . "', ";
                $paramBuilder = $paramBuilder . "'".$produk->min_qty . "', ";
                $paramBuilder = $paramBuilder . "'".$produk->barcode_barang_promo . "', ";
                $paramBuilder = $paramBuilder . "'".$produk->qty_promo . "'";

                $query_produk = $query_produk . "(";
                $query_produk = $query_produk . "'".$id_kurir . "', ";
                $query_produk = $query_produk . $paramBuilder . ")";

            $i++;
        }
        $query_produk = str_replace(")(", "),(", $query_produk);

        // echo $query_produk; die();
        
        $this->db->query($query_produk);
    }


    function set_harga_promo_batch($id_kurir, $rows, $id_merchant_promo) { 

        $insert = "INSERT INTO p_detail_produk (id_produk, id_kurir, nama_produk, id_kategori, keterangan, harga, harga_promo, min_antar, active) VALUES ";
        $update = "ON DUPLICATE KEY UPDATE harga_promo=VALUES(harga_promo), min_antar=VALUES(min_antar), active=1";

        foreach ($rows as $row) {
                $insert = $insert . "('" . $id_kurir . "_" . $row->barcode . "', '" . $id_kurir . "', '', 1, '', 0, " . $row->harga_promo . ", " . $row->min_qty . ", 1), ";
            // $i++;
        }
        $stmt = $insert . $update;
        $stmt = str_replace("), ON DUPLICATE KEY UPDATE", ") ON DUPLICATE KEY UPDATE", $stmt);

        // echo $stmt; die();
        $this->db->query($stmt);
        
    }

    function insert_produk_promo_diskon_batch($rows, $id_merchant_promo) { 
       
        $update2 = "XXX";

        $insert2 = "INSERT INTO produk_promo_diskon (id_produk, id_merchant_promo) VALUES ";
        foreach ($rows as $row) {
            $insert2 = $insert2 . "('" . $row->barcode . "', " . $id_merchant_promo . "), ";
            // $i++;
        }

        $stmt2 = $insert2 . $update2;
        $stmt2 = str_replace("), XXX", ")", $stmt2);
        // echo $stmt; die();
        $this->db->query($stmt2);
        
    }

    function update_waktu_buka_tutup($id_kurir, $waktu_buka, $waktu_tutup) {


        $id_kurir    = $this->db->escape_str($id_kurir);
        $waktu_buka = $this->db->escape_str($waktu_buka);
        $waktu_tutup = $this->db->escape_str($waktu_tutup);
        
        $sql = "update m_kurir set waktu_buka = '$waktu_buka', waktu_tutup='$waktu_tutup' where id_kurir = '$id_kurir'";
        // echo $sql; die();
        $this->db->query($sql);      
    }

}