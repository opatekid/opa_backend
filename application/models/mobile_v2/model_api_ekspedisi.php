<?php  
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_api_ekspedisi extends CI_Model 
{

    function __construct()
    {
            parent::__construct();
            $this->load->database();
    }

    function kirim_kode($id_kode,$kode){
       $this->db->query("INSERT INTO `kode_sms`(`id`, `kode`) VALUES('$id_kode','$kode')"); 
    }

    function cek_no_telp($no_telp){
        $sub = substr($no_telp, 0, 3);
        $sub2 = substr($no_telp, 3);
        // str_replace(search, replace, subject)
        $sub = str_replace("628", "08", $sub);
        //$no_telp = $sub . $sub2;
        
        $query = $this->db->query("select count(*) as total from m_ekspedisi where kontak = '$no_telp'");
        return $query->first_row()->total;
    }

    function check_deviceid($idkurir, $deviceid)
    {
        $row = $this->db->query("select deviceid, id_ekspedisi from m_ekspedisi where id_ekspedisi='$idkurir'")->row();
        
        if(isset($row))
        {
            if($row->deviceid == null) return false;
            else if($row->deviceid == "") return false;
            else if($row->deviceid != $deviceid) return false;
            else return true;
        }
        return false;
    }


    function cek_no_telp_withid($no_telp, $id_ekspedisi){
        $sub = substr($no_telp, 0, 3);
        $sub2 = substr($no_telp, 3);
        // str_replace(search, replace, subject)
        $sub = str_replace("628", "08", $sub);
        //$no_telp = $sub . $sub2;
        
        $query = $this->db->query("select count(*) as total from m_ekspedisi where kontak = '$no_telp' and id_ekspedisi='$id_ekspedisi'");
        return $query->first_row()->total;
    }

    function aktifasi($id_ekspedisi){
        $query = $this->db->query("update m_ekspedisi set status=1 where id_ekspedisi='$id_ekspedisi'");
        return $query;
    }

    function deaktifasi($id_ekspedisi){
        $query = $this->db->query("update m_ekspedisi set status=0 where id_ekspedisi='$id_ekspedisi'");
        return $query;
    }


    function cek_kode($id_kode,$kode){
        $id_kode    = $this->db->escape_str($id_kode);
        $kode    = $this->db->escape_str($kode);
        $query = $this->db->query("select count(*) as total from kode_sms where id = '$id_kode' and kode = '$kode'");
        return $query->first_row()->total;
    }

    function login($no_telp,$password){

        $no_telp   = $this->db->escape_str($no_telp);
        $password   = hash("sha512", $this->db->escape_str($password));

        // $no_telp   = "081265582400";
        // $password   = "a79a122977d9a519f17dea1335ef4dda415f0d3b5005c3cafa1bc7081770240a292f123e4cd14fa986720b99eca38abfb12ab81a765d06cbe00d5b3486b52b93";
        $stmt = "select * from m_ekspedisi where kontak='$no_telp' and password='$password' and status=1";
        // echo "$stmt"; die();
       
        $query = $this->db->query($stmt);
       
        return $query;
    } 

    function insert_lokasi_usaha($id_ekspedisi,$id_lokasi,$jenis_lokasi){

        $id_ekspedisi    = $this->db->escape_str($id_ekspedisi);
        $jenis_lokasi    = $this->db->escape_str($jenis_lokasi);
        $id_lokasi    = $this->db->escape_str($id_lokasi);
        
        $this->db->query("insert into p_detail_lokasi (id_lokasi,id_ekspedisi,nama,lat,lng,jenis_lokasi)
            values ('$id_lokasi','$id_ekspedisi','Nama Lokasi','0','0','$jenis_lokasi')");
    }


    function insert_ekspedisi($id_ekspedisi,$nama,$foto){

        $id_ekspedisi    = $this->db->escape_str($id_ekspedisi);
        $nama    = $this->db->escape_str($nama);
        $foto    = $this->db->escape_str($foto);

        $sql = "insert into m_ekspedisi (id_ekspedisi,nama,foto)
            values ('$id_ekspedisi','$nama','$foto')";

        // echo $sql; die();
        
        $this->db->query($sql);
    }

    function update_ekspedisi($id_ekspedisi,$nama){

        $id_ekspedisi    = $this->db->escape_str($id_ekspedisi);
        $nama    = $this->db->escape_str($nama);
        
        $this->db->query("update m_ekspedisi set nama='$nama' where id_ekspedisi='$id_ekspedisi'");
    }

    function update_nama_lokasi_usaha($id_lokasi,$nama_lokasi){

        $nama_lokasi    = $this->db->escape_str($nama_lokasi);
        $id_lokasi    = $this->db->escape_str($id_lokasi);
        
        $this->db->query("update p_detail_lokasi set nama = '$nama_lokasi' where id_lokasi = '$id_lokasi'");
    }

    function update_lokasi_usaha($id_lokasi,$nama_lokasi,$lat,$lng){

        $lat    = $this->db->escape_str($lat);
        $lng    = $this->db->escape_str($lng);
        $id_lokasi    = $this->db->escape_str($id_lokasi);
        
        $this->db->query("update p_detail_lokasi set lat = '$lat', lng = '$lng', nama = '$nama_lokasi' where id_lokasi = '$id_lokasi'");
    }

    function get_kurir_all() {
        $sql = "select * from m_ekspedisi";
        // echo $sql;  die();
        return $this->db->query($sql)->result();
    }

    function get_layanan($id_ekspedisi) {
        $sql = "select * from m_layanan_ekspedisi where id_ekspedisi='$id_ekspedisi'";
        // echo $sql;  die();
        return $this->db->query($sql)->result();
    }

    function tambah_layanan($id_ekspedisi, $nama) {
        $id_ekspedisi    = $this->db->escape_str($id_ekspedisi);
        $nama    = $this->db->escape_str($nama);

        $sql = "insert into m_layanan_ekspedisi (id_ekspedisi, nama) VALUE ('$id_ekspedisi', '$nama')";
        // echo $sql;  die();
        return $this->db->query($sql);
    }

    function update_id($id_ekspedisi, $new_id) {
        return $this->db->query("update m_ekspedisi set id_ekspedisi='$new_id' where id_ekspedisi='$id_ekspedisi'");
    }

    function get_open_pemilik($id_ekspedisi) {
        return $this->db->query("select a.*, b.tipe as tipe_usaha  
            from m_ekspedisi a left join m_jenis_kategori_usaha b on (a.tipe=2 OR a.id_jenis=b.id_jenis)
            where a.id_ekspedisi='$id_ekspedisi' AND (((CURRENT_TIME() < a.waktu_tutup) AND a.waktu_buka IS NOT NULL AND a.waktu_tutup IS NOT NULL) OR (a.waktu_buka IS NULL AND a.waktu_tutup IS NULL)) LIMIT 1")->result();
    }

    function get_lokasi_usaha($id_ekspedisi){
        
        $id_ekspedisi    = $this->db->escape_str($id_ekspedisi);
       
        $query = $this->db->query("select * from p_detail_lokasi where id_ekspedisi = '$id_ekspedisi' ");
       
        return $query;
    } 

    function get_radius(){
        $query = $this->db->query("select * from m_radius order by radius");
        return $query->result();
    }

    function get_pemilik_fee($id_ekspedisi){
        $id_ekspedisi    = $this->db->escape_str($id_ekspedisi);
        $query = $this->db->query("select fee from m_ekspedisi where id_ekspedisi='$id_ekspedisi'");
        return $query->row()->fee;
    }
        
    function get_random_num() {
        $result = $this->db->query("SELECT FLOOR(RAND() * 99999999) AS random_num
            FROM m_ekspedisi WHERE 'random_num' NOT IN (SELECT id_ekspedisi FROM m_ekspedisi)
            LIMIT 1")->row();
        return $result->random_num;
    }

    function get_random_nums($count) {
        $result = $this->db->query("SELECT FLOOR(RAND() * 99999999) AS random_num
            FROM m_ekspedisi WHERE 'random_num' NOT IN (SELECT id_ekspedisi FROM m_ekspedisi)
            LIMIT " . $count)->result();
        return $result;
    }

    function get_random_phones($count) {
        $result = $this->db->query("SELECT FLOOR(RAND() * 999999999999) AS random_num
            FROM m_ekspedisi WHERE 'random_num' NOT IN (SELECT kontak FROM m_ekspedisi)
            LIMIT " . $count)->result();
        return $result;
    }

    function get_kategori_produk(){

        $query = $this->db->query("select * from m_kategori_produk where status=1");
        
        return $query->result();
    }



    function get_layanan_by_etd() {
        return $this->db->query("SELECT mle.id_layanan_ekspedisi, mle.id_ekspedisi, mle.nama as nama_layanan , CONCAT('" . URL_IMAGE_EKSPEDISI_WEBADMIN . "', me.foto) as foto, me.courier_slug from m_layanan_ekspedisi mle left join m_ekspedisi me on me.id_ekspedisi=mle.id_ekspedisi where me.courier_slug IS NOT NULL order by mle.id_ekspedisi")->result();
    }


    function get_kategori_produk_pemilik($id_ekspedisi){

        $query = $this->db->query("select * from m_kategori_produk where id_ekspedisi='$id_ekspedisi'");
        
        return $query->result();
    }

    function get_kategori_produk_by_nama($kategori,$id_ekspedisi){

        $kategori = $this->db->escape_str($kategori);
        $id_ekspedisi= $this->db->escape_str($id_ekspedisi);

        /*$query = $this->db->query("select * from m_kategori_produk where status=1 and nama_kategori='$nama'");*/
        $query = $this->db->query("select * from m_kategori_produk where nama_kategori='$kategori' and id_ekspedisi='$id_ekspedisi'");  
        
        return $query->first_row()->id_kategori;
    }

    function get_kategori_by_nama_kategori($kategori){

        $id_kategori = 0;
        $kategori = $this->db->escape_str($kategori);

        /*$query = $this->db->query("select * from m_kategori_produk where status=1 and nama_kategori='$nama'");*/
        $query = $this->db->query("select * from m_kategori_produk where nama_kategori='$kategori'");  
        
        $result = $query->result();
        if (count($result) > 0) {
            $id_kategori = $query->first_row()->id_kategori;
        } else {
            $id_kategori = 0;
        }

        return $id_kategori;
    }

    function insert_kategori_produk($kategori,$id_ekspedisi){
	
        $query_kategori = "insert into m_kategori_produk (id_ekspedisi,nama_kategori)
                            SELECT '$id_ekspedisi',tmp.* FROM (SELECT '$kategori') AS tmp
                            WHERE NOT EXISTS (
                                SELECT nama_kategori FROM m_kategori_produk WHERE nama_kategori = '$kategori' and id_ekspedisi = '$id_ekspedisi' 
                            ) LIMIT 1";
                            // echo $query_kategori; die();
                            
        //$query_kategori = "insert into m_kategori_produk (nama_kategori,id_ekspedisi) values ('$kategori','$id_ekspedisi')";                  
        $this->db->query($query_kategori);
        return $this->db->insert_id();
    }

    function insert_produk($id_produk,$id_ekspedisi,$nama,$harga,$harga_promo,$stok,$keterangan,$id_kategori, $min_antar, $ket_promo){        

        $id_produk    = $this->db->escape_str($id_produk);
        $id_ekspedisi    = $this->db->escape_str($id_ekspedisi);
        $nama    = $this->db->escape_str($nama);
        $harga    = $this->db->escape_str($harga);
        $harga_promo    = $this->db->escape_str($harga_promo);
        $stok    = $this->db->escape_str($stok);
        $keterangan    = $this->db->escape_str($keterangan);
        $id_kategori    = $this->db->escape_str($id_kategori);
        $ket_promo    = $this->db->escape_str($ket_promo);

        // echo "harga_promo: " .$harga_promo . "\n";
        if ($harga_promo == "") {
            $harga_promo = "NULL";
        }


        $query_produk   = "INSERT INTO `p_detail_produk`(`id_produk`, `id_ekspedisi`, `id_kategori`, `nama_produk`, `stok`, `keterangan`, `harga`, `harga_promo`, `status`, `min_antar`, `active`, `ket_promo`) 
        VALUES('$id_produk','$id_ekspedisi','$id_kategori','$nama','$stok','$keterangan','$harga', " . $harga_promo . ", '1', '$min_antar', 1, '$ket_promo') ";

        // echo $query_produk; die();
        
        $this->db->query($query_produk);
    }

    function insert_produk_batch($id_ekspedisi, $headers, $rows){    


        $query_produk   = "INSERT INTO `p_detail_produk` (";
        $query_produk = $query_produk . "`id_ekspedisi`, ";
        $id_produk_pos = 0;
        $k = 0;
        $updateStmt = "ON DUPLICATE KEY UPDATE ";
        foreach ($headers as $header) {
            if ($header == 'id_barcode') {
                $id_produk_pos = $k;
                $header = 'id_produk';
            }
            $query_produk = $query_produk . "`".$header . "`, ";
            $updateStmt = $updateStmt . " " . $header . "=VALUES(" . $header . "), ";
            $k++;
        }
        $query_produk = $query_produk . " `active`) VALUES ";
        $query_produk = str_replace(", ) VALUES ", ") VALUES ", $query_produk);

        $updateStmt = $updateStmt . "HOHOHO";
        $updateStmt = str_replace("), HOHOHO", ") ", $updateStmt);

        $i = 0;
        foreach ($rows as $produk) {
            if (count($produk) <  $k) continue;

            $query_produk = $query_produk . "(";
            $query_produk = $query_produk . "'".$id_ekspedisi . "', ";
            $j=0;
            foreach ($produk as $column) {
                // $column = $produk[$j];
                if ($j == $id_produk_pos) {
                    $column = $id_ekspedisi . "_" . $column;
                }
                $query_produk = $query_produk . "'".$column . "', ";
                $j++;
            }

            $query_produk = $query_produk . "0, ), HIHI";
            $query_produk = str_replace(", ), HIHI", "),", $query_produk);

            $i++;
        }

        $query_produk = $query_produk . " HEHE";
        $query_produk = str_replace("), HEHE", ")", $query_produk);

        $query_produk = $query_produk . " " . $updateStmt;

        // echo $query_produk; die();
        
        $this->db->query($query_produk);
    }

    function set_produk_siap_antar_batch($id_ekspedisi, $headers, $rows){    


        $query_produk   = "UPDATE `p_detail_produk` SET min_antar=1 WHERE id_produk IN (";
        $id_produk_pos = 0;
        $k = 0;
        foreach ($headers as $header) {
            if ($header == 'id_barcode') {
                $id_produk_pos = $k;
            }
            $k++;
        }

        $i = 0;
        foreach ($rows as $produk) {
            $query_produk = $query_produk . "'".$id_ekspedisi . "_" . $produk[$id_produk_pos] . "', ";
            $i++;
        }

        $query_produk = $query_produk . ")";
        $query_produk = str_replace(", )", ")", $query_produk);


        // echo $query_produk; die();
        
        $this->db->query($query_produk);
    }

    // function insert_produk_batch($produks){    


    //     $query_produk   = "INSERT INTO `p_detail_produk`(`id_produk`, `id_ekspedisi`, `id_kategori`, `nama_produk`, `stok`, `keterangan`, `harga`, `harga_promo`, `status`, `min_antar`) VALUES ";

    //     $i = 0;
    //     foreach ($produks as $produk) {
    //         $id_produk    = $produk->$id_produk;
    //         $id_ekspedisi    = $produk->$id_ekspedisi;
    //         $nama    = $produk->$nama;
    //         $harga    = $produk->$harga;
    //         $harga_promo    = $produk->$harga_promo;
    //         $stok    = $produk->$stok;
    //         $keterangan    = $produk->$keterangan;
    //         $id_kategori    = $produk->$id_kategori;

    //         if ($i > 0) {
    //             $query_produk = $query_produk . ", ('$id_produk','$id_ekspedisi','$id_kategori','$nama','$stok','$keterangan','$harga', '$harga_promo', '1', '$min_antar')";
    //         } else {
    //             $query_produk = $query_produk . " ('$id_produk','$id_ekspedisi','$id_kategori','$nama','$stok','$keterangan','$harga', '$harga_promo', '1', '$min_antar')";
    //         }

    //         $i++;
    //     }
        
    //     $this->db->query($query_produk);
    // }

    function update_produk($id_produk,$id_kategori,$nama,$harga,$harga_promo,$stok,$keterangan, $min_antar, $ket_promo){        

        $id_produk    = $this->db->escape_str($id_produk);
        $nama    = $this->db->escape_str($nama);
        $harga    = $this->db->escape_str($harga);
        $harga_promo    = $this->db->escape_str($harga_promo);
        $stok    = $this->db->escape_str($stok);
        $keterangan    = $this->db->escape_str($keterangan);
        $ket_promo    = $this->db->escape_str($ket_promo);

        if ($harga_promo == '') {
            $harga_promo == "NULL";
        }

        $query_produk   = "update p_detail_produk set nama_produk = '$nama', id_kategori = '$id_kategori', harga = '$harga', harga_promo = " . $harga_promo . ", stok = '$stok', keterangan = '$keterangan', min_antar = '$min_antar', ket_promo='$ket_promo'
                            where id_produk = '$id_produk'";
        
        $this->db->query($query_produk);
    }


    function update_produk_umkm($id_produk,$id_kategori,$nama,$harga,$harga_promo,$stok,$keterangan, $min_antar, $ket_promo){        

        $id_produk    = $this->db->escape_str($id_produk);
        $nama    = $this->db->escape_str($nama);
        $harga    = $this->db->escape_str($harga);
        $harga_promo    = $this->db->escape_str($harga_promo);
        $stok    = $this->db->escape_str($stok);
        $keterangan    = $this->db->escape_str($keterangan);
        $ket_promo    = $this->db->escape_str($ket_promo);

        $query_produk   = "update p_detail_produk_umkm set nama_produk = '$nama', id_kategori = '$id_kategori', harga = '$harga', harga_promo = '$harga_promo', stok = '$stok', keterangan = '$keterangan', min_antar = '$min_antar', ket_promo='$ket_promo'
                            where id_produk = '$id_produk'";
        
        $this->db->query($query_produk);
    }

    function update_stok($id_produk,$stok){

        $id_produk    = $this->db->escape_str($id_produk);
        $stok    = $this->db->escape_str($stok);
        
        $this->db->query("update p_detail_produk set stok = '$stok' where id_produk = '$id_produk'");
    }


    // function update_stocks($id_ekspedisi, $stocks){
    //     $when = ""; 
    //     $in = "where id_produk in (";
    //     $stmt = "update p_detail_produk set stok = case";
    //     $i = 0;
    //     // echo json_encode($stocks); die();
    //     foreach ($stocks as $stock) {
    //         $when = $when . " when id_produk = '" . $id_ekspedisi . "_". $stock->id_produk . "' then " .  $stock->stok;
    //         if ($i == (count($stocks)-1)) {
    //             $in = $in . "'" . $id_ekspedisi . "_". $stock->id_produk . "'";
    //         } else {    
    //             $in = $in . "'" . $id_ekspedisi . "_". $stock->id_produk . "',";
    //         }
    //         $i++;
    //     }
    //     $when = $when . " end ";
    //     $in = $in . ")";

    //     $stmt = $stmt . $when . $in;
    //     $this->db->query($stmt);

    //     // echo $stmt; die();

    //     // $this->db->update_batch("p_detail_produk", $stocks, "id_produk'");
    // }

    function update_stocks($id_ekspedisi, $stocks){
        $i = 0;

        // $return = $this->db->query("delete from p_detail_produk where id_ekspedisi='" . $id_ekspedisi . "'");
        
        $insert = "INSERT INTO p_detail_produk (id_produk, id_ekspedisi, nama_produk, id_kategori, stok, harga, foto, keterangan, active) VALUES ";
        $update = "ON DUPLICATE KEY UPDATE id_ekspedisi=VALUES(id_ekspedisi), nama_produk=VALUES(nama_produk), id_kategori=VALUES(id_kategori), stok=VALUES(stok), harga=VALUES(harga), foto=CONCAT(REPLACE(VALUES(id_produk), '".$id_ekspedisi."_', ''), '.png'), active=1";

        foreach ($stocks as $stock) {
            $insert = $insert . "('" . $id_ekspedisi . "_" . $stock->id_produk . "', '" . $id_ekspedisi . "', '" . (isset($stock->nama_produk) ? self::clean($stock->nama_produk) : '') . "', " . (isset($stock->id_kategori) ? $stock->id_kategori : '0') . ", " . (isset($stock->stok) ? $stock->stok : '0') . ", " . (isset($stock->harga) ? $stock->harga : '0') . ", '" . (isset($stock->foto) ? $stock->foto : '') . "', '". (isset($stock->keterangan) ? $stock->keterangan : '') . "', 1), ";
            $i++;
            
        }

        $stmt = $insert . $update;
        $stmt = str_replace("), ON DUPLICATE KEY UPDATE", ") ON DUPLICATE KEY UPDATE", $stmt);
        // echo $stmt; die();
        // $stmt = self::clean($stmt);
        $return = $this->db->query($stmt);
        $errMess = "";

        if(!$return)
        {
           $errNo   = $this->db->_error_number();
           $errMess = $this->db->_error_message();
           // Do something with the error message or just show_404();
        }
        return $errMess;
    }


    function update_produks_umkm($id_ekspedisi, $stocks){
        $i = 0;

        // $return = $this->db->query("delete from p_detail_produk where id_ekspedisi='" . $id_ekspedisi . "'");
        
        $insert = "INSERT INTO p_detail_produk_umkm (id_produk, id_ekspedisi, nama_produk, stok, harga, harga_promo, keterangan, active) VALUES ";
        $update = "ON DUPLICATE KEY UPDATE id_ekspedisi=VALUES(id_ekspedisi), nama_produk=VALUES(nama_produk), nama_produk=VALUES(nama_produk), harga=VALUES(harga), harga_promo=VALUES(harga_promo), active=1";

        foreach ($stocks as $stock) {
            $harga_promo = "";
            if ($stock->harga_promo == "") {
                $harga_promo = "NULL";
            } else {
                $harga_promo = $stock->harga_promo;
            }
            $insert = $insert . "('" . $id_ekspedisi . "_" . $stock->id_produk . "', '" . $id_ekspedisi . "', '" . (isset($stock->nama_produk) ? self::clean($stock->nama_produk) : '') . "', " . (isset($stock->stok) ? $stock->stok : '0') . ", " . (isset($stock->harga) ? $stock->harga : '0')  . ", " . $harga_promo . ", '". (isset($stock->keterangan) ? $stock->keterangan : '') . "', 1), ";
            $i++;
            
        }

        $stmt = $insert . $update;
        $stmt = str_replace("), ON DUPLICATE KEY UPDATE", ") ON DUPLICATE KEY UPDATE", $stmt);
        // echo $stmt; die();
        // $stmt = self::clean($stmt);
        $return = $this->db->query($stmt);
        $errMess = "";

        if(!$return)
        {
           $errNo   = $this->db->_error_number();
           $errMess = $this->db->_error_message();
           // Do something with the error message or just show_404();
        }
        return $errMess;
    }

    function get_produks_umkm($id_ekspedisi, $page){

        $id_ekspedisi = $this->db->escape_str($id_ekspedisi);
        $page = $this->db->escape_str($page);

        if (intval($page >=1 )) { // kalau paging dimulai dari 1, enable pagint. Jika tidak, tampilkan seluruhnya
            $offset = ($page-1) * 50; 
            $pagingParam =  "LIMIT 50 OFFSET " . $offset;
        }
        
        $sql = "select a.*,b.nama_kategori, 0 as is_official from p_detail_produk_umkm a left join m_ekspedisi c on c.id_partner=a.id_ekspedisi or c.id_ekspedisi=a.id_ekspedisi left join m_kategori_produk b on a.id_kategori=b.id_kategori where a.id_ekspedisi='$id_ekspedisi' AND a.nama_produk <> '' AND a.active=1 order by b.nama_kategori " . $pagingParam;
        // echo $sql; die();
        $query = $this->db->query($sql);
        
        return $query->result();
    }

    function get_total_produks_umkm($id_ekspedisi){

        $id_ekspedisi = $this->db->escape_str($id_ekspedisi);
        
        $query = $this->db->select('count(*) as total')->from('p_detail_produk_umkm')->where('id_ekspedisi', $id_ekspedisi);
        $query = $this->db->get();
        return $query->row()->total;
    }

    function count_products($id_ekspedisi) {
        $stmt = "SELECT COUNT(*) as total FROM p_detail_produk WHERE id_ekspedisi='".$id_ekspedisi."'";
        $query = $this->db->query($stmt);
        return $query->row_array();
    }

    function copy_products($id_ekspedisi_old, $id_ekspedisi_new) {
        $stmt = "INSERT INTO p_detail_produk (id_produk,id_ekspedisi,id_kategori,nama_produk,stok,harga,harga_promo,active)
        SELECT REPLACE(id_produk, '".$id_ekspedisi_old."_', '".$id_ekspedisi_new."_'), '".$id_ekspedisi_new."', id_kategori, nama_produk, stok, harga, harga_promo, 1
        FROM p_detail_produk WHERE id_ekspedisi = '".$id_ekspedisi_old."' ON DUPLICATE KEY UPDATE id_ekspedisi=VALUES(id_ekspedisi), id_kategori=VALUES(id_kategori), nama_produk=VALUES(nama_produk), stok=VALUES(stok), harga=VALUES(harga), harga_promo=VALUES(harga_promo), active=1";
        // echo $stmt; die();
        $query = $this->db->query($stmt);
        return $query;
    }

    function clean($string) {
       $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
       return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    // function get_produk($id_ekspedisi){

    //     $id_ekspedisi    = $this->db->escape_str($id_ekspedisi);

    //     return $this->db->query("select a.*,b.nama_kategori
    //                         from p_detail_produk a, m_kategori_produk b
    //                         where
    //                         a.id_kategori = b.id_kategori
    //                         and
    //                         a.id_ekspedisi = '$id_ekspedisi' and a.status = 1
    //                         order by b.nama_kategori")->result();
    
    // }

    function get_pemilik_by_partner($id_partner){

        $id_partner    = $this->db->escape_str($id_partner);
        
        $query = $this->db->query("select * from m_ekspedisi where id_partner = '$id_partner'");

        return $query;
    }

    function get_ekspedisi($id_ekspedisi){

        $id_ekspedisi    = $this->db->escape_str($id_ekspedisi);
        
        $query = $this->db->query("select * from m_ekspedisi where id_ekspedisi = '$id_ekspedisi' LIMIT 1");

        return $query->first_row();
    }

    function get_first_pemilik_by_partner($id_partner){

        $id_partner    = $this->db->escape_str($id_partner);
        
        $query = $this->db->query("select * from m_ekspedisi where id_partner = '$id_partner' ORDER BY tanggal_insert LIMIT 1");

        return $query->row();
    }

    function delete_produk($id_produk){

        $id_produk    = $this->db->escape_str($id_produk);
        
        $this->db->query("delete from p_detail_produk where id_produk = '$id_produk'");
    }

    function copy_produk_to_histori($id_produk) {
        $stmt = "INSERT INTO histori_p_detail_produk SELECT p.* FROM p_detail_produk p WHERE p.id_produk = '".$id_produk."'";
        $this->db->query($stmt);
    }

    function delete_agen_by_ekspedisi($id_ekspedisi){

        $id_ekspedisi    = $this->db->escape_str($id_ekspedisi);
        
        $this->db->query("delete from m_agen_ekspedisi where id_ekspedisi = '$id_ekspedisi'");
    }


    function delete_layanan_by_ekspedisi($id_ekspedisi){

        $id_ekspedisi    = $this->db->escape_str($id_ekspedisi);
        
        $this->db->query("delete from m_layanan_ekspedisi where id_ekspedisi = '$id_ekspedisi'");
    }

    function delete_agen($id_agen){

        $id_agen    = $this->db->escape_str($id_agen);
        
        $this->db->query("delete from m_agen_ekspedisi where id_agen = '$id_agen'");
    }


    function delete_layanan($id_layanan_ekspedisi){

        $id_layanan_ekspedisi    = $this->db->escape_str($id_layanan_ekspedisi);
        
        $this->db->query("delete from m_layanan_ekspedisi where id_layanan_ekspedisi = '$id_layanan_ekspedisi'");
    }

    function get_order($id_ekspedisi, $status=1){
        
        $id_ekspedisi    = $this->db->escape_str($id_ekspedisi);
        /*
        status  1 : baru
                2 : sudah di proses
                3 : sudah selesai
                4 : cancel
        */

        $query = $this->db->query("select a.*,b.nama,b.alamat,b.kontak,b.lat,b.lng,b.foto,b.versi, c.nama as nama_kota, e.nama as nama_merchant, e.lat as lat_merchant, e.lng as lng_merchant, e.alamat as alamatmerchant, e.foto as fotomerchant 
from konsumen_order a, m_konsumen b , m_kota c , m_ekspedisi d , m_pemilik_usaha e 
                    where  
                    a.id_konsumen=b.id_konsumen and a.id_pemilik=e.id_pemilik and e.kontak_sms2=d.kontak and d.id_ekspedisi='$id_ekspedisi' and c.id = b.id_kota and a.status=$status group by a.id_order
                    order by a.status asc");     
        
        return $query->result();
    }


    public function get_data_aktif() {
        $sql = "select *, CONCAT('" . URL_IMAGE_EKSPEDISI_WEBADMIN . "', foto) as foto from m_ekspedisi where status=1";
        
        $query =  $this->db->query($sql);
        return $query;

    }

    public function get_data() {
        $sql = "select *, CONCAT('" . URL_IMAGE_EKSPEDISI_WEBADMIN . "', foto) as foto from m_ekspedisi";
        
        $query =  $this->db->query($sql);
        return $query;

    }

    public function get_agen_ekspedisi($id_ekspedisi) {
        // echo "ini"; die()
        $id_ekspedisi    = $this->db->escape_str($id_ekspedisi);
        $sql = "select * from m_agen_ekspedisi where id_ekspedisi='$id_ekspedisi'";
        // echo $sql;die();
        
        $query =  $this->db->query($sql);
        return $query;

    }

    public function get_agen($id_agen) {
        // echo "ini"; die()
        $id_agen    = $this->db->escape_str($id_agen);
        $sql = "select * from m_agen_ekspedisi where id_agen='$id_agen'";
        // echo $sql;die();
        
        $query =  $this->db->query($sql);
        return $query;

    }


    public function get_agen_terdekat($lat, $lng) {
        $sql = "select b.*, 'pickuppost' as tipe_merchant, CONCAT('" . URL_IMAGE_EKSPEDISI_WEBADMIN . "', a.foto) as foto, ( 6371 * ACOS( COS( RADIANS( '$lat' ) ) * COS( RADIANS( b.lat ) ) * COS( RADIANS( b.lng ) - RADIANS( '$lng' ) ) + SIN( RADIANS( '$lat' ) ) * SIN( RADIANS( b.lat ) ) ) ) as distance from m_agen_ekspedisi b left join m_ekspedisi a on a.id_ekspedisi=b.id_ekspedisi 

        WHERE ( 6371 * ACOS( COS( RADIANS( '$lat' ) ) * COS( RADIANS( b.lat ) ) * COS( RADIANS( b.lng ) - RADIANS( '$lng' ) ) + SIN( RADIANS( '$lat' ) ) * SIN( RADIANS( b.lat ) ) ) ) <= (5000 /1000 ) ";

        // echo $sql; die();
        
        $query =  $this->db->query($sql);
        return $query;

    }


    public function get_agen_ekspedisi_terdekat($lat, $lng) {
        $sql = "select b.*, a.*, 'pickuppost' as tipe_merchant, CONCAT('" . URL_IMAGE_EKSPEDISI_WEBADMIN . "', a.foto) as foto, ( 6371 * ACOS( COS( RADIANS( '$lat' ) ) * COS( RADIANS( b.lat ) ) * COS( RADIANS( b.lng ) - RADIANS( '$lng' ) ) + SIN( RADIANS( '$lat' ) ) * SIN( RADIANS( b.lat ) ) ) ) as distance from m_ekspedisi a left join m_agen_ekspedisi b on a.id_ekspedisi=b.id_ekspedisi" . 
        "  WHERE ( 6371 * ACOS( COS( RADIANS( '$lat' ) ) * COS( RADIANS( b.lat ) ) * COS( RADIANS( b.lng ) - RADIANS( '$lng' ) ) + SIN( RADIANS( '$lat' ) ) * SIN( RADIANS( b.lat ) ) ) ) <= (5000 /1000 )  " .
         " GROUP BY b.id_ekspedisi ORDER BY distance LIMIT 10";

        // echo $sql; die();
        
        $query =  $this->db->query($sql);
        return $query;

    }

    public function get_data_ekspedisi($requestData) {
        
        $sql = "SELECT a.*, CONCAT('" . URL_IMAGE_EKSPEDISI_WEBADMIN . "', a.foto) as foto FROM m_ekspedisi a";
        
        $sql.=" order by a.nama LIMIT ".$requestData['start']." ,".$requestData['length']."   ";

        // echo $sql; die();
        
        $query = $this->db->query($sql);
        return $query;
    }

    public function get_data_ekspedisi_filter($requestData) {
        $sql = "";

        $keyword = " like '%" . $requestData['search']['value'] . "%'";
            $sql = "SELECT a.*, CONCAT('" . URL_IMAGE_EKSPEDISI_WEBADMIN . "', a.foto) as foto FROM m_ekspedisi a where (a.id_pemilik ". $keyword . " OR a.nama ". $keyword .")";
        
        $sql.=" order by a.nama";
        $query = $this->db->query($sql);
        return $query;
    }


    public function get_data_agen($id_ekspedisi, $requestData) {
        
        $sql = "SELECT a.* FROM m_agen_ekspedisi a WHERE id_ekspedisi='$id_ekspedisi'";
        
        $sql.=" order by a.nama LIMIT ".$requestData['start']." ,".$requestData['length']."   ";

        // echo $sql; die();
        
        $query = $this->db->query($sql);
        return $query;
    }

    public function get_data_agen_filter($id_ekspedisi, $requestData) {
        $sql = "";

        $keyword = " like '%" . $requestData['search']['value'] . "%'";
            $sql = "SELECT a.* FROM m_agen_ekspedisi a where (a.id_ekspedisi ". $keyword . " OR a.nama ". $keyword ."  OR a.alamat ". $keyword .") AND id_ekspedisi='$id_ekspedisi'";
        
        $sql.=" order by a.nama";
        $query = $this->db->query($sql);
        return $query;
    }

    public function insert_agen($id_ekspedisi, $nama, $lat, $lng, $alamat) {

        $id_ekspedisi    = $this->db->escape_str($id_ekspedisi);
        $nama    = $this->db->escape_str($nama);
        $lat    = $this->db->escape_str($lat);
        $lng    = $this->db->escape_str($lng);
        $alamat    = $this->db->escape_str($alamat);

        $sql = "insert into m_agen_ekspedisi (id_ekspedisi, nama, lat, lng, alamat) VALUE ('$id_ekspedisi', '$nama', '$lat', '$lng', '$alamat')";
        $query = $this->db->query($sql);
        return $query;
    }


    function get_done_order($no_telp_kurir, $tipe=1){
                    // echo $no_telp_kurir; die();
        
        $no_telp_kurir   = $this->db->escape_str($no_telp_kurir);
        $tipe    = $this->db->escape_str($tipe);

        $tipe_param = "";
        if (intval($tipe) == 1) { // usaha / toko
            $tipe_param = " (e.tipe=1 OR e.tipe=2 OR e.tipe=3)";
        } else if (intval($tipe) == 2) { // catering
            $tipe_param = " e.tipe=5";
        } else if (intval($tipe) == 3) { // pickup post
            $tipe_param = " e.tipe=6";
        }
        

        /*
        status  1 : baru
                2 : sudah di proses
                3 : sudah selesai
                4 : cancel
        */

        $stmt = "select a.*,  CONCAT('" . URL_IMAGE_KONSUMEN . "', b.foto) as foto, b.nama,b.alamat,b.kontak,b.lat,b.lng,b.versi, c.nama as nama_kota, e.nama as nama_merchant, e.lat as lat_merchant, e.lng as lng_merchant, e.alamat as alamatmerchant, e.foto as fotomerchant 
from konsumen_order a left join m_konsumen b on a.id_konsumen=b.id_konsumen left join m_kota c on c.id = b.id_kota left join m_pemilik_usaha e on a.id_pemilik=e.id_pemilik
                    where e.kontak_sms2='$no_telp_kurir' and (a.status=4 OR a.status=3) and b.nama IS NOT NULL and " . $tipe_param . " group by a.id_order
                    order by a.status asc";


        $query = $this->db->query($stmt);     
        
        return $query->result();
    }

    function get_new_order_count($no_telp_kurir, $tipe) {
        $no_telp_kurir    = $this->db->escape_str($no_telp_kurir);
        $tipe    = $this->db->escape_str($tipe);

        $tipe_param = "";
        if (intval($tipe) == 1) { // usaha / toko
            $tipe_param = " (e.tipe=1 OR e.tipe=2 OR e.tipe=3)";
        } else if (intval($tipe) == 2) { // catering
            $tipe_param = " e.tipe=5";
        } else if (intval($tipe) == 3) { // pickup post
            $tipe_param = " e.tipe=6";
        }
        
        $stmt = "select a.*, CONCAT('" . URL_IMAGE_KONSUMEN . "', b.foto) as foto,b.nama,b.alamat,b.kontak,b.lat,b.lng,b.versi, c.nama as nama_kota, e.nama_produk as nama_produk, e.harga, d.kuantitas as kuantitas, d.id_produk 
                    from konsumen_order a left join m_konsumen b on a.id_konsumen=b.id_konsumen left join m_kota c on c.id = b.id_kota left join detail_order d on d.id_order=a.id_order left join p_detail_produk e on e.id_produk=d.id_produk left join m_pemilik_usaha f on f.id_pemilik=e.id_pemilik
                    where f.kontak_sms2='$no_telp_kurir' and a.status!=4 and a.status!=3 and b.nama IS NOT NULL
                    and " . $tipe_param . " group by a.id_order order by a.id_order asc 
                    ";
        $query = $this->db->query($stmt);

        $record =  $query->num_rows();
        // echo json_encode($record); die();

        return $record;
    }

    function get_order_detail($id_ekspedisi, $id_order){
        
        $id_ekspedisi    = $this->db->escape_str($id_ekspedisi);
        $id_order    = $this->db->escape_str($id_order);
        /*
        status  1 : baru
                2 : sudah di proses
                3 : sudah selesai
                4 : cancel
        */

        $query = $this->db->query("select a.*,b.nama,b.alamat,b.kontak,b.lat,b.lng,b.foto,b.versi, c.nama as nama_kota, e.nama as nama_merchant, e.lat as lat_merchant, e.lng as lng_merchant, e.alamat as alamatmerchant, e.foto as fotomerchant 
from konsumen_order a, m_konsumen b , m_kota c , m_ekspedisi d , m_pemilik_usaha e 
                    where  
                    a.id_konsumen=b.id_konsumen and a.id_ekspedisi=e.id_ekspedisi and e.kontak_sms2=d.kontak and d.id_ekspedisi='$id_ekspedisi' and c.id = b.id_kota and a.id_order = '$id_order' group by a.id_order
                    order by a.status asc");     
        
        return $query->result();
    }

    function get_undone_order($no_telp_kurir, $tipe){
        
        $no_telp_kurir    = $this->db->escape_str($no_telp_kurir);
        $tipe    = $this->db->escape_str($tipe);

        $tipe_param = "";
        if (intval($tipe) == 1) { // usaha / toko
            $tipe_param = " (f.tipe=1 OR f.tipe=2 OR f.tipe=3)";
        } else if (intval($tipe) == 2) { // catering
            $tipe_param = " f.tipe=5";
        } else if (intval($tipe) == 3) { // pickup post
            $tipe_param = " f.tipe=6";
        }
        
        $sql = "select a.*, CONCAT('" . URL_IMAGE_KONSUMEN . "', b.foto) as foto,b.nama,b.alamat,b.kontak,b.lat,b.lng,b.versi, c.nama as nama_kota, e.nama_produk as nama_produk, e.harga, d.kuantitas as kuantitas, d.id_produk 
                    from konsumen_order a left join m_konsumen b on a.id_konsumen=b.id_konsumen left join m_kota c on c.id = b.id_kota left join detail_order d on d.id_order=a.id_order left join p_detail_produk e on e.id_produk=d.id_produk left join m_pemilik_usaha f on f.id_pemilik=e.id_pemilik
                    where f.kontak_sms2='$no_telp_kurir' and a.status!=4 and a.status!=3 and b.nama IS NOT NULL
                    and " . $tipe_param . " group by a.id_order order by a.id_order asc 
                    ";

        // echo $sql; die();
        $query = $this->db->query($sql);
        
        return $query->result();
    }

    function set_status($id_ekspedisi,$status){

        $status    = $this->db->escape_str($status);
        $id_ekspedisi    = $this->db->escape_str($id_ekspedisi);
        
        $this->db->query("update m_ekspedisi set status_toko = '$status' where id_ekspedisi = '$id_ekspedisi'");
    }

    function update_nama_toko($id_ekspedisi,$nama){

        $id_ekspedisi    = $this->db->escape_str($id_ekspedisi);
        $nama    = $this->db->escape_str($nama);
        
        $this->db->query("update m_ekspedisi set nama = '$nama' where id_ekspedisi = '$id_ekspedisi'");
    }

    function update_agen($id_agen,$nama,$alamat,$lat,$lng){

        $id_agen    = $this->db->escape_str($id_agen);
        $nama    = $this->db->escape_str($nama);
        $alamat    = $this->db->escape_str($alamat);
        $lat    = $this->db->escape_str($lat);
        $lng    = $this->db->escape_str($lng);
        
        $this->db->query("update m_agen_ekspedisi set nama = '$nama', alamat = '$alamat', lat = '$lat', lng = '$lng' where id_agen = '$id_agen'");
    }


    function update_alamat_kurir($id_ekspedisi,$nama, $no_telp, $alamat){

        $id_ekspedisi    = $this->db->escape_str($id_ekspedisi);
        $nama    = $this->db->escape_str($nama);
        $alamat    = $this->db->escape_str($alamat);
        $no_telp    = $this->db->escape_str($no_telp);
        
        $this->db->query("update m_ekspedisi set nama = '$nama', alamat='$alamat', kontak='$no_telp', id_ekspedisi='$no_telp' where id_ekspedisi = '$id_ekspedisi'");
        if($id_ekspedisi != $no_telp)  $this->db->query("update m_pemilik_usaha set kontak_sms='$no_telp' where kontak_sms='$id_ekspedisi'");
        // update 
    }

    function update_username($id_ekspedisi,$username){

        $id_ekspedisi    = $this->db->escape_str($id_ekspedisi);
        $username    = $this->db->escape_str($username);
        
        $this->db->query("update m_ekspedisi set username = '$username' where id_ekspedisi = '$id_ekspedisi'");
    }

    function update_jargon_toko($id_ekspedisi,$jargon){

        $id_ekspedisi    = $this->db->escape_str($id_ekspedisi);
        $jargon    = $this->db->escape_str($jargon);
        
        $this->db->query("update m_ekspedisi set jargon = '$jargon' where id_ekspedisi = '$id_ekspedisi'");
    }


    function update_no_telp_password_alamat_toko($id_ekspedisi,$kontak, $kontak_sms, $kontak_sms2, $password, $alamat){

        $id_ekspedisi    = $this->db->escape_str($id_ekspedisi);
        $kontak    = $this->db->escape_str($kontak);
        $kontak_sms    = $this->db->escape_str($kontak_sms);
        $kontak_sms2    = $this->db->escape_str($kontak_sms2);
        $wherePemilik = "";
        if (strlen($password) > 0) {
            $password    = hash('sha512', $this->db->escape_str($password));
            $wherePemilik = ", password = '$password'";
        }
        $alamt    = $this->db->escape_str($alamat);
        
        $stmt = "update m_ekspedisi set kontak = '$kontak', kontak_sms='$kontak_sms', kontak_sms2='$kontak_sms2'" . $wherePemilik . ", alamat = '$alamt' where id_ekspedisi = '$id_ekspedisi'";
        // echo $stmt;
        $this->db->query($stmt);
    }

    function update_radius_toko($id_ekspedisi,$radius, $min_antar, $fee) {

        $id_ekspedisi    = $this->db->escape_str($id_ekspedisi);
        $radius    = $this->db->escape_str($radius);
        $min_antar    = $this->db->escape_str($min_antar);
        $fee    = $this->db->escape_str($fee);
        
        $this->db->query("update m_ekspedisi set radius = $radius, min_antar=$min_antar, fee=$fee where id_ekspedisi = '$id_ekspedisi'");
    }

    function update_min_antar_fee($id_ekspedisi, $min_antar, $fee) {

        $id_ekspedisi    = $this->db->escape_str($id_ekspedisi);
        $min_antar    = $this->db->escape_str($min_antar);
        $fee    = $this->db->escape_str($fee);
        
        $this->db->query("update m_ekspedisi set min_antar=$min_antar, fee=$fee where id_ekspedisi = '$id_ekspedisi'");
    }

    function update_jenis($id_ekspedisi,$id_jenis){

        $id_ekspedisi    = $this->db->escape_str($id_ekspedisi);
        $id_jenis    = $this->db->escape_str($id_jenis);
        
        $this->db->query("update m_ekspedisi set id_jenis = $id_jenis where id_ekspedisi = '$id_ekspedisi'");
    }


    function update_min_antar($id_ekspedisi,$min_antar){

        $id_ekspedisi    = $this->db->escape_str($id_ekspedisi);
        $min_antar    = $this->db->escape_str($min_antar);
        
        $this->db->query("update m_ekspedisi set min_antar = $min_antar where id_ekspedisi = '$id_ekspedisi'");
    }

    function update_kategori_usaha($id_ekspedisi,$json){

        $id_ekspedisi    = $this->db->escape_str($id_ekspedisi);

        $this->db->query("delete from p_detail_kategori where id_ekspedisi='$id_ekspedisi'");
        
        $arr = json_decode($json);

        for($i=0;$i<count($arr);$i++){

            $id_kategori_usaha  = $arr[$i]->id_kategori;

            $this->db->query("insert into p_detail_kategori (id_ekspedisi,id_kategori) values ('$id_ekspedisi','$id_kategori_usaha')");
        }
    }

    function update_kategori_usaha_awal($id_ekspedisi,$json){

        $id_ekspedisi    = $this->db->escape_str($id_ekspedisi);

        $this->db->query("delete from p_detail_kategori where id_ekspedisi='$id_ekspedisi'");
        
        $arr = json_decode($json);

        for($i=0;$i<count($arr);$i++){

            $id_kategori_usaha  = $arr[$i]->id_kategori;
            $id_jenis  = $arr[$i]->id_jenis;

            $this->db->query("insert into p_detail_kategori (id_ekspedisi,id_kategori) values ('$id_ekspedisi','$id_kategori_usaha')");
            $this->db->query("update m_ekspedisi set id_jenis = '$id_jenis' where id_ekspedisi = '$id_ekspedisi'");
        }
    }

    function get_jenis_kategori_usaha(){
        
        $query = $this->db->query("select * from m_jenis_kategori_usaha");
        
        return $query->result();
    }

    function get_detail_order($id_order){

        $id_order    = $this->db->escape_str($id_order);
        $sql = "SELECT *, count(id_produk) id_produk_count from 

            (select a.*,b.nama_produk, b.foto, b.keterangan as keterangan_produk, d.alamat_antar, d.ket_perkiraan_antar as ket_konsumen, f.ket_konsumen as keterangan, d.lat_antar, d.lng_antar, d.status as order_status, nama_kategori, e.kontak, d.charge, d.include_fee, d.include_fee_note, 'umkm' as tipe_produk 
            from p_detail_produk_umkm b left join detail_order a on b.id_produk = a.id_produk left join m_kategori_produk c on b.id_kategori=c.id_kategori left join konsumen_order d on d.id_order=a.id_order left join m_konsumen e on e.id_konsumen=d.id_konsumen left join m_perkiraan_antar f on f.id=d.id_perkiraan_antar
            where  a.id_order='$id_order'
            UNION select a.*,b.nama_produk, b.foto, b.keterangan as keterangan_produk, d.alamat_antar, d.ket_perkiraan_antar as ket_konsumen, f.ket_konsumen as keterangan, d.lat_antar, d.lng_antar, d.status as order_status, nama_kategori, e.kontak, d.charge, d.include_fee, d.include_fee_note, 'promo' as tipe_produk 
            from p_detail_produk_promo b left join detail_order a on b.id_produk = a.id_produk left join m_kategori_produk c on b.id_kategori=c.id_kategori left join konsumen_order d on d.id_order=a.id_order left join m_konsumen e on e.id_konsumen=d.id_konsumen left join m_perkiraan_antar f on f.id=d.id_perkiraan_antar
            where  a.id_order='$id_order'
            UNION select a.*,b.nama_produk, b.foto, b.keterangan as keterangan_produk, d.alamat_antar, d.ket_perkiraan_antar as ket_konsumen, f.ket_konsumen as keterangan, d.lat_antar, d.lng_antar, d.status as order_status, nama_kategori, e.kontak, d.charge, d.include_fee, d.include_fee_note, 'regular' as tipe_produk
            from p_detail_produk b left join detail_order a on b.id_produk = a.id_produk left join m_kategori_produk c on b.id_kategori=c.id_kategori left join konsumen_order d on d.id_order=a.id_order left join m_konsumen e on e.id_konsumen=d.id_konsumen left join m_perkiraan_antar f on f.id=d.id_perkiraan_antar
            where  a.id_order='$id_order'
            UNION select a.*,b.nama_produk, b.foto, b.keterangan as keterangan_produk, d.alamat_antar, d.ket_perkiraan_antar as ket_konsumen, f.ket_konsumen as keterangan, d.lat_antar, d.lng_antar, d.status as order_status, nama_kategori, e.kontak, d.charge, d.include_fee, d.include_fee_note, 'histori' as tipe_produk 
            from histori_p_detail_produk b left join detail_order a on b.id_produk = a.id_produk left join m_kategori_produk c on b.id_kategori=c.id_kategori left join konsumen_order d on d.id_order=a.id_order left join m_konsumen e on e.id_konsumen=d.id_konsumen left join m_perkiraan_antar f on f.id=d.id_perkiraan_antar
            where  a.id_order='$id_order' ) T1

            GROUP BY id_produk ORDER BY id_produk
            ";
            // echo $sql; die();
            $query = $this->db->query($sql);
            $results = $query->result();
            if (count($results) > 0) {
                if ($results[0]->ket_konsumen == '') {
                    $results[0]->ket_konsumen = $results[0]->keterangan;
                }
                return $results;
            } 
        
        return $query->result();
    }

    function get_histori_detail_order($id_order){

        $id_order    = $this->db->escape_str($id_order);
        $stmt = "select a.*,b.harga_promo, b.nama_produk, b.foto, b.keterangan as keterangan_produk, d.alamat_antar, d.ket_perkiraan_antar, d.ket_alamat, d.lat_antar, d.lng_antar, d.status as order_status, nama_kategori, e.kontak, d.include_fee, d.include_fee_note, d.charge, d.fee 
            from histori_p_detail_produk b left join detail_order a on b.id_produk = a.id_produk left join m_kategori_produk c on b.id_kategori=c.id_kategori left join konsumen_order d on d.id_order=a.id_order left join m_konsumen e on e.id_konsumen=d.id_konsumen
            where  a.id_order='$id_order'";
            // echo $stmt; die();
        $query = $this->db->query($stmt);
        
        return $query->result();
    }

    function get_detail_order_promo($id_order){

        $id_order    = $this->db->escape_str($id_order);
        $stmt = "select a.*,b.harga_promo, b.nama_produk, b.foto, b.keterangan as keterangan_produk, d.alamat_antar, d.ket_perkiraan_antar, d.ket_alamat, d.lat_antar, d.lng_antar, d.status as order_status, nama_kategori, e.kontak, d.include_fee, d.include_fee_note, d.charge, d.fee 
            from p_detail_produk_promo b left join detail_order a on b.id_produk = a.id_produk left join m_kategori_produk c on b.id_kategori=c.id_kategori left join konsumen_order d on d.id_order=a.id_order left join m_konsumen e on e.id_konsumen=d.id_konsumen
            where  a.id_order='$id_order'";
            // echo $stmt; die();
        $query = $this->db->query($stmt);
        
        return $query->result();
    }

    function create_perkiraan_antar($pesan_perkiraan_antar){
        $this->db->query("insert into m_perkiraan_antar (nama, ket_konsumen, ket_pemilik, status) value ('$pesan_perkiraan_antar','$pesan_perkiraan_antar','$pesan_perkiraan_antar', 1)");
        return $this->db->insert_id();
    }

    function get_perkiraan_antar($id_perkiraan_antar){

        $id_perkiraan_antar    = $this->db->escape_str($id_perkiraan_antar);

        $query = $this->db->query("select * from m_perkiraan_antar where id='$id_perkiraan_antar' and status = 1");
        
        return $query->result();
    }

    function get_all_perkiraan_antar(){

        $query = $this->db->query("select * from m_perkiraan_antar where status = 1");
        
        return $query->result();
    }

    function get_kategori_usaha($id_ekspedisi){

        $query = $this->db->query("select a.*,b.nama as induk_kategori from m_kategori_usaha a left join m_jenis_kategori_usaha b on a.id_jenis=b.id_jenis
            where a.status=1 and b.status=1 and a.id_jenis = (select id_jenis from m_ekspedisi where id_ekspedisi = '$id_ekspedisi') order by a.nama_kategori");
        
        return $query->result();
    }

    function get_kategori_usaha_awal(){

        $query = $this->db->query("select a.*,b.nama as induk_kategori from m_kategori_usaha a,m_jenis_kategori_usaha b 
            where a.id_jenis=b.id_jenis and a.status=1 and b.status=1 order by a.nama_kategori");
        
        return $query->result();
    }

    function get_pemilik_kategori($id_ekspedisi){

        $id_ekspedisi    = $this->db->escape_str($id_ekspedisi);
        
        $query = $this->db->query("select a.*,b.nama_kategori from p_detail_kategori a left join m_kategori_usaha b on  a.id_kategori = b.id_kategori where a.id_ekspedisi='$id_ekspedisi' ");
        
        return $query->result();
    }
    
    function get_pemilik_lokasi($id_ekspedisi){

        $id_ekspedisi    = $this->db->escape_str($id_ekspedisi);
        
        $query = $this->db->query("select * from p_detail_lokasi where id_ekspedisi='$id_ekspedisi'");
        
        return $query->result();
    }
    
    function get_pemilik_produk($id_ekspedisi){

        $id_ekspedisi = $this->db->escape_str($id_ekspedisi);
        
        $query = $this->db->query("select a.*,b.nama_kategori, 0 as is_official from p_detail_produk a left join m_ekspedisi c on c.id_partner=a.id_ekspedisi or c.id_ekspedisi=a.id_ekspedisi left join m_kategori_produk b on a.id_kategori=b.id_kategori where a.id_ekspedisi='$id_ekspedisi' AND a.nama_produk <> '' AND a.harga > 0 AND a.active=1 order by b.nama_kategori");
        
        // $sql = "SELECT a . * , b.nama_kategori,'1' as acuan
        // FROM p_detail_produk a, m_kategori_produk b
        // WHERE a.id_kategori = b.id_kategori
        // AND a.id_ekspedisi = '$id_ekspedisi'
        // AND b.id_ekspedisi = '$id_ekspedisi' AND a.status = '1' 
        // UNION
        // select '' as id_produk,'' as id_ekspedisi,a.id_kategori,'' as nama_produk,0 as stok,'' as keterangan, 
        // 0 as harga, '1' as status, '' as foto, '' as versi, a.nama_kategori, '2' as acuan 
        // from m_kategori_produk a where a.id_ekspedisi='$id_ekspedisi' and a.status = '1' 
        // and 
        // a.id_kategori not in 
        // (select id_kategori from p_detail_produk where id_ekspedisi='$id_ekspedisi' and status = '1') group by a.nama_kategori ORDER BY acuan, nama_kategori";
        // $query = $this->db->query($sql);
        return $query->result();
    }


    function get_pemilik_produk_with_promo($id_ekspedisi, $id_promos, $tipe){

        $id_ekspedisi = $this->db->escape_str($id_ekspedisi);
            $default_value = "null";
            if ($tipe == '2') {
                $default_value = "null";
            } else {
                $default_value = "a.harga_promo";
            }
        
        $sql = "select a.*,  " . $default_value . " as harga_promo, b.nama_kategori as nama_kategori, 0 as is_official from p_detail_produk a left join m_ekspedisi c on c.id_partner=a.id_ekspedisi or c.id_ekspedisi=a.id_ekspedisi left join m_kategori_produk b on a.id_kategori=b.id_kategori where a.id_ekspedisi='$id_ekspedisi' AND a.nama_produk <> '' AND a.harga > 0 AND a.active=1 ";

        foreach ($id_promos as $id_promo) {
            $sql = $sql . " UNION select a.*, a.harga_promo as harga_promo, b.nama_kategori as nama_kategori, 0 as is_official from p_detail_produk a left join m_ekspedisi c on c.id_partner=a.id_ekspedisi or c.id_ekspedisi=a.id_ekspedisi left join m_kategori_produk b on a.id_kategori=b.id_kategori left join produk_promo_diskon ppd on ppd.id_produk=REPLACE(a.id_produk, '".$id_ekspedisi."_', '') where a.id_ekspedisi='$id_ekspedisi' AND a.nama_produk <> '' AND a.harga > 0 AND a.active=1 AND ppd.id_merchant_promo='".$id_promo."' ";
        }

        $sql = $sql . " order by nama_kategori";
        $query = $this->db->query($sql);
        
        return $query->result();
    }

    function get_pemilik_produk_by_pemilik($id_ekspedisi){

        $id_ekspedisi = $this->db->escape_str($id_ekspedisi);
        
        $query = $this->db->query("select a.*, REPLACE(a.id_produk, '".$id_ekspedisi."_', '') AS id_produk, b.nama_kategori from p_detail_produk a left join m_ekspedisi c on c.id_partner=a.id_ekspedisi or c.id_ekspedisi=a.id_ekspedisi left join m_kategori_produk b on a.id_kategori=b.id_kategori where a.id_ekspedisi='$id_ekspedisi' AND c.nama <> '' order by b.nama_kategori");
        
        // $sql = "SELECT a . * , b.nama_kategori,'1' as acuan
        // FROM p_detail_produk a, m_kategori_produk b
        // WHERE a.id_kategori = b.id_kategori
        // AND a.id_ekspedisi = '$id_ekspedisi'
        // AND b.id_ekspedisi = '$id_ekspedisi' AND a.status = '1' 
        // UNION
        // select '' as id_produk,'' as id_ekspedisi,a.id_kategori,'' as nama_produk,0 as stok,'' as keterangan, 
        // 0 as harga, '1' as status, '' as foto, '' as versi, a.nama_kategori, '2' as acuan 
        // from m_kategori_produk a where a.id_ekspedisi='$id_ekspedisi' and a.status = '1' 
        // and 
        // a.id_kategori not in 
        // (select id_kategori from p_detail_produk where id_ekspedisi='$id_ekspedisi' and status = '1') group by a.nama_kategori ORDER BY acuan, nama_kategori";
        // $query = $this->db->query($sql);
        return $query->result();
    }

    function get_pemilik($id_ekspedisi){

        $id_ekspedisi    = $this->db->escape_str($id_ekspedisi);
        
        // $query = $this->db->query("select a.*,b.tipe as tipe_usaha,c.nama_kategori 
        //     from m_ekspedisi a,m_jenis_kategori_usaha b,m_kategori_usaha c 
        //     where a.id_jenis=b.id_jenis and c.id_kategori=a.id_kategori and
        //     a.id_ekspedisi='$id_ekspedisi'");
        $sql = "select a.*,b.tipe as tipe_usaha  
            from m_ekspedisi a left join m_jenis_kategori_usaha b on (a.tipe=2 OR a.id_jenis=b.id_jenis)
            where a.id_ekspedisi='$id_ekspedisi'";
       // echo $sql; die();
       $stmt = $sql;
        $query = $this->db->query($stmt);

        return $query->result();
    }


    function get_toko($id_pemilik){

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        
        // $query = $this->db->query("select a.*,b.tipe as tipe_usaha,c.nama_kategori 
        //     from m_ekspedisi a,m_jenis_kategori_usaha b,m_kategori_usaha c 
        //     where a.id_jenis=b.id_jenis and c.id_kategori=a.id_kategori and
        //     a.id_ekspedisi='$id_ekspedisi'");
        $sql = "select a.*,b.tipe as tipe_usaha  
            from m_pemilik_usaha a left join m_jenis_kategori_usaha b on (a.tipe=2 OR a.id_jenis=b.id_jenis)
            where a.id_pemilik='$id_pemilik'";
       // echo $sql; die();
       $stmt = $sql;
        $query = $this->db->query($stmt);

        return $query->result();
    }

    function get_preactive_pemilik($id_ekspedisi){

        $id_ekspedisi    = $this->db->escape_str($id_ekspedisi);
        
        // $query = $this->db->query("select a.*,b.tipe as tipe_usaha,c.nama_kategori 
        //     from m_ekspedisi a,m_jenis_kategori_usaha b,m_kategori_usaha c 
        //     where a.id_jenis=b.id_jenis and c.id_kategori=a.id_kategori and
        //     a.id_ekspedisi='$id_ekspedisi'");
       
        $query = $this->db->query("select a.*,b.tipe as tipe_usaha
            from m_ekspedisi a,m_jenis_kategori_usaha b,
            where a.id_jenis=b.id_jenis and 
            a.id_ekspedisi='$id_ekspedisi'");

        return $query->row();
    }

    function get_preactive_produk($id_ekspedisi){

        $id_ekspedisi    = $this->db->escape_str($id_ekspedisi);
        
        // $query = $this->db->query("select a.*,b.tipe as tipe_usaha,c.nama_kategori 
        //     from m_ekspedisi a,m_jenis_kategori_usaha b,m_kategori_usaha c 
        //     where a.id_jenis=b.id_jenis and c.id_kategori=a.id_kategori and
        //     a.id_ekspedisi='$id_ekspedisi'");
       
        $query = $this->db->query("select * 
            from p_detail_produk 
            where  id_ekspedisi='$id_ekspedisi'");

        return $query->row();
    }

    function proses_order($id_order,$id_perkiraan_antar, $pesan_perkiraan_antar){
        
        $id_order   = $this->db->escape_str($id_order);
        $id_perkiraan_antar   = $this->db->escape_str($id_perkiraan_antar);
        $stmt = "update konsumen_order set `status`=2, tanggal_proses = '".date('Y-m-d H:i:s')."', `id_perkiraan_antar`='$id_perkiraan_antar', `ket_perkiraan_antar`='$pesan_perkiraan_antar' where `id_order`='".$id_order."'";
        $this->db->query($stmt);    

        return $stmt;
    }

    function selesai_order($id_order){
        
        // $id_order   = $this->db->escape_str($id_order);
        
        $stmt = "update konsumen_order set `status`=4 where `id_order`='" . $id_order ."'";
        $this->db->query($stmt);
        $fee = "";
        $id_ekspedisi = "";
        
        $query = $this->db->query("select * from `konsumen_order` where `id_order` = '". $id_order . "'")->first_row();
        
        $fee = $query->fee;
        //$id_ekspedisi = $query->id_ekspedisi;

        
        //$this->db->query("update `m_ekspedisi` set `kredit` = `kredit` - " . $fee . " where `id_ekspedisi` = '".$id_ekspedisi."'"); 

        return $fee;       
    }

     function get_provinsi(){
        $query = $this->db->query("select * from m_provinsi where status=1 order by nama");
        return $query->result();
    }

    function get_kota($id_provinsi){
        $id_provinsi    = $this->db->escape_str($id_provinsi);
        
        $query = $this->db->query("select * from m_kota where id_provinsi = '$id_provinsi' and status=1 order by nama");
        return $query->result();
    }

    function daftar($id_ekspedisi,$kontak,$password,$username,$kode,$id_provinsi,$id_kota, $radius){

        $id_ekspedisi = $this->db->escape_str($id_ekspedisi);
        $kontak     = $this->db->escape_str($kontak);
        $password       = hash("sha512",$password);
        $username       = $this->db->escape_str($username);
        $kode           = $this->db->escape_str($kode);
        $radius      = $this->db->escape_str($radius);
        $id_provinsi    = $this->db->escape_str($id_provinsi);
        $id_kota        = $this->db->escape_str($id_kota);
        $lat = "-6.2293866";
        $lng = "106.6890876";
        
        $sql_stmt = "INSERT INTO `m_ekspedisi`(`id_ekspedisi`, `username`, `email`, `kontak`, `password`, `nama`, `jargon`, `alamat`, 
            `lat`, `lng`, `radius`, `foto`, `kode`, `tanggal_update`, `token`, `id_kategori`,id_provinsi,id_kota,level,id_jenis,tipe,kredit, waktu_buka, waktu_tutup) 
            select '$id_ekspedisi','$username','','$kontak','$password','','','','$lat','$lng','$radius','','$kode',now(),'','0','$id_provinsi','$id_kota',1,1,1,default_kredit, '08:00:00', '20:00:00'
            from setting_umum where id = '1'";
		$this->db->query($sql_stmt);
		$affected_rows = $this->db->affected_rows();

        $this->db->query("delete from kode_sms where kode = '$kode'");
        
        return $affected_rows;
    }


    function daftar_pre($id_ekspedisi,$kontak,$password,$username,$kode,$id_provinsi,$id_kota, $radius){

        $id_ekspedisi = $this->db->escape_str($id_ekspedisi);
        $kontak     = $this->db->escape_str($kontak);
        $password       = hash("sha512",$password);
        $username       = $this->db->escape_str($username);
        $kode           = $this->db->escape_str($kode);
        $radius      = $this->db->escape_str($radius);
        $id_provinsi    = $this->db->escape_str($id_provinsi);
        $id_kota        = $this->db->escape_str($id_kota);
        $lat = "-6.2293866";
        $lng = "106.6890876";
        
        $sql_stmt = "INSERT INTO `m_ekspedisi`(`id_ekspedisi`, `username`, `email`, `kontak`, `password`, `nama`, `jargon`, `alamat`, 
            `lat`, `lng`, `radius`, `foto`, `kode`, `tanggal_update`, `token`, `id_kategori`, `status`,id_provinsi,id_kota,level,id_jenis,tipe,kredit) 
            select '$id_ekspedisi','$username','','$kontak','$password','','','','$lat','$lng','$radius','','$kode',now(),'','0', '0', '$id_provinsi','$id_kota',1,0,1,default_kredit 
            from setting_umum where id = '1'";
        $this->db->query($sql_stmt);
        $affected_rows = $this->db->affected_rows();

        $this->db->query("delete from kode_sms where kode = '$kode'");
        
        return $affected_rows;
    }


    function tambah_toko_preaktif($id_ekspedisi,$id_partner,$kontak,$kontak_sms, $kontak_sms2, $password,$username,$kode,$id_provinsi,$id_kota, $radius, $min_antar, $fee, $email){

        $id_ekspedisi = $this->db->escape_str($id_ekspedisi);
        $id_partner = $this->db->escape_str($id_partner);
        $kontak     = $this->db->escape_str($kontak);
        $kontak_sms     = $this->db->escape_str($kontak_sms);
        $kontak_sms2     = $this->db->escape_str($kontak_sms2);
        $password       = hash("sha512",$password);
        $username       = $this->db->escape_str($username);
        $kode           = $this->db->escape_str($kode);
        $radius      = $this->db->escape_str($radius);
        $min_antar      = $this->db->escape_str($min_antar);
        $id_provinsi    = $this->db->escape_str($id_provinsi);
        $id_kota        = $this->db->escape_str($id_kota);
        $email        = $this->db->escape_str($email);
        $lat = "-6.2293866";
        $lng = "106.6890876";
        
        $sql_stmt = "INSERT INTO `m_ekspedisi`(`id_ekspedisi`, `id_partner`, `username`, `email`, `kontak`, `kontak_sms`, `kontak_sms2`, `password`, `nama`, `jargon`, `alamat`, 
            `lat`, `lng`, `radius`, `foto`, `kode`, `tanggal_update`, `token`, `id_kategori`, `status` ,id_provinsi,id_kota,level,id_jenis,tipe,kredit, min_antar, `fee`) 
            select '$id_ekspedisi', '$id_partner', '$username','$email','$kontak','$kontak_sms','$kontak_sms2','$password','','','','$lat','$lng','$radius','','$kode',now(),'','0', 0, '$id_provinsi','$id_kota',1,1,2,default_kredit, $min_antar, $fee
            from setting_umum where id = '1'";
        $this->db->query($sql_stmt);
        $affected_rows = $this->db->affected_rows();

        $this->db->query("delete from kode_sms where kode = '$kode'");
        
        return $affected_rows;
    }


    function tambah_toko($id_ekspedisi,$id_partner,$kontak,$kontak_sms, $kontak_sms2, $password,$username,$kode,$id_provinsi,$id_kota, $radius, $min_antar, $fee, $nama=''){

        $id_ekspedisi = $this->db->escape_str($id_ekspedisi);
        $id_partner = $this->db->escape_str($id_partner);
        $nama = $this->db->escape_str($nama);
        $kontak     = $this->db->escape_str($kontak);
        $kontak_sms     = $this->db->escape_str($kontak_sms);
        $kontak_sms2     = $this->db->escape_str($kontak_sms2);
        $password       = hash("sha512",$password);
        $username       = $this->db->escape_str($username);
        $kode           = $this->db->escape_str($kode);
        $radius      = $this->db->escape_str($radius);
        $min_antar      = $this->db->escape_str($min_antar);
        $id_provinsi    = $this->db->escape_str($id_provinsi);
        $id_kota        = $this->db->escape_str($id_kota);
        $lat = "-6.2293866";
        $lng = "106.6890876";
        
        $sql_stmt = "INSERT INTO `m_ekspedisi`(`id_ekspedisi`, `id_partner`, `username`, `email`, `kontak`, `kontak_sms`, `kontak_sms2`, `password`, `nama`, `jargon`, `alamat`, 
            `lat`, `lng`, `radius`, `foto`, `kode`, `tanggal_update`, `token`, `id_kategori`, `status`,id_provinsi,id_kota,level,id_jenis,tipe,kredit, min_antar, `fee`, waktu_buka, waktu_tutup) 
            select '$id_ekspedisi', '$id_partner', '$username','','$kontak','$kontak_sms','$kontak_sms2','$password','$nama','','','$lat','$lng','$radius','','$kode',now(),'','0', '1', '$id_provinsi','$id_kota',1,1,2,default_kredit, 0, 0, '08:00:00', '20:00:00' 
            from setting_umum where id = '1'";

            // echo $sql_stmt; die();
        $this->db->query($sql_stmt);
        $affected_rows = $this->db->affected_rows();

        $this->db->query("delete from kode_sms where kode = '$kode'");
        
        return $affected_rows;
    }

    function tambah_toko_batch($id_ekspedisi,$id_partner,$kontak,$kontak_sms, $kontak_sms2, $password,$username,$kode,$id_provinsi,$id_kota, $radius, $min_antar, $fee){

        $id_ekspedisi = $this->db->escape_str($id_ekspedisi);
        $id_partner = $this->db->escape_str($id_partner);
        $kontak     = $this->db->escape_str($kontak);
        $kontak_sms     = $this->db->escape_str($kontak_sms);
        $kontak_sms2     = $this->db->escape_str($kontak_sms2);
        $password       = hash("sha512",$password);
        $username       = $this->db->escape_str($username);
        $kode           = $this->db->escape_str($kode);
        $radius      = $this->db->escape_str($radius);
        $min_antar      = $this->db->escape_str($min_antar);
        $id_provinsi    = $this->db->escape_str($id_provinsi);
        $id_kota        = $this->db->escape_str($id_kota);
        $lat = "-6.2293866";
        $lng = "106.6890876";
        
        $sql_stmt = "INSERT INTO `m_ekspedisi`(`id_ekspedisi`, `id_partner`, `username`, `email`, `kontak`, `kontak_sms`, `kontak_sms2`, `password`, `nama`, `jargon`, `alamat`, 
            `lat`, `lng`, `radius`, `foto`, `kode`, `tanggal_update`, `token`, `id_kategori`, `status`,id_provinsi,id_kota,level,id_jenis,tipe,kredit, min_antar, `fee`, waktu_buka, waktu_tutup) 
            select '$id_ekspedisi', '$id_partner', '$username','','$kontak','$kontak_sms','$kontak_sms2','$password','','','','$lat','$lng','$radius','','$kode',now(),'','0', '1', '$id_provinsi','$id_kota',1,1,3,default_kredit, $min_antar, $fee, '08:00:00', '20:00:00' 
            from setting_umum where id = '1'";

            // echo $sql_stmt; die();
        $this->db->query($sql_stmt);
        $affected_rows = $this->db->affected_rows();

        $this->db->query("delete from kode_sms where kode = '$kode'");
        
        return $affected_rows;
    }


    function insert_pemilik($id_ekspedisi,$id_jenis, $kontak,$password,$username,$nama, $jargon, $alamat, $kode, $id_provinsi, $id_kota, $id_kategori, $radius, $lat, $lng) {

        $id_ekspedisi = $this->db->escape_str($id_ekspedisi);
        $kontak     = $this->db->escape_str($kontak);
        $password       = hash("sha512",$password);
        $username       = $this->db->escape_str($username);
        $jargon       = $this->db->escape_str($jargon);
        $kode           = $this->db->escape_str($kode);
        $radius      = $this->db->escape_str($radius);
        $id_provinsi    = $this->db->escape_str($id_provinsi);
        $id_kota        = $this->db->escape_str($id_kota);
        $alamat       = $this->db->escape_str($alamat);
        
        $sql_stmt = "INSERT INTO `m_ekspedisi`(`id_ekspedisi`, `username`, `email`, `kontak`, `password`, `nama`, `jargon`, `alamat`, 
            `lat`, `lng`, `radius`, `foto`, `kode`, `tanggal_update`, `token`, `id_kategori`, `status`,id_provinsi,id_kota,level,`id_jenis`,tipe,kredit) 
            select '$id_ekspedisi','$username','','$kontak','$password','$nama','$jargon','$alamat','$lat','$lng','$radius','','$kode',now(),'', '0', '0','$id_provinsi','$id_kota',1,$id_jenis,1,default_kredit 
            from setting_umum where id = '1'";

        $this->db->query($sql_stmt);
        
        return $this->db->affected_rows() == 1;
    }

    function isi_profil_awal($id_ekspedisi,$nama,$alamat,$jargon, $radius, $kontak_sms, $kontak_sms2, $fee, $include_fee='1', $include_fee_note=''){

        $id_ekspedisi    = $this->db->escape_str($id_ekspedisi);
        $nama    = $this->db->escape_str($nama);
        $alamat    = $this->db->escape_str($alamat);
        $jargon    = $this->db->escape_str($jargon);
        $fee    = $this->db->escape_str($fee);
        $radius    = $this->db->escape_str($radius);
        $include_fee    = $this->db->escape_str($include_fee);
        $include_fee_note    = $this->db->escape_str($include_fee_note);
        
        $this->db->query("update m_ekspedisi set nama = '$nama', alamat = '$alamat', jargon = '$jargon', radius='$radius', kontak_sms='$kontak_sms', kontak_sms2='$kontak_sms2', fee=$fee, include_fee='$include_fee', include_fee_note='$include_fee_note' where id_ekspedisi='$id_ekspedisi'");
    }


    function update_profile($id_ekspedisi, $nama, $alamat, $kontak, $fee, $include_fee='1', $include_fee_note=''){

        $id_ekspedisi    = $this->db->escape_str($id_ekspedisi);
        $nama    = $this->db->escape_str($nama);
        $alamat    = $this->db->escape_str($alamat);
        $kontak    = $this->db->escape_str($kontak);
        $fee    = $this->db->escape_str($fee);
        $include_fee    = $this->db->escape_str($include_fee);
        $include_fee_note    = $this->db->escape_str($include_fee_note);

        $stmt = "update m_ekspedisi set nama = '$nama', alamat = '$alamat', kontak='$kontak', fee=$fee, include_fee='$include_fee', include_fee_note='$include_fee_note' where id_ekspedisi='$id_ekspedisi'";
        // echo $stmt; die();
        
        $this->db->query($stmt);
    }


    function tambah_ongkirs($id_ekspedisi, $ongkirs) {
        $this->db->query("delete from m_ongkir_kurir where id_ekspedisi='" . $id_ekspedisi ."'");
        foreach ($ongkirs as $ongkir) {
            $this->db->query("insert into m_ongkir_kurir (id_ekspedisi, jarak_min, jarak_max, fee) VALUE('" . $id_ekspedisi . "', " 
                . $ongkir->radius_min . ", " . $ongkir->radius_max . ", " . $ongkir->fee . ")");
        }
    }


    function set_alamat_min_antar($id_ekspedisi,$alamat,$min_antar){

        $id_ekspedisi    = $this->db->escape_str($id_ekspedisi);
        $min_antar    = $this->db->escape_str($min_antar);
        $alamat    = $this->db->escape_str($alamat);
        
        $this->db->query("update m_ekspedisi set alamat = '$alamat', min_antar='$min_antar' where id_ekspedisi = '$id_ekspedisi'");
    }

    function set_fee($id_ekspedisi, $fee){

        $id_ekspedisi    = $this->db->escape_str($id_ekspedisi);
        $fee    = $this->db->escape_str($fee);
        
        $this->db->query("update m_ekspedisi set fee=$fee where id_ekspedisi = '$id_ekspedisi'");
    }

    function get_kurir_fee($id_ekspedisi){

        $id_ekspedisi    = $this->db->escape_str($id_ekspedisi);
        
        $result = $this->db->query("select fee from m_ekspedisi where id_ekspedisi = '$id_ekspedisi' limit 1");
        return $result->first_row();
    }

    function set_radius($id_ekspedisi, $radius){

        $id_ekspedisi    = $this->db->escape_str($id_ekspedisi);
        $radius    = $this->db->escape_str($radius);
        
        $this->db->query("update m_ekspedisi set radius='$radius' where id_ekspedisi = '$id_ekspedisi'");
    }

     function set_no_telp($id_ekspedisi, $kontak, $kontak_sms, $kontak_sms2) {

        $id_ekspedisi    = $this->db->escape_str($id_ekspedisi);
        $kontak    = $this->db->escape_str($kontak);
        $kontak_sms    = $this->db->escape_str($kontak_sms);
        $kontak_sms2    = $this->db->escape_str($kontak_sms2);

        $this->db->query("update m_ekspedisi set kontak='$kontak', kontak_sms='$kontak_sms', kontak_sms2='$kontak_sms2'  where id_ekspedisi = '$id_ekspedisi'");
     }

    function isi_lokasi_awal($id_ekspedisi,$lat,$lng,$nama_lokasi){

        $id_ekspedisi    = $this->db->escape_str($id_ekspedisi);
        $lat    = $this->db->escape_str($lat);
        $lng    = $this->db->escape_str($lng);
        $nama_lokasi    = $this->db->escape_str($nama_lokasi);
        
        $this->db->query("update m_ekspedisi set lat = '$lat',lng = '$lng' where id_ekspedisi = '$id_ekspedisi'");

        $lokasi = $this->db->query("select * from p_detail_lokasi where id_ekspedisi='$id_ekspedisi' LIMIT 1")->row();

        if (empty($lokasi)) {
            $id_lokasi = date("YmdHis").$id_ekspedisi;
            $this->db->query("insert into p_detail_lokasi (id_lokasi,id_ekspedisi,nama,lat,lng,jenis_lokasi) values ('$id_lokasi','$id_ekspedisi','$nama_lokasi','$lat','$lng','1')");
        } else {
            $this->db->query("update p_detail_lokasi set id_ekspedisi='$id_ekspedisi', nama='$nama_lokasi', lat='$lat',lng='$lng', jenis_lokasi='1' where id_ekspedisi='$id_ekspedisi'");
        }
    }

    function get_id_ekspedisi_by_no_telp_and_username($no_telp, $username){
        
        $no_telp   = $this->db->escape_str($no_telp);
        $username   = $this->db->escape_str($username);

        $query = $this->db->query("select * from m_ekspedisi where kontak = '$no_telp' or username = '$username'");
        return $query->first_row();
    }

    function get_id_ekspedisi_by_no_telp($no_telp){
        
        $no_telp   = $this->db->escape_str($no_telp);

        $query = $this->db->query("select * from m_ekspedisi where kontak = '$no_telp'");
        return $query->first_row();
    }

    function new_password($id_ekspedisi,$password){

        $id_ekspedisi    = $this->db->escape_str($id_ekspedisi);
        $password   = hash("sha512",$password);
       
        $this->db->query("update m_ekspedisi set password = '$password' where id_ekspedisi = '$id_ekspedisi'");
    }

    function check_password($id_ekspedisi,$password){
        $id_ekspedisi    = $this->db->escape_str($id_ekspedisi);
        $password   = hash("sha512",$password);

        return $this->db->query("select * from m_ekspedisi where id_ekspedisi = '$id_ekspedisi' and password = '$password'")->row();
    }

    function copy_to_histori($id_ekspedisi){
        
        $id_ekspedisi   = $this->db->escape_str($id_ekspedisi);

        $this->db->query("INSERT INTO `histori_m_ekspedisi` SELECT * FROM `m_ekspedisi` WHERE id_ekspedisi = '$id_ekspedisi'");
    }

    function hapus_akun($id_ekspedisi){

        $this->db->query("DELETE FROM `m_ekspedisi` WHERE id_ekspedisi = '$id_ekspedisi'");
    }

    function get_penjualan_harian($id_ekspedisi,$tanggal){
        $query = $this->db->query("select a.id_order,a.tanggal_order,sum(a.fee) as fee, sum(a.charge) as charge,a.id_ekspedisi,b.nama_kategori,
        COALESCE(f.nama_produk, COALESCE(c.nama_produk, e.nama_produk)) as nama_produk, COALESCE(f.id_produk, COALESCE(c.id_produk, e.id_produk)) as id_produk,  COALESCE(f.keterangan, COALESCE(c.keterangan, e.keterangan)) as keterangan, COALESCE(f.foto, COALESCE(c.foto, e.foto)) as foto,
                    sum(d.kuantitas) as total_kuantitas,sum(d.subtotal) as subtotal
                    from
                    konsumen_order a 
                    left join detail_order d on a.id_order = d.id_order 
                    left join p_detail_produk c on c.id_produk = d.id_produk 
                    left join histori_p_detail_produk e on e.id_produk = d.id_produk 
                    left join p_detail_produk_promo f on f.id_produk = d.id_produk 
                    left join m_kategori_produk b on (b.id_kategori = c.id_kategori or b.id_kategori = e.id_kategori)
                    where
                    DATE(a.tanggal_order) = '$tanggal'
                    and
                    a.id_ekspedisi = '$id_ekspedisi'
                    and
                    a.status = 4
                    group by c.id_produk
                    order by b.nama_kategori");
        return $query->result();
    }
    
    function get_penjualan_bulanan($id_ekspedisi,$bulan,$tahun){
        $stmt = "select a.id_order,a.tanggal_order,sum(a.fee) as fee, sum(a.charge) as charge,a.id_ekspedisi,b.nama_kategori, COALESCE(f.nama_produk, COALESCE(c.nama_produk, e.nama_produk)) as nama_produk, COALESCE(f.id_produk, COALESCE(c.id_produk, e.id_produk)) as id_produk,  COALESCE(f.keterangan, COALESCE(c.keterangan, e.keterangan)) as keterangan,  COALESCE(f.foto, COALESCE(c.foto, e.foto)) as foto,
                    sum(d.kuantitas) as total_kuantitas,sum(d.subtotal) as subtotal
                    from
                    konsumen_order a 
                    left join detail_order d on a.id_order = d.id_order 
                    left join p_detail_produk c on c.id_produk = d.id_produk 
                    left join histori_p_detail_produk e on e.id_produk = d.id_produk 
                    left join p_detail_produk_promo f on f.id_produk = d.id_produk 
                    left join m_kategori_produk b on (b.id_kategori = c.id_kategori or b.id_kategori = e.id_kategori)
                    where MONTH(a.tanggal_order) = '$bulan' and YEAR(a.tanggal_order) = '$tahun'
                    and
                    a.id_ekspedisi = '$id_ekspedisi'
                    and
                    a.status = 4
                    group by c.id_produk
                    order by b.nama_kategori";
                    // echo $stmt;die();
        $query = $this->db->query($stmt);
        return $query->result();
    }
    
    function get_penjualan_tahunan($id_ekspedisi,$tahun){
        $stmt = "select a.id_order,a.tanggal_order,sum(a.fee) as fee, sum(a.charge) as charge,a.id_ekspedisi,b.nama_kategori,
        COALESCE(f.nama_produk, COALESCE(c.nama_produk, e.nama_produk)) as nama_produk, COALESCE(f.id_produk, COALESCE(c.id_produk, e.id_produk)) as id_produk,  COALESCE(f.keterangan, COALESCE(c.keterangan, e.keterangan)) as keterangan,  COALESCE(f.foto, COALESCE(c.foto, e.foto)) as foto,
                    sum(d.kuantitas) as total_kuantitas,sum(d.subtotal) as subtotal
                    from
                    konsumen_order a 
                    left join detail_order d on a.id_order = d.id_order 
                    left join p_detail_produk c on c.id_produk = d.id_produk 
                    left join histori_p_detail_produk e on e.id_produk = d.id_produk 
                    left join p_detail_produk_promo f on f.id_produk = d.id_produk 
                    left join m_kategori_produk b on (b.id_kategori = c.id_kategori or b.id_kategori = e.id_kategori)
                    where
                    YEAR(a.tanggal_order) = '$tahun'
                    and
                    a.id_ekspedisi = '$id_ekspedisi'
                    and
                    a.status = 4
                    group by d.id_produk
                    order by b.nama_kategori";
                    // echo $stmt; die();
        $query = $this->db->query($stmt);

        
        
        return $query->result();
    }

    function set_foto($id_ekspedisi,$foto){
        
        $id_ekspedisi   = $this->db->escape_str($id_ekspedisi);
        $foto   = $this->db->escape_str($foto);

        $this->db->query("update m_ekspedisi set foto = '$foto' where id_ekspedisi = '$id_ekspedisi'");
    }

    function set_foto_produk($id_produk,$foto){
        
        $id_produk   = $this->db->escape_str($id_produk);
        $foto   = $this->db->escape_str($foto);

        $this->db->query("update p_detail_produk set foto = '$foto', versi = versi+1 where id_produk = '$id_produk'");
    }

    function set_foto_avatar($id_ekspedisi,$foto){
        
        $id_ekspedisi   = $this->db->escape_str($id_ekspedisi);
        $foto   = $this->db->escape_str($foto);

        $this->db->query("update m_ekspedisi set avatar = '$foto', versi_avatar = versi_avatar+1 where id_ekspedisi = '$id_ekspedisi'");
    }

    function get_foto($id_ekspedisi){
        
        $id_ekspedisi   = $this->db->escape_str($id_ekspedisi);

        $query = $this->db->query("select foto,versi from m_ekspedisi where id_ekspedisi = '$id_ekspedisi'");

        return $query->first_row();
    }

    function get_foto_avatar($id_ekspedisi){
        
        $id_ekspedisi   = $this->db->escape_str($id_ekspedisi);

        $query = $this->db->query("select avatar,versi_avatar from m_ekspedisi where id_ekspedisi = '$id_ekspedisi'");

        return $query->first_row();
    }

    function get_foto_produk($id_produk){
        
        $id_ekspedisi   = $this->db->escape_str($id_produk);

        $query = $this->db->query("select foto,versi from p_detail_produk where id_produk = '$id_produk'");

        return $query->first_row();
    }

    function get_produk_by_id($id_produk){
        
        $id_ekspedisi   = $this->db->escape_str($id_produk);

        $query = $this->db->query("select * from p_detail_produk where id_produk = '$id_produk'");

        return $query->first_row();
    }

    function get_produk_umkm_by_id($id_produk){
        
        $id_ekspedisi   = $this->db->escape_str($id_produk);

        $query = $this->db->query("select * from p_detail_produk_umkm where id_produk = '$id_produk'");

        return $query->first_row();
    }

    function notif_get_pemilik($id_ekspedisi){
        
        $query = $this->db->query("select * from m_ekspedisi where id_ekspedisi = '$id_ekspedisi'");
        return $query->result();
    }

    function notif_get_konsumen($id_order){
        
        $query = $this->db->query("select a.* 
            from m_konsumen a, konsumen_order b
            where a.id_konsumen = b.id_konsumen and b.id_order = '$id_order'");
        return $query->result();
    }

    function notif_get_order($id_order){
        $query = $this->db->query("select ko.id_order, ko.total, ko.status, ko.id_perkiraan_antar, ko.tanggal_order
            from konsumen_order ko
            where ko.id_order = '$id_order'");
        return $query->result();
    }

    function update_token($id_ekspedisi,$token){
        $id_ekspedisi   = $this->db->escape_str($id_ekspedisi);
        $token   = $this->db->escape_str($token);
        
        $query = $this->db->query("update m_ekspedisi set token='$token' where id_ekspedisi='$id_ekspedisi'");
    }

    function get_order_dari_notif($id_order){
        
        $id_order    = $this->db->escape_str($id_order);
        
        $query = $this->db->query("select a.*,b.nama,b.alamat,b.kontak,b.lat,b.lng,b.foto,b.versi, c.nama as nama_kota, e.nama as nama_merchant, e.lat as lat_merchant, e.lng as lng_merchant, e.alamat as alamatmerchant, e.foto as fotomerchant 
                    from konsumen_order a left join m_pemilik_usaha e ON a.id_pemilik=e.id_pemilik left join m_konsumen b ON a.id_konsumen=b.id_konsumen left join m_kota c ON c.id = b.id_kota 
                    where a.id_order='$id_order' 
                    order by a.status asc 
                    ");
        
        return $query->result();
    }

    function get_konsumen_order($id_order){
        
        $id_order   = $this->db->escape_str($id_order);

        $query = $this->db->query("select * from konsumen_order where id_order='$id_order' LIMIT 1")->row();

        // echo $this->db->last_query(); die();

        return $query;
    }


    function update_provinsi_kota($id_konsumen,$id_provinsi,$id_kota){

        $id_konsumen    = $this->db->escape_str($id_konsumen);
        $id_provinsi    = $this->db->escape_str($id_provinsi);
        $id_kota    = $this->db->escape_str($id_kota);
       
        $this->db->query("update m_ekspedisi set id_provinsi = '$id_provinsi', id_kota = '$id_kota' where id_ekspedisi = '$id_konsumen'");
    }

    
    function update_kategori_produk($id_kategori,$nama_kategori){        

        $id_kategori    = $this->db->escape_str($id_kategori);
        $nama_kategori    = $this->db->escape_str($nama_kategori);

        $query   = "update m_kategori_produk set nama_kategori = '$nama_kategori' 
                            where id_kategori = '$id_kategori'";
        
        $this->db->query($query);
    }

    function delete_kategori_produk($id_kategori){

        $id_kategori    = $this->db->escape_str($id_kategori);
        
        $this->db->query("update m_kategori_produk set status = '2' where id_kategori = '$id_kategori'");        
        $this->db->query("update p_detail_produk set status = '2' where id_kategori = '$id_kategori'");
    }

    // function tambah_ongkirs($id_ekspedisi, $ongkirs) {
    //     $this->db->query("delete from m_ongkir where id_ekspedisi='" . $id_ekspedisi ."'");
    //     foreach ($ongkirs as $ongkir) {
    //         $this->db->query("insert into m_ongkir (id_ekspedisi, jarak_min, jarak_max, fee) VALUE('" . $id_ekspedisi . "', " 
    //             . $ongkir->radius_min . ", " . $ongkir->radius_max . ", " . $ongkir->fee . ")");
    //     }
    // }


    function get_ongkirs($id_ekspedisi) {
        return $this->db->query("select * from m_ongkir_kurir where id_ekspedisi='" . $id_ekspedisi . "'")->result();
    }

    function insert_produk_promo_batch($id_ekspedisi, $rows){    

        $query_produk   = "INSERT INTO `m_promo_produk` (`id_merchant_promo`, `id_barcode`, `qty`, `id_barcode2`, `qty2`) VALUES ";

        // echo $k; die();

        $i = 0;
        foreach ($rows as $produk) {
                $j=0;

                $paramBuilder = "";

                $paramBuilder = $paramBuilder . "'".$produk->barcode . "', ";
                $paramBuilder = $paramBuilder . "'".$produk->min_qty . "', ";
                $paramBuilder = $paramBuilder . "'".$produk->barcode_barang_promo . "', ";
                $paramBuilder = $paramBuilder . "'".$produk->qty_promo . "'";

                $query_produk = $query_produk . "(";
                $query_produk = $query_produk . "'".$id_ekspedisi . "', ";
                $query_produk = $query_produk . $paramBuilder . ")";

            $i++;
        }
        $query_produk = str_replace(")(", "),(", $query_produk);

        // echo $query_produk; die();
        
        $this->db->query($query_produk);
    }


    function set_harga_promo_batch($id_ekspedisi, $rows, $id_merchant_promo) { 

        $insert = "INSERT INTO p_detail_produk (id_produk, id_ekspedisi, nama_produk, id_kategori, keterangan, harga, harga_promo, min_antar, active) VALUES ";
        $update = "ON DUPLICATE KEY UPDATE harga_promo=VALUES(harga_promo), min_antar=VALUES(min_antar), active=1";

        foreach ($rows as $row) {
                $insert = $insert . "('" . $id_ekspedisi . "_" . $row->barcode . "', '" . $id_ekspedisi . "', '', 1, '', 0, " . $row->harga_promo . ", " . $row->min_qty . ", 1), ";
            // $i++;
        }
        $stmt = $insert . $update;
        $stmt = str_replace("), ON DUPLICATE KEY UPDATE", ") ON DUPLICATE KEY UPDATE", $stmt);

        // echo $stmt; die();
        $this->db->query($stmt);
        
    }

    function insert_produk_promo_diskon_batch($rows, $id_merchant_promo) { 
       
        $update2 = "XXX";

        $insert2 = "INSERT INTO produk_promo_diskon (id_produk, id_merchant_promo) VALUES ";
        foreach ($rows as $row) {
            $insert2 = $insert2 . "('" . $row->barcode . "', " . $id_merchant_promo . "), ";
            // $i++;
        }

        $stmt2 = $insert2 . $update2;
        $stmt2 = str_replace("), XXX", ")", $stmt2);
        // echo $stmt; die();
        $this->db->query($stmt2);
        
    }

    function update_waktu_buka_tutup($id_ekspedisi, $waktu_buka, $waktu_tutup) {


        $id_ekspedisi    = $this->db->escape_str($id_ekspedisi);
        $waktu_buka = $this->db->escape_str($waktu_buka);
        $waktu_tutup = $this->db->escape_str($waktu_tutup);
        
        $sql = "update m_ekspedisi set waktu_buka = '$waktu_buka', waktu_tutup='$waktu_tutup' where id_ekspedisi = '$id_ekspedisi'";
        // echo $sql; die();
        $this->db->query($sql);      
    }

}