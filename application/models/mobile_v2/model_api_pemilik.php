<?php  
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_api_pemilik extends CI_Model 
{

    function __construct()
    {
            parent::__construct();
            $this->load->database();
    }


    function check_session($id_pemilik, $token){
        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $token    = $this->db->escape_str($token);
        
        $query = $this->db->query("select * from m_pemilik_usaha where id_pemilik = '$id_pemilik' AND token='$token'");
        return $query->first_row();
    }

    function kirim_kode($id_kode,$kode){
       $this->db->query("INSERT INTO `kode_sms`(`id`, `kode`) VALUES('$id_kode','$kode')"); 
    }

    function cek_no_telp($no_telp){
        $query = $this->db->query("select count(*) as total from m_pemilik_usaha where kontak = '$no_telp'");
        return $query->first_row()->total;
    }

    function aktifasi($id_pemilik){
        $query = $this->db->query("update m_pemilik_usaha set status=1 where id_pemilik='$id_pemilik'");
        return $query;
    }

    function deaktifasi($id_pemilik){
        $query = $this->db->query("update m_pemilik_usaha set status=0 where id_pemilik='$id_pemilik'");
        return $query;
    }


    function cek_kode($id_kode,$kode){
        $id_kode    = $this->db->escape_str($id_kode);
        $kode    = $this->db->escape_str($kode);
        $query = $this->db->query("select count(*) as total from kode_sms where id = '$id_kode' and kode = '$kode'");
        return $query->first_row()->total;
    }

    function login($no_telp,$password){

        $no_telp   = $this->db->escape_str($no_telp);
        $password   = hash("sha512", $this->db->escape_str($password));

        // $no_telp   = "081265582400";
        // $password   = "a79a122977d9a519f17dea1335ef4dda415f0d3b5005c3cafa1bc7081770240a292f123e4cd14fa986720b99eca38abfb12ab81a765d06cbe00d5b3486b52b93";
        $stmt = "select * from m_pemilik_usaha where kontak='$no_telp' and password='$password'";
        // echo "$stmt"; die();
       
        $query = $this->db->query($stmt);
       
        return $query;
    } 

    function insert_lokasi_usaha($id_pemilik,$id_lokasi,$jenis_lokasi){

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $jenis_lokasi    = $this->db->escape_str($jenis_lokasi);
        $id_lokasi    = $this->db->escape_str($id_lokasi);
        
        $this->db->query("insert into p_detail_lokasi (id_lokasi,id_pemilik,nama,lat,lng,jenis_lokasi)
            values ('$id_lokasi','$id_pemilik','Nama Lokasi','0','0','$jenis_lokasi')");
    }

    function update_nama_lokasi_usaha($id_lokasi,$nama_lokasi){

        $nama_lokasi    = $this->db->escape_str($nama_lokasi);
        $id_lokasi    = $this->db->escape_str($id_lokasi);
        
        $this->db->query("update p_detail_lokasi set nama = '$nama_lokasi' where id_lokasi = '$id_lokasi'");
    }

    function update_lokasi_usaha($id_lokasi,$nama_lokasi,$lat,$lng){

        $lat    = $this->db->escape_str($lat);
        $lng    = $this->db->escape_str($lng);
        $id_lokasi    = $this->db->escape_str($id_lokasi);
        
        $this->db->query("update p_detail_lokasi set lat = '$lat', lng = '$lng', nama = '$nama_lokasi' where id_lokasi = '$id_lokasi'");
    }

    function get_pemilik_all() {
        $id_pemilik    = $this->db->escape_str($id_pemilik);
        return $this->db->query("select * from m_pemilik_usaha")->result();
    }

    function update_id($id_pemilik, $new_id) {
        return $this->db->query("update m_pemilik_usaha set id_pemilik='$new_id' where id_pemilik='$id_pemilik'");
    }

    function get_open_pemilik($id_pemilik) {
        return $this->db->query("select a.*, b.tipe as tipe_usaha  
            from m_pemilik_usaha a left join m_jenis_kategori_usaha b on (a.tipe=2 OR a.id_jenis=b.id_jenis)
            where a.id_pemilik='$id_pemilik' AND (((CURRENT_TIME() < a.waktu_tutup) AND a.waktu_buka IS NOT NULL AND a.waktu_tutup IS NOT NULL) OR (a.waktu_buka IS NULL AND a.waktu_tutup IS NULL)) LIMIT 1")->result();
    }

    function get_lokasi_usaha($id_pemilik){
        
        $id_pemilik    = $this->db->escape_str($id_pemilik);
       
        $query = $this->db->query("select * from p_detail_lokasi where id_pemilik = '$id_pemilik' ");
       
        return $query;
    } 

    function get_radius(){
        $query = $this->db->query("select * from m_radius order by radius");
        return $query->result();
    }

    function get_pemilik_fee($id_pemilik){
        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $query = $this->db->query("select fee, include_fee, include_fee_note from m_pemilik_usaha where id_pemilik='$id_pemilik'");
        return $query->first_row();
    }
        
    function get_random_num() {
        $result = $this->db->query("SELECT FLOOR(RAND() * 99999999) AS random_num
            FROM m_pemilik_usaha WHERE 'random_num' NOT IN (SELECT id_pemilik FROM m_pemilik_usaha)
            LIMIT 1")->row();
        return $result->random_num;
    }

    function get_random_nums($count) {
        $result = $this->db->query("SELECT FLOOR(RAND() * 99999999) AS random_num
            FROM m_pemilik_usaha WHERE 'random_num' NOT IN (SELECT id_pemilik FROM m_pemilik_usaha)
            LIMIT " . $count)->result();
        return $result;
    }

    function get_random_phones($count) {
        $result = $this->db->query("SELECT CONCAT('99', FLOOR(RAND() * 9999999999)) AS random_num
            FROM m_pemilik_usaha WHERE 'random_num' NOT IN (SELECT kontak FROM m_pemilik_usaha)
            LIMIT " . $count)->result();
        return $result;
    }

    function get_kategori_produk(){

        $query = $this->db->query("select * from m_kategori_produk where status=1");
        
        return $query->result();
    }

    function get_kategori_produk_pemilik($id_pemilik){

        // $query = $this->db->query("select * from m_kategori_produk where id_pemilik='$id_pemilik'");
        $query = $this->db->query("SELECT DISTINCT a.id_kategori, b.nama_kategori from p_detail_produk a left join m_kategori_produk b on a.id_kategori=b.id_kategori where a.id_pemilik='$id_pemilik' AND b.nama_kategori IS NOT NULL AND b.nama_kategori != ''");
        
        return $query->result();
    }

    function get_kategori_produk_by_nama($kategori,$id_pemilik){

        $kategori = $this->db->escape_str($kategori);
        $id_pemilik= $this->db->escape_str($id_pemilik);

        /*$query = $this->db->query("select * from m_kategori_produk where status=1 and nama_kategori='$nama'");*/
        $query = $this->db->query("select * from m_kategori_produk where nama_kategori='$kategori' and id_pemilik='$id_pemilik'");  
        
        return $query->first_row()->id_kategori;
    }

    function get_kategori_by_nama_kategori($kategori){

        $id_kategori = 0;
        $kategori = $this->db->escape_str($kategori);

        /*$query = $this->db->query("select * from m_kategori_produk where status=1 and nama_kategori='$nama'");*/
        $query = $this->db->query("select * from m_kategori_produk where nama_kategori='$kategori'");  
        
        $result = $query->result();
        if (count($result) > 0) {
            $id_kategori = $query->first_row()->id_kategori;
        } else {
            $id_kategori = 0;
        }

        return $id_kategori;
    }

    function insert_kategori_produk($kategori,$id_pemilik){
	
        $query_kategori = "insert into m_kategori_produk (id_pemilik,nama_kategori)
                            SELECT '$id_pemilik',tmp.* FROM (SELECT '$kategori') AS tmp
                            WHERE NOT EXISTS (
                                SELECT nama_kategori FROM m_kategori_produk WHERE nama_kategori = '$kategori' and id_pemilik = '$id_pemilik' 
                            ) LIMIT 1";
                            // echo $query_kategori; die();
                            
        //$query_kategori = "insert into m_kategori_produk (nama_kategori,id_pemilik) values ('$kategori','$id_pemilik')";                  
        $this->db->query($query_kategori);
        return $this->db->insert_id();
    }

    function insert_produk($id_produk,$id_pemilik,$nama,$harga,$harga_promo,$stok,$keterangan,$id_kategori, $min_antar, $ket_promo){        

        $id_produk    = $this->db->escape_str($id_produk);
        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $nama    = $this->db->escape_str($nama);
        $harga    = $this->db->escape_str($harga);
        $harga_promo    = $this->db->escape_str($harga_promo);
        $stok    = $this->db->escape_str($stok);
        $keterangan    = $this->db->escape_str($keterangan);
        $id_kategori    = $this->db->escape_str($id_kategori);
        $ket_promo    = $this->db->escape_str($ket_promo);

        // echo "harga_promo: " .$harga_promo . "\n";
        if ($harga_promo == "") {
            $harga_promo = "NULL";
        }


        $query_produk   = "INSERT INTO `p_detail_produk`(`id_produk`, `id_pemilik`, `id_kategori`, `nama_produk`, `stok`, `keterangan`, `harga`, `harga_promo`, `status`, `min_antar`, `active`, `ket_promo`) 
        VALUES('$id_produk','$id_pemilik','$id_kategori','$nama','$stok','$keterangan','$harga', " . $harga_promo . ", '1', '$min_antar', 1, '$ket_promo') ";

        // echo $query_produk; die();
        
        $this->db->query($query_produk);
    }

    function insert_produk_batch($id_pemilik, $headers, $rows, $active=0){    


        $query_produk   = "INSERT INTO `p_detail_produk` (";
        $query_produk = $query_produk . "`id_pemilik`, ";
        $id_produk_pos = 0;
        $k = 0;
        $updateStmt = "ON DUPLICATE KEY UPDATE ";
        foreach ($headers as $header) {
            if ($header == 'id_barcode') {
                $id_produk_pos = $k;
                $header = 'id_produk';
            }
            $query_produk = $query_produk . "`".$header . "`, ";
            $updateStmt = $updateStmt . " " . $header . "=VALUES(" . $header . "), ";
            $k++;
        }
        $query_produk = $query_produk . " `active`) VALUES ";
        $query_produk = str_replace(", ) VALUES ", ") VALUES ", $query_produk);

        $updateStmt = $updateStmt . "HOHOHO";
        $updateStmt = str_replace("), HOHOHO", ") ", $updateStmt);

        $i = 0;
        foreach ($rows as $produk) {
            if (count($produk) <  $k) continue;

            $query_produk = $query_produk . "(";
            $query_produk = $query_produk . "'".$id_pemilik . "', ";
            $j=0;
            foreach ($produk as $column) {
                // $column = $produk[$j];
                if ($j == $id_produk_pos) {
                    $column = $id_pemilik . "_" . $column;
                }
                $query_produk = $query_produk . "'".$column . "', ";
                $j++;
            }

            $query_produk = $query_produk . "'$active', ), HIHI";
            $query_produk = str_replace(", ), HIHI", "),", $query_produk);

            $i++;
        }

        $query_produk = $query_produk . " HEHE";
        $query_produk = str_replace("), HEHE", ")", $query_produk);

        $query_produk = $query_produk . " " . $updateStmt;

        // echo $query_produk; die();
        
        $this->db->query($query_produk);
        $this->db->query("DELETE FROM p_detail_produk WHERE id_pemilik='$id_pemilik' AND id_produk = CONCAT('$id_pemilik', '_')");
    }

    function set_produk_siap_antar_batch($id_pemilik, $headers, $rows){    


        $query_produk   = "UPDATE `p_detail_produk` SET min_antar=1 WHERE id_produk IN (";
        $id_produk_pos = 0;
        $k = 0;
        foreach ($headers as $header) {
            if ($header == 'id_barcode') {
                $id_produk_pos = $k;
            }
            $k++;
        }

        $i = 0;
        foreach ($rows as $produk) {
            $query_produk = $query_produk . "'".$id_pemilik . "_" . $produk[$id_produk_pos] . "', ";
            $i++;
        }

        $query_produk = $query_produk . ")";
        $query_produk = str_replace(", )", ")", $query_produk);


        // echo $query_produk; die();
        
        $this->db->query($query_produk);
    }


    function set_produk_batch($id_pemilik, $headers, $rows){    

        $query_produk   = "UPDATE `p_detail_produk` SET min_antar=1 WHERE id_produk IN (";
        $id_produk_pos = 0;
        $k = 0;
        foreach ($headers as $header) {
            if ($header == 'id_barcode') {
                $id_produk_pos = $k;
            }
            $k++;
        }

        $i = 0;
        foreach ($rows as $produk) {
            $query_produk = $query_produk . "'".$id_pemilik . "_" . $produk[$id_produk_pos] . "', ";
            $i++;
        }

        $query_produk = $query_produk . ")";
        $query_produk = str_replace(", )", ")", $query_produk);


        // echo $query_produk; die();
        
        $this->db->query($query_produk);
    }

    // function insert_produk_batch($produks){    


    //     $query_produk   = "INSERT INTO `p_detail_produk`(`id_produk`, `id_pemilik`, `id_kategori`, `nama_produk`, `stok`, `keterangan`, `harga`, `harga_promo`, `status`, `min_antar`) VALUES ";

    //     $i = 0;
    //     foreach ($produks as $produk) {
    //         $id_produk    = $produk->$id_produk;
    //         $id_pemilik    = $produk->$id_pemilik;
    //         $nama    = $produk->$nama;
    //         $harga    = $produk->$harga;
    //         $harga_promo    = $produk->$harga_promo;
    //         $stok    = $produk->$stok;
    //         $keterangan    = $produk->$keterangan;
    //         $id_kategori    = $produk->$id_kategori;

    //         if ($i > 0) {
    //             $query_produk = $query_produk . ", ('$id_produk','$id_pemilik','$id_kategori','$nama','$stok','$keterangan','$harga', '$harga_promo', '1', '$min_antar')";
    //         } else {
    //             $query_produk = $query_produk . " ('$id_produk','$id_pemilik','$id_kategori','$nama','$stok','$keterangan','$harga', '$harga_promo', '1', '$min_antar')";
    //         }

    //         $i++;
    //     }
        
    //     $this->db->query($query_produk);
    // }

    function update_produk($id_produk,$id_kategori,$nama,$harga,$harga_promo,$stok,$keterangan, $min_antar, $ket_promo){        

        $id_produk    = $this->db->escape_str($id_produk);
        $nama    = $this->db->escape_str($nama);
        $harga    = $this->db->escape_str($harga);
        $harga_promo    = $this->db->escape_str($harga_promo);
        $stok    = $this->db->escape_str($stok);
        $keterangan    = $this->db->escape_str($keterangan);
        $ket_promo    = $this->db->escape_str($ket_promo);

        if ($harga_promo == '') {
            $harga_promo == "NULL";
        }

        $query_produk   = "update p_detail_produk set nama_produk = '$nama', id_kategori = '$id_kategori', harga = '$harga', harga_promo = " . $harga_promo . ", stok = '$stok', keterangan = '$keterangan', min_antar = '$min_antar', ket_promo='$ket_promo'  where id_produk = '$id_produk'";
        
        $this->db->query($query_produk);
    }


    function nullify_harga_promo($id_pemilik) {
        // $query_produk   = "update p_detail_produk pdp left join merchant_promo mp on mp.id_pemilik=pdp.id_pemilik left join produk_promo_diskon ppd on ppd.id_merchant_promo=mp.id set pdp.harga_promo=NULL where pdp.id_pemilik = '$id_pemilik' AND CURDATE() > mp.end_date";
        $query_produk   = "update p_detail_produk pdp left join produk_promo_diskon ppd on ppd.id_produk=REPLACE(pdp.id_produk, '" . $id_pemilik . "_', '') left join merchant_promo mp on mp.id=ppd.id_merchant_promo set pdp.harga_promo=NULL where mp.id_pemilik = '" . $id_pemilik . "' AND CURDATE() > mp.end_date";
        $this->db->query($query_produk);
    }


    function nullify_all_harga_promo($id_pemilik) {
        // $query_produk   = "update p_detail_produk pdp left join merchant_promo mp on mp.id_pemilik=pdp.id_pemilik left join produk_promo_diskon ppd on ppd.id_merchant_promo=mp.id set pdp.harga_promo=NULL where pdp.id_pemilik = '$id_pemilik' AND CURDATE() > mp.end_date";
        $query_produk   = "update p_detail_produk SET harga_promo=NULL where id_pemilik = '" . $id_pemilik . "'";
        $this->db->query($query_produk);
    }

    function nullify_harga_promo_by_promos($id_pemilik, $id_promos) {
        // $query_produk   = "update p_detail_produk pdp left join merchant_promo mp on mp.id_pemilik=pdp.id_pemilik left join produk_promo_diskon ppd on ppd.id_merchant_promo=mp.id set pdp.harga_promo=NULL where pdp.id_pemilik = '$id_pemilik' AND CURDATE() > mp.end_date";
        $query_produk   = "update p_detail_produk pdp left join produk_promo_diskon ppd on ppd.id_produk=REPLACE(pdp.id_produk, '" . $id_pemilik . "_', '') left join merchant_promo mp set pdp.harga_promo=NULL where mp.id IN (" . $id_promos . ") AND (CURDATE() > mp.end_date OR CURDATE() < mp.start_date) ";

        echo $query_produk; die();
        $this->db->query($query_produk);
    }


    function update_produk_umkm($id_produk,$id_kategori,$nama,$harga,$harga_promo,$stok,$keterangan, $min_antar, $ket_promo){        

        $id_produk    = $this->db->escape_str($id_produk);
        $nama    = $this->db->escape_str($nama);
        $harga    = $this->db->escape_str($harga);
        $harga_promo    = $this->db->escape_str($harga_promo);
        $stok    = $this->db->escape_str($stok);
        $keterangan    = $this->db->escape_str($keterangan);
        $ket_promo    = $this->db->escape_str($ket_promo);

        $harga_promo_value = "";
            if (!isset($harga_promo) || $harga_promo == "") {
                $harga_promo_value = "NULL";
            } else {
                $harga_promo_value = $harga_promo;
            }
        $query_produk   = "update p_detail_produk_umkm set nama_produk = '$nama', id_kategori = '$id_kategori', harga = '$harga', harga_promo = " . $harga_promo_value . " , stok = " . $stok . ", keterangan = '$keterangan', min_antar = '$min_antar', ket_promo='$ket_promo'
                            where id_produk = '$id_produk'";

                            // echo $query_produk; die();
        
        $this->db->query($query_produk);
    }

    function update_stok($id_produk,$stok){

        $id_produk    = $this->db->escape_str($id_produk);
        $stok    = $this->db->escape_str($stok);
        
        $this->db->query("update p_detail_produk set stok = '$stok' where id_produk = '$id_produk'");
    }


    // function update_stocks($id_pemilik, $stocks){
    //     $when = ""; 
    //     $in = "where id_produk in (";
    //     $stmt = "update p_detail_produk set stok = case";
    //     $i = 0;
    //     // echo json_encode($stocks); die();
    //     foreach ($stocks as $stock) {
    //         $when = $when . " when id_produk = '" . $id_pemilik . "_". $stock->id_produk . "' then " .  $stock->stok;
    //         if ($i == (count($stocks)-1)) {
    //             $in = $in . "'" . $id_pemilik . "_". $stock->id_produk . "'";
    //         } else {    
    //             $in = $in . "'" . $id_pemilik . "_". $stock->id_produk . "',";
    //         }
    //         $i++;
    //     }
    //     $when = $when . " end ";
    //     $in = $in . ")";

    //     $stmt = $stmt . $when . $in;
    //     $this->db->query($stmt);

    //     // echo $stmt; die();

    //     // $this->db->update_batch("p_detail_produk", $stocks, "id_produk'");
    // }

    function update_stocks($id_pemilik, $stocks){
        $i = 0;

        // $return = $this->db->query("delete from p_detail_produk where id_pemilik='" . $id_pemilik . "'");
        
        $insert = "INSERT INTO p_detail_produk (id_produk, id_pemilik, nama_produk, stok, harga, foto, keterangan, active) VALUES ";
        $update = "ON DUPLICATE KEY UPDATE id_pemilik=VALUES(id_pemilik), nama_produk=VALUES(nama_produk), stok=VALUES(stok), harga=VALUES(harga), foto=REPLACE(VALUES(id_produk), '".$id_pemilik."_', ''), active=1";

        foreach ($stocks as $stock) {
            $insert = $insert . "('" . $id_pemilik . "_" . $stock->id_produk . "', '" . $id_pemilik . "', '" . (isset($stock->nama_produk) ? self::clean($stock->nama_produk) : '') . "', " . (isset($stock->stok) ? $stock->stok : '0') . ", '" . (isset($stock->harga) ? $stock->harga : '0') . "', '" . (isset($stock->foto) ? $stock->foto : '') . "', '". (isset($stock->keterangan) ? $stock->keterangan : '') . "', 1), ";
            $i++;
            
        }

        $stmt = $insert . $update;
        $stmt = str_replace("), ON DUPLICATE KEY UPDATE", ") ON DUPLICATE KEY UPDATE", $stmt);
        // echo $stmt; die();
        // $stmt = self::clean($stmt);
        $return = $this->db->query($stmt);
        $errMess = "";

        if(!$return)
        {
           $errNo   = $this->db->_error_number();
           $errMess = $this->db->_error_message();
           // Do something with the error message or just show_404();
        }
        return $errMess;
    }


    function update_produks_umkm($id_pemilik, $stocks){
        $i = 0;

        // $return = $this->db->query("delete from p_detail_produk where id_pemilik='" . $id_pemilik . "'");
        
        $insert = "INSERT INTO p_detail_produk_umkm (id_produk, id_pemilik, nama_produk, stok, harga, harga_promo, foto, keterangan, active) VALUES ";
        $update = "ON DUPLICATE KEY UPDATE id_pemilik=VALUES(id_pemilik), nama_produk=VALUES(nama_produk), nama_produk=VALUES(nama_produk), harga=VALUES(harga), harga_promo=VALUES(harga_promo), foto=CONCAT(VALUES(id_produk), '.png'), active=1";

        foreach ($stocks as $stock) {
            $harga_promo = "";
            if (!isset($stock->harga_promo) || $stock->harga_promo == "") {
                $harga_promo = "NULL";
            } else {
                $harga_promo = "'".$stock->harga_promo."'";
            }
            $insert = $insert . "('" . $id_pemilik . "_" . $stock->id_produk . "', '" . $id_pemilik . "', '" . (isset($stock->nama_produk) ? self::clean($stock->nama_produk) : '') . "', " . (isset($stock->stok) ? $stock->stok : '0') . ", '" . (isset($stock->harga) ? $stock->harga : '0')  . "', " . $harga_promo . ", '" . $id_pemilik . "_" . $stock->id_produk . ".png', '". (isset($stock->keterangan) ? $stock->keterangan : '') . "', 1), ";
            $i++;
            
        }

        $stmt = $insert . $update;
        $stmt = str_replace("), ON DUPLICATE KEY UPDATE", ") ON DUPLICATE KEY UPDATE", $stmt);
        // echo $stmt; die();
        // $stmt = self::clean($stmt);
        $return = $this->db->query($stmt);
        $errMess = "";

        if(!$return)
        {
           $errNo   = $this->db->_error_number();
           $errMess = $this->db->_error_message();
           // Do something with the error message or just show_404();
        }
        return $errMess;
    }



    function cari_produk_by_pemilik($q, $id_pemilik, $tipe, $page, $id_promos) {
            $q = $this->db->escape_str($q);
            $id_pemilik = $this->db->escape_str($id_pemilik);
            $page = $this->db->escape_str($page);

            $pagingParam = "";
            if (intval($page >=1 )) { // kalau paging dimulai dari 1, enable pagint. Jika tidak, tampilkan seluruhnya
                $offset = ($page-1) * 50; 
                $pagingParam =  "LIMIT 10 OFFSET " . $offset;
            }

            $promo_params = "";
            if (str_replace(' ', '', $id_promos) != "") {
                $promo_params = "AND ppd.id_merchant_promo IN (" . $id_promos . ")";
            }

            $q = str_replace("-", " ", $q);

            $produk_promo_query = "";
            if ($tipe == "2") {
                $produk_promo_query = "select a.id_produk, a.nama_produk, a.id_pemilik, a.id_kategori, a.stok, a.keterangan, a.harga, a.status, a.foto, a.versi, a.min_antar, a.active, CONCAT('Harga promo untuk pembelian ', ppd.min_qty, ' buah') as ket_promo, ppd.harga_promo as harga_promo, 0 as is_umkm, c.nama as nama_pemilik, ppd.min_qty, 0 as is_official from p_detail_produk a left join m_pemilik_usaha c on c.id_partner=a.id_pemilik or c.id_pemilik=a.id_pemilik left join produk_promo_diskon ppd on ppd.id_produk=REPLACE(a.id_produk, '" . $id_pemilik . "_', '') where (REPLACE(a.nama_produk, '-', ' ') LIKE '%$q%' OR REPLACE(a.keterangan, '-', ' ') LIKE '%$q%' OR REPLACE(a.ket_promo, '-', ' ') LIKE '%$q%') AND  a.id_pemilik='" . $id_pemilik . "' AND a.nama_produk <> '' AND a.active=1  " . $promo_params . " " . $pagingParam . "  UNION ";
            }

            $sql = "SELECT *, COUNT(id_produk) AS produk_count FROM ("
             . $produk_promo_query . "select pdp.id_produk, pdp.nama_produk, pdp.id_pemilik, pdp.id_kategori, pdp.stok, pdp.keterangan, pdp.harga, pdp.status, pdp.foto, pdp.versi, pdp.min_antar, pdp.active, pdp.ket_promo,  pdp.harga_promo as harga_promo,  0 as is_umkm, mpu.nama as nama_pemilik, 1 as min_qty,  0 as is_official from p_detail_produk_umkm pdp left join m_pemilik_usaha mpu on mpu.id_pemilik=pdp.id_pemilik WHERE  (REPLACE(pdp.nama_produk, '-', ' ') LIKE '%$q%' OR REPLACE(pdp.keterangan, '-', ' ') LIKE '%$q%' OR REPLACE(pdp.ket_promo, '-', ' ') LIKE '%$q%') AND pdp.id_pemilik='" . $id_pemilik . "' AND pdp.id_pemilik <> '' AND pdp.nama_produk <> '' AND pdp.active=1 " . $pagingParam . "  UNION select pdp.id_produk, pdp.nama_produk, pdp.id_pemilik, pdp.id_kategori, pdp.stok, pdp.keterangan, pdp.harga, pdp.status, pdp.foto, pdp.versi, pdp.min_antar, pdp.active, pdp.ket_promo,  pdp.harga_promo as harga_promo,  0 as is_umkm, mpu.nama as nama_pemilik, 1 as min_qty,  0 as is_official from p_detail_produk pdp left join m_pemilik_usaha mpu on mpu.id_pemilik=pdp.id_pemilik WHERE  (REPLACE(pdp.nama_produk, '-', ' ') LIKE '%$q%' OR REPLACE(pdp.keterangan, '-', ' ') LIKE '%$q%' OR REPLACE(pdp.ket_promo, '-', ' ') LIKE '%$q%') AND pdp.id_pemilik='" . $id_pemilik . "' AND pdp.id_pemilik <> '' AND pdp.nama_produk <> '' AND pdp.active=1 " . $pagingParam . " ) T1 GROUP BY id_produk ORDER BY stok=0, harga";
            // echo $sql; die();

            $query = $this->db->query($sql);

            return $query->result();
    }

    function get_produks_umkm($id_pemilik, $page){

        $id_pemilik = $this->db->escape_str($id_pemilik);
        $page = $this->db->escape_str($page);

        if (intval($page >=1 )) { // kalau paging dimulai dari 1, enable pagint. Jika tidak, tampilkan seluruhnya
            $offset = ($page-1) * 50; 
            $pagingParam =  "LIMIT 50 OFFSET " . $offset;
        }
        
        $sql = "select a.*,b.nama_kategori, 0 as is_official, 1 as is_umkm from p_detail_produk_umkm a left join m_pemilik_usaha c on c.id_partner=a.id_pemilik or c.id_pemilik=a.id_pemilik left join m_kategori_produk b on a.id_kategori=b.id_kategori where a.id_pemilik='$id_pemilik' AND a.nama_produk <> '' AND a.active=1 order by b.nama_kategori " . $pagingParam;
        // echo $sql; die();
        $query = $this->db->query($sql);
        
        return $query->result();
    }

    function get_produks_umkm_count($id_pemilik){

        $id_pemilik = $this->db->escape_str($id_pemilik);

        $sql = "select COUNT(DISTINCT id_produk) as id_produk_count from p_detail_produk_umkm where id_pemilik='$id_pemilik' AND nama_produk <> '' AND active=1";
        // echo $sql; die();
        $query = $this->db->query($sql);
        $result = $query->row_array();
        
        return $result['id_produk_count'];
    }


    function get_produk_pemilik($id_pemilik, $id_promos, $tipe, $min_antar, $page){
            $id_pemilik = $this->db->escape_str($id_pemilik);
            $default_value = "null";

            $minAntarParam = "";

            if ($tipe == '2') {
                $default_value = "null";

                if ($min_antar == "1") {
                    $minAntarParam = " AND a.min_antar = 1";
                } else {
                    $minAntarParam = " AND a.min_antar != 1";
                }
            } else {
                $default_value = "a.harga_promo";
            }

            $page = $this->db->escape_str($page);

            if (intval($page >=1 )) { // kalau paging dimulai dari 1, enable pagint. Jika tidak, tampilkan seluruhnya

                $offset = ($page-1) * 50; 
                $pagingParam =  " LIMIT 50 OFFSET " . $offset;
            }

        
            $sql = "SELECT a.*, " . $default_value . " as harga_promo, b.nama_kategori as nama_kategori, 0 as is_umkm from p_detail_produk a left join m_kategori_produk b on a.id_kategori=b.id_kategori left outer join m_pemilik_usaha c on c.id_partner=a.id_pemilik or c.id_pemilik=a.id_pemilik where a.id_pemilik='$id_pemilik' AND c.nama <> '' AND a.nama_produk <> '' AND a.active=1 AND a.harga_promo IS NULL AND a.harga > 0 " . $minAntarParam;

            $sql = $sql . " group by a.id_produk order by nama_kategori"  . $pagingParam;
            // echo $sql; die();

            $query = $this->db->query($sql);

            return $query->result();
    }



    function get_produk_pemilik_count($id_pemilik, $id_promos, $tipe, $min_antar){
            $id_pemilik = $this->db->escape_str($id_pemilik);
            $default_value = "null";
                
            $minAntarParam = "";
            if ($tipe == '2') {
                $default_value = "null";

                if ($min_antar == "1") {
                    $minAntarParam = " AND a.min_antar = 1";
                } else {
                    $minAntarParam = " AND a.min_antar != 1";
                }
            } else {
                $default_value = "a.harga_promo";
            }

            $page = $this->db->escape_str($page);

            if (intval($page >=1 )) { // kalau paging dimulai dari 1, enable pagint. Jika tidak, tampilkan seluruhnya

                $offset = ($page-1) * 50; 
                $pagingParam =  " LIMIT 50 OFFSET " . $offset;
            }

        
            $sql = "SELECT a.*, " . $default_value . " as harga_promo, b.nama_kategori as nama_kategori, 0 as is_umkm from p_detail_produk a left join m_kategori_produk b on a.id_kategori=b.id_kategori left outer join m_pemilik_usaha c on c.id_partner=a.id_pemilik or c.id_pemilik=a.id_pemilik where a.id_pemilik='$id_pemilik' AND c.nama <> '' AND a.nama_produk <> '' AND a.active=1 AND a.harga_promo IS NULL";

            $sql = $sql . " group by a.id_produk order by nama_kategori"  . $pagingParam;
            // echo $sql;

            $query = $this->db->query($sql);

            return $query->result();
    }



    function test_get_produk_siap_antar($id_pemilik, $promo, $tipe, $page){
            $id_pemilik = $this->db->escape_str($id_pemilik);
            $default_value = "null";
            if ($tipe == '2') {
                $default_value = "null";
            } else {
                $default_value = "a.harga_promo";
            }

            $page = $this->db->escape_str($page);

            $pagingParam = "";

            if (intval($page >=1 )) { // kalau paging dimulai dari 1, enable pagint. Jika tidak, tampilkan seluruhnya

                $offset = ($page-1) * 50; 
                $pagingParam =  " LIMIT 50 OFFSET " . $offset;
            }
                
            $minAntarParam = " AND a.min_antar = 0";

            $sql = "SELECT a.*, " . $default_value . " as harga_promo, b.nama_kategori as nama_kategori from p_detail_produk a left join m_kategori_produk b on a.id_kategori=b.id_kategori left outer join m_pemilik_usaha c on c.id_partner=a.id_pemilik or c.id_pemilik=a.id_pemilik where a.id_pemilik='$id_pemilik' AND c.nama <> '' AND a.nama_produk <> '' AND a.active=1 AND a.harga_promo IS NULL " . $minAntarParam;

            $sql = $sql . " group by a.id_produk order by nama_kategori"  . $pagingParam;
            // echo $sql; die();

            $query = $this->db->query($sql);

            return $query->result();
    }


    function get_total_produks_umkm($id_pemilik){

        $id_pemilik = $this->db->escape_str($id_pemilik);
        
        $query = $this->db->select('count(*) as total')->from('p_detail_produk_umkm')->where('id_pemilik', $id_pemilik);
        $query = $this->db->get();
        return $query->row()->total;
    }

    function count_products($id_pemilik) {
        $stmt = "SELECT COUNT(*) as total FROM p_detail_produk WHERE id_pemilik='".$id_pemilik."'";
        $query = $this->db->query($stmt);
        return $query->row_array();
    }

    function copy_products($id_pemilik_old, $id_pemilik_new) {
        $stmt = "INSERT INTO p_detail_produk (id_produk,id_pemilik,id_kategori,nama_produk,stok,harga,harga_promo,active)
        SELECT REPLACE(id_produk, '".$id_pemilik_old."_', '".$id_pemilik_new."_'), '".$id_pemilik_new."', id_kategori, nama_produk, stok, harga, harga_promo, 1
        FROM p_detail_produk WHERE id_pemilik = '".$id_pemilik_old."' ON DUPLICATE KEY UPDATE id_pemilik=VALUES(id_pemilik), id_kategori=VALUES(id_kategori), nama_produk=VALUES(nama_produk), stok=VALUES(stok), harga=VALUES(harga), harga_promo=VALUES(harga_promo), active=1";
        // echo $stmt; die();
        $query = $this->db->query($stmt);
        return $query;
    }

    function clean($string) {
       $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
       return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    // function get_produk($id_pemilik){

    //     $id_pemilik    = $this->db->escape_str($id_pemilik);

    //     return $this->db->query("select a.*,b.nama_kategori
    //                         from p_detail_produk a, m_kategori_produk b
    //                         where
    //                         a.id_kategori = b.id_kategori
    //                         and
    //                         a.id_pemilik = '$id_pemilik' and a.status = 1
    //                         order by b.nama_kategori")->result();
    
    // }

    function get_pemilik_by_partner($id_partner){

        $id_partner    = $this->db->escape_str($id_partner);
        
        $query = $this->db->query("select * from m_pemilik_usaha where id_partner = '$id_partner'");

        return $query;
    }

    function get_first_pemilik_by_partner($id_partner){

        $id_partner    = $this->db->escape_str($id_partner);
        
        $query = $this->db->query("select * from m_pemilik_usaha where id_partner = '$id_partner' ORDER BY tanggal_insert LIMIT 1");

        return $query->row();
    }

    function delete_produk($id_produk, $is_umkm){

        $id_produk    = $this->db->escape_str($id_produk);

        $table_name = "p_detail_produk";
        if (intval($is_umkm) == 1) 
            $table_name = "p_detail_produk_umkm";
        
        $this->db->query("delete from " . $table_name . " where id_produk = '$id_produk'");
    }

    function copy_produk_to_histori($id_produk, $is_umkm) {
        $table_name = "p_detail_produk";
        if (intval($is_umkm) == 1) 
            $table_name = "p_detail_produk_umkm";

        $stmt = "INSERT INTO histori_p_detail_produk SELECT p.* FROM " . $table_name . " p WHERE p.id_produk = '".$id_produk."'";
        $this->db->query($stmt);
    }

    function copy_produks_pemilik_to_histori($id_produks) {
        $stmt = "INSERT INTO histori_p_detail_produk SELECT p.* FROM p_detail_produk p WHERE p.id_produk IN (".$id_produks.")";
        $this->db->query($stmt);
    }


    function copy_produks_umkm_pemilik_to_histori($id_produks) {
        $stmt = "INSERT INTO histori_p_detail_produk SELECT p.* FROM p_detail_produk_umkm p WHERE p.id_produk IN (".$id_produks.")";
        $this->db->query($stmt);
    }

    function delete_produk_by_pemilik($id_pemilik){

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        
        $this->db->query("delete from p_detail_produk where id_pemilik = '$id_pemilik'");
    }


    function delete_produks_by_pemilik($id_pemilik, $id_produks){

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        
        $this->db->query("delete from p_detail_produk where id_pemilik = '$id_pemilik' AND id_produk IN (" . $id_produks . ")");
    }

    function delete_produks_umkm_by_pemilik($id_pemilik, $id_produks){

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        
        $this->db->query("delete from p_detail_produk_umkm where id_pemilik = '$id_pemilik' AND id_produk IN (" . $id_produks . ")");
    }

    function get_order($id_pemilik){
        
        $id_pemilik    = $this->db->escape_str($id_pemilik);
        
        $query = $this->db->query("SELECT *, count(id_order) id_order_count from 

            (
                    select a.id_order, a.id_konsumen, a.id_pemilik, a.tanggal_order, a.total, a.fee, a.jenis_alamat, a.alamat_antar, a.ket_alamat, a.lat_antar, a.lng_antar, a.id_perkiraan_antar, a.ket_perkiraan_antar, a.charge, a.include_fee, a.include_fee_note, a.tipe, a.ket_order, a.status as status, 'Akun dihapus' as status_note, b.nama,b.alamat,b.kontak,b.lat,b.lng,CONCAT('" . URL_IMAGE_KONSUMEN . "', b.foto) as foto,b.versi, c.nama as nama_kota from konsumen_order a left join m_konsumen b on a.id_konsumen=b.id_konsumen left join m_kota c ON  
                    c.id = b.id_kota where  b.nama IS NOT NULL and a.id_pemilik='$id_pemilik'
                    UNION
                    select a.id_order, a.id_konsumen, a.id_pemilik, a.tanggal_order, a.total, a.fee, a.jenis_alamat, a.alamat_antar, a.ket_alamat, a.lat_antar, a.lng_antar, a.id_perkiraan_antar, a.ket_perkiraan_antar, a.charge, a.include_fee, a.include_fee_note, a.tipe, a.ket_order, '3' as status, '' as status_note, b.nama,b.alamat,b.kontak,b.lat,b.lng,CONCAT('" . URL_IMAGE_KONSUMEN . "', b.foto) as foto,b.versi, c.nama as nama_kota from konsumen_order a left join histori_m_konsumen b on a.id_konsumen=b.id_konsumen left join m_kota c ON  
                    c.id = b.id_kota where a.id_pemilik='$id_pemilik' AND  b.nama IS NOT NULL 
                    )
                    T1

            GROUP BY id_order ORDER BY id_order 
                    ");
        
        return $query->result();
    }


    function set_aktif($id_pemilik, $aktif) {

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $aktif    = $this->db->escape_str($aktif);

        $query = $this->db->query("update m_pemilik_usaha set aktif=" . $aktif . " WHERE id_pemilik=" .$id_pemilik . "");
    }


    function allow_kuber($id_pemilik, $allow_kuber) {

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $allow_kuber    = $this->db->escape_str($allow_kuber);

        $query = $this->db->query("update m_pemilik_usaha set allow_kuber=" . $allow_kuber . " WHERE id_pemilik=" .$id_pemilik . "");
    }


    function get_undone_order($id_pemilik){
        
        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $sql = "SELECT *, count(id_order) id_order_count from 
            (select a.id_order, a.id_konsumen, a.id_pemilik, a.tanggal_order, a.total, a.fee, a.jenis_alamat, a.alamat_antar, a.ket_alamat, a.lat_antar, a.lng_antar, a.id_perkiraan_antar, a.ket_perkiraan_antar, a.charge, a.include_fee, a.include_fee_note, a.tipe, a.ket_order, a.status as status, '' as status_note, b.nama,b.alamat,b.kontak,b.lat,b.lng,CONCAT('" . URL_IMAGE_KONSUMEN . "', b.foto) as foto,b.versi, c.nama as nama_kota, e.nama_produk as nama_produk, e.harga, d.kuantitas as kuantitas, d.id_produk 
            from konsumen_order a left join m_konsumen b on  a.id_konsumen=b.id_konsumen left join m_kota c on c.id = b.id_kota left join detail_order d on d.id_order=a.id_order left join p_detail_produk e on e.id_produk=d.id_produk 
            where a.id_pemilik='$id_pemilik' and (a.status=1 OR a.status=2)  AND  b.nama IS NOT NULL UNION
            select a.id_order, a.id_konsumen, a.id_pemilik, a.tanggal_order, a.total, a.fee, a.jenis_alamat, a.alamat_antar, a.ket_alamat, a.lat_antar, a.lng_antar, a.id_perkiraan_antar, a.ket_perkiraan_antar, a.charge, a.include_fee, a.include_fee_note, a.tipe, a.ket_order, a.status as status, 'Akun dihapus' as status_note, b.nama,b.alamat,b.kontak,b.lat,b.lng,CONCAT('" . URL_IMAGE_KONSUMEN . "', b.foto) as foto,b.versi, c.nama as nama_kota, e.nama_produk as nama_produk, e.harga, d.kuantitas as kuantitas, d.id_produk 
            from konsumen_order a left join histori_m_konsumen b on  a.id_konsumen=b.id_konsumen left join m_kota c on c.id = b.id_kota left join detail_order d on d.id_order=a.id_order left join p_detail_produk e on e.id_produk=d.id_produk 
            where a.id_pemilik='$id_pemilik' and (a.status=1 OR a.status=2) AND  b.nama IS NOT NULL)
            T1 GROUP BY id_order ORDER BY tanggal_order ASC";
                   // echo $sql; die();
        
        $query = $this->db->query($sql);
        
        return $query->result();
    }


    function get_done_order($id_pemilik){
        
        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $sql = "SELECT *, count(id_order) id_order_count from 
            (select a.id_order, a.id_konsumen, a.id_pemilik, a.tanggal_order, a.total, a.fee, a.jenis_alamat, a.alamat_antar, a.ket_alamat, a.lat_antar, a.lng_antar, a.id_perkiraan_antar, a.ket_perkiraan_antar, a.charge, a.include_fee, a.include_fee_note, a.tipe, a.ket_order, a.status as status, '' as status_note, b.nama,b.alamat,b.kontak,b.lat,b.lng,CONCAT('" . URL_IMAGE_KONSUMEN . "', b.foto) as foto,b.versi, c.nama as nama_kota, e.nama_produk as nama_produk, e.harga, d.kuantitas as kuantitas, d.id_produk 
            from konsumen_order a left join m_konsumen b on  a.id_konsumen=b.id_konsumen left join m_kota c on c.id = b.id_kota left join detail_order d on d.id_order=a.id_order left join p_detail_produk e on e.id_produk=d.id_produk 
            where a.id_pemilik='$id_pemilik' and (a.status=4 OR a.status=3 OR a.status=5)  AND  b.nama IS NOT NULL UNION
            select a.id_order, a.id_konsumen, a.id_pemilik, a.tanggal_order, a.total, a.fee, a.jenis_alamat, a.alamat_antar, a.ket_alamat, a.lat_antar, a.lng_antar, a.id_perkiraan_antar, a.ket_perkiraan_antar, a.charge, a.include_fee, a.include_fee_note, a.tipe, a.ket_order, a.status as status, 'Akun dihapus' as status_note, b.nama,b.alamat,b.kontak,b.lat,b.lng,CONCAT('" . URL_IMAGE_KONSUMEN . "', b.foto) as foto,b.versi, c.nama as nama_kota, e.nama_produk as nama_produk, e.harga, d.kuantitas as kuantitas, d.id_produk 
            from konsumen_order a left join histori_m_konsumen b on  a.id_konsumen=b.id_konsumen left join m_kota c on c.id = b.id_kota left join detail_order d on d.id_order=a.id_order left join p_detail_produk e on e.id_produk=d.id_produk 
            where a.id_pemilik='$id_pemilik' and (a.status=4 OR a.status=3 OR a.status=5) and  b.nama IS NOT NULL)
            T1 GROUP BY id_order ORDER BY tanggal_order ASC";
                   // echo $sql; die();
        
        $query = $this->db->query($sql);
        
        return $query->result();
    }

    function buka_toko($id_pemilik,$status, $triggered){

        $status    = $this->db->escape_str($status);
        $id_pemilik    = $this->db->escape_str($id_pemilik);
        
        $this->db->query("update m_pemilik_usaha set status_toko = '$status', triggered=" . $triggered . " where id_pemilik = '$id_pemilik'");
    }

    function update_nama_toko($id_pemilik,$nama){

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $nama    = $this->db->escape_str($nama);
        
        $this->db->query("update m_pemilik_usaha set nama = '$nama' where id_pemilik = '$id_pemilik'");
    }

    function update_nama_email_toko($id_pemilik,$nama,$email){

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $nama    = $this->db->escape_str($nama);
        $email    = $this->db->escape_str($email);
        
        $this->db->query("update m_pemilik_usaha set nama = '$nama', email = '$email' where id_pemilik = '$id_pemilik'");
    }

    function update_username($id_pemilik,$username){

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $username    = $this->db->escape_str($username);
        
        $this->db->query("update m_pemilik_usaha set username = '$username' where id_pemilik = '$id_pemilik'");
    }

    function update_jargon_toko($id_pemilik,$jargon){

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $jargon    = $this->db->escape_str($jargon);
        
        $this->db->query("update m_pemilik_usaha set jargon = '$jargon' where id_pemilik = '$id_pemilik'");
    }

    
    function update_alamat_usaha($id_pemilik,$alamat){

        $alamat    = $this->db->escape_str($alamat);
        $id_pemilik    = $this->db->escape_str($id_pemilik);
        
        $this->db->query("update m_pemilik_usaha set alamat = '$alamat' where id_pemilik = '$id_pemilik'");
    }


    function update_no_telp_password_alamat_toko($id_pemilik,$kontak, $kontak_sms, $kontak_sms2, $password, $alamat){

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $kontak    = $this->db->escape_str($kontak);
        $kontak_sms    = $this->db->escape_str($kontak_sms);
        $kontak_sms2    = $this->db->escape_str($kontak_sms2);
        $wherePemilik = "";
        if (strlen($password) > 0) {
            $password    = hash('sha512', $this->db->escape_str($password));
            $wherePemilik = ", password = '$password'";
        }
        $alamt    = $this->db->escape_str($alamat);
        
        $stmt = "update m_pemilik_usaha set kontak = '$kontak', kontak_sms='$kontak_sms', kontak_sms2='$kontak_sms2'" . $wherePemilik . ", alamat = '$alamt' where id_pemilik = '$id_pemilik'";
        // echo $stmt;
        $this->db->query($stmt);
    }

    function update_radius_toko($id_pemilik,$radius) {

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $radius    = $this->db->escape_str($radius);
        
        $this->db->query("update m_pemilik_usaha set radius = $radius where id_pemilik = '$id_pemilik'");
    }

    function set_area_layanan($id_pemilik,$lat,$lng,$radius) {

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $radius    = $this->db->escape_str($radius);
        $lat    = $this->db->escape_str($lat);
        $lng    = $this->db->escape_str($lng);
        
        $this->db->query("update m_pemilik_usaha set radius = '$radius', lat='$lat', lng='$lng' where id_pemilik = '$id_pemilik'");
    }

    function update_min_antar_fee($id_pemilik, $min_antar, $fee) {

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $min_antar    = $this->db->escape_str($min_antar);
        $fee    = $this->db->escape_str($fee);
        
        $this->db->query("update m_pemilik_usaha set min_antar=$min_antar, fee=$fee where id_pemilik = '$id_pemilik'");
    }

    function update_jenis($id_pemilik,$id_jenis){

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $id_jenis    = $this->db->escape_str($id_jenis);
        
        $this->db->query("update m_pemilik_usaha set id_jenis = $id_jenis where id_pemilik = '$id_pemilik'");
    }


    function update_min_antar($id_pemilik,$min_antar){

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $min_antar    = $this->db->escape_str($min_antar);
        
        $this->db->query("update m_pemilik_usaha set min_antar = $min_antar where id_pemilik = '$id_pemilik'");
    }

    function update_kategori_usaha($id_pemilik,$json){

        $id_pemilik    = $this->db->escape_str($id_pemilik);

        $this->db->query("delete from p_detail_kategori where id_pemilik='$id_pemilik'");
        
        $arr = json_decode($json);

        for($i=0;$i<count($arr);$i++){

            $id_kategori_usaha  = $arr[$i]->id_kategori;

            $this->db->query("insert into p_detail_kategori (id_pemilik,id_kategori) values ('$id_pemilik','$id_kategori_usaha')");
        }
    }

    function update_kategori_usaha_awal($id_pemilik,$json){

        $id_pemilik    = $this->db->escape_str($id_pemilik);

        $this->db->query("delete from p_detail_kategori where id_pemilik='$id_pemilik'");
        
        $arr = json_decode($json);

        for($i=0;$i<count($arr);$i++){

            $id_kategori_usaha  = $arr[$i]->id_kategori;
            $id_jenis  = $arr[$i]->id_jenis;

            $this->db->query("insert into p_detail_kategori (id_pemilik,id_kategori) values ('$id_pemilik','$id_kategori_usaha')");
            $this->db->query("update m_pemilik_usaha set id_jenis = '$id_jenis' where id_pemilik = '$id_pemilik'");
        }
    }

    function get_jenis_kategori_usaha(){
        
        $query = $this->db->query("select * from m_jenis_kategori_usaha");
        
        return $query->result();
    }

    function get_detail_order($id_order){

        $id_order    = $this->db->escape_str($id_order);
        $sql = "SELECT *, count(id_produk) id_produk_count from 

            (select a.*,b.nama_produk, CONCAT('" . URL_IMAGE_PRODUK . "', b.foto) as foto, b.keterangan as keterangan_produk, d.alamat_antar, d.ket_perkiraan_antar as ket_konsumen, f.ket_konsumen as keterangan, d.lat_antar, d.lng_antar, d.status as order_status, nama_kategori, e.kontak, d.charge, d.include_fee, d.include_fee_note, 'umkm' as tipe_produk 
            from p_detail_produk_umkm b left join detail_order a on b.id_produk = a.id_produk left join m_kategori_produk c on b.id_kategori=c.id_kategori left join konsumen_order d on d.id_order=a.id_order left join m_konsumen e on e.id_konsumen=d.id_konsumen left join m_perkiraan_antar f on f.id=d.id_perkiraan_antar
            where  a.id_order='$id_order'
            UNION select a.*,b.nama_produk, CONCAT('" . URL_IMAGE_PRODUK . "', b.foto) as foto, b.keterangan as keterangan_produk, d.alamat_antar, d.ket_perkiraan_antar as ket_konsumen, f.ket_konsumen as keterangan, d.lat_antar, d.lng_antar, d.status as order_status, nama_kategori, e.kontak, d.charge, d.include_fee, d.include_fee_note, 'promo' as tipe_produk 
            from p_detail_produk_promo b left join detail_order a on b.id_produk = a.id_produk left join m_kategori_produk c on b.id_kategori=c.id_kategori left join konsumen_order d on d.id_order=a.id_order left join m_konsumen e on e.id_konsumen=d.id_konsumen left join m_perkiraan_antar f on f.id=d.id_perkiraan_antar
            where  a.id_order='$id_order'
            UNION select a.*,b.nama_produk, CONCAT('" . URL_IMAGE_PRODUK . "', b.foto) as foto, b.keterangan as keterangan_produk, d.alamat_antar, d.ket_perkiraan_antar as ket_konsumen, f.ket_konsumen as keterangan, d.lat_antar, d.lng_antar, d.status as order_status, nama_kategori, e.kontak, d.charge, d.include_fee, d.include_fee_note, 'regular' as tipe_produk
            from p_detail_produk b left join detail_order a on b.id_produk = a.id_produk left join m_kategori_produk c on b.id_kategori=c.id_kategori left join konsumen_order d on d.id_order=a.id_order left join m_konsumen e on e.id_konsumen=d.id_konsumen left join m_perkiraan_antar f on f.id=d.id_perkiraan_antar
            where  a.id_order='$id_order'
            UNION select a.*,b.nama_produk, CONCAT('" . URL_IMAGE_PRODUK . "', b.foto) as foto, b.keterangan as keterangan_produk, d.alamat_antar, d.ket_perkiraan_antar as ket_konsumen, f.ket_konsumen as keterangan, d.lat_antar, d.lng_antar, d.status as order_status, nama_kategori, e.kontak, d.charge, d.include_fee, d.include_fee_note, 'histori' as tipe_produk 
            from histori_p_detail_produk b left join detail_order a on b.id_produk = a.id_produk left join m_kategori_produk c on b.id_kategori=c.id_kategori left join konsumen_order d on d.id_order=a.id_order left join m_konsumen e on e.id_konsumen=d.id_konsumen left join m_perkiraan_antar f on f.id=d.id_perkiraan_antar
            where  a.id_order='$id_order' ) T1

            GROUP BY id_produk ORDER BY id_produk
            ";
            // echo $sql; die();
            $query = $this->db->query($sql);
            $results = $query->result();
            if (count($results) > 0) {
                if ($results[0]->ket_konsumen == '') {
                    $results[0]->ket_konsumen = $results[0]->keterangan;
                }
                return $results;
            } 
        
        return $query->result();
    }

    function get_histori_detail_order($id_order){

        $id_order    = $this->db->escape_str($id_order);
        $stmt = "select a.*,b.harga_promo, b.nama_produk, b.foto, b.keterangan as keterangan_produk, d.alamat_antar, d.ket_perkiraan_antar, d.ket_alamat, d.lat_antar, d.lng_antar, d.status as order_status, nama_kategori, e.kontak, d.include_fee, d.include_fee_note, d.charge, d.fee 
            from histori_p_detail_produk b left join detail_order a on b.id_produk = a.id_produk left join m_kategori_produk c on b.id_kategori=c.id_kategori left join konsumen_order d on d.id_order=a.id_order left join m_konsumen e on e.id_konsumen=d.id_konsumen
            where  a.id_order='$id_order'";
            // echo $stmt; die();
        $query = $this->db->query($stmt);
        
        return $query->result();
    }

    function get_detail_order_promo($id_order){

        $id_order    = $this->db->escape_str($id_order);
        $stmt = "select a.*,b.harga_promo, b.nama_produk, b.foto, b.keterangan as keterangan_produk, d.alamat_antar, d.ket_perkiraan_antar, d.ket_alamat, d.lat_antar, d.lng_antar, d.status as order_status, nama_kategori, e.kontak, d.include_fee, d.include_fee_note, d.charge, d.fee 
            from p_detail_produk_promo b left join detail_order a on b.id_produk = a.id_produk left join m_kategori_produk c on b.id_kategori=c.id_kategori left join konsumen_order d on d.id_order=a.id_order left join m_konsumen e on e.id_konsumen=d.id_konsumen
            where  a.id_order='$id_order'";
            // echo $stmt; die();
        $query = $this->db->query($stmt);
        
        return $query->result();
    }

    function create_perkiraan_antar($pesan_perkiraan_antar){
        $this->db->query("insert into m_perkiraan_antar (nama, ket_konsumen, ket_pemilik, status) value ('$pesan_perkiraan_antar','$pesan_perkiraan_antar','$pesan_perkiraan_antar', 1)");
        return $this->db->insert_id();
    }

    function get_perkiraan_antar($id_perkiraan_antar){

        $id_perkiraan_antar    = $this->db->escape_str($id_perkiraan_antar);

        $query = $this->db->query("select * from m_perkiraan_antar where id='$id_perkiraan_antar' and status = 1");
        
        return $query->result();
    }

    function get_all_perkiraan_antar(){

        $query = $this->db->query("select * from m_perkiraan_antar where status = 1");
        
        return $query->result();
    }

    function get_kategori_usaha($id_pemilik){

        $query = $this->db->query("select a.*,b.nama as induk_kategori from m_kategori_usaha a left join m_jenis_kategori_usaha b on a.id_jenis=b.id_jenis
            where a.status=1 and b.status=1 and a.id_jenis = (select id_jenis from m_pemilik_usaha where id_pemilik = '$id_pemilik') order by a.nama_kategori");
        
        return $query->result();
    }

    function get_kategori_usaha_awal(){

        $query = $this->db->query("select a.*,b.nama as induk_kategori from m_kategori_usaha a,m_jenis_kategori_usaha b 
            where a.id_jenis=b.id_jenis and a.status=1 and b.status=1 order by a.nama_kategori");
        
        return $query->result();
    }

    function get_pemilik_kategori($id_pemilik){

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        
        $query = $this->db->query("select a.*,b.nama_kategori from p_detail_kategori a left join m_kategori_usaha b on  a.id_kategori = b.id_kategori where a.id_pemilik='$id_pemilik' ");
        
        return $query->result();
    }
    
    function get_pemilik_lokasi($id_pemilik){

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        
        $query = $this->db->query("select * from p_detail_lokasi where id_pemilik='$id_pemilik'");
        
        return $query->result();
    }
    
    function get_pemilik_produk($id_pemilik){

        $id_pemilik = $this->db->escape_str($id_pemilik);
        
        $query = $this->db->query("select a.*,b.nama_kategori, 0 as is_official from p_detail_produk a left join m_pemilik_usaha c on c.id_partner=a.id_pemilik or c.id_pemilik=a.id_pemilik left join m_kategori_produk b on a.id_kategori=b.id_kategori where a.id_pemilik='$id_pemilik' AND a.nama_produk <> '' AND a.harga > 0 AND a.active=1 order by b.nama_kategori");
        
        // $sql = "SELECT a . * , b.nama_kategori,'1' as acuan
        // FROM p_detail_produk a, m_kategori_produk b
        // WHERE a.id_kategori = b.id_kategori
        // AND a.id_pemilik = '$id_pemilik'
        // AND b.id_pemilik = '$id_pemilik' AND a.status = '1' 
        // UNION
        // select '' as id_produk,'' as id_pemilik,a.id_kategori,'' as nama_produk,0 as stok,'' as keterangan, 
        // 0 as harga, '1' as status, '' as foto, '' as versi, a.nama_kategori, '2' as acuan 
        // from m_kategori_produk a where a.id_pemilik='$id_pemilik' and a.status = '1' 
        // and 
        // a.id_kategori not in 
        // (select id_kategori from p_detail_produk where id_pemilik='$id_pemilik' and status = '1') group by a.nama_kategori ORDER BY acuan, nama_kategori";
        // $query = $this->db->query($sql);
        return $query->result();
    }


    function get_pemilik_produk_with_promo($id_pemilik, $id_promos, $tipe){

        $id_pemilik = $this->db->escape_str($id_pemilik);
            $default_value = "null";
            if ($tipe == '2') {
                $default_value = "null";
            } else {
                $default_value = "a.harga_promo";
            }
        
        $sql = "select a.*,  " . $default_value . " as harga_promo, b.nama_kategori as nama_kategori, 0 as is_official from p_detail_produk a left join m_pemilik_usaha c on c.id_partner=a.id_pemilik or c.id_pemilik=a.id_pemilik left join m_kategori_produk b on a.id_kategori=b.id_kategori where a.id_pemilik='$id_pemilik' AND a.nama_produk <> '' AND a.harga > 0 AND a.active=1 ";

        foreach ($id_promos as $id_promo) {
            $sql = $sql . " UNION select a.*, a.harga_promo as harga_promo, b.nama_kategori as nama_kategori, 0 as is_official from p_detail_produk a left join m_pemilik_usaha c on c.id_partner=a.id_pemilik or c.id_pemilik=a.id_pemilik left join m_kategori_produk b on a.id_kategori=b.id_kategori left join produk_promo_diskon ppd on ppd.id_produk=REPLACE(a.id_produk, '".$id_pemilik."_', '') where a.id_pemilik='$id_pemilik' AND a.nama_produk <> '' AND a.harga > 0 AND a.active=1 AND ppd.id_merchant_promo='".$id_promo."' ";
        }

        $sql = $sql . " order by nama_kategori";
        $query = $this->db->query($sql);
        
        return $query->result();
    }

    function get_pemilik_produk_by_pemilik($id_pemilik){

        $id_pemilik = $this->db->escape_str($id_pemilik);
        
        $query = $this->db->query("select a.*, REPLACE(a.id_produk, '".$id_pemilik."_', '') AS id_produk, b.nama_kategori from p_detail_produk a left join m_pemilik_usaha c on c.id_partner=a.id_pemilik or c.id_pemilik=a.id_pemilik left join m_kategori_produk b on a.id_kategori=b.id_kategori where a.id_pemilik='$id_pemilik' AND c.nama <> '' order by b.nama_kategori");
        
        // $sql = "SELECT a . * , b.nama_kategori,'1' as acuan
        // FROM p_detail_produk a, m_kategori_produk b
        // WHERE a.id_kategori = b.id_kategori
        // AND a.id_pemilik = '$id_pemilik'
        // AND b.id_pemilik = '$id_pemilik' AND a.status = '1' 
        // UNION
        // select '' as id_produk,'' as id_pemilik,a.id_kategori,'' as nama_produk,0 as stok,'' as keterangan, 
        // 0 as harga, '1' as status, '' as foto, '' as versi, a.nama_kategori, '2' as acuan 
        // from m_kategori_produk a where a.id_pemilik='$id_pemilik' and a.status = '1' 
        // and 
        // a.id_kategori not in 
        // (select id_kategori from p_detail_produk where id_pemilik='$id_pemilik' and status = '1') group by a.nama_kategori ORDER BY acuan, nama_kategori";
        // $query = $this->db->query($sql);
        return $query->result();
    }

    function get_pemilik($id_pemilik){

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        
        // $query = $this->db->query("select a.*, CONCAT('" . URL_IMAGE_AVATAR_PEMILIK . "', a.avatar) as avatar, b.tipe as tipe_usaha   from m_pemilik_usaha a left join m_jenis_kategori_usaha b on (a.tipe=2 OR a.id_jenis=b.id_jenis) where a.id_pemilik='$id_pemilik'");
        $sql = "select a.*, CONCAT('" . URL_IMAGE_AVATAR_PEMILIK . "', a.avatar) as avatar, CONCAT('" . URL_IMAGE_PEMILIK . "', a.foto) as foto, b.tipe as tipe_usaha  
            from m_pemilik_usaha a left join m_jenis_kategori_usaha b on (a.tipe=2 OR a.id_jenis=b.id_jenis)
            where a.id_pemilik='$id_pemilik'";
        $pemiliks = $this->db->query($sql)->result();

        $sql = "select default_fee from setting_umum LIMIT 1";
        $setting = $this->db->query($sql)->first_row();
        $pemiliks[0]->default_fee = $setting->default_fee;

        return $pemiliks;
    }


    function test_get_pemilik($id_pemilik){

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        
        // $query = $this->db->query("select a.*, CONCAT('" . URL_IMAGE_AVATAR_PEMILIK . "', a.avatar) as avatar, b.tipe as tipe_usaha   from m_pemilik_usaha a left join m_jenis_kategori_usaha b on (a.tipe=2 OR a.id_jenis=b.id_jenis) where a.id_pemilik='$id_pemilik'");
        $sql = "select a.*, CONCAT('" . URL_IMAGE_AVATAR_PEMILIK . "', a.avatar) as avatar, CONCAT('" . URL_IMAGE_PEMILIK . "', a.foto) as foto, b.tipe as tipe_usaha  
            from m_pemilik_usaha a left join m_jenis_kategori_usaha b on (a.tipe=2 OR a.id_jenis=b.id_jenis)
            where a.id_pemilik='$id_pemilik'";
       // echo $sql; die();
       $stmt = $sql;
        $query = $this->db->query($stmt);

        return $query->result();
    }

    function get_preactive_pemilik($id_pemilik){

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        
        // $query = $this->db->query("select a.*,b.tipe as tipe_usaha,c.nama_kategori 
        //     from m_pemilik_usaha a,m_jenis_kategori_usaha b,m_kategori_usaha c 
        //     where a.id_jenis=b.id_jenis and c.id_kategori=a.id_kategori and
        //     a.id_pemilik='$id_pemilik'");
       
        $query = $this->db->query("select a.*,b.tipe as tipe_usaha
            from m_pemilik_usaha a,m_jenis_kategori_usaha b,
            where a.id_jenis=b.id_jenis and 
            a.id_pemilik='$id_pemilik'");

        return $query->row();
    }

    function get_preactive_produk($id_pemilik){

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        
        // $query = $this->db->query("select a.*,b.tipe as tipe_usaha,c.nama_kategori 
        //     from m_pemilik_usaha a,m_jenis_kategori_usaha b,m_kategori_usaha c 
        //     where a.id_jenis=b.id_jenis and c.id_kategori=a.id_kategori and
        //     a.id_pemilik='$id_pemilik'");
       
        $query = $this->db->query("select * 
            from p_detail_produk 
            where  id_pemilik='$id_pemilik'");

        return $query->row();
    }

    function proses_order($id_order,$id_perkiraan_antar, $pesan_perkiraan_antar){
        
        $id_order   = $this->db->escape_str($id_order);
        $id_perkiraan_antar   = $this->db->escape_str($id_perkiraan_antar);
        $stmt = "update konsumen_order set `status`=2, tanggal_proses = '".date('Y-m-d H:i:s')."', `id_perkiraan_antar`='$id_perkiraan_antar', `ket_perkiraan_antar`='$pesan_perkiraan_antar' where `id_order`='".$id_order."'";
        $this->db->query($stmt);    

        return $stmt;
    }

    function selesai_order($id_order){
        
        // $id_order   = $this->db->escape_str($id_order);
        
        $stmt = "update konsumen_order set `status`=4 where `id_order`='" . $id_order ."'";
        $this->db->query($stmt);
        $fee = "";
        $id_pemilik = "";
        
        $query = $this->db->query("select * from `konsumen_order` where `id_order` = '". $id_order . "'")->first_row();
        
        $fee = $query->fee;
        $id_pemilik = $query->id_pemilik;

        
        $this->db->query("update `m_pemilik_usaha` set `kredit` = `kredit` - " . $fee . " where `id_pemilik` = '".$id_pemilik."'"); 

        return $id_pemilik;       
    }

     function get_provinsi(){
        $query = $this->db->query("select * from m_provinsi where status=1 order by nama");
        return $query->result();
    }

    function get_kota($id_provinsi){
        $id_provinsi    = $this->db->escape_str($id_provinsi);
        
        $query = $this->db->query("select * from m_kota where id_provinsi = '$id_provinsi' and status=1 order by nama");
        return $query->result();
    }

    function daftar($id_pemilik,$kontak,$password,$username,$kode,$id_provinsi,$id_kota, $radius){

        $id_pemilik = $this->db->escape_str($id_pemilik);
        $kontak     = $this->db->escape_str($kontak);
        $password       = hash("sha512",$password);
        $username       = $this->db->escape_str($username);
        $kode           = $this->db->escape_str($kode);
        $radius      = $this->db->escape_str($radius);
        $id_provinsi    = $this->db->escape_str($id_provinsi);
        $id_kota        = $this->db->escape_str($id_kota);
        $lat = "-6.2293866";
        $lng = "106.6890876";
        
        $sql_stmt = "INSERT INTO `m_pemilik_usaha`(`id_pemilik`, `username`, `email`, `kontak`, `password`, `nama`, `jargon`, `alamat`, 
            `lat`, `lng`, `radius`, `foto`, `kode`, `tanggal_update`, `token`, `id_kategori`,id_provinsi,id_kota,level,id_jenis,tipe,kredit, waktu_buka, waktu_tutup) 
            select '$id_pemilik','$username','','$kontak','$password','','','','$lat','$lng','$radius','','$kode',now(),'','0','$id_provinsi','$id_kota',1,1,1,default_kredit, '07:00:00', '21:00:00'
            from setting_umum where id = '1'";
		$this->db->query($sql_stmt);
		$affected_rows = $this->db->affected_rows();

        $this->db->query("delete from kode_sms where kode = '$kode'");
        
        return $affected_rows;
    }

    public function get_data_produk($requestData) {
        
        $sql = "SELECT a.*, REPLACE(a.id_produk, CONCAT(a.id_pemilik, '_'), '') as id_produk, c.nama_kategori as nama_kategori FROM p_detail_produk a left join m_pemilik_usaha b on b.id_pemilik=a.id_pemilik left join m_kategori_produk c on c.id_kategori=a.id_kategori WHERE b.id_partner='" . ID_PARTNER . "' and a.nama_produk != '' group by a.nama_produk";
        
        $sql.=" order by a.nama_produk LIMIT ".$requestData['start']." ,".$requestData['length']."   ";

        // echo $sql; 
        // die() ;
        
        $query = $this->db->query($sql);
        return $query;
    }


    function get_partner_produk($id_partner){

        $id_pemilik = $this->db->escape_str($id_partner);
        
        $query = $this->db->query("select a.*,b.nama_kategori, 0 as is_official from p_detail_produk a left join m_kategori_produk b on a.id_kategori=b.id_kategori left join m_pemilik_usaha c on c.id_pemilik=a.id_pemilik where c.id_partner='" . ID_PARTNER . "' and a.nama_produk != '' group by a.nama_produk order by b.nama_kategori");
        
        // $sql = "SELECT a . * , b.nama_kategori,'1' as acuan
        // FROM p_detail_produk a, m_kategori_produk b
        // WHERE a.id_kategori = b.id_kategori
        // AND a.id_pemilik = '$id_pemilik'
        // AND b.id_pemilik = '$id_pemilik' AND a.status = '1' 
        // UNION
        // select '' as id_produk,'' as id_pemilik,a.id_kategori,'' as nama_produk,0 as stok,'' as keterangan, 
        // 0 as harga, '1' as status, '' as foto, '' as versi, a.nama_kategori, '2' as acuan 
        // from m_kategori_produk a where a.id_pemilik='$id_pemilik' and a.status = '1' 
        // and 
        // a.id_kategori not in 
        // (select id_kategori from p_detail_produk where id_pemilik='$id_pemilik' and status = '1') group by a.nama_kategori ORDER BY acuan, nama_kategori";
        // $query = $this->db->query($sql);
        return $query;
    }


    function daftar_pre($id_pemilik,$kontak,$password,$username,$kode,$id_provinsi,$id_kota, $radius){

        $id_pemilik = $this->db->escape_str($id_pemilik);
        $kontak     = $this->db->escape_str($kontak);
        $password       = hash("sha512",$password);
        $username       = $this->db->escape_str($username);
        $kode           = $this->db->escape_str($kode);
        $radius      = $this->db->escape_str($radius);
        $id_provinsi    = $this->db->escape_str($id_provinsi);
        $id_kota        = $this->db->escape_str($id_kota);
        $lat = "-6.2293866";
        $lng = "106.6890876";
        
        $sql_stmt = "INSERT INTO `m_pemilik_usaha`(`id_pemilik`, `username`, `email`, `kontak`, `password`, `nama`, `jargon`, `alamat`, 
            `lat`, `lng`, `radius`, `foto`, `kode`, `tanggal_update`, `token`, `id_kategori`, `status`,id_provinsi,id_kota,level,id_jenis,tipe,kredit) 
            select '$id_pemilik','$username','','$kontak','$password','','','','$lat','$lng','$radius','','$kode',now(),'','0', '0', '$id_provinsi','$id_kota',1,0,1,default_kredit 
            from setting_umum where id = '1'";
        $this->db->query($sql_stmt);
        $affected_rows = $this->db->affected_rows();

        $this->db->query("delete from kode_sms where kode = '$kode'");
        
        return $affected_rows;
    }


    function tambah_toko_preaktif($id_pemilik,$id_partner,$kontak,$kontak_sms, $kontak_sms2, $password,$username,$kode,$id_provinsi,$id_kota, $radius, $min_antar, $fee, $email){

        $id_pemilik = $this->db->escape_str($id_pemilik);
        $id_partner = $this->db->escape_str($id_partner);
        $kontak     = $this->db->escape_str($kontak);
        $kontak_sms     = $this->db->escape_str($kontak_sms);
        $kontak_sms2     = $this->db->escape_str($kontak_sms2);
        $password       = hash("sha512",$password);
        $username       = $this->db->escape_str($username);
        $kode           = $this->db->escape_str($kode);
        $radius      = $this->db->escape_str($radius);
        $min_antar      = $this->db->escape_str($min_antar);
        $id_provinsi    = $this->db->escape_str($id_provinsi);
        $id_kota        = $this->db->escape_str($id_kota);
        $email        = $this->db->escape_str($email);
        $lat = "-6.2293866";
        $lng = "106.6890876";
        
        $sql_stmt = "INSERT INTO `m_pemilik_usaha`(`id_pemilik`, `id_partner`, `username`, `email`, `kontak`, `kontak_sms`, `kontak_sms2`, `password`, `nama`, `jargon`, `alamat`, 
            `lat`, `lng`, `radius`, `foto`, `kode`, `tanggal_update`, `token`, `id_kategori`, `status` ,id_provinsi,id_kota,level,id_jenis,tipe,kredit, min_antar, `fee`) 
            select '$id_pemilik', '$id_partner', '$username','$email','$kontak','$kontak_sms','$kontak_sms2','$password','','','','$lat','$lng','$radius','','$kode',now(),'','0', 0, '$id_provinsi','$id_kota',1,1,2,default_kredit, $min_antar, $fee
            from setting_umum where id = '1'";
        $this->db->query($sql_stmt);
        $affected_rows = $this->db->affected_rows();

        $this->db->query("delete from kode_sms where kode = '$kode'");
        
        return $affected_rows;
    }


    function tambah_toko($id_pemilik,$id_partner,$kontak,$kontak_sms, $kontak_sms2, $password,$username,$kode,$id_provinsi,$id_kota, $radius, $min_antar, $fee){

        $id_pemilik = $this->db->escape_str($id_pemilik);
        $id_partner = $this->db->escape_str($id_partner);
        $kontak     = $this->db->escape_str($kontak);
        $kontak_sms     = $this->db->escape_str($kontak_sms);
        $kontak_sms2     = $this->db->escape_str($kontak_sms2);
        $password       = hash("sha512",$password);
        $username       = $this->db->escape_str($username);
        $kode           = $this->db->escape_str($kode);
        $radius      = $this->db->escape_str($radius);
        $min_antar      = $this->db->escape_str($min_antar);
        $id_provinsi    = $this->db->escape_str($id_provinsi);
        $id_kota        = $this->db->escape_str($id_kota);
        $lat = "-6.2293866";
        $lng = "106.6890876";
        
        $sql_stmt = "INSERT INTO `m_pemilik_usaha`(`id_pemilik`, `id_partner`, `username`, `email`, `kontak`, `kontak_sms`, `kontak_sms2`, `password`, `nama`, `jargon`, `alamat`, 
            `lat`, `lng`, `radius`, `foto`, `kode`, `tanggal_update`, `token`, `id_kategori`, `status`,id_provinsi,id_kota,level,id_jenis,tipe,kredit, min_antar, `fee`, waktu_buka, waktu_tutup) 
            select '$id_pemilik', '$id_partner', '$username','','$kontak','$kontak_sms','$kontak_sms2','$password','','','','$lat','$lng','$radius','','$kode',now(),'','0', '1', '$id_provinsi','$id_kota',1,1,2,default_kredit, $min_antar, $fee, '07:00:00', '21:00:00' 
            from setting_umum where id = '1'";

            // echo $sql_stmt; die();
        $this->db->query($sql_stmt);
        $affected_rows = $this->db->affected_rows();

        $this->db->query("delete from kode_sms where kode = '$kode'");
        
        return $affected_rows;
    }

    function tambah_toko_batch($id_pemilik,$id_partner,$kontak,$kontak_sms, $kontak_sms2, $password,$username,$kode,$id_provinsi,$id_kota, $radius, $min_antar, $fee, $tipe){

        $id_pemilik = $this->db->escape_str($id_pemilik);
        $id_partner = $this->db->escape_str($id_partner);
        $kontak     = $this->db->escape_str($kontak);
        $kontak_sms     = $this->db->escape_str($kontak_sms);
        $kontak_sms2     = $this->db->escape_str($kontak_sms2);
        $password       = hash("sha512",$password);
        $username       = $this->db->escape_str($username);
        $kode           = $this->db->escape_str($kode);
        $radius      = $this->db->escape_str($radius);
        $min_antar      = $this->db->escape_str($min_antar);
        $id_provinsi    = $this->db->escape_str($id_provinsi);
        $id_kota        = $this->db->escape_str($id_kota);
        $tipe        = $this->db->escape_str($tipe);
        $lat = "-6.2293866";
        $lng = "106.6890876";


        $sql = "select default_fee from setting_umum LIMIT 1";
        $setting = $this->db->query($sql)->first_row();
        $fee = $setting->default_fee;
        
        $sql_stmt = "INSERT INTO `m_pemilik_usaha`(`id_pemilik`, `id_partner`, `username`, `email`, `kontak`, `kontak_sms`, `kontak_sms2`, `password`, `nama`, `jargon`, `alamat`, 
            `lat`, `lng`, `radius`, `foto`, `kode`, `tanggal_update`, `token`, `id_kategori`, `status`,id_provinsi,id_kota,level,id_jenis,tipe,kredit, min_antar, `fee`, waktu_buka, waktu_tutup, aktif) 
            select '$id_pemilik', '$id_partner', '$username','','$kontak','$kontak_sms','$kontak_sms2','$password','','','','$lat','$lng','$radius','','$kode',now(),'','0', '1', '$id_provinsi','$id_kota',1,1,'$tipe',default_kredit, $min_antar, $fee, '07:00:00', '21:00:00','0'
            from setting_umum where id = '1'";

            // echo $sql_stmt; die();
        $this->db->query($sql_stmt);
        $affected_rows = $this->db->affected_rows();

        $this->db->query("delete from kode_sms where kode = '$kode'");
        
        return $affected_rows;
    }


    function insert_pemilik($id_pemilik,$id_jenis, $kontak,$password,$username,$nama, $jargon, $alamat, $kode, $id_provinsi, $id_kota, $id_kategori, $radius, $lat, $lng) {

        $id_pemilik = $this->db->escape_str($id_pemilik);
        $kontak     = $this->db->escape_str($kontak);
        $password       = hash("sha512",$password);
        $username       = $this->db->escape_str($username);
        $jargon       = $this->db->escape_str($jargon);
        $kode           = $this->db->escape_str($kode);
        $radius      = $this->db->escape_str($radius);
        $id_provinsi    = $this->db->escape_str($id_provinsi);
        $id_kota        = $this->db->escape_str($id_kota);
        $alamat       = $this->db->escape_str($alamat);
        
        $sql_stmt = "INSERT INTO `m_pemilik_usaha`(`id_pemilik`, `username`, `email`, `kontak`, `password`, `nama`, `jargon`, `alamat`, 
            `lat`, `lng`, `radius`, `foto`, `kode`, `tanggal_update`, `token`, `id_kategori`, `status`,id_provinsi,id_kota,level,`id_jenis`,tipe,kredit) 
            select '$id_pemilik','$username','','$kontak','$password','$nama','$jargon','$alamat','$lat','$lng','$radius','','$kode',now(),'', '0', '0','$id_provinsi','$id_kota',1,$id_jenis,1,default_kredit 
            from setting_umum where id = '1'";

        $this->db->query($sql_stmt);
        
        return $this->db->affected_rows() == 1;
    }

    function isi_profil_awal($id_pemilik,$nama,$alamat,$jargon, $radius, $kontak_sms, $kontak_sms2, $include_fee='1', $include_fee_note='', $delivery=0, $tipe_kurir=0){

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $nama    = $this->db->escape_str($nama);
        $alamat    = $this->db->escape_str($alamat);
        $jargon    = $this->db->escape_str($jargon);
        $radius    = $this->db->escape_str($radius);
        $include_fee    = $this->db->escape_str($include_fee);
        $include_fee_note    = $this->db->escape_str($include_fee_note);
        $delivery    = $this->db->escape_str($delivery);
        $tipe_kurir    = $this->db->escape_str($tipe_kurir);
        
        $this->db->query("update m_pemilik_usaha set aktif=1, nama = '$nama', alamat = '$alamat', jargon = '$jargon', radius='$radius', kontak_sms='$kontak_sms', kontak_sms2='$kontak_sms2', include_fee='$include_fee', include_fee_note='$include_fee_note', delivery='$delivery', tipe_kurir='$tipe_kurir' where id_pemilik='$id_pemilik'");
    }


    function update_profile($id_pemilik,$nama,$alamat,$jargon, $radius, $kontak_sms, $kontak_sms2, $fee, $include_fee='1', $include_fee_note='', $delivery=0, $tipe_kurir=0){

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $nama    = $this->db->escape_str($nama);
        $alamat    = $this->db->escape_str($alamat);
        $jargon    = $this->db->escape_str($jargon);
        $fee    = $this->db->escape_str($fee);
        $radius    = $this->db->escape_str($radius);
        $include_fee    = $this->db->escape_str($include_fee);
        $include_fee_note    = $this->db->escape_str($include_fee_note);
        $delivery    = $this->db->escape_str($delivery);
        $tipe_kurir    = $this->db->escape_str($tipe_kurir);

        $stmt = "update m_pemilik_usaha set aktif=1, nama = '$nama', alamat = '$alamat', jargon = '$jargon', radius='$radius', kontak_sms='$kontak_sms', kontak_sms2='$kontak_sms2', fee=$fee, include_fee='$include_fee', include_fee_note='$include_fee_note', delivery='$delivery', tipe_kurir='$tipe_kurir' where id_pemilik='$id_pemilik'";
        // echo $stmt; die();
        
        $this->db->query($stmt);
    }


    function set_kurir_sendiri($id_pemilik, $fee, $include_fee='1', $include_fee_note='', $delivery=0, $tipe_kurir=0){

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $fee    = $this->db->escape_str($fee);
        $include_fee    = $this->db->escape_str($include_fee);
        $include_fee_note    = $this->db->escape_str($include_fee_note);
        $delivery    = $this->db->escape_str($delivery);
        $tipe_kurir    = $this->db->escape_str($tipe_kurir);

        $stmt = "update m_pemilik_usaha set fee='$fee', include_fee='$include_fee', include_fee_note='$include_fee_note', delivery='$delivery', tipe_kurir='$tipe_kurir' where id_pemilik='$id_pemilik'";
        // echo $stmt; die();
        
        $this->db->query($stmt);
    }




    function set_tipe_kurir($id_pemilik, $tipe_kurir=0){

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $tipe_kurir    = $this->db->escape_str($tipe_kurir);

        $stmt = "update m_pemilik_usaha set tipe_kurir='$tipe_kurir' where id_pemilik='$id_pemilik'";
        // echo $stmt; die();
        
        $this->db->query($stmt);
    }


    function set_delivery($id_pemilik,$delivery=0, $tipe_kurir=0){

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $delivery    = $this->db->escape_str($delivery);
        $tipe_kurir    = $this->db->escape_str($tipe_kurir);
        
        $this->db->query("update m_pemilik_usaha set delivery='$delivery', tipe_kurir='$tipe_kurir' where id_pemilik='$id_pemilik'");
    }



    function set_include_fee($id_pemilik, $fee, $include_fee, $include_fee_note=''){

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $fee    = $this->db->escape_str($fee);
        $include_fee    = $this->db->escape_str($include_fee);
        $include_fee_note    = $this->db->escape_str($include_fee_note);
        
        $this->db->query("update m_pemilik_usaha set fee=$fee, include_fee='$include_fee', include_fee_note='$include_fee_note' where id_pemilik='$id_pemilik'");
    }

    function set_alamat_min_antar($id_pemilik,$alamat,$min_antar){

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $min_antar    = $this->db->escape_str($min_antar);
        $alamat    = $this->db->escape_str($alamat);
        
        $this->db->query("update m_pemilik_usaha set alamat = '$alamat', min_antar='$min_antar' where id_pemilik = '$id_pemilik'");
    }

    function set_lokasi_min_antar($id_pemilik,$alamat,$min_antar,$lat,$lng){

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $min_antar    = $this->db->escape_str($min_antar);
        $alamat    = $this->db->escape_str($alamat);
        $lat    = $this->db->escape_str($lat);
        $lng    = $this->db->escape_str($lng);
        
        $this->db->query("update m_pemilik_usaha set alamat = '$alamat', min_antar='$min_antar', lat='$lat', lng='$lng' where id_pemilik = '$id_pemilik'");
    }

    function set_fee($id_pemilik, $fee){

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $fee    = $this->db->escape_str($fee);
        
        $this->db->query("update m_pemilik_usaha set fee=$fee where id_pemilik = '$id_pemilik'");
    }

    function set_radius($id_pemilik, $radius){

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $radius    = $this->db->escape_str($radius);
        
        $this->db->query("update m_pemilik_usaha set radius='$radius' where id_pemilik = '$id_pemilik'");
    }

     function set_no_telp($id_pemilik, $kontak, $kontak_sms, $kontak_sms2) {

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $kontak    = $this->db->escape_str($kontak);
        $kontak_sms    = $this->db->escape_str($kontak_sms);
        $kontak_sms2    = $this->db->escape_str($kontak_sms2);

        $this->db->query("update m_pemilik_usaha set kontak='$kontak', kontak_sms='$kontak_sms', kontak_sms2='$kontak_sms2'  where id_pemilik = '$id_pemilik'");
     }


     function update_no_telp_kuber($id_pemilik, $kontak_sms2) {

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $kontak_sms2    = $this->db->escape_str($kontak_sms2);

        $this->db->query("update m_pemilik_usaha set kontak_sms2='$kontak_sms2'  where id_pemilik = '$id_pemilik'");
     }

     function update_kontak_n_password($id_pemilik, $kontak, $password) {

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $kontak    = $this->db->escape_str($kontak);
        $password    =  hash("sha512",$password);

        $this->db->query("update m_pemilik_usaha set kontak='$kontak', password='$password'  where id_pemilik = '$id_pemilik'");
     }

    function update_no_telp_usaha($id_pemilik, $kontak) {

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $kontak    = $this->db->escape_str($kontak);

        $this->db->query("update m_pemilik_usaha set kontak='$kontak'  where id_pemilik = '$id_pemilik'");
     }

    function update_no_telp_notifikasi($id_pemilik, $kontak_sms) {

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $kontak_sms    = $this->db->escape_str($kontak_sms);

        $this->db->query("update m_pemilik_usaha set kontak_sms='$kontak_sms'  where id_pemilik = '$id_pemilik'");
     }

    function isi_lokasi_awal($id_pemilik,$lat,$lng,$nama_lokasi){

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $lat    = $this->db->escape_str($lat);
        $lng    = $this->db->escape_str($lng);
        $nama_lokasi    = $this->db->escape_str($nama_lokasi);
        
        $this->db->query("update m_pemilik_usaha set lat = '$lat',lng = '$lng' where id_pemilik = '$id_pemilik'");

        $lokasi = $this->db->query("select * from p_detail_lokasi where id_pemilik='$id_pemilik' LIMIT 1")->row();

        if (empty($lokasi)) {
            $id_lokasi = date("YmdHis").$id_pemilik;
            $this->db->query("insert into p_detail_lokasi (id_lokasi,id_pemilik,nama,lat,lng,jenis_lokasi) values ('$id_lokasi','$id_pemilik','$nama_lokasi','$lat','$lng','1')");
        } else {
            $this->db->query("update p_detail_lokasi set id_pemilik='$id_pemilik', nama='$nama_lokasi', lat='$lat',lng='$lng', jenis_lokasi='1' where id_pemilik='$id_pemilik'");
        }
    }

    function get_id_pemilik_by_no_telp_and_username($no_telp, $username){
        
        $no_telp   = $this->db->escape_str($no_telp);
        $username   = $this->db->escape_str($username);

        $query = $this->db->query("select * from m_pemilik_usaha where kontak = '$no_telp' or username = '$username'");
        return $query->first_row();
    }

    function get_id_pemilik_by_no_telp($no_telp){
        
        $no_telp   = $this->db->escape_str($no_telp);

        $query = $this->db->query("select * from m_pemilik_usaha where kontak = '$no_telp'");
        return $query->first_row();
    }

    function new_password($id_pemilik,$password){

        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $password   = hash("sha512",$password);
       
        $this->db->query("update m_pemilik_usaha set password = '$password' where id_pemilik = '$id_pemilik'");
    }

    function check_password($id_pemilik,$password){
        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $password   = hash("sha512",$password);

        return $this->db->query("select * from m_pemilik_usaha where id_pemilik = '$id_pemilik' and password = '$password'")->row();
    }

    function copy_to_histori($id_pemilik){
        
        $id_pemilik   = $this->db->escape_str($id_pemilik);

        $this->db->query("INSERT INTO `histori_m_pemilik_usaha` SELECT * FROM `m_pemilik_usaha` WHERE id_pemilik = '$id_pemilik'");
    }

    function hapus_akun($id_pemilik){

        $this->db->query("DELETE FROM `m_pemilik_usaha` WHERE id_pemilik = '$id_pemilik'");
    }

    function get_penjualan_harian($id_pemilik,$tanggal){
        $query = $this->db->query("select a.id_order,a.tanggal_order,sum(a.fee) as fee, sum(a.charge) as charge,a.id_pemilik,b.nama_kategori,
        COALESCE(f.nama_produk, COALESCE(c.nama_produk, e.nama_produk)) as nama_produk, COALESCE(f.id_produk, COALESCE(c.id_produk, e.id_produk)) as id_produk,  COALESCE(f.keterangan, COALESCE(c.keterangan, e.keterangan)) as keterangan, COALESCE(f.foto, COALESCE(c.foto, e.foto)) as foto,
                    sum(d.kuantitas) as total_kuantitas,sum(d.subtotal) as subtotal
                    from
                    konsumen_order a 
                    left join detail_order d on a.id_order = d.id_order 
                    left join p_detail_produk c on c.id_produk = d.id_produk 
                    left join histori_p_detail_produk e on e.id_produk = d.id_produk 
                    left join p_detail_produk_promo f on f.id_produk = d.id_produk 
                    left join m_kategori_produk b on (b.id_kategori = c.id_kategori or b.id_kategori = e.id_kategori)
                    where
                    DATE(a.tanggal_order) = '$tanggal'
                    and
                    a.id_pemilik = '$id_pemilik'
                    and
                    a.status = 4
                    group by c.id_produk
                    order by b.nama_kategori");
        return $query->result();
    }
    
    function get_penjualan_bulanan($id_pemilik,$bulan,$tahun){
        $stmt = "select a.id_order,a.tanggal_order,sum(a.fee) as fee, sum(a.charge) as charge,a.id_pemilik,b.nama_kategori, COALESCE(f.nama_produk, COALESCE(c.nama_produk, e.nama_produk)) as nama_produk, COALESCE(f.id_produk, COALESCE(c.id_produk, e.id_produk)) as id_produk,  COALESCE(f.keterangan, COALESCE(c.keterangan, e.keterangan)) as keterangan,  COALESCE(f.foto, COALESCE(c.foto, e.foto)) as foto,
                    sum(d.kuantitas) as total_kuantitas,sum(d.subtotal) as subtotal
                    from
                    konsumen_order a 
                    left join detail_order d on a.id_order = d.id_order 
                    left join p_detail_produk c on c.id_produk = d.id_produk 
                    left join histori_p_detail_produk e on e.id_produk = d.id_produk 
                    left join p_detail_produk_promo f on f.id_produk = d.id_produk 
                    left join m_kategori_produk b on (b.id_kategori = c.id_kategori or b.id_kategori = e.id_kategori)
                    where MONTH(a.tanggal_order) = '$bulan' and YEAR(a.tanggal_order) = '$tahun'
                    and
                    a.id_pemilik = '$id_pemilik'
                    and
                    a.status = 4
                    group by c.id_produk
                    order by b.nama_kategori";
                    // echo $stmt;die();
        $query = $this->db->query($stmt);
        return $query->result();
    }
    
    function get_penjualan_tahunan($id_pemilik,$tahun){
        $stmt = "select a.id_order,a.tanggal_order,sum(a.fee) as fee, sum(a.charge) as charge,a.id_pemilik,b.nama_kategori,
        COALESCE(f.nama_produk, COALESCE(c.nama_produk, e.nama_produk)) as nama_produk, COALESCE(f.id_produk, COALESCE(c.id_produk, e.id_produk)) as id_produk,  COALESCE(f.keterangan, COALESCE(c.keterangan, e.keterangan)) as keterangan,  COALESCE(f.foto, COALESCE(c.foto, e.foto)) as foto,
                    sum(d.kuantitas) as total_kuantitas,sum(d.subtotal) as subtotal
                    from
                    konsumen_order a 
                    left join detail_order d on a.id_order = d.id_order 
                    left join p_detail_produk c on c.id_produk = d.id_produk 
                    left join histori_p_detail_produk e on e.id_produk = d.id_produk 
                    left join p_detail_produk_promo f on f.id_produk = d.id_produk 
                    left join m_kategori_produk b on (b.id_kategori = c.id_kategori or b.id_kategori = e.id_kategori)
                    where
                    YEAR(a.tanggal_order) = '$tahun'
                    and
                    a.id_pemilik = '$id_pemilik'
                    and
                    a.status = 4
                    group by d.id_produk
                    order by b.nama_kategori";
                    // echo $stmt; die();
        $query = $this->db->query($stmt);

        
        
        return $query->result();
    }

    function set_foto($id_pemilik,$foto){
        
        $id_pemilik   = $this->db->escape_str($id_pemilik);
        $foto   = $this->db->escape_str($foto);

        $this->db->query("update m_pemilik_usaha set foto = '$foto', versi = versi+1 where id_pemilik = '$id_pemilik'");
    }


    function set_foto_master_batch($id_pemilik){
        
        $id_pemilik   = $this->db->escape_str($id_pemilik);
        $stmt = "update p_detail_produk set foto = REPLACE(id_produk, '" . $id_pemilik . "_', '') where id_pemilik = '$id_pemilik'";
        // echo $stmt; die();

        $this->db->query($stmt);
    }

    function set_foto_avatar($id_pemilik,$foto){
        
        $id_pemilik   = $this->db->escape_str($id_pemilik);
        $foto   = $this->db->escape_str($foto);

        $this->db->query("update m_pemilik_usaha set avatar = '$foto', versi_avatar = versi_avatar+1 where id_pemilik = '$id_pemilik'");
    }

    function set_foto_produk($id_produk,$foto, $is_umkm){
        
        $id_produk   = $this->db->escape_str($id_produk);
        $foto   = $this->db->escape_str($foto);

        $umkm = "";
        if (intval($is_umkm) == 1) {
            $umkm = "_umkm";
        }

        $this->db->query("update p_detail_produk" . $umkm . " set foto = '$foto', versi = versi+1 where id_produk = '$id_produk'");
    }

    function get_foto($id_pemilik){
        
        $id_pemilik   = $this->db->escape_str($id_pemilik);

        $query = $this->db->query("select foto,versi from m_pemilik_usaha where id_pemilik = '$id_pemilik'");

        return $query->first_row();
    }

    function get_foto_avatar($id_pemilik){
        
        $id_pemilik   = $this->db->escape_str($id_pemilik);

        $query = $this->db->query("select avatar,versi_avatar from m_pemilik_usaha where id_pemilik = '$id_pemilik'");

        return $query->first_row();
    }

    function get_foto_produk($id_produk){
        
        $id_pemilik   = $this->db->escape_str($id_produk);

        $query = $this->db->query("select foto,versi from p_detail_produk where id_produk = '$id_produk'");

        return $query->first_row();
    }

    function get_produk_by_id($id_produk){
        
        $id_produk   = $this->db->escape_str($id_produk);

        $query = $this->db->query("select * from p_detail_produk where id_produk = '$id_produk'");

        return $query->first_row();
    }


    function get_produk_by_barcode_id($id_produk, $nama_produk){
        $id_produk   = $this->db->escape_str($id_produk);
        $nama_produk   = $this->db->escape_str($nama_produk);

        $stmt = "select * from p_detail_produk where id_produk LIKE '%_". $id_produk . "%' AND nama_produk ='".$nama_produk."'";
        // echo $stmt; die();
        $query = $this->db->query($stmt);

        return $query->first_row();
    }

    function get_produk_umkm_by_id($id_produk){
        
        $id_pemilik   = $this->db->escape_str($id_produk);

        $query = $this->db->query("select * from p_detail_produk_umkm where id_produk = '$id_produk'");

        return $query->first_row();
    }

    function notif_get_pemilik($id_pemilik){
        
        $query = $this->db->query("select * from m_pemilik_usaha where id_pemilik = '$id_pemilik'");
        return $query->result();
    }

    function notif_get_konsumen($id_order){
        
        $query = $this->db->query("select a.* 
            from m_konsumen a left join konsumen_order b on a.id_konsumen = b.id_konsumen
            where b.id_order = '$id_order'");
        return $query->result();
    }

    function notif_get_order($id_order){
        $query = $this->db->query("select ko.id_order, ko.total, ko.status, ko.id_perkiraan_antar, ko.tanggal_order
            from konsumen_order ko
            where ko.id_order = '$id_order'");
        return $query->result();
    }

    function update_token($id_pemilik,$token){
        $id_pemilik   = $this->db->escape_str($id_pemilik);
        $token   = $this->db->escape_str($token);
        
        $query = $this->db->query("update m_pemilik_usaha set token='$token' where id_pemilik='$id_pemilik'");
    }

    function get_order_dari_notif($id_order){
        
        $id_order    = $this->db->escape_str($id_order);
        
        $query = $this->db->query("select a.*, mpa.nama as ket_antar, b.nama,b.alamat,b.kontak,b.lat,b.lng,CONCAT('" . URL_IMAGE_KONSUMEN . "', b.foto) as foto,b.versi, c.nama as nama_kota 
                    from konsumen_order a left join m_konsumen b ON a.id_konsumen=b.id_konsumen left join m_kota c ON c.id = b.id_kota left join m_perkiraan_antar mpa on mpa.id=a.id_perkiraan_antar
                    where a.id_order='$id_order' 
                    order by a.status asc 
                    ");
        
        return $query->result();
    }

    function get_konsumen_order($id_order){
        
        $id_order   = $this->db->escape_str($id_order);

        $query = $this->db->query("select * from konsumen_order where id_order='$id_order' LIMIT 1")->row();

        // echo $this->db->last_query(); die();

        return $query;
    }


    function update_provinsi_kota($id_konsumen,$id_provinsi,$id_kota){

        $id_konsumen    = $this->db->escape_str($id_konsumen);
        $id_provinsi    = $this->db->escape_str($id_provinsi);
        $id_kota    = $this->db->escape_str($id_kota);
       
        $this->db->query("update m_pemilik_usaha set id_provinsi = '$id_provinsi' AND id_kota = '$id_kota' where id_pemilik = '$id_konsumen'");
    }

    
    function update_kategori_produk($id_kategori,$nama_kategori){        

        $id_kategori    = $this->db->escape_str($id_kategori);
        $nama_kategori    = $this->db->escape_str($nama_kategori);

        $query   = "update m_kategori_produk set nama_kategori = '$nama_kategori' 
                            where id_kategori = '$id_kategori'";
        
        $this->db->query($query);
    }

    function delete_kategori_produk($id_kategori){

        $id_kategori    = $this->db->escape_str($id_kategori);
        
        $this->db->query("update m_kategori_produk set status = '2' where id_kategori = '$id_kategori'");        
        $this->db->query("update p_detail_produk set status = '2' where id_kategori = '$id_kategori'");
    }

    function tambah_ongkirs($id_pemilik, $ongkirs) {
        $this->db->query("delete from m_ongkir where id_pemilik='" . $id_pemilik ."'");
        foreach ($ongkirs as $ongkir) {
            $this->db->query("insert into m_ongkir (id_pemilik, jarak_min, jarak_max, fee) VALUE('" . $id_pemilik . "', " 
                . $ongkir->radius_min . ", " . $ongkir->radius_max . ", " . $ongkir->fee . ")");
        }
    }


    function get_ongkirs($id_pemilik) {
        return $this->db->query("select * from m_ongkir where id_pemilik='" . $id_pemilik . "'")->result();
    }

    function insert_produk_promo_batch($id_merchant_promo, $rows){    

        $query_produk   = "INSERT INTO `m_promo_produk` (`id_merchant_promo`, `id_barcode`, `qty`, `id_barcode2`, `qty2`) VALUES ";

        // echo $k; die();
        // echo "count " . count($rows); die();

        $i = 0;
        foreach ($rows as $produk) {
            // if ($produk->harga_promo != "") {
                $j=0;

                $paramBuilder = "";

                $paramBuilder = $paramBuilder . "'".$produk->barcode . "', ";
                $paramBuilder = $paramBuilder . "'".$produk->min_qty . "', ";
                $paramBuilder = $paramBuilder . "'".$produk->barcode_barang_promo . "', ";
                $paramBuilder = $paramBuilder . "'".$produk->qty_promo . "'";

                $query_produk = $query_produk . "(";
                $query_produk = $query_produk . "'".$id_merchant_promo . "', ";
                $query_produk = $query_produk . $paramBuilder . ")";

                $i++;
            // }
        }
        $query_produk = str_replace(")(", "),(", $query_produk);

        // echo $query_produk; die();
        
        $this->db->query($query_produk);
    }


    function set_harga_promo_batch($id_pemilik, $rows, $id_merchant_promo) { 

        $insert = "INSERT INTO p_detail_produk (id_produk, id_pemilik, nama_produk, id_kategori, keterangan, harga, harga_promo, min_antar, active) VALUES ";
        $update = "ON DUPLICATE KEY UPDATE harga_promo=VALUES(harga_promo), min_antar=VALUES(min_antar), active=1";

        foreach ($rows as $row) {
            if ($row->harga_promo != "") {
                $insert = $insert . "('" . $id_pemilik . "_" . $row->barcode . "', '" . $id_pemilik . "', '', 1, '', 0, " . $row->harga_promo . ", 1, 1), ";
            }
            // $i++;
        }
        $stmt = $insert . $update;
        $stmt = str_replace("), ON DUPLICATE KEY UPDATE", ") ON DUPLICATE KEY UPDATE", $stmt);

        // echo $stmt; die();
        $this->db->query($stmt);
        
    }

    function insert_produk_promo_diskon_batch($rows, $id_merchant_promo) { 
       
        $update2 = "XXX";

        $insert2 = "INSERT INTO produk_promo_diskon (id_produk, id_merchant_promo, min_qty, harga_promo) VALUES ";
        foreach ($rows as $row) {
            if ($row->harga_promo != "") {
                $insert2 = $insert2 . "('" . $row->barcode . "', " . $id_merchant_promo . ", " . (isset($row->min_qty) ? $row->min_qty : "1") . ", " . $row->harga_promo . "), ";
            }
            // $i++;
        }

        $stmt2 = $insert2 . $update2;
        $stmt2 = str_replace("), XXX", ")", $stmt2);
        // echo $stmt; die();
        $this->db->query($stmt2);
        
    }

    function update_waktu_buka_tutup($id_pemilik, $waktu_buka, $waktu_tutup) {


        $id_pemilik    = $this->db->escape_str($id_pemilik);
        $waktu_buka = $this->db->escape_str($waktu_buka);
        $waktu_tutup = $this->db->escape_str($waktu_tutup);
        
        $sql = "update m_pemilik_usaha set waktu_buka = '$waktu_buka', waktu_tutup='$waktu_tutup' where id_pemilik = '$id_pemilik'";
        // echo $sql; die();
        $this->db->query($sql);      
    }

}