<?php  
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_api_konsumen extends CI_Model 
{

    const TIPE_PEDAGANG_KELILING = 4; 

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }


    function get_random_num() {
        $result = $this->db->query("SELECT FLOOR(RAND() * 99999999) AS random_num
            FROM m_pemilik_usaha WHERE 'random_num' NOT IN (SELECT id_pemilik FROM m_pemilik_usaha)
            LIMIT 1")->row();
        return $result->random_num;
    }
    
    //untuk login    
    function login($no_telp,$password){

        $no_telp   = $this->db->escape_str($no_telp);
        $password   = hash("sha512",$password);
       
        $query = $this->db->query("select * from m_konsumen where kontak='$no_telp' AND (id_partner IS NULL OR id_partner='') and password='$password' and status=1");

        // echo $no_telp." -> ".$password;
       
        return $query;
    } 

    function login_id($id_konsumen){

        $id_konsumen    = $this->db->escape_str($id_konsumen);
       
        $query = $this->db->query("select * from m_konsumen where id_konsumen='$id_konsumen' and status=1");
       
        return $query->result();
    }

    function insert_code($id,$code){

        $id    = $this->db->escape_str($id);
        $code    = $this->db->escape_str($code);
       
        $this->db->query("insert into kode_sms (id,kode) values ('$id','$code')");
    }

    function cek_no_telp($no_telp){
        $no_telp   = $this->db->escape_str($no_telp);

        $query = $this->db->query("select count(*) as total from m_konsumen where kontak='$no_telp' AND (id_partner IS NULL OR id_partner='') and status=1");
       
        return $query->first_row()->total;
    }

    function cek_kode($id_kode,$kode){
        $id_kode    = $this->db->escape_str($id_kode);
        $kode    = $this->db->escape_str($kode);
        $query = $this->db->query("select count(*) as total from kode_sms where id = '$id_kode' and kode = '$kode'");
        return $query->first_row()->total;
    }

    function new_password($id_konsumen,$password){

        $id_konsumen    = $this->db->escape_str($id_konsumen);
        $password   = hash("sha512",$password);
       
        $this->db->query("update m_konsumen set password = '$password' where id_konsumen = '$id_konsumen'");
    }    

    function check_password($id_konsumen,$password){
        $id_konsumen    = $this->db->escape_str($id_konsumen);
        $password   = hash("sha512",$password);

        return $this->db->query("select * from m_konsumen where id_konsumen = '$id_konsumen' and password = '$password'")->row();
    }

    function get_radius(){
        $query = $this->db->query("select * from m_radius order by radius");
        return $query->result();
    }

    function get_provinsi(){
        $query = $this->db->query("select * from m_provinsi where status=1 order by nama");
        return $query->result();
    }
    
    function get_kota($id_provinsi){
        $id_provinsi    = $this->db->escape_str($id_provinsi);
        
        $query = $this->db->query("select * from m_kota where id_provinsi = '$id_provinsi' and status=1 order by nama");
        return $query->result();
    }

    function get_jenis_usaha(){
        $query = $this->db->query("select a.*,b.* from m_kategori_usaha a,m_jenis_kategori b where a.id_jenis = b.id_jenis and a.status=1");
        return $query->result();
    }

    function get_pemilik($id_kategori,$lat,$lng){
             $id_kategori   = $this->db->escape_str($id_kategori);
             // $kategori_where = "a.id_kategori IN ( 0,$id_kategori ) AND ";
             // if (substr($id_kategori, -1) == ',') {
             //    $id_kategori = str_replace(",", "0", $id_kategori);
             // }

             // if ($id_kategori == '0') {

                $kategori_where = "";
             // }

            $kategori_where = str_replace(", )", ")", $kategori_where);

            $sql = "";
           
            // $sql = "SELECT b . * , c.nama AS nama_kota, f.icon_biru, f.icon_hitam, f.versi_icon_biru, f.versi_icon_hitam, (
            // 6371 * ACOS( COS( RADIANS(  '$lat' ) ) * COS( RADIANS( e.lat ) ) * COS( RADIANS( e.lng ) - RADIANS(  '$lng' ) ) + SIN( RADIANS( '$lat' ) ) * SIN( RADIANS( e.lat ) ) )
            // ) as distance
            // FROM m_pemilik_usaha b, m_kota c, m_level d, p_detail_lokasi e, m_jenis_kategori_usaha f
            // WHERE 
            // b.id_pemilik = e.id_pemilik
            // AND b.level = d.id
            // AND b.id_kota = c.id
            // AND b.id_jenis = f.id_jenis
            // AND (
            // 6371 * ACOS( COS( RADIANS(  '$lat' ) ) * COS( RADIANS( e.lat ) ) * COS( RADIANS( e.lng ) - RADIANS(  '$lng' ) ) + SIN( RADIANS( '$lat' ) ) * SIN( RADIANS( e.lat ) ) )
            // ) <= (b.radius /1000 ) 
            // ORDER BY distance
            // LIMIT 0 , 30";

            $sql = "SELECT b . *, 'toko' as tipe_merchant, CONCAT('" . URL_IMAGE_AVATAR_PEMILIK . "', b.avatar) as avatar, CONCAT('" . URL_IMAGE_PEMILIK . "', b.foto) as foto , c.nama AS nama_kota, f.icon_biru, f.icon_hitam, f.versi_icon_biru, f.versi_icon_hitam, ( 6371 * ACOS( COS( RADIANS( '$lat' ) ) * COS( RADIANS( b.lat ) ) * COS( RADIANS( b.lng ) - RADIANS( '$lng' ) ) + SIN( RADIANS( '$lat' ) ) * SIN( RADIANS( b.lat ) ) ) ) as distance 
            FROM m_pemilik_usaha b 
            left join m_kota c ON b.id_kota = c.id 
            left join m_level d ON b.level = d.id 
            left join p_detail_lokasi e ON b.id_pemilik = e.id_pemilik 
            left join m_jenis_kategori_usaha f ON b.id_jenis = f.id_jenis 
            WHERE ( 6371 * ACOS( COS( RADIANS( '$lat' ) ) * COS( RADIANS( b.lat ) ) * COS( RADIANS( b.lng ) - RADIANS( '$lng' ) ) + SIN( RADIANS( '$lat' ) ) * SIN( RADIANS( b.lat ) ) ) ) <= (b.radius /1000 )  AND b.aktif=1 AND b.tipe!=4
            ORDER BY distance";


            // echo $sql; die();
            

            $query = $this->db->query($sql);

            return $query->result();
    }


    function test_get_pemilik($id_kategori,$lat,$lng){
             $id_kategori   = $this->db->escape_str($id_kategori);
             // $kategori_where = "a.id_kategori IN ( 0,$id_kategori ) AND ";
             // if (substr($id_kategori, -1) == ',') {
             //    $id_kategori = str_replace(",", "0", $id_kategori);
             // }

             // if ($id_kategori == '0') {

                $kategori_where = "";
             // }

            $kategori_where = str_replace(", )", ")", $kategori_where);

            $sql = "";
           
            // $sql = "SELECT b . * , c.nama AS nama_kota, f.icon_biru, f.icon_hitam, f.versi_icon_biru, f.versi_icon_hitam, (
            // 6371 * ACOS( COS( RADIANS(  '$lat' ) ) * COS( RADIANS( e.lat ) ) * COS( RADIANS( e.lng ) - RADIANS(  '$lng' ) ) + SIN( RADIANS( '$lat' ) ) * SIN( RADIANS( e.lat ) ) )
            // ) as distance
            // FROM m_pemilik_usaha b, m_kota c, m_level d, p_detail_lokasi e, m_jenis_kategori_usaha f
            // WHERE 
            // b.id_pemilik = e.id_pemilik
            // AND b.level = d.id
            // AND b.id_kota = c.id
            // AND b.id_jenis = f.id_jenis
            // AND (
            // 6371 * ACOS( COS( RADIANS(  '$lat' ) ) * COS( RADIANS( e.lat ) ) * COS( RADIANS( e.lng ) - RADIANS(  '$lng' ) ) + SIN( RADIANS( '$lat' ) ) * SIN( RADIANS( e.lat ) ) )
            // ) <= (b.radius /1000 ) 
            // ORDER BY distance
            // LIMIT 0 , 30";

            $sql = "SELECT b . *, CONCAT('" . URL_IMAGE_AVATAR_PEMILIK . "', b.avatar) as avatar, CONCAT('" . URL_IMAGE_PEMILIK . "', b.foto) as foto , c.nama AS nama_kota, f.icon_biru, f.icon_hitam, f.versi_icon_biru, f.versi_icon_hitam, ( 6371 * ACOS( COS( RADIANS( '$lat' ) ) * COS( RADIANS( b.lat ) ) * COS( RADIANS( b.lng ) - RADIANS( '$lng' ) ) + SIN( RADIANS( '$lat' ) ) * SIN( RADIANS( b.lat ) ) ) ) as distance 
            FROM m_pemilik_usaha b 
            left join m_kota c ON b.id_kota = c.id 
            left join m_level d ON b.level = d.id 
            left join p_detail_lokasi e ON b.id_pemilik = e.id_pemilik 
            left join m_jenis_kategori_usaha f ON b.id_jenis = f.id_jenis 
            WHERE ( 6371 * ACOS( COS( RADIANS( '$lat' ) ) * COS( RADIANS( b.lat ) ) * COS( RADIANS( b.lng ) - RADIANS( '$lng' ) ) + SIN( RADIANS( '$lat' ) ) * SIN( RADIANS( b.lat ) ) ) ) <= (b.radius /1000 )  AND b.aktif=1 AND b.tipe!=4
            ORDER BY distance";


            // echo $sql; die();
            

            $query = $this->db->query($sql);

            return $query->result();
    }


    function get_pedagang($id_kategori,$lat,$lng){
             $id_kategori   = $this->db->escape_str($id_kategori);
             // $kategori_where = "a.id_kategori IN ( 0,$id_kategori ) AND ";
             // if (substr($id_kategori, -1) == ',') {
             //    $id_kategori = str_replace(",", "0", $id_kategori);
             // }

             // if ($id_kategori == '0') {

                $kategori_where = "";
             // }

            $kategori_where = str_replace(", )", ")", $kategori_where);

            $sql = "";
           
            // $sql = "SELECT b . * , c.nama AS nama_kota, f.icon_biru, f.icon_hitam, f.versi_icon_biru, f.versi_icon_hitam, (
            // 6371 * ACOS( COS( RADIANS(  '$lat' ) ) * COS( RADIANS( e.lat ) ) * COS( RADIANS( e.lng ) - RADIANS(  '$lng' ) ) + SIN( RADIANS( '$lat' ) ) * SIN( RADIANS( e.lat ) ) )
            // ) as distance
            // FROM m_pemilik_usaha b, m_kota c, m_level d, p_detail_lokasi e, m_jenis_kategori_usaha f
            // WHERE 
            // b.id_pemilik = e.id_pemilik
            // AND b.level = d.id
            // AND b.id_kota = c.id
            // AND b.id_jenis = f.id_jenis
            // AND (
            // 6371 * ACOS( COS( RADIANS(  '$lat' ) ) * COS( RADIANS( e.lat ) ) * COS( RADIANS( e.lng ) - RADIANS(  '$lng' ) ) + SIN( RADIANS( '$lat' ) ) * SIN( RADIANS( e.lat ) ) )
            // ) <= (b.radius /1000 ) 
            // ORDER BY distance
            // LIMIT 0 , 30";

            $sql = "SELECT b . *, e.lat as lat, e.lng as lng, CONCAT('" . URL_IMAGE_AVATAR_PEMILIK . "', b.avatar) as avatar, CONCAT('" . URL_IMAGE_PEMILIK . "', b.foto) as foto , c.nama AS nama_kota, f.icon_biru, f.icon_hitam, f.versi_icon_biru, f.versi_icon_hitam, ( 6371 * ACOS( COS( RADIANS( '$lat' ) ) * COS( RADIANS( e.lat ) ) * COS( RADIANS( e.lng ) - RADIANS( '$lng' ) ) + SIN( RADIANS( '$lat' ) ) * SIN( RADIANS( e.lat ) ) ) ) as distance 
            FROM m_pemilik_usaha b 
            left join m_kota c ON b.id_kota = c.id 
            left join m_level d ON b.level = d.id 
            left join p_detail_lokasi e ON b.id_pemilik = e.id_pemilik 
            left join m_jenis_kategori_usaha f ON b.id_jenis = f.id_jenis 
            WHERE ( 6371 * ACOS( COS( RADIANS( '$lat' ) ) * COS( RADIANS( e.lat ) ) * COS( RADIANS( e.lng ) - RADIANS( '$lng' ) ) + SIN( RADIANS( '$lat' ) ) * SIN( RADIANS( e.lat ) ) ) ) <= (b.radius /1000 )  AND b.aktif=1 AND b.status_toko=1 AND b.tipe=4
            ORDER BY distance";


            // echo $sql; die();
            

            $query = $this->db->query($sql);

            return $query->result();
    }

    function get_order($id_konsumen){
        $id_konsumen    = $this->db->escape_str($id_konsumen);

        $query = $this->db->query("select a.*,b.nama as nama_pemilik,b.tipe as tipe_pemilik, COALESCE(b.jargon,g.jargon) as jargon,c.ket_konsumen, e.nama_produk, COALESCE(b.foto, g.foto) as foto, f.nama_kategori
            from konsumen_order a 
            left join detail_order d on d.id_order=a.id_order 
            left join p_detail_produk e on e.id_produk=d.id_produk 
            left join m_kategori_produk f on f.id_kategori=e.id_kategori 
            left join m_pemilik_usaha b on a.id_pemilik=b.id_pemilik 
            left join histori_m_pemilik_usaha g on a.id_pemilik=g.id_pemilik 
            left join m_perkiraan_antar c 
            on a.id_perkiraan_antar=c.id where a.id_konsumen = '$id_konsumen'  group by a.id_order order by a.tanggal_order desc" );
        return $query->result();
    }


    function get_undone_order($id_konsumen){
        $id_konsumen    = $this->db->escape_str($id_konsumen);

        $query = $this->db->query("select '1' as tipe_order, a.*,b.nama as nama_pemilik,b.tipe as tipe_pemilik, COALESCE(b.jargon,g.jargon) as jargon,c.ket_konsumen, e.nama_produk, COALESCE(b.foto, g.foto) as foto, f.nama_kategori
            from konsumen_order a 
            left join detail_order d on d.id_order=a.id_order 
            left join p_detail_produk e on e.id_produk=d.id_produk 
            left join m_kategori_produk f on f.id_kategori=e.id_kategori 
            left join m_pemilik_usaha b on a.id_pemilik=b.id_pemilik 
            left join histori_m_pemilik_usaha g on a.id_pemilik=g.id_pemilik 
            left join m_perkiraan_antar c 
            on a.id_perkiraan_antar=c.id where a.id_konsumen = '$id_konsumen' and a.status!=4 and a.status!=3 group by a.id_order order by a.tanggal_order desc" );

        return $query->result();
    }

    
    function get_undone_kiriman($id_konsumen){
        $id_konsumen    = $this->db->escape_str($id_konsumen);

        $query = $this->db->query("select '2' as tipe_order, a.*, a.id_kiriman as id_order, b.nama,b.alamat,b.kontak,b.lat,b.lng,b.versi from konsumen_kiriman a 
            left join m_konsumen b on a.id_konsumen=b.id_konsumen 
            left join detail_kiriman d on d.id_kiriman=a.id_kiriman 
            where a.id_konsumen='$id_konsumen' and a.status!=4 and a.status!=3 and b.nama IS NOT NULL
            group by a.id_kiriman order by a.id_kiriman asc" );

        return $query->result();
    }
            


    function get_done_order($id_konsumen){
        $id_konsumen    = $this->db->escape_str($id_konsumen);

        $query = $this->db->query("select '1' as tipe_order, a.*,b.nama as nama_pemilik,b.tipe as tipe_pemilik, COALESCE(b.jargon,g.jargon) as jargon,c.ket_konsumen, e.nama_produk, COALESCE(b.foto, g.foto) as foto, f.nama_kategori
            from konsumen_order a 
            left join detail_order d on d.id_order=a.id_order 
            left join p_detail_produk e on e.id_produk=d.id_produk 
            left join m_kategori_produk f on f.id_kategori=e.id_kategori 
            left join m_pemilik_usaha b on a.id_pemilik=b.id_pemilik 
            left join histori_m_pemilik_usaha g on a.id_pemilik=g.id_pemilik 
            left join m_perkiraan_antar c 
            on a.id_perkiraan_antar=c.id where a.id_konsumen = '$id_konsumen' and (a.status=4 OR a.status=3) group by a.id_order order by a.tanggal_order desc" );

        return $query->result();
    }

    
    function get_done_kiriman($id_konsumen){
        $id_konsumen    = $this->db->escape_str($id_konsumen);

        $query = $this->db->query("select '2' as tipe_order, a.*, a.id_kiriman as id_order, b.nama,b.alamat,b.kontak,b.lat,b.lng,b.versi from konsumen_kiriman a 
            left join m_konsumen b on a.id_konsumen=b.id_konsumen 
            left join detail_kiriman d on d.id_kiriman=a.id_kiriman 
            where a.id_konsumen='$id_konsumen' and (a.status=4 OR a.status=3) and b.nama IS NOT NULL
            group by a.id_kiriman order by a.id_kiriman asc" );

        return $query->result();
    }
            


    function get_order_antri($id_konsumen){
        $id_konsumen    = $this->db->escape_str($id_konsumen);

        $query = $this->db->query("select a.*,b.nama as nama_pemilik,b.tipe as tipe_pemilik, COALESCE(b.jargon,g.jargon) as jargon,c.ket_konsumen, e.nama_produk, COALESCE(b.foto, g.foto) as foto, f.nama_kategori
            from konsumen_order a 
            left join detail_order d on d.id_order=a.id_order 
            left join p_detail_produk e on e.id_produk=d.id_produk 
            left join m_kategori_produk f on f.id_kategori=e.id_kategori 
            left join m_pemilik_usaha b on a.id_pemilik=b.id_pemilik 
            left join histori_m_pemilik_usaha g on a.id_pemilik=g.id_pemilik 
            left join m_perkiraan_antar c 
            on a.id_perkiraan_antar=c.id where a.id_konsumen = '$id_konsumen' AND (a.status=1 OR a.status=2) AND b.nama IS NOT NULL AND b.tipe=4 group by a.id_order order by a.tanggal_order desc" );
        return $query->result();
    }

    function update_nama($id_konsumen,$nama){

        $id_konsumen    = $this->db->escape_str($id_konsumen);
        $nama    = $this->db->escape_str($nama);
       
        $this->db->query("update m_konsumen set nama = '$nama' where id_konsumen = '$id_konsumen'");
    }

    function update_alamat($id_konsumen,$alamat){

        $id_konsumen    = $this->db->escape_str($id_konsumen);
        $alamat    = $this->db->escape_str($alamat);
       
        $this->db->query("update m_konsumen set alamat = '$alamat' where id_konsumen = '$id_konsumen'");
    }


    function update_no_telp($id_konsumen,$kontak){

        $id_konsumen    = $this->db->escape_str($id_konsumen);
        $kontak    = $this->db->escape_str($kontak);
       
        $this->db->query("update m_konsumen set kontak = '$kontak' where id_konsumen = '$id_konsumen'");
    }

    function update_provinsi_kota($id_konsumen,$id_provinsi,$id_kota){

        $id_konsumen    = $this->db->escape_str($id_konsumen);
        $id_provinsi    = $this->db->escape_str($id_provinsi);
        $id_kota    = $this->db->escape_str($id_kota);
       
        $this->db->query("update m_konsumen set id_provinsi = '$id_provinsi', id_kota = '$id_kota' where id_konsumen = '$id_konsumen'");
    }

    function cancel_order($id_order){

        $id_order    = $this->db->escape_str($id_order);
       
        $this->db->query("update konsumen_order set status=3 where id_order='$id_order'");
    }

    function cancel_kiriman($id_kiriman, $cancel_req=0){

        $id_kiriman    = $this->db->escape_str($id_kiriman);
       
        $this->db->query("update konsumen_kiriman set status=3, cancel_req='$cancel_req' where id_kiriman='$id_kiriman'");
    }

    function set_cancel_req($id_kiriman, $cancel_req){

        $id_kiriman    = $this->db->escape_str($id_kiriman);
        $cancel_req    = $this->db->escape_str($cancel_req);
       
        $this->db->query("update konsumen_kiriman set cancel_req='$cancel_req' where id_kiriman='$id_kiriman'");
    }

    function request_cancel_kiriman($id_kiriman, $ket_perkiraan_antar){

        $id_kiriman    = $this->db->escape_str($id_kiriman);
        $ket_perkiraan_antar    = $this->db->escape_str($ket_perkiraan_antar);
       
        $this->db->query("update konsumen_kiriman set cancel_req=1, ket_perkiraan_antar='$ket_perkiraan_antar' where id_kiriman='$id_kiriman'");
    }
    
    function finish_order($id_order){

        $id_order    = $this->db->escape_str($id_order);
       
        $this->db->query("update konsumen_order set status=4 where id_order='$id_order'");
    }
    

    function update_lokasi($id_konsumen,$lat,$lng){

        $id_konsumen    = $this->db->escape_str($id_konsumen);
        $lat    = $this->db->escape_str($lat);
        $lng    = $this->db->escape_str($lng);
       
        $this->db->query("update m_konsumen set lat = '$lat',lng = '$lng' where id_konsumen = '$id_konsumen'");
    }
    
    function get_kredit($id_pemilik){
        $id_pemilik = $this->db->escape_str($id_pemilik);
        
        $query = $this->db->query("select kredit from m_pemilik_usaha where id_pemilik='$id_pemilik'");
        return $query->first_row()->kredit;
    }
    

    function check_order($json){


        $arr = json_decode($json);
        $kueri_detail = "";
        $id_produks = "";
        $id_produks_umkm = "";

        $produks = array();

        for($i=0;$i<count($arr);$i++){

            $id_produk  = $arr[$i]->id_produk;
            if (intval($arr[$i]->is_umkm) == 0) {
                $id_produks = $id_produks . "'" . $id_produk . "',";
            } else {
                $id_produks_umkm = $id_produks_umkm . "'" . $id_produk . "',";
            }
        }

        if ($id_produks != "") {
            $id_produks = $id_produks."hehehe1234";
            $id_produks = str_replace(",hehehe1234", "", $id_produks);
            $sql = "SELECT *, 0 as is_umkm from p_detail_produk WHERE id_produk IN (" . $id_produks . ") ORDER BY id_produk";
            $query = $this->db->query($sql);
            $produks = $query->result();
        }

        if ($id_produks_umkm != "") {
            $id_produks_umkm = $id_produks_umkm."hehehe1234";
            $id_produks_umkm = str_replace(",hehehe1234", "", $id_produks_umkm);
            $sql = "SELECT *, 1 as is_umkm from p_detail_produk_umkm WHERE id_produk IN (" . $id_produks_umkm . ") ORDER BY id_produk";
            $query = $this->db->query($sql);
            $produks = array_merge($produks, $query->result());
        }

        return $produks;
    }

    function insert_order($id_order,$id_konsumen,$id_pemilik,$total,$json,$fee,$jenis_alamat,$alamat,$ket_alamat,$lat,$lng, $charge, $include_fee, $include_fee_note,$tipe_pemilik){
           // echo "tipe " . $tipe_pemilik; die();
        $id_order   = $this->db->escape_str($id_order);
        $id_konsumen    = $this->db->escape_str($id_konsumen);
        $id_pemilik = $this->db->escape_str($id_pemilik);
        $total      = $this->db->escape_str($total);
        $fee            = $this->db->escape_str($fee);
        $jenis_alamat   = $this->db->escape_str($jenis_alamat);
        $alamat         = $this->db->escape_str($alamat);
        $lat         = $this->db->escape_str($lat);
        $lng         = $this->db->escape_str($lng);
        $charge         = $this->db->escape_str($charge);
        $include_fee         = $this->db->escape_str($include_fee);
        $include_fee_note         = $this->db->escape_str($include_fee_note);

        $stmt = "INSERT INTO `konsumen_order`(`id_order`, `id_konsumen`, `id_pemilik`, `total`, `fee`, `jenis_alamat`, 
            `alamat_antar`, `ket_alamat`, `lat_antar`, `lng_antar`, `id_perkiraan_antar`, `status`, `charge`, `include_fee`, `include_fee_note`) 
            VALUES ('$id_order','$id_konsumen','$id_pemilik','$total','$fee','$jenis_alamat','$alamat', '$ket_alamat', '$lat','$lng','0','1', '$charge', '$include_fee', '$include_fee_note')";

            // echo $stmt; die();
        $this->db->query($stmt);

        $arr = json_decode($json);
        $kueri_detail = "";

        for($i=0;$i<count($arr);$i++){

            $id_produk  = $arr[$i]->id_produk;
            $kuantitas  = $arr[$i]->kuantitas;
            $harga      = $arr[$i]->harga;
            $subtotal   = $arr[$i]->subtotal;
            $is_umkm    = $arr[$i]->is_umkm;


            $this->insert_detail_order($id_order, $id_produk, $kuantitas, $harga, $is_umkm, $subtotal, $tipe_pemilik);
        }

        return $this->db->insert_id();
    }

    function insert_detail_order($id_order,$id_produk,$kuantitas,$harga,$is_umkm,$subtotal,$tipe_pemilik){
        $id_order       = $this->db->escape_str($id_order);
        $id_produk      = $this->db->escape_str($id_produk);
        $kuantitas      = $this->db->escape_str($kuantitas);
        $harga          = $this->db->escape_str($harga);
        $subtotal       = $this->db->escape_str($subtotal);

        // echo intval($tipe_pemilik) . " != " . self::TIPE_PEDAGANG_KELILING; die();

        $this->db->query("insert into detail_order(id_order,id_produk,kuantitas,harga,subtotal) values('$id_order','$id_produk','$kuantitas', '$harga','$subtotal')");
        if (intval($is_umkm) == 0 && intval($tipe_pemilik) != self::TIPE_PEDAGANG_KELILING) {
            $this->db->query("update p_detail_produk set stok = stok-$kuantitas where id_produk = '$id_produk'");
        } else {
            $this->db->query("update p_detail_produk_umkm set stok = stok-$kuantitas where id_produk = '$id_produk'");
        }
    }



    function get_lokasi_aktual($id_pemilik){
        
        $id_pemilik    = $this->db->escape_str($id_pemilik);
       
        $query = $this->db->query("select id_pemilik, lat, lng from p_detail_lokasi where id_pemilik = '$id_pemilik' LIMIT 1");
       
        return $query;
    } 

    function revert_stok($id_produk,$kuantitas, $tipe){
        $id_produk      = $this->db->escape_str($id_produk);
        $kuantitas      = $this->db->escape_str($kuantitas);

        $table_name = "p_detail_produk";
        switch ($tipe) {
            case 'regular':
                $table_name = "p_detail_produk";
                break;
            case 'umkm':
                $table_name = "p_detail_produk_umkm";
                break;
            case 'promo':
                $table_name = "p_detail_produk_promo";
                break;
            case 'histori':
                $table_name = "histori_p_detail_produk";
                break;
        }
        $this->db->query("update " . $table_name . " set stok = stok+$kuantitas where id_produk = '$id_produk'");
    }

    function revert_stok_promo($id_produk,$kuantitas){
        $id_produk      = $this->db->escape_str($id_produk);
        $kuantitas      = $this->db->escape_str($kuantitas);
        $this->db->query("update p_detail_produk_promo set stok = stok+$kuantitas where id_produk = '$id_produk'");
    }

    function get_kategori(){

        $query = $this->db->query("select a.*,b.icon_biru,b.icon_hitam,b.versi_icon_biru,b.versi_icon_hitam 
            from m_kategori_usaha a, m_jenis_kategori_usaha b 
            where b.id_jenis = a.id_jenis and a.status = 1 order by a.nama_kategori");

        return $query->result();
    }

    function get_detail_pemilik($id_pemilik, $id_promos, $tipe){
            $id_pemilik = $this->db->escape_str($id_pemilik);
            $default_value = "null";
            if ($tipe == '2') {
                $default_value = "null";
            } else {
                $default_value = "a.harga_promo";
            }

            $sql = "select a.*, " . $default_value . " as harga_promo, b.nama_kategori as nama_kategori from p_detail_produk a left join m_kategori_produk b on a.id_kategori=b.id_kategori left outer join m_pemilik_usaha c on c.id_partner=a.id_pemilik or c.id_pemilik=a.id_pemilik where a.id_pemilik='$id_pemilik' AND c.nama <> '' AND a.active=1 ";
            // echo $sql; die();

            foreach ($id_promos as $id_promo) {
                $sql = $sql . " UNION select a.*, a.harga_promo as harga_promo, b.nama_kategori as nama_kategori from p_detail_produk a left join m_kategori_produk b on a.id_kategori=b.id_kategori left join produk_promo_diskon ppd on ppd.id_produk=REPLACE(a.id_produk, '".$id_pemilik."_', '') left outer join m_pemilik_usaha c on c.id_partner=a.id_pemilik or c.id_pemilik=a.id_pemilik where a.id_pemilik='$id_pemilik' AND c.nama <> '' AND a.active=1 AND ppd.id_merchant_promo='".$id_promo."' ";
            }

            $sql = $sql . "group by a.id_produk order by nama_kategori";

            // echo $sql;

            $query = $this->db->query($sql);

            return $query->result();
    }


    function get_produk_pemilik($id_pemilik, $id_promos, $tipe, $min_antar, $page){
            $id_pemilik = $this->db->escape_str($id_pemilik);
            $default_value = "null";

            $minAntarParam = "";
            if ($tipe == '2') {
                $default_value = "null";

                if ($min_antar == "1") {
                    $minAntarParam = " AND a.min_antar = 1";
                } else {
                    $minAntarParam = " AND a.min_antar != 1";
                }
            } else {
                $default_value = "a.harga_promo";
            }

            $page = $this->db->escape_str($page);

            if (intval($page >=1 )) { // kalau paging dimulai dari 1, enable pagint. Jika tidak, tampilkan seluruhnya

                $offset = ($page-1) * 50; 
                $pagingParam =  " LIMIT 50 OFFSET " . $offset;
            }

        

            $sql = "SELECT a.*, " . $default_value . " as harga_promo, b.nama_kategori as nama_kategori from p_detail_produk a left join m_kategori_produk b on a.id_kategori=b.id_kategori left outer join m_pemilik_usaha c on c.id_partner=a.id_pemilik or c.id_pemilik=a.id_pemilik where a.id_pemilik='$id_pemilik' AND c.nama <> '' AND a.nama_produk <> '' AND a.active=1 AND a.harga_promo IS NULL " . $minAntarParam;
            // echo $sql; die();

            $sql = $sql . " group by a.id_produk order by a.nama_produk "  . $pagingParam;

            // echo $sql; die();

            $query = $this->db->query($sql);

            return $query->result();
    }


    function get_produk_pemilik_count($id_pemilik, $id_promos, $tipe, $min_antar){
            $id_pemilik = $this->db->escape_str($id_pemilik);
            $default_value = "null";

            $minAntarParam = "";
            if ($tipe == '2') {
                $default_value = "null";
                if ($min_antar == "1") {
                    $minAntarParam = " AND a.min_antar = 1";
                } else {
                    $minAntarParam = " AND a.min_antar != 1";
                }
            } else {
                $default_value = "a.harga_promo";
            }
        

        
            $sql = "SELECT COUNT(DISTINCT a.id_produk) as id_produk_count from p_detail_produk a left join m_kategori_produk b on a.id_kategori=b.id_kategori left outer join m_pemilik_usaha c on c.id_partner=a.id_pemilik or c.id_pemilik=a.id_pemilik where a.id_pemilik='$id_pemilik' AND c.nama <> '' AND a.nama_produk <> '' AND a.active=1 AND a.harga_promo IS NULL ". $minAntarParam;

            $result = $this->db->query($sql)->row_array();
        return $result['id_produk_count'];
    }


    function get_produk_pemilik_diskon($id_pemilik, $id_partner, $tipe, $id_promo){
            $id_pemilik = $this->db->escape_str($id_pemilik);
            $default_value = "null";

            $sql = "";
            if ($tipe == '2') {
                $sql = " select a.*, ppd.min_qty, ppd.harga_promo as harga_promo, 0 as is_official, CONCAT('Harga promo untuk pembelian ', ppd.min_qty, ' buah') as ket_promo from p_detail_produk a left join m_pemilik_usaha c on c.id_partner=a.id_pemilik or c.id_pemilik=a.id_pemilik left join produk_promo_diskon ppd on ppd.id_produk=REPLACE(a.id_produk, '".$id_pemilik."_', '') where a.id_pemilik='$id_pemilik' AND a.nama_produk <> '' AND a.harga > 0 AND a.active=1 AND ppd.id_merchant_promo='" . $id_promo . "' " . $minAntarParam;

                $sql = $sql . " group by a.id_produk order by a.stok=0, a.nama_produk";
            } else {
                $sql = "select * from p_detail_produk where nama_produk <> '' and harga > 0 and active=1 and harga_promo IS NOT NULL and id_pemilik='" . $id_pemilik . "' order by nama_produk";
            }

            // echo $sql; die();


            $query = $this->db->query($sql);
            return $query->result();
    }


    function get_produk_pemilik_diskon_count($id_pemilik, $id_partner, $tipe, $id_promos){
            $id_pemilik = $this->db->escape_str($id_pemilik);
            $default_value = "null";

            $sql = "";
            if ($tipe == '2') {
                $sql = " select COUNT(DISTINCT a.id_produk) as id_produk_count from p_detail_produk a left join m_pemilik_usaha c on c.id_partner=a.id_pemilik or c.id_pemilik=a.id_pemilik left join produk_promo_diskon ppd on ppd.id_produk=REPLACE(a.id_produk, '".$id_pemilik."_', '') where a.id_pemilik='$id_pemilik' AND a.nama_produk <> '' AND a.harga > 0 AND a.active=1  AND ppd.id_merchant_promo IN (" . $id_promos . ") " . $minAntarParam;
                
            } else {
                $sql = "select COUNT(DISTINCT id_produk) as id_produk_count from p_detail_produk where nama_produk <> '' and harga > 0 and active=1 and harga_promo IS NOT NULL and id_pemilik='" . $id_pemilik . "'";
            }

            // echo $sql; die();

            $result = $this->db->query($sql)->row_array();
            return $result['id_produk_count'];
    }


    function get_produk_by_kategori($id_pemilik, $id_kategori, $page){
            $id_pemilik = $this->db->escape_str($id_pemilik);
            $default_value = "null";
            if ($tipe == '2') {
                $default_value = "null";
            } else {
                $default_value = "a.harga_promo";
            }

            $page = $this->db->escape_str($page);

            if (intval($page >=1 )) { // kalau paging dimulai dari 1, enable pagint. Jika tidak, tampilkan seluruhnya
                $offset = ($page-1) * 50; 
                $pagingParam =  " LIMIT 50 OFFSET " . $offset;
            }
        

            $sql = "select a.*, " . $default_value . " as harga_promo, b.nama_kategori as nama_kategori from p_detail_produk a left join m_kategori_produk b on a.id_kategori=b.id_kategori left outer join m_pemilik_usaha c on c.id_partner=a.id_pemilik or c.id_pemilik=a.id_pemilik where a.id_pemilik='$id_pemilik' AND c.nama <> '' AND a.nama_produk <> '' AND a.active=1 AND a.id_kategori=" . $id_kategori;
            // echo $sql; die();

            $sql = $sql . " group by a.id_produk order by a.stok=0, a.nama_produk asc " . $pagingParam ;

            // echo $sql;

            $query = $this->db->query($sql);

            return $query->result();
    }

    function get_produk_promo_diskon($id_pemilik, $id_promo, $page=0) {
            $id_pemilik = $this->db->escape_str($id_pemilik);
            $id_promo = $this->db->escape_str($id_promo);
            $pagingParam = "";
            
            if (intval($page >=1 )) { // kalau paging dimulai dari 1, enable pagint. Jika tidak, tampilkan seluruhnya
            $offset = ($page-1) * 20; 
                $pagingParam =  "LIMIT 10 OFFSET " . $offset;
            }

            $sql = "SELECT a.*, b.nama_kategori from p_detail_produk a left join m_kategori_produk b on a.id_kategori=b.id_kategori left join produk_promo_diskon ppd on ppd.id_produk=REPLACE(a.id_produk, '".$id_pemilik."_', '') left outer join m_pemilik_usaha c on c.id_partner=a.id_pemilik or c.id_pemilik=a.id_pemilik where a.id_pemilik='$id_pemilik' AND c.nama <> '' AND a.active=1 AND ppd.id_merchant_promo='".$id_promo."' group by a.id_produk order by b.nama_kategori " . $pagingParam;
            // echo $sql; die();

            $query = $this->db->query($sql);

            return $query->result();
    }


    function cari_produk($q, $page, $lat, $lng, $limit, $tipe="0") {
            $q = $this->db->escape_str($q);
            $page = $this->db->escape_str($page);
            $offset = ($page-1) * 50; 

            $tipe_param = "";

            if (intval($tipe) == 0) {
                $tipe_param = " AND mpu.tipe != 4";
            } else {
                $tipe_param = " AND mpu.tipe = " . $tipe;
            }

            $pagingParam =  "";
            if (intval($page >=1 )) { // kalau paging dimulai dari 1, enable pagint. Jika tidak, tampilkan seluruhnya
                $pagingParam =  "LIMIT " . $limit . " OFFSET " . $offset;
            } 

            $sql = "SELECT pdp.*, mpu.nama as nama_pemilik, ( 6371 * ACOS( COS( RADIANS( '$lat' ) ) * COS( RADIANS( e.lat ) ) * COS( RADIANS( e.lng ) - RADIANS( '$lng' ) ) + SIN( RADIANS( '$lat' ) ) * SIN( RADIANS( e.lat ) ) ) ) as distance
                from p_detail_produk pdp left join m_pemilik_usaha mpu on mpu.id_pemilik=pdp.id_pemilik 
                left join m_kota c ON mpu.id_kota = c.id 
                left join m_level d ON mpu.level = d.id 
                left join p_detail_lokasi e ON mpu.id_pemilik = e.id_pemilik 
                left join m_jenis_kategori_usaha f ON mpu.id_jenis = f.id_jenis 
                WHERE pdp.harga > 0 " . $tipe_param . " AND ( 6371 * ACOS( COS( RADIANS( '$lat' ) ) * COS( RADIANS( e.lat ) ) * COS( RADIANS( e.lng ) - RADIANS( '$lng' ) ) + SIN( RADIANS( '$lat' ) ) * SIN( RADIANS( e.lat ) ) ) ) <= (mpu.radius /1000 ) AND pdp.id_pemilik <> '' AND mpu.nama IS NOT NULL AND (pdp.nama_produk LIKE '%$q%' OR pdp.keterangan LIKE '%$q%') AND pdp.stok > 0 ORDER BY pdp.harga " . $pagingParam;

            $query = $this->db->query($sql);

            return $query->result();
    }



    function cari_produk_by_pemilik($q, $id_pemilik, $tipe, $page, $id_promos) {
            $q = $this->db->escape_str($q);
            $id_pemilik = $this->db->escape_str($id_pemilik);
            $page = $this->db->escape_str($page);

            $pagingParam = "";
            if (intval($page >=1 )) { // kalau paging dimulai dari 1, enable pagint. Jika tidak, tampilkan seluruhnya
                $offset = ($page-1) * 50; 
                $pagingParam =  "LIMIT 10 OFFSET " . $offset;
            }

            $promo_params = "";
            if (str_replace(' ', '', $id_promos) != "") {
                $promo_params = "AND ppd.id_merchant_promo IN (" . $id_promos . ")";
            }

            $q = str_replace("-", " ", $q);

            $produk_promo_query = "";
            if ($tipe == "2") {
                $produk_promo_query = "select a.id_produk, a.nama_produk, a.id_pemilik, a.id_kategori, a.stok, a.keterangan, a.harga, a.status, a.foto, a.versi, a.min_antar, a.active, CONCAT('Harga promo untuk pembelian ', ppd.min_qty, ' buah') as ket_promo, ppd.harga_promo as harga_promo, 0 as is_umkm, c.nama as nama_pemilik, ppd.min_qty, 0 as is_official from p_detail_produk a left join m_pemilik_usaha c on c.id_partner=a.id_pemilik or c.id_pemilik=a.id_pemilik left join produk_promo_diskon ppd on ppd.id_produk=REPLACE(a.id_produk, '" . $id_pemilik . "_', '') where a.harga > 0 AND (REPLACE(a.nama_produk, '-', ' ') LIKE '%$q%' OR REPLACE(a.keterangan, '-', ' ') LIKE '%$q%' OR REPLACE(a.ket_promo, '-', ' ') LIKE '%$q%') AND  a.id_pemilik='" . $id_pemilik . "' AND a.nama_produk <> '' AND a.active=1  " . $promo_params . " " . $pagingParam . "  UNION ";
            }

            $sql = "SELECT *, COUNT(id_produk) AS produk_count FROM ("
             . $produk_promo_query . "select pdp.id_produk, pdp.nama_produk, pdp.id_pemilik, pdp.id_kategori, pdp.stok, pdp.keterangan, pdp.harga, pdp.status, pdp.foto, pdp.versi, pdp.min_antar, pdp.active, pdp.ket_promo,  pdp.harga_promo as harga_promo,  0 as is_umkm, mpu.nama as nama_pemilik, 1 as min_qty,  0 as is_official from p_detail_produk_umkm pdp left join m_pemilik_usaha mpu on mpu.id_pemilik=pdp.id_pemilik WHERE pdp.harga > 0 AND  (REPLACE(pdp.nama_produk, '-', ' ') LIKE '%$q%' OR REPLACE(pdp.keterangan, '-', ' ') LIKE '%$q%' OR REPLACE(pdp.ket_promo, '-', ' ') LIKE '%$q%') AND pdp.id_pemilik='" . $id_pemilik . "' AND pdp.id_pemilik <> '' AND pdp.nama_produk <> '' AND pdp.active=1 " . $pagingParam . "  UNION select pdp.id_produk, pdp.nama_produk, pdp.id_pemilik, pdp.id_kategori, pdp.stok, pdp.keterangan, pdp.harga, pdp.status, pdp.foto, pdp.versi, pdp.min_antar, pdp.active, pdp.ket_promo,  pdp.harga_promo as harga_promo,  0 as is_umkm, mpu.nama as nama_pemilik, 1 as min_qty,  0 as is_official from p_detail_produk pdp left join m_pemilik_usaha mpu on mpu.id_pemilik=pdp.id_pemilik WHERE pdp.harga > 0 AND  (REPLACE(pdp.nama_produk, '-', ' ') LIKE '%$q%' OR REPLACE(pdp.keterangan, '-', ' ') LIKE '%$q%' OR REPLACE(pdp.ket_promo, '-', ' ') LIKE '%$q%') AND pdp.id_pemilik='" . $id_pemilik . "' AND pdp.id_pemilik <> '' AND pdp.nama_produk <> '' AND pdp.active=1 " . $pagingParam . " ) T1 GROUP BY id_produk ORDER BY stok=0, harga";
            // echo $sql; die();

            $query = $this->db->query($sql);

            return $query->result();
    }


    function cari_produk_promo($q, $page, $lat, $lng, $limit) {
            $q = $this->db->escape_str($q);
            $page = $this->db->escape_str($page);
            $offset = ($page-1) * 50; 

            $pagingParam =  "LIMIT " . $limit;
            if (intval($page >=1 )) { // kalau paging dimulai dari 1, enable pagint. Jika tidak, tampilkan seluruhnya
                $pagingParam =  "LIMIT " . $limit . " OFFSET " . $offset;
            }

            $sql = "SELECT pdp.*, mpu.nama as nama_pemilik, ( 6371 * ACOS( COS( RADIANS( '$lat' ) ) * COS( RADIANS( e.lat ) ) * COS( RADIANS( e.lng ) - RADIANS( '$lng' ) ) + SIN( RADIANS( '$lat' ) ) * SIN( RADIANS( e.lat ) ) ) ) as distance
                from p_detail_produk_promo pdp left join promo_produk pp on pp.id_produk=pdp.id_produk left join promo_code pc on pp.id_promo=pc.promo_id  left join m_pemilik_usaha mpu on mpu.id_pemilik=pc.id_pemilik 
                left join m_kota c ON mpu.id_kota = c.id 
                left join m_level d ON mpu.level = d.id 
                left join p_detail_lokasi e ON mpu.id_pemilik = e.id_pemilik 
                left join m_jenis_kategori_usaha f ON mpu.id_jenis = f.id_jenis 
                WHERE ( 6371 * ACOS( COS( RADIANS( '$lat' ) ) * COS( RADIANS( e.lat ) ) * COS( RADIANS( e.lng ) - RADIANS( '$lng' ) ) + SIN( RADIANS( '$lat' ) ) * SIN( RADIANS( e.lat ) ) ) ) <= (mpu.radius /1000 ) AND pc.id_pemilik <> '' AND mpu.nama IS NOT NULL AND (pdp.nama_produk LIKE '%$q%' OR pdp.keterangan LIKE '%$q%')  ORDER BY pdp.harga " . $pagingParam;

            // $sql = "SELECT pdp.*, mpu.nama as nama_pemilik from p_detail_produk_promo pdp left join promo_produk pp on pp.id_produk=pdp.id_produk left join promo_code pc on pp.id_promo=pc.promo_id left join m_pemilik_usaha mpu on mpu.id_pemilik=pc.id_pemilik WHERE mpu.nama IS NOT NULL AND (pdp.nama_produk LIKE '%$q%' OR pdp.keterangan LIKE '%$q%') ORDER BY pdp.harga " . $pagingParam;

            // echo $sql; die();

            $query = $this->db->query($sql);

            return $query->result();
    }


    function cari_pemilik_produk($q, $page, $lat, $lng, $limit) {
            $q = $this->db->escape_str($q);
            $page = $this->db->escape_str($page);
            $offset = ($page-1) * 50; 

            $pagingParam =  "LIMIT " . $limit;
            if (intval($page >=1 )) { // kalau paging dimulai dari 1, enable pagint. Jika tidak, tampilkan seluruhnya
                $pagingParam =  "LIMIT " . $limit . " OFFSET " . $offset;
            }

            $sql = "SELECT b . * , c.nama AS nama_kota, f.icon_biru, f.icon_hitam, f.versi_icon_biru, f.versi_icon_hitam, ( 6371 * ACOS( COS( RADIANS( '$lat' ) ) * COS( RADIANS( e.lat ) ) * COS( RADIANS( e.lng ) - RADIANS( '$lng' ) ) + SIN( RADIANS( '$lat' ) ) * SIN( RADIANS( e.lat ) ) ) ) as distance 
                FROM m_pemilik_usaha b 
                left join m_kota c ON b.id_kota = c.id 
                left join m_level d ON b.level = d.id 
                left join p_detail_lokasi e ON b.id_pemilik = e.id_pemilik 
                left join m_jenis_kategori_usaha f ON b.id_jenis = f.id_jenis 
                WHERE ( 6371 * ACOS( COS( RADIANS( '$lat' ) ) * COS( RADIANS( e.lat ) ) * COS( RADIANS( e.lng ) - RADIANS( '$lng' ) ) + SIN( RADIANS( '$lat' ) ) * SIN( RADIANS( e.lat ) ) ) ) <= (b.radius /1000 )  AND (b.nama LIKE '%$q%' OR b.jargon LIKE '%$q%') ORDER BY distance " . $pagingParam;

            // echo $sql; die();

            $query = $this->db->query($sql);

            return $query->result();
    }

    
    function tambah_toko_preaktif($id_pemilik,$id_partner,$kontak,$kontak_sms, $kontak_sms2, $password,$username,$kode,$id_provinsi,$id_kota, $id_radius, $min_antar, $fee){

        $id_pemilik = $this->db->escape_str($id_pemilik);
        $id_partner = $this->db->escape_str($id_partner);
        $kontak     = $this->db->escape_str($kontak);
        $kontak_sms     = $this->db->escape_str($kontak_sms);
        $kontak_sms2     = $this->db->escape_str($kontak_sms2);
        $password       = hash("sha512",$password);
        $username       = $this->db->escape_str($username);
        $kode           = $this->db->escape_str($kode);
        $id_radius      = $this->db->escape_str($id_radius);
        $min_antar      = $this->db->escape_str($min_antar);
        $id_provinsi    = $this->db->escape_str($id_provinsi);
        $id_kota        = $this->db->escape_str($id_kota);
        $lat = "-6.2293866";
        $lng = "106.6890876";
        
        $sql_stmt = "INSERT INTO `m_pemilik_usaha`(`id_pemilik`, `id_partner`, `username`, `email`, `kontak`, `kontak_sms`, `kontak_sms2`, `password`, `nama`, `jargon`, `alamat`, 
            `lat`, `lng`, `id_radius`, `foto`, `kode`, `tanggal_update`, `token`, `id_kategori`, `status`,id_provinsi,id_kota,level,id_jenis,tipe,kredit, min_antar, `fee`) 
            select '$id_pemilik', '$id_partner', '$username','','$kontak','$kontak_sms','$kontak_sms2','$password','','','','$lat','$lng','$id_radius','','$kode',now(),'','0', '0', '$id_provinsi','$id_kota',1,1,2,default_kredit, $min_antar, $fee
            from setting_umum where id = '1'";
        $this->db->query($sql_stmt);
        $affected_rows = $this->db->affected_rows();

        $this->db->query("delete from kode_sms where kode = '$kode'");
        
        return $affected_rows;
    }

    function get_fee(){
        
        $query = $this->db->query("select * from m_fee where status=1 order by batas_bawah desc");
        return $query->result();
    }


    function notif_get_pemilik($id_pemilik){
        
        $query = $this->db->query("select mp.* from m_pemilik_usaha mp where mp.id_pemilik = '$id_pemilik'");
        $result = $query->result();
        if (count($result) <= 0) {
            $query = $this->db->query("select mp.* from histori_m_pemilik_usaha mp where mp.id_pemilik = '$id_pemilik'");
            $result = $query->result();
        }
        return $result;
    }

    function notif_get_konsumen($id_konsumen){

        $sql = "select * from m_konsumen where id_konsumen = '$id_konsumen'";
        // echo $sql; die();
        
        $query = $this->db->query($sql);
        return $query->first_row();
    }

    function check_session($id_konsumen, $token){
        $id_konsumen    = $this->db->escape_str($id_konsumen);
        $token    = $this->db->escape_str($token);
        
        $query = $this->db->query("select * from m_konsumen where id_konsumen = '$id_konsumen' AND token='$token'");
        return $query->first_row();
    }

    function notif_get_order($id_order){
        
        $query = $this->db->query("select a.*,b.nama,b.alamat,b.kontak,b.lat,b.lng,b.foto,b.versi, c.nama as nama_kota 
                    from konsumen_order a left join m_konsumen b on a.id_konsumen=b.id_konsumen left join m_kota c on c.id = b.id_kota 
                    where
                    a.id_order = '$id_order'
                    ");
        return $query->result();
    }

    function daftar($id_konsumen,$kontak,$password,$username,$kode,$id_provinsi,$id_kota){

        $id_konsumen    = $this->db->escape_str($id_konsumen);
        $kontak     = $this->db->escape_str($kontak);
        $password       = hash("sha512",$password);
        $username       = $this->db->escape_str($username);
        $kode           = $this->db->escape_str($kode);
        $id_provinsi    = $this->db->escape_str($id_provinsi);
        $id_kota        = $this->db->escape_str($id_kota);
        
        $this->db->query("INSERT INTO `m_konsumen`(`id_konsumen`, `username`, `kontak`, `password`, `nama`, `alamat`,  `lat`, `lng`, 
        `foto`, `kode`, `tanggal_update`,id_provinsi,id_kota) VALUES('$id_konsumen','$username','$kontak','$password','','','0','0','','$kode',now(),
                '$id_provinsi','$id_kota')");
    }

    function isi_profil_awal($id_konsumen,$nama,$alamat, $id_member){
        $id_konsumen       = $this->db->escape_str($id_konsumen);
        $nama      = $this->db->escape_str($nama);
        $alamat      = $this->db->escape_str($alamat);
        $id_member      = $this->db->escape_str($id_member);

        $this->db->query("update m_konsumen set nama = '$nama', alamat = '$alamat', id_member='$id_member' where id_konsumen = '$id_konsumen'");

        // echo $nama." -> ".$alamat;
    }

    function isi_lokasi_awal($id_konsumen,$lat,$lng){

        $id_konsumen    = $this->db->escape_str($id_konsumen);
        $lat    = $this->db->escape_str($lat);
        $lng    = $this->db->escape_str($lng);
        
        $this->db->query("update m_konsumen set lat = '$lat',lng = '$lng' where id_konsumen = '$id_konsumen'");
    }

    function get_id_konsumen_by_no_telp($no_telp){
        
        $no_telp   = $this->db->escape_str($no_telp);

        $query = $this->db->query("select * from m_konsumen where kontak = '$no_telp'");
        return $query->first_row();
    }

    function copy_to_histori($id_konsumen){
        
        $id_konsumen   = $this->db->escape_str($id_konsumen);

        $this->db->query("INSERT INTO `histori_m_konsumen` SELECT * FROM `m_konsumen` WHERE id_konsumen = '$id_konsumen'");
    }

    function hapus_akun($id_konsumen){
        
        $id_konsumen   = $this->db->escape_str($id_konsumen);

        $this->db->query("DELETE FROM `m_konsumen` WHERE id_konsumen = '$id_konsumen'");
    }


    // function get_detail_kiriman($id_kiriman) {
    //     $id_kiriman    = $this->db->escape_str($id_kiriman);
    //     // echo $id_kiriman; die();

    //     $sql = "SELECT a.*, CONCAT('" . URL_IMAGE_KIRIMAN . "', a.foto_resi) as foto_resi from detail_kiriman a left join konsumen_kiriman b on a.id_kiriman=b.id_kiriman where a.id_kiriman='$id_kiriman'
    //     UNION SELECT a.*, CONCAT('" . URL_IMAGE_KIRIMAN . "', a.foto_resi) as foto_resi from detail_kiriman_ekspedisi a left join konsumen_kiriman b on a.id_kiriman=b.id_kiriman where a.id_kiriman='$id_kiriman'";

    //     $query = $this->db->query($sql);

    //     return $query->result();

    // }



    function get_detail_kiriman($id_kiriman, $tipe) {
        $id_kiriman    = $this->db->escape_str($id_kiriman);
        // echo $id_kiriman; die();

        $sql = "SELECT a.*, IF(a.foto_resi = '', a.foto_resi, CONCAT('" . URL_IMAGE_KIRIMAN . "', a.foto_resi)) as foto_resi from detail_kiriman a left join konsumen_kiriman b on a.id_kiriman=b.id_kiriman where a.id_kiriman='$id_kiriman'";

        if (intval($tipe) == 3) {
            $sql = "SELECT a.*, IF(a.foto_resi = '', a.foto_resi, CONCAT('" . URL_IMAGE_KIRIMAN . "', a.foto_resi)) as foto_resi, c.id_ekspedisi, c.nama as nama_layanan, d.nama as nama_ekspedisi, d.courier_slug, CONCAT('" . URL_IMAGE_EKSPEDISI_WEBADMIN . "', d.foto) as foto, e.lat, e.lng from detail_kiriman_ekspedisi a left join konsumen_kiriman b on a.id_kiriman=b.id_kiriman left join m_layanan_ekspedisi c on c.id_layanan_ekspedisi=a.id_layanan_ekspedisi left join m_ekspedisi d on d.id_ekspedisi=c.id_ekspedisi left join m_agen_ekspedisi e on e.id_agen=a.id_agen where a.id_kiriman='$id_kiriman' ORDER BY c.id_ekspedisi";
        }
        // echo $sql; die();

        $query = $this->db->query($sql);

        return $query->result();

    }

    function get_detail_order($id_order){
            $id_order   = $this->db->escape_str($id_order);
            $sql = "SELECT *, count(id_produk) id_produk_count from 

            (select a.*,b.nama_produk, b.foto, b.keterangan as keterangan_produk, d.alamat_antar, d.ket_perkiraan_antar as ket_konsumen, f.ket_konsumen as keterangan, d.lat_antar, d.lng_antar, d.status as order_status, nama_kategori, e.kontak, d.charge, d.include_fee, d.include_fee_note, 'umkm' as tipe_produk 
            from p_detail_produk_umkm b left join detail_order a on b.id_produk = a.id_produk left join m_kategori_produk c on b.id_kategori=c.id_kategori left join konsumen_order d on d.id_order=a.id_order left join m_konsumen e on e.id_konsumen=d.id_konsumen left join m_perkiraan_antar f on f.id=d.id_perkiraan_antar
            where  a.id_order='$id_order'
            UNION select a.*,b.nama_produk, b.foto, b.keterangan as keterangan_produk, d.alamat_antar, d.ket_perkiraan_antar as ket_konsumen, f.ket_konsumen as keterangan, d.lat_antar, d.lng_antar, d.status as order_status, nama_kategori, e.kontak, d.charge, d.include_fee, d.include_fee_note, 'promo' as tipe_produk 
            from p_detail_produk_promo b left join detail_order a on b.id_produk = a.id_produk left join m_kategori_produk c on b.id_kategori=c.id_kategori left join konsumen_order d on d.id_order=a.id_order left join m_konsumen e on e.id_konsumen=d.id_konsumen left join m_perkiraan_antar f on f.id=d.id_perkiraan_antar
            where  a.id_order='$id_order'
            UNION select a.*,b.nama_produk, b.foto, b.keterangan as keterangan_produk, d.alamat_antar, d.ket_perkiraan_antar as ket_konsumen, f.ket_konsumen as keterangan, d.lat_antar, d.lng_antar, d.status as order_status, nama_kategori, e.kontak, d.charge, d.include_fee, d.include_fee_note, 'regular' as tipe_produk
            from p_detail_produk b left join detail_order a on b.id_produk = a.id_produk left join m_kategori_produk c on b.id_kategori=c.id_kategori left join konsumen_order d on d.id_order=a.id_order left join m_konsumen e on e.id_konsumen=d.id_konsumen left join m_perkiraan_antar f on f.id=d.id_perkiraan_antar
            where  a.id_order='$id_order'
            UNION select a.*,b.nama_produk, b.foto, b.keterangan as keterangan_produk, d.alamat_antar, d.ket_perkiraan_antar as ket_konsumen, f.ket_konsumen as keterangan, d.lat_antar, d.lng_antar, d.status as order_status, nama_kategori, e.kontak, d.charge, d.include_fee, d.include_fee_note, 'histori' as tipe_produk 
            from histori_p_detail_produk b left join detail_order a on b.id_produk = a.id_produk left join m_kategori_produk c on b.id_kategori=c.id_kategori left join konsumen_order d on d.id_order=a.id_order left join m_konsumen e on e.id_konsumen=d.id_konsumen left join m_perkiraan_antar f on f.id=d.id_perkiraan_antar
            where  a.id_order='$id_order' ) T1

            GROUP BY id_produk ORDER BY id_produk
            ";
            // echo $sql; die();
            $query = $this->db->query($sql);
            $results = $query->result();
            if (count($results) > 0) {
                if ($results[0]->ket_konsumen == '') {
                    $results[0]->ket_konsumen = $results[0]->keterangan;
                }
                return $results;
            } 
            return $results;
    }

    function get_detail_order_promo($id_order){
            $id_order   = $this->db->escape_str($id_order);
            $sql = "select a.*,b.nama_produk, b.foto, b.keterangan as keterangan_produk, d.alamat_antar, d.ket_perkiraan_antar as ket_konsumen, f.ket_konsumen as keterangan, d.lat_antar, d.lng_antar, d.status as order_status, nama_kategori, e.kontak, d.charge, d.include_fee, d.include_fee_note 
            from p_detail_produk b left join detail_order a on b.id_produk = a.id_produk left join m_kategori_produk c on b.id_kategori=c.id_kategori left join konsumen_order d on d.id_order=a.id_order left join m_konsumen e on e.id_konsumen=d.id_konsumen left join m_perkiraan_antar f on f.id=d.id_perkiraan_antar
            where  a.id_order='$id_order'";
            // echo $sql; die();
            $query = $this->db->query($sql);
            $results = $query->result();
            if (count($results) > 0) {
                if ($results[0]->ket_konsumen == '') {
                    $results[0]->ket_konsumen = $results[0]->keterangan;
                }
                return $results;
            } 
            return $results;
    }


    function get_produk_favorit($id_konsumen, $id_pemilik, $tipe, $id_promos, $page) {
             $page = $this->db->escape_str($page);
             $offset = 0;
             $pagingParam = "";

            if (intval($page >=1 )) { // kalau paging dimulai dari 1, enable pagint. Jika tidak, tampilkan seluruhnya

                $offset = ($page-1) * 50; 
                $pagingParam =  " LIMIT 50 OFFSET " . $offset;
            }


            $promo_params = "";
            if (str_replace(' ', '', $id_promos) != "") {
                $promo_params = "AND ppd.id_merchant_promo IN (" . $id_promos . ")";
            }

            $produk_promo_query = "";
            if ($tipe == "2") {
                $produk_promo_query = "select a.id_produk, a.nama_produk, a.harga, ppd.harga_promo as harga_promo, ppd.min_qty, a.keterangan, a.stok, a.min_antar, a.id_kategori, a.foto, CONCAT('Harga promo untuk pembelian ', ppd.min_qty, ' buah') as ket_promo from p_detail_produk a left join m_produk_favorit b on b.id_produk=a.id_produk left join produk_promo_diskon ppd on ppd.id_produk=REPLACE(a.id_produk, '".$id_pemilik."_', '') where b.id_konsumen='$id_konsumen' AND a.id_pemilik='$id_pemilik' " . $promo_params . " " . $pagingParam ." UNION ";
            }


            $stmt = "SELECT *, COUNT(id_produk) AS id_produk_count FROM (
            select a.id_produk, a.nama_produk, a.harga, a.harga_promo, 1 as min_qty,  a.keterangan, a.stok, a.min_antar, a.id_kategori, a.foto, a.ket_promo from p_detail_produk_umkm a left join m_produk_favorit b on b.id_produk=a.id_produk where b.id_konsumen='$id_konsumen' AND a.id_pemilik='$id_pemilik' " . $pagingParam ."  UNION "
            . $produk_promo_query . 
            " select a.id_produk, a.nama_produk, a.harga, a.harga_promo, 1 as min_qty, a.keterangan, a.stok, a.min_antar, a.id_kategori, a.foto, a.ket_promo from p_detail_produk a left join m_produk_favorit b on b.id_produk=a.id_produk where b.id_konsumen='$id_konsumen' AND a.id_pemilik='$id_pemilik' " . $pagingParam ." 
            UNION select a.id_produk, a.nama_produk, a.harga, a.harga_promo, 1 as min_qty, a.keterangan, a.stok, 1 as min_antar, a.id_kategori, a.foto, a.ket_promo from p_detail_produk_promo a left join m_produk_favorit b on b.id_produk=a.id_produk where b.id_konsumen='$id_konsumen' " . $pagingParam . ")T1
                GROUP BY id_produk
            ";

            // echo $stmt; die();

        return $this->db->query($stmt)->result();
    }


    function get_produk_favorit_count($id_konsumen, $id_pemilik, $tipe, $id_promos) {

            $promo_params = "";
            if (str_replace(' ', '', $id_promos) != "") {
                $promo_params = "AND ppd.id_merchant_promo IN (" . $id_promos . ")";
            }

            $produk_promo_query = "";
            if ($tipe == "2") {
                $produk_promo_query = " select a.id_produk, a.nama_produk, a.harga, ppd.harga_promo as harga_promo, ppd.min_qty, a.keterangan, a.stok, a.min_antar, a.id_kategori, a.foto, CONCAT('Harga promo untuk pembelian ', ppd.min_qty, ' buah') as ket_promo from p_detail_produk a left join m_produk_favorit b on b.id_produk=a.id_produk left join produk_promo_diskon ppd on ppd.id_produk=REPLACE(a.id_produk, '".$id_pemilik."_', '') where b.id_konsumen='$id_konsumen' AND a.id_pemilik='$id_pemilik' " . $promo_params . " UNION ";
            }

            $stmt = "SELECT COUNT(DISTINCT id_produk) AS id_produk_count FROM (
            select a.id_produk, a.nama_produk, a.harga, a.harga_promo, 1 as min_qty,  a.keterangan, a.stok, a.min_antar, a.id_kategori, a.foto, a.ket_promo from p_detail_produk_umkm a left join m_produk_favorit b on b.id_produk=a.id_produk where b.id_konsumen='$id_konsumen' AND a.id_pemilik='$id_pemilik'
            UNION " . $produk_promo_query .  " select a.id_produk, a.nama_produk, a.harga, a.harga_promo, 1 as min_qty, a.keterangan, a.stok, a.min_antar, a.id_kategori, a.foto, a.ket_promo from p_detail_produk a left join m_produk_favorit b on b.id_produk=a.id_produk where b.id_konsumen='$id_konsumen' AND a.id_pemilik='$id_pemilik'
            UNION select a.id_produk, a.nama_produk, a.harga, a.harga_promo, 1 as min_qty, a.keterangan, a.stok, 1 as min_antar, a.id_kategori, a.foto, a.ket_promo from p_detail_produk_promo a left join m_produk_favorit b on b.id_produk=a.id_produk where b.id_konsumen='$id_konsumen' )T1
                ";

            // echo $stmt; die();
            $result = $this->db->query($stmt)->row_array();
        return $result['id_produk_count'];
    }


    function tambah_produk_favorit($id_konsumen, $id_produk) {


        $this->db->query("insert into m_produk_favorit (id_konsumen, id_produk) VALUE ('$id_konsumen', '$id_produk')");
    }


    function hapus_produk_favorit($id_konsumen, $id_produk) {


        $this->db->query("delete from m_produk_favorit WHERE id_konsumen='$id_konsumen' AND id_produk='$id_produk'");
    }

    function set_foto($id_konsumen,$foto){
        
        $id_konsumen   = $this->db->escape_str($id_konsumen);
        $foto   = $this->db->escape_str($foto);

        $this->db->query("update m_konsumen set foto = '$foto', versi = versi+1 where id_konsumen = '$id_konsumen'");
    }

    function set_foto_avatar($id_konsumen,$foto){
        
        $id_konsumen   = $this->db->escape_str($id_konsumen);
        $foto   = $this->db->escape_str($foto);

        $this->db->query("update m_konsumen set avatar = '$foto', versi_avatar = versi_avatar+1 where id_konsumen = '$id_konsumen'");
    }

    function get_foto($id_konsumen){
        
        $id_konsumen   = $this->db->escape_str($id_konsumen);

        $query = $this->db->query("select foto,versi from m_konsumen where id_konsumen = '$id_konsumen'");

        return $query->first_row();
    }

    function get_foto_avatar($id_konsumen){
        
        $id_konsumen   = $this->db->escape_str($id_konsumen);

        $query = $this->db->query("select avatar,versi_avatar from m_konsumen where id_konsumen = '$id_konsumen'");

        return $query->first_row();
    }

    function update_token($id_konsumen,$token){
        $id_konsumen   = $this->db->escape_str($id_konsumen);
        $token   = $this->db->escape_str($token);
        
        $query = $this->db->query("update m_konsumen set token='$token' where id_konsumen='$id_konsumen'");
    }

    function get_order_dari_notif($id_order){
        
        $id_order   = $this->db->escape_str($id_order);

        $query = $this->db->query("select a.*, a.ket_perkiraan_antar as ket_konsumen, b.nama as nama_pemilik,b.jargon, c.nama as nama_konsumen, c.kontak, c.id_konsumen,d.ket_konsumen as keterangan 
            from konsumen_order a,m_pemilik_usaha b, m_konsumen c,m_perkiraan_antar d 
            where a.id_pemilik = b.id_pemilik and c.id_konsumen = a.id_konsumen and (a.id_perkiraan_antar=d.id or a.ket_perkiraan_antar <> '')
            and a.id_order = '$id_order' limit 1");

        $results = $query->result();
        if (count($results) > 0) {
            if ($results[0]->ket_konsumen == '') {
                $results[0]->ket_konsumen = $results[0]->keterangan;
            }
            return $results;
        } else {
            return array();
        }
    }

    function cek_status_toko($id_pemilik){
        $id_pemilik   = $this->db->escape_str($id_pemilik);

        $query = $this->db->query("select status_toko from m_pemilik_usaha where id_pemilik = '$id_pemilik' ");

        return $query->first_row();
    }










}