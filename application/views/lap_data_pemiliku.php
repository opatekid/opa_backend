<script>
var type = "3";
    var list = [];
    var carii ="";
    function load_cari(cari){
        var html ="";
        for(var i =0;i<cari.length;i++){
            var aksi_hapus = '<a onclick="hapus_modal('+i+')"><i rel="tooltip" title="Hapus" style="cursor: pointer" class="icon icon-trash"></i></a>';
            var a ='<tr>\n\
            <td>'+(i+1)+'</td>\n\
            <td>'+cari[i].tanggal_insert+'</td>\n\
            <td>'+cari[i].nama+'</td>\n\
            <td>'+cari[i].id_pemilik+'</td>\n\
            <td>'+cari[i].kredit+'</td>\n\
            <td>'+cari[i].alamat+'</td>\n\
            <td>'+aksi_hapus+'</td>\n\
        </tr>';

        html = html + a;
    }
    $('#isi_tabel').html(html);
    $("#id_tabel").dataTable();
}
</script>
<div id="modal_data_hapus" class="modal hide fade" role="dialog">
    <div class="modal-dialog" >

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h3>Konfirmasi</h3>
            </div>
            <div class="modal-body">
                <center>
                    <input type="hidden" id="modal_data_hapus_id">
                    <p style="font-size: 15px;">Yakin Menghapus ?</p>
                </center>
            </div>

        </div>
        <div class="modal-footer">
            <span id="modal_data_hapus_notif"></span>
            <span><img id="modal_data_hapus_loading" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>

            <button class="btn" data-dismiss="modal" aria-hidden="true">Batal</button>
            <button onclick="hapus()" class="btn btn-danger"><i class="icon-trash"></i> Hapus</button>
        </div>
    </div>
</div>


<div id="modal_add" class="modal hide fade" role="dialog">
    <div class="modal-dialog" >

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h3>Tambah Gerai</h3>
            </div>
            <div class="modal-body">
                <div class="control-group">
                    <form id="add_form_data" method="post"  enctype="multipart/form-data" accept-charset="utf-8">
                        <div class="control-group">
                            <h4 style="margin-bottom: 20px; margin-top: 20px">DATA TOKO</h4>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Nama Usaha<span class="apt_bintang">*</span></label>
                            <div class="controls">
                                <input type="text" id="nama" placeholder="Nama Toko" name="nama" class="input-xlarge" required />
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Email</label>
                            <div class="controls">
                                <input type="text" id="email" required name="email" placeholder="Alamat email PIC toko" required class="input-xlarge">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Tipe<span class="apt_bintang">*</span></label>
                                 <div class="controls">
                                    <div class="input-xlarge">
                                         <select id="tipe" name="tipe" class='chosen-select input-xlarge' required>
                                                        <option value="1">Toko Tradisional</option>
                                                        <option value="2">Minimarket Hydro</option>
                                                        <option value="3">Pedagang Kaki Lima</option>
                                                        <option value="4">Pedagang Keliling</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                    </form>
                </div>
            </div>

        </div>
        <div class="modal-footer">
            <span id="modal_data_notif"></span>
            <span><img id="modal_data_loading" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>

            <button class="btn" data-dismiss="modal" aria-hidden="true">Batal</button>
            <button onclick="tambah_toko()" class="btn btn-danger"><i class="icon-upload"></i> Tambah Gerai</button>
        </div>
    </div>
</div>



<div class="page-header">
    <div class="pull-left">
        <h1>Pemilik Usaha</h1>
    </div>
    <div class="pull-right">
        <ul class="stats">
            <li class='lightred'>
                <i class="icon-calendar"></i>
                <div class="details">
                    <span class="big">-, -</span>
                    <span>-, -</span>
                </div>
            </li>
        </ul>
    </div>
</div>

<div class="breadcrumbs">
    <ul>
        <li>
            <a>Laporan</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a>Data User</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a>Pemilik Usaha</a>
        </li>
    </ul>
    <div class="close-bread">
        <a href="#"><i class="icon-remove" style="display: none;" ></i></a>
    </div>
</div>


<div class="row-fluid">
    <div class="span12">
    </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <button type="submit" onclick="showAddModal()" class="btn btn-primary"><i class="icon-plus"></i> Tambah</button>
    </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <div class="box box-color box-bordered">
            <div class="box-title">
                <h3><i class="icon-table"></i>Tabel Data Pencarian</h3>
            </div>
            <div class="box-content nopadding"  style="overflow: scroll">
                <div class="row-fluid">
                    <div class="span4" style="padding-left: 10px;">
                        <div class="box box-color box-bordered orange">
                            <div class="box-title">
                                <h3><i class="icon-table"></i>Cari Per Tanggal</h3>
                            </div>
                            <div class="box-content nopadding">
                                <form id="form_cari1">
                                    <div class="row-fluid" style="padding: 10px;">
                                        <div class="control-group">
                                            <label class="control-label">Tanggal<span class="apt_bintang">*</span></label>
                                            <div class="controls">
                                                <div class="input-xlarge">
                                                    <input type="text" name="id_tanggal" id="id_tanggal" class="input-xlarge datepicker" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <div class="controls">
                                                <div class="input-xlarge">
                                                    <button type="submit" class="btn btn-primary pull-right"><i class="icon-search"></i> Cari</button>
                                                    <span class="pull-right" style="margin-right: 10px;"><img id="form_loading1" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;"/></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="box box-color box-bordered green">
                            <div class="box-title">
                                <h3><i class="icon-table"></i>Cari Per Bulan</h3>
                            </div>
                            <div class="box-content nopadding">
                                <form id="form_cari2">
                                    <div class="row-fluid" style="padding: 10px;">
                                        <div class="control-group">
                                            <label class="control-label">Bulan<span class="apt_bintang">*</span></label>
                                            <div class="controls">
                                                <div class="input-xlarge">
                                                    <select id="id_bulan" name="id_bulan" class='chosen-select input-xlarge' required>
                                                        <option value="">-Pilih Bulan-</option>
                                                        <option value="01">Januari</option>
                                                        <option value="02">Februari</option>
                                                        <option value="03">Maret</option>
                                                        <option value="04">April</option>
                                                        <option value="05">Mei</option>
                                                        <option value="06">Juni</option>
                                                        <option value="07">Juli</option>
                                                        <option value="08">Agustus</option>
                                                        <option value="09">September</option>
                                                        <option value="10">Oktober</option>
                                                        <option value="11">November</option>
                                                        <option value="12">Desember</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Tahun<span class="apt_bintang">*</span></label>
                                            <div class="controls">
                                                <div class="input-xlarge">
                                                    <select id="id_tahun" name="id_tahun" class='chosen-select input-xlarge' required>
                                                        <option value="">-Pilih Tahun-</option>
                                                        <?php

                                                        for($i=2016; $i<=date('Y'); $i++)
                                                        {
                                                          ?>
                                                          <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                                          <?php
                                                      }
                                                      ?>
                                                  </select>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="control-group">
                                        <div class="controls">
                                            <div class="input-xlarge">
                                                <button type="submit" class="btn btn-primary pull-right"><i class="icon-search"></i> Cari</button>
                                                <span class="pull-right" style="margin-right: 10px;"><img id="form_loading2" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;"/></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="span4" style="padding-right: 10px;">
                    <div class="box box-color box-bordered red">
                        <div class="box-title">
                            <h3><i class="icon-table"></i>Cari Per Tahun</h3>
                        </div>
                        <div class="box-content nopadding">
                            <form id="form_cari3">
                                <div class="row-fluid" style="padding: 10px;">
                                    <div class="control-group">
                                        <label class="control-label">Tahun<span class="apt_bintang">*</span></label>
                                        <div class="controls">
                                            <div class="input-xlarge">
                                                <select id="id_tahun1" name="id_tahun1" class='chosen-select input-xlarge' required>
                                                    <?php

                                                    for($i=2016; $i<=date('Y'); $i++)
                                                    {
                                                      ?>
                                                      <option value="<?php echo $i;?>" <?php if ($i<=date('Y')) echo "selected='selected'"; ?> ><?php echo $i;?></option>
                                                      <?php
                                                    } 
                                                  ?>
                                              </select>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="control-group">
                                    <div class="controls">
                                        <div class="input-xlarge">
                                            <button type="submit" class="btn btn-primary pull-right"><i class="icon-search"></i> Cari</button>
                                            <span class="pull-right" style="margin-right: 10px;"><img id="form_loading3" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;"/></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <h4 style="padding-left: 10px; display: none;" id="notif_title_cari"></h4>
        <div class="row-fluid" style="padding-top: 10px;">
            <table class="table table-hover table-nomargin table-bordered" id="id_tabel" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th>Tanggal</th>
                        <th>Nama</th>
                        <th>ID</th>
                        <th>Kontak</th>
                        <th>Alamat</th>
                        <th>Tipe</th>
                        <th>Produk</th>
                        <th>Bisa Delivery</th>
                        <th>Status</th>
                        <th>Opsi</th>
                    </tr>
                </thead>
                <tbody id="isi_tabel">

                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
</div>
<script>


    $("#modal_add").modal("hide");
    function showAddModal() {
        $("#modal_add").modal("show");
    }

    $(document).ready(function(){
        $(".chosen-select").chosen({
            no_results_text: "Data tidak ada!",
            width: "100%",
            placeholder_text_single : "Pilih Salah Satu"
        });
        $('.datepicker').datepicker({
           format: 'dd-mm-yyyy'
       });
    });
    
    $("#form_cari1").submit(function (e){
        e.preventDefault();
        $("#notif_title_cari").hide();
        $("#form_loading1").show();
        $("#form_loading1").fadeOut(2000, function(){
            $("#notif_title_cari").text("Hasil Pencarian per Tanggal: "+date_indo_convert($("#id_tanggal").val()));
            $("#notif_title_cari").fadeIn(1000, function (){
                datatable_server_side("1");
                type= "1";
            });
        });
//        $.ajax({
//            url: "php echo URL_OPA.'lap_data_pemiliku/caritgl'; ?>",
//            data: { "tanggal":$("#id_tanggal").val()
//                    },
//            type: 'POST',
//            dataType: 'json',
//            success: function (data, textStatus, jqXHR) {
//                $("#form_loading1").fadeOut(2000, function(){
//                    $("#notif_title_cari").text("Hasil Pencarian per Tanggal: "+date_indo_convert($("#id_tanggal").val()));
//                    $("#notif_title_cari").fadeIn(1000, function (){
//                        $('#isi_tabel').fadeIn(1000, function (){
//                            carii =data.cari;
//                            load_cari(carii); 
//                        });
//                    });
//                });
//            },
//            error: function (jqXHR, textStatus, errorThrown) {
//                console.log("NO");
//                        
//            }
//        });
});
    
    $("#form_cari2").submit(function (e){
        e.preventDefault();
        $("#notif_title_cari").hide();
        $("#form_loading2").show();
        $("#form_loading2").fadeOut(2000, function(){
            var bulan = "";
            if($("#id_bulan").val()=="01"){
                bulan = "Januari";
            }else if($("#id_bulan").val()=="02"){
                bulan = "Februari";
            }else if($("#id_bulan").val()=="03"){
                bulan = "Maret";
            }else if($("#id_bulan").val()=="04"){
                bulan = "April";
            }else if($("#id_bulan").val()=="05"){
                bulan = "Mei";
            }else if($("#id_bulan").val()=="06"){
                bulan = "Juni";
            }else if($("#id_bulan").val()=="07"){
                bulan = "Juli";
            }else if($("#id_bulan").val()=="08"){
                bulan = "Agustus";
            }else if($("#id_bulan").val()=="09"){
                bulan = "September";
            }else if($("#id_bulan").val()=="10"){
                bulan = "Oktober";
            }else if($("#id_bulan").val()=="11"){
                bulan = "November";
            }else if($("#id_bulan").val()=="12"){
                bulan = "Desember";
            }
            $("#notif_title_cari").text("Hasil Pencarian per Bulan: "+bulan+", Tahun: "+$("#id_tahun").val());
            $("#notif_title_cari").fadeIn(1000, function (){
                datatable_server_side("2");
                type = "2";

            });
        });
//        $.ajax({
//            url: " echo URL_OPA.'lap_data_pemiliku/caribln'; ?>",
//            data: { "tahun":$("#id_tahun").val(),
//                    "bulan":$("#id_bulan").val()
//                    },
//            type: 'POST',
//            dataType: 'json',
//            success: function (data, textStatus, jqXHR) {
//                $("#form_loading2").fadeOut(2000, function(){
//                    var bulan = "";
//                    if($("#id_bulan").val()=="01"){
//                        bulan = "Januari";
//                    }else if($("#id_bulan").val()=="02"){
//                        bulan = "Februari";
//                    }else if($("#id_bulan").val()=="03"){
//                        bulan = "Maret";
//                    }else if($("#id_bulan").val()=="04"){
//                        bulan = "April";
//                    }else if($("#id_bulan").val()=="05"){
//                        bulan = "Mei";
//                    }else if($("#id_bulan").val()=="06"){
//                        bulan = "Juni";
//                    }else if($("#id_bulan").val()=="07"){
//                        bulan = "Juli";
//                    }else if($("#id_bulan").val()=="08"){
//                        bulan = "Agustus";
//                    }else if($("#id_bulan").val()=="09"){
//                        bulan = "September";
//                    }else if($("#id_bulan").val()=="10"){
//                        bulan = "Oktober";
//                    }else if($("#id_bulan").val()=="11"){
//                        bulan = "November";
//                    }else if($("#id_bulan").val()=="12"){
//                        bulan = "Desember";
//                    }
//                    $("#notif_title_cari").text("Hasil Pencarian per Bulan: "+bulan+", Tahun: "+$("#id_tahun").val());
//                    $("#notif_title_cari").fadeIn(1000, function (){
//                        $('#isi_tabel').fadeIn(1000, function (){
//                            carii =data.cari;
//                            load_cari(carii);
//                        });
//                    });
//                });
//            },
//            error: function (jqXHR, textStatus, errorThrown) {
//                console.log("NO");
//                        
//            }
//        });
});


    function tambah_toko(){
        $("#modal_data_loading").show();
        var dataPost = new FormData($('#add_form_data')[0]);
        $.ajax({
            url:"<?php echo URL_OPA;?>mobile_v2/api_pemilik/tambah_toko/0",
            data: dataPost,
            type: 'POST',
            dataType: 'json',
            async : true,
            cache : false,
            contentType : false,
            processData : false,
            success: function (data, textStatus, jqXHR) {
                $("#modal_data_loading").fadeOut(10,function(){
                    datatable_server_side(type);
                    $("#modal_add").modal("hide");
                });
                
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("gagal");

            }
        });
    }


    function hapus_modal(i){
        console.log(list)
        $("#modal_data_hapus_id").val(i);
        $("#modal_data_hapus").modal("show");
    }

    function hapus(){
        $("#modal_data_hapus_loading").show();
        $.ajax({
            url: "<?php echo URL_OPA.'lap_data_pemiliku/delete'; ?>",
            data: {"id":$("#modal_data_hapus_id").val()},
            type: 'POST',
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                if (data.code == 1) {
                    $("#modal_data_hapus_loading").fadeOut(1000,function(){
                        $("#modal_data_hapus_notif").text(data.status);
                        $("#modal_data_hapus_notif").show();
                        datatable_server_side(type);
                        $("#modal_data_hapus_notif").fadeOut(2000, function (){
                            $("#modal_data_hapus").modal("hide");
                            
                        });

                    });
                }
                
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("gagal");

            }
        });
    }


    function set_aktif(id_pemilik, aktif){
        $("#modal_data_hapus_loading").show();
        $.ajax({
            url: "<?php echo URL_OPA.'mobile_v2/api_pemilik/set_aktif'; ?>",
            data: {"id_pemilik":id_pemilik,"aktif":aktif},
            type: 'POST',
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                if (data.code == 1) {
                    $("#modal_data_hapus_loading").fadeOut(1000,function(){
                        $("#modal_data_hapus_notif").text(data.status);
                        $("#modal_data_hapus_notif").show();
                        datatable_server_side(type);
                        $("#modal_data_hapus_notif").fadeOut(2000, function (){
                            $("#modal_data_hapus").modal("hide");
                            
                        });

                    });
                }
                
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("gagal");

            }
        });
    }

    function allow_delivery(id_pemilik, allow_delivery){
        $("#modal_data_hapus_loading").show();
        $.ajax({
            url: "<?php echo URL_OPA.'mobile_v2/api_pemilik/allow_delivery'; ?>",
            data: {"id_pemilik":id_pemilik,"allow_delivery":allow_delivery},
            type: 'POST',
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                if (data.code == 1) {
                    $("#modal_data_hapus_loading").fadeOut(1000,function(){
                        $("#modal_data_hapus_notif").text(data.status);
                        $("#modal_data_hapus_notif").show();
                        datatable_server_side(type);
                        $("#modal_data_hapus_notif").fadeOut(2000, function (){
                            $("#modal_data_hapus").modal("hide");
                            
                        });

                    });
                }
                
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("gagal");

            }
        });
    }
    
    $("#form_cari3").submit(function (e){
        e.preventDefault();
        $("#notif_title_cari").hide();
        $("#form_loading3").show();
        $("#form_loading3").fadeOut(2000, function(){
            $("#notif_title_cari").text("Hasil Pencarian per Tahun: "+$("#id_tahun1").val());
            $("#notif_title_cari").fadeIn(1000, function (){
                datatable_server_side("3");
                type = "3";
            });
        });
//        $.ajax({
//            url: "echo URL_OPA.'lap_data_pemiliku/carithn'; ",
//            data: { "tahun":$("#id_tahun1").val()
//                    },
//            type: 'POST',
//            dataType: 'json',
//            success: function (data, textStatus, jqXHR) {
//                $("#form_loading3").fadeOut(2000, function(){
//                    $("#notif_title_cari").text("Hasil Pencarian per Tahun: "+$("#id_tahun1").val());
//                    $("#notif_title_cari").fadeIn(1000, function (){
//                        $('#isi_tabel').fadeIn(1000, function (){
//                            carii =data.cari;
//                            load_cari(carii);
//                        });
//                    });
//                });
//            },
//            error: function (jqXHR, textStatus, errorThrown) {
//                console.log("NO");
//                        
//            }
//        });
});
    
    ///////////////////////
    
    var dataTable;
    
    function datatable_server_side(tipe){
        console.log("server");
        if(dataTable != null)dataTable.fnDestroy();
        
        dataTable = $('#id_tabel').dataTable( {
            'iDisplayLength': 10,
            "aoColumnDefs": [
            { 'bSortable': false, 'aTargets': [ 0,1,2,3,4] }
            ],
            "processing": true,
            "serverSide": true,
            "ajax":{
                url :"<?php  echo URL_OPA; ?>lap_data_pemiliku/get_data", // json datasource
                type: "post",  // method  , by default get
                "data": function ( d ) {
                    if(tipe == '1'){
                        d.tanggal = $("#id_tanggal").val();
                        d.tipe = tipe;
                    }else if(tipe == '2'){
                        d.tahun = $("#id_tahun").val();
                        d.bulan = $("#id_bulan").val();
                        d.tipe = tipe;
                    }else if(tipe == '3'){
                        d.tahun = $("#id_tahun1").val();
                        d.tipe = tipe;
                    }
                },
                error: function(){  // error handling
                    $(".id_tabel-error").html("");
                    $("#id_tabel").append('<tbody class="id_tabel-error"><tr><th colspan="5">No data found in the server</th></tr></tbody>');
                    $("#id_tabel_processing").css("display","none");

                },
                "dataSrc": function ( json ) {
                    json_upgrade = json.json;
                    
                    return json.data;
                }
            }
        });
    }
</script>