<script>
    var json_isi = <?php echo json_encode($tran_isi)?>;
    var status_edit = "0";
    
    function load_opsi(json){
        for(var i=0;i<json.length;i++){
            if(json[i].id_role == "20"){
                status_edit = "1";
            }
        }   
    }
    
    function load_isi(tran_isi){
        $("#id_tabel").dataTable().fnDestroy();
        var html ="";
        var tipe ="";
        for(var i =0;i<tran_isi.length;i++){
            if(tran_isi[i].tipe == "1"){
                tipe ="Free";
            }else if(tran_isi[i].tipe=="2"){
                tipe="Premium";
            }
            var aksi_edit = "";
            
            if(status_edit == "1"){
                aksi_edit = '<a onclick="edit_modal('+i+')"><i rel="tooltip" title="Ubah" style="cursor: pointer" class="icon icon-pencil"></i></a>';
            }
            var a ='<tr>\n\
                        <td>'+(i+1)+'</td>\n\
                        <td>'+tran_isi[i].nama+'</td>\n\
                        <td>'+tipe+'</td>\n\
                        <td>'+aptikmacurrency(parseInt(tran_isi[i].kredit),"")+'</td>\n\
                        <td>'+aksi_edit+'</td>\n\
                    </tr>';
            
            html = html + a;
        }
        $('#isi_tabel').html(html);
        $("#id_tabel").dataTable();
    }
    load_opsi(json_role);
</script>

<div id="modal_data" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-header">
        <a type="button" class="close" data-dismiss="modal" aria-hidden="true">x</a>
        <h3 id="">Data Kredit</h3>
    </div>
    <form id="form_data" class="form-horizontal" enctype="multipart/form-data">
        <input type="hidden" id="form_id" name="form_id">
        <div class="modal-body">
            <div class="control-group">
                <label class="control-label">Nama<span class="apt_bintang">*</span></label>
                <div class="controls">
                    <input type="text" disabled id="id_nama" name="id_nama" required class="input-xlarge">
                    
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Tipe<span class="apt_bintang">*</span></label>
                <div class="controls">
                    <div class="input-xlarge">
                        <input type="text" disabled id="id_tipe" name="id_tipe" required class="input-xlarge">
                    </div>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Nominal<span class="apt_bintang">*</span></label>
                <div class="controls">
                    <input type="text" id="id_nominal" name="id_nominal" required class="input-xlarge" onkeypress="return isNumberKey(event)" onKeyup="uang('id_nominal','');">
                    <span class="help-block"><small> Masukkan nominal kredit yang akan ditambahkan </small></span>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <span id="form_notif"></span>
            <span><img id="form_loading" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>

            <a class="btn" data-dismiss="modal" aria-hidden="true">Batal</a>
            <button type="submit" class="btn btn-primary"><i class="icon-save"></i> Simpan</button>
        </div>
    </form>
</div>

<div class="page-header">
    <div class="pull-left">
            <h1>Isi Kredit</h1>
    </div>
    <div class="pull-right">
        <ul class="stats">
            <li class='lightred'>
                <i class="icon-calendar"></i>
                <div class="details">
                    <span class="big">-, -</span>
                    <span>-, -</span>
                </div>
            </li>
        </ul>
    </div>
</div>
<div class="breadcrumbs">
        <ul>
            <li>
                <a>Transaksi</a>
                <i class="icon-angle-right"></i>
            </li>
            <li>
                <a>Isi Kredit</a>
            </li>
        </ul>
        <div class="close-bread">
                <a href="#"><i class="icon-remove" style="display: none;" ></i></a>
        </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <div class="box box-color box-bordered">
            <div class="box-title">
                <h3><i class="icon-table"></i>Tabel Isi Kredit</h3>
            </div>
            <div class="box-content nopadding"  style="overflow: scroll">                
                <table class="table table-hover table-nomargin table-bordered" id="id_tabel">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Pemilik</th>
                            <th>Tipe</th>
                            <th>Nominal</th>
                            <th>Opsi</th>
                        </tr>
                    </thead>
                    <tbody id="isi_tabel">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
//    load_isi(json_isi);
    $(document).ready(function(){
       
        $(".chosen-select").chosen({
            no_results_text: "Data tidak ada!",
            width: "100%",
            placeholder_text_single : "Pilih Salah Satu"
        });
    });
    
    function edit_modal(i){
        var tipe = "";
        if(json_isi[i].tipe=="1"){
            tipe = "Free";
        }else if(json_isi[i].tipe=="2"){
            tipe = "Premium";
        }
        $("#form_id").val(json_isi[i].id_pemilik);
        $("#id_nama").val(json_isi[i].nama);
        $("#id_tipe").val(tipe);
        $("#id_nominal").val("");
        $("#modal_data").modal("show");
    }
    
    $("#form_data").submit(function (e){
        e.preventDefault();
        $("#form_loading").show();
        $.ajax({
            url: "<?php echo URL_OPA.'tran_isi_kredit/isi_kredit'; ?>",
            data: { "nama":$("#id_nama").val(),
                    "tipe":$("#id_tipe").val(),
                    "nominal":aptikmastring($("#id_nominal").val(),''),
                    "id":$("#form_id").val()
                    },
            type: 'POST',
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                console.log("OK");
                $("#form_loading").fadeOut(1000, function (){
                    $("#form_notif").text(data.status);
                    $("#form_notif").show();
                    $("#form_notif").fadeOut(2000, function (){
                        datatable_server_side();
                        $("#modal_data").modal("hide");
                    });
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("NO");
            }
        });
    });
    
    ///////////////////////
    
    var dataTable;
    
    datatable_server_side();
    
    function datatable_server_side(){
        console.log("server");
        if(dataTable != null)dataTable.fnDestroy();
        
        dataTable = $('#id_tabel').dataTable( {
            'iDisplayLength': 10,
            "aoColumnDefs": [
                { 'bSortable': false, 'aTargets': [ 0,1,2,3,4 ] }
            ],
            "processing": true,
            "serverSide": true,
            "ajax":{
                url :"<?php  echo URL_OPA; ?>tran_isi_kredit/get_data", // json datasource
                type: "post",  // method  , by default get
                "data": function ( d ) {
                    
                },
                error: function(){  // error handling
                    $(".id_tabel-error").html("");
                    $("#id_tabel").append('<tbody class="id_tabel-error"><tr><th colspan="5">No data found in the server</th></tr></tbody>');
                    $("#id_tabel_processing").css("display","none");

                },
                "dataSrc": function ( json ) {
                    json_isi = json.json;
                    return json.data;
                }
            }
            
        });
		
    }
</script>