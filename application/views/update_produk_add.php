<script></script>
<style type="text/css">
    #map {
        height: 1200px;
        width: 100%;
        position: relative;
    }
   #wrapper { position: relative; }
   #over_map { 
        background-color: white; position: 
        absolute; 
        top: 50px; 
        left: 10px; 
        z-index: 99; 
        padding: 10px; 
        width: 450px;
    }
    .option_tab {
        display:inline-block;
        margin-right: 10px;
        *display:inline;/* For IE7 */
        *zoom:1;/* For IE7 */
        white-space:normal;
        font-size:13px;
        vertical-align:top;
    }

    .controls-pac {
        margin-top: 10px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 366;
        margin-left: 0px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 366px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      .pac-container {
        font-family: Roboto;
      }

      #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
      }

      #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }
      #target {
        width: 345px;
      }
</style>
<style>
</style>

<div class="page-header">
    <div class="pull-left">
        <h1>Outlet Product</h1>
    </div>
    <div class="pull-right">
        <ul class="stats">
            <li class='lightred'>
                <i class="icon-calendar"></i>
                <div class="details">
                    <span class="big">-, -</span>
                    <span>-, -</span>
                </div>
            </li>
        </ul>
    </div>
</div>
<div class="breadcrumbs">
    <ul>
        <li>
            <a>Add New Products</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a>Add New</a>
        </li>
    </ul>
    <div class="close-bread" style="display: none;">
        <a href="#"><i class="icon-remove"></i></a>
    </div>
</div>
<div class="row-fluid">
    <div class="span12">
        <div class="box box-color box-bordered">
            <div class="box-title">
                <h3><i class="icon-table"></i>Add an Product</h3>
            </div>
            <div class="box-content" id="form_layout" style="overflow: scroll; height: 100%">
                
                <form id="form_data" class="span12" enctype="multipart/form-data" accept-charset="utf-8">
                    
                    <div class="control-group">
                        <h4 style="margin-bottom: 20px; margin-top: 20px">PRODUCT DETAIL</h4>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Name<span class="apt_bintang">*</span></label>
                        <div class="controls">
                            <input type="text" id="nama_produk" name="nama_produk" placeholder="Product Name" required class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Category<span class="apt_bintang">*</span></label>
                        <div class="controls">
                            <div class="input-xlarge" id="provinsi_form">
                                <select id="id_kategori_produk" name="id_kategori_produk" class='chosen-select input-xlarge' required>
                        </select>
                            </div>
                        </div>
                    </div>
                    <div class="control-group" id="description_form" style="display: none;">
                        <label class="control-label">Description</label>
                        <div class="controls">
                            <input type="text" id="keterangan" placeholder="Short Description" name="keterangan" class="input-xlarge" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Price</label>
                        <div class="controls input-prepend">
                            <span class="add-on">Rp</span>&nbsp<input type="number" min="0" step="0.01" id="harga" name="harga" required class="input">
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">Upload a photo<span class="apt_bintang">*</span></label>
                        <div class="controls">
                            <input type="file" id="form_file" name="form_file" required class="input-xlarge">
                        </div>
                    </div>
                    
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary"><i class="icon-add"></i> Save</button>
                        <span id="form_notif"></span>
                        <span><img id="form_loading" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    var id_kategori;
    var search_mode = 0;
    var id_kategories_produk;

    function sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    $(document).ready(function() {
        $(".chosen-select").chosen({
            no_results_text: "Data tidak ada!",
            width: "100%",
            placeholder_text_single: "Pilih Salah Satu"
        });


        $('#id_kategories_produk').change(function() {
            id_kategories_produk = $('#id_kategories_produk').val();
            load_kota(id_provinsi);
        });

    });

    function hide_show_forms(e) {
        if (e == 1) {
            $('#form_for_usaha').show();
            $('#code_count_form').prop('required', true);
        } else {
            $('#form_for_usaha').hide();
            $('#code_count_form').prop('required', false);
        }
    }

    function hide_show_code_form() {
        var e = $('#target').val();
        if (e == 1) {
            $('#code_count_form').show();
        } else {
            $('#code_count_form').hide();
        }
    }

    var id_pemilikes = [];



    var id_provinsies = [];

    load_kategori_produk();


    var id_kategories_produk = [];

    function load_kategori_produk() {
        var awal = '';
        $.ajax({
            url: "<?php echo URL_OPA.'mobile_v2/api_pemilik/get_kategori_produk'; ?>",
            data: "",
            type: 'GET',
            dataType: 'json',
            success: function(data, textStatus, jqXHR) {
                var html = "";

                console.log(data);

                for (var i = 0; i < data.data.length; i++) {
                    if (i==0) id_kategories_produk = data.data[i].id_kategori;
                    
                    var a = '<option value="' + data.data[i].id_kategori + '">' + data.data[i].nama_kategori + '</option>';
                    html = html + a;
                }
                id_kategories_produk = data.data;
                $('#id_kategori_produk').html(awal + html).trigger("liszt:updated");

            },
            error: function(jqXHR, textStatus, errorThrown) {

            }
        });
    }
    load_kategori_produk();


    $("#form_data").submit(function(e) {
        e.preventDefault();

        if (window.File && window.FileReader && window.FileList && window.Blob)
        {
            //get the file size and file type from file input field
            var fsize = $('#form_file')[0].files[0].size;
            
            if(fsize >1048576) //do something if file size more than 1 mb (1048576)
            {
                alert("Ukuran file tidak boleh lebih dari 1 MB");
                return;
            }
        }else{
            alert("Mohon perbarui browser Anda karena tidak cocok dengan sebagian fitur kami");
        }

        $("#form_loading").show();
        var dataPost = new FormData($(this)[0]);
        dataPost.append("id_kategori", id_kategori);
        $.ajax({
            url: "<?php echo URL_OPA.'update_produk/create'; ?>",
            data: dataPost,
            type: 'POST',
            dataType: 'json',
            async : false,
            cache : false,
            contentType : false,
            processData : false,
            success: function(data, textStatus, jqXHR) {
                console.log("OK");
                if (data.code == 1) {
                    
                    $('#form_data').find("input[type=text], textarea").val("");
                    $("#form_loading").fadeOut(1000, function() {
                        $("#form_notif").text(data.status);
                        $("#form_notif").show();
                        $("#form_notif").fadeOut(2000, function() {
                            buka_halaman('update_produk', 5);
                        });
                    });
                } else {
                        $("#form_notif").text(data.status);
                        $("#form_notif").show();
                        $("#form_loading").hide();
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("NO");
            }
        });
    });

</script>