<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<!-- Apple devices fullscreen -->
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<!-- Apple devices fullscreen -->
	<meta names="apple-mobile-web-app-status-bar-style" content="black-translucent" />
	
	<title>OPA - Merchant Login</title>

	<!-- Bootstrap -->
	<link rel="stylesheet" href="<?php echo URL_CSS; ?>bootstrap.min.css">
	<!-- Bootstrap responsive -->
	<link rel="stylesheet" href="<?php echo URL_CSS; ?>bootstrap-responsive.min.css">
	<!-- Theme CSS -->
	<link rel="stylesheet" href="<?php echo URL_CSS; ?>style.css">
	<!-- Color CSS -->
	<link rel="stylesheet" href="<?php echo URL_CSS; ?>themes.css">


	<!-- jQuery -->
	<script src="<?php echo URL_JS; ?>jquery.min.js"></script>
	<!-- Bootstrap -->
	<script src="<?php echo URL_JS; ?>bootstrap.min.js"></script>

</head>

<body class='login'>
    <div class="wrapper">
        <h1><a>OPA MERCHANT</a></h1>
        <div class="login-body">
            <h2>SIGN IN</h2>
            <form class='form-validate' id="id_form_login">
                <div class="control-group">
                    <div class="email controls">
                        <input type="text" id="id_login_username" name='username' placeholder="Username" class='input-block-level' required>
                    </div>
                </div>
                <div class="control-group">
                    <div class="pw controls">
                        <input type="password" id="id_login_password" name="password" placeholder="Password" class='input-block-level' required>
                    </div>
                </div>
                <div class="submit">
                    <input type="submit" value="Sign me in" class='btn btn-primary'>
                    <img id="load-login" class="pull-right" src="<?php echo URL_IMG.'loader.gif' ?>" style="display: none; margin-right: 15px;"/>
                    <p id="notif-login" class="pull-right" style="display: none; color: #000;font-size: 11px; margin-right: 15px;" ></p>
                </div>
            </form>
            <div class="forget">
                <a onclick="lupass()" style="cursor: pointer;"><span>Forgot password?</span></a>
            </div>
        </div>
    </div>
    <div id="modal_lupapas" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" style="width: 33%;">
        <div class="modal-header">
            <a type="button" class="close" data-dismiss="modal" aria-hidden="true">x</a>
            <h3 id="">Konfirmasi</h3>
        </div>
        <form class='form-validate' id="form_lupapas">
            <div class="modal-body">
                <div class="control-group">
                    <div class="controls">
                        <div class="input-prepend" style="margin-left: 10%;">
                            <span class="add-on">@</span>
                            <input type="email" id="email_lupapas" name="email_lupapas" required class="input-xlarge" placeholder="Masukkan email anda">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <span><img id="form_loading" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>
                <span id="form_notif"></span>
                <a class="btn" data-dismiss="modal" aria-hidden="true">Batal</a>
                <button type="submit" class="btn btn-primary"><i class="icon-save"></i> Kirim</button>
            </div>
        </form>
    </div>
    <div id="modal_lupapas1" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" style="width: 34%;">
        <div class="modal-header">
            <a type="button" class="close" data-dismiss="modal" aria-hidden="true">x</a>
            <h3>Konfirmasi Kode</h3>
        </div>
        <form class='form-validate' id="form_lupapas1">
            <div class="modal-body">
                <div class="control-group">
                    <div class="controls">
                        <div class="input-prepend" style="margin-left: 10%;">
                            <span class="add-on"><i class="icon-key"></i></span>
                            <input type="text" id="email_lupapas1" name="email_lupapas1" required class="input-xlarge" placeholder="Masukkan kode verifikasi">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <span><img id="form_loading1" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>
                <span id="form_notif1"></span>
                <a class="btn" data-dismiss="modal" aria-hidden="true">Batal</a>
                <button type="submit" class="btn btn-primary"><i class="icon-save"></i> Kirim</button>
            </div>
        </form>
    </div>
    <script>
        $("#id_form_login").submit(function (e){
            e.preventDefault();
            $("#load-login").show();
            $.ajax({
                url: "<?php echo URL_OPA.'merchant_login/proses_login/'; ?>",
                data: {"username":$("#id_login_username").val(),
                       "password":$("#id_login_password").val()},
                type: 'POST',
                dataType: 'json',
                success: function (data, textStatus, jqXHR) {
                    console.log('OK');
                    $("#notif-login").text(data.status);
                    $("#load-login").fadeOut(1000,function (){
                        $("#notif-login").show();
                        $("#notif-login").fadeOut(3000,function (){
                            if(data.code == "1"){
                                window.location.href="<?php echo URL_OPA . 'merchant_home'; ?>";
                            }
                        });
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log('NO');
                    
                }
            });
        });
        
        function lupass(){
            $("#modal_lupapas").modal("show");
        }
        $("#form_lupapas").submit(function (e){
            e.preventDefault();
            $("#form_notif").hide();
            $("#form_loading").show();
            $.ajax({
                url: "<?php echo URL_OPA.'login/lupa_pas'; ?>",
                data: { "email":$("#email_lupapas").val()
                        },
                type: 'POST',
                dataType: 'json',
                success: function (data, textStatus, jqXHR) {
                    console.log("OK");
                    $("#form_loading").fadeOut(1000, function (){
                        if(data.stat=="2"){
                            $("#form_notif").text(data.status);
                            $("#form_notif").show();
                        }else if(data.stat=="1"){
                            $("#form_notif").text(data.status);
                            $("#form_notif").show();
                            $("#form_notif").fadeOut(2000, function (){
                                $("#modal_lupapas").modal("hide");
                                $("#modal_lupapas1").modal("show");
                            });
                        }
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log("NO");
                }
            });
        });
        
        $("#form_lupapas1").submit(function(e){
            e.preventDefault();
            $("#form_notif1").hide();
            $("#form_loading1").show();
            $.ajax({
                url: "<?php echo URL_OPA.'login/lupa_pas_kode'; ?>",
                data: { "kodepas":$("#email_lupapas1").val()
                        },
                type: 'POST',
                dataType: 'json',
                success: function (data, textStatus, jqXHR) {
                    console.log("OK");
                    $("#form_loading1").fadeOut(1000, function (){
                        if(data.stat=="2"){
                            $("#form_notif1").text(data.status);
                            $("#form_notif1").show();
                        }else if(data.stat=="1"){
                            $("#form_notif1").text(data.status);
                            $("#form_notif1").show();
                            $("#form_notif1").fadeOut(2000, function (){
                                $("#modal_lupapas1").modal("hide");
                            });
                        }
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log("NO");
                }
            });
        });
    </script>
</body>
</html>