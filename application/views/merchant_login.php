<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
	<meta name="author" content="">
	<title>PA - Merchant Login</title>
	<link rel="shortcut icon" href="img/favicon.png">

	<!-- Bootstrap core CSS -->
	<link href="<?php echo URL_FRONT_CSS; ?>bootstrap.css" rel="stylesheet">
	<link href="<?php echo URL_FRONT_CSS; ?>bootstrap.min.css" rel="stylesheet">

	<!-- Style -->
	<link href="<?php echo URL_FRONT_CSS; ?>style.css" rel="stylesheet">
	<link href="<?php echo URL_FRONT_CSS; ?>animate.css" rel="stylesheet">
	<link href="<?php echo URL_FRONT_CSS; ?>responsive.css" rel="stylesheet">

	<!-- Custom Google Web Font -->
	<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href='https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>


	<script src="<?php echo URL_FRONT_JS; ?>modernizr-2.8.3.min.js"></script>  
</head>
<body>

	<div id="right" class="col-md-6 col-sm-6 nopadding"><!--  #Left -->
		<div class="left-section"><!-- .Left-section -->
			<div class="login-area"><!-- .Login-area -->
				<div class="logo-big text-center"><img src="<?php echo URL_FRONT_IMG; ?>logo.png"></div>
				<h2 class="subtitle">Merchant Login</h2>
				<div class="form-area"><!-- .Form-area -->
					<form  id="id_form_login">
                        <div class="form-group">
                            <p class="pull-right" style="color: #000;font-size: 12px; margin-right: 15px;" ><?php if (isset($status)) echo $status ?></p>
                        </div>
						<div class="form-group">
							<input name="" type="text" id="id_login_username" required class="form-control" placeholder="Username" />
						</div>
						<div class="form-group">
							<input name="" type="password" id="id_login_password" required class="form-control" placeholder="Password" id="pwd" />
							<i class="fa fa-eye active" aria-hidden="true" id="eye"></i>
						</div>
						<div class="row">
							<div class="col-md-6 col-xs-6">
								<a href="" id="forgot_password_a"><p class="text-left">Lupa Password ?</p></a>
							</div>
							<div class="col-md-6 col-xs-6">
								<a href="" id="login_button" class="btn btn-submit">Masuk</a>
			                    <img id="load-login" class="pull-right" src="<?php echo URL_FRONT_IMG.'loader.gif' ?>" style="display: none; margin-right: 15px;"/>
			                    <p id="notif-login" class="pull-right" style="display: none; color: #000;font-size: 11px; margin-right: 15px;" ></p>
							</div>
						</div>
					</form>
                    <form  id="form_lupapas" style="display: none;">
                        <div class="form-group" id="sendlink_input_form">
                            <input name="" type="text" id="email_lupapas" required class="form-control" placeholder="Email Anda" />
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-xs-6">
                                <a href="" id="back_to_login_a"><p class="text-left">Login</p></a>
                            </div>
                            <div class="col-md-6 col-xs-6">
                                <a href="" id="send_link_button" class="btn btn-submit">Kirim Link</a>
                                <img id="load-sendlink" class="pull-right" src="<?php echo URL_FRONT_IMG.'loader.gif' ?>" style="display: none; margin-right: 15px;"/>
                                <p id="notif-sendlink" class="pull-right" style="display: none; color: #000;font-size: 11px; margin-right: 15px;" ></p>
                            </div>
                        </div>
                    </form>
				</div><!-- / .Form-area -->
			</div><!-- / .Login-area -->
		</div><!-- .Left-section -->
	</div><!-- / #Left -->

    <div id="left" class="col-md-6 col-sm-6 nopadding"><!-- #Right -->
        <div class="right-section">
            <div class="logo-small">
                <img src="<?php echo URL_FRONT_IMG; ?>logo.png"> Smart Neighborhood
            </div>
        </div>
    </div><!-- / #Right -->



	<!-- JavaScript -->
	<script src="<?php echo URL_FRONT_JS; ?>jquery.min.js"></script>
	<!-- Bootstrap -->
	<script src="<?php echo URL_FRONT_JS; ?>bootstrap.min.js"></script>
	<!-- JavaScript HIDDEN -->
	<script type="text/javascript">

    $('#form_lupapas').hide();

    $('#forgot_password_a').click(function(event) {
        event.preventDefault();
        console.log("forgot");
        $('#form_lupapas').show();
        $('#id_form_login').hide();
    });

    $('#back_to_login_a').click(function(event) {
        event.preventDefault(event);
        console.log("forgot");
        $('#form_lupapas').hide();
        $('#id_form_login').show();
    });
		
    function show() {
			var p = document.getElementById('pwd');
			p.setAttribute('type', 'text');
		}

		function hide() {
			var p = document.getElementById('pwd');
			p.setAttribute('type', 'password');
		}

		var pwShown = 0;

		document.getElementById("eye").addEventListener("click", function () {
			if (pwShown == 0) {
				pwShown = 1;
				show();
			} else {
				pwShown = 0;
				hide();
			}
		}, false);

        $("#id_form_login").submit(function (e){
            e.preventDefault();
           login();
        });

        $("#login_button").click(function(event) {
        	event.preventDefault();
        	login(event);
        });
        
        function login(event) {
        	event.preventDefault();
        	 $("#load-login").show();
            $.ajax({
                url: "<?php echo URL_OPA.'merchant_login/proses_login/'; ?>",
                data: {"username":$("#id_login_username").val(),
                       "password":$("#id_login_password").val()},
                type: 'POST',
                dataType: 'json',
                success: function (data, textStatus, jqXHR) {
                    console.log('OK');
                    $("#notif-login").text(data.status);
                    $("#load-login").fadeOut(1000,function (){
                        $("#notif-login").show();
                        $("#notif-login").fadeOut(3000,function (){
                            if(data.code == "1"){
                                window.location.replace("<?php echo URL_OPA . 'merchant_home'; ?>");
                            }
                        });
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log('NO');
                    
                }
            });

        }
        function lupass(){
            $("#modal_lupapas").modal("show");
        }
        $("#send_link_button").click(function (e){
            e.preventDefault();
            $("#notif-sendlink").hide();
            $("#load-sendlink").show();
            $.ajax({
                url: "<?php echo URL_OPA.'merchant_login/lupa_pas'; ?>",
                data: { "email":$("#email_lupapas").val()
                        },
                type: 'POST',
                dataType: 'json',
                success: function (data, textStatus, jqXHR) {
                    console.log("OK");
                    $("#load-sendlink").fadeOut(1000, function (){
                        if(data.stat=="2"){
                            $("#notif-sendlink").text(data.status);
                            $("#notif-sendlink").show();
                        }else if(data.stat=="1"){
                            $("#notif-sendlink").text(data.status);
                            $("#notif-sendlink").show();
                            $("#sendlink_input_form").hide();
                            $("#send_link_button").hide();
                        }
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log("NO");
                }
            });
        });
        
        $("#form_lupapas1").submit(function(e){
            e.preventDefault();
            $("#form_notif1").hide();
            $("#form_loading1").show();
            $.ajax({
                url: "<?php echo URL_OPA.'merhant_login/lupa_pas_kode'; ?>",
                data: { 
                    "kodepas":$("#email_lupapas1").val()
                },
                type: 'POST',
                dataType: 'json',
                success: function (data, textStatus, jqXHR) {
                    console.log("OK");
                    $("#form_loading1").fadeOut(1000, function (){
                        if(data.stat=="2"){
                            $("#form_notif1").text(data.status);
                            $("#form_notif1").show();
                        }else if(data.stat=="1"){
                            $("#form_notif1").text(data.status);
                            $("#form_notif1").show();
                        }
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log("NO");
                }
            });
        });

	</script>
	<script>
		$('i').click(function(){
			$(this).toggleClass('active');
		});
	</script>
	<!-- / JavaScript -->

</body>
</html>