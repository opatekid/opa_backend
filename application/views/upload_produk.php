<div class="page-header">
    <div class="pull-left">
            <h1><?php echo $pemilik->nama; ?></h1>
    </div>
    <div class="pull-right">
        <ul class="stats">
            <li class='lightred'>
                <i class="icon-calendar"></i>
                <div class="details">
                    <span class="big">February 22, 2013</span>
                    <span>Wednesday, 13:56</span>
                </div>
            </li>
        </ul>
    </div>
</div>
<div class="breadcrumbs">
        <ul>
            <li>
                <a href="index.html">Produk Tanpa Minimum<?php echo $pemilik->nama; ?></a>
            </li>
        </ul>
        <div class="close-bread">
                <a href="#"><i class="icon-remove" style="display: none;" ></i></a>
        </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <div class="box">
        </div>
    </div>
</div>
<div class="row-fluid">
    <div class="span12">
        <div class="box box-color box-bordered">
            <div class="box-title">
                <h3><i class="icon-table"></i>Upload Database Produk Tanpa Minimum</h3>
            </div>
            <div class="box-content nopadding"  style="overflow: scroll">
                <form id="form_data" class="form-horizontal" enctype="multipart/form-data">
                    <div class="control-group" style=" margin: 20px;">
                        <label class="control-label">Data XLS</label>
                        <div class="controls">
                            <input type="file" id="form_file" name="form_file" required class="input-xlarge">
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" id="upload_button" class="btn btn-primary"><i class="icon-save"></i><span id="upload_span"> Upload</span></button>
                        <span id="form_notif"></span>
                        <span><img id="form_loading" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<!--<div class="row-fluid">
    <div class="span-12">
     
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Pendapatan Perbulan</h3>
            <div class="box-tools pull-right">
              <button class="btn btn-box-tool" data-widget="collapse"><i class="icon-plus"></i></button>
              <button class="btn btn-box-tool" data-widget="remove"><i class="icon-remove" style="display: none;" ></i></button>
            </div>
          </div>
          <div class="box-body">
              <div class="chart">
              <canvas id="areaChart"></canvas>

            </div>
          </div> 
        </div> 
    </div>
</div>-->
<script>
    
    $(function () {

    $('#overwrite').change(function(e) {
        var v = $(this).is(':checked')? 1:0 ;
        console.log(v);
        $(this).val(v);
    });

    $("#form_data").submit(function (e){
        e.preventDefault();
        $("#form_loading").show();
        $("#notif_username").hide();
        $("#notif_email").hide();
        $("#upload_button").attr('disabled', 'disabled');
        $("#upload_span").text("Uploading...");
        $("#form_notif").text("Mohon tidak menuntup tab/jendela ini selama proses importing berlangsung")
        $.ajax({
            url: "<?php echo URL_OPA.'mobile_v2/api_pemilik/insert_produk_batch/' . $pemilik->id_pemilik; ?>",
            data: new FormData($(this)[0]),
            type: 'POST',
            dataType: 'json',
            async : true,
            cache : false,
            contentType : false,
            processData : false,
            success: function (data, textStatus, jqXHR) {
                console.log("OK");
                $("#form_loading").fadeOut(1000, function (){
                    $("#upload_span").text("Uploaded");
                    if(data.code == '1'){
                        $("#form_notif").text(data.pesan);
                        $("#form_notif").show();
                        $("#form_notif").fadeOut(2000, function (){
                            produk=data.data;
                        console.log(produk);
                            load_produk(produk);
                            $("#modal_data").modal("hide");
                        });
                    } else {
                        $("#form_notif").text(data.pesan);
                    }
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("NO");
            }
        });
    });
    
});
    
    
    
        
</script>