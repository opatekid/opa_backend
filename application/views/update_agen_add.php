<script></script>
<style type="text/css">
    #map {
        height: 1200px;
        width: 100%;
        position: relative;
    }

   #wrapper { position: relative; }
   #over_map { 
        background-color: white; position: 
        absolute; 
        top: 10px; 
        left: 200px; 
        z-index: 99; 
        padding: 10px; 
        width: 480px;
    }

    .option_tab {
        display:inline-block;
        margin-right: 10px;
        *display:inline;/* For IE7 */
        *zoom:1;/* For IE7 */
        white-space:normal;
        font-size:13px;
        vertical-align:top;
    }

</style>
<div class="page-header">
    <div class="pull-left">
        <h1>Agen</h1>
    </div>
    <div class="pull-right">
        <ul class="stats">
            <li class='lightred'>
                <i class="icon-calendar"></i>
                <div class="details">
                    <span class="big">-, -</span>
                    <span>-, -</span>
                </div>
            </li>
        </ul>
    </div>
</div>
<div class="breadcrumbs">
    <ul>
        <li>
            <a>Daftar Agen</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a>Edit Detail</a>
        </li>
    </ul>
    <div class="close-bread">
        <a href="#"><i class="icon-remove" style="display: none;" ></i></a>
    </div>
</div>
<div class="row-fluid">
    <div class="span12">
        <div class="box box-color box-bordered">
            <div class="box-title">
                <h3><i class="icon-table"></i>Edit Data Agen</h3>
            </div>
            <div class="box-content nopadding" id="form_layout" style="overflow: scroll; height: 100%">
                <div class="span8" id="wrapper" >
                    <div id="map"></div>
                    <div id="over_map">
                        <div class="row-fluid">
                            <div class="span12">
                                <h5><b>Set Lokasi Agen pada Map </b></h5>
                            </div>
                        </div> 
                        <div class="row-fluid" style="margin-bottom: 10px;">
                            <span class="option_tab">
                                <a id="sharelink_button" class="link_button">
                                <span id="sharelink_wrap" class="link_wrap">Share Link</span></a>
                            </span>
                            <span class="option_tab">
                                <a id="shortlink_button" class="link_button">
                                <span id="shortlink_wrap" class="link_wrap">Short Link</span></a>
                            </span>
                            <span class="option_tab">
                                <a id="latlnglink_button" class="link_button">
                                <span id="latlnglink_wrap" class="link_wrap">Latitude/Longitude</span></a>              
                            </span>
                        </div>
                        <div class="row-fluid">
                            <div class="span9">
                                <div class="span12" id="sharelinkform_input">
                                    <input type="text" id="sharelink_input" placeholder="Share Link" name="sharelink_input" class="span12">
                                </div>
                                <div class="row-fluid" id="shortlinkform_input">
                                    <div class="span12">
                                        <input type="text" name="shortlink_input" placeholder="Short Link" class="span12">
                                    </div>
                                </div>
                                <div class="row-fluid" id="latlngform_input">
                                    <div class="span12">
                                        <input type="text" id="lat" name="lat" value="<?php echo $agen->lat; ?>" placeholder="Latitude" class="span6 input-xlarge location-input">
                                        <input type="text" id="lng" style="margin-left: 5px;" name="lng" value="<?php echo $agen->lng; ?>" placeholder="Longitude" name="lng" class="input-xlarge location-input span6" required />
                                    </div>
                                </div>
                            </div>
                            <button id="search_button" class="btn btn-primary span3">Cari</button>
                        </div>
                        <div class="row-fluid">
                            <span class="span12">Atau Anda dapat mengaturnya secara manual dengan mengklik pada peta
                            </span>
                        </div>
                     </div>
                </div>
                <form id="form_data" class="span4" enctype="multipart/form-data" accept-charset="utf-8">
                    
                    <div class="control-group">
                        <label class="control-label">Nama</label>
                        <div class="controls">
                            <input type="text" id="nama" required name="nama" placeholder="Misal: JNE Jagakarsa, TIKI Kalibata, dll" value="<?php echo $agen->nama; ?>" required class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group" id="description_form">
                        <label class="control-label">Alamat Agen</label>
                        <div class="controls">
                            <input type="text" id="alamat" placeholder="Alamat Lengkap agen" value="<?php echo $agen->alamat; ?>" name="alamat" class="input-xlarge" required />
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary"><i class="icon-add"></i> Simpan</button>
                        <span id="form_notif"></span>
                        <span><img id="form_loading" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBuDrC5iMEnz7vgrcXHSwKqqPpmHCVrp-c&callback=initMap"></script>
<script>

    var currentMarker;
    var map;
    var id_ekspedisi = '<?php echo $agen->id_ekspedisi; ?>';;
    var lat = parseFloat('<?php echo $agen->lat; ?>');;
    var lng = parseFloat('<?php echo $agen->lng; ?>');;
    var markersArray = new Array();
    var search_mode = 0;

    function sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
   $('#sharelink_button').contents().unwrap();
        $('#latlnglink_button').contents().unwrap();
        $('#shortlink_wrap').wrap('<a id="shortlink_button" style="cursor: pointer;">');
        $('#latlnglink_wrap').wrap('<a id="latlnglink_button" style="cursor: pointer;">');
        $('#sharelink_button').contents().unwrap();

        $('#sharelinkform_input').show();
        $('#shortlinkform_input').hide();
        $('#latlngform_input').hide();

    $('span.option_tab').on('click', '#sharelink_button', function() {
        console.log('sharelink_button');
        $('#sharelink_button').contents().unwrap();
        $('#latlnglink_button').contents().unwrap();
        $('#shortlink_wrap').wrap('<a id="shortlink_button" style="cursor: pointer;">');
        $('#latlnglink_wrap').wrap('<a id="latlnglink_button" style="cursor: pointer;">');
        $('#sharelink_button').contents().unwrap();

        $('#sharelinkform_input').show();
        $('#shortlinkform_input').hide();
        $('#latlngform_input').hide();
        search_mode = 0;
    });

    $('span.option_tab').on('click', '#shortlink_button',  function() {
        console.log('shortlink_button');
        $('#sharelink_button').contents().unwrap();
        $('#latlnglink_button').contents().unwrap();
        $('#sharelink_wrap').wrap('<a id="sharelink_button" style="cursor: pointer;">');
        $('#latlnglink_wrap').wrap('<a id="latlnglink_button" style="cursor: pointer;">');
        $('#shortlink_button').contents().unwrap();

        $('#sharelinkform_input').hide();
        $('#shortlinkform_input').show();
        $('#latlngform_input').hide();
        search_mode = 1;
    });

    $('span.option_tab').on('click', '#latlnglink_button', function() {
        console.log('latlnglink_button');
        $('#sharelink_button').contents().unwrap();
        $('#shortlink_button').contents().unwrap();
        $('#sharelink_wrap').wrap('<a id="sharelink_button" style="cursor: pointer;">');
        $('#shortlink_wrap').wrap('<a id="shortlink_button" style="cursor: pointer;">');
        $('#latlnglink_button').contents().unwrap();

        $('#sharelinkform_input').hide();
        $('#shortlinkform_input').hide();
        $('#latlngform_input').show();
        search_mode = 2;
    });


    function initMap() {
        var latlng = {
            lat: lat,
            lng: lng
        };
        lat = latlng.lat;
        lng = latlng.lng;
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 16,
            center: latlng
        });

        addNewMarker(latlng);

        google.maps.event.addListener(map, 'click', function(event) {
            placeMarker(event.latLng);
        });

        function placeMarker(location) {
            lat = location.lat();
            lng = location.lng();
            console.log(lat + ", " + lng);
            $('#lat').val(lat + '');
            $('#lng').val(lng + '');

            currentMarker.setPosition(new google.maps.LatLng(lat, lng));
        }

           
        $('#search_button').click(function() {
            if (search_mode == 0) {
                var url = $('#sharelink_input').val();
                var regex = new RegExp('@(.*),(.*),');
                var lon_lat_match = url.match(regex);
                lat = lon_lat_match[1];
                lng = lon_lat_match[2];
                console.log(lat + ", " + lng);
                if (currentMarker !=null) {
                    currentMarker.setPosition(new google.maps.LatLng(lat, lng));
                } else {
                    var latlng = {
                        lat: lat,
                        lng: lng
                    };
                    addNewMarker(latlng)
                }
                var center = new google.maps.LatLng(lat, lng);
                // using global variable:
                map.panTo(center);
            } else if (search_mode == 1) { 
            } else if (search_mode == 2) {
                lat = parseFloat($('#lat').val() != null &&  $('#lat').val() != '' ? $('#lat').val() : 0.0);
                lng = parseFloat($('#lng').val() != null &&  $('#lng').val() != '' ? $('#lng').val() : 0.0);
                console.log(lat + ", " + lng);
                if (currentMarker !=null) {
                    currentMarker.setPosition(new google.maps.LatLng(lat, lng));
                } else {
                    var latlng = {
                        lat: lat,
                        lng: lng
                    };
                    addNewMarker(latlng)
                }
                var center = new google.maps.LatLng(lat, lng);
                // using global variable:
                map.panTo(center);
            }
        });

        
    }

    function addNewMarker(latlng) {
        currentMarker = new google.maps.Marker({
            position: latlng,
            map: map,
            animation: google.maps.Animation.DROP
        });
    }

    $(document).ready(function() {
        $(".chosen-select").chosen({
            no_results_text: "Data tidak ada!",
            width: "100%",
            placeholder_text_single: "Pilih Salah Satu"
        });


        $('#id_provinsi').change(function() {
            id_provinsi = $('#id_provinsi').val();
            load_kota(id_provinsi);
        });

        $('#id_kota').change(function() {
            id_kota = $('#id_kota').val();
        });

    });

    function hide_show_forms(e) {
        if (e == 1) {
            $('#form_for_agen').show();
            $('#code_count_form').prop('required', true);
        } else {
            $('#form_for_agen').hide();
            $('#code_count_form').prop('required', false);
        }
    }

    function hide_show_code_form() {
        var e = $('#target').val();
        if (e == 1) {
            $('#code_count_form').show();
        } else {
            $('#code_count_form').hide();
        }
    }


    $("#form_data").submit(function(e) {
        e.preventDefault();
        currentMarker.setMap(null);
        var latlng = {
            lat: lat,
            lng: lng
        };
        var newMarker = new google.maps.Marker({
            position: latlng,
            map: map,
            animation: google.maps.Animation.DROP
        });
        newMarker.setIcon('<?php echo URL_IMG . "plugins/gmap/"; ?>' + 'green-dot.png');

        var contentString = "<b>" + $('#nama').val() + "</b><br>" + $('#alamat').val();
        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });
        
        infowindow.open(map, newMarker);
        newMarker.addListener('click', function() {
            infowindow.open(map, newMarker);
        });

        $("#form_loading").show();
        var dataPost = new FormData($(this)[0]);
        // dataPost = dataPost + "&lat=" + lat + "&lng=" + lng + "&id_provinsi=" + id_provinsi + "&id_kota=" + id_kota;
        dataPost.append("lat", lat);
        dataPost.append("lng", lng);
        $.ajax({
            url: "<?php echo URL_OPA.'update_ekspedisi/update_agen/'.$agen->id_agen; ?>",
            data: dataPost,
            type: 'POST',
            dataType: 'json',
            async : false,
            cache : false,
            contentType : false,
            processData : false,
            success: function(data, textStatus, jqXHR) {
                console.log("OK");
                if (data.code == 1) {
                    markersArray.push(newMarker);
                    var latlng = {
                        lat: lat,
                        lng: lng
                    };
                    addNewMarker(latlng)
                    $('#form_data').find("input[type=text], textarea").val("");
                    $("#form_loading").fadeOut(1000, function() {
                        $("#form_notif").text(data.status);
                        $("#form_notif").show();
                        $("#form_notif").fadeOut(2000, function() {
                            buka_halaman("update_ekspedisi/agen/" + id_ekspedisi, 11);
                        });
                    });
                } else {

                        $("#form_notif").text(data.status);
                        $("#form_notif").show();
                        $("#form_loading").hide();
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("NO");
            }
        });
    });
</script>