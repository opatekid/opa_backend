<script>
var kategoriTeks = '<?php echo json_encode($kategori); ?>';
var kategori  = JSON.parse(kategoriTeks);
    function load_produk(produk){
        $("#id_tabel").dataTable().fnDestroy();
        var html ="";
        for(var i =0;i<produk.length;i++){
            var a ='<tr>\n\
                        <td>'+(i+1)+'</td>\n\
                        <td><img src="<?php echo URL_IMAGE.'produk/'; ?>'+produk[i].foto+'" /></td>\n\
                        <td>'+produk[i].nama_produk+'</td>\n\
                        <td>'+produk[i].nama_kategori+'</td>\n\
                        <td>Rp '+produk[i].harga+',-</td>\n\
                        <td>'+produk[i].stok+'</td>\n\
                        <td><a onclick="hak_modal('+i+')"><i rel="tooltip" title="Hak Akses" data-placement="bottom" style="cursor: pointer; margin-right: 15px;" class="icon icon-th-list"></i></a>\
                            <a onclick="hapus_modal('+i+')"><i rel="tooltip" title="Hapus" data-placement="bottom" style="cursor: pointer" class="icon icon-trash"></i></a>\
                        </td>\n\
                    </tr>';
            
            html = html + a;
        }
        $('#isi_tabel').html(html);
        $("#id_tabel").dataTable();
    }

    function load_kategori(kategori){
        var html ="";
        for(var i =0;i<kategori.length;i++){
            var a ="<option value='" + kategori[i].id_kategori + "'>" + kategori[i].nama_kategori + "</option>";
            html = html + a;
        }
        $('#id_kategori').html(html);
    }
</script>


<div id="modal_data" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-header">
        <a type="button" class="close" data-dismiss="modal" aria-hidden="true">x</a>
        <h3 id="">Tambah Produk</h3>
    </div>
    <form id="form_data" class="form-horizontal" enctype="multipart/form-data" accept-charset="utf-8">
        <div class="modal-body">
            <div class="control-group">
                <label class="control-label">Nama<span class="apt_bintang">*</span></label>
                <div class="controls">
                    <input type="text" id="nama" name="nama" required class="input-xlarge">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Kategori<span class="apt_bintang">*</span></label>
                
                            <div class="controls">
                                <div class="input-xlarge">
                                    <select id="id_kategori" name="id_kategori" class='chosen-select input-xlarge' required>
                                        
                                    </select>
                                </div>
                            </div>
            </div>
            <div class="control-group">
                <label class="control-label">Harga<span class="apt_bintang">*</span></label>
                <div class="controls">
                    <input type="number" min="0" value="0" id="harga" name="harga" required class="input-xlarge">
                </div>
            </div>

            <div class="control-group">
                <label class="control-label">Stok<span class="apt_bintang">*</span></label>
                <div class="controls">
                    <input type="number" min="1" value="1" id="stok" name="stok" required class="input-xlarge">
                </div>
            </div>

            <div class="control-group">
                <label class="control-label">Gambar<span class="apt_bintang">*</span></label>
                <div class="controls">
                    <input type="file" id="form_file" name="form_file" required class="input-xlarge">
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <span id="form_notif"></span>
            <span><img id="form_loading" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>

            <a class="btn" data-dismiss="modal" aria-hidden="true">Batal</a>
            <button type="submit" class="btn btn-primary"><i class="icon-save"></i> Simpan</button>
        </div>
    </form>
</div>

<div id="modal_data_hapus" class="modal hide fade" role="dialog">
    <div class="modal-dialog" >

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h3>Konfirmasi</h3>
            </div>
            <div class="modal-body">
                <center>
                    <input type="hidden" id="modal_data_hapus_id">
                    <p style="font-size: 15px;">Yakin Menghapus <span id="modal_data_hapus_p" style="font-weight: bold;"></span> ?</p>
                </center>
            </div>

        </div>
        <div class="modal-footer">
            <span id="modal_data_hapus_notif"></span>
            <span><img id="modal_data_hapus_loading" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>

            <button class="btn" data-dismiss="modal" aria-hidden="true">Batal</button>
            <button onclick="hapus()" class="btn btn-danger"><i class="icon-trash"></i> Hapus</button>
        </div>
    </div>
</div>

<div class="page-header">
    <div class="pull-left">
        <h1>Promo</h1>
    </div>
    <div class="pull-right">
        <ul class="stats">
            <li class='lightred'>
                <i class="icon-calendar"></i>
                <div class="details">
                    <span class="big">-, -</span>
                    <span>-, -</span>
                </div>
            </li>
        </ul>
    </div>
</div>
<div class="breadcrumbs">
    <ul>
        <li>
            <a>Promo</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a>Produk Promo</a>
        </li>
    </ul>
    <div class="close-bread">
            <a href="#"><i class="icon-remove" style="display: none;" ></i></a>
    </div>
</div>

<div class="row-fluid">
    <div class="span12">
                <h3>Kode Promo : <b><?php echo $promo_code->code; ?></b></h3>
        <div class="box box-color box-bordered">
            <div class="box-title">
                <h3><i class="icon-table"></i>Daftar Produk</h3>
            </div>
            <div class="box-content nopadding"  style="overflow: scroll">
                <button class="btn btn-blue tombol_add" onclick="buka_modal()" style="margin: 10px;"><i class="icon-plus"></i> Tambah</button>
                <table class="table table-hover table-nomargin table-bordered" id="id_tabel">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Gambar</th>
                            <th>Nama</th>
                            <th>Kategori</th>
                            <th>Harga</th>
                            <th>Stok</th>
                            <th>Opsi</th>
                        </tr>
                    </thead>
                    <tbody id="isi_tabel">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
load_kategori(kategori);
    $('.check_table').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        increaseArea: '20%' // optional
    });
    function buka_modal(){
        $("#id_nama").val("");
        $("#id_username").val("");
        $("#id_email").val("");
        $("#modal_data").modal("show");
    }

    function get_produk_promo() {
        $.ajax({
            url: "<?php echo URL_OPA.'promo_old/get_produk_promo/' . $id_promo; ?>",
            type: 'POST',
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                console.log("OK");
                $("#form_loading").fadeOut(1000, function (){
                    if(data.code == '1'){
                        $("#form_notif").text(data.pesan);
                        $("#form_notif").show();
                        $("#form_notif").fadeOut(2000, function (){
                            produk=data.data;
                        console.log(produk);
                            load_produk(produk);
                            $("#modal_data").modal("hide");
                        });
                    } else {
                        $("#form_notif").text(data.pesan);
                    }
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("NO");
            }
        });
    }

    get_produk_promo();
    
    
    $("#form_data").submit(function (e){
        e.preventDefault();
        $("#form_loading").show();
        $("#notif_username").hide();
        $("#notif_email").hide();
        $.ajax({
            url: "<?php echo URL_OPA.'promo_old/insert_produk/' . $id_promo; ?>",
            data: new FormData($(this)[0]),
            type: 'POST',
            dataType: 'json',
            async : false,
            cache : false,
            contentType : false,
            processData : false,
            success: function (data, textStatus, jqXHR) {
                console.log("OK");
                $("#form_loading").fadeOut(1000, function (){
                    if(data.code == '1'){
                        $("#form_notif").text(data.pesan);
                        $("#form_notif").show();
                        $("#form_notif").fadeOut(2000, function (){
                            produk=data.data;
                        console.log(produk);
                            load_produk(produk);
                            $("#modal_data").modal("hide");
                        });
                    } else {
                        $("#form_notif").text(data.pesan);
                    }
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("NO");
            }
        });
    });
    
    function hapus_modal(i){
        $("#modal_data_hapus_id").val(produk[i].id);
        $("#modal_data_hapus_p").html(produk[i].nama);
        $("#modal_data_hapus").modal("show");
    }
    
    function hapus(){
        $("#modal_data_hapus_loading").show();
        $.ajax({
            url: "<?php echo URL_OPA.'sett_produk/delete'; ?>",
            data: {"id":$("#modal_data_hapus_id").val()},
            type: 'POST',
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $("#modal_data_hapus_loading").fadeOut(1000,function(){
                    $("#modal_data_hapus_notif").text(data.status);
                    $("#modal_data_hapus_notif").show();
                    $("#modal_data_hapus_notif").fadeOut(2000, function (){
                        produk=data.list;
                        console.log(produk);
                        load_produk(produk);
                        $("#modal_data_hapus").modal("hide");
                    });
                    
                });
                
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("gagal");

            }
        });
    }
    var check ="";
    
</script>