<script>
    var active_code = <?php echo json_encode($active_code)?>;
    var promo_id = <? echo json_encode($promo_id) ?>;
    
    function load_promo(active_code){
        var html ="";
        var tipe ="";
        for(var i =0;i<active_code.length;i++){
            var konsumen_count = 'Belum ada konsumen';
            if (active_code[i].konsumen_count > 0) {
                konsumen_count = active_code[i].konsumen_count + " konsumen terdaftar";
            }
            aksi_show_konsumen = '<a onclick="open_modal('+active_code[i].id+', \'Konsumen '+active_code[i].nama_pemilik+'\')"><i rel="tooltip" title="Konsumen terdaftar" style="cursor: pointer" class="icon icon-file"> '+konsumen_count+'</i></a>';
            aksi_show_histori = '<a onclick="open_modal_histori('+active_code[i].id+', \'Histori Promo '+active_code[i].nama_pemilik+'\')"><i rel="tooltip" title="Histori Order Promo" style="cursor: pointer" class="icon icon-file"> '+active_code[i].order_count+' order</i></a>';
            aksi_hapus = '<a onclick="edit_modal('+i+')"><i rel="tooltip" title="Hapus" style="cursor: pointer" class="icon icon-trash"></i></a>';
            var jenis = 'No Price';
            switch (active_code[i].jenis) {
                case '1':
                jenis = "No Price";
                break;
                case '2':
                jenis = "No Fee";
                break;
                case '3':
                jenis = "Free fee 1 year";
                break;
            }
            var a ='<tr>\n\
            <td>'+(i+1)+'</td>\n\
            <td>'+active_code[i].id_pemilik+'</td>\n\
            <td>'+active_code[i].nama_pemilik+'</td>\n\
            <td>'+jenis+'</td>\n\
            <td>'+active_code[i].code+'</td>\n\
            <td>'+aksi_show_konsumen+'</td>\n\
            <td>'+aksi_show_histori+'</td>\n\</tr>';

            html = html + a;
        }
        $('#isi_tabel').html(html);
        $("#id_tabel").dataTable();
    }

    load_promo(active_code);
</script>

<div id="modal_data" style="width: 720px" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-header">
        <a type="button" class="close" data-dismiss="modal" aria-hidden="true">x</a>
        <div class="box-title">
            <h3 id="modal_title"><i class="icon-table"></i>Konsumen Terdaftar</h3>
        </div>
        <div class="box-content nopadding"  style="overflow: scroll">                
            <table class="table table-hover table-nomargin table-bordered" id="tabel_konsumen">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>ID</th>
                        <th>Nama</th>
                        <th>Code Input</th>
                    </tr>
                </thead>
                <tbody id="isi_tabel_konsumen">

                </tbody>
            </table>
        </div>
    </div>
</div>

<div id="modal_data_histori" style="width: 960px; left: 35%" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-header">
        <a type="button" class="close" data-dismiss="modal" aria-hidden="true">x</a>
        <div class="box-title">
            <h3 id="modal_title_histori"><i class="icon-table"></i>Histori Order Promo</h3>
        </div>
        <div class="box-content nopadding"  style="overflow: scroll">                
            <table class="table table-hover table-nomargin table-bordered" id="tabel_histori">
                <thead>
                    <tr>
                        <th>Konsumen</th>
                        <th>Pemilik Usaha</th>
                        <th>Produk</th>
                        <th>Qty</th>
                        <th>Total Harga</th>
                        <th>Diorder</th>
                        <th>Diproses</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody id="isi_tabel_histori">

                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="page-header">
    <div class="pull-left">
        <h1><?php echo $nama_promo ?></h1>
    </div>
    <div class="pull-right">
        <ul class="stats">
            <li class='lightred'>
                <i class="icon-calendar"></i>
                <div class="details">
                    <span class="big">-, -</span>
                    <span>-, -</span>
                </div>
            </li>
        </ul>
    </div>
</div>
<div class="breadcrumbs">
    <ul>
        <li>
            <a>Promo</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a><?php echo $nama_promo ?></a>
        </li>
    </ul>
    <div class="close-bread">
        <a href="#"><i class="icon-remove" style="display: none;" ></i></a>
    </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <div class="box box-color box-bordered">
            <div class="box-title">
                <h3><i class="icon-table"></i>Daftar Pemilik Usaha</h3>
            </div>
            <div class="box-content nopadding"  style="overflow: scroll">  
                <table class="table table-hover table-nomargin table-bordered">              
                    <tbody>
                        <tr>
                            <td>Tampilkan &nbsp;&nbsp;<select id="filter_select" name="filter_select">
                                <option value="0" selected="selected">Semua</option>
                                <option value="1">Teraktivasi</option>
                            </select></td>
                        </tr>
                    </tbody>
                </table>
                <table class="table table-hover table-nomargin table-bordered" id="id_tabel">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>ID</th>     
                            <th>Nama Toko</th>
                            <th>Jenis Promo</th>
                            <th>Promo Code</th>
                            <th>Konsumen</th>
                            <th>Histori</th>
                        </tr>
                    </thead>
                    <tbody id="isi_tabel">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    //load_upgrade(json_upgrade);
    $(document).ready(function(){

        $(".chosen-select").chosen({
            no_results_text: "Data tidak ada!",
            width: "100%",
            placeholder_text_single : "Pilih Salah Satu"
        });
        $('#filter_select').select2();
        $('#filter_select').change(function() {
            buka_halaman('promo_old/detail?id='+promo_id+'&filter_select='+$('#filter_select').val())
        })

    });
    
    function edit_modal(i){
        $("#modal_data").modal("show");
        $("#form_id").val(json_upgrade[i].id_pemilik);
        $("#id_nama").val(json_upgrade[i].nama);
        $("#id_jenis").val(json_upgrade[i].nama_jenis);
        $("#id_level").val(json_upgrade[i].level).trigger("liszt:updated");
    }

    function load_promo_code_konsumen(id){
        $.ajax({          
            url: "<?php echo URL_OPA.'promo_old/konsumen?id='; ?>"+id,
            data:"",
            type: 'GET',
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                console.log(data);
                var html ="";
                var konsumens = data.konsumen;
                for (var i = 0; i < konsumens.length; i++) {
                    var a ='<tr>\n\
                    <td>'+(i+1)+'</td>\n\
                    <td>'+konsumens[i].konsumen_id+'</td>\n\
                    <td>'+konsumens[i].nama_konsumen+'</td>\n\
                    <td>'+konsumens[i].pemilik_user_id+'</td>\n\</tr>';
                    html = html + a;
                }
                $('#isi_tabel_konsumen').html(html);
                $("#tabel_konsumen").dataTable();
            },
            error: function (jqXHR, textStatus, errorThrown) {

            }
        });
    }

    function load_histori(id){
        $.ajax({          
            url: "<?php echo URL_OPA.'promo_old/histori_transaksi?id='; ?>"+id,
            data:"",
            type: 'GET',
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                console.log(data);
                var html ="";
                var histories = data.histories;
                for (var i = 0; i < histories.length; i++) {
                    var a ='<tr>\n\
                    <td>'+histories[i].nama_konsumen+'</td>\n\
                    <td>'+histories[i].nama_pemilik+'</td>\n\
                    <td>'+histories[i].nama_produk+'</td>\n\
                    <td>'+histories[i].kuantitas+'</td>\n\
                    <td>Rp '+histories[i].total+'</td>\n\
                    <td>'+histories[i].tanggal_order+'</td>\n\
                    <td>'+histories[i].tanggal_proses+'</td>\n\
                    <td>'+(histories[i].status == "1" ? "Dipesan" : "Selesai") +'</td>\n\</tr>';
                    html = html + a;
                }
                $('#isi_tabel_histori').html(html);
                $("#tabel_histori").dataTable();
            },
            error: function (jqXHR, textStatus, errorThrown) {

            }
        });
    }

    function open_modal(id, title){
        $("#modal_data").modal("show");
        $('#modal_title').text(title);
        load_promo_code_konsumen(id);
    }

    function open_modal_histori(id, title){
        $("#modal_data_histori").modal("show");
        $('#modal_title_histori').text(title);
        load_histori(id);
    }


    ///////////////////////
    
    var dataTable;
    
    // datatable_server_side();
    
    function datatable_server_side(){
        console.log("server");
        if(dataTable != null)dataTable.fnDestroy();
        
        dataTable = $('#id_tabel').dataTable( {
            'iDisplayLength': 10,
            "aoColumnDefs": [
            { 'bSortable': false, 'aTargets': [ 0,1,2,3,4,5 ] }
            ],
            "processing": true,
            "serverSide": true,
            "ajax":{
                url :"<?php  echo URL_OPA; ?>promo/get_data", // json datasource
                type: "post",  // method  , by default get
                "data": function ( d ) {

                },
                error: function(){  // error handling
                    $(".id_tabel-error").html("");
                    $("#id_tabel").append('<tbody class="id_tabel-error"><tr><th colspan="6">No data found in the server</th></tr></tbody>');
                    $("#id_tabel_processing").css("display","none");

                },
                "dataSrc": function ( json ) {
                    json_upgrade = json.json;
                    return json.data;
                }
            }
            
        });

    }
</script>