<style type="text/css">


    /* Important part */
    .modal-dialog {
        overflow-y: initial !important
    }
    #data-modal-body {
        height: auto;
        overflow-y: auto;
    }
</style>
<script>
    var json_upgrade = <?php echo json_encode($tran_upgrade)?>;
    var level = <?php echo json_encode($level)?>;
    var status_edit = "0";
    
    function load_opsi(json){
        for(var i=0;i<json.length;i++){
            if(json[i].id_role == "18"){
                status_edit = "1";
            }
        }   
    }
    
    function load_upgrade(tran_upgrade){
        //$("#id_tabel").dataTable().fnDestroy();
        var html ="";
        var tipe ="";
        for(var i =0;i<tran_upgrade.length;i++){
            if (tran_upgrade[i].tipe == '1'){
                tipe = "Tradisional";
            }else if(tran_upgrade[i].tipe == '2'){
                tipe = "Minimarket";
            }
            var aksi_edit = "";
            
            if(status_edit == "1"){
                aksi_edit = '<a onclick="buka_halaman(\'update_ekspedisi_edit/' + tran_upgrade[i].id_ekspedisi + '\', 2)"><i rel="tooltip" title="Ubah" style="cursor: pointer" class="icon icon-pencil"></i></a>';
            }

            aksi_upload = '<a onclick="buka_halaman(\'merchant_beranda/' + tran_upgrade[i].id_ekspedisi + '\', \'1\')"><i rel="tooltip" title="Ubah" style="cursor: pointer" class="icon icon-pencil"></i></a>';

            var aksi_delete = '<a onclick="hapus_modal(' + tran_upgrade[i].id_ekspedisi + ')"><i rel="tooltip" title="Hapus" style="cursor: pointer" class="icon icon-trash"></i></a>'
            var a ='<tr>\n\
                        <td>'+(i+1)+'</td>\n\
                        <td>'+tran_upgrade[i].nama+'</td>\n\
                        <td>'+tran_upgrade[i].id_ekspedisi+'</td>\n\
                        <td>'+tran_upgrade[i].username+'</td>\n\
                        <td>'+tran_upgrade[i].radius+'</td>\n\
                        <td>'+tran_upgrade[i].kontak+'/'+ tran_upgrade[i].kontak_sms + '</td>\n\
                        <td>'+tran_upgrade[i].kontak+'</td>\n\
                        <td>'+aksi_edit + '&nbsp' + aksi_delete + '&nbsp' + aksi_upload + '</td>\n\
                    </tr>';
            
            html = html + a;
        }
        $('#isi_tabel').html(html);
        $("#id_tabel").dataTable();
    }
    function load_level(level){
        var awal = '<option value="">-Pilih Level-</option>';
        var html = "";
        
        for (var i=0;i<level.length;i++){
            var a ='<option value="'+level[i].id+'">'+level[i].nama+'</option>';
           html = html + a;
        }

        $('#id_level').html(awal+html).trigger("liszt:updated");

    }
    // load_opsi(json_role);
</script>

<div id="modal_data" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-header">
        <a type="button" class="close" data-dismiss="modal" aria-hidden="true">x</a>
        <h3 id="">Data Ekspedisi</h3>
    </div>
    <form id="form_data" class="form-horizontal" enctype="multipart/form-data">
            <div id="data-modal-body" class="modal-body">
        <input type="hidden" id="form_id" name="form_id" />
        <span>Apakah Anda yakin akan menghapus ekspedisi ini?</span>
        </div>
        <div class="modal-footer">
            <span id="form_notif"></span>
            <span><img id="form_loading" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>

            <a class="btn" data-dismiss="modal" aria-hidden="true">Batal</a>
            <button type="submit" id="hapus_button" class="btn btn-primary mr-auto"><i class="icon-delete"></i> Hapus</button>
        </div>
    </form>
</div>


<div id="modal_add" class="modal hide fade" role="dialog">
    <div class="modal-dialog" >

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h3>Tambah Ekspedisi</h3>
            </div>
            <div class="modal-body">
                <div class="control-group">
                    <form id="add_form_data" method="post"  enctype="multipart/form-data" accept-charset="utf-8">
                        <div class="control-group">
                            <h4 style="margin-bottom: 20px; margin-top: 20px">DATA EKSPEDISI</h4>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Nama Ekspedisi<span class="apt_bintang">*</span></label>
                            <div class="controls">
                                <input type="text" id="nama" placeholder="Nama Ekspedisi" name="nama" class="input-xlarge" required />
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Upload Gambar<span class="apt_bintang">*</span></label>
                            <div class="controls">
                                <input type="file" id="form_file" name="form_file" required class="input-xlarge">
                            </div>
                        </div>
                        <div class="control-group" style="display: none;">
                            <label class="control-label">Foto<span class="apt_bintang">*</span></label>
                            <div class="controls">
                                <input type="text" id="foto" name="foto" value="foto.png" class="input-xlarge" required />
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
        <div class="modal-footer">
            <span id="modal_data_notif"></span>
            <span><img id="modal_data_loading" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>

            <button class="btn" data-dismiss="modal" aria-hidden="true">Batal</button>
            <button id="btn_tambah" onclick="tambah_ekspedisi()" class="btn btn-danger"><i class="icon-upload"></i> Tambah Ekspedisi</button>
        </div>
    </div>
</div>



<div class="page-header">
    <div class="pull-left">
            <h1>Kelola Ekspedisi</h1>
    </div>
    <div class="pull-right">
        <ul class="stats">
            <li class='lightred'>
                <i class="icon-calendar"></i>
                <div class="details">
                    <span class="big">-, -</span>
                    <span>-, -</span>
                </div>
            </li>
        </ul>
    </div>
</div>
<div class="breadcrumbs">
        <ul>
            <li>
                <a>Manage</a>
                <i class="icon-angle-right"></i>
            </li>
            <li>
                <a>Data Ekspedisi</a>
            </li>
        </ul>
        <div class="close-bread">
                <a href="#"><i class="icon-remove" style="display: none;" ></i></a>
        </div>
</div>

<div class="row-fluid">
    <div class="span12">
    </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <button type="submit" onclick="showAddModal()" class="btn btn-primary"><i class="icon-plus"></i> Tambah</button>
    </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <div class="box box-color box-bordered">
            <div class="box-title">
                <h3><i class="icon-table"></i>Tabel Ekspedisi Usaha</h3>
            </div>
            <div class="box-content nopadding"  style="overflow: scroll">                
                <table class="table usertable table-hover table-nomargin table-bordered" id="id_tabel">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>ID Ekspedisi</th>
                            <th>Foto</th>
                            <th>Nama Ekspedisi</th>
                            <th>Layanan</th>
                            <th>Agen</th>
                            <th>Edit Detail</th>
                        </tr>
                    </thead>
                    <tbody id="isi_tabel">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    //load_upgrade(json_upgrade);
    // load_level(level);

    $("#modal_add").modal("hide");
    function showAddModal() {
        $("#modal_add").modal("show");
    }

    $('#hapus_button').click(function() {
        console.log('clickeddddd');
    });

    function tambah_ekspedisi(){
        $("#btn_tambah").attr("disabled", "disabled");
        $("#modal_data_loading").show();
        var dataPost = new FormData($('#add_form_data')[0]);
        $.ajax({
            url:"<?php echo URL_OPA;?>update_ekspedisi/tambah_ekspedisi",
            data: dataPost,
            type: 'POST',
            dataType: 'json',
            async : true,
            cache : false,
            contentType : false,
            processData : false,
            success: function (data, textStatus, jqXHR) {
                $("#btn_tambah").removeAttr("disabled");
                $("#modal_data_loading").fadeOut(10,function(){
                    datatable_server_side();
                    $("#modal_add").modal("hide");
                });
                
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $("#btn_tambah").removeAttr("disabled");
                console.log("gagal");

            }
        });
    }


    $(document).ready(function(){
       
        $(".chosen-select").chosen({
            no_results_text: "Data tidak ada!",
            width: "100%",
            placeholder_text_single : "Pilih Salah Satu"
        });
    });
    
    function hapus_modal(id_ekspedisi){
        $("#modal_data").modal("show");
        $("#form_id").val(id_ekspedisi);
    }
    
    $("#form_data").submit(function (e){
        e.preventDefault();
        $("#form_loading").show();
        $.ajax({
            url: "<?php echo URL_OPA.'update_ekspedisi/hapus'; ?>",
            data: { 
                    "id_ekspedisi":$("#form_id").val()
                    },
            type: 'POST',
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                console.log("OK");
                $("#form_loading").fadeOut(1000, function (){
                    $("#form_notif").text(data.status);
                    $("#form_notif").show();
                    $("#form_notif").fadeOut(2000, function (){
                        datatable_server_side();
                        $("#modal_data").modal("hide");
                    });
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("NO");
            }
        });
    });
    
    ///////////////////////
    
    var dataTable;
    
    datatable_server_side();
    
    function datatable_server_side(){
        console.log("server");
        if(dataTable != null) dataTable.fnDestroy();


        console.log("ini");
        
        dataTable = $('#id_tabel').dataTable( {
            'searching': false,
            'iDisplayLength': 10,
            "aoColumnDefs": [
                { 'bSortable': false, 'aTargets': [ 0,1,2,3,4,5 ] }
            ],
            "processing": true,
            "serverSide": true,
            "ajax":{
                url :"<?php  echo URL_OPA; ?>update_ekspedisi/get_data", // json datasource
                type: "post",  // method  , by default get
                "data": function ( d ) {
                    
                },
                error: function(){  // error handling
                    $(".id_tabel-error").html("");
                    $("#id_tabel").append('<tbody class="id_tabel-error"><tr><th colspan="6">No data found in the server</th></tr></tbody>');
                    $("#id_tabel_processing").css("display","none");

                },
                "dataSrc": function ( json ) {
                    if (json.pesan == 'session expired') {
                        window.location.href = "<?php echo URL_OPA . 'merchant_login'; ?>";
                    }
                    json_upgrade = json.json;
                    return json.data;
                }
            }
            
        });
		
    }
</script>