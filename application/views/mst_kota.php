<script>
    var kota = <?php echo json_encode($kota)?>;
    var provinsi = <?php echo json_encode($provinsi)?>;
    var status_edit = "0";
    var status_hapus = "0";
    
    function load_opsi(json){
        for(var i=0;i<json.length;i++){
            if(json[i].id_role == "55"){
                $("#opsi_tambah").html('<button class="btn btn-blue tombol_add" onclick="buka_modal()" style="margin: 10px;"><i class="icon-plus"></i> Tambah</button>');
            }else if(json[i].id_role == "56"){
                status_edit = "1";
            }else if(json[i].id_role == "57"){
                status_hapus = "1";
            }
        }   
    }
    
    function load_kota(kota){
        $("#id_tabel").dataTable().fnDestroy();
        var html ="";
        for(var i =0;i<kota.length;i++){
            var aksi_edit = "";
            var aksi_hapus = "";
            
            if(status_edit == "1"){
                aksi_edit = '<a onclick="edit_modal('+i+')"><i rel="tooltip" title="Ubah" style="cursor: pointer" class="icon icon-pencil"></i></a>';
            }if(status_hapus == "1"){
                aksi_hapus = '<a onclick="hapus_modal('+i+')"><i rel="tooltip" title="Hapus" data-placement="bottom" style="cursor: pointer" class="icon icon-trash"></i></a>';
            }
            var a ='<tr>\n\
                        <td>'+(i+1)+'</td>\n\
                        <td>'+kota[i].nama+'</td>\n\
                        <td>'+kota[i].nama_provinsi+'</td>\n\
                        <td>'+aksi_edit+'&nbsp;&nbsp;&nbsp;'+aksi_hapus+'</td>\n\
                    </tr>';
            
            html = html + a;
        }
        $('#isi_tabel').html(html);
        $("#id_tabel").dataTable();
    }
    function load_provinsi(provinsi){
        var awal = '<option value="">-Pilih Provinsi-</option>';
        var html = "";
        
        for (var i=0;i<provinsi.length;i++){
            var a ='<option value="'+provinsi[i].id+'">'+provinsi[i].nama+'</option>';
           html = html + a;
        }

        $('#id_provinsi').html(awal+html).trigger("liszt:updated");

    }
    
    load_opsi(json_role);
</script>

<div id="modal_data" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-header">
        <a type="button" class="close" data-dismiss="modal" aria-hidden="true">x</a>
        <h3 id="">Data Kota</h3>
    </div>
    <form id="form_data" class="form-horizontal" enctype="multipart/form-data">
        <input type="hidden" id="form_id" name="form_id">
        <div class="modal-body">
            <div class="control-group">
                <label class="control-label">Nama Kota<span class="apt_bintang">*</span></label>
                <div class="controls">
                    <input type="text" id="id_nama" name="id_nama" required class="input-xlarge">
                    
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Provinsi<span class="apt_bintang">*</span></label>
                <div class="controls">
                    <div class="input-xlarge">
                        <select id="id_provinsi" name="id_provinsi" class='chosen-select input-xlarge' required>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <span id="form_notif"></span>
            <span><img id="form_loading" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>

            <a class="btn" data-dismiss="modal" aria-hidden="true">Batal</a>
            <button type="submit" class="btn btn-primary"><i class="icon-save"></i> Simpan</button>
        </div>
    </form>
</div>

<div id="modal_data_hapus" class="modal hide fade" role="dialog">
    <div class="modal-dialog" >

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h3>Konfirmasi</h3>
            </div>
            <div class="modal-body">
                <center>
                    <input type="hidden" id="modal_data_hapus_id">
                    <p style="font-size: 15px;">Yakin Menghapus <span id="modal_data_hapus_p" style="font-weight: bold;"></span> ?</p>
                </center>
            </div>

        </div>
        <div class="modal-footer">
            <span id="modal_data_hapus_notif"></span>
            <span><img id="modal_data_hapus_loading" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>

            <button class="btn" data-dismiss="modal" aria-hidden="true">Batal</button>
            <button onclick="hapus()" class="btn btn-danger"><i class="icon-trash"></i> Hapus</button>
        </div>
    </div>
</div>

<div class="page-header">
    <div class="pull-left">
            <h1>Kota</h1>
    </div>
    <div class="pull-right">
        <ul class="stats">
            <li class='lightred'>
                <i class="icon-calendar"></i>
                <div class="details">
                    <span class="big">-, -</span>
                    <span>-, -</span>
                </div>
            </li>
        </ul>
    </div>
</div>

<div class="breadcrumbs">
        <ul>
            <li>
                <a>Master</a>
                <i class="icon-angle-right"></i>
            </li>
            <li>
                <a>Kota</a>
            </li>
        </ul>
        <div class="close-bread">
                <a href="#"><i class="icon-remove" style="display: none;" ></i></a>
        </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <div class="box box-color box-bordered">
            <div class="box-title">
                <h3><i class="icon-table"></i>Tabel Data Kota</h3>
            </div>
            <div class="box-content nopadding"  style="overflow: scroll">
                <div id="opsi_tambah">
                    
                </div>
                
                <table class="table table-hover table-nomargin table-bordered" id="id_tabel">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Kota</th>
                            <th>Provinsi</th>
                            <th>Opsi</th>
                        </tr>
                    </thead>
                    <tbody id="isi_tabel">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    load_kota(kota);
    load_provinsi(provinsi);
    $(document).ready(function(){
        $(".chosen-select").chosen({
            no_results_text: "Data tidak ada!",
            width: "100%",
            placeholder_text_single : "Pilih Salah Satu"
        });
    });
    
    function buka_modal(){
        $("#modal_data").modal("show");
        $("#id_nama").val("");
        $("#id_provinsi").val("").trigger("liszt:updated");
        $("#form_id").val("");
    }
    
    $("#form_data").submit(function (e){
        e.preventDefault();
        $("#form_loading").show();
        $.ajax({
            url: "<?php echo URL_OPA.'mst_kota/insert_kota'; ?>",
            data: { "nama":$("#id_nama").val(),
                    "provinsi":$("#id_provinsi").val(),
                    "id":$("#form_id").val()
                    },
            type: 'POST',
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                console.log("OK");
                $("#form_loading").fadeOut(1000, function (){
                    $("#form_notif").text(data.status);
                    $("#form_notif").show();
                    $("#form_notif").fadeOut(2000, function (){
                        kota=data.list;
                        load_kota(kota);
                        $("#modal_data").modal("hide");
                    });
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("NO");
            }
        });
    });
    
    function edit_modal(i){
        $("#modal_data").modal("show");        
        $("#form_id").val(kota[i].id);
        $("#id_nama").val(kota[i].nama);
        $("#id_provinsi").val(kota[i].id_provinsi).trigger("liszt:updated");
    }
    
    function hapus_modal(i){
        $("#modal_data_hapus_id").val(kota[i].id);
        $("#modal_data_hapus_p").text(kota[i].nama);
        $("#modal_data_hapus").modal("show");
    }
    
    function hapus(){
        $("#modal_data_hapus_loading").show();
        $.ajax({
            url: "<?php echo URL_OPA.'mst_kota/delete_kota'; ?>",
            data: {"id":$("#modal_data_hapus_id").val()},
            type: 'POST',
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $("#modal_data_hapus_loading").fadeOut(1000,function(){
                    $("#modal_data_hapus_notif").text(data.status);
                    $("#modal_data_hapus_notif").show();
                    $("#modal_data_hapus_notif").fadeOut(2000, function (){
                        kota=data.list;
                        load_kota(kota);
                        $("#modal_data_hapus").modal("hide");
                    });
                    
                });
                
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("gagal");

            }
        });
    }
</script>