<script></script>
<style type="text/css">
    #map {
        height: 1200px;
        width: 100%;
        position: relative;
    }
   #wrapper { position: relative; }
   #over_map { 
        background-color: white; position: 
        absolute; 
        top: 50px; 
        left: 10px; 
        z-index: 99; 
        padding: 10px; 
        width: 450px;
    }
    .option_tab {
        display:inline-block;
        margin-right: 10px;
        *display:inline;/* For IE7 */
        *zoom:1;/* For IE7 */
        white-space:normal;
        font-size:13px;
        vertical-align:top;
    }

    .controls-pac {
        margin-top: 10px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 366;
        margin-left: 0px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 366px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      .pac-container {
        font-family: Roboto;
      }

      #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
      }

      #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }
      #target {
        width: 345px;
      }
</style>
<style>
</style>

<div class="page-header">
    <div class="pull-left">
        <h1>Ekspedisi</h1>
    </div>
    <div class="pull-right">
        <ul class="stats">
            <li class='lightred'>
                <i class="icon-calendar"></i>
                <div class="details">
                    <span class="big">-, -</span>
                    <span>-, -</span>
                </div>
            </li>
        </ul>
    </div>
</div>
<div class="breadcrumbs">
    <ul>
        <li>
            <a>Daftar Ekspedisi</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a>Tambah Baru</a>
        </li>
    </ul>
    <div class="close-bread">
        <a href="#"><i class="icon-remove" style="display: none;" ></i></a>
    </div>
</div>
<div class="row-fluid">
    <div class="span12">
        <div class="box box-color box-bordered">
            <div class="box-title">
                <h3><i class="icon-table"></i>Tambah Ekspedisi</h3>
            </div>
            <div class="box-content nopadding" id="form_layout" style="overflow: scroll; height: 100%">
                <div class="span8" id="wrapper" >

                                        <input type="text" id="pac-input" name="pac-input" placeholder="Nama Tempat..." class="span12 controls-pac">
                    <div id="map">
                        
                    </div>

                                <!-- <div class="row-fluid" id="shortlinkform_input">
                                    <div class="span12">
                                    </div>
                                </div> -->
                     <div id="over_map">
                        <div class="row-fluid">
                            <div class="span12">
                                <h5><b>Set Lokasi Ekspedisi pada Map </b></h5>
                            </div>
                        </div> 
                        <div class="row-fluid" style="margin-bottom: 10px;">
                            <span class="option_tab">
                                <a id="sharelink_button" class="link_button">
                                <span id="sharelink_wrap" class="link_wrap">Share Link</span></a>
                            </span>
                            <!-- <span class="option_tab">
                                <a id="shortlink_button" class="link_button">
                                <span id="shortlink_wrap" class="link_wrap">Nama Tempat</span></a>
                            </span> -->
                            <span class="option_tab">
                                <a id="latlnglink_button" class="link_button">
                                <span id="latlnglink_wrap" class="link_wrap">Latitude/Longitude</span></a>              
                            </span>
                        </div>
                        <div class="row-fluid">
                            <div class="span9">
                                <div class="span12" id="sharelinkform_input">
                                    <input type="text" id="sharelink_input" placeholder="Share Link" name="sharelink_input" class="span12">
                                </div>
                                <div class="row-fluid" id="latlngform_input">
                                    <div class="span12">
                                        <input type="text" id="lat" name="lat" placeholder="Latitude" class="span6 input-xlarge location-input">
                                        <input type="text" id="lng" style="margin-left: 5px;" name="lng" placeholder="Longitude" name="lng" class="input-xlarge location-input span6" required />
                                    </div>
                                </div>
                            </div>
                            <button id="search_button" class="btn btn-primary span3">Cari</button>
                        </div>
                        <div class="row-fluid">
                            <span class="span12">Atau Anda dapat mengaturnya secara manual dengan mengklik pada peta
                            </span>
                        </div>
                     </div>
                </div>
                <form id="form_data" class="span4" enctype="multipart/form-data" accept-charset="utf-8">
                    <div class="control-group">
                        <h4 style="margin-bottom: 20px; margin-top: 20px">LOGIN EKSPEDISI</h4>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Nomor Handphone Login<span class="apt_bintang">*</span></label>
                        <div class="controls">
                            <input type="text" id="kontak" placeholder="Nomor untuk Login handset" name="kontak" class="input-xlarge" required />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Kata Sandi<span class="apt_bintang">*</span></label>
                        <div class="controls">
                            <input type="password" id="password" placeholder="Kata Sandi untuk Login" name="password" class="input-xlarge" required />
                        </div>
                    </div>
                    <div class="control-group">
                        <h4 style="margin-bottom: 20px; margin-top: 20px">DATA TEKNIS EKSPEDISI</h4>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Alias</label>
                        <div class="controls">
                            <input type="text" id="username" required name="username" placeholder="ID Number atau Alias versi ekspedisi Anda, jika ada" required class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">No HP notifikasi SMS</label>
                        <div class="controls">
                            <input type="text" id="kontak_sms" placeholder="Nomor untuk notifikasi order via SMS, jika ada" name="kontak_sms" class="input-xlarge" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">No HP kedua notifikasi SMS</label>
                        <div class="controls">
                            <input type="text" id="kontak_sms2" placeholder="Nomor kedua untuk notifikasi order via SMS, jika ada" name="kontak_sms2" class="input-xlarge" />
                        </div>
                    </div>
                    <div class="control-group">
                        <h4 style="margin-bottom: 20px; margin-top: 20px">LOKASI ADMINISTRASI USAHA</h4>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Provinsi<span class="apt_bintang">*</span></label>
                        <div class="controls">
                            <div class="input-xlarge" id="provinsi_form">
                                <select id="id_provinsi" name="id_provinsi" class='chosen-select input-xlarge' required>
                        </select>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Kota<span class="apt_bintang">*</span></label>
                        <div class="controls">
                            <div class="input-xlarge">
                                <select id="id_kota" name="id_kota" class='chosen-select input-xlarge' required>
                        </select>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <h4 style="margin-bottom: 20px; margin-top: 20px">PROFIL EKSPEDISI</h4>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Nama Usaha<span class="apt_bintang">*</span></label>
                        <div class="controls">
                            <input type="text" id="nama" name="nama" placeholder="Nama usaha Anda" required class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group" id="description_form">
                        <label class="control-label">Alamat Usaha</label>
                        <div class="controls">
                            <input type="text" id="alamat" placeholder="Alamat Lengkap Usaha" name="alamat" class="input-xlarge" required />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Jargon</label>
                        <div class="controls">
                            <input type="text" id="jargon" placeholder="Misal: Serba Ada, Serba Murah" name="jargon" required class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group" style="display: none;">
                        <label class="control-label">Radius Antar<span class="apt_bintang">*</span></label>
                        <div class="controls input-postpend">
                            <input type="number" min="1" id="radius" name="radius" value="1000" required class="input">&nbsp<span class="add-on">meter</span>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Minimum Pembelian</label>
                        <div class="controls input-prepend">
                            <span class="add-on">Rp</span>&nbsp<input type="number" id="min_antar" name="min_antar" required class="input">
                        </div>
                    </div>
                    <div class="control-group" style="display: none;">
                        <label class="control-label">Ongkos Kirim</label>
                        <div class="controls input-prepend">
                            <span class="add-on">Rp</span>&nbsp<input type="number" min="0" value="0" id="fee" name="fee" required class="input">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Upload Gambar Ekspedisi/Bangunan Usaha<span class="apt_bintang">*</span></label>
                        <div class="controls">
                            <input type="file" id="form_file" name="form_file" required class="input-xlarge">
                        </div>
                    </div>
                    
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary"><i class="icon-add"></i> Simpan</button>
                        <span id="form_notif"></span>
                        <span><img id="form_loading" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBuDrC5iMEnz7vgrcXHSwKqqPpmHCVrp-c&callback=initMap&libraries=places"></script>
<script>
    var currentMarker;
    var map;
    var lat;
    var lng;
    var id_provinsi;
    var id_kota;
    var markersArray = new Array();
    var search_mode = 0;

    function sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

        $('#sharelink_button').contents().unwrap();
        $('#latlnglink_button').contents().unwrap();
        $('#shortlink_wrap').wrap('<a id="shortlink_button" style="cursor: pointer;">');
        $('#latlnglink_wrap').wrap('<a id="latlnglink_button" style="cursor: pointer;">');
        $('#sharelink_button').contents().unwrap();

        $('#sharelinkform_input').show();
        // $('#shortlinkform_input').hide();
        $('#latlngform_input').hide();

    $('span.option_tab').on('click', '#sharelink_button', function() {
        console.log('sharelink_button');
        $('#sharelink_button').contents().unwrap();
        $('#latlnglink_button').contents().unwrap();
        $('#shortlink_wrap').wrap('<a id="shortlink_button" style="cursor: pointer;">');
        $('#latlnglink_wrap').wrap('<a id="latlnglink_button" style="cursor: pointer;">');
        $('#sharelink_button').contents().unwrap();

        $('#sharelinkform_input').show();
        // $('#shortlinkform_input').hide();
        $('#latlngform_input').hide();
        search_mode = 0;
    });

    $('span.option_tab').on('click', '#shortlink_button',  function() {
        console.log('shortlink_button');
        $('#sharelink_button').contents().unwrap();
        $('#latlnglink_button').contents().unwrap();
        $('#sharelink_wrap').wrap('<a id="sharelink_button" style="cursor: pointer;">');
        $('#latlnglink_wrap').wrap('<a id="latlnglink_button" style="cursor: pointer;">');
        $('#shortlink_button').contents().unwrap();

        $('#sharelinkform_input').hide();
        $('#shortlinkform_input').show();
        $('#latlngform_input').hide();
        search_mode = 1;
    });

    $('span.option_tab').on('click', '#latlnglink_button', function() {
        console.log('latlnglink_button');
        $('#sharelink_button').contents().unwrap();
        $('#shortlink_button').contents().unwrap();
        $('#sharelink_wrap').wrap('<a id="sharelink_button" style="cursor: pointer;">');
        $('#shortlink_wrap').wrap('<a id="shortlink_button" style="cursor: pointer;">');
        $('#latlnglink_button').contents().unwrap();

        $('#sharelinkform_input').hide();
        // $('#shortlinkform_input').hide();
        $('#latlngform_input').show();
        search_mode = 2;
    });

    function initMap() {
        var latlng = {
            lat: -6.175421,
            lng: 106.827334
        };
        lat = latlng.lat;
        lng = latlng.lng;
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 16,
            center: latlng
        });

        addNewMarker(latlng);

        google.maps.event.addListener(map, 'click', function(event) {
            placeMarker(event.latLng);
        });

        function placeMarker(location) {
            lat = location.lat();
            lng = location.lng();
            console.log(lat + ", " + lng);
            $('#lat').val(lat + '');
            $('#lng').val(lng + '');

            currentMarker.setPosition(new google.maps.LatLng(lat, lng));
        }
        
        sleep(1000);
        load_pemilik();


        
        $('#search_button').click(function() {
            if (search_mode == 0) {
                var url = $('#sharelink_input').val();
                var regex = new RegExp('@(.*),(.*),');
                var lon_lat_match = url.match(regex);
                lat = lon_lat_match[1];
                lng = lon_lat_match[2];
                console.log(lat + ", " + lng);
                if (currentMarker !=null) {
                    currentMarker.setPosition(new google.maps.LatLng(lat, lng));
                } else {
                    var latlng = {
                        lat: lat,
                        lng: lng
                    };
                    addNewMarker(latlng)
                }
                var center = new google.maps.LatLng(lat, lng);
                // using global variable:
                map.panTo(center);
            } else if (search_mode == 1) { 
            } else if (search_mode == 2) {
                lat = parseFloat($('#lat').val() != null &&  $('#lat').val() != '' ? $('#lat').val() : 0.0);
                lng = parseFloat($('#lng').val() != null &&  $('#lng').val() != '' ? $('#lng').val() : 0.0);
                if (currentMarker !=null) {
                    currentMarker.setPosition(new google.maps.LatLng(lat, lng));
                } else {
                    var latlng = {
                        lat: lat,
                        lng: lng
                    };
                    addNewMarker(latlng)
                }
                var center = new google.maps.LatLng(lat, lng);
                // using global variable:
                map.panTo(center);
            }
        });

        initAutocomplete();
    
    }

    function addNewMarker(latlng) {
        currentMarker = new google.maps.Marker({
            position: latlng,
            map: map,
            animation: google.maps.Animation.DROP
        });
    }

    $(document).ready(function() {
        $(".chosen-select").chosen({
            no_results_text: "Data tidak ada!",
            width: "100%",
            placeholder_text_single: "Pilih Salah Satu"
        });


        $('#id_provinsi').change(function() {
            id_provinsi = $('#id_provinsi').val();
            load_kota(id_provinsi);
        });

        $('#id_kota').change(function() {
            id_kota = $('#id_kota').val();
        });

    });

    function hide_show_forms(e) {
        if (e == 1) {
            $('#form_for_usaha').show();
            $('#code_count_form').prop('required', true);
        } else {
            $('#form_for_usaha').hide();
            $('#code_count_form').prop('required', false);
        }
    }

    function hide_show_code_form() {
        var e = $('#target').val();
        if (e == 1) {
            $('#code_count_form').show();
        } else {
            $('#code_count_form').hide();
        }
    }

    var id_ekspedisies = [];

    function load_pemilik() {
        var awal = '';
        $.ajax({
            url: "<?php echo URL_OPA.'update_pusaha/get_preactive_pemilik'; ?>",
            data: "",
            type: 'GET',
            dataType: 'json',
            success: function(data, textStatus, jqXHR) {
                var html = "";

                console.log(data);

                for (var i = 0; i < data.data.length; i++) {

                    var lat = parseFloat(data.data[i].lat);
                    var lng = parseFloat(data.data[i].lng);

                    var latlng = {
                        lat: lat,
                        lng: lng
                    };
                    var newMarker = new google.maps.Marker({
                        position: latlng,
                        map: map,
                        animation: google.maps.Animation.DROP
                    });
                    newMarker.setIcon('<?php echo URL_IMG . "plugins/gmap/"; ?>' + 'green-dot.png');

                    var contentString = "<b>" + data.data[i].nama + "</b><br>" + data.data[i].alamat;
                    var infowindow = new google.maps.InfoWindow();
                    
                    google.maps.event.addListener(newMarker,'click', (function(newMarker,contentString,infowindow){ 
                        return function() {
                            infowindow.setContent(contentString);
                            infowindow.open(map,newMarker);
                        };
                    })(newMarker,contentString,infowindow));  

                    sleep(500);

                }

                id_ekspedisies = data.data;
            },
            error: function(jqXHR, textStatus, errorThrown) {

            }
        });
    }


    var id_provinsies = [];

    function load_provinsi() {
        var awal = '';
        $.ajax({
            url: "<?php echo URL_OPA.'mobile/api_pemilik/get_provinsi'; ?>",
            data: "",
            type: 'GET',
            dataType: 'json',
            success: function(data, textStatus, jqXHR) {
                var html = "";

                console.log(data);

                for (var i = 0; i < data.data.length; i++) {
                    var a = '<option value="' + data.data[i].id + '">' + data.data[i].nama + '</option>';
                    html = html + a;
                }
                id_provinsies = data.data;
                $('#id_provinsi').html(awal + html).trigger("liszt:updated");
                if (data.data.length > 0) {
                    load_kota(data.data[0].id);
                    id_provinsi = data.data[0].id;
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {

            }
        });
    }
    load_provinsi();

    var id_kotaes = [];

    function load_kota(id_provinsi) {
        var awal = '';
        console.log('id_provinsi = ' + id_provinsi);
        $.ajax({
            url: "<?php echo URL_OPA.'mobile/api_pemilik/get_kota'; ?>",
            data: "id_provinsi=" + id_provinsi,
            type: 'POST',
            dataType: 'json',
            success: function(data, textStatus, jqXHR) {
                var html = "";

                console.log(data);

                for (var i = 0; i < data.data.length; i++) {
                    var a = '<option value="' + data.data[i].id + '">' + data.data[i].nama + '</option>';
                    html = html + a;
                }
                id_kotaes = data.data;
                $('#id_kota').html(awal + html).trigger("liszt:updated");
                if (data.data.length > 0) {
                    id_kota = data.data[0].id;
                }

            },
            error: function(jqXHR, textStatus, errorThrown) {

            }
        });
    }

    var id_jenises = [];

    function load_jenis() {
        var awal = '';
        $.ajax({
            url: "<?php echo URL_OPA.'mobile/api_pemilik/get_jenis_kategori_usaha'; ?>",
            data: "",
            type: 'GET',
            dataType: 'json',
            success: function(data, textStatus, jqXHR) {
                var html = "";

                console.log(data);

                for (var i = 0; i < data.data.length; i++) {
                    var a = '<option value="' + data.data[i].id_jenis + '">' + data.data[i].nama + '</option>';
                    html = html + a;
                }
                id_jenises = data.data;
                $('#id_jenis').html(awal + html).trigger("liszt:updated");

            },
            error: function(jqXHR, textStatus, errorThrown) {

            }
        });
    }
    load_jenis();

    var id_kategories = [];

    function load_kategori() {
        var awal = '';
        $.ajax({
            url: "<?php echo URL_OPA.'mobile/api_pemilik/get_kategori_usaha'; ?>",
            data: "",
            type: 'GET',
            dataType: 'json',
            success: function(data, textStatus, jqXHR) {
                var html = "";

                console.log(data);

                for (var i = 0; i < data.data.length; i++) {
                    var a = '<option value="' + data.data[i].id_kategori + '">' + data.data[i].nama_kategori + '</option>';
                    html = html + a;
                }
                id_kategories = data.data;
                $('#id_kategori').html(awal + html).trigger("liszt:updated");

            },
            error: function(jqXHR, textStatus, errorThrown) {

            }
        });
    }
    load_kategori();


    var id_kategories_produk = [];

    function load_kategori_produk() {
        var awal = '';
        $.ajax({
            url: "<?php echo URL_OPA.'mobile/api_pemilik/get_kategori_produk'; ?>",
            data: "",
            type: 'GET',
            dataType: 'json',
            success: function(data, textStatus, jqXHR) {
                var html = "";

                console.log(data);

                for (var i = 0; i < data.data.length; i++) {
                    var a = '<option value="' + data.data[i].id_kategori + '">' + data.data[i].nama_kategori + '</option>';
                    html = html + a;
                }
                id_kategories_produk = data.data;
                $('#id_kategori_produk').html(awal + html).trigger("liszt:updated");

            },
            error: function(jqXHR, textStatus, errorThrown) {

            }
        });
    }
    load_kategori_produk();


    var id_radiuses = [];

    function load_radius() {
        var awal = '';
        $.ajax({
            url: "<?php echo URL_OPA.'mobile/api_pemilik/get_radius'; ?>",
            data: "",
            type: 'GET',
            dataType: 'json',
            success: function(data, textStatus, jqXHR) {
                var html = "";

                console.log(data);

                for (var i = 0; i < data.data.length; i++) {
                    var a = '<option value="' + data.data[i].id_radius + '">' + data.data[i].radius + ' m</option>';
                    html = html + a;
                }
                id_radiuses = data.data;
                $('#id_radius').html(awal + html).trigger("liszt:updated");

            },
            error: function(jqXHR, textStatus, errorThrown) {

            }
        });
    }
    load_radius();

    $("#form_data").submit(function(e) {
        e.preventDefault();
        currentMarker.setMap(null);
        var latlng = {
            lat: lat,
            lng: lng
        };
        var newMarker = new google.maps.Marker({
            position: latlng,
            map: map,
            animation: google.maps.Animation.DROP
        });
        newMarker.setIcon('<?php echo URL_IMG . "plugins/gmap/"; ?>' + 'green-dot.png');

        var contentString = "<b>" + $('#nama').val() + "</b><br>" + $('#alamat').val();
        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });
        
        infowindow.open(map, newMarker);
        newMarker.addListener('click', function() {
            infowindow.open(map, newMarker);
        });

        if (window.File && window.FileReader && window.FileList && window.Blob)
        {
            //get the file size and file type from file input field
            var fsize = $('#form_file')[0].files[0].size;
            
            if(fsize >1048576) //do something if file size more than 1 mb (1048576)
            {
                alert("Ukuran file tidak boleh lebih dari 1 MB");
                return;
            }
        }else{
            alert("Mohon perbarui browser Anda karena tidak cocok dengan sebagian fitur kami");
        }

        $("#form_loading").show();
        var dataPost = new FormData($(this)[0]);
        // dataPost = dataPost + "&lat=" + lat + "&lng=" + lng + "&id_provinsi=" + id_provinsi + "&id_kota=" + id_kota;
        dataPost.append("lat", lat);
        dataPost.append("lng", lng);
        dataPost.append("id_provinsi", id_provinsi);
        dataPost.append("id_kota", id_kota);
        $.ajax({
            url: "<?php echo URL_OPA.'update_pusaha/create'; ?>",
            data: dataPost,
            type: 'POST',
            dataType: 'json',
            async : false,
            cache : false,
            contentType : false,
            processData : false,
            success: function(data, textStatus, jqXHR) {
                console.log("OK");
                if (data.code == 1) {
                    markersArray.push(newMarker);
                    var latlng = {
                        lat: -6.175421,
                        lng: 106.827334
                    };
                    addNewMarker(latlng)
                    $('#form_data').find("input[type=text], textarea").val("");
                    $("#form_loading").fadeOut(1000, function() {
                        $("#form_notif").text(data.status);
                        $("#form_notif").show();
                        $("#form_notif").fadeOut(2000, function() {});
                    });
                } else {
                        $("#form_notif").text(data.status);
                        $("#form_notif").show();
                        $("#form_loading").hide();
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("NO");
            }
        });
    });

    function initAutocomplete() {
        

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }

          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
            }));

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
      }
</script>