<!doctype html>
<html>
<head>
	<?php $this->load->view('head'); ?>
    <script>
    
        var json_role = <?php echo json_encode($role);?>;
    
    </script>
</head>

<body>
    <?php $this->load->view('menu'); ?>
    
	<div class="container-fluid" id="content">
            <div id="main" style="margin-left: 0px;">
                <div id="load" class="loader"></div>
                <div class="container-fluid" id="isi">
                    <?php $this->load->view('beranda'); ?>

                </div>
            </div>        
        </div>
    
<div id="modal_ubahpass" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-header">
        <a type="button" class="close" data-dismiss="modal" aria-hidden="true">x</a>
        <h3>Ubah Password</h3>
    </div>
    <form id="form_ubahpass" class="form-horizontal" enctype="multipart/form-data">
        <div class="modal-body">
            <div class="control-group" style="margin-left: 15%;">
                <div class="input-append">
                    <input class="input-xlarge" required id="n_pass" type="password" placeholder="New Password"><span class="add-on"><i class="icon-key"></i></span>
                </div>
            </div>
            <div class="control-group" style="margin-left: 15%;">
                <div class="input-append">
                    <input class="input-xlarge" required id="rn_pass" type="password" placeholder="Retype new password"><span class="add-on"><i class="icon-key"></i></span>
                </div>
                <p id="notif_npwd" style="display: none;">Pastikan password baru anda sama</p>
            </div>
            <hr>
            <div class="control-group" style="margin-left: 15%;">
                <div class="input-append">
                    <input class="input-xlarge" required id="o_pass" type="password" placeholder="Old Password"><span class="add-on"><i class="icon-key"></i></span>
                </div>
                <p id="notif_opwd" style="display: none;">Password salah!</p>
            </div>
        </div>
        <div class="modal-footer">
            <span id="notif_form_upwd"></span>
            <span><img id="load_form_upwd" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>
            <a class="btn" data-dismiss="modal" aria-hidden="true">Batal</a>
            <button type="submit" class="btn btn-primary"><i class="icon-save"></i> Simpan</button>
        </div>
    </form>
</div>
    
<div id="modal_sunting" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-header">
        <a type="button" class="close" data-dismiss="modal" aria-hidden="true">x</a>
        <h3>Ubah Profil</h3>
    </div>
    <form id="form_sunting" class="form-horizontal" enctype="multipart/form-data">
        <div class="modal-body">
            <div class="control-group">
                <label class="control-label">Nama<span class="apt_bintang">*</span></label>
                <div class="controls">
                    <input type="text" id="id_nama_sunting" name="id_nama_sunting" required class="input-xlarge">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Username<span class="apt_bintang">*</span></label>
                <div class="controls">
                    <input type="text" id="id_username_sunting" name="id_username_sunting" required class="input-xlarge">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Email<span class="apt_bintang">*</span></label>
                <div class="controls">
                    <input type="email" id="id_email_sunting" name="id_email_sunting" required class="input-xlarge">
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <span id="notif_form_sunting"></span>
            <span><img id="load_form_sunting" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>
            <a class="btn" data-dismiss="modal" aria-hidden="true">Batal</a>
            <button type="submit" class="btn btn-primary"><i class="icon-save"></i> Simpan</button>
        </div>
    </form>
</div>
		
</body>

<script>
    var head_pengguna = <?php echo json_encode($pengguna);?>;
    
    function buka_halaman(a,angka){
var dir ="<?php echo URL_OPA; ?>"+a;
        $("#load").show();
        $(".menu_main").removeClass("active");
        $("#menu_main"+angka).addClass("active");
        
        //alert('ijdj');
        $.ajax({ 
            url: dir,
            success: function(response){
  	    $('#isi').html(response);
            $("#load").hide();
        
  	 }, 
  	 error: function(response){ 
  	 	alert('salah'+response.toString());
              //  $('#status').html(response);
              $("#load").hide();
  	  } 
  	}); 
    }
    
    function showgantipass(){
        $("#n_pass").val("");
        $("#rn_pass").val("");
        $("#o_pass").val("");
        $("#modal_ubahpass").modal("show");
    }
    
    $("#form_ubahpass").submit(function (e){
        e.preventDefault();
        $("#notif_npwd").hide();
        $("#notif_opwd").hide();
        $("#notif_form_upwd").hide();
        $("#load_form_upwd").show();
        $.ajax({
            url:"<?php echo URL_OPA;?>mainpage/ubah_pwd/",
            data: {"new_pass":$("#n_pass").val(),
                   "rnew_pass":$("#rn_pass").val(),
                   "old_pass":$("#o_pass").val()
                  },
            type: 'POST',
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                console.log(data.status);
                $("#load_form_upwd").fadeOut(1000, function (){
                    if(data.status == "satu"){
                        $("#notif_npwd").fadeIn(1000);
                    }else if(data.status == "dua"){
                        $("#notif_opwd").fadeIn(1000);
                    }else{
                        $("#notif_form_upwd").text(data.status);
                        $("#notif_form_upwd").fadeIn(1000);
                        $("#notif_form_upwd").fadeOut(2000, function (){
                            $("#modal_ubahpass").modal("hide");
                        });
                    }
                });
                       
                },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR.status);

                }
        });
    });
    
    function showsunting(){
        $("#id_nama_sunting").val(head_pengguna[0].nama);
        $("#id_username_sunting").val(head_pengguna[0].username);
        $("#id_email_sunting").val(head_pengguna[0].email);
        $("#modal_sunting").modal("show");
    }
    
    $("#form_sunting").submit(function (e){
        e.preventDefault();
        $("#notif_form_sunting").hide();
        $("#load_form_sunting").show();
        $.ajax({
            url:"<?php echo URL_OPA;?>mainpage/sunting_profil/",
            data: {"nama":$("#id_nama_sunting").val(),
                   "username":$("#id_username_sunting").val(),
                   "email":$("#id_email_sunting").val()
                  },
            type: 'POST',
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $("#load_form_sunting").fadeOut(1000, function (){
                    if(data.stat == "1"){
                        $("#notif_form_sunting").text(data.status);
                        $("#notif_form_sunting").fadeIn(1000);
                    }else if(data.stat == "2"){
                        $("#notif_form_sunting").text(data.status);
                        $("#notif_form_sunting").fadeIn(1000);
                    }else{
                        $("#notif_form_sunting").text(data.status);
                        $("#notif_form_sunting").fadeIn(1000);
                        $("#notif_form_sunting").fadeOut(2000, function (){
                            $("#id_nama_user").html(data.namaad);
                            head_pengguna = data.list;
                            $("#modal_sunting").modal("hide");
                        });
                    }
                });
                       
                },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR.status);

                }
        });
    });
</script>
</html>