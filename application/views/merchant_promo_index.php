<script>
    var active_promo = <?php echo json_encode($active_promo)?>;
    
    function load_promo(active_promo){
        var html ="";
        var tipe ="";
        for(var i =0;i<active_promo.length;i++){
            // if (active_promo[i].promo_for == 2) {
                aksi_edit = "<a onclick=\"buka_halaman('merchant_promo/edit/"+active_promo[i].id+"', '3')\"><i rel=\"tooltip\" title=\"Detail\" style=\"cursor: pointer\" class=\"icon icon-pencil\"></i></a>";
            // }
            aksi_hapus = '<a onclick="hapus_modal('+i+')"><i rel="tooltip" title="Hapus" style="cursor: pointer" class="icon icon-trash"></i></a>&nbsp ';
            var jenis = 'Free Items';
            switch (active_promo[i].tipe) {
                case '1':
                jenis = "Diskon Harga";
                break;
                case '2':
                jenis = "Free Items";
                break;
            }
            var a ='<tr>\n\
            <td>'+(i+1)+'</td>\n\
            <td>'+active_promo[i].nama+'</td>\n\
            <td>'+jenis+'</td>\n\
            <td><a onclick=\'buka_halaman("merchant_promo/banners/' + active_promo[i].id + '", "3")\' style=\'cursor: pointer;\'>Atur</a></td>\n\
            <td><a onclick=\'buka_halaman("merchant_promo/produk_promo/' + active_promo[i].id + '", "3")\' style=\'cursor: pointer;\'>Lihat</a> </td>\n\
            <td>'+active_promo[i].start_date+' s.d. ' + active_promo[i].end_date + '</td>\n\
            <td>'+aksi_edit + '&nbsp&nbsp&nbsp' + aksi_hapus+'</td>\n\</tr>';

            html = html + a;
        }
        $('#isi_tabel').html(html);
        $("#id_tabel").dataTable();
    }

    load_promo(active_promo);
</script>
<div id="modal_data_hapus" class="modal hide fade" role="dialog">
    <div class="modal-dialog" >

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h3>Konfirmasi</h3>
            </div>
            <div class="modal-body">
                <center>
                    <input type="hidden" id="modal_data_hapus_id">
                    <p style="font-size: 15px;">Yakin Menghapus <span id="modal_data_hapus_p" style="font-weight: bold;"></span> ?</p>
                </center>
            </div>

        </div>
        <div class="modal-footer">
            <span id="modal_data_hapus_notif"></span>
            <span><img id="modal_data_hapus_loading" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>

            <button class="btn" data-dismiss="modal" aria-hidden="true">Batal</button>
            <button onclick="hapus()" class="btn btn-danger"><i class="icon-trash"></i> Hapus</button>
        </div>
    </div>
</div>


<div id="modal_data" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-header">
        <a type="button" class="close" data-dismiss="modal" aria-hidden="true">x</a>
        <h3 id="">Data Pemilik Usaha</h3>
    </div>
    <form id="form_data" class="form-horizontal" enctype="multipart/form-data">
        <input type="hidden" id="form_id" name="form_id">
        <div class="modal-body">
            <div class="control-group">
                <label class="control-label">Nama<span class="apt_bintang">*</span></label>
                <div class="controls">
                    <input type="text" disabled id="id_nama" name="id_nama" required class="input-xlarge">
                    
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Jenis<span class="apt_bintang">*</span></label>
                <div class="controls">
                    <div class="input-xlarge">
                        <input type="text" disabled id="id_jenis" name="id_jenis" required class="input-xlarge">
                    </div>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Level<span class="apt_bintang">*</span></label>
                <div class="controls">
                    <div class="input-xlarge">
                        <select id="id_level" name="id_level" class='chosen-select input-xlarge' required>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <span id="form_notif"></span>
            <span><img id="form_loading" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>

            <a class="btn" data-dismiss="modal" aria-hidden="true">Batal</a>
            <button type="submit" class="btn btn-primary"><i class="icon-save"></i> Simpan</button>
        </div>
    </form>
</div>

<div class="page-header">
    <div class="pull-left">
        <h1>Promo</h1>
    </div>
    <div class="pull-right">
        <ul class="stats">
            <li class='lightred'>
                <i class="icon-calendar"></i>
                <div class="details">
                    <span class="big">-, -</span>
                    <span>-, -</span>
                </div>
            </li>
        </ul>
    </div>
</div>
<div class="breadcrumbs">
    <ul>
        <li>
            <a>Promo</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a>Promo Saat Ini</a>
        </li>
    </ul>
    <div class="close-bread">
        <a href="#"><i class="icon-remove" style="display: none;" ></i></a>
    </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <div class="box box-color box-bordered">
            <div class="box-title">
                <h3><i class="icon-table"></i>Daftar promo</h3>
            </div>
            <div class="box-content nopadding"  style="overflow: scroll">                
                <table class="table table-hover table-nomargin table-bordered" id="id_tabel">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Promo</th>
                            <th>Tipe</th>
                            <th>Banner</th>
                            <th>Data Promo</th>
                            <th>Durasi Promo</th>
                            <th>Edit Promo</th>
                        </tr>
                    </thead>
                    <tbody id="isi_tabel">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    //load_upgrade(json_upgrade);
    $(document).ready(function(){

        $(".chosen-select").chosen({
            no_results_text: "Data tidak ada!",
            width: "100%",
            placeholder_text_single : "Pilih Salah Satu"
        });
    });
    
    function edit_modal(i){
        $("#modal_data").modal("show");
        $("#form_id").val(json_upgrade[i].id_pemilik);
        $("#id_nama").val(json_upgrade[i].nama);
        $("#id_jenis").val(json_upgrade[i].nama_jenis);
        $("#id_level").val(json_upgrade[i].level).trigger("liszt:updated");
    }

    function hapus_modal(i){
        $("#modal_data_hapus_id").val(active_promo[i].id);
        $("#modal_data_hapus_p").text(active_promo[i].nama);
        $("#modal_data_hapus").modal("show");
    }

    function hapus(){
        $("#modal_data_hapus_loading").show();
        $.ajax({
            url: "<?php echo URL_OPA.'merchant_promo/delete'; ?>",
            data: {"id":$("#modal_data_hapus_id").val()},
            type: 'POST',
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                buka_halaman('merchant_promo', '3');
                $("#modal_data_hapus_loading").fadeOut(100,function(){
                    $("#modal_data_hapus_notif").text(data.status);
                    $("#modal_data_hapus_notif").show();
                    $("#modal_data_hapus_notif").fadeOut(200, function (){
                        $("#modal_data_hapus").modal("hide");
                    });
                    
                });
                
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("gagal");

            }
        });
    }
    
    ///////////////////////
    
    var dataTable;
    
    // datatable_server_side();
    
    function datatable_server_side(){
        console.log("server");
        if(dataTable != null)dataTable.fnDestroy();
        
        dataTable = $('#id_tabel').dataTable( {
            'iDisplayLength': 10,
            "aoColumnDefs": [
            { 'bSortable': false, 'aTargets': [ 0,1,2,3,4,5 ] }
            ],
            "processing": true,
            "serverSide": true,
            "ajax":{
                url :"<?php  echo URL_OPA; ?>promo/get_data", // json datasource
                type: "post",  // method  , by default get
                "data": function ( d ) {

                },
                error: function(){  // error handling
                    $(".id_tabel-error").html("");
                    $("#id_tabel").append('<tbody class="id_tabel-error"><tr><th colspan="6">No data found in the server</th></tr></tbody>');
                    $("#id_tabel_processing").css("display","none");

                },
                "dataSrc": function ( json ) {
                    json_upgrade = json.json;
                    return json.data;
                }
            }
            
        });

    }
</script>