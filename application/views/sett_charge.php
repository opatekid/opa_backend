<script>
    var json_charge = <?php echo json_encode($charge)?>;
</script>
<div class="page-header">
    <div class="pull-left">
        <h1>Default Biaya Layanan Pemilik Usaha</h1>
    </div>
    <div class="pull-right">
        <ul class="stats">
            <li class='lightred'>
                <i class="icon-calendar"></i>
                <div class="details">
                    <span class="big">-, -</span>
                    <span>-, -</span>
                </div>
            </li>
        </ul>
    </div>
</div>
<div class="breadcrumbs">
    <ul>
        <li>
            <a>Setting</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a>Default Biaya Layanan</a>
        </li>
    </ul>
    <div class="close-bread">
            <a href="#"><i class="icon-remove" style="display: none;" ></i></a>
    </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <div class="box box-color box-bordered">
            <div class="box-title">
                <h3><i class="icon-table"></i>Setting Biaya Layanan Pemilik Usaha</h3>
            </div>
            <div class="box-content nopadding"  style="overflow: scroll">
                <form id="form_data" class="form-horizontal" enctype="multipart/form-data">
                    <div class="control-group" style=" margin: 20px;">
                        <label class="control-label">Biaya Layanan</label>
                        <div class="controls">
                            <input type="text" id="id_charge" name="id_charge" required class="input-xlarge" onkeypress="return isNumberKey(event)" onKeyup="uang('id_charge','');">
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary"><i class="icon-save"></i> Simpan</button>
                        <span id="form_notif"></span>
                        <span><img id="form_loading" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $("#id_charge").val(aptikmacurrency(parseInt(json_charge[0].charge),""));
    $("#form_data").submit(function(e){
        e.preventDefault();
        $("#form_loading").show();
        $.ajax({
            url: "<?php echo URL_OPA.'sett_charge/update'; ?>",
            data: { "id":json_charge[0].id,
                    "charge":aptikmastring($("#id_charge").val(),"")
                    },
            type: 'POST',
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                console.log("OK");
                $("#form_loading").fadeOut(1000, function (){
                    $("#form_notif").text(data.status);
                    $("#form_notif").show();
                    $("#form_notif").fadeOut(2000, function (){
                        json_charge=data.list;
                    });
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("NO");
            }
        });
    });
</script>