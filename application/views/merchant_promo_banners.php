<script>

</script>
<div id="modal_data_hapus" class="modal hide fade" role="dialog">
    <div class="modal-dialog" >

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h3>Konfirmasi</h3>
            </div>
            <div class="modal-body">
                <center>
                    <input type="hidden" id="modal_data_hapus_id">
                    <p style="font-size: 15px;">Yakin Menghapus gambar ini ?</p>
                </center>
            </div>

        </div>
        <div class="modal-footer">
            <span id="modal_data_hapus_notif"></span>
            <span>
            <img id="modal_data_hapus_loading" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>

            <button class="btn" data-dismiss="modal" aria-hidden="true">Batal</button>
            <button onclick="hapus()" class="btn btn-danger"><i class="icon-trash"></i> Hapus</button>
        </div>
    </div>
</div>

<div id="modal_add" class="modal hide fade" role="dialog">
    <div class="modal-dialog" >

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h3>Tambah Gambar</h3>
            </div>
            <div class="modal-body">
                <div class="control-group">
                <form id="banner_form" method="post" enctype="multipart">
                    <label class="control-label">Upload Gambar<span class="apt_bintang">*</span></label>
                    <div class="controls">
                        <input type="file" id="form_file" name="form_file" required class="input-xlarge">
                    </div>
                    </form>
                </div>
            </div>

        </div>
        <div class="modal-footer">
            <span id="modal_data_notif"></span>
            <span><img id="modal_data_loading" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>

            <button class="btn" data-dismiss="modal" aria-hidden="true">Batal</button>
            <button onclick="upload()" class="btn btn-danger"><i class="icon-upload"></i> Upload</button>
        </div>
    </div>
</div>


<div class="page-header">
    <div class="pull-left">
        <h1>Promo <?php echo $promo->nama; ?></h1>
    </div>
    <div class="pull-right">
        <ul class="stats">
            <li class='lightred'>
                <i class="icon-calendar"></i>
                <div class="details">
                    <span class="big">-, -</span>
                    <span>-, -</span>
                </div>
            </li>
        </ul>
    </div>
</div>
<div class="breadcrumbs">
    <ul>
        <li>
            <a>Promo <?php echo $promo->nama; ?></a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a>Banner</a>
        </li>
    </ul>
    <div class="close-bread">
        <a href="#"><i class="icon-remove" style="display: none;" ></i></a>
    </div>
</div>

<div class="row-fluid">
    <div class="span12">
    </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <button type="submit" onclick="showAddModal()" class="btn btn-primary"><i class="icon-plus"></i> Tambah</button>
    </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <div class="box box-color box-bordered">
            <div class="box-title">
                <h3><i class="icon-table"></i>Daftar banner yang dipakai</h3>
            </div>
            <div class="box-content nopadding"  style="overflow: scroll">                
                <table class="table table-hover table-nomargin table-bordered" id="id_tabel">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Gambar</th>
                            <th>Hapus</th>
                        </tr>
                    </thead>
                    <tbody id="isi_tabel">

                    <?php 

                    $i = 1;
                    foreach ($banners as $banner) {
                        echo "<tr><td>" . $i . "</td><td><img src='" . URL_IMAGE_PROMO_WEBADMIN . $banner->image . "' height='200' width='600' /></td><td> <a onclick='hapus_modal(".$banner->id.")'><i rel='tooltip' title='Hapus' style='cursor: pointer' class='icon icon-trash'></i></a></td></tr>";
                        $i++;
                    }

                    ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    //load_upgrade(json_upgrade);
    var id_merchant_promo = '<?php echo $id_merchant_promo; ?>';

    $("#modal_add").modal("hide");
    function showAddModal() {
        $("#modal_add").modal("show");
    }
    $(document).ready(function(){

        $(".chosen-select").chosen({
            no_results_text: "Data tidak ada!",
            width: "100%",
            placeholder_text_single : "Pilih Salah Satu"
        });
    });
    
    function edit_modal(i){
        $("#modal_data").modal("show");
        $("#form_id").val(json_upgrade[i].id_pemilik);
        $("#id_nama").val(json_upgrade[i].nama);
        $("#id_jenis").val(json_upgrade[i].nama_jenis);
        $("#id_level").val(json_upgrade[i].level).trigger("liszt:updated");
    }

    function hapus_modal(id){
        $("#modal_data_hapus_id").val(id);
        $("#modal_data_hapus").modal("show");
    }

    function upload(){
        $("#modal_data_loading").show();
        var dataPost = new FormData($('#banner_form')[0]);
        $.ajax({
            url:"<?php echo URL_OPA;?>merchant_promo/upload_banner/" + id_merchant_promo,
            data: dataPost,
            type: 'POST',
            dataType: 'json',
            async : true,
            cache : false,
            contentType : false,
            processData : false,
            success: function (data, textStatus, jqXHR) {
                        buka_halaman('merchant_promo/banners/' + id_merchant_promo, '2');
                $("#modal_data_loading").fadeOut(10,function(){
                    $("#modal_data_notif").text(data.status);
                    $("#modal_data_notif").show();
                    $("#modal_data_notif").fadeOut(100, function (){
                        $("#modal_add").modal("hide");
                        active_promo=data.data;
                    });
                    
                });
                
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("gagal");

            }
        });
    }

    function hapus(){
        $("#modal_data_hapus_loading").show();
        $.ajax({
            url: "<?php echo URL_OPA.'merchant_promo/delete_banner'; ?>",
            data: {"id":$("#modal_data_hapus_id").val()},
            type: 'POST',
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                        buka_halaman('merchant_promo/banners/' + id_merchant_promo, '2');
                $("#modal_data_hapus_loading").fadeOut(100,function(){
                    $("#modal_data_hapus_notif").text(data.status);
                    $("#modal_data_hapus_notif").show();
                    $("#modal_data_hapus_notif").fadeOut(200, function (){
                        $("#modal_data_hapus").modal("hide");
                    });
                    
                });
                
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("gagal");

            }
        });
    }
</script>