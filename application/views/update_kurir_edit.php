<script></script>
<style type="text/css">
    #map {
        height: 1200px;
        width: 100%;
        position: relative;
    }

   #wrapper { position: relative; }
   #over_map { 
        background-color: white; position: 
        absolute; 
        top: 10px; 
        left: 200px; 
        z-index: 99; 
        padding: 10px; 
        width: 480px;
    }

    .option_tab {
        display:inline-block;
        margin-right: 10px;
        *display:inline;/* For IE7 */
        *zoom:1;/* For IE7 */
        white-space:normal;
        font-size:13px;
        vertical-align:top;
    }

</style>
<div class="page-header">
    <div class="pull-left">
        <h1>Kurir Usaha</h1>
    </div>
    <div class="pull-right">
        <ul class="stats">
            <li class='lightred'>
                <i class="icon-calendar"></i>
                <div class="details">
                    <span class="big">-, -</span>
                    <span>-, -</span>
                </div>
            </li>
        </ul>
    </div>
</div>
<div class="breadcrumbs">
    <ul>
        <li>
            <a>Daftar Kurir</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a>Edit Detail</a>
        </li>
    </ul>
    <div class="close-bread">
        <a href="#"><i class="icon-remove" style="display: none;" ></i></a>
    </div>
</div>
<div class="row-fluid">
    <div class="span12">
        <div class="box box-color box-bordered">
            <div class="box-title">
                <h3><i class="icon-table"></i>Edit Data Kurir</h3>
            </div>
            <div class="box-content" id="form_layout" style="overflow: scroll; height: 100%">
                <form id="form_data" class="span4" enctype="multipart/form-data" accept-charset="utf-8">
                    <div class="control-group">
                        <h4 style="margin-bottom: 20px; margin-top: 20px">LOGIN KURIR</h4>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Nomor Handphone Login<span class="apt_bintang">*</span></label>
                        <div class="controls">
                            <input type="text" id="kontak" placeholder="Nomor untuk Login handset" name="kontak" class="input-xlarge" value="<?php echo $pemilik->kontak; ?>" required />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Kata Sandi Baru</label>
                        <div class="controls">
                            <input type="password" id="password" placeholder="Kata Sandi Baru untuk Login" name="password" class="input-xlarge" />
                        </div>
                    </div>
                    <div class="control-group">
                        <h4 style="margin-bottom: 20px; margin-top: 20px">DATA TEKNIS KURIR</h4>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Username</label>
                        <div class="controls">
                            <input type="text" id="username" required name="username" placeholder="ID Number atau Alias versi toko Anda, jika ada" value="<?php echo $pemilik->username; ?>" required class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">No HP notifikasi SMS</label>
                        <div class="controls">
                            <input type="text" id="kontak_sms" placeholder="Nomor untuk notifikasi order via SMS, jika ada" name="kontak_sms" value="<?php echo $pemilik->kontak_sms; ?>" class="input-xlarge" />
                        </div>
                    </div>
                    <div class="control-group">
                        <h4 style="margin-bottom: 20px; margin-top: 20px">LOKASI ADMINISTRASI USAHA</h4>
                    </div>
                    <div class="control-group">
                        <h4 style="margin-bottom: 20px; margin-top: 20px">PROFIL KURIR</h4>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Nama Kurir<span class="apt_bintang">*</span></label>
                        <div class="controls">
                            <input type="text" id="nama" name="nama" value="<?php echo $pemilik->nama; ?>" placeholder="Nama usaha Anda" required class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"></label>
                        <div class="controls">
                        <img src="<?php echo URL_IMAGE_AVATAR_KURIR . $pemilik->foto; ?>" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Upload Foto</label>
                        <div class="controls">
                            <input type="file" id="form_file" name="form_file" class="input-xlarge">
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary"><i class="icon-add"></i> Simpan</button>
                        <span id="form_notif"></span>
                        <span><img id="form_loading" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBuDrC5iMEnz7vgrcXHSwKqqPpmHCVrp-c&callback=initMap"></script>
<script>
    var currentMarker;
    var map;
    var lat = parseFloat('<?php echo $pemilik->lat; ?>');;
    var lng = parseFloat('<?php echo $pemilik->lng; ?>');;
    var id_provinsi = parseInt('<?php echo $pemilik->id_provinsi; ?>');
    var id_kota = parseInt('<?php echo $pemilik->id_kota; ?>');
    var id_jenis = parseInt('<?php echo $pemilik->id_jenis; ?>');
    var id_kategori = parseInt('<?php echo $pemilik->id_kategori; ?>');
    var id_radius = parseInt('<?php echo $pemilik->id_radius; ?>');
    var markersArray = new Array();
    var search_mode = 0;

    console.log('id_provinsi ' + id_provinsi);
    console.log('id_kota ' + id_kota);
    console.log('id_jenis ' + id_jenis);
    console.log('id_kategori ' + id_kategori);
    console.log('id_radius ' + id_radius);

    function sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
   $('#sharelink_button').contents().unwrap();
        $('#latlnglink_button').contents().unwrap();
        $('#shortlink_wrap').wrap('<a id="shortlink_button" style="cursor: pointer;">');
        $('#latlnglink_wrap').wrap('<a id="latlnglink_button" style="cursor: pointer;">');
        $('#sharelink_button').contents().unwrap();

        $('#sharelinkform_input').show();
        $('#shortlinkform_input').hide();
        $('#latlngform_input').hide();

    $('span.option_tab').on('click', '#sharelink_button', function() {
        console.log('sharelink_button');
        $('#sharelink_button').contents().unwrap();
        $('#latlnglink_button').contents().unwrap();
        $('#shortlink_wrap').wrap('<a id="shortlink_button" style="cursor: pointer;">');
        $('#latlnglink_wrap').wrap('<a id="latlnglink_button" style="cursor: pointer;">');
        $('#sharelink_button').contents().unwrap();

        $('#sharelinkform_input').show();
        $('#shortlinkform_input').hide();
        $('#latlngform_input').hide();
        search_mode = 0;
    });

    $('span.option_tab').on('click', '#shortlink_button',  function() {
        console.log('shortlink_button');
        $('#sharelink_button').contents().unwrap();
        $('#latlnglink_button').contents().unwrap();
        $('#sharelink_wrap').wrap('<a id="sharelink_button" style="cursor: pointer;">');
        $('#latlnglink_wrap').wrap('<a id="latlnglink_button" style="cursor: pointer;">');
        $('#shortlink_button').contents().unwrap();

        $('#sharelinkform_input').hide();
        $('#shortlinkform_input').show();
        $('#latlngform_input').hide();
        search_mode = 1;
    });

    $('span.option_tab').on('click', '#latlnglink_button', function() {
        console.log('latlnglink_button');
        $('#sharelink_button').contents().unwrap();
        $('#shortlink_button').contents().unwrap();
        $('#sharelink_wrap').wrap('<a id="sharelink_button" style="cursor: pointer;">');
        $('#shortlink_wrap').wrap('<a id="shortlink_button" style="cursor: pointer;">');
        $('#latlnglink_button').contents().unwrap();

        $('#sharelinkform_input').hide();
        $('#shortlinkform_input').hide();
        $('#latlngform_input').show();
        search_mode = 2;
    });




    function addNewMarker(latlng) {
        currentMarker = new google.maps.Marker({
            position: latlng,
            map: map,
            animation: google.maps.Animation.DROP
        });
    }

    $(document).ready(function() {
        $(".chosen-select").chosen({
            no_results_text: "Data tidak ada!",
            width: "100%",
            placeholder_text_single: "Pilih Salah Satu"
        });


        $('#id_provinsi').change(function() {
            id_provinsi = $('#id_provinsi').val();
            load_kota(id_provinsi);
        });

        $('#id_kota').change(function() {
            id_kota = $('#id_kota').val();
        });

    });

    function hide_show_forms(e) {
        if (e == 1) {
            $('#form_for_usaha').show();
            $('#code_count_form').prop('required', true);
        } else {
            $('#form_for_usaha').hide();
            $('#code_count_form').prop('required', false);
        }
    }

    function hide_show_code_form() {
        var e = $('#target').val();
        if (e == 1) {
            $('#code_count_form').show();
        } else {
            $('#code_count_form').hide();
        }
    }


    function load_provinsi() {
        var awal = '';
        $.ajax({
            url: "<?php echo URL_OPA.'mobile/api_pemilik/get_provinsi'; ?>",
            data: "",
            type: 'GET',
            dataType: 'json',
            success: function(data, textStatus, jqXHR) {
                var html = "";

                console.log(data);

                for (var i = 0; i < data.data.length; i++) {
                    var selected = '';
                    if (data.data[i].id == id_provinsi) {
                        selected = 'selected="selected"';
                    }
                    var a = '<option value="' + data.data[i].id + '" '+selected+'>' + data.data[i].nama + '</option>';
                    html = html + a;
                }
                id_provinsies = data.data;
                $('#id_provinsi').html(awal + html).trigger("liszt:updated");
                if (data.data.length > 0) {
                    load_kota(data.data[0].id);
                    id_provinsi = data.data[0].id;
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {

            }
        });
    }
    load_provinsi();

    var id_kotaes = [];

    function load_kota(id_provinsi) {
        var awal = '';
        console.log('id_provinsi = ' + id_provinsi);
        $.ajax({
            url: "<?php echo URL_OPA.'mobile/api_pemilik/get_kota'; ?>",
            data: "id_provinsi=" + id_provinsi,
            type: 'POST',
            dataType: 'json',
            success: function(data, textStatus, jqXHR) {
                var html = "";

                console.log(data);

                for (var i = 0; i < data.data.length; i++) {
                    var selected = '';
                    if (data.data[i].id == id_kota) {
                        selected = 'selected="selected"';
                    }
                    var a = '<option value="' + data.data[i].id + '" '+selected+'>' + data.data[i].nama + '</option>';
                    html = html + a;
                }
                id_kotaes = data.data;
                $('#id_kota').html(awal + html).trigger("liszt:updated");
                if (data.data.length > 0) {
                    id_kota = data.data[0].id;
                }

            },
            error: function(jqXHR, textStatus, errorThrown) {

            }
        });
    }

    var id_jenises = [];

    function load_jenis() {
        var awal = '';
        $.ajax({
            url: "<?php echo URL_OPA.'mobile/api_pemilik/get_jenis_kategori_usaha'; ?>",
            data: "",
            type: 'GET',
            dataType: 'json',
            success: function(data, textStatus, jqXHR) {
                var html = "";

                console.log(data);

                for (var i = 0; i < data.data.length; i++) {

                    var selected = '';
                    if (data.data[i].id_jenis == id_jenis) {
                        selected = 'selected="selected"';
                    }
                    var a = '<option value="' + data.data[i].id_jenis + '" '+selected+'>' + data.data[i].nama + '</option>';
                    html = html + a;
                }
                id_jenises = data.data;
                $('#id_jenis').html(awal + html).trigger("liszt:updated");

            },
            error: function(jqXHR, textStatus, errorThrown) {

            }
        });
    }
    load_jenis();

    var id_kategories = [];

    function load_kategori() {
        var awal = '';
        $.ajax({
            url: "<?php echo URL_OPA.'mobile/api_pemilik/get_kategori_usaha'; ?>",
            data: "",
            type: 'GET',
            dataType: 'json',
            success: function(data, textStatus, jqXHR) {
                var html = "";

                console.log(data);

                for (var i = 0; i < data.data.length; i++) {
                    var selected = '';
                    if (data.data[i].id_kategori == id_kategori) {
                        selected = 'selected="selected"';
                    }
                    var a = '<option value="' + data.data[i].id_kategori + '" '+selected+'>' + data.data[i].nama_kategori + '</option>';
                    html = html + a;
                }
                id_kategories = data.data;
                $('#id_kategori').html(awal + html).trigger("liszt:updated");

            },
            error: function(jqXHR, textStatus, errorThrown) {

            }
        });
    }
    load_kategori();


    var id_radiuses = [];

    function load_radius() {
        var awal = '';
        $.ajax({
            url: "<?php echo URL_OPA.'mobile/api_pemilik/get_radius'; ?>",
            data: "",
            type: 'GET',
            dataType: 'json',
            success: function(data, textStatus, jqXHR) {
                var html = "";

                console.log(data);
                for (var i = 0; i < data.data.length; i++) {
                    var selected = '';
                    if (data.data[i].id_radius == id_radius) {
                        selected = 'selected="selected"';
                    }
                    var a = '<option value="' + data.data[i].id_radius + '" '+selected+'>' + data.data[i].radius + ' m</option>';
                    html = html + a;
                }
                id_radiuses = data.data;
                $('#id_radius').html(awal + html).trigger("liszt:updated");

            },
            error: function(jqXHR, textStatus, errorThrown) {

            }
        });
    }
    load_radius();

    $("#form_data").submit(function(e) {
        e.preventDefault();
        currentMarker.setMap(null);
        var latlng = {
            lat: lat,
            lng: lng
        };
        var newMarker = new google.maps.Marker({
            position: latlng,
            map: map,
            animation: google.maps.Animation.DROP
        });
        newMarker.setIcon('<?php echo URL_IMG . "plugins/gmap/"; ?>' + 'green-dot.png');

        var contentString = "<b>" + $('#nama').val() + "</b><br>" + $('#alamat').val();
        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });
        
        infowindow.open(map, newMarker);
        newMarker.addListener('click', function() {
            infowindow.open(map, newMarker);
        });

        if (($('#form_file')[0].files).length > 0){
        if (window.File && window.FileReader && window.FileList && window.Blob)
        {
            //get the file size and file type from file input field
            var fsize = $('#form_file')[0].files[0].size;
            
            if(fsize >1048576) //do something if file size more than 1 mb (1048576)
            {
                alert("Ukuran file tidak boleh lebih dari 1 MB");
                return;
            }
        }else{
            alert("Mohon perbarui browser Anda karena tidak cocok dengan sebagian fitur kami");
        }
    }

        $("#form_loading").show();
        var dataPost = new FormData($(this)[0]);
        // dataPost = dataPost + "&lat=" + lat + "&lng=" + lng + "&id_provinsi=" + id_provinsi + "&id_kota=" + id_kota;
        dataPost.append("lat", lat);
        dataPost.append("lng", lng);
        dataPost.append("id_provinsi", id_provinsi);
        dataPost.append("id_kota", id_kota);
        $.ajax({
            url: "<?php echo URL_OPA.'update_pusaha/update/'.$pemilik->id_pemilik; ?>",
            data: dataPost,
            type: 'POST',
            dataType: 'json',
            async : false,
            cache : false,
            contentType : false,
            processData : false,
            success: function(data, textStatus, jqXHR) {
                console.log("OK");
                if (data.code == 1) {
                    markersArray.push(newMarker);
                    var latlng = {
                        lat: lat,
                        lng: lng
                    };
                    addNewMarker(latlng)
                    $('#form_data').find("input[type=text], textarea").val("");
                    $("#form_loading").fadeOut(1000, function() {
                        $("#form_notif").text(data.status);
                        $("#form_notif").show();
                        $("#form_notif").fadeOut(2000, function() {
                            buka_halaman("update_pusaha/index");
                        });
                    });
                } else {

                        $("#form_notif").text(data.status);
                        $("#form_notif").show();
                        $("#form_loading").hide();
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("NO");
            }
        });
    });
</script>