<script>
    var json_pemilik = <?php echo json_encode($pemilik)?>;
    console.log(json_pemilik);
    var status_edit = "0";
    
    function load_opsi(json){
        for(var i=0;i<json.length;i++){
            if(json[i].id_role == "18"){
                status_edit = "1";
            }
        }   
    }
    
    function load_upgrade(pemilik){
        //$("#id_tabel").dataTable().fnDestroy();
        var html ="";
        for(var i =0;i<pemilik.length;i++){
            var aksi_edit = "";
            
            aksi_hapus = '<a onclick="hapus_modal('+i+')"><i rel="tooltip" title="Hapus" style="cursor: pointer" class="icon icon-trash"></i></a>';

            var link = 'preactive_store/edit/'+pemilik[i].id_pemilik;
            link = link.replace(/'/g, "\\'");

            aksi_edit = "<a onclick='buka_halaman(\""+link+"\")'><i rel='tooltip' title='Ubah' style='cursor: pointer' class='icon icon-pencil'></i></a>";
            var a ='<tr>\n\
                        <td>'+(i+1)+'</td>\n\
                        <td>'+pemilik[i].id_pemilik+'</td>\n\
                        <td>'+pemilik[i].nama+'</td>\n\
                        <td>'+pemilik[i].nama_kategori+'</td>\n\
                        <td>'+pemilik[i].jargon+'</td>\n\
                        <td>'+pemilik[i].alamat+'</td>\n\
                        <td>'+pemilik[i].radius+' m</td>\n\
                        <td>'+aksi_hapus+'&nbsp&nbsp'+aksi_edit+'</td>\n\
                    </tr>';
            
            html = html + a;
        }
        $('#isi_tabel').html(html);
        $("#id_tabel").dataTable();
    }
    load_opsi(json_role);
</script>
tive_promo);
</script>
<div id="modal_data_hapus" class="modal hide fade" role="dialog">
    <div class="modal-dialog" >

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h3>Konfirmasi</h3>
            </div>
            <div class="modal-body">
                <center>
                    <input type="hidden" id="modal_data_hapus_id">
                    <p style="font-size: 15px;">Yakin Menghapus <span id="modal_data_hapus_p" style="font-weight: bold;"></span> ?</p>
                </center>
            </div>

        </div>
        <div class="modal-footer">
            <span id="modal_data_hapus_notif"></span>
            <span><img id="modal_data_hapus_loading" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>

            <button class="btn" data-dismiss="modal" aria-hidden="true">Batal</button>
            <button onclick="hapus()" class="btn btn-danger"><i class="icon-trash"></i> Hapus</button>
        </div>
    </div>
</div>

<div id="modal_data" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-header">
        <a type="button" class="close" data-dismiss="modal" aria-hidden="true">x</a>
        <h3 id="">Data Pemilik Usaha</h3>
    </div>
    <form id="form_data" class="form-horizontal" enctype="multipart/form-data">
        <input type="hidden" id="form_id" name="form_id">
        <div class="modal-body">
            <div class="control-group">
                <label class="control-label">Nama<span class="apt_bintang">*</span></label>
                <div class="controls">
                    <input type="text" disabled id="id_nama" name="id_nama" required class="input-xlarge">
                    
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Jenis Usaha<span class="apt_bintang">*</span></label>
                <div class="controls">
                    <div class="input-xlarge">
                        <input type="text" disabled id="id_jenis" name="id_jenis" required class="input-xlarge">
                    </div>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Level<span class="apt_bintang">*</span></label>
                <div class="controls">
                    <div class="input-xlarge">
                        <select id="id_level" name="id_level" class='chosen-select input-xlarge' required>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <span id="form_notif"></span>
            <span><img id="form_loading" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>

            <a class="btn" data-dismiss="modal" aria-hidden="true">Batal</a>
            <button type="submit" class="btn btn-primary"><i class="icon-save"></i> Simpan</button>
        </div>
    </form>
</div>

<div class="page-header">
    <div class="pull-left">
            <h1>Pre-Active Store</h1>
    </div>
    <div class="pull-right">
        <ul class="stats">
            <li class='lightred'>
                <i class="icon-calendar"></i>
                <div class="details">
                    <span class="big">-, -</span>
                    <span>-, -</span>
                </div>
            </li>
        </ul>
    </div>
</div>
<div class="breadcrumbs">
        <ul>
            <li>
                <a>Pre-Active Store</a>
                <i class="icon-angle-right"></i>
            </li>
            <li>
                <a>Daftar Toko</a>
            </li>
        </ul>
        <div class="close-bread">
                <a href="#"><i class="icon-remove" style="display: none;" ></i></a>
        </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <div class="box box-color box-bordered">
            <div class="box-title">
                <h3><i class="icon-table"></i>Tabel Daftar Usaha</h3>
            </div>
            <div class="box-content nopadding"  style="overflow: scroll">                
                <table class="table table-hover table-nomargin table-bordered" id="id_tabel">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>ID User</th>
                            <th>Nama Pemilik Usaha</th>
                            <th>Kategori Usaha</th>
                            <th>Jargon</th>
                            <th>Alamat</th>
                            <th>Radius Layanan</th>
                            <th>Opsi</th>
                        </tr>
                    </thead>
                    <tbody id="isi_tabel">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    load_upgrade(json_pemilik);
    $(document).ready(function(){
       
        $(".chosen-select").chosen({
            no_results_text: "Data tidak ada!",
            width: "100%",
            placeholder_text_single : "Pilih Salah Satu"
        });
    });
    
    function edit_modal(i){
        $("#modal_data").modal("show");
        $("#form_id").val(json_pemilik[i].id_pemilik);
        $("#id_nama").val(json_pemilik[i].nama);
        $("#id_jenis").val(json_pemilik[i].nama_jenis);
        $("#id_level").val(json_pemilik[i].level).trigger("liszt:updated");
    }
    
    $("#form_data").submit(function (e){
        e.preventDefault();
        $("#form_loading").show();
        $.ajax({
            url: "<?php echo URL_OPA.'pemilik_pusaha/upgrade'; ?>",
            data: { "nama":$("#id_nama").val(),
                    "jenis":$("#id_jenis").val(),
                    "level":$("#id_level").val(),
                    "id":$("#form_id").val()
                    },
            type: 'POST',
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                console.log("OK");
                $("#form_loading").fadeOut(1000, function (){
                    $("#form_notif").text(data.status);
                    $("#form_notif").show();
                    $("#form_notif").fadeOut(2000, function (){
                        datatable_server_side();
                        $("#modal_data").modal("hide");
                    });
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("NO");
            }
        });
    });


    function hapus_modal(i){
        $("#modal_data_hapus_id").val(json_pemilik[i].id_pemilik);
        $("#modal_data_hapus_p").text(json_pemilik[i].nama);
        $("#modal_data_hapus").modal("show");
    }

    function hapus(){
        $("#modal_data_hapus_loading").show();
        $.ajax({
            url: "<?php echo URL_OPA.'preactive_store/hapus_akun'; ?>",
            data: {"id_pemilik":$("#modal_data_hapus_id").val()},
            type: 'POST',
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $("#modal_data_hapus_loading").fadeOut(1000,function(){
                    $("#modal_data_hapus_notif").text(data.pesan);
                    $("#modal_data_hapus_notif").show();
                    $("#modal_data_hapus_notif").fadeOut(2000, function (){
                        $("#modal_data_hapus").modal("hide");
                        json_pemilik=data.data;
                        load_upgrade(json_pemilik);
                    });
                    
                });
                
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("gagal");

            }
        });
    }
    
    ///////////////////////
    
</script>