<script>
    var json_mradius = <?php echo json_encode($mst_radius)?>;
    var status_edit = "0";
    var status_hapus = "0";
    
    function load_opsi(json){
        for(var i=0;i<json.length;i++){
            if(json[i].id_role == "2"){
                $("#opsi_tambah").html('<button class="btn btn-blue tombol_add" onclick="buka_modal()" style="margin: 10px;"><i class="icon-plus"></i> Tambah</button>');
            }else if(json[i].id_role == "3"){
                status_edit = "1";
            }else if(json[i].id_role == "4"){
                status_hapus = "1";
            }
        }   
    }
    
    function load_radius(mst_radius){
        $("#id_tabel").dataTable().fnDestroy();
        var html ="";
        for(var i =0;i<mst_radius.length;i++){
            var aksi_edit = "";
            var aksi_hapus = "";
            
            if(status_edit == "1"){
                aksi_edit = '<a onclick="edit_modal('+i+')"><i rel="tooltip" title="Ubah" style="cursor: pointer" class="icon icon-pencil"></i></a>';
            }if(status_hapus == "1"){
                aksi_hapus = '<a onclick="hapus_modal('+i+')"><i rel="tooltip" title="Hapus" data-placement="bottom" style="cursor: pointer" class="icon icon-trash"></i></a>';
            }
            
            var a ='<tr>\n\
                        <td>'+(i+1)+'</td>\n\
                        <td>'+mst_radius[i].nama+'</td>\n\
                        <td>'+aptikmacurrency(parseInt(mst_radius[i].radius),"")+' m</td>\n\
                        <td>'+aptikmacurrency(parseInt(mst_radius[i].harga),"")+'</td>\n\
                        <td>'+aksi_edit+'&nbsp;&nbsp;&nbsp;'+aksi_hapus+'</td>\n\
                    </tr>';
            
            html = html + a;
        }
        $('#isi_tabel').html(html);
        $("#id_tabel").dataTable();
    }
    
    load_opsi(json_role);
    load_radius(json_mradius);
</script>

<div id="modal_data" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-header">
        <a type="button" class="close" data-dismiss="modal" aria-hidden="true">x</a>
        <h3 id="">Data Radius</h3>
    </div>
    <form id="form_data" class="form-horizontal" enctype="multipart/form-data">
        <input type="hidden" id="form_id" name="form_id">
        <div class="modal-body">
            <div class="control-group">
                <label class="control-label">Nama<span class="apt_bintang">*</span></label>
                <div class="controls">
                    <input type="text" id="id_nama" name="id_nama" required class="input-xlarge">
                    
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Radius<span class="apt_bintang">*</span></label>
                <div class="controls">
                    <input type="text" id="id_radius" name="id_radius" required class="input-xlarge" onkeypress="return isNumberKey(event)" onKeyup="uang('id_radius','');">
                    
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Harga<span class="apt_bintang">*</span></label>
                <div class="controls">
                    <input type="text" id="id_harga" name="id_harga" required class="input-xlarge" onkeypress="return isNumberKey(event)" onKeyup="uang('id_harga','');">
                    
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <span id="form_notif"></span>
            <span><img id="form_loading" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>

            <a class="btn" data-dismiss="modal" aria-hidden="true">Batal</a>
            <button type="submit" class="btn btn-primary"><i class="icon-save"></i> Simpan</button>
        </div>
    </form>
</div>

<div id="modal_data_hapus" class="modal hide fade" role="dialog">
    <div class="modal-dialog" >

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h3>Konfirmasi</h3>
            </div>
            <div class="modal-body">
                <center>
                    <input type="hidden" id="modal_data_hapus_id">
                    <p style="font-size: 15px;">Yakin Menghapus <span id="modal_data_hapus_p" style="font-weight: bold;"></span> ?</p>
                </center>
            </div>

        </div>
        <div class="modal-footer">
            <span id="modal_data_hapus_notif"></span>
            <span><img id="modal_data_hapus_loading" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>

            <button class="btn" data-dismiss="modal" aria-hidden="true">Batal</button>
            <button onclick="hapus()" class="btn btn-danger"><i class="icon-trash"></i> Hapus</button>
        </div>
    </div>
</div>

<div class="page-header">
    <div class="pull-left">
            <h1>Radius</h1>
    </div>
    <div class="pull-right">
        <ul class="stats">
            <li class='lightred'>
                <i class="icon-calendar"></i>
                <div class="details">
                    <span class="big">-, -</span>
                    <span>-, -</span>
                </div>
            </li>
        </ul>
    </div>
</div>
<div class="breadcrumbs">
        <ul>
            <li>
                <a>Master</a>
                <i class="icon-angle-right"></i>
            </li>
            <li>
                <a>Radius</a>
            </li>
        </ul>
        <div class="close-bread">
                <a href="#"><i class="icon-remove" style="display: none;" ></i></a>
        </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <div class="box box-color box-bordered">
            <div class="box-title">
                <h3><i class="icon-table"></i>Tabel Data Radius</h3>
            </div>
            <div class="box-content nopadding"  style="overflow: scroll">
                <div id="opsi_tambah">
                    
                </div>
                
                <table class="table table-hover table-nomargin table-bordered" id="id_tabel">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Radius</th>
                            <th>Harga</th>
                            <th>Opsi</th>
                        </tr>
                    </thead>
                    <tbody id="isi_tabel">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    function buka_modal(){
        $("#modal_data").modal("show");
        $("#id_nama").val("");
        $("#id_radius").val("");
        $("#id_harga").val("");
        $("#form_id").val("");
    }
    
    $("#form_data").submit(function (e){
        e.preventDefault();
        $("#form_loading").show();
        $.ajax({
            url: "<?php echo URL_OPA.'mst_radius/insert_radius'; ?>",
            data: { "nama":$("#id_nama").val(),
                    "radius":aptikmastring($("#id_radius").val(),''),
                    "harga":aptikmastring($("#id_harga").val(),''),
                    "id":$("#form_id").val()
                    },
            type: 'POST',
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                console.log("OK");
                $("#form_loading").fadeOut(1000, function (){
                    $("#form_notif").text(data.status);
                    $("#form_notif").show();
                    $("#form_notif").fadeOut(2000, function (){
                        json_mradius=data.list;
                        load_radius(json_mradius);
                        $("#modal_data").modal("hide");
                    });
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("NO");
            }
        });
    });
    
    function edit_modal(i){
        $("#modal_data").modal("show");
        $("#form_id").val(json_mradius[i].id);
        $("#id_nama").val(json_mradius[i].nama);
        $("#id_radius").val(aptikmacurrency(parseInt(json_mradius[i].radius),""));
        $("#id_harga").val(aptikmacurrency(parseInt(json_mradius[i].harga),""));
    }
    
    function hapus_modal(i){
        $("#modal_data_hapus_id").val(json_mradius[i].id);
        $("#modal_data_hapus_p").text(json_mradius[i].nama);
        $("#modal_data_hapus").modal("show");
    }
    
    function hapus(){
        $("#modal_data_hapus_loading").show();
        $.ajax({
            url: "<?php echo URL_OPA.'mst_radius/delete'; ?>",
            data: {"id":$("#modal_data_hapus_id").val()},
            type: 'POST',
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $("#modal_data_hapus_loading").fadeOut(1000,function(){
                    $("#modal_data_hapus_notif").text(data.status);
                    $("#modal_data_hapus_notif").show();
                    $("#modal_data_hapus_notif").fadeOut(2000, function (){
                        json_mradius=data.list;
                        load_radius(json_mradius);
                        $("#modal_data_hapus").modal("hide");
                    });
                    
                });
                
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("gagal");

            }
        });
    }
</script>