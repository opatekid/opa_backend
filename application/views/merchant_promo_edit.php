<script>

</script>
<div class="page-header">
    <div class="pull-left">
        <h1>Promo</h1>
    </div>
    <div class="pull-right">
        <ul class="stats">
            <li class='lightred'>
                <i class="icon-calendar"></i>
                <div class="details">
                    <span class="big">-, -</span>
                    <span>-, -</span>
                </div>
            </li>
        </ul>
    </div>
</div>
<div class="breadcrumbs">
    <ul>
        <li>
            <a>Promo</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a>Create Promo</a>
        </li>
    </ul>
    <div class="close-bread">
        <a href="#"><i class="icon-remove" style="display: none;" ></i></a>
    </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <div class="box box-color box-bordered">
            <div class="box-title">
                <h3><i class="icon-table"></i>Untuk Pemilik Usaha</h3>
            </div>
            <div class="box-content nopadding"  style="overflow: scroll">
                <form id="form_data" class="form-horizontal" enctype="multipart/form-data">
                    <div class="control-group" style=" margin: 20px;">
                        <label class="control-label">Nama Promo</label>
                        <div class="controls">
                            <input type="text" id="nama" name="nama" required class="input-xlarge" value="<?php echo $merchant_promo->nama; ?>" >
                        </div>
                    </div>
                        <div class="control-group" style=" margin: 20px;">
                            <label class="control-label">Tipe Promo</label>
                            <div class="controls">
                                <div class="input-xlarge">
                                    <select id="tipe" name="tipe" class='chosen-select input-xlarge' required>
                                        <option value="1" <?php if ($merchant_promo->tipe == '1') echo "selected='selected'"; ?> >Diskon Harga</option>
                                        <option value="2" <?php if ($merchant_promo->tipe == '2') echo "selected='selected'"; ?> >Free Items</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="control-group" style=" margin: 20px;">
                            <label class="control-label">Mulai Tanggal</label>
                            <div class="controls">                        
                                <div class="input-xlarge">
                                    <input type="date" id="start_date"  value="<?php echo $merchant_promo->start_date; ?>" placeholder="Tanggal mulai berlaku promo" name="start_date" class="input-xlarge" >
                                </div>
                            </div>
                        </div>

                        <div class="control-group" style=" margin: 20px;">
                            <label class="control-label">Selesai Tanggal</label>
                            <div class="controls">                        
                                <div class="input-xlarge">
                                    <input type="date" id="end_date"  value="<?php echo $merchant_promo->end_date; ?>" placeholder="Tanggal selesai berlaku promo" name="end_date" class="input-xlarge" >
                                </div>
                            </div>
                        </div>

                        <div class="control-group" style=" margin: 20px;">
                            <label class="control-label">Upload Produk Promo</label>
                            <div class="controls">                        
                                <div class="input-xlarge">
                                    <input type="file" id="form_file" placeholder="" name="form_file" class="input-xlarge" >
                                </div>
                            </div>
                        </div>
                    
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary"><i class="icon-add"></i> Edit Promo</button>
                        <span id="form_notif"></span>
                        <span><img id="form_loading" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>

    var id = "<?php echo $merchant_promo->id; ?>";

    $(document).ready(function(){
        $(".chosen-select").chosen({
            no_results_text: "Data tidak ada!",
            width: "100%",
            placeholder_text_single : "Pilih Salah Satu"
        });
    });

    function hide_show_forms(e){
        if (e == 1) {
            $('#form_for_usaha').show();
            $('#code_count_form').prop('required', true);
        } else {
            $('#form_for_usaha').hide();
            $('#code_count_form').prop('required', false);
        }
    }

    function hide_show_code_form(){
        var e = $('#target').val();
        if (e == 1) {
            $('#code_count_form').show();
        } else {
            $('#code_count_form').hide();
        }
    }

    var kotkab ="";
    function load_kotkab(e,typ){
        var awal = '<option value="0">-All-</option>';
        $.ajax({          
            url: "<?php echo URL_OPA.'lap_belanja_kota/get_kotkab/'; ?>"+e,
            data:"",
            type: 'GET',
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                var html = "";

                for (var i=0;i<data.kotkab.length;i++){
                    var a ='<option value="'+data.kotkab[i].id+'">'+data.kotkab[i].nama+'</option>';
                    html = html + a;
                }
                kotkab = data.kotkab;
                $('#id_kota').html(awal+html).trigger("liszt:updated");

            },
            error: function (jqXHR, textStatus, errorThrown) {

            }
        });
    }

    $("#form_data").submit(function(e){
        e.preventDefault();
            $("#form_loading").show();
            $.ajax({
                url: "<?php echo URL_OPA . 'merchant_promo/update/'; ?>" + id,
                data: new FormData($(this)[0]),
                type: 'POST',
                dataType: 'json',
                async : false,
                cache : false,
                contentType : false,
                processData : false,
                success: function (data, textStatus, jqXHR) {
                    console.log("OK");
                    buka_halaman('merchant_promo', '2');
                    $('#form_data').find("input[type=text], textarea").val("");
                    $("#form_loading").fadeOut(1000, function (){
                        $("#form_notif").text(data.status);
                        $("#form_notif").show();
                        $("#form_notif").fadeOut(2000, function (){
                            json_dkredit=data.list;
                        });
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log("NO");
                }
            });
    });
</script>