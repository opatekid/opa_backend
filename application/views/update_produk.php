<style type="text/css">

    

    /* Important part */
    .modal-dialog {
        overflow-y: initial !important
    }
    #data-modal-body {
        height: auto;
        overflow-y: auto;
    }
</style>
<script>
    var json_upgrade = <?php echo json_encode($tran_upgrade)?>;
    var level = <?php echo json_encode($level)?>;
    var status_edit = "0";
    

    
    function load_level(level){
        var awal = '<option value="">-Pilih Level-</option>';
        var html = "";
        
        for (var i=0;i<level.length;i++){
            var a ='<option value="'+level[i].id+'">'+level[i].nama+'</option>';
           html = html + a;
        }

        $('#id_level').html(awal+html).trigger("liszt:updated");

    }
    // load_opsi(json_role);
</script>

<div id="modal_data" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-header">
        <a type="button" class="close" data-dismiss="modal" aria-hidden="true">x</a>
        <h3 id="">Products Data</h3>
    </div>
    <form id="form_data" class="form-horizontal" enctype="multipart/form-data">
            <div id="data-modal-body" class="modal-body">
        <input type="hidden" id="form_id" name="form_id" />
        <span>Are you sure to delete this Product?</span>
        </div>
        <div class="modal-footer">
            <span id="form_notif"></span>
            <span><img id="form_loading" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>

            <a class="btn" data-dismiss="modal" aria-hidden="true">Cancel</a>
            <button type="submit" id="hapus_button" class="btn btn-primary"><i class="icon-delete"></i> Delete</button>
        </div>
    </form>
</div>


<div id="modal_add" class="modal hide fade" role="dialog">
    <div class="modal-dialog" >

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h3>Add Image</h3>
            </div>
            <div class="modal-body">
                <div class="control-group">
                <form id="banner_form" method="post" enctype="multipart">
                    <label class="control-label">Upload Image<span class="apt_bintang">*</span></label>
                    <div class="controls">
                        <input type="file" id="form_file" name="form_file" required class="input-xlarge">
                    </div>
                    </form>
                </div>
            </div>

        </div>
        <div class="modal-footer">
            <span id="modal_data_notif"></span>
            <span><img id="modal_data_loading" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>

            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
            <button onclick="upload()" class="btn btn-danger"><i class="icon-upload"></i> Upload</button>
        </div>
    </div>
</div>


<div class="page-header">
    <div class="pull-left">
            <h1>Manage Product Products</h1>
    </div>
    <div class="pull-right">
        <ul class="stats">
            <li class='lightred'>
                <i class="icon-calendar"></i>
                <div class="details">
                    <span class="big">-, -</span>
                    <span>-, -</span>
                </div>
            </li>
        </ul>
    </div>
</div>
<div class="breadcrumbs">
        <ul>
            <li>
                <a>Manage</a>
                <i class="icon-angle-right"></i>
            </li>
            <li>
                <a>Data of Product Products</a>
            </li>
        </ul>
        <div class="close-bread" style="display: none;">
                <a href="#"><i class="icon-remove"></i></a>
        </div>
</div>

<div class="row-fluid">
    <div class="span12">
    </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <button type="submit" onclick="buka_halaman('update_produk/add', 5)" class="btn btn-primary"><i class="icon-plus"></i> Add New</button>
    </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <div class="box box-color box-bordered">
            <div class="box-title">
                <h3><i class="icon-table"></i>Product Products Table</h3>
            </div>
            <div class="box-content nopadding"  style="overflow: scroll">                
                <table class="table usertable table-hover table-nomargin table-bordered" id="id_tabel">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Product ID</th>
                            <th>Name</th>
                            <th>Category</th>
                            <th>Price</th>
                            <th>Edit</th>
                        </tr>
                    </thead>
                    <tbody id="isi_tabel">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    //load_upgrade(json_upgrade);
    load_level(level);

    $('#hapus_button').click(function() {
        console.log('clickeddddd');
    });


    $(document).ready(function(){
       
        $(".chosen-select").chosen({
            no_results_text: "Data tidak ada!",
            width: "100%",
            placeholder_text_single : "Pilih Salah Satu"
        });
    });
    
    function hapus_modal(id_produk){
        $("#modal_data").modal("show");
        $("#form_id").val(id_produk);
    }
    
    $("#form_data").submit(function (e){
        e.preventDefault();
        $("#form_loading").show();
        $.ajax({
            url: "<?php echo URL_OPA.'update_produk/delete_produk'; ?>",
            data: { 
                    "id_produk":$("#form_id").val()
                    },
            type: 'POST',
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                console.log("OK");
                $("#form_loading").fadeOut(1000, function (){
                    $("#form_notif").text(data.status);
                    $("#form_notif").show();
                    $("#form_notif").fadeOut(2000, function (){
                        datatable_server_side();
                        $("#modal_data").modal("hide");
                    });
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("NO");
            }
        });
    });
    
    ///////////////////////
    
    var dataTable;
    
    datatable_server_side();
    
    function datatable_server_side(){
        console.log("server");
        if(dataTable != null) dataTable.fnDestroy();
        
        dataTable = $('#id_tabel').dataTable( {
            'iDisplayLength': 10,
            "aoColumnDefs": [
                { 'bSortable': false, 'aTargets': [ 0,1,2,3,4 ] }
            ],
            "processing": true,
            "serverSide": true,
            "ajax":{
                url :"<?php  echo URL_OPA; ?>update_produk/get_data", // json datasource
                type: "post",  // method  , by default get
                "data": function ( d ) {
                    
                },
                error: function(){  // error handling
                    $(".id_tabel-error").html("");
                    $("#id_tabel").append('<tbody class="id_tabel-error"><tr><th colspan="6">No item found in the database</th></tr></tbody>');
                    $("#id_tabel_processing").css("display","none");

                },
                "dataSrc": function ( json ) {
                    if (json.pesan == 'session expired') {
                        window.location.href = "<?php echo URL_OPA . 'merchant_login'; ?>";
                    }
                    json_upgrade = json.json;
                    return json.data;
                }
            }
            
        });
		
    }
</script>