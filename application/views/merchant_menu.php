<div id="navigation">
    <div class="container-fluid">
        <a href="<?php echo URL_OPA . "merchant_home" ?>" id="brand">...</a>
        <ul class='main-nav' style="cursor: pointer;">    

            <li class="active menu_main menu_main2" id="menu_main2" onclick="buka_halaman('update_pusaha/index','2')" >
                <a href="#" data-toggle="dropdown" class='dropdown-toggle'>
                    <span>Kelola Gerai</span>
                </a>
            </li>   

            <li class="active menu_main menu_main1" id="menu_main1" onclick="buka_halaman('merchant_beranda','1')" >
                <a href="#" data-toggle="dropdown" class='dropdown-toggle'>
                    <span>Master Produk</span>
                </a>
            </li>      

            <li class="active menu_main menu_main5" id="menu_main5" onclick="buka_halaman('update_produk','5')" >
                <a href="#" data-toggle="dropdown" class='dropdown-toggle'>
                    <span>Products</span>
                </a>
            </li>                  
        

            <li class="menu_main menu_main3" id="menu_main3">
                <ul class="dropdown-menu">
                    <li onclick="buka_halaman('merchant_promo/add','3')">
                        <a>Tambah Promo</a>
                    </li>
                    <li onclick="buka_halaman('merchant_promo/index','3')">
                        <a>Data Promo</a>
                    </li>
                </ul>
                <a href="#" data-toggle="dropdown" class='dropdown-toggle'>
                    <span>Promo</span>
                    <span class="caret"></span>
                </a>
            </li>     

            <li class="menu_main menu_main4" id="menu_main4">
                <ul class="dropdown-menu">
                    <li onclick="buka_halaman('merchant_beranda/banners','4')">
                        <a>Daftar Banner</a>
                    </li>
                </ul>
                <a href="#" data-toggle="dropdown" class='dropdown-toggle'>
                    <span>Banner Toko</span>
                    <span class="caret"></span>
                </a>
            </li>

        </ul>
        <div class="user">
            <div class="dropdown">
                <a href="#" class='dropdown-toggle' id="id_nama_user" data-toggle="dropdown"><?php echo $namaad ?> <img src="<?php echo URL_IMAGE_PEMILIK . $pengguna->foto; ?>" style="width: 27px; height: 27px;" alt=""></a>
                <ul class="dropdown-menu pull-right" style="cursor: pointer;">
                    <li>
                        <a onclick="showsunting()">Profil</a>
                    </li>
                    <li>
                        <a onclick="showgantipass()">Ganti Password</a>
                    </li>
                    <li>
                        <a href="<?php echo URL_OPA.'merchant_login/logout/'?>">Sign out</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<script>
    
    load_menu();
    
    function load_menu(){
        $(".admin-kredit").hide();
        var menu_master = "";
        var menu_transaksi = "";
        
        var mn_lap_penjualan = "";
        var mn_lap_belanja = "";
        var mn_lap_pendapatan = "";
        var mn_lap_data = "";
        var mn_create_promo = "";
        
        var mn_rek_penjualan = "";
        var mn_rek_belanja = "";
        var mn_rek_pendapatan = "";
        var mn_rek_data_pemilkush = "";
        var mn_rek_data_konsumen = "";
        
        var mn_kredit = "";
        
            menu_transaksi += "<li onclick=\"buka_halaman('update_pusaha','59')\"><a>Manage Gerai</a></li>";
           
        $("#mn_kredit").html(mn_kredit);
        
        $("#mn_rek_data_konsumen").html(mn_rek_data_konsumen);
        $("#mn_rek_data_pemilkush").html(mn_rek_data_pemilkush);
        $("#mn_rek_pendapatan").html(mn_rek_pendapatan);
        $("#mn_rek_belanja").html(mn_rek_belanja);
        $("#mn_rek_penjualan").html(mn_rek_penjualan);
        
        $("#mn_lap_data").html(mn_lap_data);
        $("#mn_create_promo").html(mn_create_promo);
        $("#mn_lap_pendapatan").html(mn_lap_pendapatan);
        $("#mn_lap_belanja").html(mn_lap_belanja);
        $("#mn_lap_penjualan").html(mn_lap_penjualan);
        
        $("#menu_master").html(menu_master);
        $("#menu_transaksi").html(menu_transaksi);
    }
    
    var myLevel = "<?php echo $this->session->userdata("level") ?>";
    if (myLevel!="1"){
        $(".admin_aja").hide();
    }
</script>