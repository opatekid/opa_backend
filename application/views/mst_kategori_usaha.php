<script>
    var json_kategori = <?php echo json_encode($mst_kategori)?>;
    var level = <?php echo json_encode($level)?>;
    var status_edit = "0";
    var status_hapus = "0";
    
    function load_opsi(json){
        for(var i=0;i<json.length;i++){
            if(json[i].id_role == "6"){
                $("#opsi_tambah").html('<button class="btn btn-blue tombol_add" onclick="buka_modal()" style="margin: 10px;"><i class="icon-plus"></i> Tambah</button>');
            }else if(json[i].id_role == "7"){
                status_edit = "1";
            }else if(json[i].id_role == "8"){
                status_hapus = "1";
            }
        }   
    }
    
    function load_kategori(mst_kategori){
        $("#id_tabel").dataTable().fnDestroy();
        var dir = "<?php echo URL_IMAGE_IKATEGORI; ?>";
        var html ="";
        var tipe ="";
        for(var i =0;i<mst_kategori.length;i++){
            if (mst_kategori[i].tipe == "1"){
                tipe = "Tanpa area tambahan";
            } else if(mst_kategori[i].tipe == "2"){
                tipe = "Ada area tambahan";
            }
            var aksi_edit = "";
            var aksi_hapus = "";
            
            if(status_edit == "1"){
                aksi_edit = '<a onclick="edit_modal('+i+')"><i rel="tooltip" title="Ubah" style="cursor: pointer" class="icon icon-pencil"></i></a>';
            }if(status_hapus == "1"){
                aksi_hapus = '<a onclick="hapus_modal('+i+')"><i rel="tooltip" title="Hapus" data-placement="bottom" style="cursor: pointer" class="icon icon-trash"></i></a>';
            }
            var a ='<tr>\n\
                        <td>'+(i+1)+'</td>\n\
                        <td>'+mst_kategori[i].nama+'</td>\n\
                        <td>'+tipe+'</td>\n\
                        <td><img src="'+dir+mst_kategori[i].icon_biru+'?version='+mst_kategori[i].versi_icon_biru+'" style="width:85%; text-align:center;"></td>\n\
                        <td><img src="'+dir+mst_kategori[i].icon_hitam+'?version='+mst_kategori[i].versi_icon_hitam+'" style="width:85%; text-align:center;"></td>\n\
                        <td>'+aptikmacurrency(parseInt(mst_kategori[i].nama_radius),"")+' m</td>\n\
                        <td>'+aksi_edit+'&nbsp;&nbsp;&nbsp;'+aksi_hapus+'</td>\n\
                    </tr>';
            
            html = html + a;
        }
        $('#isi_tabel').html(html);
        $("#id_tabel").dataTable();
    }
    function load_level(level){
        var awal = '<option value="">-Pilih Level-</option>';
        var html = "";
        
        for (var i=0;i<level.length;i++){
            var a ='<option value="'+level[i].id+'">'+level[i].nama+' ['+aptikmacurrency(parseInt(level[i].radius),"")+' m]</option>';
           html = html + a;
        }

        $('#id_level').html(awal+html).trigger("liszt:updated");

    }
    
    load_opsi(json_role);
</script>

<div id="modal_data" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-header">
        <a type="button" class="close" data-dismiss="modal" aria-hidden="true">x</a>
        <h3 id="">Data Kategori Usaha</h3>
    </div>
    <form id="form_1upload" onSubmit="return false" method="post" enctype="multipart/form-data">
        <input onchange="upload_1img()" type="file" name="image_1file" id="modal_1photo_upload" class="hidden">
    </form>
    <form id="form_upload" onSubmit="return false" method="post" enctype="multipart/form-data">
        <input onchange="upload_img()" type="file" name="image_file" id="modal_photo_upload" class="hidden">
    </form>
    <form id="form_data" class="form-horizontal" enctype="multipart/form-data">
        <input type="hidden" id="form_id" name="form_id">
        <div class="modal-body" style="overflow-y: scroll;">
            <div class="control-group">
                <label class="control-label">Nama Kategori Usaha<span class="apt_bintang">*</span></label>
                <div class="controls">
                    <input type="text" id="id_nama" name="id_nama" required class="input-xlarge">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Jenis Area Tambahan<span class="apt_bintang">*</span></label>
                <div class="controls">
                    <div class="input-xlarge">
                        <select id="id_tipe" name="id_tipe" class='chosen-select input-xlarge' required>
                            <option value="">-Pilih Tipe-</option>
                            <option value="1">Tanpa area tambahan</option>
                            <option value="2">Ada area tambahan</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Level<span class="apt_bintang">*</span></label>
                <div class="controls">
                    <div class="input-xlarge">
                        <select id="id_level" name="id_level" class='chosen-select input-xlarge' required>
                        </select>
                    </div>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Icon Biru<span class="apt_bintang">*</span></label>
                <div class="controls">
                    <img style="cursor: pointer" id="modal_1photo"  onclick="performClick('modal_1photo_upload')"  width='128px' height='128px'>
                    <div id="progress1box" style="display:none;"><div id="progress1bar"></div><div id="status1txt">0%</div></div>
                    <div id="out1put"></div>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Icon Hitam<span class="apt_bintang">*</span></label>
                <div class="controls">
                    <img style="cursor: pointer" id="modal_photo"  onclick="performClick('modal_photo_upload')"  width='128px' height='128px'>
                    <div id="progressbox" style="display:none;"><div id="progressbar"></div><div id="statustxt">0%</div></div>
                    <div id="output"></div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <span id="form_notif"></span>
            <span><img id="form_loading" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>

            <a class="btn" data-dismiss="modal" aria-hidden="true">Batal</a>
            <button type="submit" class="btn btn-primary"><i class="icon-save"></i> Simpan</button>
        </div>
    </form>
</div>

<div id="modal_data_hapus" class="modal hide fade" role="dialog">
    <div class="modal-dialog" >

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h3>Konfirmasi</h3>
            </div>
            <div class="modal-body">
                <center>
                    <input type="hidden" id="modal_data_hapus_id">
                    <p style="font-size: 15px;">Yakin Menghapus <span id="modal_data_hapus_p" style="font-weight: bold;"></span> ?</p>
                </center>
            </div>

        </div>
        <div class="modal-footer">
            <span id="modal_data_hapus_notif"></span>
            <span><img id="modal_data_hapus_loading" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>

            <button class="btn" data-dismiss="modal" aria-hidden="true">Batal</button>
            <button onclick="hapus()" class="btn btn-danger"><i class="icon-trash"></i> Hapus</button>
        </div>
    </div>
</div>

<div class="page-header">
    <div class="pull-left">
            <h1>Kategori Usaha</h1>
    </div>
    <div class="pull-right">
        <ul class="stats">
            <li class='lightred'>
                <i class="icon-calendar"></i>
                <div class="details">
                    <span class="big">-, -</span>
                    <span>-, -</span>
                </div>
            </li>
        </ul>
    </div>
</div>

<div class="breadcrumbs">
        <ul>
            <li>
                <a>Master</a>
                <i class="icon-angle-right"></i>
            </li>
            <li>
                <a>Kategori Usaha</a>
            </li>
        </ul>
        <div class="close-bread">
                <a href="#"><i class="icon-remove" style="display: none;" ></i></a>
        </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <div class="box box-color box-bordered">
            <div class="box-title">
                <h3><i class="icon-table"></i>Tabel Kategori Usaha</h3>
            </div>
            <div class="box-content nopadding"  style="overflow: scroll">
                <div id="opsi_tambah">
                    
                </div>
                
                <table class="table table-hover table-nomargin table-bordered" id="id_tabel">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Kategori Usaha</th>
                            <th>Jenis Area Tambahan</th>
                            <th>Icon Biru</th>
                            <th>Icon Hitam</th>
                            <th>Radius</th>
                            <th>Opsi</th>
                        </tr>
                    </thead>
                    <tbody id="isi_tabel">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    var idpengguna = "<?php echo $this->session->userdata('id'); ?>";
    load_level(level);
    load_kategori(json_kategori);
    $(document).ready(function(){
        $(".chosen-select").chosen({
            no_results_text: "Data tidak ada!",
            width: "100%",
            placeholder_text_single : "Pilih Salah Satu"
        });
    });
    
    function hapus_modal(i){
        $("#modal_data_hapus_id").val(json_kategori[i].id_jenis);
        $("#modal_data_hapus_p").text(json_kategori[i].nama);
        $("#modal_data_hapus").modal("show");
    }
    
    function hapus(){
        $("#modal_data_hapus_loading").show();
        $.ajax({
            url: "<?php echo URL_OPA.'mst_kategori_usaha/delete'; ?>",
            data: {"id":$("#modal_data_hapus_id").val()},
            type: 'POST',
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $("#modal_data_hapus_loading").fadeOut(1000,function(){
                    $("#modal_data_hapus_notif").text(data.status);
                    $("#modal_data_hapus_notif").show();
                    $("#modal_data_hapus_notif").fadeOut(2000, function (){
                        json_kategori=data.list;
                        load_kategori(json_kategori);
                        $("#modal_data_hapus").modal("hide");
                    });
                    
                });
                
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("gagal");

            }
        });
    }
    
    function performClick(elemId) {
//         console.log("klik");
        var elem = document.getElementById(elemId);
        if(elem && document.createEvent) {
            var evt = document.createEvent("MouseEvents");
            evt.initEvent("click", true, false);
            elem.dispatchEvent(evt);
        }
    }
    
    function  upload_1img(){
//        console.log("tiga");
        var val = $("#modal_1photo_upload").val();
        switch(val.substring(val.lastIndexOf('.') + 1).toLowerCase()){
            case 'jpg': case 'png': case 'png': case 'jpeg':
                $('form#form_1upload').submit();
                break;
            default:
                $("#modal_1photo_upload").val('');
                alert("Unsupported file type!!");
                break;
        }
    }
    function  upload_img(){
//        console.log("tiga");
        var val = $("#modal_photo_upload").val();
        switch(val.substring(val.lastIndexOf('.') + 1).toLowerCase()){
            case 'jpg': case 'png': case 'png': case 'jpeg':
                $('form#form_upload').submit();
                break;
            default:
                $("#modal_photo_upload").val('');
                alert("Unsupported file type!!");
                break;
        }
    }
    
    var progress1box     = $('#progress1box');
    var progress1bar     = $('#progress1bar');
    var status1txt       = $('#status1txt');
    var comple1ted       = '0%';
    var file_1name       = "";
    
    var progressbox     = $('#progressbox');
    var progressbar     = $('#progressbar');
    var statustxt       = $('#statustxt');
    var completed       = '0%';
    var file_name       = "";
    
    var options1 = { 
                type: "POST",
                url : "<?php echo URL_OPA;?>mst_kategori_usaha/upload1/",
		target:   '#out1put',   // target element(s) to be updated with server response 
		beforeSubmit:  before1Submit,  // pre-submit callback 
		uploadProgress: On1Progress,
		success:       after1Success,  // post-submit callback 
                error : show1Error,
		resetForm: true        // reset the form after successful submit 
    };
    var options = { 
                type: "POST",
                url : "<?php echo URL_OPA;?>mst_kategori_usaha/upload/",
		target:   '#output',   // target element(s) to be updated with server response 
		beforeSubmit:  beforeSubmit,  // pre-submit callback 
		uploadProgress: OnProgress,
		success:       afterSuccess,  // post-submit callback 
                error : showError,
		resetForm: true        // reset the form after successful submit 
    };
    
    $('#form_1upload').submit(function() { 
//        console.log("empat");
        var val = $("#modal_1photo_upload").val();
        var ext  =  val.substring(val.lastIndexOf('.') + 1).toLowerCase();
        file_1name = "icon_biru"+idpengguna+"."+ext;
        var input = $("<input>")
               .attr("type", "hidden")
               .attr("name", "name").val(file_1name);
                $('#form_1upload').append($(input));
        $(this).ajaxSubmit(options1);  			
	// return false to prevent standard browser submit and page navigation 
	return false; 
    });
    $('#form_upload').submit(function() { 
//        console.log("empat");
        var val = $("#modal_photo_upload").val();
        var ext  =  val.substring(val.lastIndexOf('.') + 1).toLowerCase();
        file_name = "icon_hitam"+idpengguna+"."+ext;
        var input = $("<input>")
               .attr("type", "hidden")
               .attr("name", "name").val(file_name);
                $('#form_upload').append($(input));
        $(this).ajaxSubmit(options);  			
	// return false to prevent standard browser submit and page navigation 
	return false; 
    });
    
    //when upload progresses	
    function On1Progress(event, position, total, percentComplete)
    {
        //Progress bar
        console.log("lima");
        progress1bar.width(percentComplete + '%'); //update progressbar percent complete
        status1txt.html(percentComplete + '%'); //update status text
        if(percentComplete>50)
        {
            status1txt.css('color','#fff'); //change status text to white after 50%
        }
    }
    //when upload progresses	
    function OnProgress(event, position, total, percentComplete)
    {
        //Progress bar
        console.log("lima");
        progressbar.width(percentComplete + '%') //update progressbar percent complete
        statustxt.html(percentComplete + '%'); //update status text
        if(percentComplete>50)
        {
            statustxt.css('color','#fff'); //change status text to white after 50%
        }
    }
    
    function  show1Error(response){
        alert(response);
        
    }
    function  showError(response){
        alert(response);
        
    }
    
    //after succesful upload
    function after1Success()
    {
        console.log("enam");
        progress1box.hide();
        var num =  Math.floor((Math.random() * 1000000) + 1);
        $("#modal_1photo").attr("src","<?php echo URL_IMAGE_TEMP;?>"+file_1name+"?versi="+num);
    }
    //after succesful upload
    function afterSuccess()
    {
//        console.log("enam");
        progressbox.hide();
        var num =  Math.floor((Math.random() * 1000000) + 1);
        $("#modal_photo").attr("src","<?php echo URL_IMAGE_TEMP;?>"+file_name+"?versi="+num);
    }
    
    //function to check file size before uploading.
    function before1Submit(){
        //check whether browser fully supports all File API
        console.log("tuju");
       if (window.File && window.FileReader && window.FileList && window.Blob)
            {
                if( !$('#modal_1photo_upload').val()) //check empty input filed
                {
                    $("#out1put").html("Please select file?");
                    return false
                }

                var f1size = $('#modal_1photo_upload')[0].files[0].size; //get file size
                var f1type = $('#modal_1photo_upload')[0].files[0].type; // get file type
                    //allow only valid image file types 
                switch(f1type)
                {
                    case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
                        break;
                    default:
                        $("#out1put").html("<b>"+f1type+"</b> Unsupported file type!");
                        return false
                }

                //Allowed file size is less than 1 MB (1048576)
                if(f1size>1048576) 
                {
                    $("#out1put").html("<b>"+bytesToSize(f1size) +"</b> Too big Image file! <br />Please reduce the size of your photo using an image editor.");
                    return false
                }

                //Progress bar
                progress1box.show(); //show progressbar
                progress1bar.width(comple1ted); //initial value 0% of progressbar
                status1txt.html(comple1ted); //set status text
                status1txt.css('color','#000'); //initial color of status text
                $("#out1put").html("");  
            }
            else
            {
                //Output error to older unsupported browsers that doesn't support HTML5 File API
                $("#out1put").html("Please upgrade your browser, because your current browser lacks some new features we need!");
                return false;
            }
    }
    //function to check file size before uploading.
    function beforeSubmit(){
        //check whether browser fully supports all File API
        console.log("tuju");
       if (window.File && window.FileReader && window.FileList && window.Blob)
            {
                if( !$('#modal_photo_upload').val()) //check empty input filed
                {
                    $("#output").html("Please select file?");
                    return false
                }

                var fsize = $('#modal_photo_upload')[0].files[0].size; //get file size
                var ftype = $('#modal_photo_upload')[0].files[0].type; // get file type
                    //allow only valid image file types 
                switch(ftype)
                {
                    case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
                        break;
                    default:
                        $("#output").html("<b>"+ftype+"</b> Unsupported file type!");
                        return false
                }

                //Allowed file size is less than 1 MB (1048576)
                if(fsize>1048576) 
                {
                    $("#output").html("<b>"+bytesToSize(fsize) +"</b> Too big Image file! <br />Please reduce the size of your photo using an image editor.");
                    return false
                }

                //Progress bar
                progressbox.show(); //show progressbar
                progressbar.width(completed); //initial value 0% of progressbar
                statustxt.html(completed); //set status text
                statustxt.css('color','#000'); //initial color of status text
                $("#output").html("");  
            }
            else
            {
                //Output error to older unsupported browsers that doesn't support HTML5 File API
                $("#output").html("Please upgrade your browser, because your current browser lacks some new features we need!");
                return false;
            }
    }

    //function to format bites bit.ly/19yoIPO
    function bytesToSize(bytes) {
        console.log("delapan");
       var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
       if (bytes == 0) return '0 Bytes';
       var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
       return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
    }
    
    function buka_modal(){
        file_1name = "";
        file_name = "";
        $("#modal_photo").attr("src","<?php echo URL_IMAGE_IKATEGORI;?>no_image.png");
        $("#modal_1photo").attr("src","<?php echo URL_IMAGE_IKATEGORI;?>no_image.png");
        $("#id_nama").val("");
        $("#id_tipe").val("").trigger("liszt:updated");
        $("#id_level").val("").trigger("liszt:updated");
        $("#form_id").val("");
        $("#modal_data").modal("show");
    }
    
    $("#form_data").submit(function (e){
        e.preventDefault();
        $("#form_loading").show();
        if (file_1name=="" || file_name==""){
            $("#form_notif").text("Silahkan upload icon");
            $("#form_notif").fadeIn(1000);
        }else{
            $.ajax({
                url: "<?php echo URL_OPA.'mst_kategori_usaha/insert_kategori'; ?>",
                data: { "nama":$("#id_nama").val(),
                        "tipe":$("#id_tipe").val(),
                        "level":$("#id_level").val(),
                        "id":$("#form_id").val(),
                        "ibiru":file_1name,
                        "ihitam":file_name
                        },
                type: 'POST',
                dataType: 'json',
                success: function (data, textStatus, jqXHR) {
                    console.log("OK");
                    $("#form_loading").fadeOut(1000, function (){
                        $("#form_notif").text(data.status);
                        $("#form_notif").show();
                        $("#form_notif").fadeOut(2000, function (){
                            json_kategori=data.list;
                            load_kategori(json_kategori);
                            $("#modal_data").modal("hide");
                        });
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log("NO");
                }
            });
        }
    });
    
    function edit_modal(i){
        file_1name  = json_kategori[i].icon_biru;
        file_name   = json_kategori[i].icon_hitam;
        var dir = "<?php echo URL_IMAGE_IKATEGORI; ?>";
        $("#modal_photo").attr("src",dir+file_name+'?version='+json_kategori[i].versi_icon_hitam);
        $("#modal_1photo").attr("src",dir+file_1name+'?version='+json_kategori[i].versi_icon_biru);
        $("#modal_data").modal("show");
        $("#form_id").val(json_kategori[i].id_jenis);
        $("#id_nama").val(json_kategori[i].nama);
        $("#id_tipe").val(json_kategori[i].tipe).trigger("liszt:updated");
        $("#id_level").val(json_kategori[i].id_level).trigger("liszt:updated");
    }
</script>