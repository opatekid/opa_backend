<div class="page-header">
    <div class="pull-left">
            <h1><?php echo $pengguna->nama; ?></h1>
    </div>
    <div class="pull-right">
        <ul class="stats">
            <li class='lightred'>
                <i class="icon-calendar"></i>
                <div class="details">
                    <span class="big">February 22, 2013</span>
                    <span>Wednesday, 13:56</span>
                </div>
            </li>
        </ul>
    </div>
</div>
<div class="breadcrumbs">
        <ul>
            <li>
                <a href="index.html">Upload Produk <?php echo $pengguna->nama; ?></a>
            </li>
        </ul>
        <div class="close-bread">
                <a href="#"><i class="icon-remove" style="display: none;" ></i></a>
        </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <div class="box">
        </div>
    </div>
</div>
<div class="row-fluid">
    <div class="span12">
        <div class="box box-color box-bordered">
            <div class="box-title">
                <h3><i class="icon-table"></i>Upload database produk</h3>
            </div>
            <div class="box-content nopadding"  style="overflow: scroll">
                <form id="form_data" class="form-horizontal" enctype="multipart/form-data">
                    <div class="control-group" style=" margin: 20px;">
                        <label class="control-label">Data XLS Terakhir</label>
                        <div class="controls" style="margin-top: 5px;">
                            <label><a href="<?php echo URL_XLSX_PRODUK . $pengguna->produk_filename; ?>"><?php echo $pengguna->produk_filename; ?></a></label>
                            <input type="file" id="xls_form_file" name="form_file" required class="input-xlarge" style="display: none;">
                            <label id="xls_filename" class="control-label"></label>
                        </div>
                    </div>
                    <div class="control-group" style=" margin: 20px;">
                        <label class="control-label">Overwrite data lama</label>
                        <div class="controls" style="margin-top: 5px;">
                            <input type="checkbox" id="overwrite" name="overwrite" class="input-xlarge" style="margin-bottom: 5px">
                            <span class="add-on" style="margin-left: 10px;">Centang untuk menimpa data lama. Kosongkan untuk menambah entri baru.</span>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="button" id="browse_button" class="btn btn-primary-success" style="margin-right: 10px;"><i class="icon-search"></i><span id="upload_span"> Browse</span></button>
                        <button type="submit" id="upload_button" class="btn btn-primary"><i class="icon-save"></i><span id="upload_span"> Upload</span></button>
                        <span id="form_notif"></span>
                        <span><img id="form_loading" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <div class="box box-color box-bordered">
            <div class="box-title">
                <h3><i class="icon-table"></i>Upload gambar produk</h3>
            </div>
            <div class="box-content nopadding"  style="overflow: scroll">
                <form id="image_form_data" class="form-horizontal" enctype="multipart/form-data">
                    <div class="control-group" style=" margin: 20px;">
                        <label class="control-label">Berkas ZIP Terakhir</label>
                        <div class="controls" style="margin-top: 5px;">
                            <label><a href="<?php echo URL_IMAGE_PRODUK_WEBADMIN . $pengguna->zip_filename; ?>"><?php echo $pengguna->zip_filename; ?></a></label>
                            <input type="file" id="form_file" name="form_file" required class="input-xlarge" style="display: none;">
                            <input type="text" id="foto" name="foto" value="foto.zip" required class="input-xlarge" style="display: none;">
                            <label id="filename" class="control-label"></label>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="button" id="image_browse_button" class="btn btn-primary-success" style="margin-right: 10px;"><i class="icon-search"></i><span id="image_upload_span"> Browse</span></button>
                        <button type="submit" id="image_upload_button" class="btn btn-primary"><i class="icon-save"></i><span id="image_upload_span"> Upload</span></button>
                        <span id="image_form_notif"></span>
                        <span><img id="image_form_loading" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<!--<div class="row-fluid">
    <div class="span-12">
     
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Pendapatan Perbulan</h3>
            <div class="box-tools pull-right">
              <button class="btn btn-box-tool" data-widget="collapse"><i class="icon-plus"></i></button>
              <button class="btn btn-box-tool" data-widget="remove"><i class="icon-remove" style="display: none;" ></i></button>
            </div>
          </div>
          <div class="box-body">
              <div class="chart">
              <canvas id="areaChart"></canvas>

            </div>
          </div> 
        </div> 
    </div>
</div>-->
<script>
    
    $(function () {

    $('#overwrite').change(function(e) {
        var v = $(this).is(':checked')? 1:0 ;
        console.log(v);
        $(this).val(v);
    });

    $('#browse_button').click(function() {
        console.log("browse");
        $("#xls_form_file").click();
    });


    $('#image_browse_button').click(function() {
        console.log("browse");
        $("#form_file").click();
    });

    $("#xls_form_file").change(function (){
       var fileName = $(this).val();
       var stringArray = fileName.split("\\");
       fileName = stringArray[stringArray.length-1];

       $("#xls_filename").html(fileName);
     });

    $("#form_file").change(function (){
       var fileName = $(this).val();
       var stringArray = fileName.split("\\");
       fileName = stringArray[stringArray.length-1];

       $("#filename").html(fileName);
     });

    $("#form_data").submit(function (e){
        e.preventDefault();
        $("#form_loading").show();
        $("#notif_username").hide();
        $("#notif_email").hide();
        $("#upload_button").attr('disabled', 'disabled');
        $("#upload_span").text("Uploading...");
        $("#form_notif").text("Mohon tidak menuntup tab/jendela ini selama proses importing berlangsung")
        $.ajax({
            url: "<?php echo URL_OPA.'mobile_v2/api_pemilik/insert_produk_merchant_batch/' . $pengguna->id_partner; ?>",
            data: new FormData($(this)[0]),
            type: 'POST',
            dataType: 'json',
            async : true,
            cache : false,
            contentType : false,
            processData : false,
            success: function (data, textStatus, jqXHR) {
                console.log("OK");
                $("#form_loading").fadeOut(1000, function (){
                    $("#upload_span").text("Uploaded");
                    if(data.code == '1'){
                        $("#form_notif").text(data.pesan);
                        $("#form_notif").show();
                        $("#form_notif").fadeOut(2000, function (){
                            produk=data.data;
                        console.log(produk);
                            load_produk(produk);
                            $("#modal_data").modal("hide");
                            buka_halaman('merchant_beranda',1);
                        });
                    } else {
                        $("#form_notif").text(data.pesan);
                    }
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("NO");
            }
        });
    });
    
    $("#image_form_data").submit(function (e){
        e.preventDefault();
        $("#image_form_loading").show();
        $("#notif_username").hide();
        $("#notif_email").hide();
        $("#image_upload_button").attr('disabled', 'disabled');
        $("#image_upload_span").text("Uploading...");
        $("#image_form_notif").text("Mohon tidak menuntup tab/jendela ini selama proses importing berlangsung")
        $.ajax({
            url: "<?php echo URL_OPA_WEBADMIN.'mobile_v2/api_pemilik/upload_foto_produk_master_batch'; ?>",
            data: new FormData($(this)[0]),
            type: 'POST',
            dataType: 'json',
            async : true,
            cache : false,
            contentType : false,
            processData : false,
            success: function (data, textStatus, jqXHR) {
                console.log("OK");
                $("#image_form_loading").fadeOut(1000, function (){
                    $("#image_upload_span").text("Uploaded");
                    if(data.code == '1'){
                        $("#image_form_notif").text(data.pesan);
                        $("#image_form_notif").show();
                        $("#image_form_notif").fadeOut(2000, function (){
                            $("#modal_data").modal("hide");
                            buka_halaman('merchant_beranda',1);
                        });
                    } else {
                        $("#image_form_notif").text(data.pesan);
                    }
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("NO");
            }
        });
    });
    
});


    
    
    
        
</script>