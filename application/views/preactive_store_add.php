<script></script>
<style type="text/css">
    #map {
        height: 1300px;
        width: 100%;
        position: relative;
    }
</style>
<div class="page-header">
    <div class="pull-left">
        <h1>Pre-Active Stores</h1>
    </div>
    <div class="pull-right">
        <ul class="stats">
            <li class='lightred'>
                <i class="icon-calendar"></i>
                <div class="details">
                    <span class="big">-, -</span>
                    <span>-, -</span>
                </div>
            </li>
        </ul>
    </div>
</div>
<div class="breadcrumbs">
    <ul>
        <li>
            <a>Pre-Active Stores</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a>Tambah Baru</a>
        </li>
    </ul>
    <div class="close-bread">
        <a href="#"><i class="icon-remove" style="display: none;" ></i></a>
    </div>
</div>
<div class="row-fluid">
    <div class="span12">
        <div class="box box-color box-bordered">
            <div class="box-title">
                <h3><i class="icon-table"></i>Tambah Data Toko</h3>
            </div>
            <div class="box-content nopadding" id="form_layout" style="overflow: scroll; height: 100%">
                <div class="span8">
                    <div id="map"></div>
                </div>
                <form id="form_data" class="span4" enctype="multipart/form-data" accept-charset="utf-8">
                    <div class="control-group">
                        <h4 style="margin-bottom: 20px; margin-top: 20px">PROFIL TOKO</h4>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Nama Usaha/Jargon/Merek</label>
                        <div class="controls">
                            <input type="text" id="nama" name="nama" required class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Provinsi</label>
                        <div class="controls">
                            <div class="input-xlarge" id="provinsi_form">
                                <select id="id_provinsi" name="id_provinsi" class='chosen-select input-xlarge' required>
                        </select>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Kota</label>
                        <div class="controls">
                            <div class="input-xlarge">
                                <select id="id_kota" name="id_kota" class='chosen-select input-xlarge' required>
                        </select>
                            </div>
                        </div>
                    </div>
                    <div class="control-group" id="description_form">
                        <label class="control-label">Alamat Usaha</label>
                        <div class="controls">
                            <input type="text" id="alamat" placeholder="Alamat Lengkap Usaha" name="alamat" class="input-xlarge" required />
                        </div>
                    </div>
                    <div class="control-group" id="jargon_form">
                        <label class="control-label">Jargon/Tagline</label>
                        <div class="controls">
                            <input type="text" id="jargon" name="jargon" placeholder="" class="input-xlarge" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Jenis Usaha</label>
                        <div class="controls">
                            <div class="input-xlarge">
                                <select id="id_jenis" name="id_jenis" class='chosen-select input-xlarge' required>
                        </select>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Kategori Usaha</label>
                        <div class="controls">
                            <div class="input-xlarge">
                                <select id="id_kategori" name="id_kategori" class='chosen-select input-xlarge' required>
                        </select>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Radius Layanan Toko</label>
                        <div class="controls">
                            <div class="input-xlarge">
                                <select id="id_radius" name="id_radius" class='chosen-select input-xlarge' required>
                        </select>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Gambar<span class="apt_bintang">*</span></label>
                        <div class="controls">
                            <input type="file" id="form_file" name="form_file" required class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <h4 style="margin-bottom: 20px; margin-top: 20px">PRODUK</h4>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Kategori Produk</label>
                        <div class="controls">
                            <div class="input-xlarge">
                                <select id="id_kategori_produk" name="id_kategori_produk" class='chosen-select input-xlarge' required>
                        </select>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Produk</label>
                        <div class="controls">
                            <input type="text" id="nama_produk" placeholder="Nama Produk" name="nama_produk" class="input-xlarge" required="" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Stok</label>
                        <div class="controls">
                            <input type="number" id="stok" name="stok" placeholder="" min="0" value="1" class="input-xlarge" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Harga</label>
                        <div class="controls">
                            <input type="number" id="harga" name="harga" placeholder="" min="0" value="0" class="input-xlarge" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Gambar<span class="apt_bintang">*</span></label>
                        <div class="controls">
                            <input type="file" id="form_file_produk" name="form_file_produk" required class="input-xlarge">
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary"><i class="icon-add"></i> Simpan</button>
                        <span id="form_notif"></span>
                        <span><img id="form_loading" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBuDrC5iMEnz7vgrcXHSwKqqPpmHCVrp-c&callback=initMap"></script>
<script>
    var currentMarker;
    var map;
    var lat;
    var lng;
    var id_provinsi;
    var id_kota;
    var markersArray = new Array();

    function sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    function initMap() {
        var latlng = {
            lat: -6.175421,
            lng: 106.827334
        };
        lat = latlng.lat;
        lng = latlng.lng;
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 16,
            center: latlng
        });

        addNewMarker(latlng);

        google.maps.event.addListener(map, 'click', function(event) {
            placeMarker(event.latLng);
        });

        function placeMarker(location) {
            lat = location.lat();
            lng = location.lng();
            console.log(lat + ", " + lng);

            currentMarker.setPosition(new google.maps.LatLng(lat, lng));
        }
        
        sleep(1000);
        load_pemilik();
    }

    function addNewMarker(latlng) {
        currentMarker = new google.maps.Marker({
            position: latlng,
            map: map,
            animation: google.maps.Animation.DROP
        });
    }

    $(document).ready(function() {
        $(".chosen-select").chosen({
            no_results_text: "Data tidak ada!",
            width: "100%",
            placeholder_text_single: "Pilih Salah Satu"
        });


        $('#id_provinsi').change(function() {
            id_provinsi = $('#id_provinsi').val();
            load_kota(id_provinsi);
        });

        $('#id_kota').change(function() {
            id_kota = $('#id_kota').val();
        });

    });

    function hide_show_forms(e) {
        if (e == 1) {
            $('#form_for_usaha').show();
            $('#code_count_form').prop('required', true);
        } else {
            $('#form_for_usaha').hide();
            $('#code_count_form').prop('required', false);
        }
    }

    function hide_show_code_form() {
        var e = $('#target').val();
        if (e == 1) {
            $('#code_count_form').show();
        } else {
            $('#code_count_form').hide();
        }
    }

    var id_pemilikes = [];

    function load_pemilik() {
        var awal = '';
        $.ajax({
            url: "<?php echo URL_OPA.'preactive_store/get_preactive_pemilik'; ?>",
            data: "",
            type: 'GET',
            dataType: 'json',
            success: function(data, textStatus, jqXHR) {
                var html = "";

                console.log(data);

                for (var i = 0; i < data.data.length; i++) {

                    var lat = parseFloat(data.data[i].lat);
                    var lng = parseFloat(data.data[i].lng);

                    var latlng = {
                        lat: lat,
                        lng: lng
                    };
                    var newMarker = new google.maps.Marker({
                        position: latlng,
                        map: map,
                        animation: google.maps.Animation.DROP
                    });
                    newMarker.setIcon('<?php echo URL_IMG . "plugins/gmap/"; ?>' + 'green-dot.png');

                    var contentString = "<b>" + data.data[i].nama + "</b><br>" + data.data[i].alamat;
                    var infowindow = new google.maps.InfoWindow();
                    
                    google.maps.event.addListener(newMarker,'click', (function(newMarker,contentString,infowindow){ 
                        return function() {
                            infowindow.setContent(contentString);
                            infowindow.open(map,newMarker);
                        };
                    })(newMarker,contentString,infowindow));  

                    sleep(500);

                }

                id_pemilikes = data.data;
            },
            error: function(jqXHR, textStatus, errorThrown) {

            }
        });
    }


    var id_provinsies = [];

    function load_provinsi() {
        var awal = '';
        $.ajax({
            url: "<?php echo URL_OPA.'mobile/api_pemilik/get_provinsi'; ?>",
            data: "",
            type: 'GET',
            dataType: 'json',
            success: function(data, textStatus, jqXHR) {
                var html = "";

                console.log(data);

                for (var i = 0; i < data.data.length; i++) {
                    var a = '<option value="' + data.data[i].id + '">' + data.data[i].nama + '</option>';
                    html = html + a;
                }
                id_provinsies = data.data;
                $('#id_provinsi').html(awal + html).trigger("liszt:updated");
                if (data.data.length > 0) {
                    load_kota(data.data[0].id);
                    id_provinsi = data.data[0].id;
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {

            }
        });
    }
    load_provinsi();

    var id_kotaes = [];

    function load_kota(id_provinsi) {
        var awal = '';
        console.log('id_provinsi = ' + id_provinsi);
        $.ajax({
            url: "<?php echo URL_OPA.'mobile/api_pemilik/get_kota'; ?>",
            data: "id_provinsi=" + id_provinsi,
            type: 'POST',
            dataType: 'json',
            success: function(data, textStatus, jqXHR) {
                var html = "";

                console.log(data);

                for (var i = 0; i < data.data.length; i++) {
                    var a = '<option value="' + data.data[i].id + '">' + data.data[i].nama + '</option>';
                    html = html + a;
                }
                id_kotaes = data.data;
                $('#id_kota').html(awal + html).trigger("liszt:updated");
                if (data.data.length > 0) {
                    id_kota = data.data[0].id;
                }

            },
            error: function(jqXHR, textStatus, errorThrown) {

            }
        });
    }

    var id_jenises = [];

    function load_jenis() {
        var awal = '';
        $.ajax({
            url: "<?php echo URL_OPA.'mobile/api_pemilik/get_jenis_kategori_usaha'; ?>",
            data: "",
            type: 'GET',
            dataType: 'json',
            success: function(data, textStatus, jqXHR) {
                var html = "";

                console.log(data);

                for (var i = 0; i < data.data.length; i++) {
                    var a = '<option value="' + data.data[i].id_jenis + '">' + data.data[i].nama + '</option>';
                    html = html + a;
                }
                id_jenises = data.data;
                $('#id_jenis').html(awal + html).trigger("liszt:updated");

            },
            error: function(jqXHR, textStatus, errorThrown) {

            }
        });
    }
    load_jenis();

    var id_kategories = [];

    function load_kategori() {
        var awal = '';
        $.ajax({
            url: "<?php echo URL_OPA.'mobile/api_pemilik/get_kategori_usaha'; ?>",
            data: "",
            type: 'GET',
            dataType: 'json',
            success: function(data, textStatus, jqXHR) {
                var html = "";

                console.log(data);

                for (var i = 0; i < data.data.length; i++) {
                    var a = '<option value="' + data.data[i].id_kategori + '">' + data.data[i].nama_kategori + '</option>';
                    html = html + a;
                }
                id_kategories = data.data;
                $('#id_kategori').html(awal + html).trigger("liszt:updated");

            },
            error: function(jqXHR, textStatus, errorThrown) {

            }
        });
    }
    load_kategori();


    var id_kategories_produk = [];

    function load_kategori_produk() {
        var awal = '';
        $.ajax({
            url: "<?php echo URL_OPA.'mobile/api_pemilik/get_kategori_produk'; ?>",
            data: "",
            type: 'GET',
            dataType: 'json',
            success: function(data, textStatus, jqXHR) {
                var html = "";

                console.log(data);

                for (var i = 0; i < data.data.length; i++) {
                    var a = '<option value="' + data.data[i].id_kategori + '">' + data.data[i].nama_kategori + '</option>';
                    html = html + a;
                }
                id_kategories_produk = data.data;
                $('#id_kategori_produk').html(awal + html).trigger("liszt:updated");

            },
            error: function(jqXHR, textStatus, errorThrown) {

            }
        });
    }
    load_kategori_produk();


    var id_radiuses = [];

    function load_radius() {
        var awal = '';
        $.ajax({
            url: "<?php echo URL_OPA.'mobile/api_pemilik/get_radius'; ?>",
            data: "",
            type: 'GET',
            dataType: 'json',
            success: function(data, textStatus, jqXHR) {
                var html = "";

                console.log(data);

                for (var i = 0; i < data.data.length; i++) {
                    var a = '<option value="' + data.data[i].id_radius + '">' + data.data[i].radius + ' m</option>';
                    html = html + a;
                }
                id_radiuses = data.data;
                $('#id_radius').html(awal + html).trigger("liszt:updated");

            },
            error: function(jqXHR, textStatus, errorThrown) {

            }
        });
    }
    load_radius();

    $("#form_data").submit(function(e) {
        e.preventDefault();
        currentMarker.setMap(null);
        var latlng = {
            lat: lat,
            lng: lng
        };
        var newMarker = new google.maps.Marker({
            position: latlng,
            map: map,
            animation: google.maps.Animation.DROP
        });
        newMarker.setIcon('<?php echo URL_IMG . "plugins/gmap/"; ?>' + 'green-dot.png');

        var contentString = "<b>" + $('#nama').val() + "</b><br>" + $('#alamat').val();
        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });
        
        infowindow.open(map, newMarker);
        newMarker.addListener('click', function() {
            infowindow.open(map, newMarker);
        });

        $("#form_loading").show();
        var dataPost = new FormData($(this)[0]);
        // dataPost = dataPost + "&lat=" + lat + "&lng=" + lng + "&id_provinsi=" + id_provinsi + "&id_kota=" + id_kota;
        dataPost.append("lat", lat);
        dataPost.append("lng", lng);
        dataPost.append("id_provinsi", id_provinsi);
        dataPost.append("id_kota", id_kota);
        $.ajax({
            url: "<?php echo URL_OPA.'preactive_store/create'; ?>",
            data: dataPost,
            type: 'POST',
            dataType: 'json',
            async : false,
            cache : false,
            contentType : false,
            processData : false,
            success: function(data, textStatus, jqXHR) {
                console.log("OK");
                if (data.code == 1) {
                    markersArray.push(newMarker);
                    var latlng = {
                        lat: -6.175421,
                        lng: 106.827334
                    };
                    addNewMarker(latlng)
                    $('#form_data').find("input[type=text], textarea").val("");
                    $("#form_loading").fadeOut(1000, function() {
                        $("#form_notif").text(data.status);
                        $("#form_notif").show();
                        $("#form_notif").fadeOut(2000, function() {});
                    });
                } else {

                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("NO");
            }
        });
    });
</script>