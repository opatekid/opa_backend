<div class="page-header">
    <div class="pull-left">
            <h1>Beranda</h1>
    </div>
    <div class="pull-right">
        <ul class="stats">
            <li class='lightred'>
                <i class="icon-calendar"></i>
                <div class="details">
                    <span class="big">February 22, 2013</span>
                    <span>Wednesday, 13:56</span>
                </div>
            </li>
        </ul>
    </div>
</div>
<div class="breadcrumbs">
        <ul>
            <li>
                <a href="index.html">Beranda</a>
            </li>
        </ul>
        <div class="close-bread">
                <a href="#"><i class="icon-remove" style="display: none;" ></i></a>
        </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <div class="box">
        </div>
    </div>
</div>
<div class="row-fluid">
    <div class="span12">
        <div class="box box-color box-bordered">
            <div class="box-title">
                    <h3>
                            <i class="icon-bar-chart"></i>
                            Grafik Pendapatan Tahun ini
                    </h3>

            </div>
            <div class="box-content">
                <div id="chart_skt" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
            </div>
        </div>
    </div>
</div>


<!--<div class="row-fluid">
    <div class="span-12">
     
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Pendapatan Perbulan</h3>
            <div class="box-tools pull-right">
              <button class="btn btn-box-tool" data-widget="collapse"><i class="icon-plus"></i></button>
              <button class="btn btn-box-tool" data-widget="remove"><i class="icon-remove" style="display: none;" ></i></button>
            </div>
          </div>
          <div class="box-body">
              <div class="chart">
              <canvas id="areaChart"></canvas>

            </div>
          </div> 
        </div> 
    </div>
</div>-->
<script>
    var json_total = <?php echo json_encode($total);?>;
    
    $(function () {
//    $('#button3').click(function() {
//        chart.hideLoading();
//        chart.showLoading();
//    });
//    $('#button4').click(function() {
//        chart.hideLoading();
//    });
    
    
    // create the chart
    var chart = new Highcharts.Chart({
        chart: {
            renderTo: 'chart_skt',
            events: {
                load: function(){
                    //this.showLoading();
                }
            }
        },
        title: {
            text: ''
        },
        exporting: { enabled: false },    
        xAxis: {
            categories: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"]
        },
        yAxis: {
                allowDecimals: false,
                min: 0,
                title: {
                    text: 'Jumlah'
                },
                labels: {
                    formatter: function() {
                        return kurensi(this.value);
                    }
                }
            },
        credits: {
            enabled: false
        },
        series: [{
            data: [parseInt(json_total[0].total), parseInt(json_total[1].total), parseInt(json_total[2].total), parseInt(json_total[3].total),
                  parseInt(json_total[4].total), parseInt(json_total[5].total), parseInt(json_total[6].total), parseInt(json_total[7].total), 
                  parseInt(json_total[8].total), parseInt(json_total[9].total), parseInt(json_total[10].total), parseInt(json_total[11].total)],
            name: 'Pendapatan'
                }]
    });
});
    
    
    
    
//    var json_total = <?php //echo json_encode($total);?>;
//    
//
//        var lineChartData = {
//            labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "Desember"],
//          datasets: [
//            
//            {
//              label: "Digital Goods",
//              fillColor: "rgba(60,141,188,0.9)",
//              strokeColor: "rgba(60,141,188,0.8)",
//              pointColor: "#3b8bba",
//              pointStrokeColor: "rgba(60,141,188,1)",
//              pointHighlightFill: "#fff",
//              pointHighlightStroke: "rgba(60,141,188,1)",
//              data: [parseInt(json_total[0].total), parseInt(json_total[1].total), parseInt(json_total[2].total), parseInt(json_total[3].total),
//                  parseInt(json_total[4].total), parseInt(json_total[5].total), parseInt(json_total[6].total), parseInt(json_total[7].total), 
//                  parseInt(json_total[8].total), parseInt(json_total[9].total), parseInt(json_total[10].total), parseInt(json_total[11].total)]
//            }
//          ]
//
//        };
//        
//        var ctx = document.getElementById("areaChart").getContext("2d");
//        ctx.canvas.width = 100;
//        ctx.canvas.height = 100;
//        var my_chart = new Chart(ctx).Line(lineChartData, {
//                responsive: true,
//                scaleLabel : "<%= 'Rp. '+kurensi(value)%>",
//                tooltipTemplate: "<%if (label){%><%=label %>: <%}%><%= 'Rp. '+kurensi(value+'') %>",
//                multiTooltipTemplate: "<%= 'Rp. '+kurensi(value+'')%>"
//        });
        
</script>