<script>
    var provinsi = <?php echo json_encode($provinsi)?>;
    var usaha = <?php echo json_encode($usaha)?>;
    var kategori = <?php echo json_encode($kategori)?>;
    console.log(kategori);
    var carii ="";
    function load_cari(cari){
        var html ="";
        for(var i =0;i<cari.length;i++){
            var a ='<tr>\n\
            <td>'+(i+1)+'</td>\n\
            <td>'+cari[i].tanggal_order+'</td>\n\
            <td>'+cari[i].nama+'</td>\n\
            <td>'+cari[i].alamat+'</td>\n\
            <td>'+cari[i].nama_provinsi+'</td>\n\
            <td>'+cari[i].nama_kota+'</td>\n\
        </tr>';
        
        html = html + a;
    }
    $('#isi_tabel').html(html);
    $("#id_tabel").dataTable();
}
function load_provinsi(provinsi){
    var awal = '<option value="0">-All-</option>';
    var html = "";
    
    for (var i=0;i<provinsi.length;i++){
        var a ='<option value="'+provinsi[i].id+'">'+provinsi[i].nama+'</option>';
        html = html + a;
    }

    $('#id_provinsi').html(awal+html).trigger("liszt:updated");
    $('#id_kota').html(awal).trigger("liszt:updated");

}
function load_usaha(usaha){
    var awal = '<option value="">-Pilih Usaha-</option>';
    var html = "";
    
    for (var i=0;i<usaha.length;i++){
        var a ='<option value="'+usaha[i].id_pemilik+'">'+usaha[i].nama+'</option>';
        html = html + a;
    }

    $('#id_usaha').html(awal+html).trigger("liszt:updated");

}

function load_kategori(kategori){
    var awal = '';
    var html = "";
    
    for (var i=0;i<kategori.length;i++){
        var a ='<option value="'+kategori[i].id_kategori+'">'+kategori[i].nama_kategori+'</option>';
        html = html + a;
    }

    $('#id_kategori').html(awal+html).trigger("liszt:updated");

}

</script>
<div class="page-header">
    <div class="pull-left">
        <h1>Promo</h1>
    </div>
    <div class="pull-right">
        <ul class="stats">
            <li class='lightred'>
                <i class="icon-calendar"></i>
                <div class="details">
                    <span class="big">-, -</span>
                    <span>-, -</span>
                </div>
            </li>
        </ul>
    </div>
</div>
<div class="breadcrumbs">
    <ul>
        <li>
            <a>Promo</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a>Create Promo</a>
        </li>
    </ul>
    <div class="close-bread">
        <a href="#"><i class="icon-remove" style="display: none;" ></i></a>
    </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <div class="box box-color box-bordered">
            <div class="box-title">
                <h3><i class="icon-table"></i>Untuk Konsumen</h3>
            </div>
            <div class="box-content nopadding"  style="overflow: scroll">
                <form id="form_data" class="form-horizontal" enctype="multipart/form-data">
                    <div class="control-group" style=" margin: 20px;">
                        <label class="control-label">Nama Promo</label>
                        <div class="controls">
                            <input type="text" id="nama" name="nama" required class="input-xlarge" >
                        </div>
                    </div>
                    <div class="control-group" id="description_form" style=" margin: 20px;">
                        <label class="control-label">Deskripsi promo</label>
                        <div class="controls">
                            <textarea type="text" id="description" placeholder="Teks deskripsi promo pada tampilan input kode" name="description" class="input-xlarge" />
                        </div>
                    </div>
                    <div class="control-group" style=" margin: 20px; display: none;">
                        <label class="control-label">Promo Untuk</label>
                        <div class="controls">
                            <div class="input-xlarge">
                                <select id="promo_for" onchange="hide_show_forms(this.value);" name="promo_for" class='chosen-select input-xlarge' required>
                                    <option value="2" selected="">User Konsumen</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div id="form_for_usaha">
                        <div class="control-group" style=" margin: 20px; display: none;">
                            <label class="control-label">Jenis Promo</label>
                            <div class="controls">
                                <div class="input-xlarge">
                                    <select id="jenis_promo" name="jenis_promo" class='chosen-select input-xlarge' required>
                                        <option value="4">Custom</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="control-group" style=" margin: 20px;">
                            <label class="control-label">Jenis Usaha</label>
                            <div class="controls">
                                <div class="input-xlarge">
                                    <select id="id_kategori" name="id_kategori" class='chosen-select input-xlarge' required>
                                        
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="control-group" style=" margin: 20px;">
                            <label class="control-label">Maksimal pembelian per</label>
                            <div class="controls">
                                <input type="number" min="1" id="max_purchase" value="1" placeholder="Maksimal pembelian per satuan waktu" name="max_purchase" class="input-xlarge" >
                            </div>
                        </div>
                        <div class="control-group" style=" margin: 20px;">
                            <label class="control-label">Satuan Waktu</label>
                            <div class="controls">
                                <div class="input-xlarge">
                                    <select id="time_unit" name="time_unit" class='chosen-select input-xlarge' required>
                                        <option value="1">Hari</option>
                                        <option value="2">Minggu</option>
                                        <option value="3">Bulan</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="control-group" style=" margin: 20px; display: none;">
                            <label class="control-label">Target</label>
                            <div class="controls">                        
                                <div class="input-xlarge">
                                    <select id="target" name="target" onchange="hide_show_code_form()" class='chosen-select input-xlarge' required>
                                        <option value="1" selected="">User with code</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="control-group" id="code_count_form" style=" margin: 20px;">
                            <label class="control-label">Jumlah toko peserta</label>
                            <div class="controls">
                                <input type="number" min="1" id="code_count" value="1" placeholder="Number of codes to generate" name="code_count" class="input-xlarge" >
                            </div>
                        </div> -->
                    </div>
                    <div class="control-group" id="message_form" style=" margin: 20px;">
                        <label class="control-label">Pesan setelah aktivasi</label>
                        <div class="controls">
                            <textarea  id="message" placeholder="Pesan berhasil" name="message" class="input-xlarge" />
                        </div>
                    </div>
                    <div class="control-group" id="message_form" style=" margin: 20px;">
                        <label class="control-label"></label>
                        <div class="controls">
                            <textarea  id="fail_message" placeholder="Pesan gagal" name="fail_message" class="input-xlarge" />
                        </div>
                    </div>
                    <div class="control-group" style=" margin: 20px;">
                    <label class="control-label">Jumlah konsumen</label>
                        <div class="controls">
                            <input type="number" min="1" id="max_konsumen" value="1" placeholder="Jumlah maksimum konsumen berlangganan per toko" name="max_konsumen" class="input-xlarge" >
                        </div>
                    </div>
                    <div class="control-group" style=" margin: 20px;">
                        <label class="control-label">Masa berlaku</label>
                        <div class="controls">
                            <input type="number" min="1" id="valid_duration" value="1" placeholder="Masa berlaku promo dalam hitungan bulan" name="valid_duration" class="input-xlarge" > <span class="add-on">Bulan</span>
                        </div> 
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary"><i class="icon-add"></i> Tambahkan Produk >></button>
                        <span id="form_notif"></span>
                        <span><img id="form_loading" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    load_provinsi(provinsi);
    load_usaha(usaha);
    load_kategori(kategori);
    $(document).ready(function(){
        $(".chosen-select").chosen({
            no_results_text: "Data tidak ada!",
            width: "100%",
            placeholder_text_single : "Pilih Salah Satu"
        });
    });

    function hide_show_forms(e){
        if (e == 1) {
            $('#form_for_usaha').show();
            $('#code_count_form').prop('required', true);
        } else {
            $('#form_for_usaha').hide();
            $('#code_count_form').prop('required', false);
        }
    }

    function hide_show_code_form(){
        var e = $('#target').val();
        if (e == 1) {
            $('#code_count_form').show();
        } else {
            $('#code_count_form').hide();
        }
    }

    $("#form_data").submit(function(e){
        e.preventDefault();
            $("#form_loading").show();
            $.ajax({
                url: "<?php echo URL_OPA.'promo_old/create_konsumen'; ?>",
                data: $('#form_data').serialize(),
                type: 'POST',
                dataType: 'json',
                success: function (data, textStatus, jqXHR) {
                    console.log("OK");
                    buka_halaman('promo_old/set_produk_promo/'+data.id_promo, '')
                    $('#form_data').find("input[type=text], textarea").val("");
                    $("#form_loading").fadeOut(1000, function (){
                        $("#form_notif").text(data.status);
                        $("#form_notif").show();
                        $("#form_notif").fadeOut(2000, function (){
                        });
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log("NO");
                }
            });
    });
</script>