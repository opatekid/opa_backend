<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<!-- Apple devices fullscreen -->
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<!-- Apple devices fullscreen -->
	<meta names="apple-mobile-web-app-status-bar-style" content="black-translucent" />
	
	<title>OPA</title>

	<!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo URL_CSS; ?>bootstrap.min.css">
	<!-- Bootstrap responsive -->
	<link rel="stylesheet" href="<?php echo URL_CSS; ?>bootstrap-responsive.min.css">
	<!-- jQuery UI -->
	<link rel="stylesheet" href="<?php echo URL_CSS; ?>plugins/jquery-ui/smoothness/jquery-ui.css">
	<link rel="stylesheet" href="<?php echo URL_CSS; ?>plugins/jquery-ui/smoothness/jquery.ui.theme.css">
	<!-- PageGuide -->
	<link rel="stylesheet" href="<?php echo URL_CSS; ?>plugins/pageguide/pageguide.css">
	<!-- Fullcalendar -->
	<link rel="stylesheet" href="<?php echo URL_CSS; ?>plugins/fullcalendar/fullcalendar.css">
	<link rel="stylesheet" href="<?php echo URL_CSS; ?>plugins/fullcalendar/fullcalendar.print.css" media="print">
	<!-- chosen -->
	<link rel="stylesheet" href="<?php echo URL_CSS; ?>plugins/chosen/chosen.css">
	<!-- select2 -->
	<link rel="stylesheet" href="<?php echo URL_CSS; ?>plugins/select2/select2.css">
	<!-- icheck -->
	<link rel="stylesheet" href="<?php echo URL_CSS; ?>plugins/icheck/all.css">
	<!-- Theme CSS -->
	<link rel="stylesheet" href="<?php echo URL_CSS; ?>style.css">
	<!-- Color CSS -->
	<link rel="stylesheet" href="<?php echo URL_CSS; ?>themes.css">


	<!-- jQuery -->
	<script src="<?php echo URL_JS; ?>jquery.min.js"></script>
	
        <script src="<?php echo URL_JS; ?>aptikma.js"></script>
	
	<!-- Nice Scroll -->
	<script src="<?php echo URL_JS; ?>plugins/nicescroll/jquery.nicescroll.min.js"></script>
	<!-- jQuery UI -->
	<script src="<?php echo URL_JS; ?>plugins/jquery-ui/jquery.ui.core.min.js"></script>
	<script src="<?php echo URL_JS; ?>plugins/jquery-ui/jquery.ui.widget.min.js"></script>
	<script src="<?php echo URL_JS; ?>plugins/jquery-ui/jquery.ui.mouse.min.js"></script>
	<script src="<?php echo URL_JS; ?>plugins/jquery-ui/jquery.ui.draggable.min.js"></script>
	<script src="<?php echo URL_JS; ?>plugins/jquery-ui/jquery.ui.resizable.min.js"></script>
	<script src="<?php echo URL_JS; ?>plugins/jquery-ui/jquery.ui.sortable.min.js"></script>
	<!-- Touch enable for jquery UI -->
	<script src="<?php echo URL_JS; ?>plugins/touch-punch/jquery.touch-punch.min.js"></script>
	<!-- slimScroll -->
	<script src="<?php echo URL_JS; ?>plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<!-- Bootstrap -->
	<script src="<?php echo URL_JS; ?>bootstrap.min.js"></script>
	<!-- vmap -->
	<script src="<?php echo URL_JS; ?>plugins/vmap/jquery.vmap.min.js"></script>
	<script src="<?php echo URL_JS; ?>plugins/vmap/jquery.vmap.world.js"></script>
	<script src="<?php echo URL_JS; ?>plugins/vmap/jquery.vmap.sampledata.js"></script>
	<!-- Bootbox -->
	<script src="<?php echo URL_JS; ?>plugins/bootbox/jquery.bootbox.js"></script>
	<!-- Flot -->
	<script src="<?php echo URL_JS; ?>plugins/flot/jquery.flot.min.js"></script>
	<script src="<?php echo URL_JS; ?>plugins/flot/jquery.flot.bar.order.min.js"></script>
	<script src="<?php echo URL_JS; ?>plugins/flot/jquery.flot.pie.min.js"></script>
	<script src="<?php echo URL_JS; ?>plugins/flot/jquery.flot.resize.min.js"></script>
	<!-- imagesLoaded -->
	<script src="<?php echo URL_JS; ?>plugins/imagesLoaded/jquery.imagesloaded.min.js"></script>
	<!-- PageGuide -->
	<script src="<?php echo URL_JS; ?>plugins/pageguide/jquery.pageguide.js"></script>
	<!-- FullCalendar -->
	<script src="<?php echo URL_JS; ?>plugins/fullcalendar/fullcalendar.min.js"></script>
	<!-- Chosen -->
	<script src="<?php echo URL_JS; ?>plugins/chosen/chosen.jquery.min.js"></script>
	<!-- select2 -->
	<script src="<?php echo URL_JS; ?>plugins/select2/select2.min.js"></script>
	<!-- icheck -->
	<script src="<?php echo URL_JS; ?>plugins/icheck/jquery.icheck.min.js"></script>
    <!-- ChartJS 1.0.1 -->
    <script src="<?php echo URL_JS; ?>plugins/chartjs/Chart.min.js"></script>
    
<!--        <script src="<?php //echo URL_JS; ?>/plugins/highchart/js/highcharts.js"></script>-->
    <!-- 
    <script type="text/javascript" src="http://highcharts.com/js/testing.js"></script> -->
<script src='<?php echo URL_JS; ?>highcharts.js'></script>
<script src='<?php echo URL_JS; ?>exporting.js'></script>
    
        
<!--        <script src="<?php //echo URL_JS; ?>plugins/datatable/jquery.dataTables.min.js"></script>-->
        <script src="<?php echo URL_JS; ?>plugins/datatable/2jquery.dataTables.min.js"></script>
        <!-- <script src="<?php // echo URL_JS; ?>plugins/datatable/dataTables.bootstrap.min.js"></script> -->
        
        <link rel="stylesheet" href="<?php echo URL_CSS; ?>plugins/chosen/chosen.css">
        <link rel="stylesheet" href="<?php echo URL_CSS; ?>plugins/select2/select2.css">
        <script src="<?php echo URL_JS; ?>plugins/select2/select2.min.js"></script>
        <script src="<?php echo URL_JS; ?>plugins/chosen/chosen.jquery.min.js"></script>
        <link rel="stylesheet" href="<?php echo URL_CSS; ?>plugins/datepicker/datepicker.css">
        <script src="<?php echo URL_JS; ?>plugins/datepicker/bootstrap-datepicker.js"></script>
        <!-- icheck -->
	<link rel="stylesheet" href="<?php echo URL_JS; ?>plugins/icheck/all.css">
        <!-- icheck -->
	<script src="<?php echo URL_JS; ?>plugins/icheck/jquery.icheck.min.js"></script>
	<!-- Theme framework -->
	<script src="<?php echo URL_JS; ?>eakroko.min.js"></script>
	<!-- Theme scripts -->
	<script src="<?php echo URL_JS; ?>application.min.js"></script>
	<!-- Just for demonstration -->
	<script src="<?php echo URL_JS; ?>demonstration.min.js"></script>
	
	<script src="<?php echo URL_JS; ?>jquery.form.min.js"></script>
	
	<!--[if lte IE 9]>
		<script src="js/plugins/placeholder/jquery.placeholder.min.js"></script>
		<script>
			$(document).ready(function() {
				$('input, textarea').placeholder();
			});
		</script>
	<![endif]-->

	<!-- Favicon -->
	<link rel="shortcut icon" href="img/favicon.ico" />
	<!-- Apple devices Homescreen icon -->
	<link rel="apple-touch-icon-precomposed" href="img/apple-touch-icon-precomposed.png" />
        
<style>
    .loader {
            position: fixed;

            display: none;

            width: 100%;
            height: 100%;
            z-index: 10;
            background: url('<?php echo URL_IMG;?>page-loader.gif') 50% 50% no-repeat rgb(249,249,249);
    }

</style>
<script>
    function datetime_indo_convert(date) {
        var tanggal = '';
        var result="";
        tanggal = date;
        
        var BulanIndo = ["Januari", "Februari", "Maret",
						   "April", "Mei", "Juni",
						   "Juli", "Agustus", "September",
						   "Oktober", "November", "Desember"];
	//2015-11-11 11:11
        
        if(date == "0000-00-00" || date == null || date == "null"){
            result = "Tanggal tidak ada";
        }else{
            var tahun = tanggal.substr(0,4);
            var bulan = tanggal.substr(5,2);
            var tgl   = tanggal.substr(8,2);
            var jam = tanggal.substr(11,5);
            result = tgl + " " + BulanIndo[bulan-1] + " "+ tahun+" "+jam;
        }
        
        return(result);
    }
    
    function date_indo_convert(date) {
        var tanggal = '';
        var result="";
        tanggal = date;
        
        var BulanIndo = ["Januari", "Februari", "Maret",
						   "April", "Mei", "Juni",
						   "Juli", "Agustus", "September",
						   "Oktober", "November", "Desember"];
	//2015-11-11 11:11
        
        if(date == "00-00-0000" || date == null || date == "null"){
            result = "Tanggal tidak ada";
        }else{
            var tahun = tanggal.substr(6,4);
            var bulan = tanggal.substr(3,2);
            var tgl   = tanggal.substr(0,2);
            result = tgl + " " + BulanIndo[bulan-1] + " "+ tahun;
        }
        
        return(result);
    }
    
    function date_indo_convert2(date) {
        var tanggal = '';
        var result="";
        tanggal = date;
        
        var BulanIndo = ["Januari", "Februari", "Maret",
						   "April", "Mei", "Juni",
						   "Juli", "Agustus", "September",
						   "Oktober", "November", "Desember"];
	//2015-11-11 11:11
        
        if(date == "0000-00-00" || date == null || date == "null"){
            result = "Tanggal tidak ada";
        }else{
            var tahun = tanggal.substr(0,4);
            var bulan = tanggal.substr(5,2);
            var tgl   = tanggal.substr(8,2);
            result = tgl + " " + BulanIndo[bulan-1] + " "+ tahun;
        }
        
        return(result);
    }
</script>