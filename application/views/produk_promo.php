    <style type="text/css">

    .modal {
    display: block !important; /* I added this to see the modal, you don't need this */
    }

    /* Important part */
    .modal-dialog {
        overflow-y: initial !important
    }
    #data-modal-body {
        height: auto;
        overflow-y: auto;
    }
</style>
<script>
        
    function load_produk_promo(tran_upgrade){
        //$("#id_tabel").dataTable().fnDestroy();
        var html ="";
        var tipe ="";
        for(var i =0;i<tran_upgrade.length;i++){
            var a ='<tr>\n\
                        <td>'+(i+1)+'</td>\n\
                        <td>'+tran_upgrade[i].id_barcode+'</td>\n\
                        <td>'+tran_upgrade[i].qty+'</td>\n\
                        <td>'+tran_upgrade[i].id_barcode2+'</td>\n\
                        <td>'+tran_upgrade[i].qty2+'</td>\n\
                    </tr>';
            
            html = html + a;
        }
        $('#isi_tabel').html(html);
        $("#id_tabel").dataTable();
    }

    function load_produk_diskon(tran_upgrade){
        //$("#id_tabel").dataTable().fnDestroy();
        var html ="";
        var tipe ="";
        for(var i =0;i<tran_upgrade.length;i++){
            var a ='<tr>\n\
                        <td>'+(i+1)+'</td>\n\
                        <td>'+tran_upgrade[i].id_produk+'</td>\n\
                        <td>'+ (tran_upgrade[i].harga_promo != null ? tran_upgrade[i].harga_promo != null : '<i>ID produk invalid</i>') +'</td>\n\
                    </tr>';
            
            html = html + a;
        }
        $('#isi_tabel').html(html);
        $("#id_tabel").dataTable();
    }

    function load_level(level){
        var awal = '<option value="">-Pilih Level-</option>';
        var html = "";
        
        for (var i=0;i<level.length;i++){
            var a ='<option value="'+level[i].id+'">'+level[i].nama+'</option>';
           html = html + a;
        }

        $('#id_level').html(awal+html).trigger("liszt:updated");

    }
    // load_opsi(json_role);
</script>

<div id="modal_data" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-header">
        <a type="button" class="close" data-dismiss="modal" aria-hidden="true">x</a>
        <h3 id="">Data Gerai</h3>
    </div>
    <form id="form_data" class="form-horizontal" enctype="multipart/form-data">
            <div id="data-modal-body" class="modal-body">
        <input type="hidden" id="form_id" name="form_id" />
        <span>Apakah Anda yakin akan menghapus toko ini?</span>
        </div>
        <div class="modal-footer">
            <span id="form_notif"></span>
            <span><img id="form_loading" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>

            <a class="btn" data-dismiss="modal" aria-hidden="true">Batal</a>
            <button type="submit" id="hapus_button" class="btn btn-primary"><i class="icon-delete"></i> Hapus</button>
        </div>
    </form>
</div>

<div class="page-header">
    <div class="pull-left">
            <h1>Kelola Gerai Usaha</h1>
    </div>
    <div class="pull-right">
        <ul class="stats">
            <li class='lightred'>
                <i class="icon-calendar"></i>
                <div class="details">
                    <span class="big">-, -</span>
                    <span>-, -</span>
                </div>
            </li>
        </ul>
    </div>
</div>
<div class="breadcrumbs">
        <ul>
            <li>
                <a>Manage</a>
                <i class="icon-angle-right"></i>
            </li>
            <li>
                <a>Data Gerai Usaha</a>
            </li>
        </ul>
        <div class="close-bread">
                <a href="#"><i class="icon-remove" style="display: none;" ></i></a>
        </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <div class="box box-color box-bordered">
            <div class="box-title">
                <h3><i class="icon-table"></i>Tabel Gerai Usaha</h3>
            </div>
            <div class="box-content nopadding"  style="overflow: scroll">                
                <table class="table table-hover table-nomargin table-bordered" id="id_tabel">
                    <thead>
                        <tr>
                            <?php 
                            if (intval($promo->tipe) == 1) {
                                echo "<th>No</th><th>Produk Promo</th><th>Harga Promo</th>";
                            } else {
                                echo "<th>No</th><th>Produk Promo</th><th>Jumlah</th><th>Produk Free</th><th>Jumlah</th>";
                            }
                            ?>
                        </tr>
                    </thead>
                    <tbody id="isi_tabel">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>

    var promo = JSON.parse('<?php echo json_encode($promo) ?>');
    var produk_promo = JSON.parse('<?php echo json_encode($produk_promo) ?>');

    if (parseInt(promo.tipe) == 1) {
        load_produk_diskon(produk_promo);
    } else {
        load_produk_promo(produk_promo);
    }

    $('#hapus_button').click(function() {
        console.log('clickeddddd');
    });


    $(document).ready(function(){
       
        $(".chosen-select").chosen({
            no_results_text: "Data tidak ada!",
            width: "100%",
            placeholder_text_single : "Pilih Salah Satu"
        });
    });
    
    function hapus_modal(id_pemilik){
        $("#modal_data").modal("show");
        $("#form_id").val(id_pemilik);
    }
    
    $("#form_data").submit(function (e){
        e.preventDefault();
        $("#form_loading").show();
        $.ajax({
            url: "<?php echo URL_OPA.'update_pusaha/hapus_akun'; ?>",
            data: { 
                    "id_pemilik":$("#form_id").val()
                    },
            type: 'POST',
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                console.log("OK");
                $("#form_loading").fadeOut(1000, function (){
                    $("#form_notif").text(data.status);
                    $("#form_notif").show();
                    $("#form_notif").fadeOut(2000, function (){
                        datatable_server_side();
                        $("#modal_data").modal("hide");
                    });
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("NO");
            }
        });
    });
    
    ///////////////////////
    
    var dataTable;
    
    // datatable_server_side();
    
    function datatable_server_side(){
        console.log("server");
        if(dataTable != null)dataTable.fnDestroy();
        
        dataTable = $('#id_tabel').dataTable( {
            'iDisplayLength': 10,
            "aoColumnDefs": [
                { 'bSortable': false, 'aTargets': [ 0,1,2,3,4,5 ] }
            ],
            "processing": true,
            "serverSide": true,
            "ajax":{
                url :"<?php  echo URL_OPA; ?>update_pusaha/get_data", // json datasource
                type: "post",  // method  , by default get
                "data": function ( d ) {
                    
                },
                error: function(){  // error handling
                    $(".id_tabel-error").html("");
                    $("#id_tabel").append('<tbody class="id_tabel-error"><tr><th colspan="6">No data found in the server</th></tr></tbody>');
                    $("#id_tabel_processing").css("display","none");

                },
                "dataSrc": function ( json ) {
                    if (json.pesan == 'session expired') {
                        window.location.href = "<?php echo URL_OPA . 'merchant_login'; ?>";
                    }
                    json_upgrade = json.json;
                    return json.data;
                }
            }
            
        });
		
    }
</script>