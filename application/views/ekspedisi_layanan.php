<style type="text/css">

    

    /* Important part */
    .modal-dialog {
        overflow-y: initial !important
    }
    #data-modal-body {
        height: auto;
        overflow-y: auto;
    }
</style>
<script>
    var status_edit = "0";
    // load_opsi(json_role);
</script>

<div id="modal_data" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-header">
        <a type="button" class="close" data-dismiss="modal" aria-hidden="true">x</a>
        <h3 id="">Layanan Ekspedisi</h3>
    </div>
    <form id="form_data" class="form-horizontal" enctype="multipart/form-data">
            <div id="data-modal-body" class="modal-body">
        <input type="hidden" id="form_id" name="form_id" />
        <span>Are you sure to delete this service?</span>
        </div>
        <div class="modal-footer">
            <span id="form_notif"></span>
            <span><img id="form_loading" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>

            <a class="btn" data-dismiss="modal" aria-hidden="true">Cancel</a>
            <button type="submit" id="hapus_button" class="btn btn-primary"><i class="icon-delete"></i> Delete</button>
        </div>
    </form>
</div>


<div id="modal_add" class="modal hide fade" role="dialog">
    <div class="modal-dialog" >

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h3>Tambah Layanan Baru</h3>
            </div>
            <div class="modal-body">
                <div class="control-group">
                <form id="add_form_data" method="post" enctype="multipart">
                    <div class="control-group">
                            <label class="control-label">Nama Layanan<span class="apt_bintang">*</span></label>
                            <div class="controls">
                                <input type="text" id="nama" placeholder="Misal: Reguler 2-3 Hari" name="nama" class="input-xlarge" required />
                                <input type="text" id="id_ekspedisi" value="<?php echo $id_ekspedisi; ?>" name="id_ekspedisi" class="input-xlarge" style="display: none;" required />
                            </div>
                        </div>

                    </form>
                </div>
            </div>

        </div>
        <div class="modal-footer">
            <span id="modal_data_notif"></span>
            <span><img id="modal_data_loading" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>

            <button class="btn" data-dismiss="modal" aria-hidden="true">Batal</button>
            <button onclick="tambah_layanan()" class="btn btn-danger"><i class="icon-upload"></i> Simpan</button>
        </div>
    </div>
</div>


<div class="page-header">
    <div class="pull-left">
            <h1>Manage Services</h1>
    </div>
    <div class="pull-right">
        <ul class="stats">
            <li class='lightred'>
                <i class="icon-calendar"></i>
                <div class="details">
                    <span class="big">-, -</span>
                    <span>-, -</span>
                </div>
            </li>
        </ul>
    </div>
</div>
<div class="breadcrumbs">
        <ul>
            <li>
                <a>Manage</a>
                <i class="icon-angle-right"></i>
            </li>
            <li>
                <a>Data of Services</a>
            </li>
        </ul>
        <div class="close-bread" style="display: none;">
                <a href="#"><i class="icon-remove"></i></a>
        </div>
</div>

<div class="row-fluid">
    <div class="span12">
    </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <button type="submit" onclick="showAddModal()" class="btn btn-primary"><i class="icon-plus"></i> Add New</button>
    </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <div class="box box-color box-bordered">
            <div class="box-title">
                <h3><i class="icon-table"></i> Service Table</h3>
            </div>
            <div class="box-content nopadding"  style="overflow: scroll">                
                <table class="table usertable table-hover table-nomargin table-bordered" id="id_tabel">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>Opsi</th>
                        </tr>
                    </thead>
                    <tbody id="isi_tabel">

                        <?php 

                        $i = 1;
                        foreach ($layanans as $layanan) {
                            echo "<tr><td>" . $i . "</td><td>" . $layanan->nama . "</td>";
                            echo '<td><a onclick="hapus_modal(' . $layanan->id_layanan_ekspedisi . ')"><i rel="tooltip" title="Hapus" style="cursor: pointer" class="icon icon-trash"></i></a></td></tr>';
                            $i++;
                        }


                        ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    //load_upgrade(json_upgrade);
    $('#hapus_button').click(function() {
        console.log('clickeddddd');
    });

    $("#modal_add").modal("hide");
    function showAddModal() {
        $("#modal_add").modal("show");
    }


    $(document).ready(function(){
       
        $(".chosen-select").chosen({
            no_results_text: "Data tidak ada!",
            width: "100%",
            placeholder_text_single : "Pilih Salah Satu"
        });
    });
    
    function hapus_modal(id_layanan){
        $("#modal_data").modal("show");
        $("#form_id").val(id_layanan);
    }
    
    $("#form_data").submit(function (e){
        e.preventDefault();
        $("#form_loading").show();
        $.ajax({
            url: "<?php echo URL_OPA.'update_ekspedisi/delete_layanan'; ?>",
            data: { 
                    "id_layanan":$("#form_id").val()
                    },
            type: 'POST',
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                console.log("OK");
                $("#form_loading").fadeOut(1000, function (){
                    $("#form_notif").text(data.status);
                    $("#form_notif").show();
                    $("#form_notif").fadeOut(2000, function (){
                        buka_halaman('update_ekspedisi/layanan/' + $("#id_ekspedisi").val(),11);
                        $("#modal_data").modal("hide");
                    });
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("NO");
            }
        });
    });

    function tambah_layanan() {
        $("#modal_data_loading").show();
        $.ajax({
            url: "<?php echo URL_OPA.'update_ekspedisi/tambah_layanan'; ?>",
            data: { 
                    "id_ekspedisi":$("#id_ekspedisi").val(), 
                    "nama":$("#nama").val()
                    },
            type: 'POST',
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                console.log("OK");
                $("#modal_data_loading").fadeOut(1000, function (){
                    $("#form_notif").text(data.status);
                    $("#form_notif").show();
                    $("#form_notif").fadeOut(2000, function (){
                        buka_halaman('update_ekspedisi/layanan/' + $("#id_ekspedisi").val(),11);
                        $("#modal_add").modal("hide");
                    });
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("NO");
            }
        });
    };
    
    ///////////////////////
    
    var dataTable;
    
    // datatable_server_side();
    
    function datatable_server_side(){
        console.log("server");
        if(dataTable != null) dataTable.fnDestroy();
        
        dataTable = $('#id_tabel').dataTable( {
            'iDisplayLength': 2,
            "aoColumnDefs": [
                { 'bSortable': false, 'aTargets': [ 0,1] }
            ],
            "processing": true,
            "serverSide": true,
            "ajax":{
                url :"<?php  echo URL_OPA; ?>update_layanan/get_data", // json datasource
                type: "post",  // method  , by default get
                "data": function ( d ) {
                    
                },
                error: function(){  // error handling
                    $(".id_tabel-error").html("");
                    $("#id_tabel").append('<tbody class="id_tabel-error"><tr><th colspan="6">No item found in the database</th></tr></tbody>');
                    $("#id_tabel_processing").css("display","none");

                },
                "dataSrc": function ( json ) {
                    if (json.pesan == 'session expired') {
                        window.location.href = "<?php echo URL_OPA . 'merchant_login'; ?>";
                    }
                    json_upgrade = json.json;
                    return json.data;
                }
            }
            
        });
		
    }
</script>