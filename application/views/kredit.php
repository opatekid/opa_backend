<script>
    var json_isi = <?php echo json_encode($tran_isi)?>;
//    var json_isi_detail="";
    function load_isi(tran_isi){
        var html ="";
        for(var i =0;i<tran_isi.length;i++){
            var a ='<tr>\n\
                        <td>'+(i+1)+'</td>\n\
                        <td>'+tran_isi[i].nama+'</td>\n\
                        <td>'+aptikmacurrency(parseInt(tran_isi[i].kredit),"")+'</td>\n\
                        <td><a onclick="detail_modal('+tran_isi[i].id_pemilik+')"><i rel="tooltip" title="Lihat Histori" style="cursor: pointer" class="icon icon-reorder"></i></a>&nbsp;\
                        </td>\n\
                    </tr>';
            
            html = html + a;
        }
        $('#isi_tabel').html(html);
        $("#id_tabel").dataTable();
    }
    
    function load_isi_detail(detail){
        $("#id_tabel_detail").dataTable().fnDestroy();
        var html ="";
        for(var i =0;i<detail.length;i++){
            var a ='<tr>\n\
                        <td>'+(i+1)+'</td>\n\
                        <td>'+date_indo_convert2(detail[i].tanggal)+'</td>\n\
                        <td>'+detail[i].ket+'</td>\n\
                        <td>'+aptikmacurrency(parseInt(detail[i].nominal),"")+'</td>\n\
                    </tr>';
            
            html = html + a;
        }
        $('#isi_tabel_detail').html(html);
        $("#id_tabel_detail").dataTable();
    }
    
</script>

<div id="detail_modal" class="modal hide fade center" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" style="width: auto;">
    <div class="modal-header">
        <a type="button" class="close" data-dismiss="modal" aria-hidden="true">x</a>
        <h3 id="">Histori Kredit</h3>
    </div>
    <form id="form_data_detail" class="form-horizontal" enctype="multipart/form-data">
        <div class="modal-body" style="overflow-y: scroll;">
            <div class="row-fluid" style="margin-bottom: 10px;">
            <table class="table table-hover table-nomargin table-bordered" id="id_tabel_detail">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Keterangan</th>
                        <th>Nominal</th>
                    </tr>
                </thead>
                <tbody id="isi_tabel_detail">

                </tbody>
            </table>
            </div>
        </div>
        <div class="modal-footer">
            <span id="form_notif"></span>
            <span><img id="form_loading" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>

            <a class="btn" data-dismiss="modal" aria-hidden="true">Close</a>
        </div>
    </form>
</div>

<div class="page-header">
    <div class="pull-left">
        <h1>Kredit</h1>
    </div>
    <div class="pull-right">
        <ul class="stats">
            <li class='lightred'>
                <i class="icon-calendar"></i>
                <div class="details">
                    <span class="big">-, -</span>
                    <span>-, -</span>
                </div>
            </li>
        </ul>
    </div>
</div>
<div class="breadcrumbs">
    <ul>
        <li>
            <a>Kredit</a>
        </li>
    </ul>
    <div class="close-bread">
            <a href="#"><i class="icon-remove" style="display: none;" ></i></a>
    </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <div class="box box-color box-bordered">
            <div class="box-title">
                <h3><i class="icon-table"></i>Tabel Kredit</h3>
            </div>
            <div class="box-content nopadding"  style="overflow: scroll">
                <table class="table table-hover table-nomargin table-bordered" id="id_tabel">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Pemilik Usaha</th>
                            <th>Kredit</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody id="isi_tabel">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    load_isi(json_isi);
    function detail_modal(i){
        $("#detail_modal").modal("show");
        $.ajax({
            url:'<?php echo URL_OPA.'kredit/detail'; ?>',
            data:{"id":i},
            type: 'POST',
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                console.log("OK");
                json_isi_detail =data.detail;
                load_isi_detail(json_isi_detail);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("NO");
                        
            }
        });
    }
    
</script>