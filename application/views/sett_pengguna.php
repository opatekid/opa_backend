<script>
    var pengguna = <?php echo json_encode($pengguna)?>;
    function load_pengguna(pengguna){
        $("#id_tabel").dataTable().fnDestroy();
        var html ="";
        for(var i =0;i<pengguna.length;i++){
            var a ='<tr>\n\
                        <td>'+(i+1)+'</td>\n\
                        <td>'+pengguna[i].nama+'</td>\n\
                        <td>'+pengguna[i].username+'</td>\n\
                        <td>'+pengguna[i].email+'</td>\n\
                        <td><a onclick="hak_modal('+i+')"><i rel="tooltip" title="Hak Akses" data-placement="bottom" style="cursor: pointer; margin-right: 15px;" class="icon icon-th-list"></i></a>\
                            <a onclick="hapus_modal('+i+')"><i rel="tooltip" title="Hapus" data-placement="bottom" style="cursor: pointer" class="icon icon-trash"></i></a>\
                        </td>\n\
                    </tr>';
            
            html = html + a;
        }
        $('#isi_tabel').html(html);
        $("#id_tabel").dataTable();
    }
</script>

<div id="modal_hakakses" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" style="width: 95%; margin-left: -48%; overflow-y: auto;">
    <div class="modal-header">
        <a type="button" class="close" data-dismiss="modal" aria-hidden="true">x</a>
        <h3 id="">Hak Akses Pengguna</h3>
    </div>
    <form id="form_hakakses" class="form-horizontal" enctype="multipart/form-data">
        <div class="modal-body" style="overflow-y: scroll;">
            <div class="breadcrumbs">
                <ul>
                    <li>
                        <a style="font-weight: bold;">Master</a>
                    </li>
                </ul>
                <div class="close-bread">
                        <a href="#"><i class="icon-remove" style="display: none;" ></i></a>
                </div>
            </div>
            <input type="hidden" id="id_hakakses">
            <div class="row-fluid" style="width: auto; margin-bottom: 50px;">
            <div class="span3" style="padding-left: 10px;">
                <div class="box box-bordered">
                    <div class="box-title">
                        <h3><i class="icon-th-list"></i>Master Radius</h3>
                    </div>
                    <div class="box-content">
                        <div class="row-fluid pull-left" style="padding: 15px;">
                        <div class="control-group">
                            <div class="check-line">
                                <input type="checkbox" value="1" id="check1" class='check_table' checked disabled> <label class='inline' for="check1">Lihat</label>
                            </div>
                            <div class="check-line">
                                <input type="checkbox" value="2" id="check2" class='check_table'> <label class='inline' for="check2">Tambah</label>
                            </div>
                            <div class="check-line">
                                <input type="checkbox" value="3" id="check3" class='check_table'> <label class='inline' for="check3">Edit</label>
                            </div>
                            <div class="check-line">
                                <input type="checkbox" value="4" id="check4" class='check_table'> <label class='inline' for="check4">Hapus</label>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="span3">
                <div class="box box-bordered">
                    <div class="box-title">
                        <h3><i class="icon-th-list"></i>Master Jenis Kat. Usaha</h3>
                    </div>
                    <div class="box-content">
                        <div class="row-fluid pull-left" style="padding: 15px;">
                        <div class="control-group">
                            <div class="check-line">
                                <input type="checkbox" value="5" id="check5" class='check_table' checked disabled> <label class='inline' for="check5">Lihat</label>
                            </div>
                            <div class="check-line">
                                <input type="checkbox" value="6" id="check6" class='check_table'> <label class='inline' for="check6">Tambah</label>
                            </div>
                            <div class="check-line">
                                <input type="checkbox" value="7" id="check7" class='check_table'> <label class='inline' for="check7">Edit</label>
                            </div>
                            <div class="check-line">
                                <input type="checkbox" value="8" id="check8" class='check_table'> <label class='inline' for="check8">Hapus</label>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="span3">
                <div class="box box-bordered">
                    <div class="box-title">
                        <h3><i class="icon-th-list"></i>Master Kategori Usaha</h3>
                    </div>
                    <div class="box-content">
                        <div class="row-fluid pull-left" style="padding: 15px;">
                        <div class="control-group">
                            <div class="check-line">
                                <input type="checkbox" value="9" id="check9" class='check_table' checked disabled> <label class='inline' for="check9">Lihat</label>
                            </div>
                            <div class="check-line">
                                <input type="checkbox" value="10" id="check10" class='check_table'> <label class='inline' for="check10">Tambah</label>
                            </div>
                            <div class="check-line">
                                <input type="checkbox" value="11" id="check11" class='check_table'> <label class='inline' for="check11">Edit</label>
                            </div>
                            <div class="check-line">
                                <input type="checkbox" value="12" id="check12" class='check_table'> <label class='inline' for="check12">Hapus</label>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
                
            <div class="span3" style="padding-right: 10px;">
                <div class="box box-bordered">
                    <div class="box-title">
                        <h3><i class="icon-th-list"></i>Master Fee</h3>
                    </div>
                    <div class="box-content">
                        <div class="row-fluid pull-left" style="padding: 15px;">
                        <div class="control-group">
                            <div class="check-line">
                                <input type="checkbox" value="13" id="check13" class='check_table' checked disabled> <label class='inline' for="check13">Lihat</label>
                            </div>
                            <div class="check-line">
                                <input type="checkbox" value="14" id="check14" class='check_table'> <label class='inline' for="check14">Tambah</label>
                            </div>
                            <div class="check-line">
                                <input type="checkbox" value="15" id="check15" class='check_table'> <label class='inline' for="check15">Edit</label>
                            </div>
                            <div class="check-line">
                                <input type="checkbox" value="16" id="check16" class='check_table'> <label class='inline' for="check16">Hapus</label>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span3" style="margin-left: 10px;">
                <div class="box box-bordered">
                    <div class="box-title">
                        <h3><i class="icon-th-list"></i>Master Provinsi</h3>
                    </div>
                    <div class="box-content">
                        <div class="row-fluid pull-left">
                        <div class="control-group">
                            <div class="check-line">
                                <input type="checkbox" value="50" id="check50" class='check_table' checked disabled> <label class='inline' for="check50">Lihat</label>
                            </div>
                            <div class="check-line">
                                <input type="checkbox" value="51" id="check51" class='check_table'> <label class='inline' for="check51">Tambah</label>
                            </div>
                            <div class="check-line">
                                <input type="checkbox" value="52" id="check52" class='check_table'> <label class='inline' for="check52">Edit</label>
                            </div>
                            <div class="check-line">
                                <input type="checkbox" value="53" id="check53" class='check_table'> <label class='inline' for="check53">Hapus</label>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span3" >
                <div class="box box-bordered">
                    <div class="box-title">
                        <h3><i class="icon-th-list"></i>Master Kota</h3>
                    </div>
                    <div class="box-content">
                        <div class="row-fluid pull-left" style="padding: 15px;">
                        <div class="control-group">
                            <div class="check-line">
                                <input type="checkbox" value="54" id="check54" class='check_table' checked disabled> <label class='inline' for="check54">Lihat</label>
                            </div>
                            <div class="check-line">
                                <input type="checkbox" value="55" id="check55" class='check_table'> <label class='inline' for="check55">Tambah</label>
                            </div>
                            <div class="check-line">
                                <input type="checkbox" value="56" id="check56" class='check_table'> <label class='inline' for="check56">Edit</label>
                            </div>
                            <div class="check-line">
                                <input type="checkbox" value="57" id="check57" class='check_table'> <label class='inline' for="check57">Hapus</label>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            
            <div class="breadcrumbs">
                <ul>
                    <li>
                        <a style="font-weight: bold;">Transaksi</a>
                    </li>
                </ul>
                <div class="close-bread">
                        <a href="#"><i class="icon-remove" style="display: none;" ></i></a>
                </div>
            </div>
            <div class="row-fluid" style="width: auto; margin-bottom: 50px;">
            <div class="span3" style="padding-left: 10px;">
                <div class="box box-bordered">
                    <div class="box-title">
                        <h3><i class="icon-th-list"></i>Upgrade Pemilik Usaha</h3>
                    </div>
                    <div class="box-content">
                        <div class="row-fluid pull-left" style="padding: 15px;">
                        <div class="control-group">
                            <div class="check-line">
                                <input type="checkbox" value="17" id="check17" class='check_table' checked disabled> <label class='inline' for="check17">Lihat</label>
                            </div>
                            <div class="check-line">
                                <input type="checkbox" value="18" id="check18" class='check_table'> <label class='inline' for="check18">Edit</label>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="span3">
                <div class="box box-bordered">
                    <div class="box-title">
                        <h3><i class="icon-th-list"></i>Isi Kredit</h3>
                    </div>
                    <div class="box-content">
                        <div class="row-fluid pull-left" style="padding: 15px;">
                        <div class="control-group">
                            <div class="check-line">
                                <input type="checkbox" value="19" id="check19" class='check_table' checked disabled> <label class='inline' for="check19">Lihat</label>
                            </div>
                            <div class="check-line">
                                <input type="checkbox" value="20" id="check20" class='check_table'> <label class='inline' for="check20">Edit</label>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            
            <div class="breadcrumbs">
                <ul style="font-weight: bold;">
                    <li>
                        <a>Laporan</a>
                        <i class="icon-angle-right"></i>
                    </li>
                    <li>
                        <a>Penjualan</a>
                    </li>
                </ul>
                <div class="close-bread">
                        <a href="#"><i class="icon-remove" style="display: none;" ></i></a>
                </div>
            </div>
            <div class="row-fluid" style="width: auto; margin-bottom: 50px;">
            <div class="span3" style="padding-left: 10px;">
                <div class="box box-bordered">
                    <div class="box-title">
                        <h3><i class="icon-th-list"></i>Per Kategori</h3>
                    </div>
                    <div class="box-content">
                        <div class="row-fluid pull-left" style="padding: 15px;">
                        <div class="control-group">
                            <div class="check-line">
                                <input type="checkbox" value="21" id="check21" class='check_table'> <label class='inline' for="check21">Lihat</label>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="span3">
                <div class="box box-bordered">
                    <div class="box-title">
                        <h3><i class="icon-th-list"></i>Per Pemilik</h3>
                    </div>
                    <div class="box-content">
                        <div class="row-fluid pull-left" style="padding: 15px;">
                        <div class="control-group">
                            <div class="check-line">
                                <input type="checkbox" value="22" id="check22" class='check_table'> <label class='inline' for="check22">Lihat</label>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
                
            <div class="span3">
                <div class="box box-bordered">
                    <div class="box-title">
                        <h3><i class="icon-th-list"></i>Per Provinsi</h3>
                    </div>
                    <div class="box-content">
                        <div class="row-fluid pull-left" style="padding: 15px;">
                        <div class="control-group">
                            <div class="check-line">
                                <input type="checkbox" value="23" id="check23" class='check_table'> <label class='inline' for="check23">Lihat</label>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
                
            <div class="span3">
                <div class="box box-bordered">
                    <div class="box-title">
                        <h3><i class="icon-th-list"></i>Per Kota</h3>
                    </div>
                    <div class="box-content">
                        <div class="row-fluid pull-left" style="padding: 15px;">
                        <div class="control-group">
                            <div class="check-line">
                                <input type="checkbox" value="24" id="check24" class='check_table'> <label class='inline' for="check24">Lihat</label>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>    
            </div>
            
            <div class="breadcrumbs">
                <ul style="font-weight: bold;">
                    <li>
                        <a>Laporan</a>
                        <i class="icon-angle-right"></i>
                    </li>
                    <li>
                        <a>Belanja User</a>
                    </li>
                </ul>
                <div class="close-bread">
                        <a href="#"><i class="icon-remove" style="display: none;" ></i></a>
                </div>
            </div>
            <div class="row-fluid" style="width: auto; margin-bottom: 50px;">
            <div class="span3" style="padding-left: 10px;">
                <div class="box box-bordered">
                    <div class="box-title">
                        <h3><i class="icon-th-list"></i>Per Provinsi</h3>
                    </div>
                    <div class="box-content">
                        <div class="row-fluid pull-left" style="padding: 15px;">
                        <div class="control-group">
                            <div class="check-line">
                                <input type="checkbox" value="25" id="check25" class='check_table'> <label class='inline' for="check25">Lihat</label>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>    
            <div class="span3">
                <div class="box box-bordered">
                    <div class="box-title">
                        <h3><i class="icon-th-list"></i>Per Kota</h3>
                    </div>
                    <div class="box-content">
                        <div class="row-fluid pull-left" style="padding: 15px;">
                        <div class="control-group">
                            <div class="check-line">
                                <input type="checkbox" value="26" id="check26" class='check_table'> <label class='inline' for="check26">Lihat</label>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>    
            </div>
            
            <div class="breadcrumbs">
                <ul style="font-weight: bold;">
                    <li>
                        <a>Laporan</a>
                        <i class="icon-angle-right"></i>
                    </li>
                    <li>
                        <a>Pendapatan Aplikasi</a>
                    </li>
                </ul>
                <div class="close-bread">
                        <a href="#"><i class="icon-remove" style="display: none;" ></i></a>
                </div>
            </div>
            <div class="row-fluid" style="width: auto; margin-bottom: 50px;">
            <div class="span2" style="padding-left: 10px;">
                <div class="box box-bordered">
                    <div class="box-title">
                        <h3><i class="icon-th-list"></i>Per Waktu</h3>
                    </div>
                    <div class="box-content">
                        <div class="row-fluid pull-left" style="padding: 15px;">
                        <div class="control-group">
                            <div class="check-line">
                                <input type="checkbox" value="27" id="check27" class='check_table'> <label class='inline' for="check27">Lihat</label>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span2">
                <div class="box box-bordered">
                    <div class="box-title">
                        <h3><i class="icon-th-list"></i>Per Kategori</h3>
                    </div>
                    <div class="box-content">
                        <div class="row-fluid pull-left" style="padding: 15px;">
                        <div class="control-group">
                            <div class="check-line">
                                <input type="checkbox" value="28" id="check28" class='check_table'> <label class='inline' for="check28">Lihat</label>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span2">
                <div class="box box-bordered">
                    <div class="box-title">
                        <h3><i class="icon-th-list"></i>Per Provinsi</h3>
                    </div>
                    <div class="box-content">
                        <div class="row-fluid pull-left" style="padding: 15px;">
                        <div class="control-group">
                            <div class="check-line">
                                <input type="checkbox" value="29" id="check29" class='check_table'> <label class='inline' for="check29">Lihat</label>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span2">
                <div class="box box-bordered">
                    <div class="box-title">
                        <h3><i class="icon-th-list"></i>Per Kota</h3>
                    </div>
                    <div class="box-content">
                        <div class="row-fluid pull-left" style="padding: 15px;">
                        <div class="control-group">
                            <div class="check-line">
                                <input type="checkbox" value="30" id="check30" class='check_table'> <label class='inline' for="check30">Lihat</label>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span2">
                <div class="box box-bordered">
                    <div class="box-title">
                        <h3><i class="icon-th-list"></i>Per Pemilik</h3>
                    </div>
                    <div class="box-content">
                        <div class="row-fluid pull-left" style="padding: 15px;">
                        <div class="control-group">
                            <div class="check-line">
                                <input type="checkbox" value="31" id="check31" class='check_table'> <label class='inline' for="check31">Lihat</label>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            
            <div class="breadcrumbs">
                <ul style="font-weight: bold;">
                    <li>
                        <a>Laporan</a>
                        <i class="icon-angle-right"></i>
                    </li>
                    <li>
                        <a>Data User</a>
                    </li>
                </ul>
                <div class="close-bread">
                        <a href="#"><i class="icon-remove" style="display: none;" ></i></a>
                </div>
            </div>
            <div class="row-fluid" style="width: auto; margin-bottom: 50px;">
            <div class="span3" style="padding-left: 10px;">
                <div class="box box-bordered">
                    <div class="box-title">
                        <h3><i class="icon-th-list"></i>Pemilik Usaha</h3>
                    </div>
                    <div class="box-content">
                        <div class="row-fluid pull-left" style="padding: 15px;">
                        <div class="control-group">
                            <div class="check-line">
                                <input type="checkbox" value="32" id="check32" class='check_table'> <label class='inline' for="check32">Lihat</label>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>    
            <div class="span3">
                <div class="box box-bordered">
                    <div class="box-title">
                        <h3><i class="icon-th-list"></i>Konsumen</h3>
                    </div>
                    <div class="box-content">
                        <div class="row-fluid pull-left" style="padding: 15px;">
                        <div class="control-group">
                            <div class="check-line">
                                <input type="checkbox" value="33" id="check33" class='check_table'> <label class='inline' for="check33">Lihat</label>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>    
            </div>
            
            <div class="breadcrumbs">
                <ul style="font-weight: bold;">
                    <li>
                        <a>Rekapitulasi</a>
                        <i class="icon-angle-right"></i>
                    </li>
                    <li>
                        <a>Penjualan</a>
                    </li>
                </ul>
                <div class="close-bread">
                        <a href="#"><i class="icon-remove" style="display: none;" ></i></a>
                </div>
            </div>
            <div class="row-fluid" style="width: auto; margin-bottom: 50px;">
            <div class="span3" style="padding-left: 10px;">
                <div class="box box-bordered">
                    <div class="box-title">
                        <h3><i class="icon-th-list"></i>Per Kategori</h3>
                    </div>
                    <div class="box-content">
                        <div class="row-fluid pull-left" style="padding: 15px;">
                        <div class="control-group">
                            <div class="check-line">
                                <input type="checkbox" value="34" id="check34" class='check_table'> <label class='inline' for="check34">Lihat</label>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="span3">
                <div class="box box-bordered">
                    <div class="box-title">
                        <h3><i class="icon-th-list"></i>Per Pemilik</h3>
                    </div>
                    <div class="box-content">
                        <div class="row-fluid pull-left" style="padding: 15px;">
                        <div class="control-group">
                            <div class="check-line">
                                <input type="checkbox" value="35" id="check35" class='check_table'> <label class='inline' for="check35">Lihat</label>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
                
            <div class="span3">
                <div class="box box-bordered">
                    <div class="box-title">
                        <h3><i class="icon-th-list"></i>Per Provinsi</h3>
                    </div>
                    <div class="box-content">
                        <div class="row-fluid pull-left" style="padding: 15px;">
                        <div class="control-group">
                            <div class="check-line">
                                <input type="checkbox" value="36" id="check36" class='check_table'> <label class='inline' for="check36">Lihat</label>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
                
            <div class="span3">
                <div class="box box-bordered">
                    <div class="box-title">
                        <h3><i class="icon-th-list"></i>Per Kota</h3>
                    </div>
                    <div class="box-content">
                        <div class="row-fluid pull-left" style="padding: 15px;">
                        <div class="control-group">
                            <div class="check-line">
                                <input type="checkbox" value="37" id="check37" class='check_table'> <label class='inline' for="check37">Lihat</label>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>    
            </div>
            
            <div class="breadcrumbs">
                <ul style="font-weight: bold;">
                    <li>
                        <a>Rekapitulasi</a>
                        <i class="icon-angle-right"></i>
                    </li>
                    <li>
                        <a>Belanja User</a>
                    </li>
                </ul>
                <div class="close-bread">
                        <a href="#"><i class="icon-remove" style="display: none;" ></i></a>
                </div>
            </div>
            <div class="row-fluid" style="width: auto; margin-bottom: 50px;">
            <div class="span3" style="padding-left: 10px;">
                <div class="box box-bordered">
                    <div class="box-title">
                        <h3><i class="icon-th-list"></i>Per Provinsi</h3>
                    </div>
                    <div class="box-content">
                        <div class="row-fluid pull-left" style="padding: 15px;">
                        <div class="control-group">
                            <div class="check-line">
                                <input type="checkbox" value="38" id="check38" class='check_table'> <label class='inline' for="check38">Lihat</label>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>    
            <div class="span3">
                <div class="box box-bordered">
                    <div class="box-title">
                        <h3><i class="icon-th-list"></i>Per Kota</h3>
                    </div>
                    <div class="box-content">
                        <div class="row-fluid pull-left" style="padding: 15px;">
                        <div class="control-group">
                            <div class="check-line">
                                <input type="checkbox" value="39" id="check39" class='check_table'> <label class='inline' for="check39">Lihat</label>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>    
            </div>
            
            <div class="breadcrumbs">
                <ul style="font-weight: bold;">
                    <li>
                        <a>Rekapitulasi</a>
                        <i class="icon-angle-right"></i>
                    </li>
                    <li>
                        <a>Pendapatan Aplikasi</a>
                    </li>
                </ul>
                <div class="close-bread">
                        <a href="#"><i class="icon-remove" style="display: none;" ></i></a>
                </div>
            </div>
            <div class="row-fluid" style="width: auto; margin-bottom: 50px;">
            <div class="span2" style="padding-left: 10px;">
                <div class="box box-bordered">
                    <div class="box-title">
                        <h3><i class="icon-th-list"></i>Per Waktu</h3>
                    </div>
                    <div class="box-content">
                        <div class="row-fluid pull-left" style="padding: 15px;">
                        <div class="control-group">
                            <div class="check-line">
                                <input type="checkbox" value="40" id="check40" class='check_table'> <label class='inline' for="check40">Lihat</label>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span2">
                <div class="box box-bordered">
                    <div class="box-title">
                        <h3><i class="icon-th-list"></i>Per Kategori</h3>
                    </div>
                    <div class="box-content">
                        <div class="row-fluid pull-left" style="padding: 15px;">
                        <div class="control-group">
                            <div class="check-line">
                                <input type="checkbox" value="41" id="check41" class='check_table'> <label class='inline' for="check41">Lihat</label>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span2">
                <div class="box box-bordered">
                    <div class="box-title">
                        <h3><i class="icon-th-list"></i>Per Provinsi</h3>
                    </div>
                    <div class="box-content">
                        <div class="row-fluid pull-left" style="padding: 15px;">
                        <div class="control-group">
                            <div class="check-line">
                                <input type="checkbox" value="42" id="check42" class='check_table'> <label class='inline' for="check42">Lihat</label>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span2">
                <div class="box box-bordered">
                    <div class="box-title">
                        <h3><i class="icon-th-list"></i>Per Kota</h3>
                    </div>
                    <div class="box-content">
                        <div class="row-fluid pull-left" style="padding: 15px;">
                        <div class="control-group">
                            <div class="check-line">
                                <input type="checkbox" value="43" id="check43" class='check_table'> <label class='inline' for="check43">Lihat</label>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span2">
                <div class="box box-bordered">
                    <div class="box-title">
                        <h3><i class="icon-th-list"></i>Per Pemilik</h3>
                    </div>
                    <div class="box-content">
                        <div class="row-fluid pull-left" style="padding: 15px;">
                        <div class="control-group">
                            <div class="check-line">
                                <input type="checkbox" value="44" id="check44" class='check_table'> <label class='inline' for="check44">Lihat</label>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            
            <div class="breadcrumbs">
                <ul style="font-weight: bold;">
                    <li>
                        <a>Rekapitulasi</a>
                        <i class="icon-angle-right"></i>
                    </li>
                    <li>
                        <a>Data User</a>
                        <i class="icon-angle-right"></i>
                    </li>
                    <li>
                        <a>Pemilik Usaha</a>
                    </li>
                </ul>
                <div class="close-bread">
                        <a href="#"><i class="icon-remove" style="display: none;" ></i></a>
                </div>
            </div>
            <div class="row-fluid" style="width: auto; margin-bottom: 50px;">
            <div class="span3" style="padding-left: 10px;">
                <div class="box box-bordered">
                    <div class="box-title">
                        <h3><i class="icon-th-list"></i>Per Provinsi</h3>
                    </div>
                    <div class="box-content">
                        <div class="row-fluid pull-left" style="padding: 15px;">
                        <div class="control-group">
                            <div class="check-line">
                                <input type="checkbox" value="45" id="check45" class='check_table'> <label class='inline' for="check45">Lihat</label>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>    
            <div class="span3">
                <div class="box box-bordered">
                    <div class="box-title">
                        <h3><i class="icon-th-list"></i>Per Kota</h3>
                    </div>
                    <div class="box-content">
                        <div class="row-fluid pull-left" style="padding: 15px;">
                        <div class="control-group">
                            <div class="check-line">
                                <input type="checkbox" value="46" id="check46" class='check_table'> <label class='inline' for="check46">Lihat</label>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>    
            </div>
            
            <div class="breadcrumbs">
                <ul style="font-weight: bold;">
                    <li>
                        <a>Rekapitulasi</a>
                        <i class="icon-angle-right"></i>
                    </li>
                    <li>
                        <a>Data User</a>
                        <i class="icon-angle-right"></i>
                    </li>
                    <li>
                        <a>Konsumen</a>
                    </li>
                </ul>
                <div class="close-bread">
                        <a href="#"><i class="icon-remove" style="display: none;" ></i></a>
                </div>
            </div>
            <div class="row-fluid" style="width: auto; margin-bottom: 50px;">
            <div class="span3" style="padding-left: 10px;">
                <div class="box box-bordered">
                    <div class="box-title">
                        <h3><i class="icon-th-list"></i>Per Provinsi</h3>
                    </div>
                    <div class="box-content">
                        <div class="row-fluid pull-left" style="padding: 15px;">
                        <div class="control-group">
                            <div class="check-line">
                                <input type="checkbox" value="47" id="check47" class='check_table'> <label class='inline' for="check47">Lihat</label>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span3">
                <div class="box box-bordered">
                    <div class="box-title">
                        <h3><i class="icon-th-list"></i>Per Kota</h3>
                    </div>
                    <div class="box-content">
                        <div class="row-fluid pull-left" style="padding: 15px;">
                        <div class="control-group">
                            <div class="check-line">
                                <input type="checkbox" value="48" id="check48" class='check_table'> <label class='inline' for="check48">Lihat</label>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <div class="breadcrumbs">
                <ul style="font-weight: bold;">
                    <li>
                        <a>Kredit</a>
                    </li>
                </ul>
                <div class="close-bread">
                        <a href="#"><i class="icon-remove" style="display: none;" ></i></a>
                </div>
            </div>
            <div class="row-fluid" style="width: auto; margin-bottom: 50px;">
            <div class="span3" style="padding-left: 10px;">
                <div class="box box-bordered">
                    <div class="box-title">
                        <h3><i class="icon-th-list"></i>Kredit</h3>
                    </div>
                    <div class="box-content">
                        <div class="row-fluid pull-left" style="padding: 15px;">
                        <div class="control-group">
                            <div class="check-line">
                                <input type="checkbox" value="49" id="check49" class='check_table'> <label class='inline' for="check49">Lihat</label>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
        <div class="modal-footer">
            <span id="form_notif_hakakses"></span>
            <span><img id="form_loading_hakakses" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>

            <a class="btn" data-dismiss="modal" aria-hidden="true">Batal</a>
            <button type="submit" class="btn btn-primary"><i class="icon-save"></i> Simpan</button>
        </div>
    </form>
</div>

<div id="modal_data" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-header">
        <a type="button" class="close" data-dismiss="modal" aria-hidden="true">x</a>
        <h3 id="">Tambah Pengguna</h3>
    </div>
    <form id="form_data" class="form-horizontal" enctype="multipart/form-data">
        <div class="modal-body">
            <div class="control-group">
                <label class="control-label">Nama Pengguna<span class="apt_bintang">*</span></label>
                <div class="controls">
                    <input type="text" id="id_nama" name="id_nama" required class="input-xlarge">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Username<span class="apt_bintang">*</span></label>
                <div class="controls">
                    <input type="text" id="id_username" name="id_username" required class="input-xlarge">
                    <p id="notif_username" style="padding-top: 5px; display: none; color: #666666; margin-bottom: 0px;">Username sudah terdaftar</p>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Email<span class="apt_bintang">*</span></label>
                <div class="controls">
                    <input type="email" id="id_email" name="id_email" required class="input-xlarge">
                    <p id="notif_email" style="padding-top: 5px; display: none; color: #666666; margin-bottom: 0px;">Email sudah terdaftar</p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <span id="form_notif"></span>
            <span><img id="form_loading" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>

            <a class="btn" data-dismiss="modal" aria-hidden="true">Batal</a>
            <button type="submit" class="btn btn-primary"><i class="icon-save"></i> Simpan</button>
        </div>
    </form>
</div>

<div id="modal_data_hapus" class="modal hide fade" role="dialog">
    <div class="modal-dialog" >

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h3>Konfirmasi</h3>
            </div>
            <div class="modal-body">
                <center>
                    <input type="hidden" id="modal_data_hapus_id">
                    <p style="font-size: 15px;">Yakin Menghapus <span id="modal_data_hapus_p" style="font-weight: bold;"></span> ?</p>
                </center>
            </div>

        </div>
        <div class="modal-footer">
            <span id="modal_data_hapus_notif"></span>
            <span><img id="modal_data_hapus_loading" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>

            <button class="btn" data-dismiss="modal" aria-hidden="true">Batal</button>
            <button onclick="hapus()" class="btn btn-danger"><i class="icon-trash"></i> Hapus</button>
        </div>
    </div>
</div>

<div class="page-header">
    <div class="pull-left">
        <h1>Pengguna</h1>
    </div>
    <div class="pull-right">
        <ul class="stats">
            <li class='lightred'>
                <i class="icon-calendar"></i>
                <div class="details">
                    <span class="big">-, -</span>
                    <span>-, -</span>
                </div>
            </li>
        </ul>
    </div>
</div>
<div class="breadcrumbs">
    <ul>
        <li>
            <a>Setting</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a>Pengguna</a>
        </li>
    </ul>
    <div class="close-bread">
            <a href="#"><i class="icon-remove" style="display: none;" ></i></a>
    </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <div class="box box-color box-bordered">
            <div class="box-title">
                <h3><i class="icon-table"></i>Tabel Pengguna</h3>
            </div>
            <div class="box-content nopadding"  style="overflow: scroll">
                <button class="btn btn-blue tombol_add" onclick="buka_modal()" style="margin: 10px;"><i class="icon-plus"></i> Tambah</button>
                <table class="table table-hover table-nomargin table-bordered" id="id_tabel">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Pengguna</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Opsi</th>
                        </tr>
                    </thead>
                    <tbody id="isi_tabel">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    load_pengguna(pengguna);
    $('.check_table').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        increaseArea: '20%' // optional
    });
    function buka_modal(){
        $("#id_nama").val("");
        $("#id_username").val("");
        $("#id_email").val("");
        $("#modal_data").modal("show");
    }
    
    $("#form_data").submit(function (e){
        e.preventDefault();
        $("#form_loading").show();
        $("#notif_username").hide();
        $("#notif_email").hide();
        $.ajax({
            url: "<?php echo URL_OPA.'sett_pengguna/insert_pengguna'; ?>",
            data: { "nama":$("#id_nama").val(),
                    "username":$("#id_username").val(),
                    "email":$("#id_email").val()
                    },
            type: 'POST',
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                console.log("OK");
                $("#form_loading").fadeOut(1000, function (){
                    if(data.status == '1'){
                        $("#notif_username").fadeIn(1000);
                    }else if(data.status == '2'){
                        $("#notif_email").fadeIn(1000);
                    }else if(data.status == '3'){
                        $("#notif_username").fadeIn(1000);
                        $("#notif_email").fadeIn(1000);
                    }else{
                        $("#form_notif").text(data.status);
                        $("#form_notif").show();
                        $("#form_notif").fadeOut(2000, function (){
                            pengguna=data.list;
                            load_pengguna(pengguna);
                            $("#modal_data").modal("hide");
                        });
                    }
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("NO");
            }
        });
    });
    
    function hapus_modal(i){
        $("#modal_data_hapus_id").val(pengguna[i].id);
        $("#modal_data_hapus_p").html(pengguna[i].nama);
        $("#modal_data_hapus").modal("show");
    }
    
    function hapus(){
        $("#modal_data_hapus_loading").show();
        $.ajax({
            url: "<?php echo URL_OPA.'sett_pengguna/delete'; ?>",
            data: {"id":$("#modal_data_hapus_id").val()},
            type: 'POST',
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $("#modal_data_hapus_loading").fadeOut(1000,function(){
                    $("#modal_data_hapus_notif").text(data.status);
                    $("#modal_data_hapus_notif").show();
                    $("#modal_data_hapus_notif").fadeOut(2000, function (){
                        pengguna=data.list;
                        load_pengguna(pengguna);
                        $("#modal_data_hapus").modal("hide");
                    });
                    
                });
                
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("gagal");

            }
        });
    }
    var check ="";
    function hak_modal(i){
        $("#id_hakakses").val(pengguna[i].id);
        $('.check_table').iCheck("uncheck");
        $.ajax({
            url: "<?php echo URL_OPA.'sett_pengguna/hakakses'; ?>",
            data: {"id":$("#id_hakakses").val()},
            type: 'POST',
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                check=data.check;
                for(var i=0;i<check.length;i++){
                    $("#check"+check[i].id_role).iCheck("check");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("gagal");

            }
        });
        
        $("#modal_hakakses").modal("show");
    }
    
    $("#form_hakakses").submit(function (e){
       e.preventDefault();
       var isi = [];
       $.each($("input[class='check_table']:checked"), function (){
           isi.push($(this).val());
       });
       var isi2 = isi.join(",");
//       console.log(isi2);
       $("#form_loading_hakakses").show();
       $.ajax({
            url: "<?php echo URL_OPA.'sett_pengguna/insert_role'; ?>",
            data: { "isi":isi2,
                    "id":$("#id_hakakses").val()
                    },
            type: 'POST',
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                console.log("OK");
                $("#form_loading_hakakses").fadeOut(1000, function (){
                    $("#form_notif_hakakses").text(data.status);
                    $("#form_notif_hakakses").show();
                    $("#form_notif_hakakses").fadeOut(2000, function (){
                        pengguna=data.list;
                        load_pengguna(pengguna);
                        $("#modal_hakakses").modal("hide");
                    });
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("NO");
            }
        });
    });
</script>