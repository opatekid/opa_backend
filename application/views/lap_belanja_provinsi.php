<script>
    var provinsi = <?php echo json_encode($provinsi)?>;
    var carii ="";
    function load_cari(cari){
        var html ="";
        for(var i =0;i<cari.length;i++){
            var a ='<tr>\n\
                        <td>'+(i+1)+'</td>\n\
                        <td>'+cari[i].tanggal_order+'</td>\n\
                        <td>'+cari[i].nama+'</td>\n\
                        <td>'+cari[i].alamat+'</td>\n\
                        <td>'+cari[i].nama_provinsi+'</td>\n\
                    </tr>';
            
            html = html + a;
        }
        $('#isi_tabel').html(html);
        $("#id_tabel").dataTable();
    }
    function load_provinsi(provinsi){
        var awal = '<option value="">-Pilih Provinsi-</option>';
        var html = "";
        
        for (var i=0;i<provinsi.length;i++){
            var a ='<option value="'+provinsi[i].id+'">'+provinsi[i].nama+'</option>';
           html = html + a;
        }

        $('#id_cari').html(awal+html).trigger("liszt:updated");
        $('#id_cari1').html(awal+html).trigger("liszt:updated");
        $('#id_cari2').html(awal+html).trigger("liszt:updated");

    }
</script>

<div class="page-header">
    <div class="pull-left">
            <h1>Per Provinsi</h1>
    </div>
    <div class="pull-right">
        <ul class="stats">
            <li class='lightred'>
                <i class="icon-calendar"></i>
                <div class="details">
                    <span class="big">-, -</span>
                    <span>-, -</span>
                </div>
            </li>
        </ul>
    </div>
</div>
<div class="breadcrumbs">
        <ul>
            <li>
                <a>Laporan</a>
                <i class="icon-angle-right"></i>
            </li>
            <li>
                <a>Belanja User</a>
                <i class="icon-angle-right"></i>
            </li>
            <li>
                <a>Per Provinsi</a>
            </li>
        </ul>
        <div class="close-bread">
                <a href="#"><i class="icon-remove" style="display: none;" ></i></a>
        </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <div class="box box-color box-bordered">
            <div class="box-title">
                <h3><i class="icon-table"></i>Tabel Data Pencarian</h3>
            </div>
            <div class="box-content nopadding"  style="overflow: scroll">
                <div class="row-fluid">
                <div class="span4" style="padding-left: 10px;">
                    <div class="box box-color box-bordered orange">
                        <div class="box-title">
                            <h3><i class="icon-table"></i>Cari Per Tanggal</h3>
                        </div>
                        <div class="box-content nopadding">
                            <form id="form_cari1">
                            <div class="row-fluid" style="padding: 10px;">
                            <div class="control-group">
                                <label class="control-label">Tanggal<span class="apt_bintang">*</span></label>
                                <div class="controls">
                                    <div class="input-xlarge">
                                        <input type="text" name="id_tanggal" id="id_tanggal" class="input-xlarge datepicker" required>
                                    </div>
                                </div>
                            </div>    
                            <div class="control-group">
                                <label class="control-label">Pilih Provinsi<span class="apt_bintang">*</span></label>
                                <div class="controls">
                                    <div class="input-xlarge">
                                        <select id="id_cari" name="id_cari" class='chosen-select input-xlarge' required>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="controls">
                                    <div class="input-xlarge">
                                        <button type="submit" class="btn btn-primary pull-right"><i class="icon-search"></i> Cari</button>
                                        <span class="pull-right" style="margin-right: 10px;"><img id="form_loading1" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;"/></span>
                                    </div>
                                </div>
                            </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="span4">
                    <div class="box box-color box-bordered green">
                        <div class="box-title">
                            <h3><i class="icon-table"></i>Cari Per Bulan</h3>
                        </div>
                        <div class="box-content nopadding">
                            <form id="form_cari2">
                            <div class="row-fluid" style="padding: 10px;">
                            <div class="control-group">
                                <label class="control-label">Pilih Provinsi<span class="apt_bintang">*</span></label>
                                <div class="controls">
                                    <div class="input-xlarge">
                                        <select id="id_cari1" name="id_cari1" class='chosen-select input-xlarge' required>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Bulan<span class="apt_bintang">*</span></label>
                                <div class="controls">
                                    <div class="input-xlarge">
                                        <select id="id_bulan" name="id_bulan" class='chosen-select input-xlarge' required>
                                            <option value="">-Pilih Bulan-</option>
                                            <option value="01">Januari</option>
                                            <option value="02">Februari</option>
                                            <option value="03">Maret</option>
                                            <option value="04">April</option>
                                            <option value="05">Mei</option>
                                            <option value="06">Juni</option>
                                            <option value="07">Juli</option>
                                            <option value="08">Agustus</option>
                                            <option value="09">September</option>
                                            <option value="10">Oktober</option>
                                            <option value="11">November</option>
                                            <option value="12">Desember</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Tahun<span class="apt_bintang">*</span></label>
                                <div class="controls">
                                    <div class="input-xlarge">
                                        <select id="id_tahun" name="id_tahun" class='chosen-select input-xlarge' required>
                                            <option value="">-Pilih Tahun-</option>
                                            <?php

                                            for($i=2016; $i<=date('Y'); $i++)
                                             {
                                              ?>
                                             <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="controls">
                                    <div class="input-xlarge">
                                        <button type="submit" class="btn btn-primary pull-right"><i class="icon-search"></i> Cari</button>
                                        <span class="pull-right" style="margin-right: 10px;"><img id="form_loading2" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;"/></span>
                                    </div>
                                </div>
                            </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="span4" style="padding-right: 10px;">
                    <div class="box box-color box-bordered red">
                        <div class="box-title">
                            <h3><i class="icon-table"></i>Cari Per Tahun</h3>
                        </div>
                        <div class="box-content nopadding">
                            <form id="form_cari3">
                            <div class="row-fluid" style="padding: 10px;">
                            <div class="control-group">
                                <label class="control-label">Pilih Provinsi<span class="apt_bintang">*</span></label>
                                <div class="controls">
                                    <div class="input-xlarge">
                                        <select id="id_cari2" name="id_cari2" class='chosen-select input-xlarge' required>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Tahun<span class="apt_bintang">*</span></label>
                                <div class="controls">
                                    <div class="input-xlarge">
                                        <select id="id_tahun1" name="id_tahun1" class='chosen-select input-xlarge' required>
                                            <option value="">-Pilih Tahun-</option>
                                            <?php

                                            for($i=2016; $i<=date('Y'); $i++)
                                             {
                                              ?>
                                             <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="controls">
                                    <div class="input-xlarge">
                                        <button type="submit" class="btn btn-primary pull-right"><i class="icon-search"></i> Cari</button>
                                        <span class="pull-right" style="margin-right: 10px;"><img id="form_loading3" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;"/></span>
                                    </div>
                                </div>
                            </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                </div>
                <hr>
                <h4 style="padding-left: 10px; display: none;" id="notif_title_cari"></h4>
                <div class="row-fluid" style="padding-top: 10px;">
                <table class="table table-hover table-nomargin table-bordered" id="id_tabel" style="margin-top: 10px;">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal Order</th>
                            <th>Nama</th>
                            <th>Alamat</th>
                            <th>Provinsi</th>
                        </tr>
                    </thead>
                    <tbody id="isi_tabel">

                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    load_provinsi(provinsi);
    $(document).ready(function(){
        $(".chosen-select").chosen({
            no_results_text: "Data tidak ada!",
            width: "100%",
            placeholder_text_single : "Pilih Salah Satu"
        });
        $('.datepicker').datepicker({
	format: 'dd-mm-yyyy'
        });
    });
    
    $("#form_cari1").submit(function (e){
        e.preventDefault();
        var nama = "";
        var id_pilih = $("#id_cari").val();
        for(var i=0;i<provinsi.length;i++){
            if(id_pilih == provinsi[i].id){
                nama = provinsi[i].nama;
                break;
            }
        }
        $("#notif_title_cari").hide();
        $("#form_loading1").show();
        $("#form_loading1").fadeOut(2000, function(){
            $("#notif_title_cari").text("Hasil Pencarian per Tanggal: "+date_indo_convert($("#id_tanggal").val())+", Provinsi: "+nama);
             $("#notif_title_cari").fadeIn(1000, function (){
                 datatable_server_side("1");
             });
         });
//        $.ajax({
//            url: "echo URL_OPA.'lap_belanja_provinsi/caritgl'; ",
//            data: { "tanggal":$("#id_tanggal").val(),
//                    "jenis":$("#id_cari").val()
//                    },
//            type: 'POST',
//            dataType: 'json',
//            success: function (data, textStatus, jqXHR) {
//                $("#form_loading1").fadeOut(2000, function(){
//                   $("#notif_title_cari").text("Hasil Pencarian per Tanggal: "+date_indo_convert($("#id_tanggal").val())+", Provinsi: "+nama);
//                    $("#notif_title_cari").fadeIn(1000, function (){
//                        $('#isi_tabel').fadeIn(1000, function (){
//                            carii =data.cari;
//                            load_cari(carii); 
//                        });
//                    });
//                });
//            },
//            error: function (jqXHR, textStatus, errorThrown) {
//                console.log("NO");
//                        
//            }
//        });
    });
    $("#form_cari2").submit(function (e){
        e.preventDefault();
        var nama = "";
        var id_pilih = $("#id_cari1").val();
        for(var i=0;i<provinsi.length;i++){
            if(id_pilih == provinsi[i].id){
                nama = provinsi[i].nama;
                break;
            }
        }
        $("#notif_title_cari").hide();
        $("#form_loading2").show();
        $("#form_loading2").fadeOut(2000, function(){
            var bulan = "";
            if($("#id_bulan").val()=="01"){
                bulan = "Januari";
            }else if($("#id_bulan").val()=="02"){
                bulan = "Februari";
            }else if($("#id_bulan").val()=="03"){
                bulan = "Maret";
            }else if($("#id_bulan").val()=="04"){
                bulan = "April";
            }else if($("#id_bulan").val()=="05"){
                bulan = "Mei";
            }else if($("#id_bulan").val()=="06"){
                bulan = "Juni";
            }else if($("#id_bulan").val()=="07"){
                bulan = "Juli";
            }else if($("#id_bulan").val()=="08"){
                bulan = "Agustus";
            }else if($("#id_bulan").val()=="09"){
                bulan = "September";
            }else if($("#id_bulan").val()=="10"){
                bulan = "Oktober";
            }else if($("#id_bulan").val()=="11"){
                bulan = "November";
            }else if($("#id_bulan").val()=="12"){
                bulan = "Desember";
            }
            $("#notif_title_cari").text("Hasil Pencarian per Bulan: "+bulan+" "+$("#id_tahun").val()+", Provinsi: "+nama);
            $("#notif_title_cari").fadeIn(1000, function (){
                datatable_server_side("2");
            });
        });
//        $.ajax({
//            url: " echo URL_OPA.'lap_belanja_provinsi/caribln'; ",
//            data: { "tahun":$("#id_tahun").val(),
//                    "jenis":$("#id_cari1").val(),
//                    "bulan":$("#id_bulan").val()
//                    },
//            type: 'POST',
//            dataType: 'json',
//            success: function (data, textStatus, jqXHR) {
//                $("#form_loading2").fadeOut(2000, function(){
//                    var bulan = "";
//                    if($("#id_bulan").val()=="01"){
//                        bulan = "Januari";
//                    }else if($("#id_bulan").val()=="02"){
//                        bulan = "Februari";
//                    }else if($("#id_bulan").val()=="03"){
//                        bulan = "Maret";
//                    }else if($("#id_bulan").val()=="04"){
//                        bulan = "April";
//                    }else if($("#id_bulan").val()=="05"){
//                        bulan = "Mei";
//                    }else if($("#id_bulan").val()=="06"){
//                        bulan = "Juni";
//                    }else if($("#id_bulan").val()=="07"){
//                        bulan = "Juli";
//                    }else if($("#id_bulan").val()=="08"){
//                        bulan = "Agustus";
//                    }else if($("#id_bulan").val()=="09"){
//                        bulan = "September";
//                    }else if($("#id_bulan").val()=="10"){
//                        bulan = "Oktober";
//                    }else if($("#id_bulan").val()=="11"){
//                        bulan = "November";
//                    }else if($("#id_bulan").val()=="12"){
//                        bulan = "Desember";
//                    }
//                    $("#notif_title_cari").text("Hasil Pencarian per Bulan: "+bulan+" "+$("#id_tahun").val()+", Provinsi: "+nama);
//                    $("#notif_title_cari").fadeIn(1000, function (){
//                        $('#isi_tabel').fadeIn(1000, function (){
//                            carii =data.cari;
//                            load_cari(carii);
//                        });
//                        
//                    });
//                });
//            },
//            error: function (jqXHR, textStatus, errorThrown) {
//                console.log("NO");
//                        
//            }
//        });
    });
    
    $("#form_cari3").submit(function (e){
        e.preventDefault();
        var nama = "";
        var id_pilih = $("#id_cari2").val();
        for(var i=0;i<provinsi.length;i++){
            if(id_pilih == provinsi[i].id){
                nama = provinsi[i].nama;
                break;
            }
        }
        $("#notif_title_cari").hide();
        $("#form_loading3").show();
        $("#form_loading3").fadeOut(2000, function(){
            $("#notif_title_cari").text("Hasil Pencarian per Tahun: "+$("#id_tahun1").val()+", Provinsi: "+nama);
            $("#notif_title_cari").fadeIn(1000, function (){
                datatable_server_side("3");
            });
        });
//        $.ajax({
//            url: " echo URL_OPA.'lap_belanja_provinsi/carithn'; ",
//            data: { "tahun":$("#id_tahun1").val(),
//                    "jenis":$("#id_cari2").val()
//                    },
//            type: 'POST',
//            dataType: 'json',
//            success: function (data, textStatus, jqXHR) {
//                $("#form_loading3").fadeOut(2000, function(){
//                    $("#notif_title_cari").text("Hasil Pencarian per Tahun: "+$("#id_tahun1").val()+", Provinsi: "+nama);
//                    $("#notif_title_cari").fadeIn(1000, function (){
//                        $('#isi_tabel').fadeIn(1000, function (){
//                            carii =data.cari;
//                            load_cari(carii);
//                        });
//                    });
//                });
//            },
//            error: function (jqXHR, textStatus, errorThrown) {
//                console.log("NO");
//                        
//            }
//        });
    });
    
    ///////////////////////
    
    var dataTable;
    
    function datatable_server_side(tipe){
        console.log("server");
        if(dataTable != null)dataTable.fnDestroy();
        
        dataTable = $('#id_tabel').dataTable( {
            'iDisplayLength': 10,
            "aoColumnDefs": [
                { 'bSortable': false, 'aTargets': [ 0,1,2,3,4 ] }
            ],
            "processing": true,
            "serverSide": true,
            "ajax":{
                url :"<?php  echo URL_OPA; ?>lap_belanja_provinsi/get_data", // json datasource
                type: "post",  // method  , by default get
                "data": function ( d ) {
                    if(tipe == '1'){
                        d.tanggal = $("#id_tanggal").val();
                        d.jenis = $("#id_cari").val();
                        d.tipe = tipe;
                    }else if(tipe == '2'){
                        d.tahun = $("#id_tahun").val();
                        d.jenis = $("#id_cari1").val();
                        d.bulan = $("#id_bulan").val();
                        d.tipe = tipe;
                    }else if(tipe == '3'){
                        d.tahun = $("#id_tahun1").val();
                        d.jenis = $("#id_cari2").val();
                        d.tipe = tipe;
                    }
                },
                error: function(){  // error handling
                    $(".id_tabel-error").html("");
                    $("#id_tabel").append('<tbody class="id_tabel-error"><tr><th colspan="5">No data found in the server</th></tr></tbody>');
                    $("#id_tabel_processing").css("display","none");

                },
                "dataSrc": function ( json ) {
                    json_upgrade = json.json;
                    
                    return json.data;
                }
            }
        });	
    }
</script>