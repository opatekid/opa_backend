<div id="navigation">
    <div class="container-fluid">
        <a href="#" id="brand"></a>
        <ul class='main-nav' style="cursor: pointer;">
            <li class='active menu_main' id="menu_main1" onclick="buka_halaman('beranda','1')">
                <a>
                    <span>Beranda</span>
                </a>
            </li>
            <li class="menu_main" id="menu_main2">
                <a href="#" data-toggle="dropdown" class='dropdown-toggle'>
                    <span>Data</span>
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" id="menu_master">
                    
                </ul>
            </li>
            <li class="menu_main" id="menu_main3">
                <a href="#" data-toggle="dropdown" class='dropdown-toggle'>
                    <span>Transaksi</span>
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" id="menu_transaksi">
                    
                </ul>
            </li>
            <li class="menu_main" id="menu_main4">
                <a href="#" data-toggle="dropdown" class='dropdown-toggle'>
                    <span>Laporan</span>
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li class='dropdown-submenu'>
                        <a href="#">Penjualan</a>
                        <ul class="dropdown-menu" id="mn_lap_penjualan">
                            
                        </ul>
                    </li>
                    <li class='dropdown-submenu'>
                        <a href="#">Belanja User</a>
                        <ul class="dropdown-menu" id="mn_lap_belanja">
                            
                        </ul>
                    </li>
                    <li class='dropdown-submenu'>
                        <a href="#">Pendapatan Aplikasi</a>
                        <ul class="dropdown-menu" id="mn_lap_pendapatan">
                            
                        </ul>
                    </li>
                    <li class='dropdown-submenu'>
                        <a href="#">Data User</a>
                        <ul class="dropdown-menu" id="mn_lap_data">
                            
                        </ul>
                    </li>
                </ul>
            </li>

            <li class="menu_main admin-kredit" id="menu_main6" onclick="buka_halaman('kredit','6')">
                <a>
                    <span>Kredit</span>
                </a>
            </li>

            <li class="menu_main admin_aja" id="menu_main7">
                <a href="#" data-toggle="dropdown" class='dropdown-toggle'>
                    <span>Promo</span>
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    
                    <li class='dropdown-submenu'>
                        <a href="#">Create Promo</a>
                        <ul class="dropdown-menu" id="mn_create_promo">
                            
                        </ul>
                    </li>
                    <li onclick="buka_halaman('promo_old','7')">
                        <a>Promo Saat Ini</a>
                    </li>
                </ul>
            </li>
            <li class="menu_main admin_aja" id="menu_main9">
                <a href="#" data-toggle="dropdown" class='dropdown-toggle'>
                    <span>Pre-Active Stores</span>
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li onclick="buka_halaman('preactive_store/add','9')">
                        <a>Tambah Baru</a>
                    </li>
                    <li onclick="buka_halaman('preactive_store/index','9')">
                        <a>Data Pre-Active Store</a>
                    </li>
                </ul>
            </li>

            <li class="menu_main admin-kredit" id="menu_main11" onclick="buka_halaman('update_ekspedisi','11')">
                <a>
                    <span>Ekspedisi</span>
                </a>
            </li>

            <li class="menu_main admin_aja" id="menu_main10">
                <a href="#" data-toggle="dropdown" class='dropdown-toggle'>
                    <span>Kurir</span>
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li onclick="buka_halaman('update_kurir/add','10')">
                        <a>Tambah Baru</a>
                    </li>
                    <li onclick="buka_halaman('update_kurir/index','10')">
                        <a>Data Kurir</a>
                    </li>
                </ul>
            </li>

            <li class="menu_main admin_aja" id="menu_main8">
                <a href="#" data-toggle="dropdown" class='dropdown-toggle'>
                    <span>Setting</span>
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li onclick="buka_halaman('sett_pengguna','8')">
                        <a>Pengguna</a>
                    </li>
                    <li onclick="buka_halaman('sett_dkredit','8')">
                        <a>Default Kredit</a>
                    </li>
                    <li onclick="buka_halaman('sett_charge','8')">
                        <a>Biaya Layanan</a>
                    </li>
                </ul>
            </li>
        </ul>
        <div class="user">
            <div class="dropdown">
                <a href="#" class='dropdown-toggle' id="id_nama_user" data-toggle="dropdown"><?php echo $namaad ?> <img src="<?php echo URL_IMG ?>demo/user-avatar.jpg" alt=""></a>
                <ul class="dropdown-menu pull-right" style="cursor: pointer;">
                    <li>
                        <a onclick="showsunting()">Profil</a>
                    </li>
                    <li>
                        <a onclick="showgantipass()">Ganti Password</a>
                    </li>
                    <li>
                        <a href="<?php echo URL_OPA.'login/logout/'?>">Sign out</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<script>
    
    load_menu(json_role);
    
    function load_menu(json){
        $(".admin-kredit").hide();
        var menu_master = "";
        var menu_transaksi = "";
        
        var mn_lap_penjualan = "";
        var mn_lap_belanja = "";
        var mn_lap_pendapatan = "";
        var mn_lap_data = "";
        var mn_create_promo = "";
        
        var mn_rek_penjualan = "";
        var mn_rek_belanja = "";
        var mn_rek_pendapatan = "";
        var mn_rek_data_pemilkush = "";
        var mn_rek_data_konsumen = "";
        
        var mn_kredit = "";
        
        for(var i=0;i<json.length;i++){
            if(json[i].id_role == "1"){
                menu_master += "<li onclick=\"buka_halaman('mst_radius','2')\"><a>Radius Kategori Usaha</a></li>";
            }else if(json[i].id_role == "5"){
                menu_master += "<li onclick=\"buka_halaman('mst_kategori_usaha','2')\"><a>Kategori Usaha</a></li>";
            }else if(json[i].id_role == "9"){
                menu_master += "<li onclick=\"buka_halaman('mst_kategori_ushm','2')\"><a>Jenis Usaha</a></li>";
            }else if(json[i].id_role == "13"){
                menu_master += "<li onclick=\"buka_halaman('mst_fee','2')\"><a>Fee</a></li>";
            }else if(json[i].id_role == "50"){
                menu_master += "<li onclick=\"buka_halaman('mst_provinsi','2')\"><a>Provinsi</a></li>";
            }else if(json[i].id_role == "54"){
                menu_master += "<li onclick=\"buka_halaman('mst_kota','2')\"><a>Kota</a></li>";
            }else if(json[i].id_role == "17"){
                menu_transaksi += "<li onclick=\"buka_halaman('tran_upgrade_pusaha','3')\"><a>Upgrade Pemilik Usaha</a></li>";
            }else if(json[i].id_role == "19"){
                menu_transaksi += "<li onclick=\"buka_halaman('tran_isi_kredit','3')\"><a>Isi Kredit</a></li>";
            }else if(json[i].id_role == "21"){
                mn_lap_penjualan += "<li onclick=\"buka_halaman('lap_penjualan_kategori','4')\"><a>Per Kategori</a></li>";
            }else if(json[i].id_role == "22"){
                mn_lap_penjualan += "<li onclick=\"buka_halaman('lap_penjualan_pemilik','4')\"><a>Per Pemilik</a></li>";
            }else if(json[i].id_role == "23"){
                mn_lap_penjualan += "<li onclick=\"buka_halaman('lap_penjualan_provinsi','4')\"><a>Per Provinsi</a></li>";
            }else if(json[i].id_role == "24"){
                mn_lap_penjualan += "<li onclick=\"buka_halaman('lap_penjualan_kota','4')\"><a>Per Kota</a></li>";
            }else if(json[i].id_role == "25"){
                mn_lap_belanja += "<li onclick=\"buka_halaman('lap_belanja_provinsi','4')\"><a>Per Provinsi</a></li>";
            }else if(json[i].id_role == "26"){
                mn_lap_belanja += "<li onclick=\"buka_halaman('lap_belanja_kota','4')\"><a>Per Kota</a></li>";
            }else if(json[i].id_role == "27"){
                mn_lap_pendapatan += "<li onclick=\"buka_halaman('lap_pendapatan_waktu','4')\"><a>Per Waktu</a></li>";
            }else if(json[i].id_role == "28"){
                mn_lap_pendapatan += "<li onclick=\"buka_halaman('lap_pendapatan_kategori','4')\"><a>Per Kategori</a></li>";
            }else if(json[i].id_role == "29"){
                mn_lap_pendapatan += "<li onclick=\"buka_halaman('lap_pendapatan_provinsi','4')\"><a>Per Provinsi</a></li>";
            }else if(json[i].id_role == "30"){
                mn_lap_pendapatan += "<li onclick=\"buka_halaman('lap_pendapatan_kota','4')\"><a>Per Kota</a></li>";
            }else if(json[i].id_role == "31"){
                mn_lap_pendapatan += "<li onclick=\"buka_halaman('lap_pendapatan_pemilik','4')\"><a>Per Pemilik</a></li>";
            }else if(json[i].id_role == "32"){
                mn_lap_data += "<li onclick=\"buka_halaman('lap_data_pemiliku','4')\"><a>Pemilik Usaha</a></li>";
            }else if(json[i].id_role == "33"){
                mn_lap_data += "<li onclick=\"buka_halaman('lap_data_konsumen','4')\"><a>Konsumen</a></li>";
            }else if(json[i].id_role == "34"){
                mn_rek_penjualan += "<li onclick=\"buka_halaman('rekap_penjualan_kategori','5')\"><a>Per Kategori</a></li>";
            }else if(json[i].id_role == "35"){
                mn_rek_penjualan += "<li onclick=\"buka_halaman('rekap_penjualan_pemilik','5')\"><a>Per Pemilik</a></li>";
            }else if(json[i].id_role == "36"){
                mn_rek_penjualan += "<li onclick=\"buka_halaman('rekap_penjualan_provinsi','5')\"><a>Per Provinsi</a></li>";
            }else if(json[i].id_role == "37"){
                mn_rek_penjualan += "<li onclick=\"buka_halaman('rekap_penjualan_kota','5')\"><a>Per Kota</a></li>";
            }else if(json[i].id_role == "38"){
                mn_rek_belanja += "<li onclick=\"buka_halaman('rekap_belanja_provinsi','5')\"><a>Per Provinsi</a></li>";
            }else if(json[i].id_role == "39"){
                mn_rek_belanja += "<li onclick=\"buka_halaman('rekap_belanja_kota','5')\"><a>Per Kota</a></li>";
            }else if(json[i].id_role == "40"){
                mn_rek_pendapatan += "<li onclick=\"buka_halaman('rekap_pendapatan_waktu','5')\"><a>Per Waktu</a></li>";
            }else if(json[i].id_role == "41"){
                mn_rek_pendapatan += "<li onclick=\"buka_halaman('rekap_pendapatan_kategori','5')\"><a>Per Kategori</a></li>";
            }else if(json[i].id_role == "42"){
                mn_rek_pendapatan += "<li onclick=\"buka_halaman('rekap_pendapatan_provinsi','5')\"><a>Per Provinsi</a></li>";
            }else if(json[i].id_role == "43"){
                mn_rek_pendapatan += "<li onclick=\"buka_halaman('rekap_pendapatan_kota','5')\"><a>Per Kota</a></li>";
            }else if(json[i].id_role == "44"){
                mn_rek_pendapatan += "<li onclick=\"buka_halaman('rekap_pendapatan_pemilik','5')\"><a>Per Pemilik</a></li>";
            }else if(json[i].id_role == "45"){
                mn_rek_data_pemilkush += "<li onclick=\"buka_halaman('rekap_data_pemilik_prov','5')\"><a>Per Provinsi</a></li>";
            }else if(json[i].id_role == "46"){
                mn_rek_data_pemilkush += "<li onclick=\"buka_halaman('rekap_data_pemilik_kota','5')\"><a>Per Kota</a></li>";
            }else if(json[i].id_role == "47"){
                mn_rek_data_konsumen += "<li onclick=\"buka_halaman('rekap_data_konsumen_prov','5')\"><a>Per Provinsi</a></li>";
            }else if(json[i].id_role == "48"){
                mn_rek_data_konsumen += "<li onclick=\"buka_halaman('rekap_data_konsumen_kota','5')\"><a>Per Kota</a></li>";
            }else if(json[i].id_role == "49"){
                $(".admin-kredit").show();
            }else if(json[i].id_role == "51"){
                mn_create_promo += "<li onclick=\"buka_halaman('promo_old/add_pemilik','7')\"><a>Untuk Pemilik Usaha</a></li>";
            }else if(json[i].id_role == "52"){
                mn_create_promo += "<li onclick=\"buka_halaman('promo_old/add_konsumen','7')\"><a>Untuk Konsumen</a></li>";
            }
        }
        $("#mn_kredit").html(mn_kredit);
        
        $("#mn_rek_data_konsumen").html(mn_rek_data_konsumen);
        $("#mn_rek_data_pemilkush").html(mn_rek_data_pemilkush);
        $("#mn_rek_pendapatan").html(mn_rek_pendapatan);
        $("#mn_rek_belanja").html(mn_rek_belanja);
        $("#mn_rek_penjualan").html(mn_rek_penjualan);
        
        $("#mn_lap_data").html(mn_lap_data);
        $("#mn_create_promo").html(mn_create_promo);
        $("#mn_lap_pendapatan").html(mn_lap_pendapatan);
        $("#mn_lap_belanja").html(mn_lap_belanja);
        $("#mn_lap_penjualan").html(mn_lap_penjualan);
        
        $("#menu_master").html(menu_master);
        $("#menu_transaksi").html(menu_transaksi);
    }
    
    var myLevel = "<?php echo $this->session->userdata("level") ?>";
    if (myLevel!="1"){
        $(".admin_aja").hide();
    }
</script>