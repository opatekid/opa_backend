<script></script>
<style type="text/css">
    #map {
        height: 1200px;
        width: 100%;
        position: relative;
    }
   #wrapper { position: relative; }
   #over_map { 
        background-color: white; position: 
        absolute; 
        top: 50px; 
        left: 10px; 
        z-index: 99; 
        padding: 10px; 
        width: 450px;
    }
    .option_tab {
        display:inline-block;
        margin-right: 10px;
        *display:inline;/* For IE7 */
        *zoom:1;/* For IE7 */
        white-space:normal;
        font-size:13px;
        vertical-align:top;
    }

    .controls-pac {
        margin-top: 10px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 366;
        margin-left: 0px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 366px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      .pac-container {
        font-family: Roboto;
      }

      #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
      }

      #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }
      #target {
        width: 345px;
      }
</style>
<style>
</style>

<div class="page-header">
    <div class="pull-left">
        <h1>Edit Ekspedisi</h1>
    </div>
    <div class="pull-right">
        <ul class="stats">
            <li class='lightred'>
                <i class="icon-calendar"></i>
                <div class="details">
                    <span class="big">-, -</span>
                    <span>-, -</span>
                </div>
            </li>
        </ul>
    </div>
</div>
<div class="breadcrumbs">
    <ul>
        <li>
            <a>Edit Ekspedisi</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a>Edit</a>
        </li>
    </ul>
    <div class="close-bread" style="display: none;">
        <a href="#"><i class="icon-remove"></i></a>
    </div>
</div>
<div class="row-fluid">
    <div class="span12">
        <div class="box box-color box-bordered">
            <div class="box-title">
                <h3><i class="icon-table"></i>Edit Ekspedisi</h3>
            </div>
            <div class="box-content" id="form_layout" style="overflow: scroll; height: 100%">
                
                <form id="form_data" class="span12" enctype="multipart/form-data" accept-charset="utf-8">
                    
                    <div class="control-group">
                        <h4 style="margin-bottom: 20px; margin-top: 20px">DETAIL EKSPEDISI</h4>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Nama Ekspedisi<span class="apt_bintang">*</span></label>
                        <div class="controls">
                            <input type="text" id="nama" name="nama" placeholder="Misal: JNE, JNT, TIKI, dll" value="<?php  echo $ekspedisi->nama; ?>" required class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"></label>
                        <div class="controls">
                        <img src="<?php echo URL_IMAGE_EKSPEDISI_WEBADMIN .  ($ekspedisi->foto != '' ? $ekspedisi->foto : $ekspedisi->id_ekspedisi); ?>" height='100' width='300' />
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">Upload foto<span class="apt_bintang"></span></label>
                        <div class="controls">
                            <input type="file" id="form_file" name="form_file" class="input-xlarge">
                        </div>
                        <input type="text" style="display: none;" id="foto" name="foto" value="foto.png" required class="input-xlarge">
                    </div>
                    
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary"><i class="icon-add"></i> Save</button>
                        <span id="form_notif"></span>
                        <span><img id="form_loading" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>

    function sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    $(document).ready(function() {
        $(".chosen-select").chosen({
            no_results_text: "Data tidak ada!",
            width: "100%",
            placeholder_text_single: "Pilih Salah Satu"
        });


        $('#id_kategories_produk').change(function() {
            id_kategories_produk = $('#id_kategories_produk').val();
            load_kota(id_provinsi);
        });

    });

    function hide_show_forms(e) {
        if (e == 1) {
            $('#form_for_usaha').show();
            $('#code_count_form').prop('required', true);
        } else {
            $('#form_for_usaha').hide();
            $('#code_count_form').prop('required', false);
        }
    }

    function hide_show_code_form() {
        var e = $('#target').val();
        if (e == 1) {
            $('#code_count_form').show();
        } else {
            $('#code_count_form').hide();
        }
    }


    $("#form_data").submit(function(e) {
        e.preventDefault();

        if (($('#form_file')[0].files).length > 0) {
            if (window.File && window.FileReader && window.FileList && window.Blob)
            {
                //get the file size and file type from file input field
                var fsize = $('#form_file')[0].files[0].size;
                
                if(fsize >1048576) //do something if file size more than 1 mb (1048576)
                {
                    alert("Ukuran file tidak boleh lebih dari 1 MB");
                    return;
                }
            }else{
                alert("Mohon perbarui browser Anda karena tidak cocok dengan sebagian fitur kami");
            }
        }

        $("#form_loading").show();
        var dataPost = new FormData($(this)[0]);
        $.ajax({
            url: "<?php echo URL_OPA.'update_ekspedisi/update/' . $ekspedisi->id_ekspedisi; ?>",
            data: dataPost,
            type: 'POST',
            dataType: 'json',
            async : false,
            cache : false,
            contentType : false,
            processData : false,
            success: function(data, textStatus, jqXHR) {
                console.log("OK");
                if (data.code == 1) {
                    
                    $('#form_data').find("input[type=text], textarea").val("");
                    $("#form_loading").fadeOut(1000, function() {
                        $("#form_notif").text(data.status);
                        $("#form_notif").show();
                        $("#form_notif").fadeOut(2000, function() {
                            buka_halaman('update_ekspedisi', 5);
                        });
                    });
                } else {
                        $("#form_notif").text(data.status);
                        $("#form_notif").show();
                        $("#form_loading").hide();
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("NO");
            }
        });
    });

</script>