<style type="text/css">

    

    /* Important part */
    .modal-dialog {
        overflow-y: initial !important
    }
    #data-modal-body {
        height: auto;
        overflow-y: auto;
    }
</style>
<script>
    var status_edit = "0";
    // load_opsi(json_role);
</script>

<div id="modal_data" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-header">
        <a type="button" class="close" data-dismiss="modal" aria-hidden="true">x</a>
        <h3 id="">Agen Ekspedisi</h3>
    </div>
    <form id="form_data" class="form-horizontal" enctype="multipart/form-data">
            <div id="data-modal-body" class="modal-body">
        <input type="hidden" id="form_id" name="form_id" />
        <span>Are you sure to delete this agent?</span>
        </div>
        <div class="modal-footer">
            <span id="form_notif"></span>
            <span><img id="form_loading" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>

            <a class="btn" data-dismiss="modal" aria-hidden="true">Cancel</a>
            <button type="submit" id="hapus_button" class="btn btn-primary"><i class="icon-delete"></i> Delete</button>
        </div>
    </form>
</div>


<div id="modal_add" class="modal hide fade" role="dialog">
    <div class="modal-dialog" >

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h3>Tambah Agen Baru</h3>
            </div>
            <div class="modal-body">
                <div class="control-group">
                <form id="add_form_data" method="post" enctype="multipart">
                    <div class="control-group">
                            <label class="control-label">Nama Agen<span class="apt_bintang">*</span></label>
                            <div class="controls">
                                <input type="text" id="nama" placeholder="Misal: Reguler 2-3 Hari" name="nama" class="input-xlarge" required />
                                <input type="text" id="id_ekspedisi" value="<?php echo $id_ekspedisi; ?>" name="id_ekspedisi" class="input-xlarge" style="display: none;" required />
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
        <div class="modal-footer">
            <span id="modal_data_notif"></span>
            <span><img id="modal_data_loading" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>

            <button class="btn" data-dismiss="modal" aria-hidden="true">Batal</button>
            <button onclick="tambah_agen()" class="btn btn-danger"><i class="icon-upload"></i> Simpan</button>
        </div>
    </div>
</div>


<div class="page-header">
    <div class="pull-left">
            <h1>Manage Agent</h1>
    </div>
    <div class="pull-right">
        <ul class="stats">
            <li class='lightred'>
                <i class="icon-calendar"></i>
                <div class="details">
                    <span class="big">-, -</span>
                    <span>-, -</span>
                </div>
            </li>
        </ul>
    </div>
</div>
<div class="breadcrumbs">
        <ul>
            <li>
                <a>Manage</a>
                <i class="icon-angle-right"></i>
            </li>
            <li>
                <a>Data of Agents</a>
            </li>
        </ul>
        <div class="close-bread" style="display: none;">
                <a href="#"><i class="icon-remove"></i></a>
        </div>
</div>

<div class="row-fluid">
    <div class="span12">
    </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <div class="box box-color box-bordered">
            <div class="box-title">
                <h3><i class="icon-table"></i> Agent Table</h3>
            </div>
            <div class="box-content nopadding"  style="overflow: scroll">                
                <table class="table usertable table-hover table-nomargin table-bordered" id="id_tabel">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Alamat</th>
                            <th>Edit</th>
                        </tr>
                    </thead>
                    <tbody id="isi_tabel">


                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    var id_ekspedisi = '<?php echo $id_ekspedisi; ?>';
    //load_upgrade(json_upgrade);
    $('#hapus_button').click(function() {
        console.log('clickeddddd');
    });

    $("#modal_add").modal("hide");
    function showAddModal() {
        $("#modal_add").modal("show");
    }


    $(document).ready(function(){
       
        $(".chosen-select").chosen({
            no_results_text: "Data tidak ada!",
            width: "100%",
            placeholder_text_single : "Pilih Salah Satu"
        });
    });
    
    function hapus_modal(id_agen){
        $("#modal_data").modal("show");
        $("#form_id").val(id_agen);
    }
    
    $("#form_data").submit(function (e){
        e.preventDefault();
        $("#form_loading").show();
        $.ajax({
            url: "<?php echo URL_OPA.'update_ekspedisi/delete_agen'; ?>",
            data: { 
                    "id_agen":$("#form_id").val()
                    },
            type: 'POST',
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                console.log("OK");
                $("#form_loading").fadeOut(1000, function (){
                    $("#form_notif").text(data.status);
                    $("#form_notif").show();
                    $("#form_notif").fadeOut(2000, function (){
                        datatable_server_side();
                        $("#modal_data").modal("hide");
                    });
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("NO");
            }
        });
    });

    function tambah_agen() {
        $("#form_loading").show();
        $.ajax({
            url: "<?php echo URL_OPA.'update_ekspedisi/tambah_agen'; ?>",
            data: { 
                    "id_ekspedisi":$("#id_ekspedisi").val(), 
                    "nama":$("#nama").val()
                    },
            type: 'POST',
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                console.log("OK");
                $("#form_loading").fadeOut(1000, function (){
                    $("#form_notif").text(data.status);
                    $("#form_notif").show();
                    $("#form_notif").fadeOut(2000, function (){
                        datatable_server_side();
                        $("#modal_data").modal("hide");
                    });
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("NO");
            }
        });
    };
    
    ///////////////////////
    
    var dataTable;
    
    datatable_server_side();
    
    function datatable_server_side(){
        console.log("server");
        if(dataTable != null) dataTable.fnDestroy();
        
        dataTable = $('#id_tabel').dataTable( {
            'iDisplayLength': 10,
            "aoColumnDefs": [
                { 'bSortable': false, 'aTargets': [ 0,1,2,3] }
            ],
            "processing": true,
            "serverSide": true,
            "ajax":{
                url :"<?php  echo URL_OPA; ?>update_ekspedisi/get_data_agen/" + id_ekspedisi, // json datasource
                type: "post",  // method  , by default get
                "data": function ( d ) {
                    
                },
                error: function(){  // error handling
                    $(".id_tabel-error").html("");
                    $("#id_tabel").append('<tbody class="id_tabel-error"><tr><th colspan="6">No item found in the database</th></tr></tbody>');
                    $("#id_tabel_processing").css("display","none");

                },
                "dataSrc": function ( json ) {
                    if (json.pesan == 'session expired') {
                        window.location.href = "<?php echo URL_OPA . 'merchant_login'; ?>";
                    }
                    json_upgrade = json.json;
                    return json.data;
                }
            }
            
        });
		
    }
</script>