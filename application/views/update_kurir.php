<style type="text/css">

    .modal {
    display: block !important; /* I added this to see the modal, you don't need this */
    }

    /* Important part */
    .modal-dialog {
        overflow-y: initial !important
    }
    #data-modal-body {
        height: auto;
        overflow-y: auto;
    }
</style>
<script>
    var json_upgrade = <?php echo json_encode($tran_upgrade)?>;
    var level = <?php echo json_encode($level)?>;
    var status_edit = "0";
    
    function load_opsi(json){
        for(var i=0;i<json.length;i++){
            if(json[i].id_role == "18"){
                status_edit = "1";
            }
        }   
    }
    
    function load_upgrade(tran_upgrade){
        //$("#id_tabel").dataTable().fnDestroy();
        var html ="";
        var tipe ="";
        for(var i =0;i<tran_upgrade.length;i++){
            if (tran_upgrade[i].tipe == '1'){
                tipe = "Tradisional";
            }else if(tran_upgrade[i].tipe == '2'){
                tipe = "Minimarket";
            }
            // var aksi_edit = "";
            
            // if(status_edit == "1"){
                aksi_edit = '<a onclick="buka_halaman(\'update_kurir/edit/' + tran_upgrade[i].id_kurir + '\', 10)"><i rel="tooltip" title="Ubah" style="cursor: pointer" class="icon icon-pencil"></i></a>';
            // }

            var aksi_delete = '<a onclick=\'hapus_modal("' + tran_upgrade[i].id_kurir + '")\'><i rel="tooltip" title="Hapus" style="cursor: pointer" class="icon icon-trash"></i></a>'
            var a ='<tr>\n\
                        <td>'+(i+1)+'</td>\n\
                        <td>'+tran_upgrade[i].id_kurir+'</td>\n\
                        <td>'+tran_upgrade[i].username+'</td>\n\
                        <td>'+tran_upgrade[i].nama+'</td>\n\
                        <td>'+tran_upgrade[i].kontak+'/'+ tran_upgrade[i].kontak_sms + '</td>\n\
                        <td>'+aksi_edit + '&nbsp' + aksi_delete + '&nbsp</td>\n\
                    </tr>';
            
            html = html + a;
        }
        $('#isi_tabel').html(html);
        $("#id_tabel").dataTable();
    }
    function load_level(level){
        var awal = '<option value="">-Pilih Level-</option>';
        var html = "";
        
        for (var i=0;i<level.length;i++){
            var a ='<option value="'+level[i].id+'">'+level[i].nama+'</option>';
           html = html + a;
        }

        $('#id_level').html(awal+html).trigger("liszt:updated");

    }
    // load_opsi(json_role);
</script>

<div id="modal_data" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-header">
        <a type="button" class="close" data-dismiss="modal" aria-hidden="true">x</a>
        <h3 id="">Data Kurir</h3>
    </div>
    <form id="form_data" class="form-horizontal" enctype="multipart/form-data">
            <div id="data-modal-body" class="modal-body">
        <input type="hidden" id="form_id" name="form_id" />
        <span>Apakah Anda yakin akan menghapus kurir ini?</span>
        </div>
        <div class="modal-footer">
            <span id="form_notif"></span>
            <span><img id="form_loading" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>

            <a class="btn" data-dismiss="modal" aria-hidden="true">Batal</a>
            <button type="submit" id="hapus_button" class="btn btn-primary"><i class="icon-delete"></i> Hapus</button>
        </div>
    </form>
</div>

<div class="page-header">
    <div class="pull-left">
            <h1>Kelola Kurir</h1>
    </div>
    <div class="pull-right">
        <ul class="stats">
            <li class='lightred'>
                <i class="icon-calendar"></i>
                <div class="details">
                    <span class="big">-, -</span>
                    <span>-, -</span>
                </div>
            </li>
        </ul>
    </div>
</div>
<div class="breadcrumbs">
        <ul>
            <li>
                <a>Manage</a>
                <i class="icon-angle-right"></i>
            </li>
            <li>
                <a>Data Kurir</a>
            </li>
        </ul>
        <div class="close-bread">
                <a href="#"><i class="icon-remove" style="display: none;" ></i></a>
        </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <div class="box box-color box-bordered">
            <div class="box-title">
                <h3><i class="icon-table"></i>Tabel Kurir</h3>
            </div>
            <div class="box-content nopadding"  style="overflow: scroll">                
                <table class="table usertable table-hover table-nomargin table-bordered" id="id_tabel">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>ID Kurir</th>
                            <th>Username</th>
                            <th>Nama Pengemudi</th>
                            <th>No Kontak</th>
                            <th>Opsi</th>
                        </tr>
                    </thead>
                    <tbody id="isi_tabel">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    load_upgrade(json_upgrade);
    load_level(level);

    $('#hapus_button').click(function() {
        console.log('clickeddddd');
    });


    $(document).ready(function(){
       
        $(".chosen-select").chosen({
            no_results_text: "Data tidak ada!",
            width: "100%",
            placeholder_text_single : "Pilih Salah Satu"
        });
    });
    
    function hapus_modal(id_pemilik){
        $("#modal_data").modal("show");
        $("#form_id").val(id_pemilik);
    }
    
    $("#form_data").submit(function (e){
        e.preventDefault();
        $("#form_loading").show();
        $.ajax({
            url: "<?php echo URL_OPA.'update_kurir/hapus_akun'; ?>",
            data: { 
                    "id_kurir":$("#form_id").val()
                    },
            type: 'POST',
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                console.log("OK");
                $("#form_loading").fadeOut(1000, function (){
                    $("#form_notif").text(data.status);
                    $("#form_notif").show();
                    $("#form_notif").fadeOut(2000, function (){
                        buka_halaman('update_kurir/index', 10);
                        $("#modal_data").modal("hide");
                    });
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("NO");
            }
        });
    });
    
    ///////////////////////
    
    var dataTable;
    
    // datatable_server_side();
    
    function datatable_server_side(){
        console.log("server");
        if(dataTable != null) dataTable.fnDestroy();
        
        dataTable = $('#id_tabel').dataTable( {
            'iDisplayLength': 10,
            "aoColumnDefs": [
                { 'bSortable': false, 'aTargets': [ 0,1,2,3,4,5 ] }
            ],
            "processing": true,
            "serverSide": true,
            "ajax":{
                url :"<?php  echo URL_OPA; ?>update_kurir/get_data", // json datasource
                type: "post",  // method  , by default get
                "data": function ( d ) {
                    
                },
                error: function(){  // error handling
                    $(".id_tabel-error").html("");
                    $("#id_tabel").append('<tbody class="id_tabel-error"><tr><th colspan="6">No data found in the server</th></tr></tbody>');
                    $("#id_tabel_processing").css("display","none");

                },
                "dataSrc": function ( json ) {
                    if (json.pesan == 'session expired') {
                        window.location.href = "<?php echo URL_OPA . 'merchant_login'; ?>";
                    }
                    json_upgrade = json.json;
                    return json.data;
                }
            }
            
        });
        
    }
</script>