<script>
    var provinsi = <?php echo json_encode($provinsi)?>;
    var usaha = <?php echo json_encode($usaha)?>;
    var carii ="";
    function load_cari(cari){
        var html ="";
        for(var i =0;i<cari.length;i++){
            var a ='<tr>\n\
            <td>'+(i+1)+'</td>\n\
            <td>'+cari[i].tanggal_order+'</td>\n\
            <td>'+cari[i].nama+'</td>\n\
            <td>'+cari[i].alamat+'</td>\n\
            <td>'+cari[i].nama_provinsi+'</td>\n\
            <td>'+cari[i].nama_kota+'</td>\n\
        </tr>';
        
        html = html + a;
    }
    $('#isi_tabel').html(html);
    $("#id_tabel").dataTable();
}
function load_provinsi(provinsi){
    var awal = '<option value="0">-All-</option>';
    var html = "";
    
    for (var i=0;i<provinsi.length;i++){
        var a ='<option value="'+provinsi[i].id+'">'+provinsi[i].nama+'</option>';
        html = html + a;
    }

    $('#id_provinsi').html(awal+html).trigger("liszt:updated");
    $('#id_kota').html(awal).trigger("liszt:updated");

}
function load_usaha(usaha){
    var awal = '<option value="">-Pilih Usaha-</option>';
    var html = "";
    
    for (var i=0;i<usaha.length;i++){
        var a ='<option value="'+usaha[i].id_pemilik+'">'+usaha[i].nama+'</option>';
        html = html + a;
    }

    $('#id_usaha').html(awal+html).trigger("liszt:updated");

}

</script>
<div class="page-header">
    <div class="pull-left">
        <h1>Promo</h1>
    </div>
    <div class="pull-right">
        <ul class="stats">
            <li class='lightred'>
                <i class="icon-calendar"></i>
                <div class="details">
                    <span class="big">-, -</span>
                    <span>-, -</span>
                </div>
            </li>
        </ul>
    </div>
</div>
<div class="breadcrumbs">
    <ul>
        <li>
            <a>Promo</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a>Create Promo</a>
        </li>
    </ul>
    <div class="close-bread">
        <a href="#"><i class="icon-remove" style="display: none;" ></i></a>
    </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <div class="box box-color box-bordered">
            <div class="box-title">
                <h3><i class="icon-table"></i>Untuk Pemilik Usaha</h3>
            </div>
            <div class="box-content nopadding"  style="overflow: scroll">
                <form id="form_data" class="form-horizontal" enctype="multipart/form-data">
                    <div class="control-group" style=" margin: 20px;">
                        <label class="control-label">Nama Promo</label>
                        <div class="controls">
                            <input type="text" id="nama" name="nama" required class="input-xlarge" >
                        </div>
                    </div>
                    <div class="control-group" id="description_form" style=" margin: 20px;">
                        <label class="control-label">Deskripsi promo</label>
                        <div class="controls">
                            <textarea type="text" id="description" placeholder="Teks deskripsi promo pada tampilan input kode" name="description" class="input-xlarge" />
                        </div>
                    </div>
                    <div class="control-group" style=" margin: 20px; display: none;">
                        <label class="control-label">Promo Untuk</label>
                        <div class="controls">
                            <div class="input-xlarge">
                                <select id="promo_for" onchange="hide_show_forms(this.value);" name="promo_for" class='chosen-select input-xlarge' required>
                                    <option value="1" selected="">User Pemilik Usaha</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div id="form_for_usaha">
                        <div class="control-group" style=" margin: 20px;">
                            <label class="control-label">Provinsi</label>
                            <div class="controls">
                                <select id="id_provinsi" name="id_provinsi" class='chosen-select input-xlarge' required onchange="load_kotkab(this.value,'2')">
                                </select>
                            </div>
                        </div>
                        <div class="control-group" style=" margin: 20px;">
                            <label class="control-label">Kota</label>
                            <div class="controls">
                                <div class="input-xlarge">
                                    <select id="id_kota" name="id_kota" class='chosen-select input-xlarge' required>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="control-group" style=" margin: 20px;">
                            <label class="control-label">Jenis Promo</label>
                            <div class="controls">
                                <div class="input-xlarge">
                                    <select id="jenis_promo" name="jenis_promo" class='chosen-select input-xlarge' required>
                                        <option value="1">No price</option>
                                        <option value="2">No fee</option>
                                        <option value="3">Free fee 1 year</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="control-group" style=" margin: 20px; display: none;">
                            <label class="control-label">Target</label>
                            <div class="controls">                        
                                <div class="input-xlarge">
                                    <select id="target" name="target" onchange="hide_show_code_form()" class='chosen-select input-xlarge' required>
                                        <option value="1" selected="">User with code</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="control-group" id="code_count_form" style=" margin: 20px;">
                            <label class="control-label">Jumlah toko peserta</label>
                            <div class="controls">
                                <input type="number" id="code_count" value="1" placeholder="Number of codes to generate" name="code_count" class="input-xlarge" >
                            </div>
                        </div> -->
                    </div>
                    <div class="control-group" id="message_form" style=" margin: 20px;">
                        <label class="control-label">Pesan promo</label>
                        <div class="controls">
                            <textarea  id="message" placeholder="Pesan Selamat promo untuk user setelah selesai aktivasi" name="message" class="input-xlarge" />
                        </div>
                    </div>
                    <div class="control-group" style=" margin: 20px;">
                    <label class="control-label">Jumlah konsumen</label>
                        <div class="controls">
                            <input type="number" id="max_konsumen" value="1" placeholder="Jumlah maksimum konsumen berlangganan per toko" name="max_konsumen" class="input-xlarge" >
                        </div>
                    </div>
                    <div class="control-group" style=" margin: 20px;">
                        <label class="control-label">Masa berlaku</label>
                        <div class="controls">
                            <input type="number" id="valid_duration" value="0" placeholder="Masa berlaku promo dalam hitungan bulan" name="valid_duration" class="input-xlarge" >
                        </div> Bulan
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary"><i class="icon-add"></i> Buat Promo</button>
                        <span id="form_notif"></span>
                        <span><img id="form_loading" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    load_provinsi(provinsi);
    load_usaha(usaha);
    $(document).ready(function(){
        $(".chosen-select").chosen({
            no_results_text: "Data tidak ada!",
            width: "100%",
            placeholder_text_single : "Pilih Salah Satu"
        });
    });

    function hide_show_forms(e){
        if (e == 1) {
            $('#form_for_usaha').show();
            $('#code_count_form').prop('required', true);
        } else {
            $('#form_for_usaha').hide();
            $('#code_count_form').prop('required', false);
        }
    }

    function hide_show_code_form(){
        var e = $('#target').val();
        if (e == 1) {
            $('#code_count_form').show();
        } else {
            $('#code_count_form').hide();
        }
    }

    var kotkab ="";
    function load_kotkab(e,typ){
        var awal = '<option value="0">-All-</option>';
        $.ajax({          
            url: "<?php echo URL_OPA.'lap_belanja_kota/get_kotkab/'; ?>"+e,
            data:"",
            type: 'GET',
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                var html = "";

                for (var i=0;i<data.kotkab.length;i++){
                    var a ='<option value="'+data.kotkab[i].id+'">'+data.kotkab[i].nama+'</option>';
                    html = html + a;
                }
                kotkab = data.kotkab;
                $('#id_kota').html(awal+html).trigger("liszt:updated");

            },
            error: function (jqXHR, textStatus, errorThrown) {

            }
        });
    }

    $("#form_data").submit(function(e){
        e.preventDefault();
            $("#form_loading").show();
            $.ajax({
                url: "<?php echo URL_OPA.'promo_old/create'; ?>",
                data: $('#form_data').serialize(),
                type: 'POST',
                dataType: 'json',
                success: function (data, textStatus, jqXHR) {
                    console.log("OK");
                    buka_halaman('promo_old', '')
                    $('#form_data').find("input[type=text], textarea").val("");
                    $("#form_loading").fadeOut(1000, function (){
                        $("#form_notif").text(data.status);
                        $("#form_notif").show();
                        $("#form_notif").fadeOut(2000, function (){
                            json_dkredit=data.list;
                        });
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log("NO");
                }
            });
    });
</script>