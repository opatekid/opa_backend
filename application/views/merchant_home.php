<!doctype html>
<html>
<head>
	<?php $this->load->view('head'); ?>
    <style type="text/css">

        

    /* Important part */
    .modal-dialog {
        overflow-y: initial !important
    }
    #pass-modal-body {
        height: 150px;
        overflow-y: auto;
    }

    #profil-modal-body {
        height: 450px;
        overflow-y: auto;
    }
</style>
</head>

<body>
    <?php $this->load->view('merchant_menu'); ?>
    
    <div class="container-fluid" id="content">
        <div id="main" style="margin-left: 0px;">
            <div id="load" class="loader"></div>
            <div class="container-fluid" id="isi">

            </div>
        </div>        
    </div>
    
<div class="hider">
    <div id="modal_ubahpass" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
        <div class="modal-header">
            <a type="button" class="close" data-dismiss="modal" aria-hidden="true">x</a>
            <h3>Ubah Password</h3>
        </div>
        <form id="form_ubahpass" class="form-horizontal" enctype="multipart/form-data">
            <div id="pass-modal-body" class="modal-body">
                <div class="control-group" style="margin-left: 15%;">
                    <div class="input-append">
                        <input class="input-xlarge" required id="n_pass" type="password" placeholder="New Password"><span class="add-on"><i class="icon-key"></i></span>
                    </div>
                </div>
                <div class="control-group" style="margin-left: 15%;">
                    <div class="input-append">
                        <input class="input-xlarge" required id="rn_pass" type="password" placeholder="Retype new password"><span class="add-on"><i class="icon-key"></i></span>
                    </div>
                    <p id="notif_npwd" style="display: none;">Pastikan password baru anda sama</p>
                </div>
                <hr>
                <div class="control-group" style="margin-left: 15%;">
                    <div class="input-append">
                        <input class="input-xlarge" required id="o_pass" type="password" placeholder="Old Password"><span class="add-on"><i class="icon-key"></i></span>
                    </div>
                    <p id="notif_opwd" style="display: none;">Password salah!</p>
                </div>
            </div>
            <div class="modal-footer">
                <span id="notif_form_upwd"></span>
                <span><img id="load_form_upwd" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>
                <a class="btn" data-dismiss="modal" aria-hidden="true">Batal</a>
                <button type="submit" class="btn btn-primary"><i class="icon-save"></i> Simpan</button>
            </div>
        </form>
    </div>
    
    </div>

<div class="hider">
    <div id="modal_sunting" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
        <div class="modal-header">
            <a type="button" class="close" data-dismiss="modal" aria-hidden="true">x</a>
            <h3>Ubah Profil</h3>
        </div>
        <form id="form_sunting" class="form-horizontal" enctype="multipart/form-data">
            <div class="modal-dialog" style="overflow-y: scroll; max-height:85%;" > 
      
            <div id="profil-modal-body" class="modal-body">
                <div class="control-group">
                    <div class="controls">
                        <img src="<?php echo URL_IMAGE_PEMILIK . $pengguna->foto; ?>" style="width: 150px; height: 150px;">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Nama<span class="apt_bintang">*</span></label>
                    <div class="controls">
                        <input type="text" id="id_nama_sunting" name="id_nama_sunting" required class="input-xlarge">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Username<span class="apt_bintang">*</span></label>
                    <div class="controls">
                        <input type="text" id="id_username_sunting" name="id_username_sunting" required class="input-xlarge">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Email<span class="apt_bintang">*</span></label>
                    <div class="controls">
                        <input type="email" id="id_email_sunting" value="<?php echo $pengguna->email; ?>" name="id_email_sunting" required class="input-xlarge">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Gambar Profil<span class="apt_bintang">*</span></label>
                    <div class="controls">
                        <input type="file" id="form_file" name="form_file" class="input-xlarge">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Jenis Usaha<span class="apt_bintang">*</span></label>
                    <div class="controls" id="kategories_div">
                    <?php

                    foreach ($kategories as $kategori) {
                        $checked = "";
                        if (in_array($kategori->id_kategori, $selected_kategories)) {
                            $checked = "checked=\"checked\"";
                        }
                        echo "<div class=\"checkbox\"><label><input type=\"checkbox\" ".$checked." class=\"kategori\" name=\"".$kategori->id_kategori."\">".$kategori->nama_kategori."</label></div>";
                    }


                    ?>
                    </div>
                </div>
            </div>
        </div>
            <div class="modal-footer">
                <span id="notif_form_sunting"></span>
                <span><img id="load_form_sunting" src="<?php echo URL_IMG;?>loading.gif" alt="loading" style="display: none;" /></span>
                <a class="btn" data-dismiss="modal" aria-hidden="true">Batal</a>
                <button type="submit" class="btn btn-primary"><i class="icon-save"></i> Simpan</button>
            </div>
        </form>
    </div>
    </div>  

</body>
<script>
var kategories = JSON.parse('<?php echo json_encode($kategories); ?>');
    var selected_kategories = JSON.parse('<?php echo json_encode($selected_kategories); ?>');
    console.log(selected_kategories);
    var head_pengguna = <?php echo json_encode($pengguna);?>;
    console.log(head_pengguna);
    console.log(kategories);
    console.log(selected_kategories);
    $('#modal_sunting').parent().hide();
    $('#modal_ubahpass').parent().hide();
    $('#modal_sunting').on('show.bs.modal', function() {
        $(this).parent().show();
        $('#kategories_div').html('');
     for (var i = 0; i < kategories.length; i++) {
        var kategori = kategories[i];
        var checked = "";
        console.log(kategori.id_kategori);
        if (selected_kategories.indexOf(kategori.id_kategori) > -1) {
            checked = "checked=\"checked\"";
        }

        $('#kategories_div').append("<div class=\"checkbox\"><label><input type=\"checkbox\" " + checked + " class=\"kategori\" name=\"" + kategori.id_kategori + "\">" + kategori.nama_kategori + "</label></div>");
     }
});

$('#modal_sunting').on('hide.bs.modal', function() {
 $(this).parent().hide();
})

    $('#modal_ubahpass').on('show.bs.modal', function() {
 $(this).parent().show();
})
$('#modal_ubahpass').on('hide.bs.modal', function() {
 $(this).parent().hide();
})

    var selected = [];
    function buka_halaman(a,angka) {

        var dir ="<?php echo URL_OPA; ?>"+a;
        $("#load").show();
        $(".menu_main").removeClass("active");
        $("#menu_main"+angka).addClass("active");
        
        //alert('ijdj');
        $.ajax({ 
            url: dir,
            success: function(response){
             $('#isi').html(response);
             $("#load").hide();

         }, 
         error: function(response){ 
           alert('salah'+response.toString());
              //  $('#status').html(response);
              $("#load").hide();
          } 
      }); 
    }

    buka_halaman('update_pusaha', '2');
    
    function showgantipass(){
        $("#n_pass").val("");
        $("#rn_pass").val("");
        $("#o_pass").val("");
        $("#modal_ubahpass").modal("show");
    }
    
    $("#form_ubahpass").submit(function (e) { 
        e.preventDefault();
        $("#notif_npwd").hide();
        $("#notif_opwd").hide();
        $("#notif_form_upwd").hide();
        $("#load_form_upwd").show();

        $.ajax({
            url:"<?php echo URL_OPA;?>merchant_home/ubah_pwd/",
            data: {"new_pass":$("#n_pass").val(),
            "rnew_pass":$("#rn_pass").val(),
            "old_pass":$("#o_pass").val()
        },
        type: 'POST',
        dataType: 'json',
        success: function (data, textStatus, jqXHR) {
            console.log(data.status);
            $("#load_form_upwd").fadeOut(1000, function (){
                if(data.status == "satu"){
                    $("#notif_npwd").fadeIn(1000);
                }else if(data.status == "dua"){
                    $("#notif_opwd").fadeIn(1000);
                }else{
                    $("#notif_form_upwd").text(data.status);
                    $("#notif_form_upwd").fadeIn(1000);
                    $("#notif_form_upwd").fadeOut(2000, function (){
                        $("#modal_ubahpass").modal("hide");
                    });
                }
            });

        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR.status);

        }
    });
    });
    
    function showsunting(){
        $("#id_nama_sunting").val(head_pengguna.nama);
        $("#id_username_sunting").val(head_pengguna.username);
        $("#id_email_sunting").val(head_pengguna.email);
        $("#modal_sunting").modal("show");
    }
    
    $("#form_sunting").submit(function (e) {
        e.preventDefault();

$('.kategori:input:checked').each(function() {
    selected.push($(this).attr('name'));
    console.log(selected);
});

        $("#notif_form_sunting").hide();
        $("#load_form_sunting").show();

        var dataPost = new FormData($(this)[0]);
        dataPost.append("nama",$("#id_nama_sunting").val());
        dataPost.append("username",$("#id_username_sunting").val());
        dataPost.append("email",$("#id_email_sunting").val());
        dataPost.append("id_kategories", JSON.stringify(selected));
        $.ajax({
            url:"<?php echo URL_OPA;?>merchant_home/sunting_profil/",
            data: dataPost,
            type: 'POST',
            dataType: 'json',
            async : false,
            cache : false,
            contentType : false,
            processData : false,
            success: function (data, textStatus, jqXHR) {
                selected = [];
                $("#load_form_sunting").fadeOut(1000, function (){
                    if(data.stat == "1"){
                        $("#notif_form_sunting").text(data.status);
                        $("#notif_form_sunting").fadeIn(1000);
                    }else if(data.stat == "2"){
                        $("#notif_form_sunting").text(data.status);
                        $("#notif_form_sunting").fadeIn(1000);
                    }else{
                        $("#notif_form_sunting").text(data.status);
                        $("#notif_form_sunting").fadeIn(1000);
                        $("#notif_form_sunting").fadeOut(2000, function (){
                            $("#id_nama_user").html(data.namaad);
                            head_pengguna = data.list;
                            $("#modal_sunting").modal("hide");
                            location.reload();
                        });
                    }
                });

            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR.status);

            }
        });
    });

    function tandaPemisahTitik(b){
        var _minus=false;
        if (b < 0) _minus=true;
        b=b.toString();
        b=b.replace(".", "");
        b=b.replace("-", "");
        c="";
        panjang=b.length;
        j=0;
        for (i=panjang; i > 0; i--) {
            j=j + 1;
            if (((j % 3) == 1) && (j != 1)) {
                c=b.substr(i - 1, 1) + "." + c;
            } else {
                c=b.substr(i - 1, 1) + c;
            }
        }
        if (_minus) c="-" + c;
        return c;
    }
</script>
</html>