<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE', 'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 'ab');
define('FOPEN_READ_WRITE_CREATE', 'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
define('EXIT_SUCCESS', 0); // no errors
define('EXIT_ERROR', 1); // generic error
define('EXIT_CONFIG', 3); // configuration error
define('EXIT_UNKNOWN_FILE', 4); // file not found
define('EXIT_UNKNOWN_CLASS', 5); // unknown class
define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
define('EXIT_USER_INPUT', 7); // invalid user input
define('EXIT_DATABASE', 8); // database error
define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

define("DOMAIN_NAME", "https://opaindonesia.com/webadmin/");
define("DOMAIN_NAME_UNSECURE", "http://opaindonesia.com/webadmin/");
define("ROOT_DOMAIN_NAME", "https://opaindonesia.com/");
define("ROOT_DOMAIN_NAME_UNSECURE", "http://opaindonesia.com/");
// define("DOMAIN_NAME", "http://app.grobang.com/");
define("SABANA_ID_PARTNER", "79238802");


define("ROOT_URL_IMAGE_KONSUMEN", ROOT_DOMAIN_NAME."berkas/image/konsumen/");
define("ROOT_URL_IMAGE_AVATAR", ROOT_DOMAIN_NAME."./berkas/image/avatar/");
define("ROOT_URL_IMAGE_AVATAR_PEMILIK", ROOT_DOMAIN_NAME."./berkas/image/avatar/pemilik/");
define("ROOT_URL_IMAGE_AVATAR_KURIR", ROOT_DOMAIN_NAME."./berkas/image/avatar/kurir/");
define("ROOT_URL_IMAGE_PEMILIK", ROOT_DOMAIN_NAME."berkas/image/pemilik/");
define("ROOT_URL_IMAGE_PRODUK", ROOT_DOMAIN_NAME."berkas/image/produk/");
define("ROOT_URL_IMAGE_PROMO", ROOT_DOMAIN_NAME."berkas/image/promo/");
define("ROOT_URL_IMAGE_PROMO_WEBADMIN", ROOT_DOMAIN_NAME."webadmin/berkas/image/promo/");
define("ROOT_URL_IMAGE_PARTNER", ROOT_DOMAIN_NAME."./berkas/image/partner/");
define("ROOT_URL_IMAGE_PARTNER_WEBADMIN", ROOT_DOMAIN_NAME."webadmin/berkas/image/partner/");
define("ROOT_URL_IMAGE_IKATEGORI", ROOT_DOMAIN_NAME."berkas/image/icon_kategori/");
define("ROOT_URL_IMAGE_TEMP", ROOT_DOMAIN_NAME."berkas/image/temp/");
define("ROOT_URL_IMAGE_PEMILIK_WEBADMIN", ROOT_DOMAIN_NAME."webadmin/berkas/image/pemilik/");

define("URL_IMAGE_KONSUMEN", DOMAIN_NAME."berkas/image/konsumen/");
define("URL_IMAGE_AVATAR", DOMAIN_NAME."berkas/image/avatar/");
define("URL_IMAGE_AVATAR_PEMILIK", DOMAIN_NAME."berkas/image/avatar/pemilik/");
define("URL_IMAGE_AVATAR_KURIR", DOMAIN_NAME."berkas/image/avatar/kurir/");
define("URL_IMAGE_PEMILIK", DOMAIN_NAME."berkas/image/pemilik/");
define("URL_IMAGE_PRODUK", DOMAIN_NAME."berkas/image/produk/");
define("URL_IMAGE_PROMO", DOMAIN_NAME."berkas/image/promo/");
define("URL_IMAGE_PROMO_WEBADMIN", DOMAIN_NAME."webadmin/berkas/image/promo/");
define("URL_IMAGE_PARTNER", DOMAIN_NAME."berkas/image/partner/");
define("URL_IMAGE_PARTNER_WEBADMIN", DOMAIN_NAME."webadmin/berkas/image/partner/");
define("URL_IMAGE_IKATEGORI", DOMAIN_NAME."berkas/image/icon_kategori/");
define("URL_IMAGE_TEMP", DOMAIN_NAME."berkas/image/temp/");
define("URL_IMAGE_PEMILIK_WEBADMIN", DOMAIN_NAME."webadmin/berkas/image/pemilik/");

define("PATH_IMAGE_TEMP", FCPATH."./berkas/image/temp/");
define("PATH_IMAGE_KONSUMEN", FCPATH."./berkas/image/konsumen/");
define("PATH_IMAGE_AVATAR", FCPATH."./berkas/image/avatar/");
define("PATH_IMAGE_AVATAR_KURIR", FCPATH."./berkas/image/avatar/kurir/");
define("PATH_IMAGE_AVATAR_PEMILIK", FCPATH."./berkas/image/avatar/pemilik/");
define("PATH_IMAGE_IKATEGORI", FCPATH."./berkas/image/icon_kategori/");
define("PATH_IMAGE_PEMILIK", FCPATH."./berkas/image/pemilik/");
define("PATH_IMAGE_PEMILIK_WEBADMIN", FCPATH."./webadmin/berkas/image/pemilik/");
define("PATH_IMAGE_PRODUK", FCPATH."./berkas/image/produk/");
define("PATH_IMAGE_PARTNER", FCPATH."./berkas/image/partner/");

define("PATH_XLSX_PRODUK", FCPATH."./berkas/xlsx/produk/");
define("URL_XLSX_PRODUK", DOMAIN_NAME."berkas/xlsx/produk/");
define("PATH_IMAGE_PROMO", FCPATH."./webadmin/berkas/image/promo/");
define("PATH_IMAGE_PROMO_WEBADMIN", FCPATH."./webadmin/berkas/image/promo/");

define("EMAIL_ASAL", "siakad.poltekom@gmail.com");
define("EMAIL_PASSWORD", "siakadp0lt3k0m");

define("URL_JS", DOMAIN_NAME."berkas/js/");
define("URL_CSS", DOMAIN_NAME."berkas/css/");
define("URL_IMG", DOMAIN_NAME."berkas/img/");
define("URL_IMAGE", DOMAIN_NAME."berkas/image/");
define("URL_OPA", DOMAIN_NAME."index.php/");

define("URL_FRONT_JS", DOMAIN_NAME."berkas/front/js/");
define("URL_FRONT_CSS", DOMAIN_NAME."berkas/front/css/");
define("URL_FRONT_IMG", DOMAIN_NAME."berkas/front/img/");
define("URL_FRONT_IMAGE", DOMAIN_NAME."berkas/front/image/");


define("URL_LP_JS", DOMAIN_NAME."js/");
define("URL_LP_CSS", DOMAIN_NAME."css/");
define("URL_LP_IMG", DOMAIN_NAME."img/");
define("URL_LP_IMAGE", DOMAIN_NAME."images/");

define("PASS_OPA","adminopa");
define("URL_LOGIN", DOMAIN_NAME."index.php/login/");
define("URL_MERCHANT_LOGIN", DOMAIN_NAME."index.php/merchant_login/");

// define("KEY_GCM","AIzaSyDPidF85OeHR9hU-xFX5HhqeaRIGgQxazg");
// define("KEY_GCM_KONSUMEN","AIzaSyAystwuXoMEwtEbXSg3xTy6KlH7uSPnXMk");
// define("KEY_GCM_PEMILIK","AIzaSyBtc0Hby6R3wUDvXPBRZHSaVvX1Bw8Y0NA");
// define("KEY_GCM_KONSUMEN","AIzaSyBJeihjDBiBJl0z9OH3ctCDly1oEtyWrps");
// define("KEY_GCM_PEMILIK","AIzaSyBJeihjDBiBJl0z9OH3ctCDly1oEtyWrps");
// define("KEY_GCM_KONSUMEN","AAAADvVGMKU:APA91bHOmNJQ0XW2kL7yzDDLQtpPMSv3G05ZIcBfTSJ3E5W02J33iR4yQczl-mCMChIqUCgkt3Gukw8kFyblNgt-GAJRcorqeK9rYvj-ruwkBkD6ZuGfXw_PtTgYSjGMQ-JdMN9EZQNj");
// define("KEY_GCM_PEMILIK","AAAADvVGMKU:APA91bHOmNJQ0XW2kL7yzDDLQtpPMSv3G05ZIcBfTSJ3E5W02J33iR4yQczl-mCMChIqUCgkt3Gukw8kFyblNgt-GAJRcorqeK9rYvj-ruwkBkD6ZuGfXw_PtTgYSjGMQ-JdMN9EZQNj");

define("KEY_GCM_KONSUMEN","AAAAXDO99Mo:APA91bG6iJ_ZfkxMZepPUBUuMLBP2DmwuJEJAf3-xbYqcoI3ln13g0rPOZK33O6_ysfksIilWGSXZlRinQ_bYQoK9k9feK7C6p2rRBzlXq6mkzYMmv-ZfBrIPUAQXaL3vLBAKqirGXHc");
define("KEY_GCM_PEMILIK","AAAAXDO99Mo:APA91bG6iJ_ZfkxMZepPUBUuMLBP2DmwuJEJAf3-xbYqcoI3ln13g0rPOZK33O6_ysfksIilWGSXZlRinQ_bYQoK9k9feK7C6p2rRBzlXq6mkzYMmv-ZfBrIPUAQXaL3vLBAKqirGXHc");
define("KEY_GCM_PEDAGANG","AAAAMEUgEBQ:APA91bHMxIIbhA-PHaIEe4S2CFXsN2XcupsKTkrm4Ci0fjSPDl3C5h-7oqraPwk9GzgtYRe8BXLkRathcUccWkzrIPWop6BMKy9aBrfo2HECcyjxe-IGtIeJRnygT5UZt0wr4gkAm7om");
define("KEY_GCM_KURIR","AAAAXDO99Mo:APA91bG6iJ_ZfkxMZepPUBUuMLBP2DmwuJEJAf3-xbYqcoI3ln13g0rPOZK33O6_ysfksIilWGSXZlRinQ_bYQoK9k9feK7C6p2rRBzlXq6mkzYMmv-ZfBrIPUAQXaL3vLBAKqirGXHc");
//define("KEY_GCM_KURIR","AAAAYYp5skU:APA91bHYFg7vce7vzJrpa-je19KDILaB6gLRqdHMwx36pHmUX6_xsgJvlPBbpBD1gtZcqp6NoO9RXfLZD9qDD1ehfnxizDlUF2OIE2tzw-kzKU0L9APtOh2Ya7ERTL6CvDF6WFOqWl87");
