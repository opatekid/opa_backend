<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lap_data_pemiliku extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model("model_laporan");
        $this->load->model("mobile_v2/model_api_pemilik", "modelmu");
    }

    public function index() {
        $this->load->view('lap_data_pemiliku');
        
    }

    public function upload_produk($id_pemilik) {

        $pemiliks = $this->modelmu->get_pemilik($id_pemilik);

        if (count($pemiliks) > 0) {
            $data["pemilik"] = $pemiliks[0];
        }
        $this->load->view('upload_produk',$data);   
    }
    
//    public function caritgl() {
//        $tgl = $this->input->post("tanggal");
////        $tanggal = date($tgl, "Y-m-d H:i:s");
//        //20-11-2016
//        //[20,11,2016]
//        $arr_tgl = explode("-", $tgl);
//        $tanggal = $arr_tgl[2]."-".$arr_tgl[1]."-".$arr_tgl[0];
//        $tglawal = substr($tanggal, 0, 4)."-01-01";
//        $data = array("cari" => $this->model_laporan->caritgl_data_pemiliku($tanggal,$tglawal)->result());
//        echo json_encode($data);
//    }
//    
//    public function caribln() {
//        $bulan = $this->input->post("bulan");
//        $tahun = $this->input->post("tahun");
//        $data = array("cari" => $this->model_laporan->caribln_data_pemiliku($bulan,$tahun)->result());
//        echo json_encode($data);
//    }
//    public function carithn() {
//        $tahun = $this->input->post("tahun");
//        $data = array("cari"=>  $this->model_laporan->carithn_data_pemiliku($tahun)->result());
//        echo json_encode($data);
//    }
    
    /////////serverside///////////

    public function delete() {
        $id_pemilik            = $this->input->post("id");
        $this->modelmu->hapus_akun($id_pemilik);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        
        echo json_encode($data);
    }

    public function get_data(){
        
        //$tglawal,$tanggal,$bulan,$tahun,$jenis,$tipe
                
        $requestData        = $this->input->post();
        $tgl            = $this->input->post("tanggal");
        $bulan              = $this->input->post("bulan");
        $tahun              = $this->input->post("tahun");
        $tipe               = $this->input->post("tipe");
        $tanggal            = "";
        $tglawal            = "";
        $arr_tgl = explode("-", $tgl);
        if($tipe == "1"){
            $tanggal = $arr_tgl[2]."-".$arr_tgl[1]."-".$arr_tgl[0];
            $tglawal = substr($tanggal, 0, 4)."-01-01";
        }
        
        $pesan = "";
        
        $count = $this->model_laporan->filter_data_pemiliku_get_data($tglawal,$tanggal,$bulan,$tahun,$tipe);
        $totalFiltered = $count;
        $totalData =  $count;

        if( !empty($requestData['search']['value']) ) {
            $query = $this->model_laporan->filter_data_pemiliku($requestData,$tglawal,$tanggal,$bulan,$tahun,$tipe);
            $totalFiltered = $query->num_rows();
            $pesan = "pencarian..";
        }

        $query = $this->model_laporan->order_data_pemiliku($requestData,$tglawal,$tanggal,$bulan,$tahun,$tipe);

        $data = array();
        
        $no = $requestData['start']+1;
        
        $index = 0;
        foreach ($query->result() as $row){
            $tipe = "Tradisional";
            switch (intval($row->tipe)) {
                case 1:
                    $tipe = "Tradisional";
                    break;
                case 2:
                    $tipe = "Minimarket";
                    break;
                case 3:
                    $tipe = "Kaki Lima";
                    break;
                case 4:
                    $tipe = "Pedagang Keliling";
                    break;
                default:
                    break;
            }
            $nestedData=array(); 
            $nestedData[] = $row->tanggal_insert;
            $nestedData[] = $row->nama;
            $nestedData[] = $row->id_pemilik;
            $nestedData[] = $row->kontak;
            $nestedData[] = $row->alamat;
            $nestedData[] = $tipe;

            $status = "<span style='color:red'>Non-Aktif</span>";
            $allow_delivery = "<span style='color:red'>Tidak Bisa</span>";
            $aktif_text = "Aktifkan";
            $aktif_icon = "icon icon-ok";

                $delivery_text = "Aktifkan";
                $delivery_icon = "icon icon-ok";

            if (intval($row->allow_delivery) == 1) {
                $delivery_text = "Non-Aktifkan";
                $delivery_icon = "icon icon-remove";
                $allow_delivery = "<span style='color:green'>Bisa</span>&nbsp&nbsp<a onclick='set_aktif(".$row->id_pemilik.", " . (intval($row->allow_delivery) == 1 ? "0" : "1") . ")'><i rel='tooltip' title=' " . $delivery_text . "' style='cursor: pointer' class='" . $delivery_icon . "'></i></a>";
            } else {        
                $allow_delivery = "<span style='color:gray'>Tidak Bisa</span>&nbsp&nbsp<a onclick='allow_delivery(".$row->id_pemilik.", " . (intval($row->allow_delivery) == 1 ? "0" : "1") . ")'><i rel='tooltip' title=' " . $delivery_text . "' style='cursor: pointer' class='" . $delivery_icon . "'></i></a>";
            }

            if (intval($row->aktif) == 1) {
                $status = "<span style='color:green'>Aktif</span>";
                $aktif_text = "Non-Aktifkan";
                $aktif_icon = "icon icon-remove";
            } else {        
                $status = "<span style='color:red'>Non-Aktif</span>";
            }

            $nestedData[] = '<a onclick="buka_halaman(\'lap_data_pemiliku/upload_produk/' . $row->id_pemilik . '\', 4)"><i rel="tooltip" title="Upload" style="cursor: pointer" class="icon icon-upload"></i></a>';
            $nestedData[] = $allow_delivery;
            $nestedData[] = $status;
            $nestedData[] = "<a onclick='hapus_modal(".$row->id_pemilik.")''><i rel='tooltip' title='Hapus' style='cursor: pointer' class='icon icon-trash'></i></a>&nbsp&nbsp<a onclick='set_aktif(".$row->id_pemilik.", " . (intval($row->aktif) == 1 ? "0" : "1") . ")'><i rel='tooltip' title=' " . $aktif_text . "' style='cursor: pointer' class='" . $aktif_icon . "'></i></a>";
            $data[] = $nestedData;
            
            $index++;
        }

        $json_data = array(
            "pesan"           => $pesan,
            "draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
            "recordsTotal"    => intval( $totalData ),  // total number of records
            "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data"            => $data,   // total data array
            "json"            => $query->result()   // total data array
        );
        
        echo json_encode($json_data);  // send data as json format
    }
    
}