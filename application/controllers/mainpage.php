<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainpage extends CI_Controller {

    public function __construct(){
        parent::__construct();
        if ($this->session->userdata("id")!=NULL){
            $this->load->model("model_setting");
        }
        else {
            redirect(URL_LOGIN);
        }
    }

    public function index() {
        $data["konsumen"]= $this->model_setting->home_konsumen();
        $data["pusaha"]= $this->model_setting->home_pemilik();
        $data["order"]= $this->model_setting->home_order();
        $data["fee"]= $this->model_setting->home_fee();
        $data["total"]= $this->model_setting->get_total()->result();
        $id = $this->session->userdata("id");
        $data["pengguna"]=  $this->model_setting->nama_usr($id)->result();
        $data["role"]= $this->model_setting->get_role($id)->result();
        foreach ($this->model_setting->nama_usr($id)->result() as $row){
                $data["namaad"]=$row->nama;
        }
        $this->load->view('mainpage',$data);
    }
    
    public function ubah_pwd() {
        header("Content-type: application/json");
        $res = "";
        try {
            $new_pass = hash("sha512", $this->input->post("new_pass"));
            $rnew_pass = hash("sha512", $this->input->post("rnew_pass"));
            $old_pass = hash("sha512", $this->input->post("old_pass"));
            $id = $this->session->userdata("id");
            if($new_pass == $rnew_pass){
                $get_user = $this->model_setting->nama_usr($id);
                foreach ($get_user->result() as $row){
                    if($old_pass == $row->password){
                        $res="Password berhasil diganti";
                        $this->model_setting->u_pwd($rnew_pass,$id);
                    }  else {
                        $res="dua";
                    }
                }
            }  else {
                $res="satu";
            }
            
            $data = array("status"=>$res);
            echo json_encode($data);
            
        } catch (Exception $ex) {
            $res = "failed->".$ex;
            $data = array("status"=>$res);
            echo json_encode($data);
        }
    }
    
    public function sunting_profil() {
        header("Content-type: application/json");
        $res = "";
        $stat = "";
        try {
            $nama = $this->input->post("nama");
            $username = $this->input->post("username");
            $email = $this->input->post("email");
            $id = $this->session->userdata("id");
            
//            $status_username_email = $this->model_setting->cek_username_email($id,$username,$email);
            $usr_email = intval($this->model_setting->cek_usr_email($email,$id));
            $usr_username = intval($this->model_setting->cek_usr_username($username,$id));
            
            if($usr_username > 0){
                $res = "Username sudah terdaftar";
                $stat ="1";
            }
            elseif($usr_email > 0){
                $res = "Email sudah terdaftar";
                $stat ="2";
            }else{
                $stat ="3";
                $this->model_setting->sunting_prof($id,$nama,$username,$email);
                $res = "Profil berhasil diubah";
            }
            foreach ($this->model_setting->nama_usr($id)->result() as $row){
                $data = array("status"=>$res,"stat"=>$stat,"namaad"=>$row->nama,"list"=>$this->model_setting->nama_usr($id)->result());
                echo json_encode($data);
            }
            
        } catch (Exception $ex) {
            $res = "failed->".$ex;
            $data = array("status"=>$res);
            echo json_encode($data);
        }
    }



    
    public function setWebhook()
    {
        $this->Telegram->RequestWebhook("setWebhook");
    }
    
    public function getHook()
    {
        if($this->telegram->text_command("start")){
            log_message('error', "Found command and replay");
            // 1. check TELEID inside DB
            // 2. IF non REPLAY MARK_DOWN share CONTACT
            $this->telegram->send
                ->keyboard()
                    ->row()
                        ->button("Share Contact", "contact")
                        ->end_row()
                ->show(true, true)
                ->text("Silahkan share contact Anda!", "Markdown")
                ->send();
        }
        

        if($this->telegram->text_command("invoiceText")){
            $sparateTop = str_pad("", 50, "-", STR_PAD_BOTH);
            $sparate = str_pad("", 28, "-", STR_PAD_BOTH);
            
            $msg = "<i>OK SIP Minggu, 28 Agustus 2018 / 06:41</i>\n";
            $msg .= "#Inv: 426CJK0\n";
            $msg .= "\n";
            $msg .= "<b>Toko :</b>\n";
            $msg .= "Sodaqo Birrul Smart\n";
            $msg .= "Jl. Raya Condet, No 32 Cililitan\n";
            $msg .= "Kramat Jati - Jaktim\n";
            $msg .= "087888783819\n";
            $msg .= "\n";
            $msg .= "<b>Tujuan : </b>\n";
            $msg .= "Lokasi Sekarang\n";
            $msg .= "\n";
            $msg .= "<b>Penerima : </b>\n";
            $msg .= "Imam Mahmudi\n";
            $msg .= "\n";
            $msg .= "<b>Alamat : </b>\n";
            $msg .= "Jl Cendan II no 24 Rt 03/11\n";
            $msg .= "Utan Kayu Selatan, Jaktim\n";
            $msg .= "087888783819\n";
            $msg .= "\n";
            $msg .= "<b>Keterangan : </b>\n";
            $msg .= "-\n";
            $msg .= "\n";
            $msg .= "\n";
            $msg .= $sparateTop."\n";
            $msg .= "1. Soklin 2KG\n";
            $msg .= "   10 x 8.000".str_repeat(".",(31-strlen("   10 x 8.000")))."80.000\n";
            //$msg .= $sparate."\n";
            $msg .= "2. Aqua Galon \n";
            $msg .= "   2 x 18.000".str_repeat(".",(31-strlen("   2 x 18.000")))."36.000\n";
            $msg .= $sparateTop."\n";
            $msg .= "ADMIN APLIKASI".str_repeat(" ",(31-strlen("ADMIN APLIKASI")))."0.0\n";
            $msg .= "ONGKOS KIRIM".str_repeat(" ",(28-strlen("ONGKOS KIRIM")))."2.000\n";
            $msg .= "<b>TOTAL</b>".str_repeat(" ",(34-strlen("TOTAL")))."<b>65.000</b>\n";
            $msg .= "\n";
            $msg .= "\n";
            $msg .= "Terima kasih sudah belanja di OPA";
            $msg .= "\n";
            $this->telegram->send
                ->text($msg, "HTML")
                ->send();

            $this->telegram->send
                ->location("-6.204620","106.866830")
                ->send();
        }


        if($this->telegram->contact()) // if detect shared contact
        {
            $acontact = $this->telegram->data('contact');
            log_message('error', "Found Contact phone number " . $acontact->phone_number);
            $this->telegram->send
                ->notification(FALSE)
                ->keyboard()->selective(FALSE)->hide()
                ->text("Thank you.")
            ->send();
        }
        

        if($this->telegram->text_command("inlineButton"))
        {
            $this->telegram->send
                ->inline_keyboard()
                    ->row()
                        ->button("Set Terkirim", "XXSSXX","updateTerkirim")
                        ->end_row()
                ->show()
                ->text("TEST...", "Markdown")
                ->send();
        }

        if($this->telegram->answer_if_callback("Siap Akan di update", FALSE)){
            log_message('error', "Anser CallBack msg id " .$this->telegram->getMessageid());
            $this->telegram->send
                ->delete($this->telegram->getMessageid())
                ->send();
        }
    }


    /*
    JSON Format:
    {
        'order_id':'',
        'invoice_no':'',
        'order_date':'',
        'store':{'address':'', 'telp':'', 'name':''},
        'receiver':{'name':'', 'dest_address':'', 'address':'', 'telp':'', 'note':''},
        'items':[
            {'product_id':'', 'item_desc':'', 'qty':1, 'price':'1.000', 'total':'1.000'},
            {'product_id':'', 'item_desc':'', 'qty':2, 'price':'5.000', 'total':'10.000'}
        ],
        'summary':[
            {'name':'ADMIN APLIKASI', 'fee':'0'},
            {'name':'ONGKIR', 'fee':'2.000'},
            {'name':'TOTAL', 'fee':'13.000'}
        ],
        'footer':'Termikasih sudah belanja dengan OPA'
    }
    */
    public function sendKuber()
    {

    }
}
