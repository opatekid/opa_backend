<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk_promo extends CI_Controller {
	
    public function __construct(){
        parent::__construct();
        $this->load->model("mobile_v2/model_api_pemilik", "modelmu");
        $this->load->model("model_laporan");
        $this->load->model("model_transaksi");
        $this->load->model("model_master");
    }

    public function index() {
        $data["level"]=  $this->model_merchant_promo->get_produk_promo()->result();
        $data["tran_upgrade"]=  array();
        $this->load->view('produk_promo',$data);   
    }
        

    public function get_preactive_pemilik() {
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data'] = $this->model_master->get_preactive_pemilik()->result();

        echo json_encode($data);
    } 

    public function add() {
        $this->load->view('produk_promo_add',null);
    }

    public function edit($id_pemilik) {
        $pemiliks = $this->modelmu->get_pemilik($id_pemilik);
        $produks = $this->modelmu->get_pemilik_produk($id_pemilik);

        if (count($pemiliks) > 0) {
            $data["pemilik"] = $pemiliks[0];
            $data["produk"] = $produks;
        }
        // echo json_encode($data); die();

        $this->load->view('produk_promo_edit',$data);
    }


    public function create() {
        $id_pemilik = $this->modelmu->get_random_num();
        $id_partner = $this->session->userdata('id');
        $nama = $this->input->post("nama");
        $jargon = $this->input->post("jargon");
        $kontak = $this->input->post("kontak");
        $kontak_sms = $this->input->post("kontak_sms");
        $password = $this->input->post("password");
        $alamat = $this->input->post("alamat");
        $id_jenis = isset($_POST["id_jenis"]) ? $this->input->post("id_jenis") : '0';
        $id_kategori = isset($_POST["id_kategori"]) ? $this->input->post("id_kategori") : '0';
        $id_radius = $this->input->post("id_radius");
        $min_antar = $this->input->post("min_antar");
        $lat = $this->input->post("lat");
        $lng = $this->input->post("lng");
        $id_provinsi = $this->input->post("id_provinsi");
        $id_kota = $this->input->post("id_kota");

        $id_produk   = date("ymdHis") . $id_pemilik . rand(100, 1000);
        $nama_produk = $this->input->post("nama_produk");
        $id_kategori_produk = $this->input->post("id_kategori_produk");
        $stok = $this->input->post("stok");
        $harga = $this->input->post("harga");
        $keterangan = "";

        $username   = $this->input->post("username");
        $no_telp = $kontak;
        $nama_lokasi = "";
        $kode_sms = "11";
        $foto       = $_FILES["form_file"]['name'];
        
        $errors = [];

        if ($foto != "") {
            // echo "foto ada " . $foto;
            $foto2 = explode(".", $foto);
            $foto3 = $id_pemilik . "." . $foto2[count($foto2) - 1];
        } 
        
        if (isset($_FILES['form_file']) && !empty($_FILES['form_file'])) {
                if ($_FILES['form_file']['error'] != 4) {
                    $exp                     = explode(".", $_FILES["form_file"]['name']);
                    $ext                     = end($exp);
                    $config['upload_path']   = PATH_IMAGE_PEMILIK_WEBADMIN;
                    $config['allowed_types'] = '*';
                    $config['file_name']     = $foto3;
                    if (!is_dir(PATH_IMAGE_PEMILIK_WEBADMIN)) {
                        mkdir(PATH_IMAGE_PEMILIK_WEBADMIN, 0777, true);
                    }
                    
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    
                    if (!$this->upload->do_upload('form_file')) {
                        $errors[] = $this->upload->display_errors();
                    }
                }
            }

        $pemilik = $this->modelmu->get_id_pemilik_by_no_telp_and_username($no_telp, $username);

        if ($pemilik == null) {
            $id_pemilik_return = $this->modelmu->tambah_toko($id_pemilik, $id_partner, $no_telp, $kontak_sms, $password, $username, $kode_sms, $id_provinsi, $id_kota, $id_radius, $min_antar);
            if ($id_pemilik_return > 0) {

                $arr = array();
                $item = new stdClass();
                $item->id_kategori = $id_kategori;
                $item->id_jenis = 1;
                $arr[] = $item;

                $json = json_encode($arr);
            

                $this->modelmu->isi_profil_awal($id_pemilik, $nama, $alamat, $jargon, $id_radius, $kontak_sms);
                $this->modelmu->update_kategori_usaha_awal($id_pemilik, $json);
                $this->modelmu->isi_lokasi_awal($id_pemilik,$lat,$lng,$alamat);
                $this->modelmu->set_foto($id_pemilik, $foto3);

                 $data = array("status" => "berhasil ditambahkan", "code" => 1, "data" => $id_pemilik_return);
            } else {
                 $data = array("status" => "gagal ditambahkan", "code" => 0, "data" => null);
            }
        } else {
            $data = array("status" => "Nomor HP atau Alias sudah dipakai", "code" => 0, "data" => null);
        }

        echo json_encode($data);
    }

    public function hapus_akun()
    {

        $id_pemilik = $this->input->post("id_pemilik");
        
        $this->modelmu->hapus_akun($id_pemilik);
        $this->modelmu->delete_produk_by_pemilik($id_pemilik);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data'] = $this->model_master->get_preactive_pemilik()->result();
        
        echo json_encode($data);
    }


    public function upload($path, $nama_file, $id_file)
    {
        try {
            $config['upload_path']   = $path;
            $config['file_name']     = $nama_file;
            $config['allowed_types'] = '*';
            $config['overwrite']     = TRUE;
            $config['overwrite']     = TRUE;
            
            file_put_contents($path, base64_decode($nama_file));
            
            // $this->load->library('upload', $config);
            // if ( ! $this->upload->do_upload($id_file))
            // {
            //     $error = array('error' => $this->upload->display_errors());
            
            // }   
            // else
            // {
            //     $upload_data = $this->upload->data();
            // }
            return "";
        }
        catch (Exception $exc) {
            return $exc->getTraceAsString();
        }
    }

    public function update($id_pemilik) {
        $nama = $this->input->post("nama");
        $jargon = $this->input->post("jargon");
        $alamat = $this->input->post("alamat");
        $id_jenis = $this->input->post("id_jenis");
        $id_jenis = isset($_POST["id_jenis"]) ? $this->input->post("id_jenis") : '0';
        $id_kategori = isset($_POST["id_kategori"]) ? $this->input->post("id_kategori") : '0';
        $kontak = $this->input->post("kontak");
        $kontak_sms = $this->input->post("kontak_sms");
        $password = $this->input->post("password");
        $id_radius = $this->input->post("id_radius");
        $min_antar = $this->input->post("min_antar");
        $lat = $this->input->post("lat");
        $lng = $this->input->post("lng");
        $id_provinsi = $this->input->post("id_provinsi");
        $id_kota = $this->input->post("id_kota");

        $id_produk   = date("ymdHis") . $id_pemilik . rand(100, 1000);
        $nama_produk = $this->input->post("nama_produk");
        $id_kategori_produk = $this->input->post("id_kategori_produk");
        $stok = $this->input->post("stok");
        $harga = $this->input->post("harga");
        $keterangan = "";

        $username   = $this->input->post("username");
        $no_telp = $kontak;
        $nama_lokasi = "";
        $kode_sms = "11";

        // echo $kontak . " .... " . $password;
        
        $errors = [];


        if ($id_pemilik > 0) {

            $arr = array();
            $item = new stdClass();
            $item->id_kategori = $id_kategori;
            $item->id_jenis = 1;
            $arr[] = $item;

            $json = json_encode($arr);

            $this->modelmu->update_username($id_pemilik,$username);
            $this->modelmu->update_nama_toko($id_pemilik,$nama);
            $this->modelmu->update_no_telp_password_alamat_toko($id_pemilik,$no_telp, $kontak_sms, $password, $alamat);
            $this->modelmu->update_jargon_toko($id_pemilik,$jargon);
            $this->modelmu->update_radius_toko($id_pemilik,$id_radius, $min_antar);
            $this->modelmu->update_jenis($id_pemilik,$id_jenis);

            $this->modelmu->update_kategori_usaha_awal($id_pemilik, $json);
            $this->modelmu->isi_lokasi_awal($id_pemilik,$lat,$lng,$alamat);

             $data = array("status" => "berhasil diubah", "code" => 1, "data" => $id_pemilik);
        } else {
             $data = array("status" => "ada yang gagal diubah", "code" => 0, "data" => null);
        }

        echo json_encode($data);
    }

    
    public function get_data(){

        $data = array();
        $pesan = "";
        
        $id_partner = $this->session->userdata("id");
        $requestData        = $this->input->post();
        $count = $this->model_master->get_data();
        $totalFiltered = $count;
        $totalData =  $count;  

        $query = $this->model_master->get_data_order($requestData, $id_partner);

        if( !empty($requestData['search']['value']) ) {
            $query = $this->model_master->get_data_filter($requestData, $id_partner);
            $totalFiltered = $query->num_rows();
            $pesan = "pencarian";
        }

        if ($id_partner != null && !empty($id_partner) && $id_partner != '') {


            
            $no = $requestData['start']+1;
            
            $index = 0;
            $tipe = "";
            foreach ($query->result() as $row){
                if ($row->tipe == '1') {
                    $tipe = 'Tradisional';
                }elseif ($row->tipe == '2') {
                    $tipe = 'Minimarket';
                }
                $nestedData=array(); 
                $nestedData[] = $no++;
                $nestedData[] = $row->id_pemilik;
                $nestedData[] = $row->username;
                $nestedData[] = $row->nama;
                $nestedData[] = '<a onclick="buka_halaman(\'lap_penjualan_partner/index/' . $row->id_pemilik . '\', 1)"><i rel="tooltip" title="View Penjualan" style="cursor: pointer" class="icon icon-file"></i></a>';
                $nestedData[] = $row->radius . " m";
                $nestedData[] = $row->kontak . "/".$row->kontak_sms;
                $nestedData[] = '<a onclick="buka_halaman(\'merchant_beranda/index/' . $row->id_pemilik . '\', 1)"><i rel="tooltip" title="Upload Database Produk" style="cursor: pointer" class="icon icon-upload"></i></a>';
                $nestedData[] = '<a onclick="buka_halaman(\'produk_promo/edit/' . $row->id_pemilik . '\', 1)"><i rel="tooltip" title="Ubah" style="cursor: pointer" class="icon icon-pencil"></i></a> &nbsp ' . '<a onclick="hapus_modal(' . $row->id_pemilik . ')"><i rel="tooltip" title="Hapus" style="cursor: pointer" class="icon icon-trash"></i></a>' . ' &nbsp ';
                $data[] = $nestedData;
                
                $index++;
            }
        }  else {
            $pesan = "session expired";
        }

        $json_data = array(
                "pesan"           => $pesan,
                "draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
                "recordsTotal"    => intval( $totalData ),  // total number of records
                "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data"            => $data,   // total data array
                "json"            => $query->result()   // total data array
            );
        
        echo json_encode($json_data);  // send data as json format
    }
    
}