<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Preactive_store extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model("mobile_v2/model_api_pemilik", "modelmu");
        $this->load->model("model_laporan");
        $this->load->model("model_transaksi");
        $this->load->model("model_master");
        $this->load->helper('string');
    }



    public function index() {
        $data['pemilik'] = $this->model_master->get_preactive_pemilik()->result();
        $this->load->view('preactive_store_index',$data);
    } 

    public function get_preactive_pemilik() {
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data'] = $this->model_master->get_preactive_pemilik()->result();

        echo json_encode($data);
    } 

    public function add() {
        $this->load->view('preactive_store_add',null);
    }

    public function edit($id_pemilik) {
        $data["pemilik"] = $this->modelmu->get_preactive_pemilik($id_pemilik);
        $data["produk"] = $this->modelmu->get_preactive_produk($id_pemilik);

        // echo json_encode($data); die();

        $this->load->view('preactive_store_edit',$data);
    }


    public function create() {
        $id_pemilik = $this->modelmu->get_random_num();
    	$nama = $this->input->post("nama");
    	$jargon = $this->input->post("jargon");
    	$alamat = $this->input->post("alamat");
        $id_jenis = $this->input->post("id_jenis");
    	$id_kategori = $this->input->post("id_kategori");
    	$id_radius = $this->input->post("id_radius");
    	$lat = $this->input->post("lat");
    	$lng = $this->input->post("lng");
        $id_provinsi = $this->input->post("id_provinsi");
        $id_kota = $this->input->post("id_kota");

        $id_produk   = date("ymdHis") . $id_pemilik . rand(100, 1000);
    	$nama_produk = $this->input->post("nama_produk");
    	$id_kategori_produk = $this->input->post("id_kategori_produk");
    	$stok = $this->input->post("stok");
    	$harga = $this->input->post("harga");
        $keterangan = "";

        $username   = "User Baru";
        $no_telp = "";
        $password = "";
        $nama_lokasi = "";
        $kode_sms = "11";
        $foto       = $_FILES["form_file"]['name'];
        
        $errors = [];

        if ($foto != "") {
            // echo "foto ada " . $foto;
            $foto2 = explode(".", $foto);
            $foto3 = $id_produk . "." . $foto2[count($foto2) - 1];
        } 
        
        if (isset($_FILES['form_file']) && !empty($_FILES['form_file'])) {
                if ($_FILES['form_file']['error'] != 4) {
                    $exp                     = explode(".", $_FILES["form_file"]['name']);
                    $ext                     = end($exp);
                    $config['upload_path']   = PATH_IMAGE_PEMILIK;
                    $config['allowed_types'] = '*';
                    $config['file_name']     = $foto3;
                    if (!is_dir(PATH_IMAGE_PEMILIK)) {
                        mkdir(PATH_IMAGE_PEMILIK, 0777, true);
                    }
                    
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    
                    if (!$this->upload->do_upload('form_file')) {
                        $errors[] = $this->upload->display_errors();
                    }
                }
            }


        $foto_produk       = $_FILES["form_file_produk"]['name'];
        

        if ($foto_produk != "") {
            // echo "foto ada " . $foto;
            $foto2_produk = explode(".", $foto_produk);
            $foto3_produk = $id_produk . "." . $foto2[count($foto2_produk) - 1];
        } 
        
        if (isset($_FILES['form_file_produk']) && !empty($_FILES['form_file_produk'])) {
                if ($_FILES['form_file_produk']['error'] != 4) {
                    $exp_produk                     = explode(".", $_FILES["form_file_produk"]['name']);
                    $ext_produk                     = end($exp_produk);
                    $config['upload_path']   = PATH_IMAGE_PRODUK;
                    $config['allowed_types'] = '*';
                    $config['file_name']     = $foto3_produk;
                    if (!is_dir(PATH_IMAGE_PRODUK)) {
                        mkdir(PATH_IMAGE_PRODUK, 0777, true);
                    }
                    
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    
                    if (!$this->upload->do_upload('form_file_produk')) {
                        $errors[] = $this->upload->display_errors();
                    }
                }
            }

        $id_pemilik_return = $this->modelmu->daftar_pre($id_pemilik, $no_telp, $password, $username, $kode_sms, $id_provinsi, $id_kota, $id_radius);
        if ($id_pemilik_return > 0) {

            $arr = array();
            $item = new stdClass();
            $item->id_kategori = $id_kategori;
            $item->id_jenis = $id_jenis;
            $arr[] = $item;

            $json = json_encode($arr);
        

            $this->modelmu->isi_profil_awal($id_pemilik, $nama, $alamat, $jargon, $id_radius);
            $this->modelmu->update_kategori_usaha_awal($id_pemilik, $json);
            $this->modelmu->isi_lokasi_awal($id_pemilik,$lat,$lng,$alamat);
            $this->modelmu->set_foto($id_pemilik, $foto3);

            $this->modelmu->insert_produk($id_produk,$id_pemilik,$nama_produk,$harga,$stok,$keterangan,$id_kategori_produk);
            $this->modelmu->set_foto_produk($id_produk, $foto3_produk);

		     $data = array("status" => "berhasil ditambahkan", "code" => 1, "data" => $id_pemilik_return);
		} else {
		     $data = array("status" => "gagal ditambahkan", "code" => 0, "data" => null);
		}

        echo json_encode($data);
    }


    public function get_data(){
        
        $requestData        = $this->input->post();
        $pesan = "";
        
        $count = $this->model_transaksi->get_data();
        $totalFiltered = $count;
        $totalData =  $count;

        if( !empty($requestData['search']['value']) ) {
            $query = $this->model_transaksi->get_data_filter($requestData);
            $totalFiltered = $query->num_rows();
            $pesan = "pencarian";
        }

        $query = $this->model_transaksi->get_data_order($requestData);

        $data = array();
        
        $no = $requestData['start']+1;
        
        $index = 0;
        $tipe = "";
        foreach ($query->result() as $row){
            if ($row->tipe == '1') {
                $tipe = 'Free';
            }elseif ($row->tipe == '2') {
                $tipe = 'Premium';
            }
            $nestedData=array(); 
            $nestedData[] = $no++;
            $nestedData[] = $row->nama;
            $nestedData[] = $row->nama_level;
            $nestedData[] = $tipe;
            $nestedData[] = $row->nama_jenis;
            $nestedData[] = '<a onclick="edit_modal('.$index.')"><i rel="tooltip" title="Ubah" style="cursor: pointer" class="icon icon-pencil"></i></a>';
            $data[] = $nestedData;
            
            $index++;
        }

        $json_data = array(
            "pesan"           => $pesan,
            "draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
            "recordsTotal"    => intval( $totalData ),  // total number of records
            "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data"            => $data,   // total data array
            "json"            => $query->result()   // total data array
        );
        
        echo json_encode($json_data);  // send data as json format
    }

    public function hapus_akun()
    {

        $id_pemilik = $this->input->post("id_pemilik");
        
        $this->modelmu->hapus_akun($id_pemilik);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data'] = $this->model_master->get_preactive_pemilik()->result();
        
        echo json_encode($data);
    }


    public function upload($path, $nama_file, $id_file)
    {
        try {
            $config['upload_path']   = $path;
            $config['file_name']     = $nama_file;
            $config['allowed_types'] = '*';
            $config['overwrite']     = TRUE;
            $config['overwrite']     = TRUE;
            
            file_put_contents($path, base64_decode($nama_file));
            
            // $this->load->library('upload', $config);
            // if ( ! $this->upload->do_upload($id_file))
            // {
            //     $error = array('error' => $this->upload->display_errors());
            
            // }   
            // else
            // {
            //     $upload_data = $this->upload->data();
            // }
            return "";
        }
        catch (Exception $exc) {
            return $exc->getTraceAsString();
        }
    }

    public function update($id_pemilik) {
        $nama = $this->input->post("nama");
        $jargon = $this->input->post("jargon");
        $alamat = $this->input->post("alamat");
        $id_jenis = $this->input->post("id_jenis");
        $id_kategori = $this->input->post("id_kategori");
        $id_radius = $this->input->post("id_radius");
        $lat = $this->input->post("lat");
        $lng = $this->input->post("lng");
        $id_provinsi = $this->input->post("id_provinsi");
        $id_kota = $this->input->post("id_kota");

        $id_produk   = date("ymdHis") . $id_pemilik . rand(100, 1000);
        $nama_produk = $this->input->post("nama_produk");
        $id_kategori_produk = $this->input->post("id_kategori_produk");
        $stok = $this->input->post("stok");
        $harga = $this->input->post("harga");
        $keterangan = "";

        $username   = "User Baru";
        $no_telp = "";
        $password = "";
        $nama_lokasi = "";
        $kode_sms = "11";
        
        $errors = [];


        if ($id_pemilik > 0) {

            $arr = array();
            $item = new stdClass();
            $item->id_kategori = $id_kategori;
            $item->id_jenis = $id_jenis;
            $arr[] = $item;

            $json = json_encode($arr);

            $this->modelmu->update_nama_toko($id_pemilik,$nama);
            $this->modelmu->update_jargon_toko($id_pemilik,$jargon);
            $this->modelmu->update_radius_toko($id_pemilik,$id_radius);
            $this->modelmu->update_jenis($id_pemilik,$id_jenis);

            $this->modelmu->update_kategori_usaha_awal($id_pemilik, $json);
            $this->modelmu->isi_lokasi_awal($id_pemilik,$lat,$lng,$alamat);

            $this->modelmu->insert_produk($id_produk,$id_pemilik,$nama_produk,$harga,$stok,$keterangan,$id_kategori_produk);

             $data = array("status" => "berhasil diubah", "code" => 1, "data" => $id_pemilik);
        } else {
             $data = array("status" => "ada yang gagal diubah", "code" => 0, "data" => null);
        }

        echo json_encode($data);
    }
    

}