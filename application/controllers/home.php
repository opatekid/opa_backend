<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct(){
        parent::__construct();
        if ($this->session->userdata("id")!=NULL){
            $this->load->model("model_setting");
        }
        else {
            // redirect(URL_LOGIN);
        redirect('https://opaindonesia.com/index.html', 'refresh');
        }
    }

    public function index() {
        redirect('https://opaindonesia.com/index.html', 'refresh');
    }
    
    public function ubah_pwd() {
        header("Content-type: application/json");
        $res = "";
        try {
            $new_pass = hash("sha512", $this->input->post("new_pass"));
            $rnew_pass = hash("sha512", $this->input->post("rnew_pass"));
            $old_pass = hash("sha512", $this->input->post("old_pass"));
            $id = $this->session->userdata("id");
            if($new_pass == $rnew_pass){
                $get_user = $this->model_setting->nama_usr($id);
                foreach ($get_user->result() as $row){
                    if($old_pass == $row->password){
                        $res="Password berhasil diganti";
                        $this->model_setting->u_pwd($rnew_pass,$id);
                    }  else {
                        $res="dua";
                    }
                }
            }  else {
                $res="satu";
            }
            
            $data = array("status"=>$res);
            echo json_encode($data);
            
        } catch (Exception $ex) {
            $res = "failed->".$ex;
            $data = array("status"=>$res);
            echo json_encode($data);
        }
    }
    
    public function sunting_profil() {
        header("Content-type: application/json");
        $res = "";
        $stat = "";
        try {
            $nama = $this->input->post("nama");
            $username = $this->input->post("username");
            $email = $this->input->post("email");
            $id = $this->session->userdata("id");
            
//            $status_username_email = $this->model_setting->cek_username_email($id,$username,$email);
            $usr_email = intval($this->model_setting->cek_usr_email($email,$id));
            $usr_username = intval($this->model_setting->cek_usr_username($username,$id));
            
            if($usr_username > 0){
                $res = "Username sudah terdaftar";
                $stat ="1";
            }
            elseif($usr_email > 0){
                $res = "Email sudah terdaftar";
                $stat ="2";
            }else{
                $stat ="3";
                $this->model_setting->sunting_prof($id,$nama,$username,$email);
                $res = "Profil berhasil diubah";
            }
            foreach ($this->model_setting->nama_usr($id)->result() as $row){
                $data = array("status"=>$res,"stat"=>$stat,"namaad"=>$row->nama,"list"=>$this->model_setting->nama_usr($id)->result());
                echo json_encode($data);
            }
            
        } catch (Exception $ex) {
            $res = "failed->".$ex;
            $data = array("status"=>$res);
            echo json_encode($data);
        }
    }
}
