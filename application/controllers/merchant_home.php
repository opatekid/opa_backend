<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Merchant_home extends CI_Controller {

    public function __construct(){
        parent::__construct();
        if ($this->session->userdata("id")!=NULL){
            $this->load->model("model_partner");
            $this->load->model("mobile_v2/model_api_pemilik", "modelmu");
        }
        else {
            redirect(URL_MERCHANT_LOGIN);
        }
    }

    public function index() {
        $id = $this->session->userdata("id");
        // echo $id; die();
        $row = $this->model_partner->get($id)->row();
        $data["namaad"] = $row->nama;
        $data["pengguna"] = $this->model_partner->get($id)->row();
        $selected_kategories = array();
        foreach ($this->model_partner->get_pemilik_kategori($id) as $kategori) {
            $selected_kategories[] = $kategori->id_kategori;
        }
        $data["selected_kategories"] = $selected_kategories;
        $data["kategories"] = $this->modelmu->get_kategori_usaha_awal();
        $this->load->view('merchant_home',$data);
    }
    
    public function ubah_pwd() {
        header("Content-type: application/json");
        $res = "";
        try {
            $new_pass = hash("sha512", $this->input->post("new_pass"));
            $rnew_pass = hash("sha512", $this->input->post("rnew_pass"));
            $old_pass = hash("sha512", $this->input->post("old_pass"));
            $id = $this->session->userdata("id");
            if($new_pass == $rnew_pass){
                // echo "okok " . $id; die();
                $get_user = $this->model_partner->get($id);
                foreach ($get_user->result() as $row){
                    if($old_pass == $row->password){
                        $res="Password berhasil diganti";
                        $this->model_partner->u_pwd($rnew_pass,$id);
                    }  else {
                        $res="dua";
                    }
                }
            }  else {
                $res="satu";
            }
            
            $data = array("status"=>$res);
            echo json_encode($data);
            
        } catch (Exception $ex) {
            $res = "failed->".$ex;
            $data = array("status"=>$res);
            echo json_encode($data);
        }
    }
    
    public function sunting_profil() {
        header("Content-type: application/json");
        $res = "";
        $stat = "";
        try {
            $nama = $this->input->post("nama");
            $username = $this->input->post("username");
            $email = $this->input->post("email");
            $json = $this->input->post("id_kategories");
            $id = $this->session->userdata("id");
            
//            $status_username_email = $this->model_partner->cek_username_email($id,$username,$email);
            $usr_email = intval($this->model_partner->cek_usr_email($email,$id));
            $usr_username = intval($this->model_partner->cek_usr_username($username,$id));
            
            if($usr_username > 0) {
                $res = "Username sudah terdaftar";
                $stat ="1";
            }
            elseif($usr_email > 0) {
                $res = "Email sudah terdaftar";
                $stat ="2";
            }else{
                $stat ="3";
                $foto       = $_FILES["form_file"]['name'];
        
                $errors = [];

                if ($foto != "") {
                    // echo "foto ada " . $foto;
                    $foto2 = explode(".", $foto);
                    $foto3 = $id . "." . $foto2[count($foto2) - 1];
                } 
                
                if (isset($_FILES['form_file']) && !empty($_FILES['form_file'])) {
                        if ($_FILES['form_file']['error'] != 4) {
                            $exp                     = explode(".", $_FILES["form_file"]['name']);
                            $ext                     = end($exp);
                            if (is_file(PATH_IMAGE_PEMILIK . $id . ".".$ext)) {
                                unlink(PATH_IMAGE_PEMILIK . $id . ".".$ext);
                            }
                            $config['upload_path']   = PATH_IMAGE_PEMILIK;
                            $config['allowed_types'] = '*';
                            $config['file_name']     = $foto3;
                            if (!is_dir(PATH_IMAGE_PEMILIK)) {
                                mkdir(PATH_IMAGE_PEMILIK, 0777, true);
                            }
                            
                            $this->load->library('upload', $config);
                            $this->upload->initialize($config);
                            
                            if (!$this->upload->do_upload('form_file')) {
                                $errors[] = $this->upload->display_errors();
                            }
                        }
                    }
                $this->model_partner->sunting_prof($id,$nama,$username,$email);

                if ($foto != "") {
                    $this->model_partner->set_foto($id, $foto3);
                }
                
        // echo $json; die();
                $this->model_partner->update_kategori_usaha($id, $json);
                $res = "Profil berhasil diubah";
            }

            
            $data = array("status" => "");
            foreach ($this->model_partner->get($id)->result() as $row) {
                $data = array("error" => $errors, "status"=>$res,"stat"=>$stat,"namaad"=>$row->nama,"list"=>$this->model_partner->get_by_username($id)->result());
            }
            echo json_encode($data);
            
        } catch (Exception $ex) {
            $res = "failed->".$ex;
            $data = array("status"=>$res);
            echo json_encode($data);
        }
    }


    public function upload($path, $nama_file, $id_file)
    {
        try {
            $config['upload_path']   = $path;
            $config['file_name']     = $nama_file;
            $config['allowed_types'] = '*';
            $config['overwrite']     = TRUE;
            $config['overwrite']     = TRUE;
            
            file_put_contents($path, base64_decode($nama_file));
            
            // $this->load->library('upload', $config);
            // if ( ! $this->upload->do_upload($id_file))
            // {
            //     $error = array('error' => $this->upload->display_errors());
            
            // }   
            // else
            // {
            //     $upload_data = $this->upload->data();
            // }
            return "";
        }
        catch (Exception $exc) {
            return $exc->getTraceAsString();
        }
    }
}
