<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekap_pendapatan_provinsi extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model("model_rekapitulasi");
    }

    public function index() {
        $this->load->view('rekap_pendapatan_provinsi');
    }
    
    public function caritgl() {
        $tgl = $this->input->post("tanggal");
//        $tanggal = date($tgl, "Y-m-d H:i:s");
//        20-11-2016
//        [20,11,2016]
        $arr_tgl = explode("-", $tgl);
        $tanggal = $arr_tgl[2]."-".$arr_tgl[1]."-".$arr_tgl[0];
        $tglawal = substr($tanggal, 0, 4)."-01-01";
        $data = array("cari" => $this->model_rekapitulasi->caritgl_pendapatan_provinsi($tanggal,$tglawal)->result());
        echo json_encode($data);
    }
    
    public function caribln() {
        $bulan = $this->input->post("bulan");
        $tahun = $this->input->post("tahun");
        $data = array("cari" => $this->model_rekapitulasi->caribln_pendapatan_provinsi($bulan,$tahun)->result());
        echo json_encode($data);
    }
    public function carithn() {
        $tahun = $this->input->post("tahun");
        $data = array("cari"=>  $this->model_rekapitulasi->carithn_pendapatan_provinsi($tahun)->result());
        echo json_encode($data);
    }
}