<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->library("session");
        $this->load->model("model_setting");
    }
    
    public function index() {
        $this->load->view('login');
    }
    
    public function email() {
        $data["pesan"] = "test";
        $this->load->view('email', $data);
    }
    

    public function proses_login(){
        header("Content-type: application/json");
        $res = "";
        $code ="0";
        try {
            $this->load->helper('form');
            $username = $this->input->post('username');
            $password = hash("sha512", $this->input->post('password'));
            
            $get_user = $this->model_setting->get_user($username);

            if($get_user->num_rows() > 0){
                foreach ($get_user->result() as $row){
                    //lek password bener
                    if($password == $row->password){
                        $res = "Login berhasil, mohon tunggu...";
                        
                        $this->session->set_userdata('id',$row->id);
                        $this->session->set_userdata('level',$row->level);
                        $this->session->set_userdata('nama',$row->nama);
                        $code = "1";
                        
                    }
                    //lek password salah
                    else {
                        $res = "Password Salah";
                    }
                }
            }
            //lek username e salah
            else{
                $res = "Username Belum Terdaftar !! ";
            }

            $data = array("status"=>$res,"code"=>$code);
            echo json_encode($data);

        } catch (Exception $ex) {
            $res = "Login error";
            $data = array("status"=>$res,"code"=>$code);
            echo json_encode($data);
        }
    }
    
    public function logout(){
        $this->session->sess_destroy();
        redirect("login");
    }
    
    public function lupa_pas() {
        header("Content-type: application/json");
        $res = "";
        $stat = "";
        try {
            $email = $this->input->post("email");
            $kode = rand(1000, 10000);
            $get_user = $this->model_setting->get_email($email);
            if($get_user->num_rows() > 0){
                $this->session->set_userdata("kode",$kode."");
                $this->session->set_userdata("mail",$email."");
                $subjek         = "OPA-Kode";
                $pesan          = '<p>Kode Verifikasi '.$kode.'<br />';
                require_once 'ctr_email.php';
                $myemail = new Ctr_email();
                $myemail->send_email($email, $subjek, $pesan);
                $res = "Kode verifikasi telah dikirim, cek email anda";
                $stat = "1";
            }  else {
                $stat= "2";
                $res="Email tidak terdaftar";
            }
            
            $data = array("status"=>$res,"stat"=>$stat);
            echo json_encode($data);
            
        } catch (Exception $ex) {
            $res = "failed->".$ex;
            $data = array("status"=>$res);
            echo json_encode($data);
        }
    }
    
    public function lupa_pas_kode() {
        header("Content-type: application/json");
        $res = "";
        $stat ="";
        try {
//            $email = $this->input->post("email");
            $kode = rand(1000, 100000);
            $kodev = $this->input->post("kodepas");
            $seskode = $this->session->userdata("kode");
            $email = $this->session->userdata("mail");
//            $vkode = $this->input->post("vkode");
            if($kodev == $seskode){
                $n_pass = hash("sha512", $kode);
                $this->model_setting->update_lupa_pas($n_pass,$email);
                $subjek         = "OPA-New password";
                $pesan          = '<p>Silakan login dengan password : '.$kode.'</p>';
                require_once 'ctr_email.php';
                $myemail = new Ctr_email();
                $myemail->send_email($email, $subjek, $pesan);
                $this->session->sess_destroy();
                $res = "Password baru telah dikirim, cek email anda";
                $stat ="1";
            }  else {
                $stat ="2";
                $res = "Kode verifikasi salah";
            }
            
            $data = array("status"=>$res,"stat"=>$stat);
            echo json_encode($data);
            
        } catch (Exception $ex) {
            $res = "failed->".$ex;
            $data = array("status"=>$res);
            echo json_encode($data);
        }
    }
}