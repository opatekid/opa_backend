<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Update_produk extends CI_Controller {
	
    public function __construct(){
        parent::__construct();
        $this->load->model("mobile_v2/model_api_pemilik", "modelmu");
        $this->load->model("model_laporan");
        $this->load->model("model_transaksi");
        $this->load->model("v2/model_master");
    }

    public function index() {
        $data["level"] =  $this->model_master->get_level()->result();
        $data["tran_upgrade"]=  array();
        $this->load->view('update_produk',$data);   
    }
        

    public function produk_siap_antar($id_pemilik) {

        $pemiliks = $this->modelmu->get_pemilik($id_pemilik);

        if (count($pemiliks) > 0) {
            $data["pemilik"] = $pemiliks[0];
        }
        $this->load->view('produk_siap_antar',$data);   
    }
        

    public function get_preactive_pemilik() {
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data'] = $this->model_master->get_preactive_pemilik()->result();

        echo json_encode($data);
    } 

    public function add() {
        $this->load->view('update_produk_add',null);
    }

    public function edit($id_produk,$nama_produk) {
        // $strings = explode("_", $id_produk);
        // $id_produk = $strings[count($strings)-1];
        $produk = $this->modelmu->get_produk_by_barcode_id($id_produk, $nama_produk);

        $data["produk"] = $produk;
        // echo json_encode($data); die();

        $this->load->view('update_produk_edit',$data);
    }


    public function create() {
        $min_antar = "1";
        
        $id_partner = $this->session->userdata("id");
        $id_produk   = date("ymdHis") . rand(100, 1000);
        $nama        = $this->input->post("nama_produk");
        $harga       = $this->input->post("harga");
        $harga_promo       = null;
        $stok        = 0;
        $min_antar        = 0;
        $keterangan  = $this->input->post("keterangan");
        $id_kategori = $this->input->post("id_kategori_produk");
        $ket_promo = "";


        $pemiliks = $this->modelmu->get_pemilik_by_partner($id_partner)->result();
        $produk = $this->modelmu->get_produk_by_id($id_produk);

        foreach ($pemiliks as $pemilik) {
            $this->modelmu->insert_produk($pemilik->id_pemilik . "_" . $id_produk, $pemilik->id_pemilik, $nama, $harga, $harga_promo, $stok, $keterangan, $id_kategori, $min_antar, $ket_promo);
        }

        $this->modelmu->enable_kategori_produk($produk->id_kategori);
        $this->modelmu->enable_kategori_produk($id_kategori);
        

        $foto       = $_FILES["form_file"]['name'];
        $foto3 = "";
        
        $errors = [];
        $pesan = "Berhasil";
        $code = "1";

        if ($foto != "") {
            // echo "foto ada " . $foto;
            $foto2 = explode(".", $foto);
            $foto3 = $id_produk . "." . $foto2[count($foto2) - 1];
        } 
        
        if (isset($_FILES['form_file']) && !empty($_FILES['form_file'])) {
                if ($_FILES['form_file']['error'] != 4) {
                    $exp                     = explode(".", $_FILES["form_file"]['name']);
                    $ext                     = end($exp);
                    $config['upload_path']   = PATH_IMAGE_PRODUK;
                    $config['allowed_types'] = '*';
                    $config['file_name']     = $foto3;
                    if (!is_dir(PATH_IMAGE_PRODUK)) {
                        mkdir(PATH_IMAGE_PRODUK, 0777, true);
                    }
                    
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    
                    if (!$this->upload->do_upload('form_file')) {
                        $errors[] = $this->upload->display_errors();
                    }
                }

            }

        if (count($errors) > 0) {
            $pesan = "failed uploading photo";
            $code = 0;
        } else {
            foreach ($pemiliks as $pemilik) {
                $this->modelmu->set_foto_produk($pemilik->id_pemilik . "_" . $id_produk, $foto3);
            }
        }
            

        $data['code']  = $code;
        $data['pesan'] = $pesan;
        $data['data']  = array_reverse($this->modelmu->get_pemilik_produk($id_partner));
        $data['errors']  = $errors;

        echo json_encode($data);
        
    }

    
    public function delete_produk()
    {
        $id_partner = $this->session->userdata("id");   

        $id_produk  = $this->input->post("id_produk");
        $produk = $this->modelmu->get_produk_by_id($id_produk);
        $id_kategori = $produk->id_kategori;
        // $string_array = explode("_", $id_produk);
        // if (count($string_array) > 0) {
        //     $id_produk = $string_array[1];
        // }

        $this->modelmu->enable_kategori_produk($id_kategori);
        
        $pemiliks = $this->modelmu->get_pemilik_by_partner($id_partner)->result();

        foreach ($pemiliks as $pemilik) {
            $this->modelmu->copy_produk_to_histori($pemilik->id_pemilik . "_" . $id_produk);
            $this->modelmu->delete_produk($pemilik->id_pemilik . "_" . $id_produk);
        }
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->modelmu->get_pemilik_produk($id_pemilik);
        
        echo json_encode($data);
    }


    public function upload($path, $nama_file, $id_file)
    {
        try {
            $config['upload_path']   = $path;
            $config['file_name']     = $nama_file;
            $config['allowed_types'] = '*';
            $config['overwrite']     = TRUE;
            $config['overwrite']     = TRUE;
            
            file_put_contents($path, base64_decode($nama_file));
            
            // $this->load->library('upload', $config);
            // if ( ! $this->upload->do_upload($id_file))
            // {
            //     $error = array('error' => $this->upload->display_errors());
            
            // }   
            // else
            // {
            //     $upload_data = $this->upload->data();
            // }
            return "";
        }
        catch (Exception $exc) {
            return $exc->getTraceAsString();
        }
    }

    public function update($id_produk) {
        $min_antar = "1";

        // echo $id_produk; die();
        
        $string_array = explode("_", $id_produk);
        if (count($string_array) > 0) {
            $id_produk = $string_array[1];
        }

        $id_partner = $this->session->userdata("id");
        $nama        = $this->input->post("nama_produk");
        $harga       = $this->input->post("harga");
        $harga_promo       = null;
        $min_antar        = 0;
        $keterangan  = $this->input->post("keterangan");
        $id_kategori = $this->input->post("id_kategori_produk");
        $ket_promo = "";

        $pemiliks = $this->modelmu->get_pemilik_by_partner(ID_PARTNER)->result();
        $produk = $this->modelmu->get_produk_by_id($id_produk);

        $full_id_produk = "";
        foreach ($pemiliks as $pemilik) {
            // echo $pemilik->id_pemilik . "_" . $id_produk . "\n";
            // $full_id_produk = $pemilik->id_pemilik . "_" . $id_produk;
            $this->modelmu->update_produk_partner($pemilik->id_pemilik . "_" . $id_produk, $id_kategori, $nama, $harga, $harga_promo, $keterangan, $min_antar, $ket_promo, null);

        }


        $this->modelmu->enable_kategori_produk($produk->id_kategori);
        $this->modelmu->enable_kategori_produk($id_kategori);
        // die();
        $foto_produk = $this->modelmu->get_foto_produk($full_id_produk);
        

        $foto       = $_FILES["form_file"]['name'];
        $foto3 = "";
        $errors = [];

        if ($foto != "") {
            // echo "foto ada " . $foto;
            $foto2 = explode(".", $foto);
            $foto3 = $id_produk . "" . (intval($foto_produk->versi) + 1) . "." . $foto2[count($foto2) - 1];
        } 

        
        if (isset($_FILES['form_file']) && !empty($_FILES['form_file']) && $foto3 != "") {  
                if ($_FILES['form_file']['error'] != 4) {
                    $exp                     = explode(".", $_FILES["form_file"]['name']);
                    $ext                     = end($exp);
                    $config['upload_path']   = PATH_IMAGE_PRODUK_WEBADMIN;
                    $config['allowed_types'] = '*';
                    $config['file_name']     = $foto3;
                    if (!is_dir(PATH_IMAGE_PRODUK_WEBADMIN)) {
                        mkdir(PATH_IMAGE_PRODUK_WEBADMIN, 0777, true);
                    }

                    
                    $this->load->library('upload', $config);
                    // $this->upload->overwrite = true;
                    $this->upload->initialize($config);
                    
                    if (!$this->upload->do_upload('form_file')) {
                        // echo "ini";
                        $errors[] = $this->upload->display_errors();
                    } else {
                        $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
                        $foto3 = $upload_data['file_name'];
                        // echo "itu " . $foto3;
                    }
                }

                // echo "ini"; die();

                $data = array("status" => "Edit succeeded", "code" => 1, "data" => $id_produk);

                foreach ($pemiliks as $pemilik) {
                    $this->modelmu->set_foto_produk($pemilik->id_pemilik . "_" . $id_produk, $foto3);
                }

            if (count($errors) == 0) {
                $data = array("status" => "Edit succeeded", "code" => 1, "data" => $id_produk, "errors" => $errors);
            } else {
                $data = array("status" => "Failed uploading photo", "code" => 0, "data" => $id_produk, "errors" => $errors);
            }

        } else {
            $data = array("status" => "Edit succeeded", "code" => 1, "data" => $id_produk, "errors" => $errors);
        }

        echo json_encode($data);
    }

    
    public function get_data(){

        $data = array();
        $pesan = "";
        
        $id_partner = $this->session->userdata("id");

        $pemiliks = $this->modelmu->get_pemilik_by_partner($id_partner)->result();
        $pemilik = $pemiliks[0];

        $requestData        = $this->input->post();
        $count = $this->modelmu->get_partner_produk($pemilik->id_pemilik)->num_rows();
        $totalFiltered = $count;
        $totalData =  $count;  

        $query = $this->modelmu->get_data_produk($requestData);

        if( !empty($requestData['search']['value']) ) {
            // echo $requestData['search']['value']; die();
            $query = $this->modelmu->get_data_produk_filter($requestData);
            $totalFiltered = $query->num_rows();
            $pesan = "search";
        }

        if ($pemilik->id_pemilik != null && !empty($pemilik->id_pemilik) && $pemilik->id_pemilik != '') {
            
            $no = $requestData['start']+1;
            
            $index = 0;
            $tipe = "";
            foreach ($query->result() as $row){
                $nestedData=array(); 
                $nestedData[] = $no++;
                $nestedData[] = str_replace($pemilik->id_pemilik . "_" , "", $row->id_produk);
                $nestedData[] = $row->nama_produk;
                $nestedData[] = $row->nama_kategori;
                $nestedData[] = "Rp " . $row->harga;
                $nestedData[] = '<a onclick="buka_halaman(\'update_produk/edit/' . $row->id_produk . '/' . $row->nama_produk . '\', 5)"><i rel="tooltip" title="Ubah" style="cursor: pointer" class="icon icon-pencil"></i></a>';
                $data[] = $nestedData;
                
                $index++;
            }
        } else {
            $pesan = "no items";
        }

        $json_data = array(
                "pesan"           => $pesan,
                "draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
                "recordsTotal"    => intval( $totalData ),  // total number of records
                "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data"            => $data,   // total data array
                "json"            => $query->result()   // total data array
            );
        
        echo json_encode($json_data);  // send data as json format
    }
    
}