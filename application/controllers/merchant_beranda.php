<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Merchant_beranda extends CI_Controller {

    public function __construct(){
        parent::__construct();
        if ($this->session->userdata("id")!=NULL){
            $this->load->model("model_partner");
            $this->load->model("mobile_v2/model_api_pemilik", "modelmu");
        }
        else {
            redirect(URL_LOGIN);
        }
    }

    public function index() {
        $id = $this->session->userdata("id");
        // echo $id; die();
        $row = $this->model_partner->get($id)->row();
        $data["namaad"] = $row->nama;
        $data["pengguna"] = $this->model_partner->get($id)->row();
        
//        echo json_encode($data);
        $this->load->view('merchant_beranda',$data);   
    }

    public function banners() {
        $id_partner = $this->session->userdata('id');
        $data['banners'] = $this->model_partner->get_banners($id_partner)->result();
        $data['partner'] = $this->model_partner->get($id_partner)->row();;

        // echo json_encode($data); die();
        $this->load->view('merchant_banners',$data);
    }

    public function delete_banner() {
        $id = $this->input->post('id');
        $this->model_partner->delete_banner($id);
            
            $res = "Banner deleted successfully";
            
            $data = array("status"=>$res, "code" => 1);
            echo json_encode($data);
    }

    public function upload_banner() {
        $id_partner = $this->session->userdata('id');
        $last_promo = $this->model_partner->get_last_banner();
        // echo json_encode($last_promo);
        // $id = intval($last_promo != null ? $last_promo->id : "0") + 1;
        $foto       = $_FILES["form_file"]['name'];

        $errors = [];

        if ($foto != "") {
                    $id = $this->model_partner->add_banner($id_partner);
                    // echo "foto ada " . $foto;
                    $foto2 = explode(".", $foto);
                    $foto3 = $id . "." . $foto2[count($foto2) - 1];
                    $this->model_partner->set_banner($id, $foto3);
                } 
                
                if (isset($_FILES['form_file']) && !empty($_FILES['form_file'])) {
                        if ($_FILES['form_file']['error'] != 4) {
                            $exp                     = explode(".", $_FILES["form_file"]['name']);
                            $ext                     = end($exp);
                            if (is_file(PATH_IMAGE_PARTNER_WEBADMIN . $id . ".".$ext)) {
                                unlink(PATH_IMAGE_PARTNER_WEBADMIN . $id . ".".$ext);
                            }
                            $config['upload_path']   = PATH_IMAGE_PARTNER_WEBADMIN;
                            $config['allowed_types'] = '*';
                            $config['file_name']     = $foto3;
                            if (!is_dir(PATH_IMAGE_PARTNER_WEBADMIN)) {
                                mkdir(PATH_IMAGE_PARTNER_WEBADMIN, 0777, true);
                            }
                            
                            $this->load->library('upload', $config);
                            $this->upload->initialize($config);
                            
                            if (!$this->upload->do_upload('form_file')) {
                                $errors[] = $this->upload->display_errors();
                            }
                        }
                    }

                $res = "Banner uploaded successfully";
            $data = array("status"=>$res, "code" => 1);
            echo json_encode($data);


    }
    
}