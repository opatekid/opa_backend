<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promo_old extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model("mobile_v2/model_api_pemilik", "modelmu");
        $this->load->model("model_laporan");
        $this->load->model("model_transaksi");
        $this->load->model("model_promo_old");
        $this->load->model("model_master");
        $this->load->helper('string');
    }

    public function delete() {
        header("Content-type: application/json");
        $id = $this->input->post("id");
        $this->model_promo_old->delete($id);
        $list = $this->model_promo_old->list_active_promo()->result();
        $data = array("status"=>"berhasil dihapus", "code" => 1, "data" => $list);
        echo json_encode($data);
    }

    public function index() {
        $data['active_promo'] = $this->model_promo_old->list_active_promo()->result();
        $this->load->view('promo_index',$data);
    }

    public function detail() {
        $promo_id               = $this->input->get("id");
        $filter_select          = $this->input->get("filter_select");
        $queryAll = $this->model_promo_old->show_promo($promo_id);
        $query = $this->model_promo_old->show_promo_with_filter($promo_id, $filter_select);
        $data['promo_id'] = $promo_id;
        $data['active_code'] = $query->result();
        $data['nama_promo'] = $queryAll->row()->nama_promo;
        $this->load->view('promo_detail',$data);
    }    

    public function detail_for_konsumen() {
        $promo_id               = $this->input->get("id");
        $queryAll = $this->model_promo_old->show_promo_for_konsumen($promo_id);
        $query = $this->model_promo_old->show_promo_for_konsumen($promo_id);
        $data['promo_id'] = $promo_id;
        $data['active_code'] = $query->result();
        $data['nama_promo'] = $queryAll->row()->nama_promo;
        $this->load->view('promo_detail',$data);
    }    

    public function konsumen() {
        header("Content-type: application/json");
        $promo_code_id = $this->input->get("id");
        $query = $this->model_promo_old->list_promo_code_konsumen($promo_code_id);
        $data = array("status"=>$query, "code" => 1);
        $data['konsumen'] = $query->result();
        echo json_encode($data);
    }

    public function histori_transaksi() {
        header("Content-type: application/json");
        $data = array();
        $promo_code_id = $this->input->get("id");
        $query = $this->model_promo_old->histori_transaksi($promo_code_id)->result();
        $data = array("status"=>$query, "code" => 1);
        $data['histories'] = $query;
        echo json_encode($data);
    }


    public function add_pemilik() {
        $data["provinsi"] =  $this->model_laporan->get_provinsi()->result();
        $data["usaha"] =  $this->model_transaksi->get_upgrade()->result();
        $this->load->view('promo_add_pemilik',$data);
    } 

    public function add_konsumen() {
        $data['kategori'] = $this->modelmu->get_kategori_usaha_awal();
        $data["provinsi"] =  $this->model_laporan->get_provinsi()->result();
        $data["usaha"] =  $this->model_transaksi->get_upgrade()->result();
        $this->load->view('promo_add_konsumen',$data);
    } 
    
    public function set_produk_promo($id_promo) {
        $data['kategori'] = $this->modelmu->get_kategori_produk();
        $data['id_promo'] = $id_promo;
        $queryAll = $this->model_promo_old->get_promo_detail($id_promo);
        $data['promo_code'] = $queryAll->row();
        $this->load->view('promo_set_produk',$data);
    } 

    public function insert_produk($id_promo)
    {
        $id_produk   = date("ymdHis") . rand(100, 1000);
        $nama        = $this->input->post("nama");
        $harga       = $this->input->post("harga");
        $stok        = $this->input->post("stok");
        $keterangan  = "";
        $id_kategori = $this->input->post("id_kategori");
        $bitmap = $this->input->post("form_file");
        $foto       = $_FILES["form_file"]['name'];
        
        $errors = [];

        if ($foto != "") {
            // echo "foto ada " . $foto;
            $foto2 = explode(".", $foto);
            $foto3 = $id_produk . "." . $foto2[count($foto2) - 1];
        } 
        
        if (isset($_FILES['form_file']) && !empty($_FILES['form_file'])) {
                if ($_FILES['form_file']['error'] != 4) {
                    $exp                     = explode(".", $_FILES["form_file"]['name']);
                    $ext                     = end($exp);
                    $config['upload_path']   = PATH_IMAGE_PRODUK;
                    $config['allowed_types'] = '*';
                    $config['file_name']     = $foto3;
                    if (!is_dir(PATH_IMAGE_PRODUK)) {
                        mkdir(PATH_IMAGE_PRODUK, 0777, true);
                    }
                    
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    
                    if (!$this->upload->do_upload('form_file')) {
                        $errors[] = $this->upload->display_errors();
                    }
                }
            }
        $this->model_promo_old->insert_produk($id_produk, $nama, $harga, $stok, $keterangan, $id_kategori, $foto3);
        $this->model_promo_old->set_promo_produk($id_promo, $id_produk);
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->model_promo_old->get_produk_promo($id_promo);
        $data['errors'] = $errors;
        echo json_encode($data);
    }


    public function get_produk_promo($id_promo)
    {
        $promo = $this->model_promo_old->get_latest_promo()->row();
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->model_promo_old->get_produk_promo($id_promo);
        echo json_encode($data);
    }
    

    public function create() {
        header("Content-type: application/json");
        $res = "";
        try {
            $this->load->helper('form');
            $nama               = $this->input->post("nama");
            $promo_for          = $this->input->post("promo_for");
            $id_provinsi        = $this->input->post("id_provinsi");
            $id_kota            = $this->input->post("id_kota");
            $jenis_promo        = $this->input->post("jenis_promo");
            $target             = $this->input->post("target");
            $description        = $this->input->post("description");
            $valid_duration     = $this->input->post("valid_duration");
            $max_konsumen       = $this->input->post("max_konsumen");
            $message            = $this->input->post("message");
            // $code_count         = $this->input->post("code_count");

            if ($message == null) {
                $message = '';
            }

            $insert_id = $this->model_promo_old->create_promo($nama, $description, $promo_for, $id_provinsi, $id_kota, $jenis_promo, $message, $valid_duration, $max_konsumen, $target, 0);

            if ($target == 1) {
                $p_usahas = $this->model_master->get_all_pemilik_usaha()->result();
                $promo_codes = array();
                foreach ($p_usahas as $p_usaha) {
                    $promo_codes[] = random_string('numeric', 8);
                }
                // echo "count ".count($p_usahas);
                $this->model_promo_old->generate_promo_codes($insert_id, $p_usahas, $promo_codes);
            }
            
            $res = "Promo created successfully";
            
            $data = array("status"=>$res, "code" => 1, "list"=>$this->model_promo_old->list_active_promo()->result());
            echo json_encode($data);
            
        } catch (Exception $ex) {
            $res = "failed->".$ex;
            $data = array("status"=>$res);
            echo json_encode($data);
        }
    }

    public function create_konsumen() {
        header("Content-type: application/json");
        $res = "";
        try {
            $this->load->helper('form');
            $nama               = $this->input->post("nama");
            $promo_for          = $this->input->post("promo_for");
            $id_provinsi        = $this->input->post("id_provinsi");
            $id_kota            = $this->input->post("id_kota");
            $jenis_promo        = $this->input->post("jenis_promo");
            $target             = $this->input->post("target");
            $description        = $this->input->post("description");
            $valid_duration     = $this->input->post("valid_duration");
            $time_unit          = $this->input->post("time_unit");
            $max_purchase       = $this->input->post("max_purchase");
            $max_konsumen       = $this->input->post("max_konsumen");
            $id_kategori           = $this->input->post("id_kategori");
            $message            = $this->input->post("message");
            $fail_message       = $this->input->post("fail_message");
            // $code_count         = $this->input->post("code_count");

            if ($message == null) {
                $message = '';
            }

            $insert_id = $this->model_promo_old->create_promo_konsumen($nama, $description, $promo_for, $jenis_promo, $message, $fail_message, $valid_duration, $time_unit, $max_purchase, $max_konsumen, $target, 0);

            if ($target == 1) {
                // echo "count ".count($p_usahas);
                $promo_code = random_string('numeric', 8);
                $this->model_promo_old->generate_promo_code($insert_id, $promo_code);
                $this->model_promo_old->set_kategori_usaha_promo($insert_id, $id_kategori);
            }
            
            $res = "Promo created successfully";
            
            $data = array("status"=>$res, "code" => 1, "id_promo" => $insert_id, "list"=>$this->model_promo_old->list_active_promo()->result());
            echo json_encode($data);
            
        } catch (Exception $ex) {
            $res = "failed->".$ex;
            $data = array("status"=>$res);
            echo json_encode($data);
        }
    }

    
    public function get_data(){

        $requestData        = $this->input->post();
        $pesan = "";
        
        $count = $this->model_transaksi->get_data();
        $totalFiltered = $count;
        $totalData =  $count;

        if( !empty($requestData['search']['value']) ) {
            $query = $this->model_transaksi->get_data_filter($requestData);
            $totalFiltered = $query->num_rows();
            $pesan = "pencarian";
        }

        $query = $this->model_transaksi->get_data_order($requestData);

        $data = array();
        
        $no = $requestData['start']+1;
        
        $index = 0;
        $tipe = "";
        foreach ($query->result() as $row){
            if ($row->tipe == '1') {
                $tipe = 'Free';
            }elseif ($row->tipe == '2') {
                $tipe = 'Premium';
            }
            $nestedData=array(); 
            $nestedData[] = $no++;
            $nestedData[] = $row->nama;
            $nestedData[] = $row->nama_level;
            $nestedData[] = $tipe;
            $nestedData[] = $row->nama_jenis;
            $nestedData[] = '<a onclick="edit_modal('.$index.')"><i rel="tooltip" title="Ubah" style="cursor: pointer" class="icon icon-pencil"></i></a>';
            $data[] = $nestedData;
            
            $index++;
        }

        $json_data = array(
            "pesan"           => $pesan,
            "draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
            "recordsTotal"    => intval( $totalData ),  // total number of records
            "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data"            => $data,   // total data array
            "json"            => $query->result()   // total data array
            );
        
        echo json_encode($json_data);  // send data as json format
    }


    public function upload($path, $nama_file, $id_file)
    {
        try {
            $config['upload_path']   = $path;
            $config['file_name']     = $nama_file;
            $config['allowed_types'] = '*';
            $config['overwrite']     = TRUE;
            $config['overwrite']     = TRUE;
            
            file_put_contents($path, base64_decode($nama_file));
            
            // $this->load->library('upload', $config);
            // if ( ! $this->upload->do_upload($id_file))
            // {
            //     $error = array('error' => $this->upload->display_errors());
            
            // }   
            // else
            // {
            //     $upload_data = $this->upload->data();
            // }
            return "";
        }
        catch (Exception $exc) {
            return $exc->getTraceAsString();
        }
    }
}