<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mst_kategori_ushm extends CI_Controller {
	
    public function __construct(){
        parent::__construct();
        $this->load->model("model_master");
    }

    public function index() {
        $data["kategori"]=  $this->model_master->m_kategori()->result();
        $data["jenisk"]=  $this->model_master->get_jenisk()->result();
        $this->load->view('mst_kategori_ushm',$data);
        
    }
    
    public function insert_kategori() {
        header("Content-type: application/json");
        $res = "";
        try {
            $this->load->helper('form');
            $nama = $this->input->post("nama");
            $jenisk = $this->input->post("jenisk");
            $id   = $this->input->post("id");
			
            $this->model_master->insert_kategoriushm($nama,$jenisk,$id);
            
            if($id == ""){
                $res = "Insert data successfully";
            }else{
                $res = "Update data successfully";
            }
            
            $data = array("status"=>$res,"list"=>$this->model_master->m_kategori()->result());
            echo json_encode($data);
            
        } catch (Exception $ex) {
            $res = "failed->".$ex;
            $data = array("status"=>$res);
            echo json_encode($data);
        }
    }
    
    public function delete() {
        header("Content-type: application/json");
        $res = "";
        try {
            $this->load->helper('form');
            $id   = $this->input->post("id");
			
            $this->model_master->delete_kategoriushm($id);
            
            if($id == ""){
                $res = "failed data error";
            }else{
                $res = "Delete data successfully";
            }
            
            $data = array("status"=>$res,"list"=>$this->model_master->m_kategori()->result());
            echo json_encode($data);
            
        } catch (Exception $ex) {
            $res = "failed->".$ex;
            $data = array("status"=>$res);
            echo json_encode($data);
        }
    }
}