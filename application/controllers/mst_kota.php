<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mst_kota extends CI_Controller {
	
    public function __construct(){
        parent::__construct();
        $this->load->model("model_master");
    }

    public function index() {
        $data["kota"]=  $this->model_master->m_kota()->result();
        $data["provinsi"]=  $this->model_master->m_provinsi()->result();
        $this->load->view('mst_kota',$data);
        
    }
    
    public function insert_kota() {
        header("Content-type: application/json");
        $res = "";
        try {
            $this->load->helper('form');
            $nama = $this->input->post("nama");
            $provinsi = $this->input->post("provinsi");
            $id   = $this->input->post("id");
			
            $this->model_master->insert_kota($nama,$provinsi,$id);
            
            if($id == ""){
                $res = "Insert data successfully";
            }else{
                $res = "Update data successfully";
            }
            
            $data = array("status"=>$res,"list"=>$this->model_master->m_kota()->result());
            echo json_encode($data);
            
        } catch (Exception $ex) {
            $res = "failed->".$ex;
            $data = array("status"=>$res);
            echo json_encode($data);
        }
    }
    
    public function delete_kota() {
        header("Content-type: application/json");
        $res = "";
        try {
            $this->load->helper('form');
            $id   = $this->input->post("id");
			
            $this->model_master->delete_kota($id);
            
            if($id == ""){
                $res = "failed data error";
            }else{
                $res = "Delete data successfully";
            }
            
            $data = array("status"=>$res,"list"=>$this->model_master->m_kota()->result());
            echo json_encode($data);
            
        } catch (Exception $ex) {
            $res = "failed->".$ex;
            $data = array("status"=>$res);
            echo json_encode($data);
        }
    }
}