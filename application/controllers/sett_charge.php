<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sett_charge extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model("model_setting");
    }

    public function index() {
        $data["charge"]=  $this->model_setting->get_charge()->result();
        $this->load->view('sett_charge',$data);
    }
    
    public function update() {
        header("Content-type: application/json");
        $res = "";
        try {
            $this->load->helper('form');
            $id      = $this->input->post("id");
            $charge = $this->input->post("charge");
			
            $this->model_setting->update_charge($id,$charge);
            
            $res = "Update data successfully";
            
            $data = array("status"=>$res,"list"=>$this->model_setting->get_charge()->result());
            echo json_encode($data);
            
        } catch (Exception $ex) {
            $res = "failed->".$ex;
            $data = array("status"=>$res);
            echo json_encode($data);
        }
    }
}