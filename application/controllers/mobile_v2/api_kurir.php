<?php
//error_reporting(E_ALL);
//ini_set('display_errors','On');
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_kurir extends CI_Controller
{

    protected $_api_key;

    function __construct()
    {
        parent::__construct();
        $this->load->model("mobile_v2/model_api_kurir", "modelmu");
        $this->load->model("mobile_v2/model_api_ekspedisi", "model_api_ekspedisi");
        $this->load->model("mobile_v2/model_api_konsumen", "model_api_konsumen");
        $this->load->model("mobile_v2/model_api_pemilik", "model_api_pemilik");
        $this->load->model("v2/model_promo_old", "model_promo_old");
        $this->load->model("v2/model_merchant_promo", "model_merchant_promo");
        $this->load->model("v2/model_partner", "model_partner");


        $api_key = $this->input->request_headers();
        $this->_api_key = $api_key['Api-key'];
    }
    
    function kirim_kode()
    {

        $id_kode = date("YmdHis") . rand(1000, 10000);
        $kode    = rand(1000, 10000);
        $no_telp = $this->input->post("no_telp");
        
        $this->modelmu->kirim_kode($id_kode, $kode);
        
        require_once APPPATH . 'controllers/ctr_email.php';
        $myemail = new Ctr_email();
        
        $pesan_sms = "Kode SMS untuk OPA :" . $kode;
        
        //$myemail->send_email("ahmadzainuri94@gmail.com", "kirim email", $pesan_sms);
        
        
        $status_no_telp = $this->modelmu->cek_no_telp($no_telp);

        if ($status_no_telp != "0") {
            $data['code']    = "0"; //jika 0 maka gagal login, jika 1 sukses
            $data['pesan']   = "Nomor HP sudah terdaftar";

            echo json_encode($data);
        }  else {
            $data['id_kode'] = $id_kode;
            $data['code']    = "1"; //jika 0 maka gagal login, jika 1 sukses
            $data['pesan']   = "Sukses";
            echo json_encode($data);
            $myemail->send_sms($no_telp, $pesan_sms);
        }
        
    }


    function verified_phone()
    {

        $id_kode = date("YmdHis") . rand(1000, 10000);
        $kode    = rand(1000, 10000);

        $no_telp = $this->input->post("no_telp");
        $currphone = $this->input->post("curr_phone");
        
        $this->modelmu->kirim_kode($id_kode, $kode);
        
        require_once APPPATH . 'controllers/ctr_email.php';
        $myemail = new Ctr_email();
        $status_no_telp = $this->modelmu->cek_no_telp($no_telp);

        if ($status_no_telp != "0" && $currphone != "") {
            $data['code']    = "0"; //jika 0 maka gagal login, jika 1 sukses
            $data['pesan']   = "Nomor HP sudah terdaftar";
            echo json_encode($data);
        }  else {
            if($status_no_telp <= 0){
            $data['code']    = "0"; //jika 0 maka gagal login, jika 1 sukses
            $data['pesan']   = "Nomor HP tidak terdaftar";
            echo json_encode($data);
            }else{
            $pesan_sms = "Kode Verifikasi OPA KURIR :" . $kode;
            $data['id_kode'] = $id_kode;
            $data['code']    = "1"; //jika 0 maka gagal login, jika 1 sukses
            $data['pesan']   = "Sukses";
            echo json_encode($data);
            $myemail->send_sms($no_telp, $pesan_sms);
            }
        }
        
        
    }

    function kirim_kode_forgot_password()
    {

        $id_kode = date("YmdHis") . rand(1000, 10000);
        $kode    = rand(1000, 10000);
        $no_telp = $this->input->post("no_telp");
        
        $this->modelmu->kirim_kode($id_kode, $kode);
        
        require_once APPPATH . 'controllers/ctr_email.php';
        $myemail = new Ctr_email();
        
        $pesan_sms = "Kode SMS untuk OPA :" . $kode;
        
        //$myemail->send_email("ahmadzainuri94@gmail.com", "kirim email", $pesan_sms);
        
        
        $status_no_telp = $this->modelmu->cek_no_telp($no_telp);

        if ($status_no_telp != "0") {
            $data['code']    = "1"; //jika 0 maka gagal login, jika 1 sukses
            $data['pesan']   = "Sukses";
            $data['id_kode'] = $id_kode;

            echo json_encode($data);
            $myemail->send_sms($no_telp, $pesan_sms);
        }  else {
            $data['id_kode'] = $id_kode;
            $data['code']    = "0"; //jika 0 maka gagal login, jika 1 sukses
            $data['pesan']   = "No HP tidak terdaftar";
            echo json_encode($data);
        }
        
    }

    function cek_no_telp()
    {
        $no_telp = $this->input->post("no_telp");
        //$myemail->send_email("ahmadzainuri94@gmail.com", "kirim email", $pesan_sms);
        
        //die("cek_no_telp");
        $status_no_telp = $this->modelmu->cek_no_telp($no_telp);

        if ($status_no_telp == "1") {
            
            $data['code']    = "1"; //jika 0 maka gagal login, jika 1 sukses
            $data['pesan']   = "No HP sudah terdaftar silahkan login";

            echo json_encode($data);
        }  else {
            $data['code']    = "0"; //jika 0 maka gagal login, jika 1 sukses
            $data['pesan']   = "Daftar baru";
            echo json_encode($data);
        }
        
    }

    function cek_kode() {
        $id_kode_sms = $this->input->post("id_kode_sms");
        $kode_sms    = $this->input->post("kode_sms");

        $status_kode    = $this->modelmu->cek_kode($id_kode_sms, $kode_sms);

        $code  = "0";
        $pesan = "";

        if ($status_kode == "0") {
            $code  = "0";
            $pesan = "Kode salah";
        } else {
            $code  = "1";
            $pesan = "Sukses";
        }
        $data['code']        = $code;
        $data['pesan']       = $pesan;
        $data['kode']        = $id_kode_sms . " -> " . $kode_sms;
        
        echo json_encode($data);
    }

    // jangan sembarang panggil method ini !!!
    function edit_digit() {

        $results = $this->modelmu->get_kurir_all();
        foreach ($results as $result) {
            $new_id = substr($result->id_kurir, -8);
            $this->modelmu->update_id($result->id_kurir, $new_id);
        }
        $code  = "1";
        $pesan = "Sukses";
        $data['code']        = $code;
        $data['pesan']       = $pesan;
        echo json_encode($data);
    }

    function reset_status($id_kurir) {
        $this->modelmu->deaktifasi($id_kurir);

        $data['code']       = '1';
        $data['pesan']      = 'Sukses';
        $data['id_kurir'] = $id_kurir;

        echo json_encode($data);
    }
    
    function daftar()
    {

        $id_kode_sms = $this->input->post("id_kode_sms");
        $kode_sms    = $this->input->post("kode_sms");
        $id_kurir = $this->input->post("id_kurir");
        
        if ($id_kurir != null || $id_kurir != "") {
            $id_kurir    = $this->input->post("id_kurir");
            $this->modelmu->aktifasi($id_kurir);

            $code  = "1";
            $pesan = "Sukses";

        } else {
            $password    = $this->input->post("password");
            $id_provinsi = $this->input->post("id_provinsi");
            $id_kota     = $this->input->post("id_kota");
            $no_telp     = $this->input->post("no_telp");
            $radius   = $this->input->post("radius");

            $id_kurir = $this->modelmu->get_random_num();
            $username   = "User Baru";
            
            $status_no_telp = $this->modelmu->cek_no_telp($no_telp);
            $status_kode    = $this->modelmu->cek_kode($id_kode_sms, $kode_sms);
            
            $code  = "0";
            $pesan = "";
            
            if ($status_no_telp == "0") {
                // if ($status_kode == "0") {
                //     $code  = "0";
                //     $pesan = "Kode salah";
                // } else {
                $code  = "1";
                $pesan = "Sukses";

                $id_kurir_return = $this->modelmu->daftar($id_kurir, $no_telp, $password, $username, $kode_sms, $id_provinsi, $id_kota, $radius);

                    // $status_daftar = "1";


                // }
                
            } else {
                $code  = "0";
                $pesan = "No telepon sudah ada";
            }
        }
        
        
        $data['code']       = $code;
        $data['pesan']      = $pesan;
        $data['id_kurir'] = $id_kurir;
        $data['kode']       = $id_kode_sms . " -> " . $kode_sms;
        
        echo json_encode($data);
    }

    
    function lupa_password()
    {

        $no_telp = $this->input->post("no_telp");
        $id_kode = $this->input->post("id_kode_sms");
        $kode    = $this->input->post("kode_sms");
        
        // $no_telp  = "22";
        // $id_kode  = "7777777";
        // $kode     = "11";
        
        $status_no_telp = $this->modelmu->cek_no_telp($no_telp);
        $status_kode    = $this->modelmu->cek_kode($id_kode, $kode);
        $id_kurir     = $this->modelmu->get_id_kurir_by_no_telp($no_telp);
        
        $code  = "0";
        $pesan = "";
        
        if (count($id_kurir) == 0) {
            $code  = "0";
            $pesan = "Nomor telepon tidak terdaftar!";
        } else if (intval($status_kode) == 0) {
            $code  = "0";
            $pesan = "kode salah";
        } else {
            $code  = "1";
            $pesan = "ok";
        }
        
        $data['code']       = $code;
        $data['pesan']      = $pesan;
        $data['id_kurir'] = $id_kurir->id_kurir;
        
        echo json_encode($data);
    }
    
    function login()
    {
        $no_telp  = $this->input->post("no_telp");
        $password = $this->input->post("password");
        // $no_telp   = "22";
        // $password   = "tt";
        // echo $no_telp . ",  " . $password; die();
        
        $status_no_telp = $this->modelmu->cek_no_telp($no_telp);
        $status_login = $this->modelmu->login($no_telp, $password)->num_rows();
        $pesan        = "";
        
        if ($status_no_telp != "0") {
            
            if ($status_login == "1") {
                $pesan = "Sukses";
                $this->db->query("update m_kurir set deviceid='".$this->_api_key."' where id_kurir='$no_telp'");
            } else if ($status_login == "0") {

                $pesan = "Kata sandi salah!";
                
            }
            
        } else {

            $pesan = "Nomor telepon tidak terdaftar!";
            
        }
        
        
        
        $data['code']  = $status_login; //jika 0 maka gagal login, jika 1 sukses
        $data['pesan'] = $pesan;
        $data['data']  = $this->modelmu->login($no_telp, $password)->result();
        
        echo json_encode($data);
    }

    function cek_login()
    {
        $no_telp  = $this->input->post("no_telp");
        $password = $this->input->post("password");
        // $no_telp   = "22";
        // $password   = "tt";
        // echo $no_telp . ",  " . $password; die();
        
        $status_no_telp = $this->modelmu->cek_no_telp($no_telp);
        $status_login = $this->modelmu->login($no_telp, $password)->num_rows();
        $pesan        = "";
        
        if ($status_no_telp != "0") {
            
            if ($status_login == "1") {
                $pesan = "Sukses";
                
            } else if ($status_login == "0") {

                $pesan = "Kata sandi salah!";
                
            }
            
        } else {

            $pesan = "Nomor telepon tidak terdaftar!";
            
        }
        
        
        
        $data['code']  = $status_login; //jika 0 maka gagal login, jika 1 sukses
        $data['pesan'] = $pesan;
        $data['data']  = $this->modelmu->login($no_telp, $password)->result();
        
        echo json_encode($data);
    }
    

    // reset token/FCM id
    public function logout()
    {
        $id_kurir  = $this->input->post("id_kurir");
        

        $data['code']  = "1"; //jika 0 maka gagal login, jika 1 sukses
        $data['pesan'] = "Logout";
        
        echo json_encode($data);
    }

    public function insert_lokasi_kurir()
    {
        $id_kurir   = $this->input->post("id_kurir");
        $jenis_lokasi = $this->input->post("jenis_lokasi");
        $id_lokasi    = date("YmdHis") . $id_kurir;
        
        $this->modelmu->insert_lokasi_kurir($id_kurir, $id_lokasi, $jenis_lokasi);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->modelmu->get_lokasi_kurir($id_kurir)->result();
        
        echo json_encode($data);
    }
    
    public function update_nama_lokasi_kurir()
    {
        $id_kurir  = $this->input->post("id_kurir");
        $nama_lokasi = $this->input->post("nama_lokasi");
        $id_lokasi   = $this->input->post("id_lokasi");
        
        $this->modelmu->update_nama_lokasi_kurir($id_lokasi, $nama_lokasi);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->modelmu->get_lokasi_kurir($id_kurir)->result();
        
        echo json_encode($data);
    }

    
    public function update_min_antar()
    {
        $id_kurir  = $this->input->post("id_kurir");
        $min_antar = $this->input->post("min_antar");
        
        $this->modelmu->update_min_antar($id_kurir, $min_antar);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->modelmu->get_lokasi_kurir($id_kurir)->result();
        
        echo json_encode($data);
    }


    public function update_alamat_kurir()
    {
        $id_kurir  = $this->input->post("id_kurir");
        $alamat = $this->input->post("alamat");
        $nama   = $this->input->post("nama"); 
        $no_telp   = $this->input->post("no_telp"); 

        //$status_no_telp = $this->modelmu->cek_no_telp_withid($no_telp, $id_kurir);

        $this->modelmu->update_alamat_kurir($id_kurir,$nama, $no_telp, $alamat);

        $data['code']    = "1"; 
        $data['pesan']   = "update Berhasil";
        $data['data']  = $this->modelmu->get_kurir($no_telp);

        echo json_encode($data);
    }
    
    public function update_lokasi_kurir()
    {
        $id_kurir  = $this->input->post("id_kurir");
        $lat         = $this->input->post("lat");
        $lng         = $this->input->post("lng");
        $id_lokasi   = $this->input->post("id_lokasi");
        $nama_lokasi = $this->input->post("nama_lokasi");
        
        // $id_kurir   = "1605101100457770";
        // $lat = "-8.030870539137213";
        // $lng = "112.65950795263666";
        // $id_lokasi = "2315412354";
        $this->modelmu->update_lokasi_kurir($id_lokasi, $nama_lokasi, $lat, $lng);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->modelmu->get_lokasi_kurir($id_kurir)->result();
        
        echo json_encode($data);
    }
    
    public function get_kategori_produk()
    {

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->modelmu->get_kategori_produk();
        
        echo json_encode($data);
    }

    public function get_ekspedisi()
    {

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->model_api_ekspedisi->get_data()->result();
        
        echo json_encode($data);
    }

    public function get_agen_ekspedisi()
    {
        $lat = $this->input->get("lat");
        $lng = $this->input->get("lng");

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->model_api_ekspedisi->get_agen_terdekat($lat,$lng)->result();
        
        echo json_encode($data);
    }

    public function get_kuber_merchant()
    {
        $lat = $this->input->get("lat");
        $lng = $this->input->get("lng");

        $semua_usaha = array();
        
        $ekspedisi  = $this->model_api_ekspedisi->get_agen_terdekat($lat,$lng)->result();

        $id_kategori = "";
        $usaha  = $this->model_api_konsumen->get_pemilik($id_kategori, $lat, $lng);

        $semua_usaha = array_merge($ekspedisi, $usaha);


        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data'] = $semua_usaha;

        echo json_encode($data);

    }

    public function insert_agen_ekspedisi()
    {

        $id_ekspedisi = $this->input->post("id_ekspedisi");
        $nama = $this->input->post("nama");
        $lat = $this->input->post("lat");
        $lng = $this->input->post("lng");
        $alamat = $this->input->post("alamat");

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->model_api_ekspedisi->insert_agen($id_ekspedisi, $nama, $lat, $lng, $alamat);
        
        echo json_encode($data);
    }

    public function get_kategori_produk_kurir()
    {
        $id_kurir = $this->input->post("id_kurir");
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->modelmu->get_kategori_produk_kurir($id_kurir);
        
        echo json_encode($data);
    }
    
    public function insert_kategori_produk()
    {
        $nama_kategori = $this->input->post("nama_kategori");
        $id_kurir    = $this->input->post("id_kurir");
        //echo "----".$id_kurir."------";
        $this->modelmu->insert_kategori_produk($nama_kategori, $id_kurir);
        
        $data['code']        = "1";
        $data['pesan']       = "Berhasil";
        $data['data']        = $this->modelmu->get_kurir_produk($id_kurir);
        $data['id_kategori'] = $this->modelmu->get_kategori_produk_by_nama($nama_kategori, $id_kurir);
        
        echo json_encode($data);
    }

    public function insert_produk()
    {
        $min_antar = "1";
        
        $id_kurir  = $this->input->post("id_kurir");
        $id_produk   = date("ymdHis") . $id_kurir . rand(100, 1000);
        $nama        = $this->input->post("nama");
        $harga       = $this->input->post("harga");
        $harga_promo       = $this->input->post("harga_promo");
        $stok        = $this->input->post("stok");
        $min_antar        = $this->input->post("min_antar");
        $keterangan  = $this->input->post("keterangan");
        $id_kategori = 0;
        $ket_promo = $this->input->post("ket_promo");

        if (intval($id_kategori) == 0) {
            $nama_kategori = $this->input->post("nama_kategori");
            $id_kategori = $this->modelmu->insert_kategori_produk($nama_kategori, $id_kurir, $min_antar);
        }
        
        $foto        = $this->input->post("foto");
        $foto = str_replace("\"", "", $foto);

        $this->modelmu->insert_produk($id_produk, $id_kurir, $nama, $harga, $harga_promo, $stok, $keterangan, $id_kategori, $min_antar, $ket_promo);

            $errors = array();

            $foto3 = "";
            
            if ($foto != "") {
                $foto2 = explode(".", $foto);
                $foto3 = $id_produk . "." . $foto2[count($foto2) - 1];
            }
            
            if ($foto3 != "") {
                $bitmap = $_FILES;
                $return = $this->upload(PATH_IMAGE_PRODUK, $foto3, $bitmap, $id_produk); //ini nama input type file(gambar nya)
            }
            
            $this->modelmu->set_foto_produk($id_produk, $foto3);

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = array_reverse($this->modelmu->get_kurir_produk($id_kurir));
        $data['errors']  = $return;
        echo json_encode($data);
    }


    public function insert_produk_merchant_batch($id_partner)
    {
        $this->load->library('excel');

        $min_antar = "1";
        $produks = array();

        $overwrite = $this->input->post('overwrite');
        $file = $_FILES;
        $return = $this->upload_xls(PATH_XLSX_PRODUK, $id_partner, $file, $id_partner);

        $inputFileName = PATH_XLSX_PRODUK . $return;


        //  Read your Excel workbook
        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch(Exception $e) {
            die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
        }

        //  Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0); 
        $highestRow = $sheet->getHighestRow(); 
        $highestColumn = $sheet->getHighestColumn();

        $header = array();
        $rowData = $sheet->rangeToArray('A' . "1" . ':' . $highestColumn . "1",
            NULL,
            TRUE,
            FALSE);
        $header = $rowData[0];
        $rows = array();
        $k = 0;
        $id_kategori_pos = 0;
        $harga_pos = 0;
        $harga_promo_pos = 0;
        $stok_pos = 0;
        foreach ($header as $header_col) {
            if ($header_col == 'id_kategori') {
                $id_kategori_pos = $k;
            } else if ($header_col == 'harga') {
                $harga_pos = $k;
            } if ($header_col == 'harga_promo') {
                $harga_promo_pos = $k;
            } if ($header_col == 'stok') {
                $stok_pos = $k;
            } 
            $k++;
        }

        $id_kategories = array();

        for ($row = 2; $row <= $highestRow; $row++) { 
            //  Read a row of data into an array
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                NULL,
                TRUE,
                FALSE);

            $nama_kategori = $rowData[0][$id_kategori_pos];

            if (!array_key_exists($nama_kategori, $id_kategories)) {
                // die();
                $id_kategori = $this->modelmu->get_kategori_by_nama_kategori($nama_kategori);

                if (intval($id_kategori) == 0) {
                    $id_kategori = $this->modelmu->insert_kategori_produk($nama_kategori, $id_partner, $min_antar);
                    $id_kategories[$nama_kategori] = $id_kategori;
                }

                $id_kategories[$nama_kategori] = $id_kategori;
            }
            
            $rowData[0][$id_kategori_pos] = $id_kategories[$nama_kategori];
            $rowData[0][$harga_pos] = $rowData[0][$harga_pos] == '' ? '0' : str_replace(",", "", $rowData[0][$harga_pos]);
            $rowData[0][$harga_promo_pos] = $rowData[0][$harga_promo_pos] == '' ? '0' : str_replace(",", "", $rowData[0][$harga_promo_pos]);
            $rowData[0][$stok_pos] = $rowData[0][$stok_pos] == '' ? '0' : str_replace(",", "", $rowData[0][$stok_pos]);
            $rows[] = $rowData[0];
        }


        $kurirs = $this->modelmu->get_kurir_by_partner($id_partner)->result();
        if ($overwrite == 1) {
            foreach ($kurirs as $kurir) {
                $this->modelmu->delete_produk_by_kurir($kurir->id_kurir);
            }
        }

        // echo json_encode($kurirs); die();

        foreach ($kurirs as $kurir) {
            $this->modelmu->insert_produk_batch($kurir->id_kurir, $header, $rows);
        }

        //$this->modelpartner->set_produk_filename($kurir->id_partner, $return);

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $return;
        echo json_encode($data);
    }


    public function insert_produk_siap_antar_batch($id_kurir)
    {
        $this->load->library('excel');

        $min_antar = "1";
        $produks = array();

        $file = $_FILES;
        $return = $this->upload_xls(PATH_XLSX_PRODUK, $id_kurir, $file, $id_kurir);

        $inputFileName = PATH_XLSX_PRODUK . $return;


        //  Read your Excel workbook
        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch(Exception $e) {
            die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
        }

        //  Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0); 
        $highestRow = $sheet->getHighestRow(); 
        $highestColumn = $sheet->getHighestColumn();

        $header = array();
        $rowData = $sheet->rangeToArray('A' . "1" . ':' . $highestColumn . "1",
            NULL,
            TRUE,
            FALSE);
        $header = $rowData[0];
        $rows = array();
        $k = 0;

        for ($row = 2; $row <= $highestRow; $row++) { 
            //  Read a row of data into an array
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                NULL,
                TRUE,
                FALSE);

            $rows[] = $rowData[0];
        }


        // echo json_encode($kurirs); die();
        $this->modelmu->set_produk_siap_antar_batch($id_kurir, $header, $rows);
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $return;
        echo json_encode($data);
    }


    public function insert_produk_kurir_batch($id_kurir)
    {
        $this->load->library('excel');

        $min_antar = "1";
        $produks = array();

        $overwrite = $this->input->post('overwrite');
        $file = $_FILES;
        $return = $this->upload_xls(PATH_XLSX_PRODUK, $id_kurir, $file, $id_kurir);

        $inputFileName = PATH_XLSX_PRODUK . $return;


        //  Read your Excel workbook
        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch(Exception $e) {
            die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
        }

        //  Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0); 
        $highestRow = $sheet->getHighestRow(); 
        $highestColumn = $sheet->getHighestColumn();

        $header = array();
        $rowData = $sheet->rangeToArray('A' . "1" . ':' . $highestColumn . "1",
            NULL,
            TRUE,
            FALSE);
        $header = $rowData[0];
        $rows = array();
        $k = 0;
        $id_kategori_pos = 0;
        $harga_pos = 0;
        $harga_promo_pos = 0;
        $stok_pos = 0;
        foreach ($header as $header_col) {
            if ($header_col == 'id_kategori') {
                $id_kategori_pos = $k;
            } else if ($header_col == 'harga') {
                $harga_pos = $k;
            } if ($header_col == 'harga_promo') {
                $harga_promo_pos = $k;
            } if ($header_col == 'stok') {
                $stok_pos = $k;
            } 
            $k++;
        }

        $id_kategories = array();

        for ($row = 2; $row <= $highestRow; $row++){ 
            //  Read a row of data into an array
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                NULL,
                TRUE,
                FALSE);

            $nama_kategori = $rowData[0][$id_kategori_pos];

            if (!array_key_exists($nama_kategori, $id_kategories)) {
                $id_kategori = $this->modelmu->get_kategori_by_nama_kategori($nama_kategori);

                if (intval($id_kategori) == 0) {
                    $id_kategori = $this->modelmu->insert_kategori_produk($nama_kategori, $id_kurir, $min_antar);
                    $id_kategories[$nama_kategori] = $id_kategori;
                }

                $id_kategories[$nama_kategori] = $id_kategori;
            }
            
            $rowData[0][$id_kategori_pos] = $id_kategories[$nama_kategori];
            $rowData[0][$harga_pos] = $rowData[0][$harga_pos] == '' ? '0' : str_replace(",", "", $rowData[0][$harga_pos]);
            $rowData[0][$harga_promo_pos] = $rowData[0][$harga_promo_pos] == '' ? '0' : str_replace(",", "", $rowData[0][$harga_promo_pos]);
            $rowData[0][$stok_pos] = $rowData[0][$stok_pos] == '' ? '0' : str_replace(",", "", $rowData[0][$stok_pos]);
            $rows[] = $rowData[0];
        }

        if ($overwrite == 1) {
            $this->modelmu->delete_produk_by_kurir($id_kurir);
        }

        // echo json_encode($kurirs); die();

        $this->modelmu->insert_produk_batch($id_kurir, $header, $rows);

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $return;
        echo json_encode($data);
    }
    
    public function update_produk()
    {
        $min_antar = "1";

        $id_kurir = $this->input->post("id_kurir");
        $id_produk  = $this->input->post("id_produk");
        $id_kategori  = $this->input->post("id_kategori");
        $nama       = $this->input->post("nama");
        $harga      = $this->input->post("harga");
        $harga_promo      = $this->input->post("harga_promo");
        $ket_promo      = $this->input->post("ket_promo");
        $stok       = $this->input->post("stok");
        $keterangan = $this->input->post("keterangan");
        $min_antar        = $this->input->post("min_antar");

        if (intval($id_kategori) == 0) {
            $nama_kategori = $this->input->post("nama_kategori");
            $id_kategori = $this->modelmu->insert_kategori_produk($nama_kategori, $id_kurir, $min_antar);
        }
        
        $this->modelmu->update_produk($id_produk, $id_kategori, $nama, $harga, $harga_promo, $stok, $keterangan, $min_antar, $ket_promo);

        $foto        = $this->input->post("foto");
        $foto = str_replace("\"", "", $foto);

        // echo "foto: " . $foto; die();

        $errors = array();

        $foto3 = "";
        
        if ($foto != "") {
            $foto2 = explode(".", $foto);
            $foto3 = $id_produk . "." . $foto2[count($foto2) - 1];
        }
        
        if ($foto3 != "") {
            $bitmap = $_FILES;
            $return = $this->upload(PATH_IMAGE_PRODUK, $foto3, $bitmap, $id_produk); //ini nama input type file(gambar nya)
            $this->modelmu->set_foto_produk($id_produk, $foto3);
        }
        

        
        $data['code']  = "1";
        $data['pesan'] = "";
        $data['data']  = array_reverse($this->modelmu->get_kurir_produk($id_kurir));
        
        echo json_encode($data);
    }

    public function update_produk_umkm()
    {
        $min_antar = "1";

        $id_kurir = $this->input->post("id_kurir");
        $id_produk  = $this->input->post("id_produk");
        $id_kategori  = $this->input->post("id_kategori");
        $nama       = $this->input->post("nama");
        $harga      = $this->input->post("harga");
        $harga_promo      = $this->input->post("harga_promo");
        $ket_promo      = $this->input->post("ket_promo");
        $stok       = $this->input->post("stok");
        $keterangan = $this->input->post("keterangan");
        $min_antar        = $this->input->post("min_antar");

        if (intval($id_kategori) == 0) {
            $nama_kategori = $this->input->post("nama_kategori");
            $id_kategori = $this->modelmu->insert_kategori_produk($nama_kategori, $id_kurir, $min_antar);
        }
        
        $this->modelmu->update_produk_umkm($id_produk, $id_kategori, $nama, $harga, $harga_promo, $stok, $keterangan, $min_antar, $ket_promo);

        $foto        = $this->input->post("foto");
        $foto = str_replace("\"", "", $foto);

        // echo "foto: " . $foto; die();

        $errors = array();

        $foto3 = "";
        
        if ($foto != "") {
            $foto2 = explode(".", $foto);
            $foto3 = $id_produk . "." . $foto2[count($foto2) - 1];
        }
        
        if ($foto3 != "") {
            $bitmap = $_FILES;
            $return = $this->upload(PATH_IMAGE_PRODUK, $foto3, $bitmap, $id_produk); //ini nama input type file(gambar nya)
            $this->modelmu->set_foto_produk($id_produk, $foto3);
        }
        

        
        $data['code']  = "1";
        $data['pesan'] = "Sukses";
        $data['data']  = $this->modelmu->get_produk_umkm_by_id($id_produk);
        
        echo json_encode($data);
    }
    
    public function update_stok()
    {

        $id_kurir = $this->input->post("id_kurir");
        $id_produk  = $this->input->post("id_produk");
        $stok       = $this->input->post("stok");
        
        $this->modelmu->update_stok($id_produk, $stok);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->modelmu->get_kurir_produk($id_kurir);
        
        echo json_encode($data);
    }


    public function update_stok_batch($id_kurir)
    {

        $json_request_body = file_get_contents('php://input');
        $stocks = json_decode($json_request_body);

        $kurirs = $this->modelmu->get_kurir($id_kurir);
        $this_kurir = $kurirs[0];
        $kurir = $this->modelmu->get_first_kurir_by_partner($this_kurir->id_partner);

        $product_count = $this->modelmu->count_products($id_kurir);

        // echo $product_count['total'] . " -- id_kurir_old " . $kurir->id_kurir; die();
        if (intval($product_count['total']) <= 0) { 
            $this->modelmu->copy_products($kurir->id_kurir, $id_kurir);
        }

        $query_res = $this->modelmu->update_stocks($id_kurir, $stocks);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = array();
        $data['error'] = "";
        
        echo json_encode($data);
    }
    
    public function delete_produk()
    {

        $id_kurir = $this->input->post("id_kurir");
        $id_produk  = $this->input->post("id_produk");
        
        $this->modelmu->copy_produk_to_histori($id_produk);
        $this->modelmu->delete_produk($id_produk);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->modelmu->get_kurir_produk($id_kurir);
        
        echo json_encode($data);
    }
    

    public function check_deviceid()
    {
        $id_kurir = $this->input->get("id_kurir");

        if(!$this->modelmu->check_deviceid($id_kurir, $this->_api_key))
        {
            $data['code']  = "99";
            $data['pesan'] = "Wrong Deviceid";
            die(json_encode($data));
        }
        $data['code']  = "1";
        $data['pesan'] = "ok";
        die(json_encode($data));
    }

    public function get_order()
    {

        $id_kurir = $this->input->get("id_kurir");
        $status = $this->input->get("status");
        
        $data['data']  = array_reverse($this->modelmu->get_order($id_kurir, $status));
        $data['code']  = "1";
        $data['pesan'] = "sukses";
        
        echo json_encode($data);
        
    }

    public function get_done_kiriman()
    {
        // $this->check_session($this->input->request_headers());

        $id_kurir = $this->input->get("id_kurir");
        $tipe = $this->input->get("tipe");
           
        $kurirs     = $this->modelmu->get_kurir($id_kurir); 
        $orders = array();
        if (count($kurirs) > 0) {
            $kurir = $kurirs[0];
            // echo "ok"; die();
            $orders = $this->modelmu->get_done_kiriman($id_kurir, $tipe);
        } else {
            $data['data']  = array();
            $data['code']  = "0";
            $data['pesan'] = "Kurir tidak ditemukan";
            
            echo json_encode($data);
            return;
        }
        

        $data['data']  = array_reverse($orders);
        $data['code']  = "1";
        $data['pesan'] = "sukses";
        
        echo json_encode($data);
        
    }


    public function get_done_order()
    {
        // $this->check_session($this->input->request_headers());

        $id_kurir = $this->input->get("id_kurir");
        $tipe = $this->input->get("tipe");
           
        $kurirs     = $this->modelmu->get_kurir($id_kurir); 
        $orders = array();
        if (count($kurirs) > 0) {
            $kurir = $kurirs[0];
            // echo "ok"; die();
            $orders = $this->modelmu->get_done_order($kurir->kontak, $tipe);
        } else {
            $data['data']  = array();
            $data['code']  = "0";
            $data['pesan'] = "Kurir tidak ditemukan";
            
            echo json_encode($data);
            return;
        }
        

        $data['data']  = array_reverse($orders);
        $data['code']  = "1";
        $data['pesan'] = "sukses";
        
        echo json_encode($data);
        
    }


    public function get_new_order_count()
    {


        $id_kurir = $this->input->get("id_kurir");

        $counts = array();

        $kurirs     = $this->modelmu->get_kurir($id_kurir); 
        // // $orders = array();
        if (count($kurirs) > 0) {
            $kurir = $kurirs[0];

            $counts['merchant'] = $this->modelmu->get_new_order_count($kurir->kontak, 1);
            $counts['catering'] = $this->modelmu->get_new_order_count($kurir->kontak, 2);
            $counts['pickuppost'] =  $this->modelmu->get_new_kiriman_count($kurir->kontak);

        } else {
            $data['data']  = array();
            $data['code']  = "0";
            $data['pesan'] = "Kurir tidak ditemukan";
            
            echo json_encode($data);
            return;
        }


        $data['data']  = $counts;
        $data['code']  = "1";
        $data['pesan'] = "sukses";
        
        echo json_encode($data);
    }


    public function get_undone_order()
    {

        $id_kurir = $this->input->get("id_kurir");
        $tipe = $this->input->get("tipe");
        
        $kurirs     = $this->modelmu->get_kurir($id_kurir); 
        $orders = array();
        if (count($kurirs) > 0) {
            $kurir = $kurirs[0];
            $orders = $this->modelmu->get_undone_order($kurir->kontak, $tipe);
        } else {
            $data['data']  = array();
            $data['code']  = "0";
            $data['pesan'] = "Kurir tidak ditemukan";
            
            echo json_encode($data);
            return;
        }

        // $orders = array();
        // $order = new stdClass();
        // $items = array();
        // $prev_id_order = '';

        // // echo json_encode($produks); die();

        // if (count($produks) > 0) {
        //     $produk = $produks[0];
            
        //     $order = new stdClass();
        //     $order->id_order = $produk->id_order;
        //     $order->status = $produk->status;
        //     $order->tanggal_order = $produk->tanggal_order;
        //     $order->id_konsumen = $produk->id_konsumen;
        //     $order->nama = $produk->nama;
        //     $order->alamat_antar = $produk->alamat_antar;
        //     $order->ket_alamat = $produk->ket_alamat;
        //     $order->fee = $produk->fee;
        //     $order->total = $produk->total;
        //     $prev_id_order = $produk->id_order;

        //     unset($produk->id_order);
        //     unset($produk->status);
        //     unset($produk->tanggal_order);
        //     unset($produk->tanggal_proses);
        //     unset($produk->id_konsumen);
        //     unset($produk->nama);
        //     unset($produk->kontak);
        //     unset($produk->alamat_antar);
        //     unset($produk->alamat);
        //     unset($produk->lng_antar);
        //     unset($produk->lat_antar);
        //     unset($produk->lat);
        //     unset($produk->lng);
        //     unset($produk->versi);
        //     unset($produk->nama_kota);
        //     unset($produk->id_perkiraan_antar);
        //     unset($produk->ket_perkiraan_antar);
        //     unset($produk->ket_alamat);
        //     unset($produk->jenis_alamat);
        //     unset($produk->fee);
        //     unset($produk->total);
        //     $items[] = $produk;
        //     $order->items = $items;
        //     $orders[] = $order;
        // }

        // for ($i = 1; $i < count($produks); $i++) {
        //     $produk = $produks[$i];

        //     if ($produk->id_order != $prev_id_order) {
        //         $order->items = $items;
        //         $orders[] = $order;
        //         $order = new stdClass();
        //         $order->id_order = $produk->id_order;
        //         $order->status = $produk->status;
        //         $order->tanggal_order = $produk->tanggal_order;
        //         $order->id_konsumen = $produk->id_konsumen;
        //         $order->nama = $produk->nama;
        //         $order->alamat_antar = $produk->alamat_antar;
        //         $order->ket_alamat = $produk->ket_alamat;
        //         $order->fee = $produk->fee;
        //         $order->total = $produk->total;
        //         $items = array();
        //     }

        //     $prev_id_order = $produk->id_order;
        //     unset($produk->id_order);
        //     unset($produk->status);
        //     unset($produk->tanggal_order);
        //     unset($produk->tanggal_proses);
        //     unset($produk->id_konsumen);
        //     unset($produk->nama);
        //     unset($produk->kontak);
        //     unset($produk->alamat_antar);
        //     unset($produk->alamat);
        //     unset($produk->lng_antar);
        //     unset($produk->lat_antar);
        //     unset($produk->lat);
        //     unset($produk->lng);
        //     unset($produk->versi);
        //     unset($produk->nama_kota);
        //     unset($produk->id_perkiraan_antar);
        //     unset($produk->ket_perkiraan_antar);
        //     unset($produk->jenis_alamat);
        //     unset($produk->ket_alamat);
        //     unset($produk->fee);
        //     unset($produk->total);
        //     $items[] = $produk;
        //     $order->items = $items;
        //     // $orders[] = $order;
        // }

        $data['data']  = $orders;
        $data['code']  = "1";
        $data['pesan'] = "sukses";
        
        echo json_encode($data);
        
    }


    public function get_undone_kiriman()
    {

        $id_kurir = $this->input->get("id_kurir");
        $tipe = $this->input->get("tipe");
        
        $kurirs     = $this->modelmu->get_kurir($id_kurir); 
        $orders = array();
        // echo "ok"; die();
        if (count($kurirs) > 0) {
            $kurir = $kurirs[0];
            $kirimans = $this->modelmu->get_undone_kiriman($id_kurir, $tipe);
        } else {
            $data['data']  = array();
            $data['code']  = "0";
            $data['pesan'] = "Kurir tidak ditemukan";
            
            echo json_encode($data);
            return;
        }

        $data['data']  = $kirimans;
        $data['code']  = "1";
        $data['pesan'] = "sukses";
        
        echo json_encode($data);
        
    }


    public function get_detail_kiriman()
    {
        // $this->check_session($this->input->request_headers());

        $id_kiriman = $this->input->get("id_kiriman");
        // echo "ok"; die();
        
        $detail_kiriman = new stdClass();
        $kiriman_items = array();

        $detail_kiriman->data_kiriman    = $this->modelmu->notif_get_kiriman($id_kiriman);
        $kiriman = $detail_kiriman->data_kiriman;

        $kirimans = $this->modelmu->get_detail_kiriman($id_kiriman, $kiriman->tipe);
        $detail_kiriman->data_kurir = $this->modelmu->notif_get_pemilik($kiriman->id_kurir);


        if (intval($kiriman->tipe) == 3) {
            $items = array();

            $prev_id_ekspedisi = 0;
            if (count($kirimans) > 0) {
                $prev_id_ekspedisi = $kirimans[0]->id_ekspedisi;

                $kiriman_obj = new stdClass();
                $kiriman_obj->items = $items;
                $kiriman_obj->nama_ekspedisi = $kirimans[0]->nama_ekspedisi;
                $kiriman_obj->foto = $kirimans[0]->foto;
                $kiriman_obj->lat = $kirimans[0]->lat;
                $kiriman_obj->lng = $kirimans[0]->lng;
                $total++;
                $kiriman_obj->total = $total;
                $kiriman_items[] = $kiriman_obj;
            }

            $total = 0;

            foreach ($kirimans as $kiriman) {

                if ($kiriman->id_ekspedisi != $prev_id_ekspedisi) {
                    $total=0;
                    $kiriman_obj = new stdClass();
                    $kiriman_obj->items = $items;
                    $kiriman_obj->nama_ekspedisi = $kiriman->nama_ekspedisi;
                    $kiriman_obj->foto = $kiriman->foto;
                    $kiriman_obj->lat = $kiriman->lat;
                    $kiriman_obj->lng = $kiriman->lng;
                    $kiriman_items[] = $kiriman_obj;
                    $items = array();
                }
                $kiriman_obj->items[] = $kiriman;
                $total++;
                $kiriman_obj->total = $total;
                $prev_id_ekspedisi = $kiriman->id_ekspedisi;

            }

        } else {
            $kiriman_items = $kirimans;
        }

        // var_dump($kiriman_items); die();

        $detail_kiriman->detail_kiriman = $kiriman_items;

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $detail_kiriman;
        
        echo json_encode($data);

        return $detail_order;
    }
    
    public function set_status()
    {

        $id_kurir = $this->input->post("id_kurir");
        $status     = $this->input->post("status");

 
        $this->modelmu->set_status($id_kurir, $status);
        
        $data['code']  = "1";
        $data['pesan'] = "sukses";
        
        echo json_encode($data);
        
    }
    
    public function update_nama_toko()
    {

        $id_kurir = $this->input->post("id_kurir");
        $nama       = $this->input->post("nama");
        
        $this->modelmu->update_nama_toko($id_kurir, $nama);
        
        $data['code']  = "1";
        $data['pesan'] = "sukses";
        
        echo json_encode($data);
        
    }
    
    public function update_jargon_toko()
    {

        $id_kurir = $this->input->post("id_kurir");
        $jargon     = $this->input->post("jargon");
        
        $this->modelmu->update_jargon_toko($id_kurir, $jargon);
        
        $data['code']  = "1";
        $data['pesan'] = "sukses";
        
        echo json_encode($data);
        
    }

    public function update_radius()
    {

        $id_kurir = $this->input->post("id_kurir");
        $radius     = $this->input->post("radius");
        $lat     = $this->input->post("lat");
        $lng     = $this->input->post("lng");
        
        $this->modelmu->update_radius($id_kurir, $radius, $lat, $lng);
        
        $data['code']  = "1";
        $data['pesan'] = "sukses";
        
        echo json_encode($data);
        
    }
    
    public function update_kategori_kurir()
    {

        $id_kurir = $this->input->post("id_kurir");
        $json       = $this->input->post("json");
        
        $this->modelmu->update_kategori_kurir($id_kurir, $json);
        
        $data['code']  = "1";
        $data['pesan'] = "sukses";
        $data['data']  = $this->modelmu->get_kurir_kategori($id_kurir);
        echo json_encode($data);
        
    }
    
    public function update_kategori_kurir_awal()
    {

        $id_kurir = $this->input->post("id_kurir");
        $json       = $this->input->post("json");
        
        $this->modelmu->update_kategori_kurir_awal($id_kurir, $json);
        
        $data['code']  = "1";
        $data['pesan'] = "sukses";
        $data['data']  = $this->modelmu->get_kurir_kategori($id_kurir);
        echo json_encode($data);
        
    }
    
    public function get_jenis_kategori_kurir()
    {

        $data['data']  = $this->modelmu->get_jenis_kategori_kurir();
        $data['code']  = "1";
        $data['pesan'] = "sukses";
        
        echo json_encode($data);
        
    }
    
    public function get_detail_order()
    {

        $id_order = $this->input->get("id_order");
        
        $data['order_info']  = $this->modelmu->get_order_dari_notif($id_order);
        $data['orders']  = $this->modelmu->get_detail_order($id_order);
        $orders_promo  = $this->modelmu->get_detail_order_promo($id_order);
        $konsumen = $this->modelmu->notif_get_konsumen($id_order);
        $data['orders'] = array_merge($data['orders'], $orders_promo);

        if(is_array($konsumen) && count($konsumen) <= 0)
        {
            $data['code']  = "0";
            $data['pesan'] = "Konsumen di hapus";
            die(json_encode($data));
        }

        $merged = $konsumen; 
        if (count($data['orders']) > 0) {
            // echo "sini"; die();
            $first_order = $data['orders'][0];
            $ket_alamat = $first_order->ket_alamat;
            $konsumen_array = (array)$konsumen[0];
            $new_array = array( 'ket_alamat' => $ket_alamat);
            $merged = array_merge($konsumen_array, $new_array);
        }

        // $obj = new stdClass();
        // if (count($merged) > 0) {
        //     // $obj = $merged[0];
        // }
        $data['konsumen'] = $merged;
        $data['code']  = "1";
        $data['pesan'] = "sukses";
        
        echo json_encode($data);
        
    }
    
    public function get_perkiraan_antar()
    {

        $id_perkiraan_antar = $this->input->get("id_perkiraan_antar");
        
        $data['data']  = $this->modelmu->get_perkiraan_antar($id_perkiraan_antar);
        $data['code']  = "1";
        $data['pesan'] = "sukses";
        
        echo json_encode($data);
        
    }
    
    public function get_all_perkiraan_antar()
    {

        $data['data']  = $this->modelmu->get_all_perkiraan_antar();
        $data['code']  = "1";
        $data['pesan'] = "sukses";
        
        echo json_encode($data);
        
    }
    
    public function get_detail_kurir()
    {

        $id_kurir = $this->input->post("id_kurir");
        
        $data['data_kurir']     = $this->modelmu->get_kurir($id_kurir); //semua kategori kurir
        $info = $data['data_kurir'];
        $data['kategori_kurir']   = $this->modelmu->get_kategori_kurir($id_kurir); //semua kategori kurir*
        $data['kategori_produk']  = $this->modelmu->get_kategori_produk(); //semua kategori produk*
        //$data['banner']           = $this->modelpartner->get_banners($info[0]->id_partner)->result();
        $data['kurir_kategori'] = $this->modelmu->get_kurir_kategori($id_kurir); //kategori yang dipilih kurir kurir
        $data['kurir_lokasi']   = $this->modelmu->get_kurir_lokasi($id_kurir); //lokasi kurir kurir
        $data['kurir_produk']   = $this->modelmu->get_kurir_produk($id_kurir); //produk kurir kurir
        $data['code']             = "1";
        $data['pesan']            = "Sukses";
        echo json_encode($data);
        
    }

    public function get_radius()
    {

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->modelmu->get_radius();
        
        echo json_encode($data);
    }

    public function get_kurir()
    {
        $id_kurir = $this->input->post("id_kurir");
        
        $data['code']             = "1";
        $data['pesan']            = "Sukses";
        $data['data_kurir']     = $this->modelmu->get_kurir($id_kurir);

        $promo = array();
        
        if (count($data['data_kurir']) > 0) {

            $promo = $this->model_merchant_promo->get_promo($data['data_kurir'][0]->id_partner)->result();
        }

        $use_discount = false;
        $promos = array();
        foreach ($promo as $promo_item) {
            $promo_item->banner = $this->model_merchant_promo->get_banners($promo_item->id)->result();
            $promo_item->free_items = $this->model_merchant_promo->get_free_item($promo_item->id, $id_kurir)->result();
            if ($promo_item->tipe == "1") $use_discount = true;
            $promos[] = $promo_item;
        }

        $data['promo'] = $promos;

        $data['use_discount'] = $use_discount;

        $data['kategori_kurir']   = $this->modelmu->get_kategori_kurir($id_kurir); //semua kategori kurir*
        $data['kategori_produk']  = $this->modelmu->get_kategori_produk(); //semua kategori produk*
        $data['kurir_kategori'] = $this->modelmu->get_kurir_kategori($id_kurir); //kategori yang dipilih kurir kurir
        echo json_encode($data);
    }

    public function get_kurir_for_desktop()
    {
        $id_kurir = $this->input->post("id_kurir");
        
        $data['code']             = "1";
        $data['pesan']            = "Sukses";
        $data['data_kurir']     = $this->modelmu->get_kurir($id_kurir);


        // $data['kategori_kurir']   = $this->modelmu->get_kategori_kurir($id_kurir); //semua kategori kurir*
        // $data['kategori_produk']  = $this->modelmu->get_kategori_produk_kurir($id_kurir); //semua kategori produk*
        $data['kurir_kategori'] = $this->modelmu->get_kurir_kategori($id_kurir); //kategori yang dipilih kurir kurir
        echo json_encode($data);
    }
    
    public function get_kurir_kategori()
    {
        $id_kurir = $this->input->post("id_kurir");
        
        $data['code']  = "1";
        $data['pesan'] = "Sukses";
        $data['data']  = $this->modelmu->get_kurir_kategori($id_kurir); //kategori yang dipilih kurir kurir
        echo json_encode($data);
    }
    
    public function get_kurir_lokasi()
    {
        $id_kurir = $this->input->post("id_kurir");
        
        $data['code']  = "1";
        $data['pesan'] = "Sukses";
        $data['data']  = $this->modelmu->get_kurir_lokasi($id_kurir); //kategori yang dipilih kurir kurir
        echo json_encode($data);
    }
    
    public function get_kurir_produk()
    {
        $id_kurir = $this->input->post("id_kurir");

        $info = $this->modelmu->get_kurir($id_kurir);

        $promo = $this->model_merchant_promo->get_promo($info[0]->id_partner)->result();

        $use_discount = false;
        $promos = array();
        $id_promos = array();
        foreach ($promo as $promo_item) {
            $id_promos[] = $promo_item->id;
            $promo_item->banner = $this->model_merchant_promo->get_banners($promo_item->id)->result();
            $produks = $this->model_merchant_promo->get_free_item($promo_item->id, $id_kurir)->result();

            $produks_promo = array();
            $last_barcode = "";
            $free_items = array();
            $item = new stdClass();
            foreach ($produks as $produk) {
                if ($produk->id_produk != $last_barcode) {
                    $item = new stdClass();
                    $item->id_produk = $produk->id_produk;
                    $item->nama_produk = $produk->nama_produk;
                    $item->foto = $produk->foto;
                    $item->qty = $produk->qty;
                    $item->free_items = array();
                } 
                $free_item = new stdClass();
                $free_item->id_produk = $produk->id_produk2;
                $free_item->nama_produk = $produk->nama_produk2;
                $free_item->foto = $produk->foto2;
                $free_item->qty = $produk->qty2;
                $item->free_items[] = $free_item;

                $produks_promo[] = $item;

                $last_barcode = $item->id_produk;
            }

            $promo_item->produk = $produks_promo;

            if ($promo_item->tipe == "1") $use_discount = true;

            $promos[] = $promo_item;
        }

        $data['code']  = "1";
        $data['pesan'] = "Sukses";
        $data['data']['promo'] = $promos; 
        $data['data']['use_discount'] = $use_discount;
        $data['data']['produk']  = array_reverse($this->modelmu->get_kurir_produk_with_promo($id_kurir, $id_promos, $info[0]->tipe)); //kategori yang dipilih kurir kurir
        echo json_encode($data);
    }
    

    public function get_produk_by_kurir()
    {
        $id_kurir = $this->input->post("id_kurir");

        $data['code']  = "1";
        $data['pesan'] = "test";
        $data['data']  = array_reverse($this->modelmu->get_kurir_produk_by_kurir($id_kurir)); //kategori yang dipilih kurir kurir
        echo json_encode($data);
    }
    

    public function proses_order()
    {
        //id_order=1810190837266360181023160154&id_kurir=087888783819&id_perkiraan_antar=4&pesan_perkiraan_antar=wait%20yaa..
        $id_order           = $this->input->post("id_order");
        $id_kurir         = $this->input->post("id_kurir");
        $id_perkiraan_antar = $this->input->post("id_perkiraan_antar");
        $pesan_perkiraan_antar = $this->input->post("pesan_perkiraan_antar");
        
        //$id_order   = "1605301926317833160531191237";
        //$id_kurir = "1605301942108867";
        //$id_perkiraan_antar = "1";

        // if ($id_perkiraan_antar == 0 || empty($id_perkiraan_antar)) {
        //     $id_perkiraan_antar = $this->modelmu->create_perkiraan_antar($pesan_perkiraan_antar);
        // }

        if(empty($id_order) && empty($id_kurir))
        {
            $data['data']  = array();
            $data['code']  = "0";
            $data['pesan'] = "Parameter tidak valid";
            
            die(json_encode($data));
        }
        $order = $this->modelmu->get_konsumen_order($id_order);
        // echo json_encode($order); die();
        
        $data_konsumen = $this->modelmu->notif_get_konsumen($id_order);
        // echo json_encode($order); die();

        if (count($data_konsumen) == 0) {
            $data['data']  = array();
            $data['code']  = "0";
            $data['pesan'] = "Konsumen sudah dihapus";
            echo json_encode($data);
            die();
        }

        if (intval($order->status) == 3) {
            $data['data']  = array();
            $data['code']  = "0";
            $data['pesan'] = "Order telah dibatalkan";
            
            die(json_encode($data));
        } else  if (intval($order->status) == 2) {
            $data['data']  = array();
            $data['code']  = "2";
            $data['pesan'] = "Order telah diselesaikan oleh toko";
            echo json_encode($data);
            die();
        } else {
            $this->modelmu->proses_order($id_order, $id_perkiraan_antar, $pesan_perkiraan_antar);
        }

        $data['data']  = $this->modelmu->get_order($id_kurir);
        $data['code']  = "1";
        $data['pesan'] = "sukses";
        
        $data_konsumen = $this->modelmu->notif_get_konsumen($id_order);
        $data_order    = $this->modelmu->notif_get_order($id_order);
        $nama_konsumen = "";
        $id_konsumen   = "";
        $id            = "";
        $total         = "";
        $status        = "";
        
        $arr_token = array();
        foreach ($data_konsumen as $row) {
            array_push($arr_token, $row->token);
            $nama_konsumen = $row->nama;
            $id_konsumen   = $row->id_konsumen;
        }

        foreach ($data_order as $row) {
            $id     = $row->id_order;
            $total  = $row->total;
            $status = $row->status;
        }

        $kurir = $this->modelmu->get_kurir($id_kurir);

        $perkiraan_antar = "";
        if (intval($data_order[0]->id_perkiraan_antar) != 0) {
            $perkiraan_antar_arr = $this->modelmu->get_perkiraan_antar($data_order[0]->id_perkiraan_antar);
            $perkiraan_antar_obj = $perkiraan_antar_arr[0];
            $perkiraan_antar = $perkiraan_antar_obj->nama;
        } else {
            $perkiraan_antar = $pesan_perkiraan_antar;
        }


        echo json_encode($data);

        
        $message = array(
            "jenis" => "kurir",
            "id_konsumen" => $id_konsumen,
            "nama_pemilik" => $kurir[0]->nama,
            "id_order" => $id,
            "order_date"=> $data_order[0]->tanggal_order,
            "pesan" => $perkiraan_antar
        );


        
        $pushStatus = $this->send_message($arr_token, $message);

        $data["fcm_id"] = $arr_token;
        $data["gcm_result"] = $pushStatus;
        
    }



    public function proses_kiriman()
    {
        //id_order=1810190837266360181023160154&id_kurir=087888783819&id_perkiraan_antar=4&pesan_perkiraan_antar=wait%20yaa..
        $id_kiriman           = $this->input->post("id_kiriman");
        $id_kurir         = $this->input->post("id_kurir");
        $id_perkiraan_antar = $this->input->post("id_perkiraan_antar");
        $pesan_perkiraan_antar = $this->input->post("pesan_perkiraan_antar");
        

        if(empty($id_kiriman) && empty($id_kurir))
        {
            $data['data']  = array();
            $data['code']  = "0";
            $data['pesan'] = "Parameter tidak valid";
            
            die(json_encode($data));
        }
        $kiriman = $this->modelmu->get_konsumen_kiriman($id_kiriman);
        
        $data_konsumen = $this->modelmu->notif_get_konsumen_kiriman($id_kiriman);
        // echo json_encode($order); die();

        if (count($data_konsumen) == 0) {
            $data['data']  = array();
            $data['code']  = "0";
            $data['pesan'] = "Konsumen sudah dihapus";
            echo json_encode($data);
            die();
        }

        if (intval($kiriman->status) == 3) {
            $data['data']  = array();
            $data['code']  = "0";
            $data['pesan'] = "Kiriman telah dibatalkan";
            
            die(json_encode($data));
        } else  if (intval($kiriman->status) == 2) {
            $data['data']  = array();
            $data['code']  = "2";
            $data['pesan'] = "Kiriman telah diselesaikan oleh kurir";
            echo json_encode($data);
            die();
        } else {
            $this->modelmu->proses_kiriman($id_kiriman, $id_perkiraan_antar, $pesan_perkiraan_antar);
        }

        $kurir = $this->modelmu->get_kurir($id_kurir);

        $data['data']  = array();
        $data['code']  = "1";
        $data['pesan'] = "sukses";
        
        $data_konsumen = $this->modelmu->notif_get_konsumen_kiriman($id_kiriman);
        $data_kiriman    = $this->modelmu->notif_get_kiriman($id_kiriman);
        $nama_konsumen = "";
        $id_konsumen   = "";
        $id            = "";
        $total         = "";
        $status        = "";
        
        $arr_token = array();
        array_push($arr_token, $data_konsumen->token);
        $nama_konsumen = $data_konsumen->nama;
        $id_konsumen   = $data_konsumen->id_konsumen;


        $id     = $data_kiriman->id_kiriman;
        $total  = $data_kiriman->total;
        $status = $data_kiriman->status;

        
        $message = array(
            "jenis" => "kurir",
            "id_konsumen" => $id_konsumen,
            "nama_pemilik" => $kurir[0]->nama,
            "id_kiriman" => $id,
            "order_date"=> $data_kiriman->tanggal_order,
            "pesan" => "Kiriman akan segera dijemput"
        );
        
        // echo json_encode($message); die();
        
        $pushStatus = $this->send_message($arr_token, $message);

        $data["fcm_id"] = $arr_token;
        $data["gcm_result"] = $pushStatus;

        echo json_encode($data);
        
    }

    public function selesai_order()
    {

        $id_order           = $this->input->post("id_order");
        $id_pemilik         = $this->input->post("id_pemilik");
        $id_kurir         = $this->input->post("id_kurir");
        
        //$id_order   = "1605301926317833160531191237";
        //$id_kurir = "1605301942108867";
        //$id_perkiraan_antar = "1";

        // if ($id_perkiraan_antar == 0 || empty($id_perkiraan_antar)) {
        //     $id_perkiraan_antar = $this->modelmu->create_perkiraan_antar($pesan_perkiraan_antar);
        // }
        $order = $this->modelmu->get_konsumen_order($id_order);
        // echo json_encode($order); die();
        $data_konsumen = $this->modelmu->notif_get_konsumen($id_order);
        // echo json_encode($order); die();

        if (count($data_konsumen) == 0) {
            $data['data']  = array();
            $data['code']  = "0";
            $data['pesan'] = "Konsumen sudah dihapus";
            echo json_encode($data);
            die();
        }

        if (intval($order->status) == 3) {
            $data['data']  = array();
            $data['code']  = "0";
            $data['pesan'] = "Order telah dibatalkan";
            echo json_encode($data);
            die();
        } else if (intval($order->status) == 4) {
            $data['data']  = array();
            $data['code']  = "2";
            $data['pesan'] = "Order telah diselesaikan oleh toko";
            echo json_encode($data);
            die();
        } else {
            $this->modelmu->selesai_order($id_order);
        }

        $data['data']  = $this->modelmu->get_order($id_pemilik);
        $data['code']  = "1";
        $data['pesan'] = "sukses";
        
        $data_order    = $this->modelmu->notif_get_order($id_order);
        $nama_konsumen = "";
        $id_konsumen   = "";
        $id            = "";
        $total         = "";
        $status        = "";

        
        $arr_token = array();
        foreach ($data_konsumen as $row) {
            array_push($arr_token, $row->token);
            $nama_konsumen = $row->nama;
            $id_konsumen   = $row->id_konsumen;
        }
        
        foreach ($data_order as $row) {
            $id     = $row->id_order;
            $total  = $row->total;
            $status = $row->status;
        }

        $pemilik = $this->modelmu->get_toko($id_pemilik);
        $kurir = $this->modelmu->get_kurir($pemilik[0]->kontak_sms2);

        $perkiraan_antar = "";
        if (intval($data_order[0]->id_perkiraan_antar) != 0) {
            $perkiraan_antar_arr = $this->modelmu->get_perkiraan_antar($data_order[0]->id_perkiraan_antar);
            $perkiraan_antar_obj = $perkiraan_antar_arr[0];
            $perkiraan_antar = $perkiraan_antar_obj->nama;
        } 

        echo json_encode($data);
        
        $message = array(
            "jenis" => "kurir",
            "id_konsumen" => $id_konsumen,
            "nama_pemilik" => $kurir[0]->nama,
            "id_order" => $id,
            "order_date"=> $data_order[0]->tanggal_order,
            "pesan" => "Pesanan Anda telah diterima"
        );

        // echo json_encode($message);

        
        $pushStatus = $this->send_message($arr_token, $message);

        $data["gcm_result"] = $pushStatus;
        
    }


    public function selesai_kiriman()
    {
        //id_order=1810190837266360181023160154&id_kurir=087888783819&id_perkiraan_antar=4&pesan_perkiraan_antar=wait%20yaa..
        $id_kiriman           = $this->input->post("id_kiriman");
        $id_kurir         = $this->input->post("id_kurir");
        

        if(empty($id_kiriman) && empty($id_kurir))
        {
            $data['data']  = array();
            $data['code']  = "0";
            $data['pesan'] = "Parameter tidak valid";
            
            die(json_encode($data));
        }
        $kiriman = $this->modelmu->get_konsumen_kiriman($id_kiriman);
        
        $data_konsumen = $this->modelmu->notif_get_konsumen_kiriman($id_kiriman);
        // echo json_encode($order); die();

        if (count($data_konsumen) == 0) {
            $data['data']  = array();
            $data['code']  = "0";
            $data['pesan'] = "Konsumen sudah dihapus";
            echo json_encode($data);
            die();
        }

        if (intval($kiriman->status) == 3) {
            $data['data']  = array();
            $data['code']  = "0";
            $data['pesan'] = "Kiriman telah dibatalkan";
            
            die(json_encode($data));
        } else  if (intval($kiriman->status) == 4) {
            $data['data']  = array();
            $data['code']  = "2";
            $data['pesan'] = "Kiriman telah diselesaikan oleh kurir";
            echo json_encode($data);
            die();
        } else {
            $this->modelmu->selesai_kiriman($id_kiriman);
        }


        $kurir = $this->modelmu->get_kurir($id_kurir);

        $data['data']  = array();
        $data['code']  = "1";
        $data['pesan'] = "sukses " . $kurir[0]->nama;
        
        $data_konsumen = $this->modelmu->notif_get_konsumen_kiriman($id_kiriman);
        $data_kiriman    = $this->modelmu->notif_get_kiriman($id_kiriman);
        $nama_konsumen = "";
        $id_konsumen   = "";
        $id            = "";
        $total         = "";
        $status        = "";
        
        $arr_token = array();
        array_push($arr_token, $data_konsumen->token);
        $nama_konsumen = $data_konsumen->nama;
        $id_konsumen   = $data_konsumen->id_konsumen;

        $id     = $data_kiriman->id_kiriman;
        $total  = $data_kiriman->total;
        $status = $data_kiriman->status;


        echo json_encode($data);

        
        $message = array(
            "jenis" => "kurir",
            "id_konsumen" => $id_konsumen,
            "nama_pemilik" => $kurir[0]->nama,
            "id_kiriman" => $id,
            "order_date"=> $data_kiriman->tanggal_order,
            "pesan" => "Kiriman telah diselesaikan."
        );


        
        $pushStatus = $this->send_message($arr_token, $message);

        $data["fcm_id"] = $arr_token;
        $data["gcm_result"] = $pushStatus;
        
    }



    public function cancel_kiriman()
    {
        //id_order=1810190837266360181023160154&id_kurir=087888783819&id_perkiraan_antar=4&pesan_perkiraan_antar=wait%20yaa..
        $id_kiriman           = $this->input->post("id_kiriman");
        $id_kurir         = $this->input->post("id_kurir");
        $pesan_perkiraan_antar = $this->input->post("pesan_perkiraan_antar");
        

        if(empty($id_kiriman) && empty($id_kurir))
        {
            $data['data']  = array();
            $data['code']  = "0";
            $data['pesan'] = "Parameter tidak valid";
            
            die(json_encode($data));
        }
        $kiriman = $this->modelmu->get_konsumen_kiriman($id_kiriman);
        
        $data_konsumen = $this->modelmu->notif_get_konsumen_kiriman($id_kiriman);
        // echo json_encode($order); die();

        if (count($data_konsumen) == 0) {
            $data['data']  = array();
            $data['code']  = "0";
            $data['pesan'] = "Konsumen sudah dihapus";
            echo json_encode($data);
            die();
        }

        $push_message =  "Kiriman dari dibatalkan";
        if (intval($kiriman->status) == 1) {
            $data['code']     = "0";    
            $data['pesan']    = "Kiriman belum diproses";
            echo json_encode($data);
        } else if (intval($kiriman->status) == 2) {
            $push_message =  "Permintaan pembatalan kiriman";
            $this->model_api_konsumen->request_cancel_kiriman($id_kiriman, $pesan_perkiraan_antar);
        } else {
            $data['code']     = "0";    
            $data['pesan']    = "Kiriman sudah selesai atau dibatalkan";
            echo json_encode($data);
            return;
        }

        $kurir = $this->modelmu->get_kurir($id_kurir);

        $data['data']  = array();
        $data['code']  = "1";
        $data['pesan'] = "sukses " . $kurir[0]->nama;
        
        $data_konsumen = $this->modelmu->notif_get_konsumen_kiriman($id_kiriman);
        $data_kiriman    = $this->modelmu->notif_get_kiriman($id_kiriman);
        $nama_konsumen = "";
        $id_konsumen   = "";
        $id            = "";
        $total         = "";
        $status        = "";
        
        $arr_token = array();
        array_push($arr_token, $data_konsumen->token);
        $nama_konsumen = $data_konsumen->nama;
        $id_konsumen   = $data_konsumen->id_konsumen;

        $id     = $data_kiriman->id_kiriman;
        $total  = $data_kiriman->total;
        $status = $data_kiriman->status;

        echo json_encode($data);

        
        $message = array(
            "jenis" => "kurir",
            "id_konsumen" => $id_konsumen,
            "nama_pemilik" => $kurir[0]->nama,
            "id_kiriman" => $id,
            "order_date"=> $data_kiriman->tanggal_order,
            "pesan" => $pesan_perkiraan_antar
        );


        
        $pushStatus = $this->send_message($arr_token, $message);

        $data["fcm_id"] = $arr_token;
        $data["gcm_result"] = $pushStatus;
        
    }

    public function get_provinsi()
    {

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->modelmu->get_provinsi();
        
        echo json_encode($data);
    }
    
    public function get_kota()
    {

        $id_provinsi = $this->input->post("id_provinsi");
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->modelmu->get_kota($id_provinsi);
        
        echo json_encode($data);
    }
    
    public function isi_profil_awal()
    {

        $id_kurir = $this->input->post("id_kurir");
        $nama       = $this->input->post("nama");
        $alamat     = $this->input->post("alamat");
        $kontak_sms     = $this->input->post("kontak_sms");
        $kontak_sms2     = $this->input->post("kontak_sms2");
        $radius   = $this->input->post("radius");
        $fee   = $this->input->post("fee");
        $include_fee   = $this->input->post("include_fee");
        $include_fee_note   = $this->input->post("include_fee_note");
        $jargon     = $this->input->post("jargon");
        
        $this->modelmu->isi_profil_awal($id_kurir, $nama, $alamat, $jargon, $radius, $kontak_sms, $kontak_sms2, $fee, $include_fee, $include_fee_note);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        
        echo json_encode($data);
    }



    public function update_profile()
    {
        // echo "ok"; die();

        // $this->check_session($this->input->request_headers());
        
        $id_kurir = $this->input->post("id_kurir");
        $nama       = $this->input->post("nama");
        $alamat     = $this->input->post("alamat");
        $kontak     = $this->input->post("kontak");
        $fee   = $this->input->post("fee");
        $include_fee   = $this->input->post("include_fee");
        $include_fee_note   = $this->input->post("include_fee_note");

        
        $this->modelmu->update_profile($id_kurir, $nama, $alamat, $kontak, $fee, $include_fee, $include_fee_note);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        
        echo json_encode($data);
    }

    public function tambah_ongkirs($id_kurir) {

        // $this->check_session($this->input->request_headers());

        $ongkirs_json = $this->input->post("ongkirs");

        $ongkirs = json_decode($ongkirs_json);
        $this->modelmu->tambah_ongkirs($id_kurir, $ongkirs);

        $items = $this->modelmu->get_ongkirs($id_kurir);

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['ongkirs'] = $items;
        echo json_encode($data);
    }

    public function set_alamat_min_antar()
    {

        $id_kurir = $this->input->post("id_kurir");
        $alamat     = $this->input->post("alamat");
        $min_antar     = $this->input->post("min_antar");
        
        $this->modelmu->set_alamat_min_antar($id_kurir, $alamat, $min_antar);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        
        echo json_encode($data);
    }

    public function set_radius()
    {

        $id_kurir   = $this->input->post("id_kurir");
        $radius   = $this->input->post("radius");
        
        $this->modelmu->set_radius($id_kurir, $radius);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        
        echo json_encode($data);
    }

    public function set_no_telp()
    {

        $id_kurir = $this->input->post("id_kurir");
        $kontak       = $this->input->post("kontak");
        $kontak_sms     = $this->input->post("kontak_sms");
        $kontak_sms2     = $this->input->post("kontak_sms2");
        
        $this->modelmu->set_no_telp($id_kurir, $kontak, $kontak_sms, $kontak_sms2);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        
        echo json_encode($data);
    }
    
    public function isi_lokasi_awal()
    {

        $id_kurir  = $this->input->post("id_kurir");
        $lat         = $this->input->post("lat");
        $lng         = $this->input->post("lng");
        $nama_lokasi = $this->input->post("nama_lokasi");
        
        $this->modelmu->isi_lokasi_awal($id_kurir, $lat, $lng, $nama_lokasi);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        
        echo json_encode($data);
    }
    
    public function get_kategori_kurir()
    {

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->modelmu->get_kategori_kurir_awal();
        
        echo json_encode($data);
    }
    
    public function new_password()
    {
        $password   = $this->input->post("password");
        $id_kurir = $this->input->post("id_kurir");
        
        $this->modelmu->new_password($id_kurir, $password);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        echo json_encode($data);
    }

    public function update_password()
    {
        $old_password   = $this->input->post("old_password");
        $password   = $this->input->post("password");
        $id_kurir = $this->input->post("id_kurir");

        $password_valid = $this->modelmu->check_password($id_kurir, $old_password);
        if (!empty($password_valid)) {
            $this->modelmu->new_password($id_kurir, $password);

            $data['code']  = "1";
            $data['pesan'] = "Berhasil";
        } else {
            $data['code']  = "0";
            $data['pesan'] = "Kata Sandi Lama yang Anda masukkan salah.";
        }

        echo json_encode($data);
    }
    
    public function hapus_akun()
    {

        $id_kurir = $this->input->post("id_kurir");
        
        $this->modelmu->hapus_akun($id_kurir);
        $this->modelmu->copy_to_histori($id_kurir);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        
        echo json_encode($data);
    }
    
    public function get_penjualan_harian()
    {
        $tanggal       = $this->input->post("tanggal"); // FORMAT YYYY-MM-DD
        $id_kurir    = $this->input->post("id_kurir");
        $data['code']  = "1";
        $data['pesan'] = "Berhasil harian";
        $data['data']  = $this->modelmu->get_penjualan_harian($id_kurir, $tanggal);
        echo json_encode($data);
    }
    
    public function get_penjualan_bulanan()
    {
        $bulan         = $this->input->post("bulan");
        $tahun         = $this->input->post("tahun");
        $id_kurir    = $this->input->post("id_kurir");
        $data['code']  = "1";
        $data['pesan'] = "Berhasil bulanan";
        $data['data']  = $this->modelmu->get_penjualan_bulanan($id_kurir, $bulan, $tahun);
        echo json_encode($data);
    }
    
    public function get_penjualan_tahunan()
    {
        $tahun         = $this->input->post("tahun");
        $id_kurir    = $this->input->post("id_kurir");
        $data['code']  = "1";
        $data['pesan'] = "Berhasil tahunan";
        $data['data']  = $this->modelmu->get_penjualan_tahunan($id_kurir, $tahun);
        echo json_encode($data);
    }
    
    public function ganti_foto()
    {
        $id_kurir = $this->input->post("id_kurir");
        $foto        = $this->input->post("foto");
        $foto = str_replace("\"", "", $foto);

        // echo "foto: " . $foto; die();

        $errors = array();

        $foto3 = "";
        
        if ($foto != "") {
            $foto2 = explode(".", $foto);
            $foto3 = $id_kurir . "." . $foto2[count($foto2) - 1];
        }
        
        if ($foto3 != "") {
            $bitmap = $_FILES;
            $errors = $this->upload(PATH_IMAGE_KURIR, $foto3, $bitmap, $id_kurir); //ini nama input type file(gambar nya)
        }
        
        $this->modelmu->set_foto($id_kurir, $foto3);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->modelmu->get_foto($id_kurir);
        $data['errors'] = $errors;
        $data['foto'] = $id_kurir;
        
        echo json_encode($data);
    }

    public function upload_resi()
    {
        $id = $this->input->post("id_detail_kiriman");
        $foto        = $this->input->post("foto");
        $tipe        = $this->input->post("tipe");
        $foto = str_replace("\"", "", $foto);

        // echo "foto: " . $foto; die();

        $errors = array();

        $foto3 = "";
        
        if ($foto != "") {
            $foto2 = explode(".", $foto);
            $foto3 = $tipe . $id . "." . $foto2[count($foto2) - 1];
        }
        
        if ($foto3 != "") {
            $bitmap = $_FILES;
            $errors = $this->upload(PATH_IMAGE_KIRIMAN, $foto3, $bitmap, $tipe . $id); //ini nama input type file(gambar nya)
        }

        // $kiriman = $this->modelmu->get_kiriman_by_id_detail_kiriman($id);
        
        $this->modelmu->set_foto_resi($id, $foto3, $tipe);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->modelmu->get_foto_resi($id, $tipe);
        $data['errors'] = $errors;
        $data['foto'] = $id;
        
        echo json_encode($data);
    }

    public function ganti_foto_produk()
    {
        $id_produk = $this->input->post("id_produk");
        $foto        = $this->input->post("foto");
        $foto = str_replace("\"", "", $foto);

        // echo "foto: " . $foto; die();

        $errors = array();

        $foto3 = "";
        
        if ($foto != "") {
            $foto2 = explode(".", $foto);
            $foto3 = $id_produk . "." . $foto2[count($foto2) - 1];
        }
        
        if ($foto3 != "") {
            $bitmap = $_FILES;
            $return = $this->upload(PATH_IMAGE_PRODUK, $foto3, $bitmap, $id_produk); //ini nama input type file(gambar nya)
        }
        
        $this->modelmu->set_foto_produk($id_produk, $foto3);
        
        if (strlen($return) <= 0) {
            $data['code']  = "1";
            $data['pesan'] = "Berhasil";
            $data['data']  = $this->modelmu->get_foto_produk($id_produk);
        } else {
            $data['code']  = "0";
            $data['pesan'] = "Gagal upload";
            $data['data']  = null;
        }
        
        echo json_encode($data);
    }

    public function ganti_foto_avatar()
    {
        $id_kurir = $this->input->post("id_kurir");
        $foto        = $this->input->post("foto");
        
        if ($foto != "") {
            $foto2 = explode(".", $foto);
            $foto3 = $id_kurir . "." . $foto2[count($foto2) - 1];
        }

        if ($foto3 != "") {
            $bitmap = $this->input->post("form_file");
            $error = $this->upload(PATH_IMAGE_AVATAR_KURIR, $foto3, $_FILES, $id_kurir); //ini nama input type file(gambar nya)
        }
        
        $this->modelmu->set_foto_avatar($id_kurir, $foto3);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->modelmu->get_foto_avatar($id_kurir);
        //$data['error']  = $error;
        log_message('error', print_r($data, true));

        echo json_encode($data);
    }
    
    public function update_kategori_produk()
    {

        $id_kurir    = $this->input->post("id_kurir");
        $id_kategori   = $this->input->post("id_kategori");
        $nama_kategori = $this->input->post("nama_kategori");
        
        $this->modelmu->update_kategori_produk($id_kategori, $nama_kategori);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->modelmu->get_kurir_produk($id_kurir);
        
        echo json_encode($data);
    }
    
    public function delete_kategori_produk()
    {

        $id_kurir  = $this->input->post("id_kurir");
        $id_kategori = $this->input->post("id_kategori");
        
        $this->modelmu->delete_kategori_produk($id_kategori);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->modelmu->get_kurir_produk($id_kurir);
        
        echo json_encode($data);
    }
    
    
    // public function upload($path, $nama_file, $id_file)
    // {
    //     try {
    //         $config['upload_path']   = $path;
    //         $config['file_name']     = $nama_file;
    //         $config['allowed_types'] = '*';
    //         $config['overwrite']     = TRUE;
    //         $config['overwrite']     = TRUE;

    //         file_put_contents($path, base64_decode($nama_file));

    //         // $this->load->library('upload', $config);
    //         // if ( ! $this->upload->do_upload($id_file))
    //         // {
    //         //     $error = array('error' => $this->upload->display_errors());

    //         // }   
    //         // else
    //         // {
    //         //     $upload_data = $this->upload->data();
    //         // }
    //         return "";
    //     }
    //     catch (Exception $exc) {
    //         return $exc->getTraceAsString();
    //     }
    // }

    public function upload($path, $nama_file, $files, $id_file)
    {   
        $exp                     = explode(".", $nama_file);
        $ext                     = end($exp);  

        // echo $path.$id_file.".".$ext; die();

        if (is_file($path.$id_file.".".$ext)) {
            unlink($path.$id_file.".".$ext);
        }
        try {

            $config['upload_path']   = $path;
            $config['file_name']     = $id_file;
            $config['allowed_types'] = '*';
            $config['overwrite']     = TRUE;

            if (!is_dir($path)) {
                mkdir($path, 0777, true);
            }
            //log_message('error', print_r($config, true));
            //log_message('error', print_r($files, true));
            if (isset($files['form_file']) && !empty($files['form_file'])) {
                if ($files['form_file']['error'] != 4) {
                    //log_message('error', "initialize library upload ");
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    
                    if (!$this->upload->do_upload('form_file')) {
                        $errors = $this->upload->display_errors();
                        //log_message('error', "iUpload failed ");
                        //log_message('error', var_dump($errors));
                        return $errors;
                    } else {
                        $upload_data = $this->upload->data();
                        //log_message('error', "upload berhasil ");
                        //log_message('error', print_r($upload_data, true));
                        // echo json_encode($upload_data); die();
                        return $upload_data;
                    }
                }
            } 
        }
        catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }


    function unzip($path, $filename) {
        // echo $path.$filename; die();
        // get the absolute path to $file
        $zip = new ZipArchive;
        $res = $zip->open($path.$filename);
        if ($res === TRUE) {
          $zip->extractTo($path);
          $zip->close();
          return 1;
        } else {
          return 0;
        }
    }


    public function upload_xls($path, $nama_file, $files, $id_file)
    {   
        $exp                     = explode(".", $nama_file);
        $ext                     = end($exp);  

        $name = $_FILES["form_file"]["name"];
        $exploded = explode(".", $name);
        $ext = end($exploded); 
        // echo $path.$id_file.".".$ext; die();

        if (is_file($path.$id_file.".".$ext)) {
            unlink($path.$id_file.".".$ext);
        }
        try {

            $config['upload_path']   = $path;
            $config['file_name']     = str_replace(' ', '', reset($exploded)) . "_" . date("m-d-Y_G-i") . "." . $ext;
            $config['allowed_types'] = '*';
            $config['overwrite']     = TRUE;

            if (!is_dir($path)) {
                mkdir($path, 0777, true);
            }
            
            if (isset($files['form_file']) && !empty($files['form_file'])) {
                if ($files['form_file']['error'] != 4) {

                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    
                    if (!$this->upload->do_upload('form_file')) {
                        $errors = $this->upload->display_errors();
                        return "";
                        // echo json_encode($errors); die();
                    } else {
                        $upload_data = $this->upload->data();
                        return str_replace(' ', '', reset($exploded)) . "_" . date("m-d-Y_G-i") . "." . $ext;
                        // echo json_encode($upload_data); 
                    }
                }
            } 

            return str_replace(' ', '', reset($exploded)) . "_" . date("m-d-Y_G-i") . "." . $ext;
        }
        catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return "";
        }
    }
    
    public function update_token()
    {

        $id_kurir = $this->input->post("id_user");
        $token      = $this->input->post("token");
        
        // echo $token;
        
        $this->modelmu->update_token($id_kurir, $token);
        
        $data['code']  = "1";
        $data['pesan'] = "sukses";
        $data['data']  = $this->modelmu->get_kurir($id_kurir);
        
        echo json_encode($data);
    }
    
    public function get_order_dari_notif()
    {
        $id_order = $this->input->get("id_order");
        
        $data['code']     = "1";
        $data['pesan']    = "Sukses";
        $data['kategori'] = $this->modelmu->get_order_dari_notif($id_order);
        
        echo json_encode($data);
    }


    public function get_produk_promo()
    {

        $id_kurir = $this->input->get("id_kurir");

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data'] = new stdClass();
        // $kurirs = $this->modelmu->get_kategori_kurir($id_kurir);

        $promo_codes = $this->model_promo_old->get_promo_code($id_kurir)->result();
        // echo json_encode($promo_code);
        $produk_promo = array();
        foreach ($promo_codes as $promo_code) {
            $produk_promo = array_merge($produk_promo, $this->model_promo_old->get_produk_promo($promo_code->promo_id, $id_kurir));
        }
        // echo json_encode($produk_promo);
        // die();
        // $data['data']->produk_promo = [];

        // foreach ($kurirs as $kurir) {
        //     if ($kurir->id_kategori == $promo_for_konsumen->id_kategori) {
                $data['data']->produk_promo = $produk_promo;
        //     }
        // }

        echo json_encode($data);
    }

    public function get_produk_umkm($id_kurir, $page=0)
    {

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['page_count'] = ceil($this->modelmu->get_total_produks_umkm($id_kurir) / 50);
        $produk_umkm = $this->modelmu->get_produks_umkm($id_kurir, $page);
        $data['data'] = $produk_umkm;

        echo json_encode($data);
    }
    


    public function update_provinsi_kota()
    {
        $id_provinsi = $this->input->post("id_provinsi");
        $id_kota     = $this->input->post("id_kota");
        $id_kurir = $this->input->post("id_kurir");
        
        $this->modelmu->update_provinsi_kota($id_kurir, $id_provinsi, $id_kota);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil ".$id_kurir." - " .$id_provinsi. " - ".$id_kota;
        echo json_encode($data);
    }

    // public function tambah_ongkirs($id_kurir) {
    //     $ongkirs_json = $this->input->post("ongkirs");

    //     $ongkirs = json_decode($ongkirs_json);
    //     $this->modelmu->tambah_ongkirs($id_kurir, $ongkirs);

    //     $items = $this->modelmu->get_ongkirs($id_kurir);

    //     $data['code']  = "1";
    //     $data['pesan'] = "Berhasil";
    //     $data['ongkirs'] = $items;
    //     echo json_encode($data);
    // }


    public function set_fee($id_kurir) {
        $fee = $this->input->post("fee");

        $this->modelmu->set_fee($id_kurir, $fee);

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        echo json_encode($data);
    }

    public function get_ongkirs($id_kurir) {

        $items = $this->modelmu->get_ongkirs($id_kurir);
        $fee = $this->modelmu->get_kurir_fee($id_kurir);

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['fee'] = $fee->fee;
        $data['ongkirs'] = $items;
        echo json_encode($data);
    }
    
    
    
    
    
    function tes_push()
    {
        $data_konsumen = $this->modelmu->notif_get_konsumen("1605301926317833160530213139");
        $data_order    = $this->modelmu->notif_get_order("1605301926317833160530213139");
        $nama_konsumen = "";
        $id_konsumen   = "";
        $id            = "";
        $total         = "";
        $status        = "";
        
        $arr_token = array();
        foreach ($data_konsumen as $row) {
            array_push($arr_token, $row->token);
            $nama_konsumen = $row->nama;
            $id_konsumen   = $row->id_konsumen;
        }
        
        foreach ($data_order as $row) {
            $id     = $row->id_order;
            $total  = $row->total;
            $status = $row->status;
        }
        
        $message = array(
            "jenis" => "1",
            "id_konsumen" => $id_konsumen,
            "id_order" => $id,
            "total" => $total,
            "status" => $status,
            "pesan" => $nama_konsumen . " yth, Order anda sedang diproses."
        );
        
        print_r($data_konsumen);
        echo "<hr>";
        print_r($data_order);
        
        $pushStatus = $this->send_message($arr_token, $message);
    }


    function test_kirim()
    {
        $kurir = $this->modelmu->get_kurir("082134704620");
        $arr_token = array();
        foreach ($kurir as $row) {
            array_push($arr_token, $row->token);
        }
        
        $message = array(
            "jenis" => "1",
            "id_order"=>"09989897876886876",
            "merchant_name"=>"UAT STORE",
            "notif" => "Ada pesan antar yang harus di kirim."
        );
        print_r($this->send_message($arr_token, $message));
    }
    
    function send_message($registatoin_ids, $message)
    {
            // $url    = 'https://android.googleapis.com/gcm/send';
        $url    = 'https://fcm.googleapis.com/fcm/send';

        if (count($registatoin_ids) <= 0) return;

        $fields = new stdClass();
        $fields->to = $registatoin_ids[0];

        // $notification = new stdClass();
        // $notification->click_action = ".MainActivity";
        // $notification->body = $message["notif"];
        // $notification->title = "OPA";
        // $notification->icon = "ic_launcher";
        // $notification->sound = "default";
        // $notification->android_channel_id = "default";

        // $fields->notification = $notification;

        // $fields->to = "d9uV01g7ssU:APA91bEvWcqf_PyJxN2UqD0W0S1CsHJFEXVL2tJKOVKODe4BWbISQgq7y7BPyHq_NOiJyfehak7dZ4Is5CkI8Hwgef87z0hQK6l9-Evtra5iR8beZvARivAL3l5hDd-ns2Y5TNxeoDLz";
        $fields->data = $message;
        //$fields->notification = $message;
            // $fields = array(
            //     'to' => $registatoin_ids,
            //     'data' => $message
            //     );

        
        
        $headers = array(
            'Authorization: key=' . KEY_GCM_KURIR,
            'Content-Type: application/json'
        );
        $ch      = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === FALSE) {
            $result = "salah";
        }
        curl_close($ch);
        return $result;
    }
    
    function confirm_promo_code()
    {
        header("Content-type: application/json");
        
        $id_kurir = $this->input->post("id_kurir");
        $promo_code = $this->input->post("promo_code");
        
        $promo = $this->model_promo_old->validate_promo_code($id_kurir, $promo_code);
        
        $data = array();
        if ($promo != null) {
            $used = $this->model_promo_old->confirm_promo_code($promo_code);

            $data['code']  = "1";
            $data['pesan'] = "Selamat, promo Anda aktif";
            $data['data']  = "";

            // $produks_promo = $this->model_promo_old->get_produk_promo($promo->promo_id);

            // foreach ($produks_promo as $produk) {
            //     // $array = json_decode(json_encode($produk), true);
            //     $this->model_promo_old->copy_produk_promo($id_kurir, $produk);
            // }

        } else {
            $data['code']  = "0";
            $data['pesan'] = "Maaf kode promo yang Anda masukkan salah. Silakan hubungi Admin untuk meminta kode promo Anda";
            $data['data']  = "";
        }
        echo json_encode($data);
        
    }

    function get_promo($id_kurir)
    {
        header("Content-type: application/json");
        
        $promo = $this->model_promo_old->get_promo($id_kurir)->row();
        
        
        $data = array();

        if (!empty($promo)) {
            $data['code']  = "1";
            $data['pesan'] = "Promo ada";
            $data['data']  = $promo;

        } else {
            $data['code']  = "0";
            $data['pesan'] = "Tidak ada promo";
            $data['data']  = null;
        }
        echo json_encode($data);
        
    }

    public function create_merchant_promo($id_kurir) {
        header("Content-type: application/json");
        $res = "";

        $kurirs = $this->modelmu->get_kurir($id_kurir);
        if (count($kurirs) <= 0) {

            $data = array("status"=>0);
            echo json_encode($data);
            return;
        }

        $kurir = $kurirs[0];

        // echo $json_encode($kurir);

        try {
            $this->load->helper('form');

            $json_request_body = file_get_contents('php://input');
            $request_body = json_decode($json_request_body);
            // echo json_encode($request_body); die();
            $promo_array_root = $request_body->promo;

            foreach ($promo_array_root as $promo_item) {
                $id_promo               = $promo_item->id_promo;
                $nama               = "Promo";
                $start_date        = $promo_item->tgl_mulai;
                $end_date        = $promo_item->tgl_selesai;

                $id_partner = $kurir->id_partner;

                $tipe = 1;
                if (count($promo_item->rows_byqty) > 0) {
                    $tipe = 2;
                }
                $insert_id = $this->model_merchant_promo->create_promo($nama, $id_partner, $tipe, $start_date, $end_date);


                if (count($promo_item->rows_byqty) > 0) { 
                    $rows_byqty = $promo_item->rows_byqty;
                    self::insert_produk_promo_batch($insert_id, $rows_byqty);
                } 

                if (count($promo_item->rows_byharga) > 0) {  
                    $rows_byharga = $promo_item->rows_byharga;
                    self::set_harga_promo($id_partner, $insert_id, $rows_byharga);
                }
            }

            
            $res = "Promo created successfully";
            
            $data = array("status"=>$res, "code" => 1);
            echo json_encode($data);
            
        } catch (Exception $ex) {
            $res = "failed->".$ex;
            $data = array("status"=>$res);
            echo json_encode($data);
        }
    }


    public function set_harga_promo($id_partner, $id_merchant_promo, $rows_byharga)
    {

            $header = array('barcode', 'min_qty', 'harga_promo');
            $rows = $rows_byharga;
            
            //$kurirs = $this->modelpartner->get_kurir_of_partner($id_partner);

            // echo json_encode($kurirs); die();

            //foreach ($kurirs as $kurir) {
            //    $this->modelmu->set_harga_promo_batch($kurir->id_kurir, $rows, $id_merchant_promo);
            //}
            //$this->modelmu->insert_produk_promo_diskon_batch($rows, $id_merchant_promo);
    }

    public function insert_produk_promo_batch($id_merchant_promo, $rows_byqty)
    {
        $header = array('barcode', 'min_qty', 'harga_promo');

        $this->modelmu->insert_produk_promo_batch($id_merchant_promo, $rows_byqty);

    }


   
    public function update_produks_umkm_batch($id_kurir)
    {
        $json_request_body = file_get_contents('php://input');
        // echo $json_request_body; die();
        $stocks = json_decode($json_request_body);
        // echo json_encode($stocks); die();

        $kurirs = $this->modelmu->get_kurir($id_kurir);
        $this_kurir = $kurirs[0];
        $kurir = $this->modelmu->get_first_kurir_by_partner($this_kurir->id_partner);


        $query_res = $this->modelmu->update_produks_umkm($id_kurir, $stocks);
        $results = $this->modelmu->get_produks_umkm($id_kurir, 1);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $results;
        $data['error'] = "";
        
        echo json_encode($data);
    }

    public function upload_foto_produk_batch()
    {
        $id_kurir        = $this->input->post("id_kurir");
        $foto        = $this->input->post("foto");
        $foto = str_replace("\"", "", $foto);

        // echo "foto: " . $foto; die();

        $errors = array();

        $foto3 = "";
        
        if ($foto != "") {
            $foto2 = explode(".", $foto);
            $foto3 = $id_kurir . "." . $foto2[count($foto2) - 1];
        }
        
        if ($foto3 != "") {
            $bitmap = $_FILES;
            $errors = $this->upload(PATH_IMAGE_PRODUK, $foto3, $bitmap, $id_kurir); //ini nama input type file(gambar nya)
        }

        // echo json_encode($errors); echo "\n";

        $retVal = self::unzip(PATH_IMAGE_PRODUK, $id_kurir . ".zip");
        
        $data['code']  = $retVal;
        $data['pesan'] = $retVal == "1" ? "Berhasil" : "Gagal";
        $data['data']  = [];
        $data['errors'] = $errors;
        
        echo json_encode($data);
    }

    public function update_waktu_buka_tutup()
    {
        $waktu_buka = $this->input->post("waktu_buka");
        $waktu_tutup     = $this->input->post("waktu_tutup");
        $id_kurir = $this->input->post("id_kurir");
        
        $this->modelmu->update_waktu_buka_tutup($id_kurir, $waktu_buka, $waktu_tutup);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        echo json_encode($data);
    }


    public function cari_produk() {

        header("Content-type: application/json");
        date_default_timezone_set("Asia/Jakarta");

        $q = $this->input->get("q");
        $page = $this->input->get("page");
        // echo $keyword; die();

        $produks = $this->model_api_konsumen->cari_produk($q, $page);


        $data['code']     = "1";
        $data['pesan']    = "Sukses";
        $data['data']       = $produks;
        
        echo json_encode($data);
    }


    public function tambah_toko_batch($count) {

        $id_kurirs = $this->modelmu->get_random_nums($count);
        $phones = $this->modelmu->get_random_phones($count);

        $tipe = 3;
        $kontak_sms2 = '081297910898';
        $id_partner = '';
        $password = "pin123";
        $username = "User Baru";
        $kode_sms = "1234";
        $id_provinsi = "12";
        $id_kota = "163";
        $radius = "0";
        $min_antar = "0";
        $fee = "0";

        $i = 0;
        foreach ($phones as $no_telp) {
            $id_kurir = $id_kurirs[$i]->random_num;
            $this->modelmu->tambah_toko_batch($id_kurir, $id_partner, $no_telp->random_num, $no_telp->random_num, $kontak_sms2, $password, $username, $kode_sms, $id_provinsi, $id_kota, $radius, $min_antar, $fee);

            $i++;
        }
    }

}