<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_pemilik extends CI_Controller
{


    function __construct()
    {
        parent::__construct();
        $this->load->model("mobile_v2/model_api_pemilik", "modelmu");
        $this->load->model("mobile_v2/model_api_konsumen", "model_api_konsumen");
        $this->load->model("v2/model_promo_old", "model_promo_old");
        $this->load->model("v2/model_merchant_promo", "model_merchant_promo");
        $this->load->model("v2/model_partner", "model_partner");
        $this->load->model("mobile_v2/model_api_kurir", "model_api_kurir");
    }


    public function check_session($request_headers) {

        // $id_pemilik = $request_headers["Id-pemilik"];
        // $token = $request_headers["Token"];
        // $session = $this->modelmu->check_session($id_pemilik, $token);

        // if ($session == null) {
        //     $data['code']  = "0";
        //     $data['pesan'] = "Session invalid";
        //     echo json_encode($data);
        //     die();
        // }
    }
    
    
    function kirim_kode()
    {

        $id_kode = date("YmdHis") . rand(1000, 10000);
        $kode    = rand(1000, 10000);
        $no_telp = $this->input->post("no_telp");
        $id_pemilik = $this->input->post("id_pemilik");
        
        $this->modelmu->kirim_kode($id_kode, $kode);
        
        require_once APPPATH . 'controllers/ctr_email.php';
        $myemail = new Ctr_email();
        
        $pesan_sms = "Kode SMS untuk OPA :" . $kode;
        
        //$myemail->send_email("ahmadzainuri94@gmail.com", "kirim email", $pesan_sms);
        
        $pemilik = $this->modelmu->get_id_pemilik_by_no_telp($no_telp);

        // echo $pemilik->id_pemilik . " == " . $id_pemilik; die();
        
        if ($pemilik != null) {
            if ($pemilik->id_pemilik != $id_pemilik) {
                $data['code']  = "0";
                $data['pesan'] = "Phone number already used";
                echo json_encode($data);
            } else {
                $data['code']  = "1";
                $data['id_kode'] = $id_kode;
                $data['pesan'] = "Phone number unchanged";
                echo json_encode($data);
                $myemail->send_sms($no_telp, $pesan_sms);
            }
        }  else {
            $data['id_kode'] = $id_kode;
            $data['code']    = "1"; //jika 0 maka gagal login, jika 1 sukses
            $data['pesan']   = "Sukses";
            echo json_encode($data);
            $myemail->send_sms($no_telp, $pesan_sms);
        }
        
    }

    function kirim_kode_forgot_password()
    {

        $id_kode = date("YmdHis") . rand(1000, 10000);
        $kode    = rand(1000, 10000);
        $no_telp = $this->input->post("no_telp");
        
        $this->modelmu->kirim_kode($id_kode, $kode);
        
        require_once APPPATH . 'controllers/ctr_email.php';
        $myemail = new Ctr_email();
        
        $pesan_sms = "Kode SMS untuk OPA :" . $kode;
        
        //$myemail->send_email("ahmadzainuri94@gmail.com", "kirim email", $pesan_sms);
        
        
        $status_no_telp = $this->modelmu->cek_no_telp($no_telp);

        if ($status_no_telp != "0") {
            $data['code']    = "1"; //jika 0 maka gagal login, jika 1 sukses
            $data['pesan']   = "Sukses";
            $data['id_kode'] = $id_kode;

            echo json_encode($data);
            $myemail->send_sms($no_telp, $pesan_sms);
        }  else {
            $data['id_kode'] = $id_kode;
            $data['code']    = "0"; //jika 0 maka gagal login, jika 1 sukses
            $data['pesan']   = "No HP tidak terdaftar";
            echo json_encode($data);
        }
        
    }


    function email_kode_forgot_password()
    {

        $id_kode = date("YmdHis") . rand(1000, 10000);
        $kode    = rand(1000, 10000);
        $email = $this->input->post("email");
        
        $this->modelmu->kirim_kode($id_kode, $kode);
        
        require_once APPPATH . 'controllers/ctr_email.php';
        $myemail = new Ctr_email();
        
        $pesan_sms = "OPA : Your verfication code is " . $kode;
        
            $data['code']    = "1"; //jika 0 maka gagal login, jika 1 sukses
            $data['pesan']   = "Sukses";
            $data['id_kode'] = $id_kode;

            echo json_encode($data);
            $myemail->send_email($email, "OPA Password Reset", $pesan_sms);
        
    }

    function cek_no_telp()
    {
        $no_telp = $this->input->post("no_telp");
        //$myemail->send_email("ahmadzainuri94@gmail.com", "kirim email", $pesan_sms);
        
        
        $status_no_telp = $this->modelmu->cek_no_telp($no_telp);

        if ($status_no_telp != "0") {
            $data['code']    = "0"; //jika 0 maka gagal login, jika 1 sukses
            $data['pesan']   = "No HP sudah terdaftar";

            echo json_encode($data);
        }  else {
            $data['code']    = "1"; //jika 0 maka gagal login, jika 1 sukses
            $data['pesan']   = "Nomor handphone belum terdaftar";
            echo json_encode($data);
        }
        
    }

    function cek_kode() {
        $id_kode_sms = $this->input->post("id_kode_sms");
        $kode_sms    = $this->input->post("kode_sms");

        $status_kode    = $this->modelmu->cek_kode($id_kode_sms, $kode_sms);

        $code  = "0";
        $pesan = "";

        if ($status_kode == "0") {
            $code  = "0";
            $pesan = "Kode salah";
        } else {
            $code  = "1";
            $pesan = "Sukses";
        }
        $data['code']        = $code;
        $data['pesan']       = $pesan;
        $data['kode']        = $id_kode_sms . " -> " . $kode_sms;
        
        echo json_encode($data);
    }

    // jangan sembarang panggil method ini !!!
    function edit_digit() {

        $results = $this->modelmu->get_pemilik_all();
        foreach ($results as $result) {
            $new_id = substr($result->id_pemilik, -8);
            $this->modelmu->update_id($result->id_pemilik, $new_id);
        }
        $code  = "1";
        $pesan = "Sukses";
        $data['code']        = $code;
        $data['pesan']       = $pesan;
        echo json_encode($data);
    }

    function reset_status($id_pemilik) {
        $this->modelmu->deaktifasi($id_pemilik);

        $data['code']       = '1';
        $data['pesan']      = 'Sukses';
        $data['id_pemilik'] = $id_pemilik;

        echo json_encode($data);
    }
    
    function daftar()
    {

        $id_kode_sms = $this->input->post("id_kode_sms");
        $kode_sms    = $this->input->post("kode_sms");
        $id_pemilik = $this->input->post("id_pemilik");
        
        if ($id_pemilik != null || $id_pemilik != "") {
            $id_pemilik    = $this->input->post("id_pemilik");
            $this->modelmu->aktifasi($id_pemilik);

            $code  = "1";
            $pesan = "Sukses";

        } else {
            $password    = $this->input->post("password");
            $id_provinsi = $this->input->post("id_provinsi");
            $id_kota     = $this->input->post("id_kota");
            $no_telp     = $this->input->post("no_telp");
            $radius   = $this->input->post("radius");

            $id_pemilik = $this->modelmu->get_random_num();
            $username   = "User Baru";
            
            $status_no_telp = $this->modelmu->cek_no_telp($no_telp);
            $status_kode    = $this->modelmu->cek_kode($id_kode_sms, $kode_sms);
            
            $code  = "0";
            $pesan = "";
            
            if ($status_no_telp == "0") {
                // if ($status_kode == "0") {
                //     $code  = "0";
                //     $pesan = "Kode salah";
                // } else {
                $code  = "1";
                $pesan = "Sukses";

                $id_pemilik_return = $this->modelmu->daftar($id_pemilik, $no_telp, $password, $username, $kode_sms, $id_provinsi, $id_kota, $radius);

                    // $status_daftar = "1";


                // }
                
            } else {
                $code  = "0";
                $pesan = "No telepon sudah ada";
            }
        }
        
        
        $data['code']       = $code;
        $data['pesan']      = $pesan;
        $data['id_pemilik'] = $id_pemilik;
        $data['kode']       = $id_kode_sms . " -> " . $kode_sms;
        
        echo json_encode($data);
    }

    
    function lupa_password()
    {

        $no_telp = $this->input->post("no_telp");
        $id_kode = $this->input->post("id_kode_sms");
        $kode    = $this->input->post("kode_sms");
        
        // $no_telp  = "22";
        // $id_kode  = "7777777";
        // $kode     = "11";
        
        $status_no_telp = $this->modelmu->cek_no_telp($no_telp);
        $status_kode    = $this->modelmu->cek_kode($id_kode, $kode);
        $id_pemilik     = $this->modelmu->get_id_pemilik_by_no_telp($no_telp);
        
        $code  = "0";
        $pesan = "";
        
        if ($id_pemilik == null) {
            $code  = "0";
            $pesan = "Nomor telepon tidak terdaftar!";
        } else if (intval($status_kode) == 0) {
            $code  = "0";
            $pesan = "kode salah";
        } else {
            $code  = "1";
            $pesan = "ok";
        }
        
        $data['code']       = $code;
        $data['pesan']      = $pesan;
        $data['id_pemilik'] = $id_pemilik->id_pemilik;
        
        echo json_encode($data);
    }
    

    function login()
    {
        $no_telp  = $this->input->post("no_telp");
        $password = $this->input->post("password");
        // $no_telp   = "22";
        // $password   = "tt";
        // echo $no_telp . ",  " . $password; die();
        
        $status_no_telp = $this->modelmu->cek_no_telp($no_telp);
        
        $status_login = $this->modelmu->login($no_telp, $password)->first_row();
        $pesan        = "";
        $code = "0";
        
        if ($status_no_telp != "0") {

            // echo $status_login; die();

            if ($status_login != null) {

                if ($status_login->aktif == 0) {
                    $code = "2";
                    $pesan = "Daftar dulu";
                } else {
                    $code = "1";
                    $pesan = "Sukses";
                }
                
            } else if ($status_login == null) {

                $pesan = "Kata sandi salah!";
                
            }
            
        } else {

            $pesan = "Nomor telepon tidak terdaftar!";
            
        }
        
        
        
        $data['code']  = $code; //jika 0 maka gagal login, jika 1 sukses
        $data['pesan'] = $pesan;
        $data['data']  = $this->modelmu->login($no_telp, $password)->result();
        
        echo json_encode($data);
    }
    
    public function insert_lokasi_usaha()
    {
        // $this->check_session($this->input->request_headers());
        $id_pemilik   = $this->input->post("id_pemilik");
        $jenis_lokasi = $this->input->post("jenis_lokasi");
        $id_lokasi    = date("YmdHis") . $id_pemilik;
        
        $this->modelmu->insert_lokasi_usaha($id_pemilik, $id_lokasi, $jenis_lokasi);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->modelmu->get_lokasi_usaha($id_pemilik)->result();
        
        echo json_encode($data);
    }
    
    public function update_nama_lokasi_usaha()
    {
        // $this->check_session($this->input->request_headers());
        $id_pemilik  = $this->input->post("id_pemilik");
        $nama_lokasi = $this->input->post("nama_lokasi");
        $id_lokasi   = $this->input->post("id_lokasi");
        
        $this->modelmu->update_nama_lokasi_usaha($id_lokasi, $nama_lokasi);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->modelmu->get_lokasi_usaha($id_pemilik)->result();
        
        echo json_encode($data);
    }

    
    public function update_min_antar()
    {
        // $this->check_session($this->input->request_headers());
        $id_pemilik  = $this->input->post("id_pemilik");
        $min_antar = $this->input->post("min_antar");
        
        $this->modelmu->update_min_antar($id_pemilik, $min_antar);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->modelmu->get_lokasi_usaha($id_pemilik)->result();
        
        echo json_encode($data);
    }

    public function update_alamat_usaha()
    {
        $this->check_session($this->input->request_headers());
        $id_pemilik  = $this->input->post("id_pemilik");
        $alamat = $this->input->post("alamat");
        $this->modelmu->update_alamat_usaha($id_pemilik, $alamat);
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = new stdClass();
        
        echo json_encode($data);
    }

    
    public function update_lokasi_usaha()
    {
        $this->check_session($this->input->request_headers());
        $id_pemilik  = $this->input->post("id_pemilik");
        $lat         = $this->input->post("lat");
        $lng         = $this->input->post("lng");
        $id_lokasi   = $this->input->post("id_lokasi");
        $nama_lokasi = $this->input->post("nama_lokasi");
        
        // $id_pemilik   = "1605101100457770";
        // $lat = "-8.030870539137213";
        // $lng = "112.65950795263666";
        // $id_lokasi = "2315412354";
        $this->modelmu->update_lokasi_usaha($id_lokasi, $nama_lokasi, $lat, $lng);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->modelmu->get_lokasi_usaha($id_pemilik)->result();
        
        echo json_encode($data);
    }
    
    public function get_kategori_produk()
    {
        // $this->check_session($this->input->request_headers());

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->modelmu->get_kategori_produk();
        
        echo json_encode($data);
    }

    public function get_kategori_produk_pemilik()
    {
        // $this->check_session($this->input->request_headers());
        $id_pemilik = $this->input->post("id_pemilik");
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->modelmu->get_kategori_produk_pemilik($id_pemilik);
        
        echo json_encode($data);
    }
    
    public function insert_kategori_produk()
    {
        // $this->check_session($this->input->request_headers());
        $nama_kategori = $this->input->post("nama_kategori");
        $id_pemilik    = $this->input->post("id_pemilik");
        //echo "----".$id_pemilik."------";
        $this->modelmu->insert_kategori_produk($nama_kategori, $id_pemilik);
        
        $data['code']        = "1";
        $data['pesan']       = "Berhasil";
        $data['data']        = $this->modelmu->get_pemilik_produk($id_pemilik);
        $data['id_kategori'] = $this->modelmu->get_kategori_produk_by_nama($nama_kategori, $id_pemilik);
        
        echo json_encode($data);
    }

    public function insert_produk()
    {
        // $this->check_session($this->input->request_headers());
        $min_antar = "1";
        
        $id_pemilik  = $this->input->post("id_pemilik");
        $id_produk   = date("ymdHis") . $id_pemilik . rand(100, 1000);
        $nama        = $this->input->post("nama");
        $harga       = $this->input->post("harga");
        $harga_promo       = $this->input->post("harga_promo");
        $stok        = $this->input->post("stok");
        $min_antar        = $this->input->post("min_antar");
        $keterangan  = $this->input->post("keterangan");
        $id_kategori = 0;
        $ket_promo = $this->input->post("ket_promo");

        $return = array();

        if (intval($id_kategori) == 0) {
            $nama_kategori = $this->input->post("nama_kategori");
            $id_kategori = $this->modelmu->insert_kategori_produk($nama_kategori, $id_pemilik, $min_antar);
        }
        
        $foto        = $this->input->post("foto");
        $foto = str_replace("\"", "", $foto);

        $this->modelmu->insert_produk($id_produk, $id_pemilik, $nama, $harga, $harga_promo, $stok, $keterangan, $id_kategori, $min_antar, $ket_promo);

            $errors = array();

            $foto3 = "";
            
            if ($foto != "") {
                $foto2 = explode(".", $foto);
                $foto3 = $id_produk . "." . $foto2[count($foto2) - 1];
            }
            
            if ($foto3 != "") {
                $bitmap = $_FILES;
                $return = $this->upload(PATH_IMAGE_PRODUK, $foto3, $bitmap, $id_produk); //ini nama input type file(gambar nya)
            }
            
            $this->modelmu->set_foto_produk($id_produk, $foto3, "0");

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = array_reverse($this->modelmu->get_pemilik_produk($id_pemilik));
        $data['errors']  = $return;
        echo json_encode($data);
    }


    public function insert_produk_merchant_batch($id_partner)
    {
        // $this->check_session($this->input->request_headers());
        $this->load->library('excel');

        $min_antar = "1";
        $produks = array();

        $overwrite = $this->input->post('overwrite');
        $file = $_FILES;
        $return = $this->upload_xls(PATH_XLSX_PRODUK, $id_partner, $file, $id_partner);

        $inputFileName = PATH_XLSX_PRODUK . $return;


        //  Read your Excel workbook
        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch(Exception $e) {
            die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
        }

        //  Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0); 
        $highestRow = $sheet->getHighestRow(); 
        $highestColumn = $sheet->getHighestColumn();

        $header = array();
        $rowData = $sheet->rangeToArray('A' . "1" . ':' . $highestColumn . "1",
            NULL,
            TRUE,
            FALSE);
        $header = $rowData[0];
        $rows = array();
        $k = 0;
        $id_kategori_pos = 0;
        $harga_pos = 0;
        $harga_promo_pos = 0;
        $stok_pos = 0;
        foreach ($header as $header_col) {
            if ($header_col == 'id_kategori') {
                $id_kategori_pos = $k;
            } else if ($header_col == 'harga') {
                $harga_pos = $k;
            } if ($header_col == 'harga_promo') {
                $harga_promo_pos = $k;
            } if ($header_col == 'stok') {
                $stok_pos = $k;
            } 
            $k++;
        }

        $id_kategories = array();

        for ($row = 2; $row <= $highestRow; $row++) { 
            //  Read a row of data into an array
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                NULL,
                TRUE,
                FALSE);

            $nama_kategori = $rowData[0][$id_kategori_pos];

            if (!array_key_exists($nama_kategori, $id_kategories)) {
                // die();
                $id_kategori = $this->modelmu->get_kategori_by_nama_kategori($nama_kategori);

                if (intval($id_kategori) == 0) {
                    $id_kategori = $this->modelmu->insert_kategori_produk($nama_kategori, $id_partner, $min_antar);
                    $id_kategories[$nama_kategori] = $id_kategori;
                }

                $id_kategories[$nama_kategori] = $id_kategori;
            }
            
            $rowData[0][$id_kategori_pos] = $id_kategories[$nama_kategori];
            $rowData[0][$harga_pos] = $rowData[0][$harga_pos] == '' ? '0' : str_replace(",", "", $rowData[0][$harga_pos]);
            $rowData[0][$harga_promo_pos] = $rowData[0][$harga_promo_pos] == '' ? '0' : str_replace(",", "", $rowData[0][$harga_promo_pos]);
            $rowData[0][$stok_pos] = $rowData[0][$stok_pos] == '' ? '0' : str_replace(",", "", $rowData[0][$stok_pos]);
            $rows[] = $rowData[0];
        }


        $pemiliks = $this->modelmu->get_pemilik_by_partner($id_partner)->result();
        if ($overwrite == 1) {
            foreach ($pemiliks as $pemilik) {
                $this->modelmu->delete_produk_by_pemilik($pemilik->id_pemilik);
            }
        }

        // echo json_encode($pemiliks); die();

        foreach ($pemiliks as $pemilik) {
            $this->modelmu->insert_produk_batch($pemilik->id_pemilik, $header, $rows);
        }

        $this->model_partner->set_produk_filename($pemilik->id_partner, $return);

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $return;
        echo json_encode($data);
    }


    public function delete_produks_by_pemilik() {

        $id_pemilik = $this->input->post('id_pemilik');
        $produks_json  = $this->input->post('produks');
        $produks = json_decode($produks_json);

        $id_produks = "";
        $id_produks_umkm = "";

        foreach ($produks as $produk) {
            if (intval($produk->is_umkm) == 1) {
                $id_produks_umkm = $id_produks_umkm . "'" . $produk->id_produk . "', ";
            } else {
                $id_produks = $id_produks . "'" . $produk->id_produk . "', ";
            }
        }

        // echo $id_produks; die();
                
        if ($id_produks != "") {
            $id_produks = $id_produks . "HEHEHE1234";
            $id_produks = str_replace(", HEHEHE1234", "", $id_produks);

            $this->modelmu->copy_produks_pemilik_to_histori($id_produks);
            $this->modelmu->delete_produks_by_pemilik($id_pemilik, $id_produks);
        }

        if ($id_produks_umkm != "") {
            $id_produks_umkm = $id_produks_umkm . "HEHEHE1234";
            $id_produks_umkm = str_replace(", HEHEHE1234", "", $id_produks_umkm);

            $this->modelmu->copy_produks_umkm_pemilik_to_histori($id_produks_umkm);
            $this->modelmu->delete_produks_umkm_by_pemilik($id_pemilik, $id_produks_umkm);
        }


        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = array();

        echo json_encode($data);

    }


    public function insert_produk_siap_antar_batch($id_pemilik)
    {
        // $this->check_session($this->input->request_headers());
        $this->load->library('excel');

        $min_antar = "1";
        $produks = array();

        $file = $_FILES;
        $return = $this->upload_xls(PATH_XLSX_PRODUK, $id_pemilik, $file, $id_pemilik);

        $inputFileName = PATH_XLSX_PRODUK . $return;


        //  Read your Excel workbook
        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch(Exception $e) {
            die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
        }

        //  Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0); 
        $highestRow = $sheet->getHighestRow(); 
        $highestColumn = $sheet->getHighestColumn();

        $header = array();
        $rowData = $sheet->rangeToArray('A' . "1" . ':' . $highestColumn . "1",
            NULL,
            TRUE,
            FALSE);
        $header = $rowData[0];
        $rows = array();
        $k = 0;

        for ($row = 2; $row <= $highestRow; $row++) { 
            //  Read a row of data into an array
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                NULL,
                TRUE,
                FALSE);

            $rows[] = $rowData[0];
        }


        // echo json_encode($pemiliks); die();
        $this->modelmu->set_produk_siap_antar_batch($id_pemilik, $header, $rows);
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $return;
        echo json_encode($data);
    }


    public function insert_produk_batch($id_pemilik)
    {
        // echo "ok"; die();
        // $this->check_session($this->input->request_headers());
        $this->load->library('excel');

        $min_antar = "1";
        $produks = array();

        $file = $_FILES;
        $return = $this->upload_xls(PATH_XLSX_PRODUK, $id_pemilik, $file, $id_pemilik);

        $inputFileName = PATH_XLSX_PRODUK . $return;




        //  Read your Excel workbook
        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch(Exception $e) {
            die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
        }

        //  Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0); 
        $highestRow = $sheet->getHighestRow(); 
        $highestColumn = $sheet->getHighestColumn();

        $header = array();
        $rowData = $sheet->rangeToArray('A' . "1" . ':' . $highestColumn . "1",
            NULL,
            TRUE,
            FALSE);
        $header = $rowData[0];
        $rows = array();
        $k = 0;

        for ($row = 2; $row <= $highestRow; $row++) { 
            //  Read a row of data into an array
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                NULL,
                TRUE,
                FALSE);

            $rows[] = $rowData[0];
        }


        // echo json_encode($pemiliks); die();
        // $this->modelmu->set_produk_batch($id_pemilik, $header, $rows);
        $this->modelmu->insert_produk_batch($id_pemilik, $header, $rows, 1);
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $return;
        echo json_encode($data);
    }


    public function insert_produk_pemilik_batch($id_pemilik)
    {
        // $this->check_session($this->input->request_headers());

        $this->load->library('excel');

        $min_antar = "1";
        $produks = array();

        $overwrite = $this->input->post('overwrite');
        $file = $_FILES;
        $return = $this->upload_xls(PATH_XLSX_PRODUK, $id_pemilik, $file, $id_pemilik);

        $inputFileName = PATH_XLSX_PRODUK . $return;


        //  Read your Excel workbook
        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch(Exception $e) {
            die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
        }

        //  Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0); 
        $highestRow = $sheet->getHighestRow(); 
        $highestColumn = $sheet->getHighestColumn();

        $header = array();
        $rowData = $sheet->rangeToArray('A' . "1" . ':' . $highestColumn . "1",
            NULL,
            TRUE,
            FALSE);
        $header = $rowData[0];
        $rows = array();
        $k = 0;
        $id_kategori_pos = 0;
        $harga_pos = 0;
        $harga_promo_pos = 0;
        $stok_pos = 0;
        foreach ($header as $header_col) {
            if ($header_col == 'id_kategori') {
                $id_kategori_pos = $k;
            } else if ($header_col == 'harga') {
                $harga_pos = $k;
            } if ($header_col == 'harga_promo') {
                $harga_promo_pos = $k;
            } if ($header_col == 'stok') {
                $stok_pos = $k;
            } 
            $k++;
        }

        $id_kategories = array();

        for ($row = 2; $row <= $highestRow; $row++){ 
            //  Read a row of data into an array
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                NULL,
                TRUE,
                FALSE);

            $nama_kategori = $rowData[0][$id_kategori_pos];

            if (!array_key_exists($nama_kategori, $id_kategories)) {
                $id_kategori = $this->modelmu->get_kategori_by_nama_kategori($nama_kategori);

                if (intval($id_kategori) == 0) {
                    $id_kategori = $this->modelmu->insert_kategori_produk($nama_kategori, $id_pemilik, $min_antar);
                    $id_kategories[$nama_kategori] = $id_kategori;
                }

                $id_kategories[$nama_kategori] = $id_kategori;
            }
            
            $rowData[0][$id_kategori_pos] = $id_kategories[$nama_kategori];
            $rowData[0][$harga_pos] = $rowData[0][$harga_pos] == '' ? '0' : str_replace(",", "", $rowData[0][$harga_pos]);
            $rowData[0][$harga_promo_pos] = $rowData[0][$harga_promo_pos] == '' ? '0' : str_replace(",", "", $rowData[0][$harga_promo_pos]);
            $rowData[0][$stok_pos] = $rowData[0][$stok_pos] == '' ? '0' : str_replace(",", "", $rowData[0][$stok_pos]);
            $rows[] = $rowData[0];
        }

        if ($overwrite == 1) {
            $this->modelmu->delete_produk_by_pemilik($id_pemilik);
        }

        // echo json_encode($pemiliks); die();

        $this->modelmu->insert_produk_batch($id_pemilik, $header, $rows);

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $return;
        echo json_encode($data);
    }
    
    public function update_produk()
    {
        $this->check_session($this->input->request_headers());

        $min_antar = "1";

        $id_pemilik = $this->input->post("id_pemilik");
        $id_produk  = $this->input->post("id_produk");
        $id_kategori  = $this->input->post("id_kategori");
        $nama       = $this->input->post("nama");
        $harga      = $this->input->post("harga");
        $harga_promo      = $this->input->post("harga_promo");
        $ket_promo      = $this->input->post("ket_promo");
        $stok       = $this->input->post("stok");
        $keterangan = $this->input->post("keterangan");
        $min_antar        = $this->input->post("min_antar");

        if (intval($id_kategori) == 0) {
            $nama_kategori = $this->input->post("nama_kategori");
            $id_kategori = $this->modelmu->insert_kategori_produk($nama_kategori, $id_pemilik, $min_antar);
        }
        
        $this->modelmu->update_produk($id_produk, $id_kategori, $nama, $harga, $harga_promo, $stok, $keterangan, $min_antar, $ket_promo);

        $foto        = $this->input->post("foto");
        $foto = str_replace("\"", "", $foto);

        // echo "foto: " . $foto; die();

        $errors = array();

        $foto_obj = $this->modelmu->get_foto_produk($id_produk);

        $foto3 = "";
        
        if ($foto != "") {
            $foto2 = explode(".", $foto);
            $foto3 = $id_produk . "" . (intval($foto_obj->versi) + 1) . "." . $foto2[count($foto2) - 1];
        }
        
        // echo "foto3: " . $foto3; die();

        if ($foto3 != "") {
            $bitmap = $_FILES;
            $return = $this->upload(PATH_IMAGE_PRODUK, $foto3, $bitmap, $id_produk . "" . (intval($foto_obj->versi) + 1)); //ini nama input type file(gambar nya)
            $this->modelmu->set_foto_produk($id_produk, $foto3, "0");
        }
        
        $data['code']  = "1";
        $data['pesan'] = "";
        $data['data']  = array_reverse($this->modelmu->get_pemilik_produk($id_pemilik));
        
        echo json_encode($data);
    }

    public function update_produk_umkm()
    {
        $this->check_session($this->input->request_headers());
        $min_antar = "1";

        $id_pemilik = $this->input->post("id_pemilik");
        $id_produk  = $this->input->post("id_produk");
        $id_kategori  = $this->input->post("id_kategori");
        $nama       = $this->input->post("nama");
        $harga      = $this->input->post("harga");
        $harga_promo      = $this->input->post("harga_promo");
        $ket_promo      = $this->input->post("ket_promo");
        $stok       = $this->input->post("stok");
        $keterangan = $this->input->post("keterangan");
        $min_antar        = $this->input->post("min_antar");

        if (intval($id_kategori) == 0) {
            $nama_kategori = $this->input->post("nama_kategori");
            $id_kategori = $this->modelmu->insert_kategori_produk($nama_kategori, $id_pemilik, $min_antar);
        }
        
        $this->modelmu->update_produk_umkm($id_produk, $id_kategori, $nama, $harga, $harga_promo, $stok, $keterangan, $min_antar, $ket_promo);

        $foto        = $this->input->post("foto");
        $foto = str_replace("\"", "", $foto);

        // echo "foto: " . $foto; die();

        $errors = array();

        $foto3 = "";
        
        if ($foto != "") {
            $foto2 = explode(".", $foto);
            $foto3 = $id_produk . "." . $foto2[count($foto2) - 1];
        }
        
        if ($foto3 != "") {
            $bitmap = $_FILES;
            $return = $this->upload(PATH_IMAGE_PRODUK, $foto3, $bitmap, $id_produk); //ini nama input type file(gambar nya)
            $this->modelmu->set_foto_produk($id_produk, $foto3, "1");
        }
        

        
        $data['code']  = "1";
        $data['pesan'] = "Sukses";
        $data['data']  = $this->modelmu->get_produks_umkm($id_produk, 1);
        
        echo json_encode($data);
    }
    
    public function update_stok()
    {
        // $this->check_session($this->input->request_headers());

        $id_pemilik = $this->input->post("id_pemilik");
        $id_produk  = $this->input->post("id_produk");
        $stok       = $this->input->post("stok");
        
        $this->modelmu->update_stok($id_produk, $stok);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->modelmu->get_pemilik_produk($id_pemilik);
        
        echo json_encode($data);
    }


    public function update_stok_batch($id_pemilik)
    {
        // $this->check_session($this->input->request_headers());

        $json_request_body = file_get_contents('php://input');
        log_message('info','payload\n\n '.$json_request_body);
        $stocks = json_decode($json_request_body);

        $pemiliks = $this->modelmu->get_pemilik($id_pemilik);
        $this_pemilik = $pemiliks[0];
        $pemilik = $this->modelmu->get_first_pemilik_by_partner($this_pemilik->id_partner);

        $product_count = $this->modelmu->count_products($id_pemilik);

        // echo $product_count['total'] . " -- id_pemilik_old " . $pemilik->id_pemilik; die();
        if (intval($product_count['total']) <= 0) { 
            $this->modelmu->copy_products($pemilik->id_pemilik, $id_pemilik);
        }

        $query_res = $this->modelmu->update_stocks($id_pemilik, $stocks);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = array();
        $data['error'] = "";
        
        echo json_encode($data);
    }
    
    public function delete_produk()
    {
        $this->check_session($this->input->request_headers());

        $id_pemilik = $this->input->post("id_pemilik");
        $is_umkm = $this->input->post("is_umkm");
        $id_produk  = $this->input->post("id_produk");
        
        $this->modelmu->copy_produk_to_histori($id_produk, $is_umkm);
        $this->modelmu->delete_produk($id_produk, $is_umkm);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = array();
        
        echo json_encode($data);
    }
    
    public function get_order()
    {
        // $this->check_session($this->input->request_headers());

        $id_pemilik = $this->input->get("id_pemilik");
        
        $data['data']  = array_reverse($this->modelmu->get_order($id_pemilik));
        $data['code']  = "1";
        $data['pesan'] = "sukses";
        
        echo json_encode($data);
        
    }


    public function get_orders()
    {
        $this->check_session($this->input->request_headers());

        $id_pemilik = $this->input->get("id_pemilik");
        
        $data['data']  = array_reverse($this->modelmu->get_order($id_pemilik));
        $data['code']  = "1";
        $data['pesan'] = "sukses";
        
        echo json_encode($data);
        
    }

    // public function get_undone_order()
    // {
    //     // $this->check_session($this->input->request_headers());

    //     $id_pemilik = $this->input->get("id_pemilik");
        
    //     $produks = $this->modelmu->get_undone_order($id_pemilik);
    //     $orders = array();
    //     $order = new stdClass();
    //     $items = array();
    //     $prev_id_order = '';

    //     // echo json_encode($produks); die();

    //     if (count($produks) > 0) {
    //         $produk = $produks[0];
            
    //         $order = new stdClass();
    //         $order->id_order = $produk->id_order;
    //         $order->status = $produk->status;
    //         $order->tanggal_order = $produk->tanggal_order;
    //         $order->id_konsumen = $produk->id_konsumen;
    //         $order->nama = $produk->nama;
    //         $order->alamat_antar = $produk->alamat_antar;
    //         $order->ket_alamat = $produk->ket_alamat;
    //         $order->fee = $produk->fee;
    //         $order->total = $produk->total;
    //         $prev_id_order = $produk->id_order;

    //         unset($produk->id_order);
    //         unset($produk->status);
    //         unset($produk->tanggal_order);
    //         unset($produk->tanggal_proses);
    //         unset($produk->id_konsumen);
    //         unset($produk->nama);
    //         unset($produk->kontak);
    //         unset($produk->alamat_antar);
    //         unset($produk->alamat);
    //         unset($produk->lng_antar);
    //         unset($produk->lat_antar);
    //         unset($produk->lat);
    //         unset($produk->lng);
    //         unset($produk->versi);
    //         unset($produk->nama_kota);
    //         unset($produk->id_perkiraan_antar);
    //         unset($produk->ket_perkiraan_antar);
    //         unset($produk->ket_alamat);
    //         unset($produk->jenis_alamat);
    //         unset($produk->fee);
    //         unset($produk->total);
    //         $items[] = $produk;
    //         $order->items = $items;
    //         $orders[] = $order;
    //     }

    //     for ($i = 1; $i < count($produks); $i++) {
    //         $produk = $produks[$i];

    //         if ($produk->id_order != $prev_id_order) {
    //             $order->items = $items;
    //             $orders[] = $order;
    //             $order = new stdClass();
    //             $order->id_order = $produk->id_order;
    //             $order->status = $produk->status;
    //             $order->tanggal_order = $produk->tanggal_order;
    //             $order->id_konsumen = $produk->id_konsumen;
    //             $order->nama = $produk->nama;
    //             $order->alamat_antar = $produk->alamat_antar;
    //             $order->ket_alamat = $produk->ket_alamat;
    //             $order->fee = $produk->fee;
    //             $order->total = $produk->total;
    //             $items = array();
    //         }

    //         $prev_id_order = $produk->id_order;
    //         unset($produk->id_order);
    //         unset($produk->status);
    //         unset($produk->tanggal_order);
    //         unset($produk->tanggal_proses);
    //         unset($produk->id_konsumen);
    //         unset($produk->nama);
    //         unset($produk->kontak);
    //         unset($produk->alamat_antar);
    //         unset($produk->alamat);
    //         unset($produk->lng_antar);
    //         unset($produk->lat_antar);
    //         unset($produk->lat);
    //         unset($produk->lng);
    //         unset($produk->versi);
    //         unset($produk->nama_kota);
    //         unset($produk->id_perkiraan_antar);
    //         unset($produk->ket_perkiraan_antar);
    //         unset($produk->jenis_alamat);
    //         unset($produk->ket_alamat);
    //         unset($produk->fee);
    //         unset($produk->total);
    //         $items[] = $produk;
    //         $order->items = $items;
    //         // $orders[] = $order;
    //     }

    //     if (count($orders) > 1) {
    //         array_splice($orders, 0, 1);
    //     }

    //     $data['data']  = $orders;
    //     $data['code']  = "1";
    //     $data['pesan'] = "sukses";
        
    //     echo json_encode($data);
        
    // }


    public function get_undone_order()
    {
        // $this->check_session($this->input->request_headers());

        $id_pemilik = $this->input->get("id_pemilik");
        
        $orders = $this->modelmu->get_undone_order($id_pemilik);
        

        $data['data']  = array_reverse($orders);
        $data['code']  = "1";
        $data['pesan'] = "sukses";
        
        echo json_encode($data);
        
    }

    public function get_done_order()
    {
        // $this->check_session($this->input->request_headers());

        $id_pemilik = $this->input->get("id_pemilik");
        
        $orders = $this->modelmu->get_done_order($id_pemilik);
        

        $data['data']  = array_reverse($orders);
        $data['code']  = "1";
        $data['pesan'] = "sukses";
        
        echo json_encode($data);
        
    }
    
    public function buka_toko()
    {
        $this->check_session($this->input->request_headers());

        $id_pemilik = $this->input->post("id_pemilik");
        $status     = $this->input->post("status");


        $info = $this->modelmu->get_pemilik($id_pemilik);

        if (time() >= strtotime($info[0]->waktu_buka) && time() < strtotime($info[0]->waktu_tutup)) {        
            $this->modelmu->buka_toko($id_pemilik, $status, 1);
        } else {      
            $this->modelmu->buka_toko($id_pemilik, $status, 0);
        }
        
        $data['code']  = "1";
        $data['pesan'] = "sukses";
        
        echo json_encode($data);
        
    }

    public function get_status_toko()
    {
        // $this->check_session($this->input->request_headers());

        $id_pemilik = $this->input->post("id_pemilik");


        $info = $this->modelmu->get_pemilik($id_pemilik);

        if (time() >= strtotime($info[0]->waktu_buka) && time() < strtotime($info[0]->waktu_tutup)) {        
            $this->modelmu->buka_toko($id_pemilik, $status, 1);
        } else {      
            $this->modelmu->buka_toko($id_pemilik, $status, 0);
        }
        
        $data['code']  = "1";
        $data['pesan'] = "sukses";
        
        echo json_encode($data);
        
    }
    
    public function update_no_telp_kuber()
    {
        // $this->check_session($this->input->request_headers());

        $id_pemilik = $this->input->post("id_pemilik");
        $kontak_sms2  = $this->input->post("no_telp_kurir_bersama");


        $kurir = $this->model_api_kurir->get_id_kurir_by_no_telp($kontak_sms2);

        $data['code']  = "1";
        $data['pesan'] = "sukses";

        if ($kurir == null) {
            $data['code']  = "0";
            $data['pesan'] = "Kurir not found";
        } else {
            $data['code']  = "1";
            $data['pesan'] = "sukses";
            $this->modelmu->update_no_telp_kuber($id_pemilik, $kontak_sms2);
        }
        
        echo json_encode($data);
        
    }


    public function update_kontak_n_password()
    {
        // $this->check_session($this->input->request_headers());

        $id_pemilik = $this->input->post("id_pemilik");
        $no_telp  = $this->input->post("no_telp");
        $password  = $this->input->post("password");

        $pemilik = $this->modelmu->get_id_pemilik_by_no_telp($no_telp);

        $data['code']  = "1";
        $data['pesan'] = "sukses";

        if ($pemilik != null) {
            if ($pemilik->id_pemilik != $id_pemilik) {
                $data['code']  = "0";
                $data['pesan'] = "Phone number already used";
            } else {
                $data['code']  = "1";
                $data['pesan'] = "Phone number unchanged";
            }
        } else {
            $data['code']  = "1";
            $data['pesan'] = "sukses";
            $this->modelmu->update_kontak_n_password($id_pemilik, $no_telp, $password);
        }
        
        echo json_encode($data);
        
    }

    public function update_nama_toko()
    {
        // $this->check_session($this->input->request_headers());

        $id_pemilik = $this->input->post("id_pemilik");
        $nama       = $this->input->post("nama");
        
        $this->modelmu->update_nama_toko($id_pemilik, $nama);
        
        $data['code']  = "1";
        $data['pesan'] = "sukses";
        
        echo json_encode($data);
        
    }

    public function update_no_telp_usaha()
    {
        // $this->check_session($this->input->request_headers());

        $id_pemilik = $this->input->post("id_pemilik");
        $no_telp  = $this->input->post("no_telp");

        $pemilik = $this->modelmu->get_id_pemilik_by_no_telp($no_telp);

        $data['code']  = "1";
        $data['pesan'] = "sukses";

        if ($pemilik != null) {
            if ($pemilik->id_pemilik != $id_pemilik) {
                $data['code']  = "0";
                $data['pesan'] = "Phone number already used";
            } else {
                $data['code']  = "1";
                $data['pesan'] = "Phone number unchanged";
            }
        } else {
            $data['code']  = "1";
            $data['pesan'] = "sukses";
            $this->modelmu->update_no_telp_usaha($id_pemilik, $no_telp);
        }
        
        echo json_encode($data);
        
    }


    public function update_no_telp_notifikasi()
    {
        // $this->check_session($this->input->request_headers());

        $id_pemilik = $this->input->post("id_pemilik");
        $no_telp  = $this->input->post("no_telp");

        // $pemilik = $this->modelmu->get_id_pemilik_by_no_telp($no_telp);

        // $data['code']  = "1";
        // $data['pesan'] = "sukses";

        // if ($pemilik != null) {
        //     if ($pemilik->id_pemilik != $id_pemilik) {
        //         $data['code']  = "0";
        //         $data['pesan'] = "Phone number already used";
        //     } else {
        //         $data['code']  = "1";
        //         $data['pesan'] = "Phone number unchanged";
        //     }
        // } else {
            $data['code']  = "1";
            $data['pesan'] = "sukses";
            $this->modelmu->update_no_telp_notifikasi($id_pemilik, $no_telp);
        // }
        
        echo json_encode($data);
        
    }
    
    public function update_jargon_toko()
    {

        // $this->check_session($this->input->request_headers());

        $id_pemilik = $this->input->post("id_pemilik");
        $jargon     = $this->input->post("jargon");
        
        $this->modelmu->update_jargon_toko($id_pemilik, $jargon);
        
        $data['code']  = "1";
        $data['pesan'] = "sukses";
        
        echo json_encode($data);
        
    }

    public function update_radius_toko()
    {
        // $this->check_session($this->input->request_headers());

        $id_pemilik = $this->input->post("id_pemilik");
        $radius     = $this->input->post("radius");
        
        $this->modelmu->update_radius_toko($id_pemilik, $radius);
        
        $data['code']  = "1";
        $data['pesan'] = "sukses";
        
        echo json_encode($data);
        
    }


    public function set_area_layanan()
    {
        // $this->check_session($this->input->request_headers());

        $id_pemilik = $this->input->post("id_pemilik");
        $radius     = $this->input->post("radius");
        $lat     = $this->input->post("lat");
        $lng     = $this->input->post("lng");
        
        $this->modelmu->set_area_layanan($id_pemilik, $lat, $lng, $radius);
        
        $data['code']  = "1";
        $data['pesan'] = "sukses";
        
        echo json_encode($data);
        
    }
    
    public function update_kategori_usaha()
    {
        // $this->check_session($this->input->request_headers());

        $id_pemilik = $this->input->post("id_pemilik");
        $json       = $this->input->post("json");
        
        $this->modelmu->update_kategori_usaha($id_pemilik, $json);
        
        $data['code']  = "1";
        $data['pesan'] = "sukses";
        $data['data']  = $this->modelmu->get_pemilik_kategori($id_pemilik);
        echo json_encode($data);
        
    }
    
    public function update_kategori_usaha_awal()
    {
        // $this->check_session($this->input->request_headers());

        $id_pemilik = $this->input->post("id_pemilik");
        $json       = $this->input->post("json");
        
        $this->modelmu->update_kategori_usaha_awal($id_pemilik, $json);
        
        $data['code']  = "1";
        $data['pesan'] = "sukses";
        $data['data']  = $this->modelmu->get_pemilik_kategori($id_pemilik);
        echo json_encode($data);
        
    }
    
    public function get_jenis_kategori_usaha()
    {
        $this->check_session($this->input->request_headers());

        $data['data']  = $this->modelmu->get_jenis_kategori_usaha();
        $data['code']  = "1";
        $data['pesan'] = "sukses";
        
        echo json_encode($data);
        
    }
    
    public function get_detail_order()
    {
        $this->check_session($this->input->request_headers());

        $id_order = $this->input->get("id_order");
        
        $data['data']['order_info']  = $this->modelmu->get_order_dari_notif($id_order);
        $data['data']['orders']  = $this->modelmu->get_detail_order($id_order);
        $orders_promo  = $this->modelmu->get_detail_order_promo($id_order);
        $konsumen = $this->modelmu->notif_get_konsumen($id_order);

        if (count($konsumen) == 0) {
            $data['code']  = "0";
            $data['pesan'] = "Konsumen telah dihapus";
            
            echo json_encode($data);
            return;
        }

        $data['data']['orders'] = array_merge($data['data']['orders'], $orders_promo);
        $merged = $konsumen; 
        if (count($data['data']['orders']) > 0) {
            // echo "sini"; die();
            $first_order = $data['data']['orders'][0];
            $ket_alamat = $first_order->ket_alamat;
            $konsumen_array = (array)$konsumen[0];
            $new_array = array( 'ket_alamat' => $ket_alamat);
            $merged = array_merge($konsumen_array, $new_array);
        }

        // $obj = new stdClass();
        // if (count($merged) > 0) {
        //     // $obj = $merged[0];
        // }
        $data['data']['konsumen'] = $merged;
        $data['code']  = "1";
        $data['pesan'] = "sukses";
        
        echo json_encode($data);
        
    }
    
    public function get_order_detail()
    {
        $this->check_session($this->input->request_headers());

        $id_order = $this->input->get("id_order");
        
        $data['data']['order_info']  = $this->modelmu->get_order_dari_notif($id_order);
        $data['data']['orders']  = $this->modelmu->get_detail_order($id_order);
        $orders_promo  = $this->modelmu->get_detail_order_promo($id_order);
        $konsumen = $this->modelmu->notif_get_konsumen($id_order);

        if (count($konsumen) == 0) {
            $data['code']  = "0";
            $data['pesan'] = "Konsumen telah dihapus";
            
            echo json_encode($data);
            return;
        }

        $data['data']['orders'] = array_merge($data['data']['orders'], $orders_promo);
        $merged = $konsumen; 
        if (count($data['data']['orders']) > 0) {
            // echo "sini"; die();
            $first_order = $data['data']['orders'][0];
            $ket_alamat = $first_order->ket_alamat;
            $konsumen_array = (array)$konsumen[0];
            $new_array = array( 'ket_alamat' => $ket_alamat);
            $merged = array_merge($konsumen_array, $new_array);
        }

        // $obj = new stdClass();
        // if (count($merged) > 0) {
        //     // $obj = $merged[0];
        // }
        $data['data']['konsumen'] = $merged;
        $data['code']  = "1";
        $data['pesan'] = "sukses";
        
        echo json_encode($data);
        
    }
    
    public function get_perkiraan_antar()
    {        // $this->check_session($this->input->request_headers());

        $id_perkiraan_antar = $this->input->get("id_perkiraan_antar");
        
        $data['data']  = $this->modelmu->get_perkiraan_antar($id_perkiraan_antar);
        $data['code']  = "1";
        $data['pesan'] = "sukses";
        
        echo json_encode($data);
        
    }
    
    public function get_all_perkiraan_antar()
    {
        // $this->check_session($this->input->request_headers());

        $data['data']  = $this->modelmu->get_all_perkiraan_antar();
        $data['code']  = "1";
        $data['pesan'] = "sukses";
        
        echo json_encode($data);
        
    }
    
    public function get_detail_pemilik()
    {

        $id_pemilik = $this->input->post("id_pemilik");
        
        $data['data_pemilik']     = $this->modelmu->get_pemilik($id_pemilik); //semua kategori usaha
        $info = $data['data_pemilik'];
        $data['kategori_usaha']   = $this->modelmu->get_kategori_usaha($id_pemilik); //semua kategori usaha*
        $data['kategori_produk']  = $this->modelmu->get_kategori_produk(); //semua kategori produk*
        $data['banner']           = $this->model_partner->get_banners($info[0]->id_partner)->result();
        $data['pemilik_kategori'] = $this->modelmu->get_pemilik_kategori($id_pemilik); //kategori yang dipilih pemilik usaha
        $data['pemilik_lokasi']   = $this->modelmu->get_pemilik_lokasi($id_pemilik); //lokasi pemilik usaha
        $data['pemilik_produk']   = $this->modelmu->get_pemilik_produk($id_pemilik); //produk pemilik usaha
        $data['code']             = "1";
        $data['pesan']            = "Sukses";
        echo json_encode($data);
        
    }

    public function get_radius()
    {
        // $this->check_session($this->input->request_headers());

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->modelmu->get_radius();
        
        echo json_encode($data);
    }

    public function get_pemilik()
    {
        $this->check_session($this->input->request_headers());

        $id_pemilik = $this->input->post("id_pemilik");
        $pemilik = $this->modelmu->get_pemilik($id_pemilik);
        
        $data['code']             = "1";
        $data['pesan']            = "Sukses";
        $data['data_pemilik']     = $pemilik;

        
        echo json_encode($data);
    }

    public function get_pemilik_for_desktop()
    {
        // $this->check_session($this->input->request_headers());

        $id_pemilik = $this->input->post("id_pemilik");
        
        $data['code']             = "1";
        $data['pesan']            = "Sukses";
        $data['data_pemilik']     = $this->modelmu->get_pemilik($id_pemilik);


        // $data['kategori_usaha']   = $this->modelmu->get_kategori_usaha($id_pemilik); //semua kategori usaha*
        // $data['kategori_produk']  = $this->modelmu->get_kategori_produk_pemilik($id_pemilik); //semua kategori produk*
        $data['pemilik_kategori'] = $this->modelmu->get_pemilik_kategori($id_pemilik); //kategori yang dipilih pemilik usaha
        echo json_encode($data);
    }
    
    public function get_pemilik_kategori()
    {
        // $this->check_session($this->input->request_headers());

        $id_pemilik = $this->input->post("id_pemilik");
        
        $data['code']  = "1";
        $data['pesan'] = "Sukses";
        $data['data']  = $this->modelmu->get_pemilik_kategori($id_pemilik); //kategori yang dipilih pemilik usaha
        echo json_encode($data);
    }
    
    public function get_pemilik_lokasi()
    {
        // $this->check_session($this->input->request_headers());
        $id_pemilik = $this->input->post("id_pemilik");
        
        $data['code']  = "1";
        $data['pesan'] = "Sukses";
        $data['data']  = $this->modelmu->get_pemilik_lokasi($id_pemilik); //kategori yang dipilih pemilik usaha
        echo json_encode($data);
    }


    public function test_get_produk_siap_antar() {

        // $this->check_session($this->input->request_headers());
        // header("Content-type: application/json");
        // date_default_timezone_set("Asia/Jakarta");

        $id_pemilik = $this->input->post("id_pemilik");
        $page = $this->input->post("page");

            // echo "page: " . $page; die();
        // $min_antar = $this->input->post("min_antar");

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data'] = array();

        $info = $this->modelmu->get_pemilik($id_pemilik);
        // echo json_encode($info); die();

        if (count($info) > 0) {
            $data['data'] = $this->modelmu->test_get_produk_siap_antar($id_pemilik, array(), $info[0]->tipe, $page);
        }

        echo json_encode($data);
    }


    public function test_get_produk_promo() {
        // $this->check_session($this->input->request_headers());

        // header("Content-type: application/json");
        // date_default_timezone_set("Asia/Jakarta");

        $id_pemilik = $this->input->get("id_pemilik");
        $page = $this->input->get("page");

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data'] = new stdClass();

        $info = $this->modelmu->get_pemilik($id_pemilik);

        if (count($info) > 0) {

            $promo = $this->model_merchant_promo->get_promo_by_pemilik($id_pemilik)->result();

            // echo json_encode($promo); die();

            if (count($promo) <= 0 && intval($info[0]->tipe) == 2) {
                $data['code']  = "1";
                $data['pesan'] = "Tidak ada produk promo";
                $data['data'] = array();
                echo json_encode($data);
                return;
            } 

            if (intval($info[0]->tipe) != 2 && $page >= 2) {
                $data['code']  = "1";
                $data['pesan'] = "Tidak ada produk promo";
                $data['data'] = array();
                echo json_encode($data);
                return;
            }

            // echo json_encode($promo); die();

            $promo_item = $promo[$page-1];

            $use_discount = false;

            $promos = array();
            $id_merchant_promos = array();

            $data['data'] = [];

            $rc = 0;
            $previous_produk_id = "";

            if (intval($info[0]->tipe) == 2) {

                $promo_item->free_items = $this->model_merchant_promo->get_free_item($promo_item->id, $id_pemilik, $page)->result();

                if ($promo_item->tipe == "1") $use_discount = true;


                $free_produks = [];
                $free_produk_items = [];
                $produk = new stdClass();
                foreach($promo_item->free_items as $free_item) {

                    $produk2 = new stdClass();
                    $produk2->id = $free_item->id_produk2;
                    $produk2->nama_produk = $free_item->nama_produk;
                    $produk2->foto = $free_item->foto2;
                    $produk2->qty = $free_item->qty2;
                    $produk2->stok = $free_item->stok2;
                    $produk2->harga = $free_item->harga2;
                    $produk2->harga_promo = $free_item->harga_promo2;
                    $produk2->id_kategori_produk = $free_item->id_kategori_produk2;
                    $produk2->status = $free_item->status2;
                    $produk->free_items[] = $produk2;

                    if ($free_item->id_produk != $previous_produk_id) {
                        $produk = new stdClass();
                        $produk->id_produk = $free_item->id_produk;
                        $produk->nama_produk = $free_item->nama_produk;
                        $produk->foto = $free_item->foto;
                        $produk->qty = $free_item->qty;
                        $produk->stok = $free_item->stok;
                        $produk->harga = $free_item->harga;
                        $produk->harga_promo = $free_item->harga_promo;
                        $produk->id_kategori_produk = $free_item->id_kategori_produk;
                        $produk->status = $free_item->status;
                        $produk->kelipatan = $free_item->kelipatan;
                        $free_produks[] = $produk;
                    } 

                    $previous_produk_id = $free_item->id_produk;
                }


                $promos[] = $promo_item;
                $data['data'] = array_merge($data['data'], $free_produks);

                $id_merchant_promos[] = $promo_item->id;
            }


            $produk_pemilik_diskon = $this->model_api_konsumen->get_produk_pemilik_diskon($id_pemilik, $info[0]->id_partner, $info[0]->tipe, $promo_item->id);
            $data['data'] = array_merge($data['data'], $produk_pemilik_diskon);

            $use_discount = false;

            $promo = $this->model_promo_old->get_promo($id_pemilik)->row();
            $produk_promo = [];

            $promo_valid = false;
            $konsumen_valid = false;
            if (!empty($promo)) {
                $produk_promo = $this->model_promo_old->get_produk_promo($promo->promo_id, $id_pemilik, $page);
                $promo_valid = true;

                if(count($produk_promo) > 0) {
                    $promo_konsumen_order = $this->model_promo_old->get_promo_order_konsumen($id_konsumen, $produk_promo[0]->id_produk, $id_pemilik);

                    if (count($promo_konsumen_order) < 2) {
                        $konsumen_valid = true;
                    }
                }
                
            } else {
                // echo "promo empty"; die();
            }

            if ($konsumen_valid) {
                $data['data'] = array_merge($data['data'], $produk_promo);
                // $data['data']->produk_promo = $produk_promo;
            }
        }
        echo json_encode($data);
    }
    

    public function test_get_pemilik_produk() {

        $this->check_session($this->input->request_headers());
        // header("Content-type: application/json");
        // date_default_timezone_set("Asia/Jakarta");

        $id_pemilik = $this->input->post("id_pemilik");
        $page = $this->input->post("page");
        // $min_antar = $this->input->post("min_antar");

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data'] = array();

        $info = $this->modelmu->get_pemilik($id_pemilik);
        // echo json_encode($info); die();

        if (count($info) > 0) {
            $data['data'] = $this->modelmu->get_produk_pemilik($id_pemilik, array(), $info[0]->tipe, 1, $page);
        } else {

            $data['code']  = "0";
            $data['pesan'] = "Kosong";
        }

        echo json_encode($data);
    }

    public function get_produk() {

        $this->check_session($this->input->request_headers());
        // header("Content-type: application/json");
        // date_default_timezone_set("Asia/Jakarta");

        $id_pemilik = $this->input->get("id_pemilik");
        $page = $this->input->get("page");
        // $min_antar = $this->input->post("min_antar");

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data'] = array();

        $info = $this->modelmu->get_pemilik($id_pemilik);
        // echo json_encode($info); die();

        if (count($info) > 0) {


            $promo = $this->model_merchant_promo->get_promo_by_pemilik($id_pemilik)->result();

            // echo json_encode($promo); die();

            if (count($promo) <= 0 && intval($info[0]->tipe) == 2) {
                $data['code']  = "1";
                $data['pesan'] = "Tidak ada produk promo";
                $data['data'] = array();
                echo json_encode($data);
                return;
            } 

            if (intval($info[0]->tipe) != 2 && $page >= 2) {
                $data['code']  = "1";
                $data['pesan'] = "Tidak ada produk promo";
                $data['data'] = array();
                echo json_encode($data);
                return;
            }

            // echo json_encode($promo); die();

            $promo_item = $promo[$page-1];

            $use_discount = false;

            $promos = array();
            $id_merchant_promos = array();

            $data['data'] = [];

            $rc = 0;
            $previous_produk_id = "";

            if (intval($info[0]->tipe) == 2) {

                $promo_item->free_items = $this->model_merchant_promo->get_free_item($promo_item->id, $id_pemilik, $page)->result();

                if ($promo_item->tipe == "1") $use_discount = true;


                $free_produks = [];
                $free_produk_items = [];
                $produk = new stdClass();
                foreach($promo_item->free_items as $free_item) {

                    $produk2 = new stdClass();
                    $produk2->id = $free_item->id_produk2;
                    $produk2->nama_produk = $free_item->nama_produk;
                    $produk2->foto = $free_item->foto2;
                    $produk2->qty = $free_item->qty2;
                    $produk2->stok = $free_item->stok2;
                    $produk2->harga = $free_item->harga2;
                    $produk2->harga_promo = $free_item->harga_promo2;
                    $produk2->id_kategori_produk = $free_item->id_kategori_produk2;
                    $produk2->status = $free_item->status2;
                    $produk->free_items[] = $produk2;

                    if ($free_item->id_produk != $previous_produk_id) {
                        $produk = new stdClass();
                        $produk->id_produk = $free_item->id_produk;
                        $produk->nama_produk = $free_item->nama_produk;
                        $produk->foto = $free_item->foto;
                        $produk->qty = $free_item->qty;
                        $produk->stok = $free_item->stok;
                        $produk->harga = $free_item->harga;
                        $produk->harga_promo = $free_item->harga_promo;
                        $produk->id_kategori_produk = $free_item->id_kategori_produk;
                        $produk->status = $free_item->status;
                        $produk->kelipatan = $free_item->kelipatan;
                        $free_produks[] = $produk;
                    } 

                    $previous_produk_id = $free_item->id_produk;
                }


                $promos[] = $promo_item;
                $data['data'] = array_merge($data['data'], $free_produks);

                $id_merchant_promos[] = $promo_item->id;
            }


            $produk_pemilik_diskon = $this->model_api_konsumen->get_produk_pemilik_diskon($id_pemilik, $info[0]->id_partner, $info[0]->tipe, $promo_item->id);
            $data['data'] = array_merge($data['data'], $produk_pemilik_diskon);

            $use_discount = false;

            $promo = $this->model_promo_old->get_promo($id_pemilik)->row();
            $produk_promo = [];

            $promo_valid = false;
            $konsumen_valid = false;
            if (!empty($promo)) {
                $produk_promo = $this->model_promo_old->get_produk_promo($promo->promo_id, $id_pemilik, $page);
                $promo_valid = true;

                if(count($produk_promo) > 0) {
                    $promo_konsumen_order = $this->model_promo_old->get_promo_order_konsumen($id_konsumen, $produk_promo[0]->id_produk, $id_pemilik);

                    if (count($promo_konsumen_order) < 2) {
                        $konsumen_valid = true;
                    }
                }
                
            } else {
                // echo "promo empty"; die();
            }

            if ($konsumen_valid) {
                $data['data'] = array_merge($data['data'], $produk_promo);
                // $data['data']->produk_promo = $produk_promo;
            }

            $data['data'] = array_merge($data['data'], $this->modelmu->get_produk_pemilik($id_pemilik, array(), $info[0]->tipe, 1, $page));
        } else {

            $data['code']  = "0";
            $data['pesan'] = "Kosong";
        }

        echo json_encode($data);
    }
    
    public function get_pemilik_produk()
    {
        // $this->check_session($this->input->request_headers());
        $id_pemilik = $this->input->post("id_pemilik");

        $info = $this->modelmu->get_pemilik($id_pemilik);

        $promo = $this->model_merchant_promo->get_promo($info[0]->id_partner)->result();

        $use_discount = false;
        $promos = array();
        $id_promos = array();
        foreach ($promo as $promo_item) {
            $id_promos[] = $promo_item->id;
            $promo_item->banner = $this->model_merchant_promo->get_banners($promo_item->id)->result();
            $produks = $this->model_merchant_promo->get_free_item($promo_item->id, $id_pemilik)->result();

            $produks_promo = array();
            $last_barcode = "";
            $free_items = array();
            $item = new stdClass();
            foreach ($produks as $produk) {
                if ($produk->id_produk != $last_barcode) {
                    $item = new stdClass();
                    $item->id_produk = $produk->id_produk;
                    $item->nama_produk = $produk->nama_produk;
                    $item->foto = $produk->foto;
                    $item->qty = $produk->qty;
                    $item->free_items = array();
                } 
                $free_item = new stdClass();
                $free_item->id_produk = $produk->id_produk2;
                $free_item->nama_produk = $produk->nama_produk2;
                $free_item->foto = $produk->foto2;
                $free_item->qty = $produk->qty2;
                $item->free_items[] = $free_item;

                $produks_promo[] = $item;

                $last_barcode = $item->id_produk;
            }

            $promo_item->produk = $produks_promo;

            if ($promo_item->tipe == "1") $use_discount = true;

            $promos[] = $promo_item;
        }

        $data['code']  = "1";
        $data['pesan'] = "Sukses";
        $data['data']['promo'] = $promos; 
        $data['data']['use_discount'] = $use_discount;
        $data['data']['produk']  = array_reverse($this->modelmu->get_pemilik_produk_with_promo($id_pemilik, $id_promos, $info[0]->tipe)); //kategori yang dipilih pemilik usaha
        echo json_encode($data);
    }
    

    public function get_produk_by_pemilik()
    {
        // $this->check_session($this->input->request_headers());
        $id_pemilik = $this->input->post("id_pemilik");

        $data['code']  = "1";
        $data['pesan'] = "test";
        $data['data']  = array_reverse($this->modelmu->get_pemilik_produk_by_pemilik($id_pemilik)); //kategori yang dipilih pemilik usaha
        echo json_encode($data);
    }
    

    public function proses_order()
    {

        // $this->check_session($this->input->request_headers());

        $id_order           = $this->input->post("id_order");
        $id_pemilik         = $this->input->post("id_pemilik");
        $id_perkiraan_antar = $this->input->post("id_perkiraan_antar");
        $pesan_perkiraan_antar = $this->input->post("pesan_perkiraan_antar");
        
        //$id_order   = "1605301926317833160531191237";
        //$id_pemilik = "1605301942108867";
        //$id_perkiraan_antar = "1";

        // if ($id_perkiraan_antar == 0 || empty($id_perkiraan_antar)) {
        //     $id_perkiraan_antar = $this->modelmu->create_perkiraan_antar($pesan_perkiraan_antar);
        // }
        $order = $this->modelmu->get_konsumen_order($id_order);
        $data_konsumen = $this->modelmu->notif_get_konsumen($id_order);
        // echo json_encode($order); die();

        if (count($data_konsumen) == 0) {
            $data['data']  = array();
            $data['code']  = "0";
            $data['pesan'] = "Konsumen sudah dihapus";
            echo json_encode($data);
            die();
        }

        if (intval($order->status) == 3) {
            $data['data']  = array();
            $data['code']  = "0";
            $data['pesan'] = "Order telah dibatalkan";
            echo json_encode($data);
            die();
        } else if (intval($order->status) == 2) {
            $data['data']  = array();
            $data['code']  = "2";
            $data['pesan'] = "Order telah diproses oleh kurir";
            echo json_encode($data);
            die();
        } else  {
            $this->modelmu->proses_order($id_order, $id_perkiraan_antar, $pesan_perkiraan_antar);
        }

        $data['data']  = $this->modelmu->get_order($id_pemilik);
        $data['code']  = "1";
        $data['pesan'] = "sukses";
        
        $data_order    = $this->modelmu->notif_get_order($id_order);
        $nama_konsumen = "";
        $id_konsumen   = "";
        $id            = "";
        $total         = "";
        $status        = "";
        
        $arr_token = array();
        foreach ($data_konsumen as $row) {
            array_push($arr_token, $row->token);
            $nama_konsumen = $row->nama;
            $id_konsumen   = $row->id_konsumen;
        }
        
        foreach ($data_order as $row) {
            $id     = $row->id_order;
            $total  = $row->total;
            $status = $row->status;
        }

        $pemilik = $this->modelmu->get_pemilik($id_pemilik);

        $perkiraan_antar = "";
        if (intval($data_order[0]->id_perkiraan_antar) != 0) {
            $perkiraan_antar_arr = $this->modelmu->get_perkiraan_antar($data_order[0]->id_perkiraan_antar);
            $perkiraan_antar_obj = $perkiraan_antar_arr[0];
            $perkiraan_antar = $perkiraan_antar_obj->nama;
        } else {
            $perkiraan_antar = $pesan_perkiraan_antar;
        }

        
        echo json_encode($data);

        $message = array(
            "jenis" => "merchant",
            "id_konsumen" => $id_konsumen,
            "nama_pemilik" => $pemilik[0]->nama,
            "id_order" => $id,
            "order_date"=> $data_order[0]->tanggal_order,
            "pesan" => $perkiraan_antar
        );
        
        $pushStatus = $this->send_message($arr_token, $message);

        $data["gcm_result"] = $pushStatus;
        
    }

    public function selesai_order()
    {
        // $this->check_session($this->input->request_headers());

        $id_order           = $this->input->post("id_order");
        $id_pemilik         = $this->input->post("id_pemilik");
        
        //$id_order   = "1605301926317833160531191237";
        //$id_pemilik = "1605301942108867";
        //$id_perkiraan_antar = "1";

        // if ($id_perkiraan_antar == 0 || empty($id_perkiraan_antar)) {
        //     $id_perkiraan_antar = $this->modelmu->create_perkiraan_antar($pesan_perkiraan_antar);
        // }
        $order = $this->modelmu->get_konsumen_order($id_order);
        // echo json_encode($order); die();

        if (intval($order->status) == 3) {
            $data['data']  = array();
            $data['code']  = "0";
            $data['pesan'] = "Order telah dibatalkan";
            echo json_encode($data);
            die();
        } else  if (intval($order->status) == 4) {
            $data['data']  = array();
            $data['code']  = "2";
            $data['pesan'] = "Order telah diselesaikan oleh kurir";
            echo json_encode($data);
            die();
        } else {
            $this->modelmu->selesai_order($id_order);
        }

        $data['data']  = $this->modelmu->get_order($id_pemilik);
        $data['code']  = "1";
        $data['pesan'] = "sukses";
        
        $data_konsumen = $this->modelmu->notif_get_konsumen($id_order);
        $data_order    = $this->modelmu->notif_get_order($id_order);
        $nama_konsumen = "";
        $id_konsumen   = "";
        $id            = "";
        $total         = "";
        $status        = "";
        
        $arr_token = array();
        foreach ($data_konsumen as $row) {
            array_push($arr_token, $row->token);
            $nama_konsumen = $row->nama;
            $id_konsumen   = $row->id_konsumen;
        }
        
        foreach ($data_order as $row) {
            $id     = $row->id_order;
            $total  = $row->total;
            $status = $row->status;
        }

        $pemilik = $this->modelmu->get_pemilik($id_pemilik);

        $perkiraan_antar = "";
        if (intval($data_order[0]->id_perkiraan_antar) != 0) {
            $perkiraan_antar_arr = $this->modelmu->get_perkiraan_antar($data_order[0]->id_perkiraan_antar);
            $perkiraan_antar_obj = $perkiraan_antar_arr[0];
            $perkiraan_antar = $perkiraan_antar_obj->nama;
        } 

        echo json_encode($data);
        
        $message = array(
            "jenis" => "merchant",
            "id_konsumen" => $id_konsumen,
            "nama_pemilik" => $pemilik[0]->nama,
            "id_order" => $id,
            "order_date"=> $data_order[0]->tanggal_order,
            "pesan" => "Pesanan telah diterima"
        );
        
        $pushStatus = $this->send_message($arr_token, $message);

        $data["gcm_result"] = $pushStatus;
        
    }
    
    public function get_provinsi()
    {
        // $this->check_session($this->input->request_headers());

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->modelmu->get_provinsi();
        
        echo json_encode($data);
    }
    
    public function get_kota()
    {
        // $this->check_session($this->input->request_headers());

        $id_provinsi = $this->input->post("id_provinsi");
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->modelmu->get_kota($id_provinsi);
        
        echo json_encode($data);
    }
    
    public function isi_profil_awal()
    {
        $this->check_session($this->input->request_headers());

        $id_pemilik = $this->input->post("id_pemilik");
        $nama       = $this->input->post("nama");
        $alamat     = $this->input->post("alamat");
        $kontak_sms     = $this->input->post("kontak_sms");
        $kontak_sms2     = $this->input->post("kontak_sms2");
        $radius   = $this->input->post("radius");
        $include_fee   = $this->input->post("include_fee");
        $include_fee_note   = $this->input->post("include_fee_note");
        $jargon     = $this->input->post("jargon");
        $delivery     = $this->input->post("delivery");
        $tipe_kurir     = $this->input->post("tipe_kurir");
        
        $this->modelmu->isi_profil_awal($id_pemilik, $nama, $alamat, $jargon, $radius, $kontak_sms, $kontak_sms2, $include_fee, $include_fee_note, $delivery, $tipe_kurir);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        
        echo json_encode($data);
    }


    public function update_profile()
    {
        $this->check_session($this->input->request_headers());

        $id_pemilik = $this->input->post("id_pemilik");
        $nama       = $this->input->post("nama");
        $alamat     = $this->input->post("alamat");
        $kontak_sms     = $this->input->post("kontak_sms");
        $kontak_sms2     = $this->input->post("kontak_sms2");
        $radius   = $this->input->post("radius");
        $fee   = $this->input->post("fee");
        $include_fee   = $this->input->post("include_fee");
        $include_fee_note   = $this->input->post("include_fee_note");
        $jargon     = $this->input->post("jargon");
        $delivery     = $this->input->post("delivery");
        $tipe_kurir     = $this->input->post("tipe_kurir");
        $waktu_buka = $this->input->post("waktu_buka");
        $waktu_tutup     = $this->input->post("waktu_tutup");
        
        $this->modelmu->update_profile($id_pemilik, $nama, $alamat, $jargon, $radius, $kontak_sms, $kontak_sms2, $fee, $include_fee, $include_fee_note, $delivery, $tipe_kurir);
        $this->modelmu->update_waktu_buka_tutup($id_pemilik, $waktu_buka, $waktu_tutup);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        
        echo json_encode($data);
    }

    public function set_alamat_min_antar()
    {
        // $this->check_session($this->input->request_headers());

        $id_pemilik = $this->input->post("id_pemilik");
        $alamat     = $this->input->post("alamat");
        $min_antar     = $this->input->post("min_antar");
        
        $this->modelmu->set_alamat_min_antar($id_pemilik, $alamat, $min_antar);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        
        echo json_encode($data);
    }


    public function set_aktif()
    {
        // $this->check_session($this->input->request_headers());

        $id_pemilik = $this->input->post("id_pemilik");
        $aktif     = $this->input->post("aktif");
        
        $this->modelmu->set_aktif($id_pemilik, $aktif);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        
        echo json_encode($data);
    }

    public function allow_kuber()
    {
        // $this->check_session($this->input->request_headers());

        $id_pemilik = $this->input->post("id_pemilik");
        $allow_kuber     = $this->input->post("allow_kuber");
        
        $this->modelmu->allow_kuber($id_pemilik, $allow_kuber);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        
        echo json_encode($data);
    }

    public function set_delivery()
    {
        // $this->check_session($this->input->request_headers());

        $id_pemilik = $this->input->post("id_pemilik");
        $delivery     = $this->input->post("delivery");
        
        $this->modelmu->set_delivery($id_pemilik, $delivery, 0);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        
        echo json_encode($data);
    }


    public function set_radius()
    {
        // $this->check_session($this->input->request_headers());

        $id_pemilik   = $this->input->post("id_pemilik");
        $radius   = $this->input->post("radius");
        
        $this->modelmu->set_radius($id_pemilik, $radius);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        
        echo json_encode($data);
    }

    public function set_no_telp()
    {

        // $this->check_session($this->input->request_headers());
        $id_pemilik = $this->input->post("id_pemilik");
        $kontak       = $this->input->post("kontak");
        $kontak_sms     = $this->input->post("kontak_sms");
        $kontak_sms2     = $this->input->post("kontak_sms2");
        
        $this->modelmu->set_no_telp($id_pemilik, $kontak, $kontak_sms, $kontak_sms2);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        
        echo json_encode($data);
    }
    
    public function isi_lokasi_awal()
    {

        $this->check_session($this->input->request_headers());

        $id_pemilik  = $this->input->post("id_pemilik");
        $lat         = $this->input->post("lat");
        $lng         = $this->input->post("lng");
        $nama_lokasi = $this->input->post("nama_lokasi");
        
        $this->modelmu->isi_lokasi_awal($id_pemilik, $lat, $lng, $nama_lokasi);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        
        echo json_encode($data);
    }
    
    public function get_kategori_usaha()
    {
        // $this->check_session($this->input->request_headers());

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->modelmu->get_kategori_usaha_awal();
        
        echo json_encode($data);
    }


    public function get_kategori_pedagang()
    {
        // $this->check_session($this->input->request_headers());

        $kategori = array();
        $item = new stdClass();
        $item->nama = "Gerobak/Jalan Kaki";
        $item->radius = "1";
        $kategori[] = $item;
                
        $item = new stdClass();
        $item->nama = "Sepeda/Sepeda Motor";
        $item->radius = "2";
        $kategori[] = $item;
                
        $item = new stdClass();
        $item->nama = "Mobil";
        $item->radius = "5";
        $kategori[] = $item;

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $kategori;
        
        echo json_encode($data);
    }
    
    public function new_password()
    {
        // $this->check_session($this->input->request_headers());

        $password   = $this->input->post("password");
        $id_pemilik = $this->input->post("id_pemilik");
        
        $this->modelmu->new_password($id_pemilik, $password);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        echo json_encode($data);
    }

    public function update_password()
    {
        // $this->check_session($this->input->request_headers());

        $old_password   = $this->input->post("old_password");
        $password   = $this->input->post("password");
        $id_pemilik = $this->input->post("id_pemilik");

        $password_valid = $this->modelmu->check_password($id_pemilik, $old_password);
        if (!empty($password_valid)) {
            $this->modelmu->new_password($id_pemilik, $password);

            $data['code']  = "1";
            $data['pesan'] = "Berhasil";
        } else {
            $data['code']  = "0";
            $data['pesan'] = "Password lama salah";
        }

        echo json_encode($data);
    }
    
    public function hapus_akun()
    {
        // $this->check_session($this->input->request_headers());

        $id_pemilik = $this->input->post("id_pemilik");
        
        $this->modelmu->hapus_akun($id_pemilik);
        $this->modelmu->copy_to_histori($id_pemilik);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        
        echo json_encode($data);
    }
    
    public function get_penjualan_harian()
    {
        $this->check_session($this->input->request_headers());

        $tanggal       = $this->input->post("tanggal"); // FORMAT YYYY-MM-DD
        $id_pemilik    = $this->input->post("id_pemilik");
        $data['code']  = "1";
        $data['pesan'] = "Berhasil harian";
        $data['data']  = $this->modelmu->get_penjualan_harian($id_pemilik, $tanggal);
        echo json_encode($data);
    }
    
    public function get_penjualan_bulanan()
    {
        $this->check_session($this->input->request_headers());

        $bulan         = $this->input->post("bulan");
        $tahun         = $this->input->post("tahun");
        $id_pemilik    = $this->input->post("id_pemilik");
        $data['code']  = "1";
        $data['pesan'] = "Berhasil bulanan";
        $data['data']  = $this->modelmu->get_penjualan_bulanan($id_pemilik, $bulan, $tahun);
        echo json_encode($data);
    }
    
    public function get_penjualan_tahunan()
    {
        $this->check_session($this->input->request_headers());

        $tahun         = $this->input->post("tahun");
        $id_pemilik    = $this->input->post("id_pemilik");
        $data['code']  = "1";
        $data['pesan'] = "Berhasil tahunan";
        $data['data']  = $this->modelmu->get_penjualan_tahunan($id_pemilik, $tahun);
        echo json_encode($data);
    }
   

    // public function ganti_foto_produk()
    // {
    //     // $this->check_session($this->input->request_headers());

    //     $id_produk = $this->input->post("id_produk");
    //     $foto        = $this->input->post("foto");
    //     $foto = str_replace("\"", "", $foto);

    //     // echo "foto: " . $foto; die();

    //     $errors = array();

    //     $foto3 = "";
        
    //     if ($foto != "") {
    //         $foto2 = explode(".", $foto);
    //         $foto3 = $id_produk . "." . $foto2[count($foto2) - 1];
    //     }
        
    //     if ($foto3 != "") {
    //         $bitmap = $_FILES;
    //         $return = $this->upload(PATH_IMAGE_PRODUK, $foto3, $bitmap, $id_produk); //ini nama input type file(gambar nya)
    //     }
        
    //     $this->modelmu->set_foto_produk($id_produk, $foto3);
        
    //     if (strlen($return) <= 0) {
    //         $data['code']  = "1";
    //         $data['pesan'] = "Berhasil";
    //         $data['data']  = $this->modelmu->get_foto_produk($id_produk);
    //     } else {
    //         $data['code']  = "0";
    //         $data['pesan'] = "Gagal upload";
    //         $data['data']  = null;
    //     }
        
    //     echo json_encode($data);
    // }


    public function ganti_foto_produk()
    {
        $id_produk = $this->input->post("id_produk");
        $foto        = $this->input->post("foto");
        $is_umkm        = $this->input->post("is_umkm");

        $foto = str_replace("\"", "", $foto);

        // echo "foto: " . $foto; die();

        $errors = array();
        $foto_obj = $this->modelmu->get_foto_produk($id_produk);

        $foto3 = "";
        
        if ($foto != "") {
            $foto2 = explode(".", $foto);
            $foto3 = $id_produk . "." . $foto2[count($foto2) - 1];
            $foto3 = $id_produk . "" . (intval($foto_obj->versi) + 1) . "." . $foto2[count($foto2) - 1];
        }
        
        if ($foto3 != "") {
            $bitmap = $_FILES;
            $return = $this->upload(PATH_IMAGE_PRODUK, $foto3, $bitmap, $id_produk . "" . (intval($foto_obj->versi) + 1)); //ini nama input type file(gambar nya)
        }
        
        $this->modelmu->set_foto_produk($id_produk, $foto3, $is_umkm);
        
        if (strlen($return) <= 0) {
            $data['code']  = "1";
            $data['pesan'] = "Berhasil";
            $data['data']  = $this->modelmu->get_foto_produk($id_produk);
        } else {
            $data['code']  = "0";
            $data['pesan'] = "Gagal upload";
            $data['data']  = null;
        }
        
        echo json_encode($data);
    }
 
    public function ganti_foto()
    {
        $this->check_session($this->input->request_headers());

        $id_pemilik = $this->input->post("id_pemilik");
        $foto        = $this->input->post("foto");
        $foto = str_replace("\"", "", $foto);

        // echo "foto: " . $foto; die();

        $errors = array();

        $foto3 = "";
        
        if ($foto != "") {
            $foto2 = explode(".", $foto);
            $foto3 = $id_pemilik . "." . $foto2[count($foto2) - 1];
        }
        
        if ($foto3 != "") {
            $bitmap = $_FILES;
            $errors = $this->upload(PATH_IMAGE_AVATAR_PEMILIK, $foto3, $bitmap, $id_pemilik); //ini nama input type file(gambar nya)
        }
        
        $this->modelmu->set_foto_avatar($id_pemilik, $foto3);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->modelmu->get_foto($id_pemilik);
        $data['errors'] = $errors;
        $data['foto'] = $id_pemilik;
        
        echo json_encode($data);
    }


    public function ganti_foto_bg()
    {
        $this->check_session($this->input->request_headers());

        $id_pemilik = $this->input->post("id_pemilik");
        $foto        = $this->input->post("foto");
        $foto = str_replace("\"", "", $foto);

        // echo "foto: " . $foto; die();

        $errors = array();

        $foto3 = "";
        
        if ($foto != "") {
            $foto2 = explode(".", $foto);
            $foto3 = $id_pemilik . "." . $foto2[count($foto2) - 1];
        }
        
        if ($foto3 != "") {
            $bitmap = $_FILES;
            $errors = $this->upload(PATH_IMAGE_PEMILIK, $foto3, $bitmap, $id_pemilik); //ini nama input type file(gambar nya)
        }
        
        $this->modelmu->set_foto($id_pemilik, $foto3);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->modelmu->get_foto($id_pemilik);
        $data['errors'] = $errors;
        $data['foto'] = $id_pemilik;
        
        echo json_encode($data);
    }
    
    public function update_kategori_produk()
    {
        // $this->check_session($this->input->request_headers());

        $id_pemilik    = $this->input->post("id_pemilik");
        $id_kategori   = $this->input->post("id_kategori");
        $nama_kategori = $this->input->post("nama_kategori");
        
        $this->modelmu->update_kategori_produk($id_kategori, $nama_kategori);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->modelmu->get_pemilik_produk($id_pemilik);
        
        echo json_encode($data);
    }
    
    public function delete_kategori_produk()
    {
        // $this->check_session($this->input->request_headers());

        $id_pemilik  = $this->input->post("id_pemilik");
        $id_kategori = $this->input->post("id_kategori");
        
        $this->modelmu->delete_kategori_produk($id_kategori);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->modelmu->get_pemilik_produk($id_pemilik);
        
        echo json_encode($data);
    }
    
    
    // public function upload($path, $nama_file, $id_file)
    // {
    //     try {
    //         $config['upload_path']   = $path;
    //         $config['file_name']     = $nama_file;
    //         $config['allowed_types'] = '*';
    //         $config['overwrite']     = TRUE;
    //         $config['overwrite']     = TRUE;

    //         file_put_contents($path, base64_decode($nama_file));

    //         // $this->load->library('upload', $config);
    //         // if ( ! $this->upload->do_upload($id_file))
    //         // {
    //         //     $error = array('error' => $this->upload->display_errors());

    //         // }   
    //         // else
    //         // {
    //         //     $upload_data = $this->upload->data();
    //         // }
    //         return "";
    //     }
    //     catch (Exception $exc) {
    //         return $exc->getTraceAsString();
    //     }
    // }

    public function upload($path, $nama_file, $files, $id_file)
    {   
        $exp                     = explode(".", $nama_file);
        $ext                     = end($exp);  


        if (is_file($path.$id_file.".".$ext)) {
            unlink($path.$id_file.".".$ext);
        }
        try {

            $config['upload_path']   = $path;
            $config['file_name']     = $id_file;
            $config['allowed_types'] = '*';
            $config['overwrite']     = TRUE;

            if (!is_dir($path)) {
                mkdir($path, 0777, true);
            }
            
        // echo $path.$id_file.".".$ext; die();
            if (isset($files['form_file']) && !empty($files['form_file'])) {
                if ($files['form_file']['error'] != 4) {

                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    
                    if (!$this->upload->do_upload('form_file')) {
                        $errors = $this->upload->display_errors();
                        return $errors;
                    } else {
                        $upload_data = $this->upload->data();
                        // echo json_encode($upload_data); die();
                        return array();
                    }
                }
            } 
        }
        catch (Exception $exc) {
            return $exc->getTraceAsString();
             
        }
    }


    function unzip($path, $filename) {
        // echo $path.$filename; die();
        // get the absolute path to $file
        $zip = new ZipArchive;
        $res = $zip->open($path.$filename);
        if ($res === TRUE) {
          $zip->extractTo($path);
          $zip->close();
          return 1;
        } else {
          return 0;
        }
    }


    public function upload_xls($path, $nama_file, $files, $id_file)
    {   
        $exp                     = explode(".", $nama_file);
        $ext                     = end($exp);  

        $name = $_FILES["form_file"]["name"];
        $exploded = explode(".", $name);
        $ext = end($exploded); 
        // echo $path.$id_file.".".$ext; die();

        if (is_file($path.$id_file.".".$ext)) {
            unlink($path.$id_file.".".$ext);
        }
        try {

            $config['upload_path']   = $path;
            $config['file_name']     = str_replace(' ', '', reset($exploded)) . "_" . date("m-d-Y_G-i") . "." . $ext;
            $config['allowed_types'] = '*';
            $config['overwrite']     = TRUE;

            if (!is_dir($path)) {
                mkdir($path, 0777, true);
            }
            
            if (isset($files['form_file']) && !empty($files['form_file'])) {
                if ($files['form_file']['error'] != 4) {

                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    
                    if (!$this->upload->do_upload('form_file')) {
                        $errors = $this->upload->display_errors();
                        return "";
                        // echo json_encode($errors); die();
                    } else {
                        $upload_data = $this->upload->data();
                        return str_replace(' ', '', reset($exploded)) . "_" . date("m-d-Y_G-i") . "." . $ext;
                        // echo json_encode($upload_data); 
                    }
                }
            } 

            return str_replace(' ', '', reset($exploded)) . "_" . date("m-d-Y_G-i") . "." . $ext;
        }
        catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return "";
        }
    }
    
    public function update_token()
    {

        $id_pemilik = $this->input->post("id_user");
        $token      = $this->input->post("token");
        
        // echo $token;
        
        $this->modelmu->update_token($id_pemilik, $token);
        
        $data['code']  = "1";
        $data['pesan'] = "sukses";
        $data['data']  = $this->modelmu->get_pemilik($id_pemilik);
        
        echo json_encode($data);
    }
    
    public function get_order_dari_notif()
    {
        $this->check_session($this->input->request_headers());

        $id_order = $this->input->get("id_order");
        
        $data['code']     = "1";
        $data['pesan']    = "Sukses";
        $data['kategori'] = $this->modelmu->get_order_dari_notif($id_order);
        
        echo json_encode($data);
    }


    public function get_produk_promo()
    {
        $this->check_session($this->input->request_headers());

        $id_pemilik = $this->input->get("id_pemilik");

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data'] = new stdClass();
        // $pemiliks = $this->modelmu->get_kategori_usaha($id_pemilik);

        $promo_codes = $this->model_promo_old->get_promo_code($id_pemilik)->result();
        // echo json_encode($promo_code);
        $produk_promo = array();
        foreach ($promo_codes as $promo_code) {
            $produk_promo = array_merge($produk_promo, $this->model_promo_old->get_produk_promo($promo_code->promo_id, $id_pemilik));
        }
        // echo json_encode($produk_promo);
        // die();
        // $data['data']->produk_promo = [];

        // foreach ($pemiliks as $pemilik) {
        //     if ($pemilik->id_kategori == $promo_for_konsumen->id_kategori) {
                $data['data']->produk_promo = $produk_promo;
        //     }
        // }

        echo json_encode($data);
    }

    public function get_produk_umkm($id_pemilik, $page=0)
    {
        // $this->check_session($this->input->request_headers());

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['page_count'] = ceil($this->modelmu->get_total_produks_umkm($id_pemilik) / 50);
        $produk_umkm = $this->modelmu->get_produks_umkm($id_pemilik, $page);
        $data['data'] = $produk_umkm;

        echo json_encode($data);
    }
    


    public function update_provinsi_kota()
    {
        // $this->check_session($this->input->request_headers());

        $id_provinsi = $this->input->post("id_provinsi");
        $id_kota     = $this->input->post("id_kota");
        $id_pemilik = $this->input->post("id_pemilik");
        
        $this->modelmu->update_provinsi_kota($id_pemilik, $id_provinsi, $id_kota);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        echo json_encode($data);
    }

    public function tambah_ongkirs($id_pemilik) {

        // $this->check_session($this->input->request_headers());

        $ongkirs_json = $this->input->post("ongkirs");

        $ongkirs = json_decode($ongkirs_json);
        $this->modelmu->tambah_ongkirs($id_pemilik, $ongkirs);

        $items = $this->modelmu->get_ongkirs($id_pemilik);

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['ongkirs'] = $items;
        echo json_encode($data);
    }


    public function set_fee($id_pemilik) {
        // $this->check_session($this->input->request_headers());

        $fee = $this->input->post("fee");

        $this->modelmu->set_fee($id_pemilik, $fee);

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        echo json_encode($data);
    }


    public function set_kurir_sendiri()
    {
        // $this->check_session($this->input->request_headers());

        $id_pemilik = $this->input->post("id_pemilik");
        $fee   = $this->input->post("fee");
        $include_fee   = $this->input->post("include_fee");
        $include_fee_note   = $this->input->post("include_fee_note");
        $delivery     = 1;
        $tipe_kurir     = 1;
        
        $this->modelmu->set_kurir_sendiri($id_pemilik, $fee, $include_fee, $include_fee_note, $delivery, $tipe_kurir);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        
        echo json_encode($data);
    }


    public function set_tipe_kurir()
    {
        // $this->check_session($this->input->request_headers());

        $id_pemilik = $this->input->post("id_pemilik");
        $tipe_kurir   = $this->input->post("tipe_kurir");
        
        $this->modelmu->set_tipe_kurir($id_pemilik, $tipe_kurir);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        
        echo json_encode($data);
    }

    public function get_ongkirs($id_pemilik) {
        // $this->check_session($this->input->request_headers());

        $items = $this->modelmu->get_ongkirs($id_pemilik);
        $fee = $this->modelmu->get_pemilik_fee($id_pemilik);

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['fee'] = $fee->fee;
        $data['include_fee'] = $fee->include_fee;
        $data['include_fee_note'] = $fee->include_fee_note;
        $data['ongkirs'] = $items;
        echo json_encode($data);
    }
    
    
    
    
    
    function tes_push()
    {
        $data_konsumen = $this->modelmu->notif_get_konsumen("1605301926317833160530213139");
        $data_order    = $this->modelmu->notif_get_order("1605301926317833160530213139");
        $nama_konsumen = "";
        $id_konsumen   = "";
        $id            = "";
        $total         = "";
        $status        = "";
        
        $arr_token = array();
        foreach ($data_konsumen as $row) {
            array_push($arr_token, $row->token);
            $nama_konsumen = $row->nama;
            $id_konsumen   = $row->id_konsumen;
        }
        
        foreach ($data_order as $row) {
            $id     = $row->id_order;
            $total  = $row->total;
            $status = $row->status;
        }
        
        $message = array(
            "jenis" => "1",
            "id_konsumen" => $id_konsumen,
            "id_order" => $id,
            "total" => $total,
            "status" => $status,
            "notif" => $nama_konsumen . " yth, Order anda sedang diproses."
        );
        
        print_r($data_konsumen);
        echo "<hr>";
        print_r($data_order);
        
        $pushStatus = $this->send_message($arr_token, $message);
    }


    
    function send_message($registatoin_ids, $message)
    {
            // $url    = 'https://android.googleapis.com/gcm/send';
        $url    = 'https://fcm.googleapis.com/fcm/send';

        if (count($registatoin_ids) <= 0) return;

        $fields = new stdClass();
        $fields->to = $registatoin_ids[0];
        $fields->data = $message;

        // $notification = new stdClass();
        // $notification->click_action = ".MainActivity";
        // $notification->body = $message["notif"];
        // $notification->title = "OPA";
        // $notification->icon = "ic_launcher";
        // $notification->sound = "default";
        // $notification->android_channel_id = "default";

        // $fields->notification = $notification;
            // $fields = array(
            //     'to' => $registatoin_ids,
            //     'data' => $message
            //     );

        
        
        $headers = array(
            'Authorization: key=' . KEY_GCM_KONSUMEN,
            'Content-Type: application/json'
        );
        $ch      = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === FALSE) {
            $result = "salah";
        }
        curl_close($ch);
        return $result;
    }
    
    function confirm_promo_code()
    {
        // $this->check_session($this->input->request_headers());


        header("Content-type: application/json");
        
        $id_pemilik = $this->input->post("id_pemilik");
        $promo_code = $this->input->post("promo_code");
        
        $promo = $this->model_promo_old->validate_promo_code($id_pemilik, $promo_code);
        
        $data = array();
        if ($promo != null) {
            $used = $this->model_promo_old->confirm_promo_code($promo_code);

            $data['code']  = "1";
            $data['pesan'] = "Selamat, promo Anda aktif";
            $data['data']  = "";

            // $produks_promo = $this->model_promo_old->get_produk_promo($promo->promo_id);

            // foreach ($produks_promo as $produk) {
            //     // $array = json_decode(json_encode($produk), true);
            //     $this->model_promo_old->copy_produk_promo($id_pemilik, $produk);
            // }

        } else {
            $data['code']  = "0";
            $data['pesan'] = "Maaf kode promo yang Anda masukkan salah. Silakan hubungi Admin untuk meminta kode promo Anda";
            $data['data']  = "";
        }
        echo json_encode($data);
        
    }

    function get_promo($id_pemilik)
    {
        // $this->check_session($this->input->request_headers());
        header("Content-type: application/json");
        
        $promo = $this->model_promo_old->get_promo($id_pemilik)->row();
        
        
        $data = array();

        if (!empty($promo)) {
            $data['code']  = "1";
            $data['pesan'] = "Promo ada";
            $data['data']  = $promo;

        } else {
            $data['code']  = "0";
            $data['pesan'] = "Tidak ada promo";
            $data['data']  = null;
        }
        echo json_encode($data);
        
    }

    public function edit_nama_produk_master() {
        $path = PATH_IMAGE_PRODUK . "master/";

        // echo "path:  " . $path; die();

        $objects = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($path),
            RecursiveIteratorIterator::SELF_FIRST
        );

        foreach ($objects as $file => $object) {
            $basename = basename($file);
            if ($basename == '.' or $basename == '..') {
                continue;
            }
            if (is_dir($file)) {
                continue;
            }
            if (date('Y-m-d') === date('Y-m-d', filectime($file))) {
                $filename = basename($file);   // $file is set to "index.php"
                $filenameWithoutExtension = preg_replace('/\\.[^.\\s]{3,4}$/', '', $filename);
                // echo PATH_IMAGE_PRODUK.$filenameWithoutExtension . PHP_EOL;
                rename($file, PATH_IMAGE_PRODUK .$filenameWithoutExtension);
            }
        }
    }

    public function create_merchant_promo($id_pemilik) {

        // $this->check_session($this->input->request_headers());

        // header("Content-type: application/json");
        $res = "";

        $pemiliks = $this->modelmu->get_pemilik($id_pemilik);
        if (count($pemiliks) <= 0) {

            $data = array("status"=>0);
            echo json_encode($data);
            return;
        }

        $pemilik = $pemiliks[0];

        // echo $json_encode($pemilik);

        try {
            $this->load->helper('form');

            $json_request_body = file_get_contents('php://input');
            log_message('info','payload\n\n '.$json_request_body);
            $request_body = json_decode($json_request_body);
            // echo json_encode($request_body); die();
            $promo_array_root = $request_body->promo;

            foreach ($promo_array_root as $promo_item) {
                $id_promo               = $promo_item->id_promo;
                $nama               = "Promo";
                $start_date        = $promo_item->tgl_mulai;
                $end_date        = $promo_item->tgl_selesai;

                $id_partner = $pemilik->id_partner;

                $tipe = 1;
                if (count($promo_item->rows_byqty) > 0) {
                    $tipe = 2;
                }

                $insert_id = 0;

                // $this->model_merchant_promo->check_
                if ((count($promo_item->rows_byqty) > 0 || count($promo_item->rows_byharga) > 0) 
                    && $this->model_merchant_promo->get_promo_by_id_promo_and_pemilik($id_promo, $id_pemilik) == null) {
                        $insert_id = $this->model_merchant_promo->create_promo($id_promo, $nama, $id_partner, $id_pemilik, $tipe, $start_date, $end_date);
                } 

                // echo $insert_id. "\n";


                if (count($promo_item->rows_byqty) > 0 && $insert_id > 0) { 
                    $rows_byqty = $promo_item->rows_byqty;

                    // echo "count " . count($rows_byqty); die();
                    self::insert_produk_promo_batch($insert_id, $rows_byqty);
                } 

                if (count($promo_item->rows_byharga) > 0 && $insert_id > 0) {  
                    $rows_byharga = $promo_item->rows_byharga;
                    $this->modelmu->insert_produk_promo_diskon_batch($rows_byharga, $insert_id);
                }
            }

            // echo $insert_id . "\n"; die();

            
            $res = "Promo created successfully";
            
            $data = array("status"=>$res, "code" => 1);
            echo json_encode($data);
            
        } catch (Exception $ex) {
            $res = "failed->".$ex;
            $data = array("status"=>$res);
            echo json_encode($data);
        }
    }

    public function delete_old_promo($id_partner) {
        // $this->check_session($this->input->request_headers());
        $pemiliks = $this->model_partner->get_pemilik_of_partner($id_partner);
        $promos = $this->model_merchant_promo->get_promo($id_partner)->result();
        // echo json_encode($promos); die();

        $id_promos = "";
        foreach ($promos as $promo) {
            $id_promos = $id_promos . $promo->id . ", ";
        }

        $id_promos = $id_promos . "HEHEHE1234";
        $id_promos = str_replace(", HEHEHE1234", "", $id_promos);

        foreach ($pemiliks as $pemilik) {
            $this->modelmu->nullify_harga_promo_by_promos($id_pemilik, $id_promos);
        }
        $this->model_merchant_promo->delete_old_promo();
    }

    public function nullify($id_pemilik) {

        $promos = $this->model_merchant_promo->get_inactive_promo_by_pemilik($id_pemilik)->result();
        // echo json_encode($promos); die();

        $id_promos = "";
        foreach ($promos as $promo) {
            $id_promos = $id_promos . $promo->id . ", ";
        }

        $id_promos = $id_promos . "HEHEHE1234";
        $id_promos = str_replace(", HEHEHE1234", "", $id_promos);

        $this->modelmu->nullify_harga_promo_by_promos($id_pemilik, $id_promos);

    }

    public function nullify_all_pemiliks($id_partner) {

        $pemiliks = $this->model_partner->get_pemilik_of_partner($id_partner);

        foreach ($pemiliks as $pemilik) {
            $this->modelmu->nullify_all_harga_promo($pemilik->id_pemilik);
        }

    }


    // public function set_harga_promo($id_partner, $id_merchant_promo, $rows_byharga)
    // {
    //     // $this->check_session($this->input->request_headers());

    //         $header = array('barcode', 'min_qty', 'harga_promo');
    //         $rows = $rows_byharga;
            
    //         $pemiliks = $this->model_partner->get_pemilik_of_partner($id_partner);

    //         // echo json_encode($pemiliks); die();

    //         foreach ($pemiliks as $pemilik) {
    //             $this->modelmu->set_harga_promo_batch($pemilik->id_pemilik, $rows, $id_merchant_promo);
    //         }
    // }

    public function insert_produk_promo_batch($id_merchant_promo, $rows_byqty)
    {
        $header = array('barcode', 'min_qty', 'harga_promo');

        $this->modelmu->insert_produk_promo_batch($id_merchant_promo, $rows_byqty);

    }
    
   
    public function update_produks_umkm_batch($id_pemilik)
    {
        $json_request_body = file_get_contents('php://input');
        // echo $json_request_body; die();
        log_message('info','payload\n\n '.$json_request_body);
        $stocks = json_decode($json_request_body);
        // echo json_encode($stocks); die();

        $pemiliks = $this->modelmu->get_pemilik($id_pemilik);
        $this_pemilik = $pemiliks[0];
        $pemilik = $this->modelmu->get_first_pemilik_by_partner($this_pemilik->id_partner);


        $query_res = $this->modelmu->update_produks_umkm($id_pemilik, $stocks);
        $results = $this->modelmu->get_produks_umkm($id_pemilik, 1);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $results;
        $data['error'] = "";
        
        echo json_encode($data);
    }

    public function upload_foto_produk_batch()
    {
        $id_pemilik        = $this->input->post("id_pemilik");
        $foto        = $this->input->post("foto");
        $foto = str_replace("\"", "", $foto);

        // echo "foto: " . $foto; die();

        $errors = array();

        $foto3 = "";
        
        if ($foto != "") {
            $foto2 = explode(".", $foto);
            $foto3 = $id_pemilik . "." . $foto2[count($foto2) - 1];
        }
        
        if ($foto3 != "") {
            $bitmap = $_FILES;
            $errors = $this->upload(PATH_IMAGE_PRODUK, $foto3, $bitmap, $id_pemilik); //ini nama input type file(gambar nya)
        }

        // echo json_encode($errors); echo "\n";

        $retVal = self::unzip(PATH_IMAGE_PRODUK, $id_pemilik . ".zip");
        
        $data['code']  = $retVal;
        $data['pesan'] = $retVal == "1" ? "Berhasil" : "Gagal";
        $data['data']  = [];
        $data['errors'] = $errors;
        
        echo json_encode($data);
    }


    public function upload_foto_produk_master_batch()
    {
        $id_pemilik        = "83839482";
        $id_partner        = "40226821";
        $foto        = $this->input->post("foto");
        $foto = str_replace("\"", "", $foto);

        // echo "foto: " . $foto; die();

        $errors = array();

        $foto3 = "";

        $return = "";
        
        if ($foto != "") {
            $foto2 = explode(".", $foto);
            $foto3 = $id_pemilik . "." . $foto2[count($foto2) - 1];
        }
        
        if ($foto3 != "") {
            // echo "upload"; die();
            $bitmap = $_FILES;
            $return = $this->upload_xls(PATH_IMAGE_PRODUK . "master/", $foto3, $bitmap, $id_pemilik); //ini nama input type file(gambar nya)
        } else {
            echo "foto kosong"; die();
        }


        $retVal = self::unzip(PATH_IMAGE_PRODUK . "master/", $id_pemilik . ".zip");
        self::edit_nama_produk_master();
        $this->modelmu->set_foto_master_batch($id_pemilik);
        $return = str_replace(".zip", "", $return);
        // echo json_encode($return ); echo "\nini ". $id_partner; die();
        $this->model_partner->set_foto_produk_filename($id_partner, $return);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = array();
        $data['errors'] = $errors;
        
        echo json_encode($data);
    }

    public function update_waktu_buka_tutup()
    {
        $this->check_session($this->input->request_headers());

        $waktu_buka = $this->input->post("waktu_buka");
        $waktu_tutup     = $this->input->post("waktu_tutup");
        $id_pemilik = $this->input->post("id_pemilik");
        
        $this->modelmu->update_waktu_buka_tutup($id_pemilik, $waktu_buka, $waktu_tutup);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        echo json_encode($data);
    }


    public function cari_produk() {
        // $this->check_session($this->input->request_headers());

        header("Content-type: application/json");
        date_default_timezone_set("Asia/Jakarta");

        $q = $this->input->get("q");
        $page = 0;
        $id_pemilik = $this->input->get("id_pemilik");
        // echo $keyword; die();

        $info = $this->modelmu->get_pemilik($id_pemilik);

        if (count($info) > 0) {
            $promos = $this->model_merchant_promo->get_promo_by_pemilik($id_pemilik)->result();
            $id_promos = "";
            foreach ($promos as $promo) {
                $id_promos = $id_promos . $promo->id . ", ";
            }

            if ($id_promos != "") {
                $id_promos = $id_promos . "HEHEHE1234";
                $id_promos = str_replace(", HEHEHE1234", "", $id_promos);
            }

            $produks = $this->modelmu->cari_produk_by_pemilik($q, $id_pemilik, $info[0]->tipe, $page, $id_promos);
        }


        $data['code']     = "1";
        $data['pesan']    = "Sukses";
        $data['data']       = $produks;
        
        echo json_encode($data);
    }


    public function tambah_toko_batch($count) {

        $id_pemiliks = $this->modelmu->get_random_nums($count);
        $phones = $this->modelmu->get_random_phones($count);

        $tipe = 3;
        $kontak_sms2 = '081297910898';
        $id_partner = '';
        $password = "pin123";
        $username = "User Baru";
        $kode_sms = "1234";
        $id_provinsi = "12";
        $id_kota = "163";
        $radius = "0";
        $min_antar = "0";
        $fee = "0";

        $i = 0;
        foreach ($phones as $no_telp) {
            $id_pemilik = $id_pemiliks[$i]->random_num;
            $this->modelmu->tambah_toko_batch($id_pemilik, $id_partner, $no_telp->random_num, $no_telp->random_num, $kontak_sms2, $password, $username, $kode_sms, $id_provinsi, $id_kota, $radius, $min_antar, $fee, 3);

            $i++;
        }
    }


    public function tambah_toko($id_partner) {
        
        require_once APPPATH . 'controllers/ctr_email.php';
        $myemail = new Ctr_email();

        $nama = $this->input->post("nama");
        $email = $this->input->post("email");

        $id_pemiliks = $this->modelmu->get_random_nums(1);
        $phones = $this->modelmu->get_random_phones(1);
        $id_pemilik = $id_pemiliks[0];
        $no_telp = $phones[0];

        $password = self::random_string(8);

        if ($id_partner != "0") {
            $tipe = 2;
        } else {
            $tipe = $this->input->post("tipe");
            if (intval($tipe) == 2) {
                $id_partner = "40226821"; // di set ke hydro dulu sementara semua
            } else {
                $id_partner = "";
            }
        }


        $kontak_sms2 = '';
        $username = str_replace(" ", "_", $nama);
        $kode_sms = "";
        $id_provinsi = "12";
        $id_kota = "163";
        $radius = "0";
        $min_antar = "0";
        $fee = "0";

        $this->modelmu->tambah_toko_batch($id_pemilik->random_num, $id_partner, $no_telp->random_num, $no_telp->random_num, $kontak_sms2, $password, $username, $kode_sms, $id_provinsi, $id_kota, $radius, $min_antar, $fee, $tipe);
        $this->modelmu->update_nama_email_toko($id_pemilik->random_num, $nama, $email);
        $info = $this->modelmu->get_pemilik($id_pemilik->random_num);

        if (intval($tipe) != 2) {
            $pesan_sms = "<b><h2>" . $nama . "</h2></b>\n\n<br><br>Your login credentials\n\n<br>Phone number : " . $no_telp->random_num . ", password : " . $password;
            
            $email_return = $myemail->send_email($email, "Credential Login Anda", $pesan_sms);
        } else {
        	$edata = array();
        	$edata['telp'] = $no_telp->random_num;
        	$edata['password'] = $password;
        	$edata['id'] = $id_pemilik->random_num;

        	$email_return = $myemail->send_email2($email, "Credential Login Anda", $edata);
        }

        $data['code']     = "1";
        $data['pesan']    = "Sukses";
        $data['data']       = $info[0];
        $data['errors']    = $email_return;

        echo json_encode($data);

    }

    function random_string($length) {
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $key;
    }


}
