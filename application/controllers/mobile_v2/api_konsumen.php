<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_konsumen extends CI_Controller
{
    const TIPE_PEDAGANG_KELILING = 4; 
    const TIPE_EKSPEDISI = 3; 


    function __construct()
    {
        parent::__construct();
        $this->load->model("mobile_v2/model_api_konsumen", "modelmu");
        $this->load->model("mobile_v2/model_api_kurir", "model_api_kurir");
        $this->load->model("mobile_v2/model_api_ekspedisi", "model_api_ekspedisi");
        $this->load->model("v2/model_promo_old", "model_promo_old");
        $this->load->model("v2/model_merchant_promo", "model_merchant_promo");
        $this->load->model("v2/model_setting", "model_setting");
        $this->load->model("v2/model_partner", "model_partner");
        $this->load->model("mobile_v2/model_api_pemilik", "model_api_pemilik");


        $this->config->load('telegram');
        $this->load->model('telegram');
        $this->load->model('model_kurir');
    }

    // function cek_kode() {
    //     $id_kode_sms = $this->input->post("id_kode_sms");
    //     $kode_sms    = $this->input->post("kode_sms");

    //     $status_kode    = $this->modelmu->cek_kode($id_kode_sms, $kode_sms);

    //     $code  = "0";
    //     $pesan = "";
    //     // if ($status_kode == "0") {
    //         // $code  = "0";
    //         // $pesan = "Kode salah";
    //     // } else {
    //         $code  = "1";
    //         $pesan = "Sukses";
    //     // }
    //     $data['code']        = $code;
    //     $data['pesan']       = $pesan;
    //     $data['kode']        = $id_kode_sms . " -> " . $kode_sms;
        
    //     echo json_encode($data);
    // }

    function cek_kode() {
        $id_kode_sms = $this->input->post("id_kode_sms");
        $kode_sms    = $this->input->post("kode_sms");

        $status_kode    = $this->modelmu->cek_kode($id_kode_sms, $kode_sms);

        $code  = "0";
        $pesan = "";

        if (intval($status_kode) == 0) {
            $code  = "0";
            $pesan = "Kode salah";
        } else {
            $code  = "1";
            $pesan = "Sukses";
        }
        $data['code']        = $code;
        $data['pesan']       = $pesan;
        $data['kode']        = $id_kode_sms . " -> " . $kode_sms;
        
        echo json_encode($data);
    }
    
    function daftar()
    {

        $password    = $this->input->post("password");
        $id_provinsi = $this->input->post("id_provinsi");
        $id_kota     = $this->input->post("id_kota");
        $no_telp     = $this->input->post("no_telp");
        $id_kode_sms = $this->input->post("id_kode_sms");
        $kode_sms    = $this->input->post("kode_sms");
        
        
        $id_konsumen = date("ymdHis") . rand(1000, 10000);
        $username    = "User Baru";
        
        $status_no_telp = $this->modelmu->cek_no_telp($no_telp);
        $status_kode    = $this->modelmu->cek_kode($id_kode_sms, $kode_sms);
        
        $code  = "0";
        $pesan = "";
        
        if ($status_no_telp == "0") {
            // if ($status_kode == "0") {
            //     $code  = "0";
            //     $pesan = "Kode salah";
            // } else {
            $code  = "1";
            $pesan = "Sukses";
            $this->modelmu->daftar($id_konsumen, $no_telp, $password, $username, $kode_sms, $id_provinsi, $id_kota);

                // $id_pemilik_return = $this->modelmu->daftar($id_pemilik,$no_telp,$password,$username,$kode_sms,$id_provinsi,$id_kota);
                // $status_daftar = "1";
                // require_once APPPATH.'controllers/ctr_email.php';
                // $myemail = new Ctr_email();
                // $pesan = "sukses";
                // $subjek = "Kode verifikasi OPA";
                // $pesan_email = "<p>Kepada $username</p>

                // <p>Berikut adalah kode verifikasi anda yang akan digunakan untuk proses pendaftaran pemilik usaha:</p>

                // <p><strong>$kode</strong></p>

                // <p>Terima Kasih,<br />
                // OPA Support</p>
                // ";

                // // $myemail->send_email($email, $subjek, $pesan_email);
                // $pesan_sms = 'Kepada '.$username.'. Berikut adalah kode verifikasi anda yang akan digunakan untuk proses pendaftaran pemilik usaha: '.$kode.'. Terima Kasih, OPA Support.';

                // //$myemail->send_email($email, $subjek, $pesan_email);
                // $myemail->send_sms($kontak,$pesan_sms);



            // }
            
        } else {
            $code  = "0";
            $pesan = "No telepon sudah ada";
        }
        
        
        $data['code']        = $code;
        $data['pesan']       = $pesan;
        $data['id_konsumen'] = $id_konsumen;
        $data['kode']        = $id_kode_sms . " -> " . $kode_sms;
        
        echo json_encode($data);
    }
    
    function lupa_password()
    {
        $no_telp     = $this->input->post("no_telp");
        $id_kode_sms = $this->input->post("id_kode_sms");
        $kode_sms    = $this->input->post("kode_sms");
        
        // $no_telp        = "8888";
        // $id_kode_sms    = "201606022034332624";
        // $kode_sms       = "9037";
        
        $status_no_telp = $this->modelmu->cek_no_telp($no_telp);
        $status_kode    = $this->modelmu->cek_kode($id_kode_sms, $kode_sms);
        $id_konsumen    = $this->modelmu->get_id_konsumen_by_no_telp($no_telp);
        
        $code  = "0";
        $pesan = "";
        if (count($id_konsumen) != 0) {
            if ($status_kode == "0") {
                $code  = "0";
                $pesan = "Kode salah";
            } else {
                $code  = "1";
                $pesan = "Sukses";
            }
            
        } else {
            $code  = "0";
            $pesan = "Nomor telepon tidak terdaftar!";
        }
        
        
        $data['code']        = $code;
        $data['pesan']       = $pesan;
        $data['id_konsumen'] = $id_konsumen->id_konsumen;
        
        echo json_encode($data);
    }
    
    function login()
    {
        $no_telp  = $this->input->post("no_telp");
        $password = $this->input->post("password");
        
        $status_login = $this->modelmu->login($no_telp, $password)->num_rows();
        $pesan        = "";
        
        if ($status_login == "1") {
            $pesan = "Sukses";
            
        } else if ($status_login == "0") {
            $pesan = "Kata sandi salah!";
        }
        
        $data['code']  = $status_login; //jika 0 maka gagal login, jika 1 sukses
        $data['pesan'] = $pesan;
        $data['data']  = $this->modelmu->login($no_telp, $password)->result();
        
        echo json_encode($data);
    }
    
    public function login_id()
    {
        $this->check_session($this->input->request_headers());

        $id_konsumen = $this->input->post("id_konsumen");
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->modelmu->login_id($id_konsumen);
        
        echo json_encode($data);
    }
    
    public function insert_code()
    {

        $id_kode      = date("YmdHis") . rand(1000, 10000);
        $kode    = rand(1000, 10000);
        $no_telp = $this->input->post("no_telp");
        
        $this->modelmu->insert_code($id_kode, $kode);
        
        require_once APPPATH . 'controllers/ctr_email.php';
        $myemail = new Ctr_email();
        
        $pesan_sms = "Kode SMS untuk OPA :" . $kode;
        
        //$myemail->send_email("ahmadzainuri94@gmail.com", "kirim email", $pesan_sms);   

                
        $status_no_telp = $this->modelmu->cek_no_telp($no_telp);

        if ($status_no_telp != "0") {
            $data['code']    = "0"; //jika 0 maka gagal login, jika 1 sukses
            $data['pesan']   = "No HP sudah terdaftar";

            echo json_encode($data);
        }  else {
            $data['id_kode'] = $id_kode;
            $data['code']    = "1"; //jika 0 maka gagal login, jika 1 sukses
            $data['pesan']   = "Sukses";
            echo json_encode($data);
            $myemail->send_sms($no_telp, $pesan_sms);
        }
    }

    function kirim_kode_forgot_password()
    {

        $id_kode = date("YmdHis") . rand(1000, 10000);
        $kode    = rand(1000, 10000);
        $no_telp = $this->input->post("no_telp");
        
        $this->modelmu->insert_code($id_kode, $kode);
        
        require_once APPPATH . 'controllers/ctr_email.php';
        $myemail = new Ctr_email();
        
        $pesan_sms = "Kode SMS untuk OPA :" . $kode;
        
        //$myemail->send_email("ahmadzainuri94@gmail.com", "kirim email", $pesan_sms);
        
        
        $status_no_telp = $this->modelmu->cek_no_telp($no_telp);

        if ($status_no_telp != "0") {
            $data['code']    = "1"; //jika 0 maka gagal login, jika 1 sukses
            $data['pesan']   = "Sukses";
            $data['id_kode'] = $id_kode;

            echo json_encode($data);
            $myemail->send_sms($no_telp, $pesan_sms);
        }  else {
            $data['id_kode'] = $id_kode;
            $data['code']    = "0"; //jika 0 maka gagal login, jika 1 sukses
            $data['pesan']   = "No HP tidak terdaftar";
            echo json_encode($data);
        }
        
    }

    function cek_no_telp()
    {
        $no_telp = $this->input->post("no_telp");

        // echo 'no telp : '  . $no_telp; die();
        //$myemail->send_email("ahmadzainuri94@gmail.com", "kirim email", $pesan_sms);
        
        
        $status_no_telp = $this->modelmu->cek_no_telp($no_telp);

        if (!isset($no_telp) || $no_telp == '') {
            $data['code']    = "0"; //jika 0 maka gagal login, jika 1 sukses
            $data['pesan']   = "Masukkan no hp yang valid";

            echo json_encode($data);
        } else if ($status_no_telp != "0") {
            $data['code']    = "0"; //jika 0 maka gagal login, jika 1 sukses
            $data['pesan']   = "No HP sudah terdaftar";

            echo json_encode($data);
        }  else {
            $data['code']    = "1"; //jika 0 maka gagal login, jika 1 sukses
            $data['pesan']   = "Sukses";
            echo json_encode($data);
        }
        
    }
    
    // public function forgot_password(){
    //     $no_telp   = $this->input->post("no_telp");
    
    //     $status   = $this->modelmu->cek_no_telp($no_telp,$password)->num_rows();
    //     $pesan = "";
    
    //     if($status == "1"){
    //         $pesan = ""
    //     }else{
    
    //     }
    // }
    
    public function new_password()
    {
        // $this->check_session($this->input->request_headers());
        $password    = $this->input->post("password");
        $id_konsumen = $this->input->post("id_konsumen");
        
        $this->modelmu->new_password($id_konsumen, $password);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        echo json_encode($data);
    }

    public function update_password()
    {
        $this->check_session($this->input->request_headers());
        $old_password   = $this->input->post("old_password");
        $password   = $this->input->post("password");
        $id_konsumen = $this->input->post("id_konsumen");

        $password_valid = $this->modelmu->check_password($id_konsumen, $old_password);
        if (!empty($password_valid)) {
            $this->modelmu->new_password($id_konsumen, $password);

            $data['code']  = "1";
            $data['pesan'] = "Berhasil";
        } else {
            $data['code']  = "0";
            $data['pesan'] = "Password lama salah";
        }

        echo json_encode($data);
    }
    
    public function get_radius()
    {
        // $this->check_session($this->input->request_headers());

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->modelmu->get_radius();
        
        echo json_encode($data);
    }

    public function get_provinsi()
    {
        // $this->check_session($this->input->request_headers());

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->modelmu->get_provinsi();
        
        echo json_encode($data);
    }
    
    public function get_kota()
    {

        $id_provinsi = $this->input->post("id_provinsi");
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->modelmu->get_kota($id_provinsi);
        
        echo json_encode($data);
    }
    
    public function get_jenis_usaha()
    {

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->modelmu->get_jenis_usaha();
        
        echo json_encode($data);
    }
    
    public function get_pemilik_usaha() 
    {
        $this->check_session($this->input->request_headers());

        $id_kategori = $this->input->get("id_kategori");
        $latitude    = $this->input->get("latitude");
        $longitude   = $this->input->get("longitude");
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->modelmu->get_pemilik($id_kategori, $latitude, $longitude);
        
        echo json_encode($data);
    }


    public function get_ekspedisi()
    {

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->model_api_ekspedisi->get_data_aktif()->result();
        
        echo json_encode($data);
    }


    public function get_agen_ekspedisi()
    {
        $lat = $this->input->get("lat");
        $lng = $this->input->get("lng");

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->model_api_ekspedisi->get_agen_terdekat($lat,$lng)->result();
        
        echo json_encode($data);
    }


    public function get_rajaongkir($origin, $destination, $weight, $courier)
    {
        $url    = 'https://api.rajaongkir.com/starter/cost';
        $fields = "origin=$origin&destination=$destination&weight=$weight&courier=$courier";
        $headers = array(
            'key: ' . KEY_RAJAONGKIR,
            'content-type: application/x-www-form-urlencoded'
        );
        $ch      = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        $result = curl_exec($ch);
        if ($result === FALSE) {
            $result = "salah";
        }
        curl_close($ch);
        return $result;
    }

    public function get_layanan_ekspedisi()
    {
        $lat = $this->input->get("lat");
        $lng = $this->input->get("lng");
        $origin = $this->input->get("origin");
        $destination = $this->input->get("destination");

        // echo "ok"; die();

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $items  = $this->model_api_ekspedisi->get_layanan_by_etd();
        $ekspedisies = $this->model_api_ekspedisi->get_agen_ekspedisi_terdekat($lat, $lng)->result();

        // echo json_encode($ekspedisies); die();

        $ongkirs = array();

        $offsets = [1, 4, 7, 14];
        // echo "[";
        foreach ($ekspedisies as $ekspedisi) {
            $result_json = self::get_rajaongkir($origin, $destination, 1, $ekspedisi->courier_slug);
            $result = json_decode($result_json);

            if ($result->rajaongkir->status->code == 200) {
                $results = $result->rajaongkir->results;
                $courier_slug = $results[0]->code;
                $costs = $results[0]->costs;
                // echo json_encode($costs) . "\n\n";
                foreach ($costs as $cost_obj) {
                    $cost_varian = $cost_obj->cost;

                    foreach ($cost_varian as $cost_item) {
                        $cost = $cost_item->value;
                        $cost_item->etd = str_replace(" HARI", "", $cost_item->etd);
                        $etd_splitted = split("-", $cost_item->etd);
                        $start = $etd_splitted[0];
                        $end = $etd_splitted[0];
                        if (count($etd_splitted) > 1)
                            $end = $etd_splitted[1];

                        $ongkir_obj = new stdClass();
                        $ongkir_obj->cost = $cost;
                        $ongkir_obj->etd_start = $start;
                        $ongkir_obj->etd_end = $e3ed;
                        $ongkir_obj->etd_text = $cost_item->etd;
                        $ongkir_obj->etd = $start . "-" . $end;
                        $ongkir_obj->courier_slug = $courier_slug;
                        $ongkir_obj->lat = $ekspedisi->lat;
                        $ongkir_obj->lng = $ekspedisi->lng;

                        $ongkirs[] = $ongkir_obj;
                    }
                }
            }
        }

        // echo ""; die();
        usort($ongkirs, array($this, "cmp_etd"));
        // echo json_encode($ongkirs); die();

        $layanans = array();

        $current_offsets_pointer = 0;
        $ekspedisi = new stdClass();

        $data_ekspedisi = array();
        if (count($ongkirs) > 0)  {
            $ekspedisi->etd_start = intval($ongkirs[0]->etd_start);
            $ekspedisi->etd_end = $offsets[$current_offsets_pointer];
            $ekspedisi->etd = $ekspedisi->etd_start . "-" . $ekspedisi->etd_end;
            $ekspedisi->data_ekspedisi = $data_ekspedisi;
        }

        $layanans[] = $ekspedisi;

        foreach ($ongkirs as $ongkir) {
            $item = null;
            foreach($items as $struct) {
                if ($ongkir->courier_slug == $struct->courier_slug) {
                    $item = $struct;
                    break;
                }
            }

            $ongkir = (object) array_merge((array) $ongkir, (array) $item);

            if ($ongkir->etd_end > $offsets[$current_offsets_pointer]) {
                $current_offsets_pointer++;

                $ekspedisi = new stdClass();
                $data_ekspedisi = array();
                $ekspedisi->etd_start = intval($ongkir->etd_start);
                $ekspedisi->etd_end = $offsets[$current_offsets_pointer];
                $ekspedisi->etd = $ekspedisi->etd_start . "-" . $ekspedisi->etd_end;
                $ekspedisi->data_ekspedisi = $data_ekspedisi;
                $layanans[] = $ekspedisi;
            }
            
            $ekspedisi->data_ekspedisi[] = $ongkir;
        }

        $layanan_items = array();
        foreach ($layanans as $layanan) {
            // echo count($layanan->data_ekspedisi) . "\n";
            if (count($layanan->data_ekspedisi) > 0) {
                $layanan_items[] = $layanan;
            }
        }

        // $layanans = array_values($layanans);
        
        $data['data'] = $layanan_items;
        echo json_encode($data);
    }


    function cmp_etd($a, $b)
    {
        return strcmp($a->etd_end, $b->etd_end);
    }

    // public function get_ongkir_ekspedisi() {

    //     $curl = curl_init();

    //     curl_setopt_array($curl, array(
    //       CURLOPT_URL => "https://api.rajaongkir.com/starter/province",
    //       CURLOPT_RETURNTRANSFER => true,
    //       CURLOPT_ENCODING => "",
    //       CURLOPT_MAXREDIRS => 10,
    //       CURLOPT_TIMEOUT => 30,
    //       CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    //       CURLOPT_CUSTOMREQUEST => "GET",
    //       CURLOPT_HTTPHEADER => array(
    //         "key: 739601ae9e5842190806ccb21035daf5"
    //       ),
    //     ));

    //     $response = curl_exec($curl);

    //     echo $response;

    //     $err = curl_error($curl);

    //     curl_close($curl);


    // }


    public function check_kiriman() {
        $json_distances = $this->input->get("distances");
        $distances = json_decode($json_distances);

        $lat         = $this->input->get("lat");
        $lng         = $this->input->get("lng");

        $kurir = $this->model_api_kurir->get_nearest_kurir($lat, $lng);
        $fee = $kurir->fee;

        // echo "fee: " . $fee; die();

        $ongkirs = array();
        foreach ($distances as $distance) {
            $ongkirs[] = intval($distance) * intval($fee);
        }

        $data['code']  = $fee == null ? "0" : "1";
        $data['pesan'] = $fee == null ? "Tidak ada kurir" : "Berhasil";
        $data['data'] = $ongkirs;

        echo json_encode($data);
    }


    public function insert_kiriman()
    {
        $this->check_session($this->input->request_headers());

        require_once APPPATH . 'controllers/ctr_email.php';
        // $myemail = new Ctr_email();

        $id_konsumen = $this->input->post("id_konsumen");
        $id_kurir = $this->input->post("id_kurir");
        $tipe = $this->input->post("tipe");
        $id_kiriman       = $id_konsumen . date("ymdHis");
        $jenis_alamat = $this->input->post("jenis_alamat");
        $alamat       = $this->input->post("alamat");
        $lat       = $this->input->post("lat");
        $lng       = $this->input->post("lng");
        $ket_alamat       = $this->input->post("ket_alamat");
        $keterangan       = $this->input->post("keterangan");
        $fee          = $this->input->post("fee");
        $total          = $this->input->post("total");
        $kirimans_json          = $this->input->post("kirimans");
        $kirimans = json_decode($kirimans_json);

        // echo $id_kiriman;die();
        $kurir = $this->model_api_kurir->get_nearest_kurir($lat, $lng);
        // echo json_encode($kurir); die();

        $this->model_api_kurir->insert_kiriman($id_kiriman, $kurir->id_kurir, $tipe, $id_konsumen, $jenis_alamat, $keterangan, $lat, $lng, $alamat, $jenis_alamat, $ket_alamat, $fee, floatval($total) + floatval($fee));


        if (intval($tipe) != self::TIPE_EKSPEDISI) {
            foreach ($kirimans as $kiriman) {
                $this->model_api_kurir->insert_detail_kiriman($id_kiriman, $kiriman->nama_penerima, $kiriman->keterangan, $kiriman->alamat, $kiriman->lat, $kiriman->lng, $kiriman->fee);
            }
        } else {
            foreach ($kirimans as $kiriman) {
                $this->model_api_kurir->insert_detail_kiriman_ekspedisi($id_kiriman, $kiriman->nama_penerima, $kiriman->keterangan, $kiriman->alamat, $kiriman->id_agen, $kiriman->id_layanan_ekspedisi, $kiriman->id_origin, $kiriman->kota_origin, $kiriman->id_destination, $kiriman->kota_destination, $kiriman->weight, $kiriman->fee, $kiriman->est_price, $kiriman->etd);
            }
        }


        // push notif
        $data_kiriman    = $this->model_api_kurir->notif_get_kiriman($id_kiriman);
        $data_konsumen  = $this->model_api_kurir->notif_get_konsumen_kiriman($id_kiriman);

        $data["arr_token"] = array();
        $arr_token = array();

        if ($kurir != null) {
            array_push($arr_token, $kurir->token);
        } else {
            $data['code']  = "0";
            $data['pesan'] = "Kurir tidak ditemukan";
            $data['data'] = array();
        }

        $push_message = "Kiriman Baru dari " . $data_konsumen->nama;

        // echo $push_message; die();
            
        $message = array(
            "jenis" => "3",
            "id_kurir" => $id_kurir,
            "nama_kurir" => $kurir->nama,
            "id_kiriman" => $id_kiriman,
            "notif" => $push_message,
            "order" => json_encode($data_kiriman)
        );


        $data['arr_token'] = $arr_token;
        $pushStatus = $this->send_message_to_kurir($arr_token, $message);
        $data['pushKurirStatus'] = $pushStatus;

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']['id_kiriman'] = $id_kiriman;

        echo json_encode($data);
    }


    public function set_cancel_req() {
        $id_kiriman = $this->input->post("id_kiriman");
        $cancel_req = $this->input->post("cancel_req");

        $this->modelmu->set_cancel_req($id_kiriman, $cancel_req);

        $data['data']  = array();
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        echo json_encode($data);
    }


    public function cancel_kiriman() {
        $this->check_session($this->input->request_headers());

        $id_kurir = $this->input->post("id_kurir");
        $id_konsumen = $this->input->post("id_user");
        $id_kiriman = $this->input->post("id_kiriman");


        $kiriman    = $this->model_api_kurir->notif_get_kiriman($id_kiriman);

        $data['code']     = "1";
        $data['pesan']    = "Sukses";
        $data['data'] = array();

        

        if (intval($kiriman->status) == 4) {
            $data['data']  = array();
            $data['code']  = "2";
            $data['pesan'] = "Kiriman telah diselesaikan oleh kurir";
            echo json_encode($data);
            die();
        } else if (intval($kiriman->status) == 2) {
            $this->modelmu->cancel_kiriman($id_kiriman,2);
        } else {
            $this->modelmu->cancel_kiriman($id_kiriman,0);
        }
        // echo "ok"; die();


        $data_kiriman    = $this->model_api_kurir->notif_get_kiriman($id_kiriman);

        $kiriman_items = $this->model_api_kurir->get_detail_kiriman($id_kiriman, $data_kiriman->tipe);
        $data_kurir  = $this->model_api_kurir->notif_get_pemilik($id_kurir);
        $data_konsumen = $this->modelmu->notif_get_konsumen($id_konsumen);
        // echo json_encode($data_konsumen); die();
        $kurir = $data_kurir[0];
        // echo json_encode($kurir); die();

        
        $arr_token = array();
            array_push($arr_token, $kurir->token);
            $id_kurir = $kurir->id_kurir;

        $push_message = "Kiriman dari " . $data_konsumen->nama . " dibatalkan";

        $message = array(
            "jenis" => "3",
            "id_kurir" => $id_kurir,
            "nama_kurir" => $kurir->nama,
            "id_kiriman" => $id_kiriman,
            "notif" => $push_message,
            "order" => json_encode($data_kiriman)
            );
        
        $pushStatus = $this->send_message($arr_token, $message, $kurir->tipe);
        
        $arr_token = array();
        array_push($arr_token, $kurir->token);

        $data['arr_token'] = $arr_token;
        $pushStatus = $this->send_message_to_kurir($arr_token, $message);
        
        $data["gcm_result"] = $pushStatus;
        
        echo json_encode($data);
    }
    

    
    public function test_get_pemilik_usaha() 
    {
        $this->check_session($this->input->request_headers());

        $id_kategori = $this->input->get("id_kategori");
        $latitude    = $this->input->get("latitude");
        $longitude   = $this->input->get("longitude");
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->modelmu->test_get_pemilik($id_kategori, $latitude, $longitude);
        
        echo json_encode($data);
    }


    public function get_pedagang_keliling() 
    {
        $this->check_session($this->input->request_headers());

        $id_kategori = $this->input->get("id_kategori");
        $latitude    = $this->input->get("latitude");
        $longitude   = $this->input->get("longitude");
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->modelmu->get_pedagang($id_kategori, $latitude, $longitude);
        
        echo json_encode($data);
    }

    public function check_session($request_headers) {

        // $id_konsumen = $request_headers["Id-konsumen"];
        // $token = $request_headers["Token"];
        // $session = $this->modelmu->check_session($id_konsumen, $token);

        // if ($session == null) {
        //     $data['code']  = "0";
        //     $data['pesan'] = "Session invalid";
        //     echo json_encode($data);
        //     die();
        // }
    }
    

    public function get_preactive_pemilik_usaha() 
    {

        $id_kategori = $this->input->get("id_kategori");
        $latitude    = $this->input->get("latitude");
        $longitude   = $this->input->get("longitude");
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->modelmu->get_preactive_pemilik($id_kategori, $latitude, $longitude);
        
        echo json_encode($data);
    }

    
    public function daftar_preaktif() {
        $id_pemilik = $this->modelmu->get_random_num();
        $id_partner = "";
        $nama = $this->input->post("nama");
        $jargon = "";
        $kontak = $this->input->post("kontak");
        $kontak_sms = "";
        $kontak_sms2 = "";
        $password = "";
        $alamat = $this->input->post("alamat");
        $email = $this->input->post("email");
        $id_jenis = '0';
        $id_kategori = '0';
        $id_radius = '0';
        $min_antar = '0';
        $fee = '0';
        $lat = $this->input->post("lat");
        $lng = $this->input->post("lng");
        $id_provinsi = $this->input->post("id_provinsi");
        $id_kota = $this->input->post("id_kota");

        $username   = "User Baru";
        $no_telp = $kontak;
        $nama_lokasi = "";
        $kode_sms = "11";
        $foto       = "";
        
        $errors = [];

        $foto3 = "";

        
        if (isset($_FILES['form_file']) && !empty($_FILES['form_file'])) {
            $foto = $_FILES["form_file"]['name'];
            
            if ($foto != "") {
                // echo "foto ada " . $foto;
                $foto2 = explode(".", $foto);
                $foto3 = $id_pemilik . "." . $foto2[count($foto2) - 1];
            } 
                if ($_FILES['form_file']['error'] != 4) {
                    $exp                     = explode(".", $_FILES["form_file"]['name']);
                    $ext                     = end($exp);
                    $config['upload_path']   = PATH_IMAGE_PEMILIK;
                    $config['allowed_types'] = '*';
                    $config['file_name']     = $foto3;
                    if (!is_dir(PATH_IMAGE_PEMILIK)) {
                        mkdir(PATH_IMAGE_PEMILIK, 0777, true);
                    }
                    
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    
                    if (!$this->upload->do_upload('form_file')) {
                        $errors[] = $this->upload->display_errors();
                    }
                }
            }

        $pemilik = $this->model_api_pemilik->get_id_pemilik_by_no_telp($no_telp, $username);

        if ($pemilik == null) {
            $id_pemilik_return = $this->model_api_pemilik->tambah_toko_preaktif($id_pemilik, $id_partner, $no_telp, $kontak_sms, $kontak_sms2, $password, $username, $kode_sms, $id_provinsi, $id_kota, $id_radius, $min_antar, $fee, $email);
            if ($id_pemilik_return > 0) {

                $arr = array();
                $item = new stdClass();
                $item->id_kategori = $id_kategori;
                $item->id_jenis = 1;
                $arr[] = $item;

                $json = json_encode($arr);
            

                $this->model_api_pemilik->update_profile($id_pemilik, $nama, $alamat, $jargon, $id_radius, $kontak_sms, $kontak_sms2, $fee);
                $this->model_api_pemilik->update_kategori_usaha_awal($id_pemilik, $json);
                $this->model_api_pemilik->isi_lokasi_awal($id_pemilik,$lat,$lng,$alamat);
                $this->model_api_pemilik->set_foto($id_pemilik, $foto3);
                // $this->modelmu->copy_to_histori($id_pemilik);

                 $data = array("status" => "berhasil ditambahkan", "code" => 1, "data" => $id_pemilik_return);
            } else {
                 $data = array("status" => "gagal ditambahkan", "code" => 0, "data" => null);
            }
        } else {
            $data = array("status" => "Nomor HP atau Alias sudah dipakai", "code" => 0, "data" => null);
        }

        echo json_encode($data);
    }
    

    public function get_order()
    {
        // $this->check_session($this->input->request_headers());

        $id_konsumen = $this->input->get("id_konsumen");
        // $id_konsumen = "1605121015057396";
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $orders  = $this->modelmu->get_order($id_konsumen);
        $data['data'] = $orders;
        // $data['data']['pemilik'] = array();
        // $data['data']['orders'] = $orders;
        // if (count($orders) > 0) {
        //     $pemilik = $this->modelmu->notif_get_pemilik($orders[0]->id_pemilik);
        //     if (count($pemilik) > 0) {
        //         $data['data']['pemilik'] = $pemilik[0];
        //     }
        // }
        // echo $id_konsumen;
        echo json_encode($data);
    }


    public function get_undone_order()
    {
        // $this->check_session($this->input->request_headers());

        $id_konsumen = $this->input->get("id_konsumen");
        // $id_konsumen = "1605121015057396";
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $orders  = array_merge($this->modelmu->get_undone_order($id_konsumen), $this->modelmu->get_undone_kiriman($id_konsumen));
        $data['data'] = $orders;
        // $data['data']['pemilik'] = array();
        // $data['data']['orders'] = $orders;
        // if (count($orders) > 0) {
        //     $pemilik = $this->modelmu->notif_get_pemilik($orders[0]->id_pemilik);
        //     if (count($pemilik) > 0) {
        //         $data['data']['pemilik'] = $pemilik[0];
        //     }
        // }
        // echo $id_konsumen;
        echo json_encode($data);
    }

    public function get_done_order()
    {
        // $this->check_session($this->input->request_headers());

        $id_konsumen = $this->input->get("id_konsumen");
        // $id_konsumen = "1605121015057396";
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $orders  = array_merge($this->modelmu->get_done_order($id_konsumen), $this->modelmu->get_done_kiriman($id_konsumen));
        $data['data'] = $orders;
        // $data['data']['pemilik'] = array();
        // $data['data']['orders'] = $orders;
        // if (count($orders) > 0) {
        //     $pemilik = $this->modelmu->notif_get_pemilik($orders[0]->id_pemilik);
        //     if (count($pemilik) > 0) {
        //         $data['data']['pemilik'] = $pemilik[0];
        //     }
        // }
        // echo $id_konsumen;
        echo json_encode($data);
    }


    public function get_kiriman()
    {
        // $this->check_session($this->input->request_headers());

        $id_konsumen = $this->input->get("id_konsumen");
        // $id_konsumen = "1605121015057396";
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $kirimans  = $this->model_api_kurir->get_kiriman($id_konsumen);
        $data['data'] = $kirimans;
        // $data['data']['pemilik'] = array();
        // $data['data']['orders'] = $orders;
        // if (count($orders) > 0) {
        //     $pemilik = $this->modelmu->notif_get_pemilik($orders[0]->id_pemilik);
        //     if (count($pemilik) > 0) {
        //         $data['data']['pemilik'] = $pemilik[0];
        //     }
        // }
        // echo $id_konsumen;
        echo json_encode($data);
    }

    public function get_order_antri()
    {
        // $this->check_session($this->input->request_headers());

        $id_konsumen = $this->input->get("id_konsumen");
        // $id_konsumen = "1605121015057396";
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $orders  = $this->modelmu->get_order_antri($id_konsumen);
        $data['data'] = $orders;
        // $data['data']['pemilik'] = array();
        // $data['data']['orders'] = $orders;
        // if (count($orders) > 0) {
        //     $pemilik = $this->modelmu->notif_get_pemilik($orders[0]->id_pemilik);
        //     if (count($pemilik) > 0) {
        //         $data['data']['pemilik'] = $pemilik[0];
        //     }
        // }
        // echo $id_konsumen;
        echo json_encode($data);
    }
    
    public function update_nama()
    {
        // $this->check_session($this->input->request_headers());
        $nama        = $this->input->post("nama");
        $id_konsumen = $this->input->post("id_konsumen");
        
        $this->modelmu->update_nama($id_konsumen, $nama);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        echo json_encode($data);
    }
    
    public function update_alamat()
    {
        // $this->check_session($this->input->request_headers());
        $alamat      = $this->input->post("alamat");
        $id_konsumen = $this->input->post("id_konsumen");
        
        $this->modelmu->update_alamat($id_konsumen, $alamat);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        echo json_encode($data);
    }


    public function update_no_telp()
    {
        // $this->check_session($this->input->request_headers());
        $no_telp      = $this->input->post("no_telp");
        $id_konsumen = $this->input->post("id_konsumen");


        $data['code']  = "1";
        $data['pesan'] = "sukses";

        $konsumen = $this->modelmu->get_id_konsumen_by_no_telp($no_telp);

        if ($konsumen != null) {
            if ($konsumen->id_konsumen != $id_konsumen) {
                $data['code']  = "0";
                $data['pesan'] = "Phone number already used";
            } else {
                $data['code']  = "1";
                $data['pesan'] = "Phone number unchanged";
            }
        } else {
            $data['code']  = "1";
            $data['pesan'] = "sukses";
        
            $this->modelmu->update_no_telp($id_konsumen, $no_telp);
        }
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        echo json_encode($data);
    }

    public function update_provinsi_kota()
    {
        // $this->check_session($this->input->request_headers());
        $id_provinsi = $this->input->post("id_provinsi");
        $id_kota     = $this->input->post("id_kota");
        $id_konsumen = $this->input->post("id_konsumen");
        
        $this->modelmu->update_provinsi_kota($id_konsumen, $id_provinsi, $id_kota);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        echo json_encode($data);
    }
    
    public function update_lokasi()
    {
        $this->check_session($this->input->request_headers());
        $id_konsumen = $this->input->post("id_konsumen");
        $lat         = $this->input->post("lat");
        $lng         = $this->input->post("lng");
        
        $this->modelmu->update_lokasi($id_konsumen, $lat, $lng);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        echo json_encode($data);
    }


    public function get_lokasi_aktual($id_pemilik) {

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->modelmu->get_lokasi_aktual($id_pemilik)->first_row();
        echo json_encode($data);
    }

    public function check_order() {
        $json         = $this->input->post("json");
        $check_res = $this->modelmu->check_order($json);

        echo json_encode($check_res);
    }
    
    public function insert_order()
    {
        $this->check_session($this->input->request_headers());

        require_once APPPATH . 'controllers/ctr_email.php';
        // $myemail = new Ctr_email();

        $jenis_alamat = $this->input->post("jenis_alamat");
        $alamat       = $this->input->post("alamat");
        $ket_alamat       = $this->input->post("ket_alamat");
        $lat          = $this->input->post("lat");
        $lng          = $this->input->post("lng");
        $id_konsumen  = $this->input->post("id_konsumen");
        $id_pemilik   = $this->input->post("id_pemilik");
        $total        = $this->input->post("total");
        $charge        = $this->input->post("charge");
        $fee        = $this->input->post("fee");
        // $include_fee        = $this->input->post("include_fee");
        // $include_fee_note        = $this->input->post("include_fee_note");
        $json         = $this->input->post("json");
        

        $data_pemiliks  = $this->modelmu->notif_get_pemilik($id_pemilik);
        $data_pemilik = new stdClass();
            
        $data_konsumen = $this->modelmu->notif_get_konsumen($id_konsumen);
        // $data_konsumen = null;
        if ($data_konsumen == null) {
            $data['code']  = '0';
            $data['pesan'] = 'USER_NOT_FOUND';
            echo json_encode($data);
            return;
        }
        // $fee = $data_pemilik->fee;

        $id_order = '0';

        if (count($data_pemiliks) > 0) {
            $data_pemilik = $data_pemiliks[0];
            if ($data_pemilik->status_toko == "2" || $data_pemilik->status_toko == "3") {
                 $data["code"] = "0";
                $data["pesan"] = "Toko tutup";
                $data["data"] = new stdClass();
                echo json_encode($data);
                return;
            }

            /*$id_konsumen    = "1604261842332610";
            $id_pemilik     ="1604261826458808";
            $total          = "120000";
            $json           = '[{"id_produk":"85495","kuantitas":"3","harga":"10000","subtotal":"30000"}]';*/
            
            $id_order       = $id_konsumen . date("ymdHis");
            $fee_order      = "";
            $kredit_pemilik = $this->modelmu->get_kredit($id_pemilik);
            //$kredit_pemilik = "1000000";
            $pesan          = "";
            $code           = "";
            
            foreach ($this->modelmu->get_fee() as $value) {
                if (intval($total) >= intval($value->batas_bawah)) {
                    $fee_order = $value->nominal;
                    break;
                }
            }

            $konsumen_valid = false;

            $promo = $this->model_promo_old->get_promo($id_pemilik)->row();

            $promo_konsumen_valid = false;
            $promo_valid = false;
            if (!empty($promo)) {
                $promo_code = $promo->code;
                $promo_valid = true;
            }

            if ($promo_valid) {
                $promo_konsumen = $this->model_promo_old->get_promo_order_konsumen($id_konsumen, $promo->promo_id, $id_pemilik);
                $promo_konsumen_valid = count($promo_konsumen_valid) < 2;
            }
            $konsumen_order_id = 0;

            $check_res = $this->modelmu->check_order($json);
            $arr = json_decode($json);
            usort($arr, array($this, "cmp"));

            $unavalaible_produks = array();
            $k = 0;
            foreach ($check_res as $check) {
                if ((intval($check->stok) < intval($arr[$k]->kuantitas) && intval($data_pemilik->tipe) != self::TIPE_PEDAGANG_KELILING) || intval($check->stok) <= 0) {
                    $unavalaible_produks[] = $check;
                }
                $k++;
            }

            $produk_unavailable = false;
            if (count($unavalaible_produks) > 0) {
                $produk_unavailable = true;
            }

            // if ($promo_konsumen_valid ) {
            //             echo "konsumen valid\n";
            // } 

            // if ($promo_valid) {
            //     echo "promo_valid";

            // }
            // die();

            $containsProdukPromo = false;
            $promo_produk_qty = 0; 
            $id_produks = "";
            $arr = json_decode($json);
            for($i=0;$i<count($arr);$i++){
                $id_produks = $id_produks . "'" . $arr[$i]->id_produk . "', ";
            }
            $id_produks = $id_produks . "|||";
            $id_produks = str_replace(", |||", "", $id_produks);
            $produk_promo = $this->model_promo_old->get_produk_promo_by_ids($id_produks);
            if (count($produk_promo) > 0) $containsProdukPromo = true;

            foreach ($produk_promo as $produk_promo_item) {
                for($i=0;$i<count($arr);$i++){
                   if (intval($produk_promo_item->id_produk) == intval($arr[$i]->id_produk)) {
                        $promo_produk_qty =  floatval($arr[$i]->kuantitas);
                    }
                }
            }

            $credit_unsufficient = false;

            if (!$produk_unavailable) {
                if (intval($fee_order) > intval($kredit_pemilik) &&  intval($fee_order) == 0) {
                    $code  = "0";
                    $pesan = "Kredit pemilik usaha tidak mencukupi";
                    $credit_unsufficient = true;
                } else {
                    $code  = "1";
                    $pesan = "Sukses ";


                    $konsumen_order_id = $this->modelmu->insert_order($id_order, $id_konsumen, $id_pemilik, $total, $json, $fee, $jenis_alamat, $alamat, $ket_alamat, $lat, $lng, $charge, $data_pemilik->include_fee, $data_pemilik->include_fee_note, $data_pemilik->tipe);

                    if ($promo_konsumen_valid && $promo_valid && $containsProdukPromo) {
                        $this->model_promo_old->insert_promo_konsumen($id_order, $promo->promo_id);
                        // echo "promo_produk_qty: " . $promo_produk_qty . "\n"; 
                        if (floatval($promo_produk_qty) >= 2) {
                            $this->model_promo_old->insert_promo_konsumen($id_order, $promo->promo_id);
                        }
                        // die();
                        $this->model_promo_old->reduce_stok_promo($promo->id, $promo_produk_qty);
                    } 
                }
            } else {
                    $code  = "0";
                    $pesan = "Stok tidak mencukupi";
            }
            
            //json yang berisi detail order. contoh isi : 
            //[{"id_produk":"21","kuantitas":"3","harga":"10000","subtotal":"30000"},{"id_produk":"23","kuantitas":"2","harga":"3000","subtotal":"6000"}]

            // if ($konsumen_valid) {
            //     $this->model_promo_old->insert_histori_transaksi_promo($id_konsumen, $promo->id, $id_order);
            // }
            
            $data['code']  = $code;
            $data['pesan'] = $pesan;
            $data['id_order'] = $id_order;
            $data['unavalaible_produks'] = $unavalaible_produks; 

            if ($produk_unavailable || $credit_unsufficient) {
                echo json_encode($data);
                return;
            }
            // push notif
            $data_order    = $this->modelmu->notif_get_order($id_order);
            $id_pemilik    = "";
            
            $arr_token = array();
            // foreach ($data_pemilik as $row) {
                array_push($arr_token, $data_pemilik->token);
                $id_pemilik = $data_pemilik->id_pemilik;
            // }

            $push_message = "Order Baru dari " . $data_konsumen->nama;
            if (intval($data_pemilik->tipe) == self::TIPE_PEDAGANG_KELILING) {
                $push_message = "Antrian Baru dari " . $data_konsumen->nama;
            }
            
            $message = array(
                "jenis" => "0",
                "id_pemilik" => $id_pemilik,
                "nama_pemilik" => $data_pemilik->nama,
                "id_order" => $id_order,
                "notif" => $push_message,
                "order" => json_encode($data_order)
                );

            $data["arr_token"] = $arr_token;

            $data['pemilik_tipe'] = $data_pemilik->tipe . " == " . self::TIPE_PEDAGANG_KELILING;

            $data['pushStatus'] = $this->send_message($arr_token, $message, $data_pemilik->tipe);

            if (intval($data_pemilik->tipe) != self::TIPE_PEDAGANG_KELILING) {
                $kurir = $this->model_api_kurir->get_kurir($data_pemilik->kontak_sms2);
                $arr_token = array();
                foreach ($kurir as $row) {
                    array_push($arr_token, $row->token);
                }
                $data['arr_token'] = $arr_token;
                $pushStatus = $this->send_message_to_kurir($arr_token, $message);
                $data['pushKurirStatus'] = $pushStatus;
            }
            

            $pesan_sms = "OPA\n" . $data_pemilik->nama . "\n" . $data_konsumen->nama . "\n";

            // if (intval($jenis_alamat) == 1) {
                $pesan_sms = $pesan_sms . $alamat;
            // } else {
            //     $pesan_sms = $pesan_sms . "Lihat pada map untuk melihat lokasi pengantaran.";
            // }

            $pesan_sms = $pesan_sms ."\nJml Belanja: Rp " . ($total + $charge) . ".\n";

            if (floatval($fee) > 0) {
                $pesan_sms = $pesan_sms . "\nOngkir: Rp " . $fee . ".\n";
            }

            $pesan_sms = $pesan_sms . "Total Tagihan: Rp " . (intval($fee) + ($total + $charge));

            
            $sparateTop = str_pad("", 50, "-", STR_PAD_BOTH);
            $sparate = str_pad("", 28, "-", STR_PAD_BOTH);



            $order = self::get_order_detail($id_order);

            $detail_orders = $order->detail_order;
            $detail_order = $detail_orders[0];
            $data_orders = $order->data_order;
            $data_order = $data_orders[0];
            // $data_pemiliks = $order->data_pemilik;
            // $data_pemilik = $data_pemiliks[0];
            // echo json_encode($detail_order) . "\n\n"; 
            // echo json_encode($data_order) . "\n\n"; 
            // echo json_encode($data_pemilik) . "\n\n"; 
            // die();

            $msg = "<i>" . self::tgl_indo($data_order->tanggal_order) . "</i>\n";
            $msg .= "#Inv:" . $data_order->id_order . "\n";
            $msg .= "\n";
            $msg .= "<b>Toko :</b>\n";
            $msg .= $data_pemilik->nama  . "\n";
            $msg .= $data_pemilik->alamat . "\n";
            $msg .= $data_pemilik->kontak_sms . "\n";
            $msg .= "\n";
            $msg .= "<b>Tujuan : </b>\n";
            $msg .= "Lokasi Sekarang\n";
            $msg .= "\n";
            $msg .= "<b>Penerima : </b>\n";
            $msg .= $data_order->nama .  "\n";
            $msg .= "\n";
            $msg .= "<b>Alamat : </b>\n";
            $msg .= $data_order->alamat_antar . "\n";
            $msg .= $data_order->kontak . "\n";
            $msg .= "\n";
            $msg .= "<b>Keterangan : </b>\n";
            $msg .= ($data_order->ket_alamat != null ? $data_order->ket_alamat : "-" ). "\n";
            $msg .= "\n";
            $msg .= "\n";
            $msg .= $sparateTop."\n";

            $j = 1;
            foreach ($detail_orders as $detail) {
                $msg .= "<pre>" .$j . ". " . $detail->nama_produk . " \n";
                $msg .= $detail->kuantitas ." x " . self::rupiah(intval($detail->harga)) .str_repeat(".",(18-strlen($detail->kuantitas ." x " . $detail->harga))) . " " . self::rupiah((intval($detail->kuantitas) * intval($detail->harga))) . "</pre>\n";
                $j++;
            }
            $msg .= $sparateTop."\n";
            $total_idr = self::rupiah((intval($fee) + ($total + $charge)));
            $t_charge = self::rupiah($charge);
            $t_fee = self::rupiah(intval($fee));
            $total = "TOTAL".str_repeat(" ",(20-strlen("TOTAL"))) . $total_idr;
            $msg .= "<pre>ADMIN APLIKASI".str_repeat(" ",(strlen($total)-strlen("ADMIN APLIKASI") - strlen($t_charge) )). $t_charge. "</pre>\n";
            $msg .= "<pre>ONGKOS KIRIM".str_repeat(" ",(strlen($total)-strlen("ONGKOS KIRIM") - strlen($t_fee) )) . $t_fee. "</pre>\n";
            
            $msg .= "<pre>".$total."</pre>\n";
            $msg .= "\n";
            $msg .= "\n";
            $msg .= "Terima kasih sudah belanja di OPA";
            $msg .= "\n";

            // echo $msg; die();

            log_message('error', "send_telegram : startting ");
            $data['sms_to'] = $msg;

            if ($code != "0") {
                if ($data_pemilik->kontak_sms != null && trim($data_pemilik->kontak_sms) != '') {
                    self::send_sms($data_pemilik->kontak_sms, $pesan_sms);
                    $data['sms_to'] = $data_pemilik->kontak_sms;

                    if ($data_pemilik->kontak_sms2 != null && trim($data_pemilik->kontak_sms2) != '' && $data_pemilik->kontak_sms2 != $data_pemilik->kontak_sms) {
                        self::send_sms($data_pemilik->kontak_sms2, $pesan_sms);
                        $data['sms_to'] = $data_pemilik->kontak_sms2;
                    }
                } else {
                    self::send_sms($data_pemilik->kontak, $pesan_sms);
                    $data['sms_to'] = $data_pemilik->kontak;
                }
                log_message('error', "send_telegram : ".$id_order." -> ".$data['sms_to'] . " -> ". $lat ." -> ".$lng ." -> ". $data_pemilik->id_pemilik);
            }

        } else {

            $data["code"] = "0";
            $data["pesan"] = "Toko tutup";
            $data["data"] = new stdClass();
        }   

        // $data['tele_id'] = self::send_telegram($id_order, $data['sms_to'], $msg, $lat, $lng, $data_pemilik->id_pemilik);
        


        echo json_encode($data);
        // echo "\nyaya";


    }

    function tgl_indo($tanggal){;
        $timestamp = strtotime($tanggal);
        $day = date("D", $timestamp);
         // $day = $date->format("d");

        $bulan = array (
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );

        $hari = array (
            'Mon' =>   'Senin',
            'Tue' => 'Selasa',
            'Wed' => 'Rabu',
            'Thu' => 'Kamis',
            'Fri' => 'Jumat',
            'Sat' => 'Sabtu',
            'Sun' => 'Mingu'
        );
        $pisahkan = explode(' ', $tanggal);
        $pecahkan = explode('-', $pisahkan[0]);
        $jamkan = explode(':', $pisahkan[1]);
        
        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun
     
        return $hari[$day] . ", " . $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0] . " / " . $jamkan[0] . ":" . $jamkan[1];
    }

    function rupiah($angka){
    
        $hasil_rupiah = "" . number_format($angka,2,',','.');
        $hasil_rupiah = str_replace(",00", "", $hasil_rupiah);
        return $hasil_rupiah;
     
    }

    public function cmp($a, $b)
    {
        return strcmp($a->id_produk, $b->id_produk);
    }
    

   public function get_charge()
   {
        // $this->check_session($this->input->request_headers());
        $fee = $this->modelmu->get_fee();
        $data['code']     = "1";
        $data['pesan']    = "Sukses";
        $data['data'] = $fee;

        echo json_encode($data);
   }


   public function insert_order_promo()
    {
        // $this->check_session($this->input->request_headers());
        $jenis_alamat = $this->input->post("jenis_alamat");
        $alamat       = $this->input->post("alamat");
        $ket_alamat       = $this->input->post("ket_alamat");
        $lat          = $this->input->post("lat");
        $lng          = $this->input->post("lng");
        $id_konsumen  = $this->input->post("id_konsumen");
        $id_pemilik   = $this->input->post("id_pemilik");
        $total        = $this->input->post("total");
        $charge        = $this->input->post("charge");
        $json         = $this->input->post("json");

        // echo $charge . ", fee: " . $fee;
        
        /*$id_konsumen    = "1604261842332610";
        $id_pemilik     ="1604261826458808";
        $total          = "120000";
        $json           = '[{"id_produk":"85495","kuantitas":"3","harga":"10000","subtotal":"30000"}]';*/
        
        $id_order       = $id_konsumen . date("ymdHis");
        $fee_order      = "";
        $kredit_pemilik = $this->modelmu->get_kredit($id_pemilik);
        //$kredit_pemilik = "1000000";
        $pesan          = "";
        $code           = "";
        
        foreach ($this->modelmu->get_fee() as $value) {
            if (intval($total) >= intval($value->batas_bawah)) {
                $fee_order = $value->nominal;
                break;
            }
        }


        $konsumen_order_id = 0;

        if (intval($fee_order) > intval($kredit_pemilik) && !$konsumen_valid &&  intval($fee_order) == 0) {
            $code  = "0";
            $pesan = "Kredit pemilik usaha tidak mencukupi";
        } else {
            $code  = "1";
            $pesan = "Sukses ";
            $konsumen_order_id = $this->modelmu->insert_order($id_order, $id_konsumen, $id_pemilik, $total, $json, $fee_order, $jenis_alamat, $alamat, $ket_alamat, $lat, $lng, $charge);
        }
        
        //json yang berisi detail order. contoh isi : 
        //[{"id_produk":"21","kuantitas":"3","harga":"10000","subtotal":"30000"},{"id_produk":"23","kuantitas":"2","harga":"3000","subtotal":"6000"}]

        if ($konsumen_valid) {
            $this->model_promo_old->insert_histori_transaksi_promo($id_konsumen, $promo->id, $id_order);
        }
        
        $data['code']  = $code;
        $data['pesan'] = $pesan;
        $data['id_order'] = $id_order;
        
        //push notif
        $data_pemilik  = $this->modelmu->notif_get_pemilik($id_pemilik);
        $data_konsumen = $this->modelmu->notif_get_konsumen($id_konsumen);
        $data_order    = $this->modelmu->notif_get_order($id_order);
        $id_pemilik    = "";
        
        $arr_token = array();
        foreach ($data_pemilik as $row) {
            array_push($arr_token, $row->token);
            $id_pemilik = $row->id_pemilik;
        }
        
        $message = array(
            "jenis" => "0",
            "id_pemilik" => $id_pemilik,
            "id_order" => $id_order,
            "notif" => "Order baru dari konsumen " . $data_konsumen->nama,
            "order" => json_encode($data_order)
            );
        
        $pushStatus = $this->send_message($arr_token, $message, $data_pemilik[0]->tipe);
        
        echo json_encode($data);

    }

    public function cari_produk() {
        // $this->check_session($this->input->request_headers());

        // header("Content-type: application/json");
        // date_default_timezone_set("Asia/Jakarta");

        $q = $this->input->get("q");
        $page = $this->input->get("page");
        $lat = $this->input->get("lat");
        $lng = $this->input->get("lng");
        $tipe = $this->input->get("tipe");
        $limit = $this->input->get("limit");
        // echo $keyword; die();

        $produks = $this->modelmu->cari_produk($q, $page, $lat, $lng, $limit, $tipe);


        $data['code']     = "1";
        $data['pesan']    = "Sukses";
        $data['data']       = $produks;
        
        echo json_encode($data);
    }

    public function cari_produk_promo() {
        // $this->check_session($this->input->request_headers());

        // header("Content-type: application/json");
        // date_default_timezone_set("Asia/Jakarta");

        $q = $this->input->get("q");
        $page = $this->input->get("page");
        $lat = $this->input->get("lat");
        $lng = $this->input->get("lng");
        $limit = $this->input->get("limit");
        // echo $keyword; die();

        $produks = $this->modelmu->cari_produk_promo($q, $page, $lat, $lng, $limit);


        $data['code']     = "1";
        $data['pesan']    = "Sukses";
        $data['data']       = $produks;
        
        echo json_encode($data);
    }

    public function cari_pemilik_produk() {
        // $this->check_session($this->input->request_headers());

        // header("Content-type: application/json");
        // date_default_timezone_set("Asia/Jakarta");

        $q = $this->input->get("q");
        $page = $this->input->get("page");
        $lat = $this->input->get("lat");
        $lng = $this->input->get("lng");
        $limit = $this->input->get("limit");
        // echo $keyword; die();

        $produks = $this->modelmu->cari_pemilik_produk($q, $page, $lat, $lng, $limit);


        $data['code']     = "1";
        $data['pesan']    = "Sukses";
        $data['data']       = $produks;
        
        echo json_encode($data);
    }


    public function cari_produk_by_pemilik() {
        // $this->check_session($this->input->request_headers());

        // header("Content-type: application/json");
        // date_default_timezone_set("Asia/Jakarta");

        $q = $this->input->get("q");
        $id_pemilik = $this->input->get("id_pemilik");
        $page = $this->input->get("page");

        if (intval($page) == 1) {
            $page = 0;
        } 
        // echo $keyword; die();
            

        $info = $this->model_api_pemilik->get_pemilik($id_pemilik);

        if (count($info) > 0) {
            $promos = $this->model_merchant_promo->get_promo_by_pemilik($id_pemilik)->result();
            // echo json_encode($promos); die();

            $id_promos = "";
            foreach ($promos as $promo) {
                $id_promos = $id_promos . $promo->id . ", ";
            }

            if ($id_promos != "") {
                $id_promos = $id_promos . "HEHEHE1234";
                $id_promos = str_replace(", HEHEHE1234", "", $id_promos);
            }

            // echo $id_promos; die();

            $produks = $this->modelmu->cari_produk_by_pemilik($q, $id_pemilik, $info[0]->tipe, $page, $id_promos);
        }


        $data['code']     = "1";
        $data['pesan']    = "Sukses";
        $data['data']       = $produks;
        
        echo json_encode($data);
    }


    public function cancel_order() {
        $this->check_session($this->input->request_headers());

        $id_pemilik = $this->input->post("id_pemilik");
        $id_konsumen = $this->input->post("id_user");
        $id_order = $this->input->post("id_order");

        $orders    = $this->modelmu->notif_get_order($id_order);

        $data['code']     = "1";
        $data['pesan']    = "Sukses";
        $data['data'] = array();

        if (intval($orders[0]->status) > 1) {
            $data['code']     = "0";    
            $data['pesan']    = "Order yang sudah diproses tidak dapat dibatalkan";
            echo json_encode($data);
            return;
        }

        $this->modelmu->cancel_order($id_order);


        $produks = $this->modelmu->get_detail_order($id_order);
        $data_pemilik  = $this->modelmu->notif_get_pemilik($id_pemilik);
        $data_konsumen = $this->modelmu->notif_get_konsumen($id_konsumen);
        // echo json_encode($data_konsumen); die();
        $data_order    = $this->modelmu->notif_get_order($id_order);
        $kurir = $this->model_api_kurir->get_kurir($data_pemilik[0]->kontak_sms2);
        // echo json_encode($kurir); die();
        $id_pemilik    = "";


        foreach ($produks as $produk) {
            $this->modelmu->revert_stok($produk->id_produk, $produk->kuantitas, $produk->tipe_produk);
        }
        
        $arr_token = array();
        foreach ($data_pemilik as $row) {
            array_push($arr_token, $row->token);
            $id_pemilik = $row->id_pemilik;
        }

        $push_message =  "Order dari " . $data_konsumen->nama . " dibatalkan";

        // $push_message = $data_pemilik->tipe . " == " . n";
        if (intval($data_pemilik[0]->tipe) == self::TIPE_PEDAGANG_KELILING) {
            $push_message = "Antrian dari " . $data_konsumen->nama . " dibatalkan";
        }
        
        $message = array(
            "jenis" => "0",
            "id_pemilik" => $id_pemilik,
            "nama_pemilik" => $data_pemilik->nama,
            "id_order" => $id_order,
            "notif" => $push_message,
            "order" => json_encode($data_order)
            );
        
        $pushStatus = $this->send_message($arr_token, $message, $data_pemilik[0]->tipe);
        
        $arr_token = array();
        foreach ($kurir as $row) {
            array_push($arr_token, $row->token);
        }
        $data['arr_token'] = $arr_token;
        $pushStatus = $this->send_message_to_kurir($arr_token, $message);
        
        $data["gcm_result"] = $pushStatus;
        
        echo json_encode($data);
    }
    

    public function finish_order($id_order) {

        $this->modelmu->finish_order($id_order);

        $data['code']     = "1";
        $data['pesan']    = "Sukses";
        $data['data'] = array();
        
        echo json_encode($data);
    }
    

    public function get_kategori()
    {
        $data['code']     = "1";
        $data['pesan']    = "Sukses";
        $data['kategori'] = $this->modelmu->get_kategori();
        
        echo json_encode($data);
        
    }



    public function test_get_produk_umkm() {
        // $this->check_session($this->input->request_headers());

        // header("Content-type: application/json");
        // date_default_timezone_set("Asia/Jakarta");

        $id_pemilik = $this->input->get("id_pemilik");
        $id_konsumen = $this->input->get("id_konsumen");

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data'] = new stdClass();
        $page = $this->input->get("page");

        $info = $this->model_api_pemilik->get_pemilik($id_pemilik);
        // echo json_encode($info); die();

        if (count($info) > 0) {

            $data['data'] = $this->model_api_pemilik->get_produks_umkm($id_pemilik, $page);
            $data['total'] = $this->model_api_pemilik->get_produks_umkm_count($id_pemilik);

        }

        echo json_encode($data);
    }


    public function test_get_produk_umkm_count() {
        // $this->check_session($this->input->request_headers());

        // header("Content-type: application/json");
        // date_default_timezone_set("Asia/Jakarta");

        $id_pemilik = $this->input->get("id_pemilik");
        $id_konsumen = $this->input->get("id_konsumen");

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data'] = 0;

        $info = $this->model_api_pemilik->get_pemilik($id_pemilik);
        // echo json_encode($info); die();

        if (count($info) > 0) {
            $data['data'] = $this->model_api_pemilik->get_produks_umkm_count($id_pemilik);

        }

        echo json_encode($data);
    }

    public function test_get_produk_pemilik() {
        // $this->check_session($this->input->request_headers());

        // header("Content-type: application/json");
        // date_default_timezone_set("Asia/Jakarta");

        $id_pemilik = $this->input->get("id_pemilik");
        $id_konsumen = $this->input->get("id_konsumen");
        $page = $this->input->get("page");
        $min_antar = $this->input->get("min_antar");

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data'] = array();

        $info = $this->model_api_pemilik->get_pemilik($id_pemilik);
        // echo json_encode($info); die();

        if (count($info) > 0) {
            $data['data'] = $this->modelmu->get_produk_pemilik($id_pemilik, array(), $info[0]->tipe, $min_antar, $page);
            $data['total'] = $this->modelmu->get_produk_pemilik_count($id_pemilik, array(), $info[0]->tipe, $min_antar);
        }

        echo json_encode($data);
    }

    public function get_promo() {
        // $this->check_session($this->input->request_headers());

        // header("Content-type: application/json");
        // date_default_timezone_set("Asia/Jakarta");

        $id_pemilik = $this->input->get("id_pemilik");

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data'] = new stdClass();

        // $info = $this->model_api_pemilik->get_pemilik($id_pemilik);

        // if (count($info) > 0) {

            $promo = $this->model_merchant_promo->get_promo_by_pemilik($id_pemilik)->result();


                $data['code']  = "1";
                $data['pesan'] = "Berhasils";
                $data['data'] = $promo;

        // } 

        echo json_encode($data);

    }

    public function test_get_produk_promo() {
        // $this->check_session($this->input->request_headers());

        // header("Content-type: application/json");
        // date_default_timezone_set("Asia/Jakarta");

        $id_pemilik = $this->input->get("id_pemilik");
        $id_konsumen = $this->input->get("id_konsumen");
        $page = $this->input->get("page");

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data'] = new stdClass();

        $info = $this->model_api_pemilik->get_pemilik($id_pemilik);

        if (count($info) > 0) {

            $promo = $this->model_merchant_promo->get_promo_by_pemilik($id_pemilik)->result();
            
            foreach ($promo as $promo_item) {
                $id_promos = $id_promos . $promo_item->id . ", ";
            }

            if ($id_promos != "") {

                $id_promos = $id_promos . "HEHEHE1234";
                $id_promos = str_replace(", HEHEHE1234", "", $id_promos);
            }

            if (count($promo) <= 0 && intval($info[0]->tipe) == 2) { // Kalau tipe minimarket
                $data['code']  = "1";
                $data['pesan'] = "Tidak ada produk promou";
                $data['data'] = array();
                echo json_encode($data);
                return;
            } 

            if (intval($info[0]->tipe) != 2 && $page >= 2) { // Kalau bukan tipe minimarket
                $data['code']  = "1";
                $data['pesan'] = "Tidak ada produk promos";
                $data['data'] = array();
                echo json_encode($data);
                return;
            }

            $promo_item = $promo[$page-1];

            // if ($promo_item == null) {
            //     $data['code']  = "1";
            //     $data['pesan'] = "Tidak ada produk promo";
            //     $data['data'] = array();
            //     echo json_encode($data);
            //     return;
            // }

            // echo "$promo_item->id " . $promo_item->id;

            $use_discount = false;

            $promos = array();
            $id_merchant_promos = array();

            $data['data'] = [];

            $rc = 0;
            $previous_produk_id = "";


            if (intval($info[0]->tipe) == 2) {
                $promo_item->free_items = $this->model_merchant_promo->get_free_item($promo_item->id, $id_pemilik, $page)->result();
                
                if ($promo_item->tipe == "1") $use_discount = true;
                
                $free_produks = [];
                $free_produk_items = [];
                $produk = new stdClass();
                foreach($promo_item->free_items as $free_item) {

                    $produk2 = new stdClass();
                    $produk2->id = $free_item->id_produk2;
                    $produk2->nama_produk = $free_item->nama_produk;
                    $produk2->foto = $free_item->foto2;
                    $produk2->qty = $free_item->qty2;
                    $produk2->stok = $free_item->stok2;
                    $produk2->harga = $free_item->harga2;
                    $produk2->harga_promo = $free_item->harga_promo2;
                    $produk2->id_kategori_produk = $free_item->id_kategori_produk2;
                    $produk2->status = $free_item->status2;
                    $produk->free_items[] = $produk2;

                    if ($free_item->id_produk != $previous_produk_id) {
                        $produk = new stdClass();
                        $produk->id_produk = $free_item->id_produk;
                        $produk->nama_produk = $free_item->nama_produk;
                        $produk->foto = $free_item->foto;
                        $produk->qty = $free_item->qty;
                        $produk->stok = $free_item->stok;
                        $produk->harga = $free_item->harga;
                        $produk->harga_promo = $free_item->harga_promo;
                        $produk->id_kategori_produk = $free_item->id_kategori_produk;
                        $produk->status = $free_item->status;
                        $produk->kelipatan = $free_item->kelipatan;
                        $free_produks[] = $produk;
                    } 

                    $previous_produk_id = $free_item->id_produk;
                }


                $promos[] = $promo_item;
                $data['data'] = array_merge($data['data'], $free_produks);

                $id_merchant_promos[] = $promo_item->id;
            }

            // echo "ok " . $promo_item->id; die();

            $produk_pemilik_diskon = $this->modelmu->get_produk_pemilik_diskon($id_pemilik, $info[0]->id_partner, $info[0]->tipe, $promo_item->id);
            $data['total'] = intval($this->modelmu->get_produk_pemilik_diskon_count($id_pemilik, $info[0]->id_partner, $info[0]->tipe, $id_promos)) + count($data['data']) . "";

            $data['data'] = array_merge($data['data'], $produk_pemilik_diskon);

            $use_discount = false;

            $promo = $this->model_promo_old->get_promo($id_pemilik)->row();
            $produk_promo = [];

            $promo_valid = false;
            $konsumen_valid = false;
            if (!empty($promo)) {
                $produk_promo = $this->model_promo_old->get_produk_promo($promo->promo_id, $id_pemilik, $page);
                $promo_valid = true;

                if(count($produk_promo) > 0) {
                    $promo_konsumen_order = $this->model_promo_old->get_promo_order_konsumen($id_konsumen, $produk_promo[0]->id_produk, $id_pemilik);

                    if (count($promo_konsumen_order) < 2) {
                        $konsumen_valid = true;
                    }
                }
                
            } else {
                // echo "promo empty"; die();
            }

            if ($konsumen_valid) {
                $data['data'] = array_merge($data['data'], $produk_promo);
                // $data['data']->produk_promo = $produk_promo;
            }

        }
        echo json_encode($data);
    }
    
    
    public function test_get_detail_pemilik()
    {
        $this->check_session($this->input->request_headers());

        // header("Content-type: application/json");
        // date_default_timezone_set("Asia/Jakarta");

        $id_pemilik = $this->input->get("id_pemilik");
        $id_konsumen = $this->input->get("id_konsumen");

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data'] = new stdClass();

        $info = $this->model_api_pemilik->get_pemilik($id_pemilik);
        // echo json_encode($info); die();

        if (count($info) > 0) {

                if (time() < strtotime($info[0]->waktu_buka)) {
                    // echo "1";
                } else if (time() >= strtotime($info[0]->waktu_buka) && time() < strtotime($info[0]->waktu_tutup) && intval($info[0]->tipe) != self::TIPE_PEDAGANG_KELILING) {
                    // echo "2";
                    if ($info[0]->triggered == "0") {
                        $this->model_api_pemilik->buka_toko($id_pemilik, 1, 1);
                        $info[0]->triggered = "1";
                        $info[0]->status_toko = "1";
                    } 
                    // else {
                    //     $this->model_api_pemilik->buka_toko($id_pemilik, 1, 1);
                    //     $info[0]->triggered = "1";
                    // // }
                } else if (time() >= strtotime($info[0]->waktu_tutup)) {
                    // echo "3";
                    $this->model_api_pemilik->buka_toko($id_pemilik, 2, 0);
                    $info[0]->status_toko = "2";
                    $info[0]->triggered = "0";
                } 
                // echo json_encode($info[0]);
                // die();

            $data['data']->kategori_pemilik = $this->model_api_pemilik->get_kategori_usaha($id_pemilik);
            $data['data']->banner = $this->model_partner->get_banners($info[0]->id_partner)->result();
            if ($info[0]->tipe == '1') {
                $data['data']->kategori_produk = $this->model_api_pemilik->get_kategori_produk_pemilik($id_pemilik);
            } else {
                $data['data']->kategori_produk = $this->model_api_pemilik->get_kategori_produk_pemilik($info[0]->id_partner);
            }


            $setting = $this->model_setting->get_charge()->row();
            $data['data']->charge = $setting->charge;
            $data['data']->fee = 4000;
            $data['data']->info = $info[0];
            $data['data']->info->view_only = ($info[0]->id_partner != "" && intval($info[0]->delivery) == 0) ? "1" : 
            "0";

            if (count($info) > 0) {
                $data['data']->fee = $info[0]->fee;
            }

            $data['memory'] = memory_get_usage(true);
            // var_dump($setting->charge); die();
        } else {
            $data['code']  = "0";
            $data['pesan'] = "Toko tutup";
        }

        echo json_encode($data);
    }


    
    public function get_detail_pemilik()
    {
        $this->check_session($this->input->request_headers());

        // header("Content-type: application/json");
        // date_default_timezone_set("Asia/Jakarta");

        $id_pemilik = $this->input->get("id_pemilik");
        $id_konsumen = $this->input->get("id_konsumen");

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data'] = new stdClass();

        $info = $this->model_api_pemilik->test_get_pemilik($id_pemilik);
        // echo json_encode($info); die();

        if (count($info) > 0) {

                if (time() < strtotime($info[0]->waktu_buka)) {
                    // echo "1";
                } else if (time() >= strtotime($info[0]->waktu_buka) && time() < strtotime($info[0]->waktu_tutup) && intval($info[0]->tipe) != self::TIPE_PEDAGANG_KELILING) {
                    // echo "2";
                    if ($info[0]->triggered == "0") {
                        $this->model_api_pemilik->buka_toko($id_pemilik, 1, 1);
                        $info[0]->triggered = "1";
                        $info[0]->status_toko = "1";
                    } 
                    // else {
                    //     $this->model_api_pemilik->buka_toko($id_pemilik, 1, 1);
                    //     $info[0]->triggered = "1";
                    // // }
                } else if (time() >= strtotime($info[0]->waktu_tutup)) {
                    // echo "3";
                    $this->model_api_pemilik->buka_toko($id_pemilik, 2, 0);
                    $info[0]->status_toko = "2";
                    $info[0]->triggered = "0";
                } 
                // echo json_encode($info[0]);
                // die();

            $kuber = $this->model_api_kurir->get_kurir($info[0]->kontak_sms2);
            $kuber_available = true;
            if (count($kuber) > 0) {
                $kuber_available = true;
                if (intval($kuber[0]->status_toko)) {
                    $kuber_available = true;
                } else {
                    $kuber_available = false;
                }
            } else {
                $kuber_available = false;
            }


            $data['data']->kuber_available = $kuber_available;
            $data['data']->kategori_pemilik = $this->model_api_pemilik->get_kategori_usaha($id_pemilik);
            $data['data']->banner = $this->model_partner->get_banners($info[0]->id_partner)->result();
            // if ($info[0]->tipe == '1') {
                $data['data']->kategori_produk = $this->model_api_pemilik->get_kategori_produk_pemilik($id_pemilik);
            // } else {
            //     $data['data']->kategori_produk = $this->model_api_pemilik->get_kategori_produk_pemilik($info[0]->id_partner);
            // }


            $setting = $this->model_setting->get_charge()->row();
            $data['data']->charge = $setting->charge;
            $data['data']->fee = 0;
            $data['data']->info = $info[0];
            $data['data']->info->view_only = ($info[0]->id_partner != "" && intval($info[0]->delivery) == 0) ? "1" : 
            "0";

            if (count($info) > 0) {
                $data['data']->fee = $info[0]->fee;
            }

            $data['memory'] = memory_get_usage(true);
            // var_dump($setting->charge); die();
        } else {
            $data['code']  = "0";
            $data['pesan'] = "Toko tutup";
        }

        echo json_encode($data);
    }

    public function test_get_produk_by_kategori()
    {
        // $this->check_session($this->input->request_headers());

        // header("Content-type: application/json");
        // date_default_timezone_set("Asia/Jakarta");

        $id_pemilik = $this->input->get("id_pemilik");
        $id_kategori = $this->input->get("id_kategori");
        $page = $this->input->get("page");

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data'] = new stdClass();

        $info = $this->model_api_pemilik->get_pemilik($id_pemilik);
        // echo json_encode($info); die();

        if (count($info) > 0) {
            // $data['data']->kategori_produk = $this->model_api_pemilik->get_kategori_produk_pemilik($id_pemilik);
            $data['data'] = $this->modelmu->get_produk_by_kategori($id_pemilik, $id_kategori, $page);
            
            $data['memory'] = memory_get_usage(true);
        } else {
            $data['code']  = "0";
            $data['pesan'] = "Toko tutup";
        }

        echo json_encode($data);
    }


    public function get_ongkir($id_pemilik) {
        $distance = $this->input->post("distance");

        $info = $this->model_api_pemilik->get_pemilik($id_pemilik);

        $data['data'] = new stdClass();
        $data['data']->fee = 0;

        if (count($info) > 0) {
            if (intval($info[0]->tipe_kurir) != 0) {
                $ongkirs = $this->model_api_pemilik->get_ongkirs($info[0]->id_pemilik);
            } else {
                $kurirs = $this->model_api_kurir->get_kurir($info[0]->kontak_sms2);
                if (count($kurirs) > 0) {
                    $ongkirs = $this->model_api_kurir->get_ongkirs($kurirs[0]->id_kurir);
                } else {
                    $ongkirs = $this->model_api_pemilik->get_ongkirs($info[0]->id_pemilik);
                }
            }

            if (count($ongkirs) > 0) {

        
        // echo json_encode($ongkirs); echo $distance . ""; die();
                foreach ($ongkirs as $ongkir) {
                    if (floatval($distance) >= floatval($ongkir->jarak_min) && floatval($distance) <= floatval($ongkir->jarak_max)) {
                        $data['data']->fee = $ongkir->fee;
                        break;
                    }
                }

            } else {
                $data['data']->fee = $info[0]->fee;
            }
        }

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        
        echo json_encode($data);
    }
    
    public function isi_profil_awal() //menampilkan data barang apa saja yang dimiliki seorang pemilik usaha
    {
        // $this->check_session($this->input->request_headers());

        $id_konsumen = $this->input->post("id_konsumen");
        $nama        = $this->input->post("nama");
        $alamat      = $this->input->post("alamat");
        $id_member      = $this->input->post("id_member");
        
        $this->modelmu->isi_profil_awal($id_konsumen, $nama, $alamat, $id_member);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        
        echo json_encode($data);
    }

    
    // public function get_produk_promo($id_pemilik)
    // {
    //     $promo = $this->model_promo_old->get_latest_promo()->row();
    //     $data['code']  = "1";
    //     $data['pesan'] = "Berhasil";
    //     $data['data']  = $this->model_promo_old->get_produk_promo($promo->id, $id_pemilik);
    //     echo json_encode($data);
    // }
    
    
    public function isi_lokasi_awal()
    {
        // $this->check_session($this->input->request_headers());

        $id_konsumen = $this->input->post("id_konsumen");
        $lat         = $this->input->post("lat");
        $lng         = $this->input->post("lng");
        
        $this->modelmu->isi_lokasi_awal($id_konsumen, $lat, $lng);
        
        $data['code']     = "1";
        $data['pesan']    = "Berhasil";
        $data['kategori'] = $this->modelmu->get_kategori();
        
        echo json_encode($data);
    }
    
    public function hapus_akun()
    {
        // $this->check_session($this->input->request_headers());

        $id_konsumen = $this->input->post("id_konsumen");
        $this->modelmu->copy_to_histori($id_konsumen);
        
        $this->modelmu->hapus_akun($id_konsumen);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        
        echo json_encode($data);
    }

    public function get_produk_favorit($id_konsumen, $id_pemilik, $page) {
        // $this->check_session($this->input->request_headers());

        $info = $this->model_api_pemilik->get_pemilik($id_pemilik);

        $id_promos = "";
        if (count($info) > 0) {
            $promos = $this->model_merchant_promo->get_promo_by_pemilik($id_pemilik)->result();
            // echo json_encode($promos); die();

            foreach ($promos as $promo) {
                $id_promos = $id_promos . $promo->id . ", ";
            }

            if ($id_promos != "") {

                $id_promos = $id_promos . "HEHEHE1234";
                $id_promos = str_replace(", HEHEHE1234", "", $id_promos);
            }
        } else {
            $data['code']  = "0";
            $data['pesan'] = "Unknown merchant";
            $data['data']  = null;
            
            echo json_encode($data);
            return;
        }

        $produks = $this->modelmu->get_produk_favorit($id_konsumen, $id_pemilik, $info[0]->tipe, $id_promos, $page);
        $produk_count = $this->modelmu->get_produk_favorit_count($id_konsumen, $id_pemilik, $info[0]->tipe, $id_promos);

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $produks;
        $data['total']  = $produk_count;
        
        echo json_encode($data);
    }

    public function get_produk_favorit_count($id_konsumen, $id_pemilik) {
        // $this->check_session($this->input->request_headers());

        $info = $this->model_api_pemilik->get_pemilik($id_pemilik);

        $id_promos = "";
        if (count($info) > 0) {
            $promos = $this->model_merchant_promo->get_promo_by_pemilik($id_pemilik)->result();
            // echo json_encode($promos); die();

            foreach ($promos as $promo) {
                $id_promos = $id_promos . $promo->id . ", ";
            }

            if ($id_promos != "") {
                $id_promos = $id_promos . "HEHEHE1234";
                $id_promos = str_replace(", HEHEHE1234", "", $id_promos);
            }
            $produk_count = $this->modelmu->get_produk_favorit_count($id_konsumen, $id_pemilik, $info[0]->tipe, $id_promos);
        }


        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $produk_count;
        
        echo json_encode($data);
    }
    
    public function get_detail_order()
    {
        // $this->check_session($this->input->request_headers());

        $id_order = $this->input->get("id_order");
        
        $detail_order = new stdClass();
        $detail_order->detail_order = $this->modelmu->get_detail_order($id_order);
        $detail_order->data_order    = $this->modelmu->notif_get_order($id_order);
        $order = $detail_order->data_order[0];
        $detail_order->data_pemilik = $this->modelmu->notif_get_pemilik($order->id_pemilik);

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $detail_order;
        
        echo json_encode($data);

        return $detail_order;
    }

    public function get_order_detail($id_order)
    {
        // $this->check_session($this->input->request_headers());
        
        $detail_order = new stdClass();
        $detail_order->detail_order = $this->modelmu->get_detail_order($id_order);
        $detail_order->data_order    = $this->modelmu->notif_get_order($id_order);
        // $order = $detail_order->data_order[0];
        // $detail_order->data_pemilik = $this->modelmu->notif_get_pemilik($order->id_pemilik);

        
        return $detail_order;
    }


    public function get_detail_kiriman()
    {
        /// $this->check_session($this->input->request_headers());

        $id_kiriman = $this->input->get("id_kiriman");
        // echo "ok"; die();
        
        $detail_kiriman = new stdClass();
        $kiriman_items = array();

        $detail_kiriman->data_kiriman    = $this->model_api_kurir->notif_get_kiriman($id_kiriman);
        $kiriman = $detail_kiriman->data_kiriman;

        $kirimans = $this->modelmu->get_detail_kiriman($id_kiriman, $kiriman->tipe);
        $detail_kiriman->data_kurir = $this->model_api_kurir->notif_get_pemilik($kiriman->id_kurir);

        // echo json_encode($kirimans); die();


        if (intval($kiriman->tipe) == 3) {
            $items = array();

            $prev_id_ekspedisi = 0;
            if (count($kirimans) > 0) {
                $prev_id_ekspedisi = $kirimans[0]->id_ekspedisi;

                $kiriman_obj = new stdClass();
                $kiriman_obj->items = $items;
                $kiriman_obj->nama_ekspedisi = $kirimans[0]->nama_ekspedisi;
                $kiriman_obj->foto = $kirimans[0]->foto;
                $kiriman_obj->courier_slug = $kirimans[0]->courier_slug;
                // // echo $kirimans[0]->foto_resi . " == " . URL_IMAGE_KIRIMAN; die();
                // if ($kirimans[0]->foto_resi == URL_IMAGE_KIRIMAN) {
                //     $kiriman_obj->foto_resi = "";
                // } else {
                //     $kiriman_obj->foto_resi = $kirimans[0]->foto_resi;
                // }
                $total++;
                $kiriman_obj->total = $total;
                $kiriman_items[] = $kiriman_obj;
            }

            $total = 0;

            foreach ($kirimans as $kiriman) {

                if ($kiriman->id_ekspedisi != $prev_id_ekspedisi) {
                    $total=0;
                    $kiriman_obj = new stdClass();
                    $kiriman_obj->items = $items;
                    $kiriman_obj->nama_ekspedisi = $kiriman->nama_ekspedisi;
                    $kiriman_obj->foto = $kiriman->foto;
                    $kiriman_obj->courier_slug = $kiriman->courier_slug;

                    // if ($kiriman->foto_resi == URL_IMAGE_KIRIMAN) {
                    //     $kiriman_obj->foto_resi = "";
                    // } else {
                    //     $kiriman_obj->foto_resi = $kiriman->foto_resi;
                    // }
                    $kiriman_items[] = $kiriman_obj;
                    $items = array();
                }
                $kiriman_obj->items[] = $kiriman;
                $total++;
                $kiriman_obj->total = $total;
                $prev_id_ekspedisi = $kiriman->id_ekspedisi;

            }

        } else {
            $kiriman_items = $kirimans;
        }

        $detail_kiriman->detail_kiriman = $kiriman_items;

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $detail_kiriman;
        
        echo json_encode($data);

        return $detail_order;
    }

    public function tambah_produk_favorit() {
        // $this->check_session($this->input->request_headers());
        $id_konsumen = $this->input->post("id_konsumen");
        $id_produk = $this->input->post("id_produk");

        $this->modelmu->tambah_produk_favorit($id_konsumen, $id_produk);        

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = [];
        
        echo json_encode($data);
    }

    public function hapus_produk_favorit() {
        // $this->check_session($this->input->request_headers());
        $id_konsumen = $this->input->post("id_konsumen");
        $id_produk = $this->input->post("id_produk");

        $this->modelmu->hapus_produk_favorit($id_konsumen, $id_produk);        

        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = [];
        
        echo json_encode($data);
    }
    
    public function ganti_foto()
    {
        // $this->check_session($this->input->request_headers());
       $id_konsumen = $this->input->post("id_konsumen");
        $foto        = $this->input->post("foto");
        $foto = str_replace("\"", "", $foto);

        // echo "foto: " . $foto; die();

        $errors = array();

        $foto3 = "";
        
        if ($foto != "") {
            $foto2 = explode(".", $foto);
            $foto3 = $id_konsumen . "." . $foto2[count($foto2) - 1];
        }
        
        if ($foto3 != "") {
            $bitmap = $_FILES;
            $errors = $this->upload(PATH_IMAGE_KONSUMEN, $foto3, $bitmap, $id_konsumen); //ini nama input type file(gambar nya)
        }
        
        $this->modelmu->set_foto($id_konsumen, $foto3);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->modelmu->get_foto($id_konsumen);
        $data['errors'] = $errors;
        $data['foto'] = $id_konsumen;
        
        echo json_encode($data);
    }

    public function ganti_foto_avatar()
    {
        // $this->check_session($this->input->request_headers());

        $id_konsumen = $this->input->post("id_konsumen");
        $foto        = $this->input->post("foto");
        
        if ($foto != "") {
            $foto2 = explode(".", $foto);
            $foto3 = $id_konsumen . "." . $foto2[count($foto2) - 1];
        }
        
        if ($foto3 != "") {
            $bitmap = $_FILES;
            $this->upload(PATH_IMAGE_AVATAR, $foto3, $bitmap, $id_konsumen);//ini nama input type file(gambar nya)
        }
        
        $this->modelmu->set_foto_avatar($id_konsumen, $foto3);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data']  = $this->modelmu->get_foto_avatar($id_konsumen);
        
        echo json_encode($data);
    }
    
    // public function upload($path, $nama_file, $id_file)
    // {
    //     try {
    //         $config['upload_path']   = $path;
    //         $config['file_name']     = $nama_file;
    //         $config['allowed_types'] = '*';
    //         $config['overwrite']     = TRUE;
    //         $config['overwrite']     = TRUE;
            
    //         file_put_contents($path, base64_decode($nama_file));
            
    //         // $this->load->library('upload', $config);
    //         // if ( ! $this->upload->do_upload($id_file))
    //         // {
    //         //     $error = array('error' => $this->upload->display_errors());
            
    //         // }   
    //         // else
    //         // {
    //         //     $upload_data = $this->upload->data();
    //         // }
    //     }
    //     catch (Exception $exc) {
    //         echo $exc->getTraceAsString();
    //     }
    // }

    public function upload($path, $nama_file, $files, $id_file)
    {   
        $exp                     = explode(".", $nama_file);
        $ext                     = end($exp);  

        // echo $path.$id_file.".".$ext; die();

        if (is_file($path.$id_file.".".$ext)) {
            unlink($path.$id_file.".".$ext);
        }
        try {
            
            $config['upload_path']   = $path;
            $config['file_name']     = $id_file;
            $config['allowed_types'] = '*';
            $config['overwrite']     = TRUE;

            if (!is_dir($path)) {
                mkdir($path, 0777, true);
            }
            
            if (isset($files['form_file']) && !empty($files['form_file'])) {
                if ($files['form_file']['error'] != 4) {

                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    
                    if (!$this->upload->do_upload('form_file')) {
                        $errors = $this->upload->display_errors();
                        return $errors;
                        // echo json_encode($errors); die();
                    } else {
                        $upload_data = $this->upload->data();
                        return array();
                        // echo json_encode($upload_data); 
                    }
                }
            } 
        }
        catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    
    public function tes_push()
    {
        $data_pemilik = $this->modelmu->notif_get_pemilik('1606011200027569');
        //$data_konsumen = $this->modelmu->notif_get_konsumen('1605301926317833');
        //$data_order = $this->modelmu->notif_get_order('1605301926317833160531151959');
        //$id_pemilik = "";
        
        $arr_token = array();
        foreach ($data_pemilik as $row) {
            array_push($arr_token, $row->token);
            $id_pemilik = $row->id_pemilik;
        }
        
        /*$message = array(
            "jenis"=>"0",
            "id_pemilik"=>$id_pemilik,
            "notif"=>"Order baru dari konsumen ".$data_konsumen->nama,
            "order"=>json_encode($data_order)
            );*/

            $message = array(
                "jenis" => "0",
                "id_pemilik" => "dwkwqbdh",
                "notif" => "Order baru dari konsumen ",
                "order" => "dadas"
                );

            // print_r($arr_token);
            echo json_encode($message);

            $pushStatus = $this->send_message($arr_token, $message, $data_pemilik[0]->tipe);
        }

        public function update_token()
        {

            $id_konsumen = $this->input->post("id_user");
            $token       = $this->input->post("token");

        // echo $token;

            $this->modelmu->update_token($id_konsumen, $token);

            $data['code']  = "1";
            $data['pesan'] = "sukses";
        // $data['data']               = $this->modelmu->get_konsumen($id_konsumen);

            echo json_encode($data);
        }

        public function get_order_dari_notif()
        {
        // $this->check_session($this->input->request_headers());
            $id_order = $this->input->get("id_order");

            $data['code']     = "1";
            $data['pesan']    = "Sukses";
            $data['kategori'] = $this->modelmu->get_order_dari_notif($id_order);

            echo json_encode($data);
        }

        public function cek_status_toko()
        {
            $id_pemilik = $this->input->get("id_pemilik");
        //$id_pemilik    = "1606011803212180";

            $status = $this->modelmu->cek_status_toko($id_pemilik)->status_toko;
            $code   = "";
            $pesan  = "";

            if ($status == "2") {
                $code  = "0";
                $pesan = "Maaf toko sedang tutup";
            } else {
                $code  = "1";
                $pesan = "Berhasil";
            }


            $data['code']  = $code;
            $data['pesan'] = $pesan;

            echo json_encode($data);
        }













        function send_message($registatoin_ids, $message, $tipe_pemilik)
        {
        //Google cloud messaging GCM-API url
            // $url    = 'https://android.googleapis.com/gcm/send';
            $url    = 'https://fcm.googleapis.com/fcm/send';

            if (count($registatoin_ids) <= 0) return;
            
            $fields = new stdClass();
            $fields->to = $registatoin_ids[0];
            $fields->data = $message;
            // $fields = array(
            //     'to' => $registatoin_ids,
            //     'data' => $message
            //     );

            $gcm_key = KEY_GCM_PEMILIK;

            // echo intval($tipe_pemilik) . " == " . self::TIPE_PEDAGANG_KELILING . "\n";
            if (intval($tipe_pemilik) == self::TIPE_PEDAGANG_KELILING) {
                $gcm_key = KEY_GCM_PEDAGANG;
            }

            // echo "gcm id: " . $gcm_key;

            $headers = array(
                'Authorization: key=' . $gcm_key,
                'Content-Type: application/json'
                );
            $ch      = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            if ($result === FALSE) {
                $result = "salah";
            //die('Curl failed: ' . curl_error($ch));
            }
            curl_close($ch);
            return $result;
        }

    function send_message_to_kurir($registatoin_ids, $message)
    {
            // $url    = 'https://android.googleapis.com/gcm/send';
        $url    = 'https://fcm.googleapis.com/fcm/send';

        if (count($registatoin_ids) <= 0) return;

        $fields = new stdClass();
        $fields->to = $registatoin_ids[0];
        $fields->data = $message;
        //$fields->notification = $message;
            // $fields = array(
            //     'to' => $registatoin_ids,
            //     'data' => $message
            //     );

        
        
        $headers = array(
            'Authorization: key=' . KEY_GCM_KURIR,
            'Content-Type: application/json'
        );
        $ch      = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === FALSE) {
            $result = "salah";
        }
        curl_close($ch);
        return $result;
    }

        function get_latest_promo($id_konsumen)
        {
            // header("Content-type: application/json");

            $data = array();

            $promo_code_konsumen = $this->model_promo_old->check_promo_membership($id_konsumen)->result();
            if (count($promo_code_konsumen) == 0) {
                $promo = $this->model_promo_old->get_latest_promo()->row();

                if (!empty($promo)) {
                    $data['code']  = "1";
                    $data['pesan'] = "Promo ada";
                    $data['data']  = $promo;
                } else {
                    $data['code']  = "0";
                    $data['pesan'] = "Tidak ada promo";
                    $data['data']  = null;
                }
            } else {
                $data['code']  = "0";
                $data['pesan'] = "Tidak ada promo";
                $data['data']  = null;
            }
            echo json_encode($data);

        }


        function confirm_promo_code()
        {
            // header("Content-type: application/json");

            $id_konsumen = $this->input->post("id_konsumen");
            $promo_code  = $this->input->post("promo_code");

            $data = array();

            $query = $this->model_promo_old->check_promo_code($promo_code);

            if ($query->num_rows() >= 1) {

                $promo_code_obj = $query->row();

                $used = $this->model_promo_old->confirm_promo_code_konsumen($id_konsumen, $promo_code_obj->id);

                $data['code']  = "1";
                $data['pesan'] = "Selamat, promo Anda aktif";
                $data['data']  = $promo_code_obj;

            } else {
                $data['code']  = "0";
                $data['pesan'] = "Maaf kode promo sudah tidak tersedia.";
                $data['data']  = "";
            }
            echo json_encode($data);

        }

        function datediffInWeeks($date1, $date2)
        {
            if($date1 > $date2) return datediffInWeeks($date2, $date1);
            $first = DateTime::createFromFormat('Y-m-d', $date1);
            $second = DateTime::createFromFormat('Y-m-d', $date2);
            return $first->diff($second)->days % 14;
        }



        function send_telegram($order_id, $no_telp, $msg, $lat, $lng, $id_pemilik) {
            $kurir = $this->model_kurir->get_by_kontak($no_telp)->row();

            log_message("error", "send_telegram : " . $kurir->tele_id." => ");
            $this->telegram->send->chat($kurir->tele_id)->text($msg, "HTML")->send();

            $this->telegram->send->chat($kurir->tele_id)
                ->location($lat,$lng)
                ->send();


                $this->telegram->send->chat($kurir->tele_id)
                    ->inline_keyboard()
                        ->row()
                            ->button("Antar Sekarang", "deliver-".$order_id ."-".$id_pemilik . "-2-", "callback_data")
                            ->button("Tunggu 10 Menit", "deliver-".$order_id ."-".$id_pemilik . "-3-", "callback_data")
                        ->end_row()
                        ->row()
                            ->button("Tunggu 20 Menit", "deliver-".$order_id ."-".$id_pemilik . "-4-", "callback_data")
                        ->end_row()
                    ->show(true, true)
                    ->text("Proses order <b>".$order_id."</b>?", "HTML")
                    ->send();

            return $kurir->tele_id;
        }

        public function send_sms($hp,$isi){
       
            $final_isi = "";
            
            $isi_ar = explode(" ", $isi);
            
            for($i=0;$i<count($isi_ar);$i++){
                
                $final_isi = $final_isi .urlencode($isi_ar[$i]." ");
            }
            
            $curlHandle = curl_init();
            $url="http://162.211.84.203/sms/smsmasking.php?username=".urlencode("enermous")."&password=".urlencode("ACc7yB")."&key=4275a14814af13d2c108b9137781aa2a&number=".$hp."&message=".$final_isi;
            curl_setopt($curlHandle, CURLOPT_URL,$url);
            curl_setopt($curlHandle, CURLOPT_HEADER, 0);
            curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curlHandle, CURLOPT_TIMEOUT,120);
            $hasil = curl_exec($curlHandle);
            curl_close($curlHandle);    

            // return $hasil;
           
        }

    }
