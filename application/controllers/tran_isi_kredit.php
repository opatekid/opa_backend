<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tran_isi_kredit extends CI_Controller {
	
    public function __construct(){
        parent::__construct();
        $this->load->model("model_transaksi");
    }

    public function index() {
        $data["level"]=  $this->model_transaksi->get_level()->result();
        $data["kategori"]=  $this->model_transaksi->get_kategori()->result();
        $data["tran_isi"]=  $this->model_transaksi->get_upgrade()->result();
    
        $this->load->view('tran_isi_kredit',$data);
    }
    public function isi_kredit() {
        header("Content-type: application/json");
        $res = "";
        try {
            $this->load->helper('form');
            $nama = $this->input->post("nama");
            $tipe = $this->input->post("tipe");
            $nominal = $this->input->post("nominal");
            $id   = $this->input->post("id");
            $idpemilik_up = date("ymdHis")."1";
            $idp = $this->session->userdata("id");
			
            $this->model_transaksi->isi_kredit($idp,$nama,$tipe,$nominal,$id,$idpemilik_up);
            $res = "Update data successfully";
            $data = array("status"=>$res,"list"=>$this->model_transaksi->get_upgrade()->result());
            echo json_encode($data);
            
        } catch (Exception $ex) {
            $res = "failed->".$ex;
            $data = array("status"=>$res);
            echo json_encode($data);
        }
    }
    
    /////////serverside///////////
    
    public function get_data(){
        
        $requestData        = $this->input->post();
        $pesan = "";
        
        $count = $this->model_transaksi->get_data();
        $totalFiltered = $count;
        $totalData =  $count;

        if( !empty($requestData['search']['value']) ) {
            $query = $this->model_transaksi->get_data_filterisi($requestData);
            $totalFiltered = $query->num_rows();
            $pesan = "pencarian";
        }

        $query = $this->model_transaksi->get_data_orderisi($requestData);

        $data = array();
        
        $no = $requestData['start']+1;
        
        $index = 0;
        $tipe = "";
        foreach ($query->result() as $row){
            if ($row->tipe == '1') {
                $tipe = 'Free';
            }elseif ($row->tipe == '2') {
                $tipe = 'Premium';
            }
            $nestedData=array(); 
            $nestedData[] = $no++;
            $nestedData[] = $row->nama;
            $nestedData[] = $tipe;
            $nestedData[] = number_format($row->kredit);
            $nestedData[] = '<a onclick="edit_modal('.$index.')"><i rel="tooltip" title="Ubah" style="cursor: pointer" class="icon icon-pencil"></i></a>';
            $data[] = $nestedData;
            
            $index++;
        }

        $json_data = array(
            "pesan"           => $pesan,
            "draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
            "recordsTotal"    => intval( $totalData ),  // total number of records
            "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data"            => $data,   // total data array
            "json"            => $query->result()   // total data array
        );
        
        echo json_encode($json_data);  // send data as json format
    }
}