<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Merchant_login extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->library("session");
        $this->load->model("model_partner");
    }
    
    public function index() {
        $segment = $this->uri->segment(1);
        // echo "segment " . $segment; 
        // die();
        if ($segment == 'sodaqo') {
            return;
        }
        $this->load->view('merchant_login');
    }
    
    public function proses_login(){
        header("Content-type: application/json");
        $res = "";
        $code ="0";
        try {
            $this->load->helper('form');
            $username = $this->input->post('username');
            $password = hash("sha512", $this->input->post('password'));
            
            $get_user = $this->model_partner->get_by_username($username);

            if($get_user->num_rows() > 0){
                foreach ($get_user->result() as $row){
                    //lek password bener
                    if($password == $row->password){
                        $res = "Login berhasil, mohon tunggu...";
                        
                        $this->session->set_userdata('id',$row->id_partner);
                        $this->session->set_userdata('level',$row->level);
                        $this->session->set_userdata('nama',$row->nama);
                        $code = "1";
                        
                    }
                    //lek password salah
                    else {
                        $res = "Password Salah";
                    }
                }
            }
            //lek username e salah
            else{
                $res = "Username Belum Terdaftar !! ";
            }

            $data = array("status"=>$res,"code"=>$code);
            echo json_encode($data);

        } catch (Exception $ex) {
            $res = "Login error";
            $data = array("status"=>$res,"code"=>$code);
            echo json_encode($data);
        }
    }
    
    public function logout(){
        $this->session->sess_destroy();
        redirect("merchant_login");
    }
    
    public function lupa_pas() {
        header("Content-type: application/json");
        $res = "";
        $stat = "";
        $errors = array();
        try {
            $email = $this->input->post("email");
            $kode = rand(1000, 10000);
            $get_user = $this->model_partner->get_email($email);
            if($get_user->num_rows() > 0){
                $this->session->set_userdata("kode",$kode."");
                $this->session->set_userdata("mail",$email."");
                $subjek         = "[No Reply] OPA Password Reset";
                $pesan          = '<p>Kode verifikasi Anda untuk reset password: '.$kode.'<br /><br />Silakan klik link berikut untuk me-reset password Anda. <br />' . URL_OPA . 'merchant_login/lupa_pas_kode?kodepas=' . $kode;

                require_once 'ctr_email.php';
                $myemail = new Ctr_email();
                $errors = $myemail->send_email($email, $subjek, $pesan);
                $res = "Kode verifikasi telah dikirim, cek email anda";
                $stat = "1";
            }  else {
                $stat= "2";
                $res="Email tidak terdaftar";
            }
            
            $data = array("status"=>$res,"stat"=>$stat, "errors"=>$errors);
            echo json_encode($data);
            
        } catch (Exception $ex) {
            $res = "failed->".$ex;
            $data = array("status"=>$res);
            echo json_encode($data);
        }
    }
    
    public function lupa_pas_kode() {
        $res = "";
        $stat ="";
        try {
//            $email = $this->input->post("email");
            $kode = rand(1000, 100000);
            $kodev = $this->input->get("kodepas");
            if ($kodev == '' || $kodev == null) {
                $kodev = $this->input->post("kodepas");
            }
            $seskode = $this->session->userdata("kode");
            $email = $this->session->userdata("mail");
//            $vkode = $this->input->post("vkode");
            if($kodev == $seskode){
                $n_pass = hash("sha512", $kode);
                $this->model_partner->update_lupa_pas($n_pass,$email);
                $subjek         = "OPA-New password";
                $pesan          = '<p>Silakan login dengan password : '.$kode.'</p>';
                require_once 'ctr_email.php';
                $myemail = new Ctr_email();
                $myemail->send_email($email, $subjek, $pesan);
                $this->session->sess_destroy();
                $res = "Password baru telah dikirim, cek email anda";
                $stat ="1";
            } else {
                $stat ="2";
                $res = "Kode verifikasi salah";
            }
            
            $data = array("status"=>$res,"stat"=>$stat);

            // echo json_encode($data);
            
        } catch (Exception $ex) {
            $res = "failed->".$ex;
            $data = array("status"=>$res);
            // echo json_encode($data);
        }

        $this->load->view('merchant_login', $data);
    }
}