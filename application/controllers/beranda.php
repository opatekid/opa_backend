<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends CI_Controller {

    public function __construct(){
        parent::__construct();
        if ($this->session->userdata("id")!=NULL){
            $this->load->model("model_setting");
        }
        else {
            redirect(URL_LOGIN);
        }
    }

    public function index() {
        $data["konsumen"]= $this->model_setting->home_konsumen();
        $data["pusaha"]= $this->model_setting->home_pemilik();
        $data["order"]= $this->model_setting->home_order();
        $data["fee"]= $this->model_setting->home_fee();
        $data["total"]= $this->model_setting->get_total()->result();
        
//        echo json_encode($data);
        $this->load->view('beranda',$data);   
    }
    
}