<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sett_pengguna extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model("model_setting");
    }

    public function index() {
        $data["pengguna"]=  $this->model_setting->get_pengguna()->result();
        $this->load->view('sett_pengguna',$data);
    }
    
    public function insert_pengguna() {
        header("Content-type: application/json");
        $res = "";
        try {
            $nama       = $this->input->post("nama");
            $username   = $this->input->post("username");
            $email      = $this->input->post("email");
            $pass       = hash("sha512", PASS_OPA);
            $id         = $this->session->userdata("id");
            
            $status_email = $this->model_setting->cek_email($email)->num_rows();
            $status_username = $this->model_setting->cek_username($username)->num_rows();
            
            if($status_email == "1" && $status_username == "1"){
                $res = "3";
            }else if($status_email == "1"){
                $res = "2";
            }else if($status_username == "1"){
                $res = "1";
            }else {
                $res = "Insert data successfully";
                $this->model_setting->insert_pengguna($nama,$username,$email,$pass);
            }
            
            $data = array("status"=>$res,"list"=>$this->model_setting->get_pengguna()->result());
            echo json_encode($data);
            
        } catch (Exception $ex) {
            $res = "failed->".$ex;
            $data = array("status"=>$res);
            echo json_encode($data);
        }
    }
    
    public function delete() {
        header("Content-type: application/json");
        $res = "";
        try {
            $this->load->helper('form');
            $id   = $this->input->post("id");
			
            $this->model_setting->delete_pengguna($id);
            
            if($id == ""){
                $res = "failed data error";
            }else{
                $res = "Delete data successfully";
            }
            
            $data = array("status"=>$res,"list"=>$this->model_setting->get_pengguna()->result());
            echo json_encode($data);
            
        } catch (Exception $ex) {
            $res = "failed->".$ex;
            $data = array("status"=>$res);
            echo json_encode($data);
        }
    }
    
    public function insert_role() {
        header("Content-type: application/json");
        try {
            $isi   = $this->input->post("isi");
            $id     = $this->input->post("id");
            
            $this->model_setting->insert_role($isi,$id);
            $res = "Berhasil ditambahkan";
            $data = array("status"=>$res,"list"=>$this->model_setting->get_pengguna()->result());
            echo json_encode($data);
            
        } catch (Exception $ex) {
            $res = "failed->".$ex;
            $data = array("status"=>$res);
            echo json_encode($data);
        }
    }
    
    public function hakakses() {
        header("Content-type: application/json");
        $res = "";
        try {
            $id = $this->input->post('id');
            $data = array("check"=>$this->model_setting->check_hakakses($id)->result());
            echo json_encode($data);
            
        } catch (Exception $ex) {
            $res = "failed->".$ex;
            $data = array("status"=>$res);
            echo json_encode($data);
        }
    }
}