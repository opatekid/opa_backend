<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lap_data_konsumen extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model("model_laporan");
        $this->load->model("mobile_v2/model_api_konsumen", "modelmu");
    }

    public function index() {
        $this->load->view('lap_data_konsumen');
    }    


    public function delete() {
        $id_pemilik            = $this->input->post("id");
        $this->modelmu->hapus_akun($id_pemilik);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        
        echo json_encode($data);
    }

    
//    public function caritgl() {
//        $tgl = $this->input->post("tanggal");
////        $tanggal = date($tgl, "Y-m-d H:i:s");
//        //20-11-2016
//        //[20,11,2016]
//        $arr_tgl = explode("-", $tgl);
//        $tanggal = $arr_tgl[2]."-".$arr_tgl[1]."-".$arr_tgl[0];
//        $tglawal = substr($tanggal, 0, 4)."-01-01";
//        $data = array("cari" => $this->model_laporan->caritgl_data_konsumen($tanggal,$tglawal)->result());
//        echo json_encode($data);
//    }
//    
//    public function caribln() {
//        $bulan = $this->input->post("bulan");
//        $tahun = $this->input->post("tahun");
//        $data = array("cari" => $this->model_laporan->caribln_data_konsumen($bulan,$tahun)->result());
//        echo json_encode($data);
//    }
//    public function carithn() {
//        $tahun = $this->input->post("tahun");
//        $data = array("cari"=>  $this->model_laporan->carithn_data_konsumen($tahun)->result());
//        echo json_encode($data);
//    }
    
    /////////serverside///////////
    public function get_data(){
        
        //$tglawal,$tanggal,$bulan,$tahun,$jenis,$tipe
                
        $requestData        = $this->input->post();
        $tgl            = $this->input->post("tanggal");
        $bulan              = $this->input->post("bulan");
        $tahun              = $this->input->post("tahun");
        $tipe               = $this->input->post("tipe");
        $tanggal            = "";
        $tglawal            = "";
        $arr_tgl = explode("-", $tgl);
        if($tipe == "1"){
            $tanggal = $arr_tgl[2]."-".$arr_tgl[1]."-".$arr_tgl[0];
            $tglawal = substr($tanggal, 0, 4)."-01-01";
        }
        
        $pesan = "";
        
        $count = $this->model_laporan->filter_data_konsumen_get_data($tglawal,$tanggal,$bulan,$tahun,$tipe);
        $totalFiltered = $count;
        $totalData =  $count;

        if( !empty($requestData['search']['value']) ) {
            $query = $this->model_laporan->filter_data_konsumen($requestData,$tglawal,$tanggal,$bulan,$tahun,$tipe);
            $totalFiltered = $query->num_rows();
            $pesan = "pencarian..";
        }

        $query = $this->model_laporan->order_data_konsumen($requestData,$tglawal,$tanggal,$bulan,$tahun,$tipe);

        $data = array();
        
        $no = $requestData['start']+1;
        
        $index = 0;
        foreach ($query->result() as $row){
            
            $nestedData=array(); 
            $nestedData[] = $no++;
            $nestedData[] = $row->tanggal_insert;
            $nestedData[] = $row->nama;
            $nestedData[] = $row->id_konsumen;
            $nestedData[] = $row->kontak;
            $nestedData[] = $row->alamat;
            $nestedData[] = "<a onclick='hapus_modal(".$row->id_konsumen.")''><i rel='tooltip' title='Hapus' style='cursor: pointer' class='icon icon-trash'></i></a>";
            $data[] = $nestedData;
            
            $index++;
        }

        $json_data = array(
            "pesan"           => $pesan,
            "draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
            "recordsTotal"    => intval( $totalData ),  // total number of records
            "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data"            => $data,   // total data array
            "json"            => $query->result()   // total data array
        );
        
        echo json_encode($json_data);  // send data as json format
    }
    
}