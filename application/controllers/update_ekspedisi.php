<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Update_ekspedisi extends CI_Controller {
	
    public function __construct(){
        parent::__construct();
        $this->load->model("mobile_v2/model_api_ekspedisi", "modelmu");
        $this->load->model("model_laporan");
        $this->load->model("model_transaksi");
        $this->load->model("model_master");
    }

    public function index() {
        $data["level"]=  $this->model_master->get_level()->result();
        $data["tran_upgrade"]=  array();
        $this->load->view('update_ekspedisi',$data);   
    }


    public function layanan($id_ekspedisi) {

        $layanans = $this->modelmu->get_layanan($id_ekspedisi);
        $data["layanans"] = $layanans;
        $data["id_ekspedisi"] = $id_ekspedisi;

        // echo json_encode($layanans); die();

        $this->load->view('ekspedisi_layanan',$data);   
    }

    public function agen($id_ekspedisi) {


        // echo "data"; die();
        $data['agen']  = $this->modelmu->get_agen_ekspedisi($id_ekspedisi)->result();
        $data['id_ekspedisi'] = $id_ekspedisi;

        $this->load->view('ekspedisi_agen',$data);   
        
        // echo json_encode($data);
    }
        

    public function add() {
        $this->load->view('update_ekspedisi_add',null);
    }

    public function edit($id_ekspedisi) {
        $ekspedisi = $this->modelmu->get_ekspedisi($id_ekspedisi);

        $data["ekspedisi"] = $ekspedisi;
        // echo json_encode($data); die();

        $this->load->view('update_ekspedisi_edit',$data);
    }

    public function edit_agen($id_agen) {
        $agen = $this->modelmu->get_agen($id_agen)->first_row();

        $data["agen"] = $agen;
        // echo json_encode($data); die();

        $this->load->view('update_agen_edit',$data);
    }


    public function tambah_layanan() {
        // echo "foto: " ; die();
        $nama       = $this->input->post("nama");
        $id_ekspedisi       = $this->input->post("id_ekspedisi");

        $this->modelmu->tambah_layanan($id_ekspedisi, $nama);

        
        $data = array("status" => "berhasil ditambahkan", "code" => 1, "data" => $id_ekspedisi);

        echo json_encode($data);
    }


    public function tambah_ekspedisi() {
        // echo "foto: " ; die();
        $id_ekspedisi = $this->modelmu->get_random_num();
        if ($id_ekspedisi == "") $id_ekspedisi = mt_rand(10000000,99999900);
        $nama       = $this->input->post("nama");
        $foto       = $_FILES["form_file"]['name'];

        $errors = array();

        $foto3 = "";


        if ($foto != "") {
            // echo "foto ada " . $foto;
            $foto2 = explode(".", $foto);
            $foto3 = $id_ekspedisi . "." . $foto2[count($foto2) - 1];
        } 
        
        if (isset($_FILES['form_file']) && !empty($_FILES['form_file'])) {

                if ($_FILES['form_file']['error'] != 4) {
                    $exp                     = explode(".", $_FILES["form_file"]['name']);
                    $ext                     = end($exp);
                    $config['upload_path']   = PATH_IMAGE_EKSPEDISI_WEBADMIN;
                    $config['allowed_types'] = '*';
                    $config['file_name']     = $foto3;
                    if (!is_dir(PATH_IMAGE_EKSPEDISI_WEBADMIN)) {
                        mkdir(PATH_IMAGE_EKSPEDISI_WEBADMIN, 0777, true);
                    }
                    
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    
                    if (!$this->upload->do_upload('form_file')) {
                        $errors[] = $this->upload->display_errors();
                    }
                }
            }

            // echo "id_ekspedisi: " . $id_ekspedisi; die();

        $this->modelmu->insert_ekspedisi($id_ekspedisi, $nama, $foto3);

        if (count($errors) <= 0) {
            $data = array("status" => "berhasil ditambahkan", "code" => 1, "data" => $id_ekspedisi);
        } else {
            $data = array("status" => "gaga ditamlbahkan", "code" => 0, "data" => $errors);
        }

        echo json_encode($data);
    }



    public function hapus()
    {

        $id_ekspedisi = $this->input->post("id_ekspedisi");
        
        $this->modelmu->hapus_akun($id_ekspedisi);
        $this->modelmu->delete_agen_by_ekspedisi($id_ekspedisi);
        $this->modelmu->delete_layanan_by_ekspedisi($id_ekspedisi);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data'] = array();
        
        echo json_encode($data);
    }


    public function delete_agen()
    {

        $id_agen = $this->input->post("id_agen");
        
        $this->modelmu->delete_agen($id_agen);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data'] = array();
        
        echo json_encode($data);
    }


    public function delete_layanan()
    {

        $id_layanan = $this->input->post("id_layanan");
        
        $this->modelmu->delete_layanan($id_layanan);
        
        $data['code']  = "1";
        $data['pesan'] = "Berhasil";
        $data['data'] = array();
        
        echo json_encode($data);
    }


    public function upload($path, $nama_file, $id_file)
    {
        try {
            $config['upload_path']   = $path;
            $config['file_name']     = $nama_file;
            $config['allowed_types'] = '*';
            $config['overwrite']     = TRUE;
            $config['overwrite']     = TRUE;
            
            file_put_contents($path, base64_decode($nama_file));
            
            // $this->load->library('upload', $config);
            // if ( ! $this->upload->do_upload($id_file))
            // {
            //     $error = array('error' => $this->upload->display_errors());
            
            // }   
            // else
            // {
            //     $upload_data = $this->upload->data();
            // }
            return "";
        }
        catch (Exception $exc) {
            return $exc->getTraceAsString();
        }
    }

    public function update($id_ekspedisi) {
        $nama = $this->input->post("nama");
        $foto       = $_FILES["form_file"]['name'];

        // echo $kontak . " .... " . $password;
        
        $errors = [];

        $foto3 = "";

        if ($foto != "") {
            // echo "foto ada " . $foto;
            $foto2 = explode(".", $foto);
            $foto3 = $id_ekspedisi . "." . $foto2[count($foto2) - 1];
        } 
        
        if (isset($_FILES['form_file']) && !empty($_FILES['form_file'])) {
                if ($_FILES['form_file']['error'] != 4) {
                    $exp                     = explode(".", $_FILES["form_file"]['name']);
                    $ext                     = end($exp);
                    $config['upload_path']   = PATH_IMAGE_EKSPEDISI_WEBADMIN;
                    $config['allowed_types'] = '*';
                    $config['file_name']     = $foto3;
                    $config['overwrite'] = TRUE;
                    
                    if (!is_dir(PATH_IMAGE_EKSPEDISI_WEBADMIN)) {
                        mkdir(PATH_IMAGE_EKSPEDISI_WEBADMIN, 0777, true);
                    }
                    
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    
                    if (!$this->upload->do_upload('form_file')) {
                        $errors[] = $this->upload->display_errors();
                    } 

                   $this->modelmu->set_foto($id_ekspedisi,$foto3);


                }
            }

            $this->modelmu->update_ekspedisi($id_ekspedisi,$nama);

             $data = array("status" => "berhasil diubah", "code" => 1, "data" => $id_ekspedisi);

        echo json_encode($data);
    }

    public function update_agen($id_agen) {
        $nama = $this->input->post("nama");
        $alamat = $this->input->post("alamat");
        $lat = $this->input->post("lat");
        $lng = $this->input->post("lng");
        
        $this->modelmu->update_agen($id_agen,$nama,$alamat,$lat,$lng);

        $data = array("status" => "berhasil diubah", "code" => 1, "data" => $id_ekspedisi);

        echo json_encode($data);
    }

    
    public function get_data(){

        $data = array();
        $pesan = "";
        
        $requestData        = $this->input->post();
        $count = $this->modelmu->get_data()->num_rows();
        $totalFiltered = $count;
        $totalData =  $count;  

        $query = $this->modelmu->get_data_ekspedisi($requestData);

        if( !empty($requestData['search']['value']) ) {
            // echo $requestData['search']['value']; die();
            $query = $this->modelmu->get_data_ekspedisi_filter($requestData);
            $totalFiltered = $query->num_rows();
            $pesan = "pencarian";
        }

            
            $no = $requestData['start']+1;
            
            $index = 0;
            $tipe = "";
            foreach ($query->result() as $row){
                $nestedData=array(); 
                $nestedData[] = $no++;
                $nestedData[] = $row->id_ekspedisi;
                $nestedData[] = "<img src='" . $row->foto . "' height='25' width='50' />";
                $nestedData[] = $row->nama;
                $nestedData[] = '<a onclick="buka_halaman(\'update_ekspedisi/layanan/' . $row->id_ekspedisi . '\', 11)"><i rel="tooltip" title="Layanan" style="cursor: pointer" class="icon icon-file"></i></a>';
                $nestedData[] = '<a onclick="buka_halaman(\'update_ekspedisi/agen/' . $row->id_ekspedisi . '\', 11)"><i rel="tooltip" title="Daftar Agen" style="cursor: pointer" class="icon icon-file"></i></a>';
                $nestedData[] = '<a onclick="buka_halaman(\'update_ekspedisi/edit/' . $row->id_ekspedisi . '\', 11)"><i rel="tooltip" title="Ubah" style="cursor: pointer" class="icon icon-pencil"></i></a> &nbsp ' . '<a onclick="hapus_modal(' . $row->id_ekspedisi . ')"><i rel="tooltip" title="Hapus" style="cursor: pointer" class="icon icon-trash"></i></a>' . ' &nbsp ';
                $data[] = $nestedData;
                
                $index++;
            }

        $json_data = array(
                "pesan"           => $pesan,
                "draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
                "recordsTotal"    => intval( $totalData ),  // total number of records
                "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data"            => $data,   // total data array
                "json"            => $query->result()   // total data array
            );
        
        echo json_encode($json_data);  // send data as json format
    }


    public function get_data_layanan(){

        $data = array();
        $pesan = "";
        
        $requestData        = $this->input->post();
        $count = $this->modelmu->get_data()->num_rows();
        $totalFiltered = $count;
        $totalData =  $count;  

        $query = $this->modelmu->get_data_layanan($requestData);

        if( !empty($requestData['search']['value']) ) {
            // echo $requestData['search']['value']; die();
            $query = $this->modelmu->get_data_layanan_filter($requestData);
            $totalFiltered = $query->num_rows();
            $pesan = "pencarian";
        }

            
            $no = $requestData['start']+1;
            
            $index = 0;
            $tipe = "";
            foreach ($query->result() as $row){
                $nestedData=array(); 
                $nestedData[] = $no++;
                $nestedData[] = $row->id_ekspedisi;
                $nestedData[] = "<img src='" . $row->foto . "' height='100' width='300' />";
                $nestedData[] = $row->nama;
                $nestedData[] = '<a onclick="buka_halaman(\'update_ekspedisi/layanan/' . $row->id_ekspedisi . '\', 11)"><i rel="tooltip" title="Layanan" style="cursor: pointer" class="icon icon-file"></i></a>';
                $nestedData[] = '<a onclick="buka_halaman(\'update_ekspedisi/edit/' . $row->id_ekspedisi . '\', 11)"><i rel="tooltip" title="Ubah" style="cursor: pointer" class="icon icon-pencil"></i></a> &nbsp ' . '<a onclick="hapus_modal(' . $row->id_ekspedisi . ')"><i rel="tooltip" title="Hapus" style="cursor: pointer" class="icon icon-trash"></i></a>' . ' &nbsp ';
                $data[] = $nestedData;
                
                $index++;
            }

        $json_data = array(
                "pesan"           => $pesan,
                "draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
                "recordsTotal"    => intval( $totalData ),  // total number of records
                "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data"            => $data,   // total data array
                "json"            => $query->result()   // total data array
            );
        
        echo json_encode($json_data);  // send data as json format
    }


    public function get_data_agen($id_ekspedisi){

        $data = array();
        $pesan = "";
        
        $requestData        = $this->input->post();
        $count = $this->modelmu->get_data()->num_rows();
        $totalFiltered = $count;
        $totalData =  $count;  

        $query = $this->modelmu->get_data_agen($id_ekspedisi, $requestData);

        if( !empty($requestData['search']['value']) ) {
            // echo $requestData['search']['value']; die();
            $query = $this->modelmu->get_data_agen_filter($id_ekspedisi, $requestData);
            $totalFiltered = $query->num_rows();
            $pesan = "pencarian";
        }

            
        $no = $requestData['start']+1;
            
        $index = 0;
        $tipe = "";
        foreach ($query->result() as $row){
            $nestedData=array(); 
            $nestedData[] = $no++;
            $nestedData[] = $row->nama;
            $nestedData[] = $row->alamat;
            $nestedData[] = '<a onclick="buka_halaman(\'update_ekspedisi/edit_agen/' . $row->id_agen . '\', 11)"><i rel="tooltip" title="Ubah" style="cursor: pointer" class="icon icon-pencil"></i></a> &nbsp ' . '<a onclick="hapus_modal(' . $row->id_agen . ')"><i rel="tooltip" title="Hapus" style="cursor: pointer" class="icon icon-trash"></i></a>' . ' &nbsp ';
            $data[] = $nestedData;
            $index++;
        }

        $json_data = array(
                "pesan"           => $pesan,
                "draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
                "recordsTotal"    => intval( $totalData ),  // total number of records
                "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data"            => $data,   // total data array
                "json"            => $query->result()   // total data array
            );
        
        echo json_encode($json_data);  // send data as json format
    }


    public function tambah_toko_batch($count) {

        $id_ekspedisis = $this->modelmu->get_random_nums($count);
        $phones = $this->modelmu->get_random_phones($count);

        $tipe = 3;
        $kontak_sms2 = '081297910898';
        $id_partner = '';
        $password = "pin123";
        $username = "User Baru";
        $kode_sms = "1234";
        $id_provinsi = "12";
        $id_kota = "163";
        $radius = "0";
        $min_antar = "0";
        $fee = "0";

        $i = 0;
        foreach ($phones as $no_telp) {
            $id_ekspedisi = $id_ekspedisi[$i]['random_num'];
            $this->modelmu->tambah_toko($id_ekspedisi, $id_partner, $no_telp, $no_telp, $kontak_sms2, $password, $username, $kode_sms, $id_provinsi, $id_kota, $radius, $min_antar, $fee);

            $i++;
        }
    }
    
}