<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sett_dkredit extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model("model_setting");
    }

    public function index() {
        $data["dkredit"]=  $this->model_setting->get_dkredit()->result();
        $this->load->view('sett_dkredit',$data);
    }
    
    public function update() {
        header("Content-type: application/json");
        $res = "";
        try {
            $this->load->helper('form');
            $id      = $this->input->post("id");
            $dkredit = $this->input->post("dkredit");
			
            $this->model_setting->update_dkredit($id,$dkredit);
            
            $res = "Update data successfully";
            
            $data = array("status"=>$res,"list"=>$this->model_setting->get_dkredit()->result());
            echo json_encode($data);
            
        } catch (Exception $ex) {
            $res = "failed->".$ex;
            $data = array("status"=>$res);
            echo json_encode($data);
        }
    }
}