<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mst_kategori_usaha extends CI_Controller {
	
    public function __construct(){
        parent::__construct();
        $this->load->model("model_master");
    }

    public function index() {
        $data["mst_kategori"]=  $this->model_master->get_kategori()->result();
        $data["level"]=  $this->model_master->get_radius()->result();
        $this->load->view('mst_kategori_usaha',$data);
        
    }
    
    public function insert_kategori() {
        header("Content-type: application/json");
        $res = "";
        try {
            $this->load->helper('form');
            $nama   = $this->input->post("nama");
            $tipe   = $this->input->post("tipe");
            $level  = $this->input->post("level");
            $id     = $this->input->post("id");
            $ibiru  = $this->input->post("ibiru");
            $ihitam = $this->input->post("ihitam");
            $idsession = $this->session->userdata("id");
            
            if($id == ""){
                $this->model_master->insert_kategori($nama,$tipe,$level,$id,$ibiru,$ihitam);
                $max_id = $this->model_master->get_max_id();
                $ibiru2 = explode(".", $ibiru);
                $ibiru3 = "icon_biru".$max_id.".".$ibiru2[1];
                rename(PATH_IMAGE_TEMP."icon_biru".$idsession.".".$ibiru2[1], PATH_IMAGE_IKATEGORI.$ibiru3);

                $ihitam2 = explode(".", $ihitam);
                $ihitam3 = "icon_hitam".$max_id.".".$ihitam2[1];
                rename(PATH_IMAGE_TEMP."icon_hitam".$idsession.".".$ihitam2[1], PATH_IMAGE_IKATEGORI.$ihitam3);
                $res = "Insert data successfully";
            }else{
                $get_biru = $this->model_master->get_foto_biru($id);
                $get_hitam = $this->model_master->get_foto_hitam($id);
                if($ibiru!=$get_biru->icon_biru && $ihitam!=$get_hitam->icon_hitam){
                    $ibiru2 = explode(".", $ibiru);
                    $ibiru3 = "icon_biru".$id.".".$ibiru2[1];
                    rename(PATH_IMAGE_TEMP."icon_biru".$idsession.".".$ibiru2[1], PATH_IMAGE_IKATEGORI.$ibiru3);

                    $ihitam2 = explode(".", $ihitam);
                    $ihitam3 = "icon_hitam".$id.".".$ihitam2[1];
                    rename(PATH_IMAGE_TEMP."icon_hitam".$idsession.".".$ihitam2[1], PATH_IMAGE_IKATEGORI.$ihitam3);
                    $this->model_master->insert_kategori($nama,$tipe,$level,$id,$ibiru3,$ihitam3);
                }elseif($ibiru!=$get_biru->icon_biru){
                    $ibiru2 = explode(".", $ibiru);
                    $ibiru3 = "icon_biru".$id.".".$ibiru2[1];
//                    $ihitam2 = explode(".", $ihitam);
//                    $ihitam3 = "icon_hitam".$id.".".$ihitam2[1];
                    rename(PATH_IMAGE_TEMP."icon_biru".$idsession.".".$ibiru2[1], PATH_IMAGE_IKATEGORI.$ibiru3);
                    $this->model_master->insert_kategori($nama,$tipe,$level,$id,$ibiru3,$ihitam);
                }elseif($ihitam!=$get_hitam->icon_hitam){
//                    $ibiru2 = explode(".", $ibiru);
//                    $ibiru3 = "icon_biru".$id.".".$ibiru2[1];
                    $ihitam2 = explode(".", $ihitam);
                    $ihitam3 = "icon_hitam".$id.".".$ihitam2[1];
                    
                    rename(PATH_IMAGE_TEMP."icon_hitam".$idsession.".".$ihitam2[1], PATH_IMAGE_IKATEGORI.$ihitam3);
                    $this->model_master->insert_kategori($nama,$tipe,$level,$id,$ibiru,$ihitam3);
                }else{
                    $this->model_master->insert_kategori($nama,$tipe,$level,$id,$ibiru,$ihitam);
                }
                $res = "Update data successfully";
            }
            
            $data = array("status"=>$res,"list"=>$this->model_master->get_kategori()->result());
            echo json_encode($data);
            
        } catch (Exception $ex) {
            $res = "failed->".$ex;
            $data = array("status"=>$res);
            echo json_encode($data);
        }
    }
    
    public function delete() {
        header("Content-type: application/json");
        $res = "";
        try {
            $this->load->helper('form');
            $id   = $this->input->post("id");
			
            $this->model_master->delete_kategori($id);
            
            if($id == ""){
                $res = "failed data error";
            }else{
                $res = "Delete data successfully";
            }
            
            $data = array("status"=>$res,"list"=>$this->model_master->get_kategori()->result());
            echo json_encode($data);
            
        } catch (Exception $ex) {
            $res = "failed->".$ex;
            $data = array("status"=>$res);
            echo json_encode($data);
        }
    }
    
    function upload(){
        try {
             $this->load->helper('form') ;
             $path_file = '';
             $name_file = '';
//              $path_icon='';
               $config['upload_path'] = PATH_IMAGE_TEMP; 
               $config['file_name'] = $this->input->post('name');
               $config['allowed_types'] = '*';
               $config['overwrite'] = TRUE;
               $config['max_size']	= '1000000000';
               $config['overwrite'] = TRUE;
              // echo 'nama adalah '.$nama;	
               $this->load->library('upload', $config);
               if ( ! $this->upload->do_upload('image_file'))
               {
                   $error = array('error' => $this->upload->display_errors());
                   echo 'error '.$error['error'];
               }
               else
               {
                   $upload_data = $this->upload->data();
                   $path_file = $upload_data['full_path'];
                   $name_file = $upload_data['file_name'];
               }
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
    function upload1(){
        try {
             $this->load->helper('form') ;
             $path_file = '';
             $name_file = '';
//              $path_icon='';
               $config['upload_path'] = PATH_IMAGE_TEMP; 
               $config['file_name'] = $this->input->post('name');
               $config['allowed_types'] = '*';
               $config['overwrite'] = TRUE;
               $config['max_size']	= '1000000000';
               $config['overwrite'] = TRUE;
              // echo 'nama adalah '.$nama;	
               $this->load->library('upload', $config);
               if ( ! $this->upload->do_upload('image_1file'))
               {
                   $error = array('error' => $this->upload->display_errors());
                   echo 'error '.$error['error'];
               }
               else
               {
                   $upload_data = $this->upload->data();
                   $path_file = $upload_data['full_path'];
                   $name_file = $upload_data['file_name'];
               }
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
}