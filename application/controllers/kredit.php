<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kredit extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model("model_kredit");
    }

    public function index() {
        $data["tran_isi"]=  $this->model_kredit->get_kredit()->result();
        $this->load->view('kredit',$data);
        
    }
    
    public function detail() {
        $id = $this->input->post("id");
        $data = array("detail" => $this->model_kredit->get_kredit_detail($id)->result());
        echo json_encode($data);
    }
}