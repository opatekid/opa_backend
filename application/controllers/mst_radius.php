<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mst_radius extends CI_Controller {
	
    public function __construct(){
        parent::__construct();
        $this->load->model("model_master");
    }

    public function index() {
        $data["mst_radius"]=  $this->model_master->get_radius()->result();
        $this->load->view('mst_radius',$data);   
    }
    
    public function insert_radius() {
        header("Content-type: application/json");
        $res = "";
        try {
            $this->load->helper('form');
            $nama = $this->input->post("nama");
            $radius = $this->input->post("radius");
            $harga = $this->input->post("harga");
            $id   = $this->input->post("id");
			
            $this->model_master->insert_radius($nama,$radius,$harga,$id);
            
            if($id == ""){
                $res = "Insert data successfully";
            }else{
                $res = "Update data successfully";
            }
            
            $data = array("status"=>$res,"list"=>$this->model_master->get_radius()->result());
            echo json_encode($data);
            
        } catch (Exception $ex) {
            $res = "failed->".$ex;
            $data = array("status"=>$res);
            echo json_encode($data);
        }
    }
    
    public function delete() {
        header("Content-type: application/json");
        $res = "";
        try {
            $this->load->helper('form');
            $id   = $this->input->post("id");
			
            $this->model_master->delete_radius($id);
            
            if($id == ""){
                $res = "failed data error";
            }else{
                $res = "Delete data successfully";
            }
            
            $data = array("status"=>$res,"list"=>$this->model_master->get_radius()->result());
            echo json_encode($data);
            
        } catch (Exception $ex) {
            $res = "failed->".$ex;
            $data = array("status"=>$res);
            echo json_encode($data);
        }
    }
}