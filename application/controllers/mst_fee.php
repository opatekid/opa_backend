<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mst_fee extends CI_Controller {
	
    public function __construct(){
        parent::__construct();
        $this->load->model("model_master");
    }

    public function index() {
        $data["mst_fee"]=  $this->model_master->get_fee()->result();
        $this->load->view('mst_fee',$data);
    }
    
    public function insert_fee() {
        header("Content-type: application/json");
        $res = "";
        try {
            $this->load->helper('form');
            $batasa = $this->input->post("batasa");
            $batasb = $this->input->post("batasb");
            $nominal = $this->input->post("nominal");
            $id   = $this->input->post("id");
			
            $this->model_master->insert_fee($batasb,$batasa,$nominal,$id);
            
            if($id == ""){
                $res = "Insert data successfully";
            }else{
                $res = "Update data successfully";
            }
            
            $data = array("status"=>$res,"list"=>$this->model_master->get_fee()->result());
            echo json_encode($data);
            
        } catch (Exception $ex) {
            $res = "failed->".$ex;
            $data = array("status"=>$res);
            echo json_encode($data);
        }
    }
    
    public function delete() {
        header("Content-type: application/json");
        $res = "";
        try {
            $this->load->helper('form');
            $id   = $this->input->post("id");
			
            $this->model_master->delete_fee($id);
            
            if($id == ""){
                $res = "failed data error";
            }else{
                $res = "Delete data successfully";
            }
            
            $data = array("status"=>$res,"list"=>$this->model_master->get_fee()->result());
            echo json_encode($data);
            
        } catch (Exception $ex) {
            $res = "failed->".$ex;
            $data = array("status"=>$res);
            echo json_encode($data);
        }
    }
}