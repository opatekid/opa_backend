<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Merchant_promo extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model("mobile_v2/model_api_pemilik", "modelmu");
        $this->load->model("model_laporan");
        $this->load->model("model_transaksi");
        $this->load->model("model_merchant_promo");
        $this->load->model("model_master");
        $this->load->model("model_partner");
        $this->load->helper('string');
    }

    public function index() {
        // echo CI_VERSION; die();
    	$id_partner = $this->session->userdata('id');
        $data['active_promo'] = $this->model_merchant_promo->list_active_promo($id_partner)->result();
        $this->load->view('merchant_promo_index',$data);
    }

    public function add() {
    	$data = array();
        $this->load->view('merchant_promo_add',$data);
    } 

    public function edit($id) {
        $data = array();
        $data['merchant_promo'] = $this->model_merchant_promo->get($id);
        $this->load->view('merchant_promo_edit',$data);
    } 

    public function create() {
        header("Content-type: application/json");
        $res = "";
        try {
            $this->load->helper('form');
            $nama               = $this->input->post("nama");
            $tipe          = $this->input->post("tipe");
            $start_date        = $this->input->post("start_date");
            $end_date        = $this->input->post("end_date");


            $id_partner = $this->session->userdata('id');
            $insert_id = $this->model_merchant_promo->create_promo($nama, $id_partner, $tipe, $start_date, $end_date);

            // $file = $this->input->post('file');

            if (intval($tipe) > 1) { 
            	self::insert_produk_promo_batch($insert_id);
            } else {  
            	self::set_harga_promo($insert_id);
            }

            
            $res = "Promo created successfully";
            
            $data = array("status"=>$res, "code" => 1);
            echo json_encode($data);
            
        } catch (Exception $ex) {
            $res = "failed->".$ex;
            $data = array("status"=>$res);
            echo json_encode($data);
        }
    }

    public function update($id) {
        header("Content-type: application/json");
        $res = "";
        try {
            $this->load->helper('form');
            $nama               = $this->input->post("nama");
            $tipe          = $this->input->post("tipe");
            $start_date        = $this->input->post("start_date");
            $end_date        = $this->input->post("end_date");


            $id_partner = $this->session->userdata('id');
            $promo = $this->model_merchant_promo->get($id);

            $data = array(
                "nama" => $nama,
                "tipe" => $tipe,
                "start_date" => $start_date,
                "end_date" => $end_date 
            );

            $this->model_merchant_promo->delete_produk_by_promo_id($id);
            $this->model_merchant_promo->update($id, $data);

            $file = $this->input->post('file');

            if (intval($tipe) > 1) { 
                self::insert_produk_promo_batch($id);
            } else {  
                self::set_harga_promo($id);
            }

            
            $res = "Promo updated successfully";
            
            $data = array("status"=>$res, "code" => 1);
            echo json_encode($data);
            
        } catch (Exception $ex) {
            $res = "failed->".$ex;
            $data = array("status"=>$res);
            echo json_encode($data);
        }
    }

    public function produk_promo($id_promo) {
        // echo CI_VERSION; die();

        $data['produk_promo'] = array();

        // echo "ok"; die();

        $promo = $this->model_merchant_promo->get($id_promo);
        if (intval($promo->tipe) == 1) {
            $data["produk_promo"]=  $this->model_merchant_promo->get_produk_diskon($id_promo)->result(); // produk harga diskon
        } else {
            $data["produk_promo"]=  $this->model_merchant_promo->get_produk_promo($id_promo)->result(); // free items
        }

        $data['promo'] = $promo;

        // echo json_encode($data);

        $this->load->view('produk_promo',$data);
    }

    public function insert_produk_promo_batch($id_merchant_promo)
    {
        $this->load->library('excel');

        $min_antar = "1";

        $file = $_FILES;

        $return = $this->upload_xls(PATH_XLSX_PRODUK, $id_merchant_promo, $file, $id_merchant_promo);

        $inputFileName = PATH_XLSX_PRODUK . $return;


        //  Read your Excel workbook
        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch(Exception $e) {
            die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
        }

        //  Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0); 
        $highestRow = $sheet->getHighestRow(); 
        $highestColumn = $sheet->getHighestColumn();

        $header = array();
        $rowData = $sheet->rangeToArray('A' . "1" . ':' . $highestColumn . "1",
                                            NULL,
                                            TRUE,
                                            FALSE);
        $header = $rowData[0];
        $rows = array();
        $k = 0;
        $id_produk_pos = 0;
        $qty1_pos = 0;
        $id_produk2_pos = 0;
        $qty2_pos = 0;
        foreach ($header as $header_col) {
            if ($header_col == 'id_barcode') {
                $id_produk_pos = $k;
            } else if ($header_col == 'qty') {
                $qty1_pos = $k;
            } if ($header_col == 'id_barcode2') {
                $id_produk2_pos = $k;
            } if ($header_col == 'qty2') {
                $qty2_pos = $k;
            } 
            $k++;
        }

        for ($row = 2; $row <= $highestRow; $row++){ 
            //  Read a row of data into an array
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                            NULL,
                                            TRUE,
                                            FALSE);
            
            $rows[] = $rowData[0];
        }

        $this->model_merchant_promo->insert_produk_promo_batch($id_merchant_promo, $header, $rows);

    }

    public function set_harga_promo($id_merchant_promo)
    {
        $this->load->library('excel');

        $min_antar = "1";

        $file = $_FILES;

        $return = $this->upload_xls(PATH_XLSX_PRODUK, $id_merchant_promo, $file, $id_merchant_promo);

        $inputFileName = PATH_XLSX_PRODUK . $return;


        //  Read your Excel workbook
        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch(Exception $e) {
            die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
        }

        //  Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0); 
        $highestRow = $sheet->getHighestRow(); 
        $highestColumn = $sheet->getHighestColumn();

        $header = array();
        $rowData = $sheet->rangeToArray('A' . "1" . ':' . $highestColumn . "1",
                                            NULL,
                                            TRUE,
                                            FALSE);
        $header = $rowData[0];
        $rows = array();
        $k = 0;
        $id_produk_pos = 0;
        $harga_pos = 0;
        foreach ($header as $header_col) {
            if ($header_col == 'id_barcode') {
                $id_produk_pos = $k;
            } else if ($header_col == 'harga_promo') {
                $harga_pos = $k;
            } 
            $k++;
        }

        for ($row = 2; $row <= $highestRow; $row++){ 
            //  Read a row of data into an array
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                            NULL,
                                            TRUE,
                                            FALSE);
            
            $rows[] = $rowData[0];
        }

        $id_partner = $this->session->userdata('id');
        $pemiliks = $this->model_partner->get_pemilik_of_partner($id_partner);

        // echo json_encode($pemiliks); die();

        foreach ($pemiliks as $pemilik) {
        	$this->model_merchant_promo->set_harga_promo_batch($pemilik->id_pemilik, $header, $rows, $id_merchant_promo);
        }
        $this->model_merchant_promo->insert_produk_promo_diskon_batch($header, $rows, $id_merchant_promo);
    }


    public function banners($id_merchant_promo) {
        $data['banners'] = $this->model_merchant_promo->get_banners($id_merchant_promo)->result();
        $data['id_merchant_promo'] = $id_merchant_promo;
        $data['promo'] = $this->model_merchant_promo->get($id_merchant_promo);

        // echo json_encode($data); die();
        $this->load->view('merchant_promo_banners',$data);
    }

    public function delete_banner() {
        $id = $this->input->post('id');
        $this->model_merchant_promo->delete_banner($id);
            
            $res = "Banner deleted successfully";
            
            $data = array("status"=>$res, "code" => 1);
            echo json_encode($data);
    }

    public function delete() {
        $id = $this->input->post('id');
        $this->model_merchant_promo->delete($id);
            
            $res = "Promo deleted successfully";
            
            $data = array("status"=>$res, "code" => 1);
            echo json_encode($data);
    }

    public function upload_banner($id_merchant_promo) {
        $last_promo = $this->model_merchant_promo->get_last_banner();
        $id = intval($last_promo != null ? $last_promo->id : "0") + 1;
        $foto       = $_FILES["form_file"]['name'];

        $errors = [];

        if ($foto != "") {
                    $id = $this->model_merchant_promo->add_banner($id_merchant_promo);
                    // echo "foto ada " . $foto;
                    $foto2 = explode(".", $foto);
                    $foto3 = $id . "." . $foto2[count($foto2) - 1];
                    $this->model_merchant_promo->set_banner($id, $foto3);
                } 
                
                if (isset($_FILES['form_file']) && !empty($_FILES['form_file'])) {
                        if ($_FILES['form_file']['error'] != 4) {
                            $exp                     = explode(".", $_FILES["form_file"]['name']);
                            $ext                     = end($exp);
                            if (is_file(PATH_IMAGE_PROMO_WEBADMIN . $id . ".".$ext)) {
                                unlink(PATH_IMAGE_PROMO_WEBADMIN . $id . ".".$ext);
                            }
                            $config['upload_path']   = PATH_IMAGE_PROMO_WEBADMIN;
                            $config['allowed_types'] = '*';
                            $config['file_name']     = $foto3;
                            if (!is_dir(PATH_IMAGE_PROMO_WEBADMIN)) {
                                mkdir(PATH_IMAGE_PROMO_WEBADMIN, 0777, true);
                            }
                            
                            $this->load->library('upload', $config);
                            $this->upload->initialize($config);
                            
                            if (!$this->upload->do_upload('form_file')) {
                                $errors[] = $this->upload->display_errors();
                            }
                        }
                    }


                $res = "Banner uploaded successfully";
            $data = array("status"=>$res, "code" => 1);
            echo json_encode($data);


    }

    public function upload_xls($path, $nama_file, $files, $id_file)
    {   
        $exp                     = explode(".", $nama_file);
        $ext                     = end($exp);  

        $name = $_FILES["form_file"]["name"];
        $exploded = explode(".", $name);
        $ext = end($exploded); 
        // echo $path.$id_file.".".$ext; die();

        if (is_file($path.$id_file.".".$ext)) {
            unlink($path.$id_file.".".$ext);
        }
        try {
            
            $config['upload_path']   = $path;
            $config['file_name']     = $id_file;
            $config['allowed_types'] = '*';
            $config['overwrite']     = TRUE;

            if (!is_dir($path)) {
                mkdir($path, 0777, true);
            }
            
            if (isset($files['form_file']) && !empty($files['form_file'])) {
                if ($files['form_file']['error'] != 4) {

                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    
                    if (!$this->upload->do_upload('form_file')) {
                        $errors = $this->upload->display_errors();
                        return "";
                        // echo json_encode($errors); die();
                    } else {
                        $upload_data = $this->upload->data();
                        return $id_file.".".$ext;
                        // echo json_encode($upload_data); 
                    }
                }
            } 

            return $id_file.".".$ext;
        }
        catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return "";
        }
    }

}