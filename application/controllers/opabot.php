<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Opabot extends CI_Controller {

	protected $_sessData;
	protected $_sessAction;
	protected $_sessOrderid;

	function __construct()
	{
		parent::__construct();
		
		$this->config->load('telegram');
		$this->load->library('session');

		$this->load->model('telegram');
        $this->load->model("mobile_v2/model_api_pemilik", "modelmu");
		$this->load->model('model_kurir');
		
		// $this->_sessData = $this->session->userdata();
		
	}
	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function getSessionData()
	{
		print_r($this->session->userdata());
	}

	public function setSessionData()
	{
		//$_sess = array();
		$_sess["S164541585"] = array("action"=>"set_order", "order_id"=>rand(000000,9999999));
		$this->session->set_userdata($_sess);
	}

	public function unsetUserData()
	{
		$_sess["n9999"] = array("action"=>"", "order_id"=>0);
		$this->session->unset_userdata('n9999');
	}
	
	public function setWebhook()
	{
		$this->Telegram->RequestWebhook("setWebhook");

	}
	
	public function testParse()
	{
		echo $this->parseSession("164541585");
	}

	private function parseSession($userchat)
	{
		$this->_sessData = $this->session->userdata();
		//print_r($this->_sessData);
		if(is_array($this->_sessData))
		{
			//print_r(count($this->_sessData));
			log_message('error', "ARRAY ".count($this->_sessData));
			if(count($this->_sessData) > 0)
			{
				$found = false;
				foreach ($this->_sessData as $key => $val)
				{
					//$skey = substr($userchat, 1,strlen($userchat));
					//print_r("S".$userchat ." == ".$key."\t\n");
					log_message('info', $userchat ." == ".$key);
					if($userchat == $key)
					{
						$found = true;
						$this->_sessAction = $val['action'];
						$this->_sessOrderid = $val['order_id'];
						break;
					}
				}
				if($found) return 1;
			}
		}
		return 0;
	}

	public function getHook()
	{
		if($this->telegram->text_command("start")){
			log_message('error', "Found command and replay");
			
			$acontact = $this->telegram->user;
			$tele_id = $acontact->id;
			$kurir = $this->model_kurir->get_by_teleid($tele_id)->row();

			if ($kurir == null) {
				$this->telegram->send
					->keyboard()
						->row()
							->button("Share ", "contact")
							->end_row()
					->show(true, true)
					->text("Silahkan share contact Anda! ", "Markdown")
					->send();
			} else {
				$this->telegram->send
					->notification(FALSE)
					->keyboard()->selective(FALSE)->hide()
					->text("Thank you. Found Contact phone number ")
				->send();
			}
		}
		

		if($this->telegram->text_command("invoiceText")){
			$sparateTop = str_pad("", 50, "-", STR_PAD_BOTH);
			$sparate = str_pad("", 28, "-", STR_PAD_BOTH);
			
			$msg = "<i>Minggu, 28 Agustus 2018 / 06:41</i>\n";
			$msg .= "#Inv: 426CJK0\n";
			$msg .= "\n";
			$msg .= "<b>Toko :</b>\n";
			$msg .= "Sodaqo Birrul Smart\n";
			$msg .= "Jl. Raya Condet, No 32 Cililitan\n";
			$msg .= "Kramat Jati - Jaktim\n";
			$msg .= "087888783819\n";
			$msg .= "\n";
			$msg .= "<b>Tujuan : </b>\n";
			$msg .= "Lokasi Sekarang\n";
			$msg .= "\n";
			$msg .= "<b>Penerima : </b>\n";
			$msg .= "Imam Mahmudi\n";
			$msg .= "\n";
			$msg .= "<b>Alamat : </b>\n";
			$msg .= "Jl Cendan II no 24 Rt 03/11\n";
			$msg .= "Utan Kayu Selatan, Jaktim\n";
			$msg .= "087888783819\n";
			$msg .= "\n";
			$msg .= "<b>Keterangan : </b>\n";
			$msg .= "-\n";
			$msg .= "\n";
			$msg .= "\n";
			$msg .= $sparateTop."\n";
			$msg .= "1. Soklin 2KG\n";
			$msg .= "   10 x 8.000".str_repeat(".",(31-strlen("   10 x 8.000")))."80.000\n";
			//$msg .= $sparate."\n";
			$msg .= "2. Aqua Galon \n";
			$msg .= "   2 x 18.000".str_repeat(".",(31-strlen("   2 x 18.000")))."36.000\n";
			$msg .= $sparateTop."\n";
			$msg .= "ADMIN APLIKASI".str_repeat(" ",(31-strlen("ADMIN APLIKASI")))."0.0\n";
			$msg .= "ONGKOS KIRIM".str_repeat(" ",(28-strlen("ONGKOS KIRIM")))."2.000\n";
			$msg .= "<b>TOTAL</b>".str_repeat(" ",(34-strlen("TOTAL")))."<b>65.000</b>\n";
			$msg .= "\n";
			$msg .= "\n";
			$msg .= "Terima kasih sudah belanja di OPA";
			$msg .= "\n";
			$this->telegram->send
				->text($msg, "HTML")
				->send();

			$this->telegram->send
				->location("-6.204620","106.866830")
				->send();
		}


		if($this->telegram->contact()) // if detect shared contact
		{
			$acontact = $this->telegram->data('contact');
			log_message('error', "Found Contact phone number " . $acontact->phone_number);
			$phone = $this->model_api_kurir->cek_no_telp($acontact->phone_number);
			if ($phone == "0") {
				$ret = $this->model_kurir->insert_kurir($acontact->first_name . " " . $acontact->last_name, $acontact->phone_number, $acontact->user_id);
				$this->telegram->send
					->notification(FALSE)
					->keyboard()->selective(FALSE)->hide()
					->text("Thank you. Found Contact phone number ")
				->send();
			} else {
				$this->telegram->send
					->notification(FALSE)
					->keyboard()->selective(FALSE)->hide()
					->text("Phone number already exists")
				->send();
			}
		}
		

		if($this->telegram->text_command("inlineButton"))
		{
			$order_id = MD5(rand(000000,999999));
			$this->telegram->send
				->inline_keyboard()
					->row()
						->button("Set Order", "set_order-".$order_id, "callback_data")
						->button("Set Cancel", "cancel-".$order_id, "callback_data")
						->end_row()
				->show(true, true)
				->text("Set Invoice to", "Markdown")
				->send();
		}
		

		/*
		receiver text action
		1. session->action[cancel] ya
		2. session->action[cancel] tidak

		1. session->action[set_order] Antar Sekarang
		2. session->action[set_order] Tunggu 10 Menit
		3. session->action[set_order] Tunggu 20 Menit
		4. session->action[set_order] Tunggu 20 Menit
		5. session->action[set_order] Tutup
		*/
		if($this->telegram->text_has(array("Tutup","Antar Sekarang","Tunggu 10 Menit","Tunggu 20 Menit","ya","cancel")))
		{
			
			
			
		}


		if($this->telegram->answer_if_callback("Siap akan diupdate", FALSE)){
			$data = $this->telegram->callback;
			log_message('error', "Anser CallBack msg id " .$this->telegram->getMessageid() . " Data is ".$data);
			
			$data_o = explode("-", $data);
			$key = $data_o[0];
			$order_id = $data_o[1];

			$id_pemilik = "";
			$id_perkiraan_antar = "";
			$pesan_perkiraan_antar = "";

			if(count($data_o) >= 4) {
				$id_pemilik = $data_o[2];
				$id_perkiraan_antar = $data_o[3];
			}

			if ($key == "deliver")
			{
				$returnValue = self::proses_order($order_id, $id_pemilik, $id_perkiraan_antar, $pesan_perkiraan_antar);
				
				if (strpos($returnValue, 'Maaf') !== false) {
					$this->telegram->send
						->text($returnValue, "HTML")
						->send();
				} else { 
					$this->telegram->send
	                ->inline_keyboard()
	                    ->row()
	                        ->button("Selesaikan", "finish-".$order_id ."-".$id_pemilik . "--", "callback_data")
	                        ->end_row()
	                ->show(true, true)
	                ->text($returnValue , "HTML")
	                ->send();
	            }
			}
			else if($key == "finish")
			{
				$returnValue = self::selesai_order($order_id, $id_pemilik);
				$this->telegram->send
					->text($returnValue, "HTML")
					->send();
			}

			
		}
	}

	public function proses_order($id_order, $id_pemilik, $id_perkiraan_antar, $pesan_perkiraan_antar)
    {
        
        $order = $this->modelmu->get_konsumen_order($id_order);
        if (intval($order->status) == 2 || intval($order->status) == 3 || intval($order->status) == 4) {
        	if (intval($order->status) == 2) {
            	return "Maaf, order <b>" . $id_order . "</b> sudah diproses";
        	} else if (intval($order->status) == 3) {
            	return "Maaf, order <b>" . $id_order . "</b> sudah dibatalkan";
            } else if (intval($order->status) == 4) {
            	return "Maaf, order <b>" . $id_order . "</b> sudah selesai";
            }
            
        } else {
            $this->modelmu->proses_order($id_order, $id_perkiraan_antar, $pesan_perkiraan_antar);
        }

        // $data['data']  = $this->modelmu->get_order($id_pemilik);
        // $data['code']  = "1";
        // $data['pesan'] = "sukses";
        
        $data_konsumen = $this->modelmu->notif_get_konsumen($id_order);
        $data_order    = $this->modelmu->notif_get_order($id_order);
        $nama_konsumen = "";
        $id_konsumen   = "";
        $id            = "";
        $total         = "";
        $status        = "";
        
        $arr_token = array();
        foreach ($data_konsumen as $row) {
            array_push($arr_token, $row->token);
            $nama_konsumen = $row->nama;
            $id_konsumen   = $row->id_konsumen;
        }
        
        foreach ($data_order as $row) {
            $id     = $row->id_order;
            $total  = $row->total;
            $status = $row->status;
        }

        $pemilik = $this->modelmu->get_pemilik($id_pemilik);

        $perkiraan_antar = "";
        if (intval($data_order[0]->id_perkiraan_antar) != 0) {
            $perkiraan_antar_arr = $this->modelmu->get_perkiraan_antar($data_order[0]->id_perkiraan_antar);
            $perkiraan_antar_obj = $perkiraan_antar_arr[0];
            $perkiraan_antar = $perkiraan_antar_obj->nama;
        } else {
            $perkiraan_antar = $pesan_perkiraan_antar;
        }

        
        $message = array(
            "jenis" => "1",
            "id_konsumen" => $id_konsumen,
            "nama_pemilik" => $pemilik[0]->nama,
            "perkiraan_antar" => $perkiraan_antar,
            "id_order" => $id,
            "total" => $total,
            "status" => $status,
            "notif" => $nama_konsumen . " yth, Order anda sedang diproses."
        );
        
        $pushStatus = $this->send_message($arr_token, $message);

        return "Order <b>" . $id_order . "</b> diproses";
        
        // echo json_encode($data);
    }

    public function selesai_order($id_order, $id_pemilik)
    {

        $order = $this->modelmu->get_konsumen_order($id_order);

        if (intval($order->status) == 3 || intval($order->status) == 4) {
        	if (intval($order->status) == 3) {
            	return "Maaf, order <b>" . $id_order . "</b> sudah dibatalkan";
            } else if (intval($order->status) == 4) {
            	return "Maaf, order <b>" . $id_order . "</b> sudah selesai";
            }
        } else {
            $this->modelmu->selesai_order($id_order);
        }

        
        $data_konsumen = $this->modelmu->notif_get_konsumen($id_order);
        $data_order    = $this->modelmu->notif_get_order($id_order);
        $nama_konsumen = "";
        $id_konsumen   = "";
        $id            = "";
        $total         = "";
        $status        = "";
        
        $arr_token = array();
        foreach ($data_konsumen as $row) {
            array_push($arr_token, $row->token);
            $nama_konsumen = $row->nama;
            $id_konsumen   = $row->id_konsumen;
        }
        
        foreach ($data_order as $row) {
            $id     = $row->id_order;
            $total  = $row->total;
            $status = $row->status;
        }

        $pemilik = $this->modelmu->get_pemilik($id_pemilik);

        $perkiraan_antar = "";
        if (intval($data_order[0]->id_perkiraan_antar) != 0) {
            $perkiraan_antar_arr = $this->modelmu->get_perkiraan_antar($data_order[0]->id_perkiraan_antar);
            $perkiraan_antar_obj = $perkiraan_antar_arr[0];
            $perkiraan_antar = $perkiraan_antar_obj->nama;
        } 

        echo json_encode($data);
        
        $message = array(
            "jenis" => "1",
            "id_konsumen" => $id_konsumen,
            "nama_pemilik" => $pemilik[0]->nama,
            "id_order" => $id,
            "total" => $total,
            "status" => $status,
            "notif" => $nama_konsumen . " yth, Order anda sudah selesai."
        );
        
        $pushStatus = $this->send_message($arr_token, $message);

        return "Order <b>" . $id_order . "</b> selesai";
        
    }
    

	
    
    function send_message($registatoin_ids, $message)
    {
            // $url    = 'https://android.googleapis.com/gcm/send';
        $url    = 'https://fcm.googleapis.com/fcm/send';

        if (count($registatoin_ids) <= 0) return;

        $fields = new stdClass();
        $fields->to = $registatoin_ids[0];
        $fields->data = $message;
            // $fields = array(
            //     'to' => $registatoin_ids,
            //     'data' => $message
            //     );

        
        
        $headers = array(
            'Authorization: key=' . KEY_GCM_KONSUMEN,
            'Content-Type: application/json'
        );
        $ch      = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === FALSE) {
            $result = "salah";
        }
        curl_close($ch);
        return $result;
    }
    
}
