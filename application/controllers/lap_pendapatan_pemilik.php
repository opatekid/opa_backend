<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lap_pendapatan_pemilik extends CI_Controller {

	
    public function __construct(){
        parent::__construct();
        $this->load->model("model_laporan");
    }

    public function index() {
        $this->load->view('lap_pendapatan_pemilik');
    }
    
//    public function caritgl() {
//        $tgl = $this->input->post("tanggal");
////        $tanggal = date($tgl, "Y-m-d H:i:s");
//        //20-11-2016
//        //[20,11,2016]
//        $arr_tgl = explode("-", $tgl);
//        $tanggal = $arr_tgl[2]."-".$arr_tgl[1]."-".$arr_tgl[0];
//        $tglawal = substr($tanggal, 0, 4)."-01-01";
//        $data = array("cari" => $this->model_laporan->caritgl_pendapatan_pemilik($tanggal,$tglawal)->result());
//        echo json_encode($data);
//    }
//    
//    public function caribln() {
//        $bulan = $this->input->post("bulan");
//        $tahun = $this->input->post("tahun");
//        $data = array("cari" => $this->model_laporan->caribln_pendapatan_pemilik($bulan,$tahun)->result());
//        echo json_encode($data);
//    }
//    public function carithn() {
//        $tahun = $this->input->post("tahun");
//        $data = array("cari"=>  $this->model_laporan->carithn_pendapatan_pemilik($tahun)->result());
//        echo json_encode($data);
//    }
    
    /////////serverside///////////
    public function get_data(){
        
        //$tglawal,$tanggal,$bulan,$tahun,$jenis,$tipe
                
        $requestData        = $this->input->post();
        $tgl            = $this->input->post("tanggal");
        $bulan              = $this->input->post("bulan");
        $tahun              = $this->input->post("tahun");
        $tipe               = $this->input->post("tipe");
        $tanggal            = "";
        $tglawal            = "";
        $arr_tgl = explode("-", $tgl);
        if($tipe == "1"){
            $tanggal = $arr_tgl[2]."-".$arr_tgl[1]."-".$arr_tgl[0];
            $tglawal = substr($tanggal, 0, 4)."-01-01";
        }
        
        $pesan = "";
        
        $count = $this->model_laporan->filter_pendapatan_pemilik_get_data($tglawal,$tanggal,$bulan,$tahun,$tipe);
        $totalFiltered = $count;
        $totalData =  $count;

        if( !empty($requestData['search']['value']) ) {
            $query = $this->model_laporan->filter_pendapatan_pemilik($requestData,$tglawal,$tanggal,$bulan,$tahun,$tipe);
            $totalFiltered = $query->num_rows();
            $pesan = "pencarian..";
        }

        $query = $this->model_laporan->order_pendapatan_pemilik($requestData,$tglawal,$tanggal,$bulan,$tahun,$tipe);

        $data = array();
        
        $no = $requestData['start']+1;
        
        $index = 0;
        foreach ($query->result() as $row){
            
            $nestedData=array(); 
            $nestedData[] = $no++;
            $nestedData[] = $row->tanggal_order;
            $nestedData[] = $row->nama;
            $nestedData[] = number_format($row->kredit);
            $nestedData[] = $row->alamat;
            $data[] = $nestedData;
            
            $index++;
        }

        $json_data = array(
            "pesan"           => $pesan,
            "draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
            "recordsTotal"    => intval( $totalData ),  // total number of records
            "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data"            => $data,   // total data array
            "json"            => $query->result()   // total data array
        );
        
        echo json_encode($json_data);  // send data as json format
    }
    
}