-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 24, 2016 at 08:58 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `jekur`
--

-- --------------------------------------------------------

--
-- Table structure for table `driver`
--

CREATE TABLE IF NOT EXISTS `driver` (
  `id_driver` int(11) NOT NULL AUTO_INCREMENT,
  `nama_driver` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `no_hp` varchar(20) NOT NULL,
  `foto` varchar(10) NOT NULL,
  `versi_foto` int(11) NOT NULL DEFAULT '1',
  `username` varchar(100) NOT NULL,
  `email` varchar(40) NOT NULL,
  `password` varchar(150) NOT NULL,
  `saldo` double NOT NULL DEFAULT '0',
  `order_selesai` int(11) NOT NULL DEFAULT '0',
  `jarak_tempuh` double NOT NULL DEFAULT '0',
  `pendapatan` bigint(20) NOT NULL DEFAULT '0',
  `lat` double NOT NULL DEFAULT '0',
  `lng` double NOT NULL DEFAULT '0',
  `imei` text NOT NULL,
  `status_login` int(11) NOT NULL DEFAULT '0' COMMENT '0=tidak login, 1 = login',
  `status_order` int(11) NOT NULL DEFAULT '0' COMMENT '0: bebas order, 1:sedang jalan',
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_driver`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `driver`
--

INSERT INTO `driver` (`id_driver`, `nama_driver`, `alamat`, `no_hp`, `foto`, `versi_foto`, `username`, `email`, `password`, `saldo`, `order_selesai`, `jarak_tempuh`, `pendapatan`, `lat`, `lng`, `imei`, `status_login`, `status_order`, `status`) VALUES
(1, 'Baskoro', 'malang', '0999', '1.jpg', 1, '', 'pengguna1@gmail.com', 'b5622d0bcbe00389798dbcb868285678d72173c1bb9c5815d0fa0937325880e87a1f462c2c10af289a0b4e9644469a510dd0dbb05c3f61c824c2637a2eadf5e7', 2474008.5, 18, 7.6, 2000000, 0, 0, 'e0uiNlniRFc:APA91bFx3eDnq4pWuOSENacg_RHUtl0TXWQHzpDAOfTLQO42Ghn9bTzIUUJYQS0uscBiHBzvQ_u3njaUZtuZeD1_eemFDTO09xCOp_zvA-oQQlfwoZQAyZh6ifdFRzAJVUb4e1fwaqYX', 0, 0, 1),
(2, 'Agus Putro', 'Jalan Pahlawan No 10 Makasar', '00134', '2.jpg', 1, '', 'pengguna2@gmail.com', '8c151fbde93ec42d3dbe7571d961d577e94afa6f38db3bdf5e96a3f65eaadd314249259ae0eb2a9cd176c797a93d492f7bab97a7028f6143bf25483425061ad5', 80004.3, 23, 105.32, 4390000, 0, 0, 'iybl', 0, 0, 1),
(4, 'Lee Min Hoo', 'kendari', '08121212121', '4.jpg', 1, '', 'lee@mail.com', 'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e', 2500000, 0, 0, 0, 0, 0, '', 0, 0, 1),
(8, 'Josh Marcial', 'Jakarta', '081772819166666', '8.jpg', 4, '', 'josh@mail.com', 'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e', 90000, 0, 0, 0, 0, 0, '', 0, 0, 1),
(9, 'Steff', 'Kalimantan Tengah', '08982728', '9.jpg', 2, '', 'steff@mail.com', 'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e', 12000, 0, 0, 0, 0, 0, '', 0, 0, 1),
(10, 'ramdhan', 'malang', '1234', '10.jpg', 1, '', 'ramdhan@mail.com', '8c151fbde93ec42d3dbe7571d961d577e94afa6f38db3bdf5e96a3f65eaadd314249259ae0eb2a9cd176c797a93d492f7bab97a7028f6143bf25483425061ad5', 0, 0, 0, 0, 0, 0, '', 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `konfigurasi`
--

CREATE TABLE IF NOT EXISTS `konfigurasi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `biaya_jt` double NOT NULL,
  `biaya_tunggu` double NOT NULL,
  `no_hp` varchar(15) NOT NULL,
  `harga_voucher` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `konfigurasi`
--

INSERT INTO `konfigurasi` (`id`, `biaya_jt`, `biaya_tunggu`, `no_hp`, `harga_voucher`) VALUES
(1, 2.5, 1.4, '082144331230', 500);

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `waktu_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `waktu_logout` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `id_driver` int(11) NOT NULL,
  `imei` text NOT NULL,
  `lat_login` float NOT NULL,
  `lng_login` float NOT NULL,
  `lat_logout` float NOT NULL,
  `lng_logout` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`waktu_login`, `waktu_logout`, `id_driver`, `imei`, `lat_login`, `lng_login`, `lat_logout`, `lng_logout`) VALUES
('2016-01-05 15:07:26', '0000-00-00 00:00:00', 1, '353811052283002', 0, 0, 0, 0),
('2016-01-06 01:42:21', '0000-00-00 00:00:00', 1, '355998049455161', 0, 0, 0, 0),
('2016-01-06 01:47:48', '0000-00-00 00:00:00', 1, '355998049455161', 0, 0, 0, 0),
('2016-01-06 01:50:41', '0000-00-00 00:00:00', 1, '355998049455161', 0, 0, 0, 0),
('2016-01-06 01:52:50', '0000-00-00 00:00:00', 1, '355998049455161', 0, 0, 0, 0),
('2016-01-06 01:56:18', '0000-00-00 00:00:00', 1, '355998049455161', 0, 0, 0, 0),
('2016-01-06 02:10:01', '0000-00-00 00:00:00', 1, '355998049455161', 0, 0, 0, 0),
('2016-01-06 02:12:29', '0000-00-00 00:00:00', 1, '355998049455161', 0, 0, 0, 0),
('2016-01-06 02:20:14', '0000-00-00 00:00:00', 1, '355998049455161', 0, 0, 0, 0),
('2016-01-06 02:41:11', '0000-00-00 00:00:00', 1, '355998049455161', 0, 0, 0, 0),
('2016-01-06 02:58:37', '0000-00-00 00:00:00', 1, '355998049455161', 0, 0, 0, 0),
('2016-01-06 03:09:09', '0000-00-00 00:00:00', 1, '355998049455161', 0, 0, 0, 0),
('2016-01-06 03:20:23', '0000-00-00 00:00:00', 1, '355998049455161', 0, 0, 0, 0),
('2016-01-06 03:32:38', '0000-00-00 00:00:00', 1, '355998049455161', 0, 0, 0, 0),
('2016-01-06 04:29:32', '0000-00-00 00:00:00', 1, '355998049455161', 0, 0, 0, 0),
('2016-01-06 04:39:19', '2016-01-06 05:59:55', 1, '355998049455161', 0, 0, 0, 0),
('2016-01-06 06:00:04', '2016-01-06 06:00:09', 1, '355998049455161', 0, 0, 0, 0),
('2016-01-06 08:22:35', '0000-00-00 00:00:00', 1, '353811052283002', 0, 0, 0, 0),
('2016-01-06 08:24:16', '0000-00-00 00:00:00', 1, '353811052283002', 0, 0, 0, 0),
('2016-01-06 08:28:55', '0000-00-00 00:00:00', 1, '353811052283002', 0, 0, 0, 0),
('2016-01-06 08:41:31', '2016-01-06 08:43:48', 1, '355998049455161', 0, 0, 0, 0),
('2016-01-06 08:44:12', '0000-00-00 00:00:00', 1, '355998049455161', 0, 0, 0, 0),
('2016-01-06 08:52:20', '0000-00-00 00:00:00', 1, '355998049455161', 0, 0, 0, 0),
('2016-01-06 09:18:18', '0000-00-00 00:00:00', 1, '353811052283002', 0, 0, 0, 0),
('2016-01-06 09:20:05', '0000-00-00 00:00:00', 1, '353811052283002', 0, 0, 0, 0),
('2016-01-06 09:21:38', '0000-00-00 00:00:00', 1, '353811052283002', 0, 0, 0, 0),
('2016-01-06 09:23:11', '0000-00-00 00:00:00', 1, '353811052283002', 0, 0, 0, 0),
('2016-01-06 09:27:22', '2016-01-06 10:02:33', 1, '353811052283002', 0, 0, 0, 0),
('2016-01-06 10:02:43', '2016-01-06 10:05:15', 1, '353811052283002', 0, 0, 0, 0),
('2016-01-06 10:05:25', '2016-01-06 10:06:25', 1, '353811052283002', 0, 0, 0, 0),
('2016-01-06 10:07:08', '2016-01-06 10:50:06', 1, '353811052283002', 0, 0, 0, 0),
('2016-01-06 10:50:40', '2016-01-06 10:51:55', 1, '353811052283002', 0, 0, 0, 0),
('2016-01-06 11:04:33', '2016-01-06 11:05:36', 1, '353811052283002', 0, 0, 0, 0),
('2016-01-06 11:05:53', '2016-01-06 11:14:43', 1, '353811052283002', 0, 0, 0, 0),
('2016-01-06 11:30:29', '2016-01-06 11:31:03', 1, '353811052283002', 0, 0, 0, 0),
('2016-01-06 11:37:54', '2016-01-06 11:38:40', 1, '353811052283002', 0, 0, 0, 0),
('2016-01-06 11:38:47', '0000-00-00 00:00:00', 1, '353811052283002', 0, 0, 0, 0),
('2016-01-06 11:48:06', '2016-01-06 11:48:17', 1, '353811052283002', 0, 0, 0, 0),
('2016-01-06 11:51:57', '2016-01-06 11:55:00', 1, '353811052283002', 0, 0, 0, 0),
('2016-01-06 11:55:31', '2016-01-06 12:09:21', 1, '353811052283002', 0, 0, 0, 0),
('2016-01-06 12:09:27', '2016-01-06 12:14:06', 1, '353811052283002', 0, 0, 0, 0),
('2016-01-06 12:14:08', '2016-01-06 12:36:04', 1, '353811052283002', 0, 0, 0, 0),
('2016-01-06 12:36:10', '2016-01-06 12:37:18', 1, '353811052283002', 0, 0, 0, 0),
('2016-01-06 12:37:22', '2016-01-06 12:46:18', 1, '353811052283002', 0, 0, 0, 0),
('2016-01-06 12:46:21', '0000-00-00 00:00:00', 1, '353811052283002', 0, 0, 0, 0),
('2016-01-07 04:08:22', '0000-00-00 00:00:00', 1, '353811052283002', 0, 0, 0, 0),
('2016-01-07 04:13:29', '0000-00-00 00:00:00', 1, '353811052283002', 0, 0, 0, 0),
('2016-01-07 07:00:04', '0000-00-00 00:00:00', 1, '353811052283002', 0, 0, 0, 0),
('2016-01-07 15:08:21', '0000-00-00 00:00:00', 1, '353811052283002', 0, 0, 0, 0),
('2016-01-07 15:15:39', '2016-01-07 15:43:09', 1, '353811052283002', 0, 0, 0, 0),
('2016-01-07 15:43:11', '0000-00-00 00:00:00', 1, '353811052283002', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE IF NOT EXISTS `order` (
  `id_order` bigint(20) NOT NULL AUTO_INCREMENT,
  `no_order` varchar(20) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `jenis` int(11) NOT NULL,
  `id_driver` int(11) NOT NULL DEFAULT '0',
  `biaya_jt` double NOT NULL DEFAULT '0',
  `biaya_tunggu` double NOT NULL DEFAULT '0',
  `jarak` double NOT NULL DEFAULT '0',
  `waktu_tunggu` int(11) NOT NULL DEFAULT '0',
  `biaya_sa` int(11) NOT NULL DEFAULT '0',
  `total_jt` int(11) NOT NULL DEFAULT '0',
  `total_tunggu` int(11) NOT NULL DEFAULT '0',
  `dari` varchar(100) NOT NULL,
  `sampai` varchar(100) NOT NULL,
  `coordinate_awal` text NOT NULL,
  `coordinate_akhir` text NOT NULL,
  `penumpang` varchar(30) NOT NULL DEFAULT '0',
  `catatan` varchar(100) NOT NULL DEFAULT '-',
  `est_jarak` int(11) NOT NULL DEFAULT '0',
  `est_biaya` int(11) NOT NULL,
  PRIMARY KEY (`id_order`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=77 ;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`id_order`, `no_order`, `tanggal`, `jenis`, `id_driver`, `biaya_jt`, `biaya_tunggu`, `jarak`, `waktu_tunggu`, `biaya_sa`, `total_jt`, `total_tunggu`, `dari`, `sampai`, `coordinate_awal`, `coordinate_akhir`, `penumpang`, `catatan`, `est_jarak`, `est_biaya`) VALUES
(64, '201601132#20739', '2016-01-13 08:34:00', 2, 2, 0, 0, 0, 0, 0, 0, 0, 'dua', 'empat', '', '', '0', 'lima', 0, 0),
(65, '201601131#67426', '2016-01-13 08:48:04', 2, 1, 0, 0, 0, 0, 0, 0, 0, 'situ', 'siapa', '', '', '0', 'okelah', 0, 0),
(66, '201601131#76415', '2016-01-13 08:57:10', 2, 1, 0, 0, 0, 0, 0, 0, 0, 'iki', 'iku', '', '', '0', 'ojo', 0, 0),
(67, '201601131#19871', '2016-01-13 08:58:37', 2, 1, 1, 2, 3, 4, 5, 6, 7, 'Malang', 'Blitar', '', '', '0', 'langsung', 8, 10000),
(68, '201601131#48307', '2016-01-13 09:09:51', 2, 1, 0, 0, 0, 0, 20000, 32000, 12000, 'nggalek', 'nggalek juga', '', '', '0', 'Ketemu di poltekom', 0, 0),
(69, '20160115apt#11667', '2016-01-15 07:12:27', 2, 1, 0, 0, 0, 0, 0, 0, 0, 'Jalan karya', 'jalan menteng', '', '', '0', 'sip lah', 0, 0),
(70, '201601164#57104', '2016-01-16 15:15:43', 2, 4, 0, 0, 0, 0, 2700, 7900, 2300, 'kulon progo', 'alas purwo', '', '', '0', 'jam 12 bengi', 0, 0),
(71, '201601161#28542', '2016-01-16 15:19:58', 2, 1, 0, 0, 0, 0, 0, 0, 0, 'j', '', '', '', '0', '', 0, 0),
(72, '20160116apt#75160', '2016-01-16 15:20:32', 2, 1, 0, 0, 0, 0, 0, 0, 0, 'k', 'j', '', '', '0', 'o', 0, 0),
(73, '20160122apt#82488', '2016-01-22 07:23:29', 2, 0, 0, 0, 0, 0, 0, 0, 0, 'jalan awal', 'jalan akhir', '', '', '0', 'catatan', 0, 0),
(74, '20160122apt#81326', '2016-01-22 07:24:05', 2, 0, 0, 0, 0, 0, 0, 0, 0, 'jalan a', 'jalan b', '', '', '0', 'catatan', 0, 0),
(75, '20160122apt#65783', '2016-01-22 07:26:27', 2, 1, 0, 0, 0, 0, 0, 0, 0, 'jalan c', 'jalan d', '', '', '0', 'catatan 3', 0, 0),
(76, '20160122apt#40825', '2016-02-22 07:26:44', 2, 1, 0, 0, 0, 0, 0, 0, 0, 'jalan q', 'jalan w', '', '', '0', 'catatan 4', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_voucher`
--

CREATE TABLE IF NOT EXISTS `pembelian_voucher` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_pengguna` int(11) NOT NULL,
  `id_driver` int(11) NOT NULL,
  `harga_voucher` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `total` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `pembelian_voucher`
--

INSERT INTO `pembelian_voucher` (`id`, `tanggal`, `id_pengguna`, `id_driver`, `harga_voucher`, `jumlah`, `total`) VALUES
(1, '2016-01-16 09:31:43', 1, 2, 5000, 0, 0),
(2, '2016-01-16 09:31:55', 1, 1, 5000, 9, 45000),
(3, '2016-01-16 15:09:23', 1, 8, 4000, 90, 360000),
(4, '2016-01-16 15:36:52', 1, 9, 4000, 12, 48000),
(5, '2016-01-22 02:48:32', 1, 4, 500, 100, 50000),
(6, '2016-01-22 02:54:55', 1, 1, 500, 10, 5000),
(7, '2016-01-22 02:56:22', 1, 1, 500, 500, 250000),
(8, '2016-01-22 02:57:26', 1, 1, 500, 100, 50000),
(9, '2016-01-22 02:58:01', 1, 1, 500, 1000, 500000),
(10, '2016-01-22 03:00:08', 1, 1, 500, 50, 25000),
(11, '2016-01-22 03:00:28', 1, 0, 500, 10, 5000),
(12, '2016-01-22 03:00:55', 1, 1, 500, 20, 10000),
(13, '2016-01-22 03:03:12', 1, 1, 500, 30, 15000),
(14, '2016-01-22 03:04:28', 1, 1, 500, 15, 7500),
(15, '2016-01-22 03:06:27', 1, 1, 500, 40, 20000),
(16, '2016-01-22 03:07:56', 1, 1, 500, 30, 15000),
(17, '2016-01-22 03:08:12', 1, 1, 500, 60, 30000),
(18, '2016-01-22 03:11:22', 1, 1, 500, 20, 10000),
(19, '2016-01-22 03:13:04', 1, 2, 500, 80, 40000),
(20, '2016-01-22 03:14:11', 1, 1, 500, 66, 33000),
(21, '2016-01-22 03:30:33', 1, 1, 500, 68, 34000),
(22, '2016-01-22 03:31:04', 1, 1, 500, 100, 50000),
(23, '2016-01-22 03:37:38', 1, 4, 500, 2400, 1200000),
(24, '2016-01-22 03:39:59', 1, 1, 500, 12, 6000),
(25, '2016-01-22 03:49:20', 3, 1, 500, 34, 17000),
(26, '2016-01-22 03:50:05', 3, 1, 500, 310, 155000);

--
-- Triggers `pembelian_voucher`
--
DROP TRIGGER IF EXISTS `tambah_saldo`;
DELIMITER //
CREATE TRIGGER `tambah_saldo` AFTER INSERT ON `pembelian_voucher`
 FOR EACH ROW begin

update driver set saldo = saldo+(1000*new.jumlah) where id_driver = new.id_driver;

end
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `pengguna`
--

CREATE TABLE IF NOT EXISTS `pengguna` (
  `id_pengguna` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `username` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(130) NOT NULL,
  `saldo` bigint(20) NOT NULL DEFAULT '0',
  `level` int(11) NOT NULL COMMENT '1=admin,2=kasir',
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_pengguna`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `pengguna`
--

INSERT INTO `pengguna` (`id_pengguna`, `nama`, `username`, `email`, `password`, `saldo`, `level`, `status`) VALUES
(1, 'Administrator', 'admin', 'admin@mail.com', 'c7ad44cbad762a5da0a452f9e854fdc1e0e7a52a38015f23f3eab1d80b931dd472634dfac71cd34ebc35d16ab7fb8a90c81f975113d6c7538dc69dd8de9077ec', 0, 1, 1),
(2, 'rizaldi', 'rizaldi', 'ahmadzainuri94@gmail.comm', '8c151fbde93ec42d3dbe7571d961d577e94afa6f38db3bdf5e96a3f65eaadd314249259ae0eb2a9cd176c797a93d492f7bab97a7028f6143bf25483425061ad5', 545000, 2, 1),
(3, 'john', 'john', 'john@mail.com', '8c151fbde93ec42d3dbe7571d961d577e94afa6f38db3bdf5e96a3f65eaadd314249259ae0eb2a9cd176c797a93d492f7bab97a7028f6143bf25483425061ad5', 1022000, 2, 1),
(4, 'roni', 'roni', 'roni@mail.com', '8c151fbde93ec42d3dbe7571d961d577e94afa6f38db3bdf5e96a3f65eaadd314249259ae0eb2a9cd176c797a93d492f7bab97a7028f6143bf25483425061ad5', 0, 2, 1),
(5, 'wardi', 'wardi', 'wardi@mail.com', '8c151fbde93ec42d3dbe7571d961d577e94afa6f38db3bdf5e96a3f65eaadd314249259ae0eb2a9cd176c797a93d492f7bab97a7028f6143bf25483425061ad5', 2000000, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `topup`
--

CREATE TABLE IF NOT EXISTS `topup` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_pengguna` int(11) NOT NULL,
  `jumlah` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `topup`
--

INSERT INTO `topup` (`id`, `tanggal`, `id_pengguna`, `jumlah`) VALUES
(1, '2016-01-22 02:23:19', 2, 1000000),
(2, '2016-01-22 02:23:45', 2, 200000),
(3, '2016-01-22 03:15:29', 3, 1200000),
(4, '2016-01-22 03:28:41', 2, 200000),
(5, '2016-01-22 03:33:24', 5, 2000000),
(6, '2016-01-22 03:38:49', 3, 1000000),
(7, '2016-01-22 03:39:08', 3, 200000);

--
-- Triggers `topup`
--
DROP TRIGGER IF EXISTS `update_pengguna`;
DELIMITER //
CREATE TRIGGER `update_pengguna` AFTER INSERT ON `topup`
 FOR EACH ROW begin

update pengguna set saldo=saldo+new.jumlah where id_pengguna=new.id_pengguna;

end
//
DELIMITER ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
