{
  "code": "1",
  "pesan": "Berhasil",
  "data": {
    "supplier": [
      {
        "id_kategori": "1",
        "nama_kategori": "Air Galon",
        "id_jenis": "1",
        "status": "1",
        "induk_kategori": "Toko"
      },
      {
        "id_kategori": "2",
        "nama_kategori": "Gas",
        "id_jenis": "1",
        "status": "1",
        "induk_kategori": "Toko"
      },
      {
        "id_kategori": "8",
        "nama_kategori": "Sembako",
        "id_jenis": "1",
        "status": "1",
        "induk_kategori": "Toko"
      },
      {
        "id_kategori": "9",
        "nama_kategori": "Warung Makan",
        "id_jenis": "1",
        "status": "1",
        "induk_kategori": "Toko"
      },
      {
        "id_kategori": "12",
        "nama_kategori": "Toko Listrik",
        "id_jenis": "1",
        "status": "1",
        "induk_kategori": "Toko"
      },
      {
        "id_kategori": "13",
        "nama_kategori": "Toko Bangunan",
        "id_jenis": "1",
        "status": "1",
        "induk_kategori": "Toko"
      },
      {
        "id_kategori": "14",
        "nama_kategori": "Telor",
        "id_jenis": "1",
        "status": "1",
        "induk_kategori": "Toko"
      },
      {
        "id_kategori": "15",
        "nama_kategori": "Bensin Eceran",
        "id_jenis": "1",
        "status": "1",
        "induk_kategori": "Toko"
      },
      {
        "id_kategori": "16",
        "nama_kategori": "Apotik & Kesehatan",
        "id_jenis": "1",
        "status": "1",
        "induk_kategori": "Toko"
      }
    ],
    "info": [
      {
        "id_supplier": "35400284",
        "level": "1",
        "id_jenis": "1",
        "tipe": "1",
        "kredit": "998500",
        "username": "User Baru",
        "email": "",
        "password": "a79a122977d9a519f17dea1335ef4dda415f0d3b5005c3cafa1bc7081770240a292f123e4cd14fa986720b99eca38abfb12ab81a765d06cbe00d5b3486b52b93",
        "nama": "AHS Handoyo",
        "jargon": "",
        "alamat": "Jl. Pertani Gg. I",
        "kontak": "089636656498",
        "id_provinsi": "12",
        "id_kota": "163",
        "lat": "-6.257581228022878",
        "lng": "106.84098891913892",
        "id_radius": "4",
        "foto": "35400284.jpg",
        "versi": "2",
        "avatar": "",
        "versi_avatar": "1",
        "kode": "1818",
        "tanggal_insert": "2016-11-21 09:53:16",
        "tanggal_update": "2016-11-21 02:53:16",
        "token": "dpajqPAJvxE:APA91bF7xUmAN64Rbsg_oIn008ek-6UXdMrGNBxCCgEiu0rM4IfIBNMWcqlKsy9oLu1rNRontHTTJEd_xnif79TRyOp9QGsSQepkjWwhedh5WwNY220fFSzTw0bMqVmielwurjy-AZ63",
        "id_kategori": "0",
        "status": "1",
        "status_toko": "1",
        "tipe_usaha": "1",
        "radius": "1000"
      }
    ],
    "produk": [
      {
        "id_produk": "16112109552335400284515",
        "id_supplier": "35400284",
        "id_kategori": "54",
        "nama_produk": "Gas 12 kg",
        "stok": "22",
        "keterangan": "",
        "harga": "85000",
        "status": "1",
        "foto": "",
        "versi": "1",
        "nama_kategori": "Gas"
      },
      {
        "id_produk": "16112109561935400284289",
        "id_supplier": "35400284",
        "id_kategori": "205",
        "nama_produk": "Gas 3 kg / Melon",
        "stok": "147",
        "keterangan": "",
        "harga": "25000",
        "status": "1",
        "foto": "",
        "versi": "1",
        "nama_kategori": "Gas"
      },
      {
        "id_produk": "16112109565735400284661",
        "id_supplier": "35400284",
        "id_kategori": "206",
        "nama_produk": "Refill Aqua",
        "stok": "173",
        "keterangan": "",
        "harga": "18000",
        "status": "1",
        "foto": "",
        "versi": "1",
        "nama_kategori": "Galon Refill"
      }
    ],
    "produk_promo": [
      
    ]
  }
}